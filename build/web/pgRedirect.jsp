<%@page import="java.util.UUID"%>
<%@page import="com.paytm.pg.merchant.CheckSumServiceHelper"%>
<%@page import="java.util.TreeMap"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%
/*
Enumeration<String> paramNames = request.getParameterNames();
Map<String, String[]> mapData = request.getParameterMap();
TreeMap<String,String> parameters = new TreeMap<String,String>();
String paytmChecksum =  "";
while(paramNames.hasMoreElements()) {
	String paramName = (String)paramNames.nextElement();
	parameters.put(paramName,mapData.get(paramName)[0]);	
}
*/
    String orderid=UUID.randomUUID().toString();
TreeMap<String,String> parameters = new TreeMap<String,String>();
parameters.put("MID","SATTVI68059644107892");
parameters.put("ORDER_ID",orderid);
parameters.put("CHANNEL_ID","WEB");
parameters.put("INDUSTRY_TYPE_ID","Retail");
parameters.put("CUST_ID","cust123");
parameters.put("TXN_AMOUNT","1");
parameters.put("WEBSITE","WEBSTAGING");
parameters.put("MOBILE_NO","9876543210");
parameters.put("EMAIL","test@gmail.com");
parameters.put("CALLBACK_URL", "http://localhost:8084/spycis/pgResponse1.jsp");


String checkSum =  CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum("NuCU1k%3B9tQNL#Y", parameters);


StringBuilder outputHtml = new StringBuilder();
outputHtml.append("<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>");
outputHtml.append("<html>");
outputHtml.append("<head>");
outputHtml.append("<title>Merchant Check Out Page</title>");
outputHtml.append("</head>");
outputHtml.append("<body>");
outputHtml.append("<center><h1>Please do not refresh this page...</h1></center>");
outputHtml.append("<form method='post' action='https://securegw-stage.paytm.in/theia/processTransaction' name='f1'>");
outputHtml.append("<table border='1'>");
outputHtml.append("<tbody>");

/*for(Map.Entry<String,String> entry : parameters.entrySet()) {
	String key = entry.getKey();
	String value = entry.getValue();
	outputHtml.append("<input type='hidden' name='"+key+"' value='" +value+"'>");	
}	  
	*/  
outputHtml.append("<input type='hidden' name='MID' value='SATTVI68059644107892'>");
outputHtml.append("<input type='hidden' name='CHANNEL_ID' value='WEB'>");
outputHtml.append("<input type='hidden' name='INDUSTRY_TYPE_ID' value='Retail'>");
outputHtml.append("<input type='hidden' name='WEBSITE' value='WEBSTAGING'>");
outputHtml.append("<input type='hidden' name='TXN_AMOUNT' value='1'>");
outputHtml.append("<input type='hidden' name='ORDER_ID' value='"+orderid+"'>");
outputHtml.append("<input type='hidden' name='MOBILE_NO' value='9876543210'>");
outputHtml.append("<input type='hidden' name='CUST_ID' value='cust123'>");
outputHtml.append("<input type='hidden' name='EMAIL' value='test@gmail.com'>");	
outputHtml.append("<input type='hidden' name='CALLBACK_URL' value='http://localhost:8084/spycis/pgResponse1.jsp'>");
outputHtml.append("<input type='hidden' name='CHECKSUMHASH' value='"+checkSum+"'>");
outputHtml.append("</tbody>");
outputHtml.append("</table>");
outputHtml.append("<script type='text/javascript'>");
outputHtml.append("document.f1.submit();");
outputHtml.append("</script>");
outputHtml.append("</form>");
outputHtml.append("</body>");
outputHtml.append("</html>");
out.println(outputHtml);
%>
