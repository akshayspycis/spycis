
<!--    Document   : Mock test
    Created on : Aug 19, 2015, 01:50:04 AM
    Author     : Akshay Bilani-->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Welcomes to Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="../../dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
        
label.valid {
	width: 24px;
	height: 24px;
	background: url(../../dist/images/valid.png) center center no-repeat;
	display: inline-block;
	text-indent: -9999px;
}
label.error {
	font-weight: bold;
	color: red;
	padding: 2px 8px;
	margin-top: 2px;
}

        .timer{
            color:white;
            font-weight: bold;
            font-family:verdana;
            padding-left:10px;
            padding-right:10px;
            padding-top:8px;
            top:15px;
            position:relative;
            padding-bottom:10px;
            
        }

        .caret-up {
        width: 0; 
        height: 0; 
        border-left: 4px solid rgba(0, 0, 0, 0);
        border-right: 4px solid rgba(0, 0, 0, 0);
        border-bottom: 4px solid;

        display: inline-block;
        margin-left: 2px;
        vertical-align: middle;
        }
        #success{
            background:yellowgreen;
            border:none;
            float:right;
        }
       #success:hover{
            background:yellowgreen;
            font-weight:bold;
            
        }
        #defaultb{
            background:white;
            border:none;
            float:right;
            color:black;
        }
        #defaultb:hover{
             background:yellowgreen;
             font-weight:bold;
        }
        #review{
            background:#f39c12;
            border:none;
            float:right;
        }
        #review:hover{
             background:yellowgreen;
                font-weight:bold;
            
        }
        #notanswer{
            background:#D81B60;
            border:none;
            float:right;
        }
        #notanswer:hover{
              background:yellowgreen;
             font-weight:bold;
        }
        .box-title{
            font-weight:bold;
        }
        .question{
            font-family: Verdana, Geneva, sans-serif;
            font-size:12px;
            font-weight: bold;
            line-height:24px;
        }
       
    </style>
    
                
      <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>    
<!--                <script src="../model/Config.js"></script>
                <script src="../view/HeaderVgr.js"></script>
                <script src="../view/MainVgr.js"></script>
                <script src="../view/MainSidebarVgr.js"></script>
                <script src="../model/Profileconfig.js"></script>-->
                <!--<script src="view/notificationVgr.js"></script>-->
          <!-- jQuery 2.1.4 -->
    <!--<script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>-->
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.js" type="text/javascript"></script>
    <script src="../../dist/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <!--<script src="../dist/js/demo.js" type="text/javascript"></script>-->
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue sidebar-mini ">
       <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header ">
        <!-- Logo -->
        <a class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>V E</b> </span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Vibrant Education</b> </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li><span class="timer"><i class="fa fa-bell" style="font-size:20px;"></i> 10:10:00</span></li>
               <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image" />
                  <span class="hidden-xs">Lalit Pastor</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                      <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                    <p>
                       Lalit Pastor
                      <small>Candidate Id  V1232</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                <!-- Menu Footer-->
                  <li class="user-footer">
                    
                    <div class="pull-right">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                  <a href="#" data-toggle="control-sidebar" class="active" title="Open the sidebar"><i style="font-size:18px;" class="fa  fa fa-edit"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <ul class="sidebar-menu">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i> <span>Reasoning</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li><a href="#">Answered   <span style="float:right"class="badge" id="success">45</span> </a></li>
                  <li><a href="#"> Not Answered <span class="badge" id="notanswer">30</span></a></li>
                  <li><a href="#"> Mark for Review <span class="badge"id="review">25</span></a></li>
                  <li><a href="#">  Not Visited <span class="badge" id="defaultb">45</span></a></li>
              </ul>
            </li>
            <li class="treeview active">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>English</span>
                <span class="label label-primary pull-right">4</span>
              </a>
               <ul class="treeview-menu">
                  <li><a href="#">Answered   <span style="float:right"class="badge" id="success">45</span> </a></li>
                  <li><a href="#"> Not Answered <span class="badge" id="notanswer">30</span></a></li>
                  <li><a href="#"> Mark for Review <span class="badge"id="review">25</span></a></li>
                  <li><a href="#">  Not Visited <span class="badge" id="defaultb">45</span></a></li>
              </ul>
            </li>
            <li>
              <a href="../widgets.html">
                <i class="fa fa-th"></i> <span>Quantitative Aptitude</span> <small class="label pull-right bg-green">new</small>
              </a>
                 <ul class="treeview-menu">
                  <li><a href="#">Answered   <span style="float:right"class="badge" id="success">45</span> </a></li>
                  <li><a href="#"> Not Answered <span class="badge" id="notanswer">30</span></a></li>
                  <li><a href="#"> Mark for Review <span class="badge"id="review">25</span></a></li>
                  <li><a href="#">  Not Visited <span class="badge" id="defaultb">45</span></a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>General Awareness </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
               <ul class="treeview-menu">
                  <li><a href="#">Answered   <span style="float:right"class="badge" id="success">45</span> </a></li>
                  <li><a href="#"> Not Answered <span class="badge" id="notanswer">30</span></a></li>
                  <li><a href="#"> Mark for Review <span class="badge"id="review">25</span></a></li>
                  <li><a href="#">  Not Visited <span class="badge" id="defaultb">45</span></a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Computer Knowledge</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li><a href="#">Answered   <span style="float:right"class="badge" id="success">45</span> </a></li>
                  <li><a href="#"> Not Answered <span class="badge" id="notanswer">30</span></a></li>
                  <li><a href="#"> Mark for Review <span class="badge"id="review">25</span></a></li>
                  <li><a href="#">  Not Visited <span class="badge" id="defaultb">45</span></a></li>
              </ul>
            </li>
           
          
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <p>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">General Elements</li>
            </ol>
            </p>
            <ol style="margin:0px;padding:0px;"class="breadcrumb">
          </ol>
        </section>

        <!-- Main content -->
            <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
                    <h3 class="box-title">Configuration</h3>
            </div>
                <div class="box-body">
                    <div class="col-md-6">
                            <div class="box-header with-border">
                              <h3 class="box-title">Category</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                              <table class="table table-bordered">
                                <tbody><tr>
                                  <th style="width: 10px">#</th>
                                  <th>Category Name</th>
                                  <th style="width: 40px">Action</th>
                                </tr>
                                <tr>
                                  <td>1.</td>
                                  <td>Update software</td>
                                  <td>
                                          <div class="input-group-btn">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Delete</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Edit</a></li>
                                              </ul>
                                          </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>2.</td>
                                  <td>Clean database</td>
                                  <td>
                                  <div class="input-group-btn">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Delete</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Edit</a></li>
                                              </ul>
                                          </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>3.</td>
                                  <td>Cron job running</td>
                                  <td>
                                          <div class="input-group-btn">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Delete</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Edit</a></li>
                                              </ul>
                                          </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>4.</td>
                                  <td>Fix and squish bugs</td>
                                  <td>
                                  <div class="input-group-btn">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Delete</a></li>
                                                <li class="divider"></li>
                                                <li><a  >Edit</a>
                                                </li>
                                                    
                                              </ul>
                                          </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>4.</td>
                                  <td>Fix and squish bugs</td>
                                  <td>
                                  <div class="input-group-btn">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Delete</a></li>
                                                <li class="divider"></li>
                                                <li><a  >Edit</a>
                                                </li>
                                                    
                                              </ul>
                                          </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>4.</td>
                                  <td>Fix and squish bugs</td>
                                  <td>
                                  <div class="input-group-btn">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Delete</a></li>
                                                <li class="divider"></li>
                                                <li><a  >Edit</a>
                                                </li>
                                                    
                                              </ul>
                                          </div>
                                  </td>
                                </tr>
                              </tbody></table>
                            </div><!-- /.box-body -->
                            <div class="box-footer clearfix">
                                
                                <div class="col-md-3 col-xs-4"><button class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal">New</button></div>
                                
                                
                                     <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                              <div class="modal-header with-border">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Category Details</h4>
                                              </div>
                                              <div class="modal-body">
                                                  
                                          <form id="registration-form" class="form-horizontal">
                                                        <div class="form-group">
                                                <div class="form-control-group col-lg-12">
                                                    <label class="control-label" for="name">Your Name</label>
                                                    <div class="controls">
                                                    <input type="text" class="input-xlarge form-control" name="name" id="name">
                                                    </div>
                                                </div>
                                                <div class="form-control-group col-lg-12">
                                                    <label class="control-label" for="name">User Name</label>
                                                    <div class="controls ">
                                                    <input type="text" class="input-xlarge form-control" name="username" id="username">
                                                    </div>
                                                </div>
                                                <div class="form-control-group col-lg-12">
                                                    <label class="control-label " for="name">Password</label>
                                                    <div class="controls ">
                                                    <input type="password" class="input-xlarge form-control" name="password" id="password">
                                                    </div>
                                                </div>
                                                <div class="form-control-group col-lg-12">
                                                    <label class="control-label " for="name"> Retype Password</label>
                                                    <div class="controls ">
                                                    <input type="password" class="input-xlarge form-control" name="confirm_password" id="confirm_password">
                                                    </div>
                                                </div>
                                                <div class="form-control-group col-lg-12">
                                                    <label class="control-label" for="email">Email Address</label>
                                                    <div class="controls ">
                                                    <input type="text" class="input-xlarge form-control" name="email" id="email">
                                                    </div>
                                                </div>
                                                <div class="form-control-group col-lg-12">
                                                    <label class="control-label" for="message">Your Address</label>
                                                    <div class="controls ">
                                                    <textarea class="input-xlarge form-control" name="address" id="address" rows="3"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-control-group col-lg-12">
                                                    <label class="control-label " for="message"> Please agree to our policy</label>
                                                    <div class="controls ">
                                                    <input id="agree" class="checkbox" type="checkbox" name="agree">
                                                    </div>
                                                </div>
                                              
                                               </div>
                                                        <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                              </div>
                                            </form>
                                                  
                                                  
                                              </div>
                                              
                                        </div>
                                    </div>
                                </div>
                              <ul class="pagination pagination-sm no-margin pull-right">
                                <li><a href="#">�</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">�</a></li>
                              </ul>
                            </div>
                    </div>
                    <div class="col-md-6">
                            <div class="box-header with-border">
                              <h3 class="box-title">Sub Category</h3>
                              
                            </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Category :</label>
                                        <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                            <option selected="selected">Alabama</option>
                                            <option>Alaska</option>
                                            <option>California</option>
                                            <option>Delaware</option>
                                            <option>Tennessee</option>
                                            <option>Texas</option>
                                            <option>Washington</option>
                                        </select>
                                    </div>
                              <table class="table table-bordered">
                                <tbody><tr>
                                  <th style="width: 10px">#</th>
                                  <th>Sub Category</th>
                                  <th style="width: 40px">Action</th>
                                </tr>
                                <tr>
                                  <td>1.</td>
                                  <td>Update software</td>
                                  <td>
                                          <div class="input-group-btn">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Delete</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Edit</a></li>
                                              </ul>
                                          </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>2.</td>
                                  <td>Clean database</td>
                                  <td>
                                  <div class="input-group-btn">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Delete</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Edit</a></li>
                                              </ul>
                                          </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>3.</td>
                                  <td>Cron job running</td>
                                  <td>
                                          <div class="input-group-btn">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Delete</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Edit</a></li>
                                              </ul>
                                          </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>4.</td>
                                  <td>Fix and squish bugs</td>
                                  <td>
                                  <div class="input-group-btn">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="#">Delete</a></li>
                                                <li class="divider"></li>
                                                <li><a  >Edit</a>
                                                </li>
                                                    
                                              </ul>
                                          </div>
                                  </td>
                                </tr>
                              </tbody></table>
                            </div><!-- /.box-body -->
                            <div class="box-footer clearfix">
                                <div class="col-md-3 col-xs-4"><button class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal">New</button></div>
                                 <!--                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                              <div class="modal-header with-border">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Category Details</h4>
                                              </div>
                                              <div class="modal-body">
                                                    <form class="form-horizontal">
                                                        <div class="form-group">
                                                          <label for="inputEmail3" class="col-sm-2 control-label">Category</label>
                                                          <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="inputEmail3" placeholder="Category Name">
                                                          </div>
                                                        </div>
                                                    </form>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save</button>
                                              </div>
                                        </div>
                                    </div>
                                </div>-->
                              <ul class="pagination pagination-sm no-margin pull-right">
                                <li><a href="#">�</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">�</a></li>
                              </ul>
                            </div>
                    </div>
                    
                </div><!-- /.box-body -->
            <div class="box-footer">
            
            </div><!-- /.box-footer-->
          </div><!-- /.box -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b>
        </div>
       <strong> </strong>
      </footer>

      <!-- Control Sidebar -->
            
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
    </div><!-- ./wrapper -->
  </body>
  
  
    <script>
            $(function(){
                $(".dropdown").hover(            
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                            $(this).toggleClass('open');
                            $('b', this).toggleClass("caret caret-up");                
                        },
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                            $(this).toggleClass('open');
                            $('b', this).toggleClass("caret caret-up");                
            });
    });
    
        $(window).resize(function() {
             if ($(window).width() >767) {
                 $('body').addClass('control-sidebar-open');
                 $('body').addClass('sidebar-collapse');
             } else {
                 $('body').removeClass('control-sidebar-open');
                 $('body').removeClass('sidebar-collapse');
             }
            });
            if ($(window).width() >767) {
                 $('body').addClass('control-sidebar-open');
                 $('body').addClass('sidebar-collapse');
             } else {
                 $('body').removeClass('control-sidebar-open');
                 $('body').removeClass('sidebar-collapse');
             }
             
             
             $(document).ready(function(){

$('#registration-form').validate({
	    rules: {
	       name: {
	        required: true,
	       required: true
	      },
		  
		 username: {
	        minlength: 6,
	        required: true
	      },
		  
		  password: {
				required: true,
				minlength: 6
			},
			confirm_password: {
				required: true,
				minlength: 6,
				equalTo: "#password"
			},
		  
	      email: {
	        required: true,
	        email: true
	      },
		  
	     
		   address: {
	      	minlength: 10,
	        required: true
	      },
		  
		  agree: "required"
		  
	    },
			highlight: function(element) {
				$(element).closest('.control-group').removeClass('success').addClass('error');
			},
			success: function(element) {
				element
				.text('OK!').addClass('valid')
				.closest('.control-group').removeClass('error').addClass('success');
			}
	  });

}); // end document.ready

		addEventListener('load', prettyPrint, false);
		$(document).ready(function(){
		$('pre').addClass('prettyprint linenums');
			});

    </script>

</html>

    