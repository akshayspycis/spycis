jQuery.extend({
	MockTestHome: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
                
		function toArray(software_name_id){
			var item = [];
                            try {
                                if(cache[software_name_id]!=null)
                                    item.push(cache[software_name_id]);
                            }catch(e){}
			return item;
		}
		/**
		 * load a json` response from an
		 * ajax call
		 */
                this.getValue = function (key){
                    return cache[key];
                }
                this.updValue = function (key,value){
                    cache[config.getModel("ProfileConfig").getUser_id()][key]=value;
                }
                
		function loadResponse(user_id,data){
                    $.each(data,function(k,v){
                        cache[k]=v;
                        that.loadItem(k,v);
                    });
                        
		}
                
                function updResponse(data){
                    cache[data.category_id][data.subcategory_id][data.section_id][data.topic_id].topic_name=data.topic_name;
                    that.updItemLoaded(data);
		}
                
                function delResponse(category_id,subcategory_id,section_id,topic_id){
                        delete cache[category_id][subcategory_id][section_id][topic_id];
		}
                
		/**
		 * look in the cache for a single item
		 * if it's there, get it, if not
		 * ask the server to load it
		 */
		this.getMockTestHome = function(category_id,subcategory_id,section_id,topic_id){
                            return cache[category_id][subcategory_id][section_id][topic_id].topic_name;
		}
                
                this.insMockTestHome =function (form){
                        that.insLoadBegin();
                        $.each(form["language_details"],function(i){
                                    $.each(form["language_details"][i]["question_details"],function(j){
                                        form["language_details"][i]["question_details"][j]["img_details"]=null;
                                    });
                        });
                        var jsonobj=JSON.stringify(form);
                        $.ajax({
                            url: config.getU()+"/InsMockTestHomeDetailsSvr",
                            type: 'POST',
                            data: jsonobj,
                            success: function(data)
                            {
                                //loadResponse(data) ;
                                that.insLoadFinish();
                                //$("#topic_form")[0].reset();
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFinish();
                                //$("#topic_form")[0].reset();
                                that.insLoadFail();
                            }
                        });
                }
                
                this.updMockTestHome =function (form){
                    item = {}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="subcategory_id"){
                                item ["subcategory_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="category_id"){
                                item ["category_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="section_id"){
                                item ["section_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="topic_id"){
                                item ["topic_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="topic_name"){
                                item ["topic_name"]= $(this).val();
                            }
                        });
                        updResponse(item);
                        that.updLoadBegin();
                    $.ajax({
                        type: "POST",
                        url: "/UpdMockTestHomeDetailsSvr",
                        data: form.serialize(), // serializes the form's elements.
                        success: function(data)
                        {
                            that.updLoadFinish();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.updLoadFinish();
                            that.updLoadFail();
                        }
                    });
                }
		
		/**
		 * load lots of data from the server
		 * or return data from cache if it's already
		 * loaded
		 */
                
		fthis.selMockTestHome = function(){
                    var user_id=config.getModel("ProfileConfig").getUser_id();
                    var outCache = toArray(1);
			if(outCache.length) return outCache;
    			that.selLoadBegin();
			$.ajax({
                                url:config.getU()+ "/SelMockTestHomeSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'user_id':user_id},
				error: function(){
                                    that.selLoadFinish();
                                    that.selLoadFail();
				},
				success: function(data){
                                    that.selLoadFinish();
                                    if(data!="")
                                    loadResponse(user_id,data) ;
				}
			});
		}
                
                this.delMockTestHome = function(category_id,subcategory_id,section_id,topic_id){
                        delResponse(category_id,subcategory_id,section_id,topic_id);
                	that.delLoadBegin();
			$.ajax({
				url: config.getU()+"/DelMockTestHomeDetailsSvr",
				type: 'GET',
                                data : {id : topic_id },
				error: function(){
					that.delLoadFail();
				},
				success: function(data){
                                    if(data=="ok"){
                                        that.delLoadFinish();
                                    }
				}
			});
		}
                
		/**
		 * load lots of data from the server
		 */
		this.clearAll = function(){
			cache = new Array();
		}
		/**
		 * add a listener to this model
		 */
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                this.selLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin();
			});
		}
                this.delLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadBegin();
			});
		}
                this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadBegin();
			});
		}

		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                this.selLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish();
			});
		}
                this.delLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFinish();
			});
		}
                this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                
		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
                
                this.selLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadFail();
			});
		}
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
                
		/**
		 * tell everyone the item we've loaded
		 */
                
		this.loadItem = function(key,item){
			$.each(listeners, function(i){
				listeners[i].loadItem(key,item);
			});
		}
                
                this.updItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].updLoadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	MockTestHomeListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        updLoadItem: function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
