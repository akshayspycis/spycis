jQuery.extend({
	Config: function(){
            //......................................................................................................................................
		var that = this;
		var model = new Array();
                var view = new Array();
                var controller = new Array();
                var language_id;
                var question_id ;
                var div_box ;
                var instruncaition=1;
                var test_id;
                var test_time;
                var width=$(window).width();
                var height=$(window).height();
                var test_date;
                var active_time;
                var peer;
                var conn;
                
                that.getPeer=function (){
                    return peer;
                }
                that.setPeer=function (value){
                    peer=value;
                }
                that.getConn=function (){
                    return conn;
                }
                that.setConn=function (value){
                    peer=conn;
                }
                that.getWidth=function (){
                    return width;
                }
                that.getHeight=function (){
                    return height;
                }
                that.getTest_Date=function (){
                    return test_date;
                }
                that.setTest_Date=function (value){
                    test_date=value;
                }
                that.getActive_Time=function (){
                    return active_time;
                }
                that.setActive_Time=function (value){
                    active_time=value;
                }
                that.getTest_Time=function (){
                    return test_time;
                }
                that.setTest_Time=function (value){
                    test_time=value;
                }
                that.getIns=function (){
                    return instruncaition;
                }
                that.setIns=function (value){
                    instruncaition=value;
                }
                that.getTest_id=function (){
                    return test_id;
                }
                that.setTest_id=function (value){
                    test_id=value;
                }
                
                var load_js = $('<img />', { 
                    class: 'img-rounded',
                    src: '../../dist/images/loading_1.gif',
                    css:{'display':' block','margin-left':'auto','margin-right':'auto','margin-top':'auto','margin-bottom':'auto'}
                });
                
                var animation_infoprakr =$("<h1></h1>").addClass("loader")
                                            .append($("<span>S</span>"))
                                            .append($("<span>P</span>"))
                                            .append($("<span>Y</span>"))
                                            .append($("<span>C</span>"))
                                            .append($("<span>I</span>"))
                                            .append($("<span>S</span>"));
//                var animation_infoprakr =$("<h1></h1>").addClass("loader")
//                                            .append($("<span>V</span>"))
//                                            .append($("<span>I</span>"))
//                                            .append($("<span>B</span>"))
//                                            .append($("<span>R</span>"))
//                                            .append($("<span>A</span>"))
//                                            .append($("<span>N</span>"))
//                                            .append($("<span>T</span>"));
//                    
                
            //.............Contructor.........................................................................................................................                
                
                this.Config =function (){
                    $("body").empty();
                    var fontsize;
                    if(width>1100){
                        fontsize="150px";
                    }else if(width>991&&width<=1100){
                        fontsize="130px";
                    }else if(width>768&&width<=991){
                        fontsize="80px";
                    }else if(width>467&&width<=768){
                        fontsize="60px";
                    }else if(width>350&&width<=467){
                        fontsize="50px";
                    }else if(width>300&&width<=350){
                        fontsize="45px";
                    }else if(width>270&&width<=300){
                        fontsize="40px";
                    }else if(width>248&&width<=270){
                        fontsize="35px";
                    }else if(width>210&&width<=248){
                        fontsize="30px";
                    }else {
                        fontsize="25px";
                    }
                    animation_infoprakr.find("span" ).css({"font-size":fontsize});
                    that.startAnimationInfoaprk($("body"));
                    that.loadProfileConfig();
                } 
            //.......................Methods...............................................................................................................    
            
            //for load Profile Config js file
                this.loadProfileConfig=function (){
                    if(that.getModel("ProfileConfig")==null){
                        $.get("../model/Profileconfig.js").done(function() {
                        try {
                            that.setModel("ProfileConfig",new $.ProfileConfig(that));
                        }catch(e){
                                alert("Error:000 profile.js Loading Problem "+e);
                        }
                        }).fail(function() {alert("Error:000 profile.js  Loading Problem");}); 
                    }else{
                        that.finishAnimationInfoaprk($("body"))
                        that.setBox();
                        that.loadLoginBoxVgr();
                    }
                }
                
                this.loadLoginBoxVgr=function (obj){
                    if(that.getView("LoginBoxVgr")==null){
                        $.get("../view/LoginBoxVgr.js").done(function() {
                        try {
                            that.setView("LoginBoxVgr",new $.LoginBoxVgr(that));
                            that.finishAnimationInfoaprk($("body"))
                            that.setBox();
                            that.loginBox(obj);
                            that.setTitle("Login into Spycis");
                        }catch(e){
                                alert("LoginBoxVgr.js in "+e);
                        }
                        }).fail(function() {alert("Error:LoginBoxVgr.js  Loading Problem");}); 
                    }else{
                        if(obj!=null){
                        obj.removeLoading();}
                        that.loginBox(obj);
                    }
                }
                this.loginBox=function (obj){
                    $("body").find(".login-box").find(".register-box-body").remove();
                    $("body").find(".login-box").find(".login-box-body").remove();
                    div_box.append(that.getView("LoginBoxVgr").getLoginBox());
                    $.get("../../dist/js/jquery.validate.min.js").done(function() {
                        if(that.getModel("Login")==null){
                            $.getScript("../model/Login.js").done(function(){
                                    try {
                                        that.setModel("Login",new $.Login(that));
                                        that.getModel("Login").setValidation();
                                    }catch(e){}
                            }).fail(function() {alert("Error: registration.js Loading Problem");}); 
                        }else{
                            that.getModel("Login").setValidation();
                        }
                        }).fail(function() {alert("Error: jquery.validate.min.js Loading Problem");}); 
                }
                this.loadRegistraionBoxVgr=function (obj){
                    if(that.getView("RegistrationBoxVgr")==null){
                        $.get("../view/RegistrationBoxVgr.js").done(function() {
                            that.setView("RegistrationBoxVgr",new $.RegistrationBoxVgr(that));
                            that.RegistrationBox(obj);
                        }).fail(function() {alert("Error: RegistrationBoxVgr.js Loading Problem");}); 
                    }else{
                        that.RegistrationBox(obj);
                    }
                }
                this.RegistrationBox=function (obj){
                    obj.removeLoading(); 
                    $("body").find(".login-box").find(".login-box-body").remove();
                    $("body").find(".login-box").append(that.getView("RegistrationBoxVgr").getRegistrationBox());
                    $.get("../../dist/js/jquery.validate.min.js").done(function() {
                        if(that.getModel("Registration")==null){
                            $.getScript("../model/Registration.js").done(function(){
                                    try {
                                        that.setModel("Registration",new $.Registration(that));
                                        that.getModel("Registration").setValidation();
                                        that.getModel("Registration").setCaptcha($("#h1_img").parent());
                                    }catch(e){}
                            }).fail(function() {alert("Error: registration.js Loading Problem");}); 
                        }else{
                            that.getModel("Registration").setValidation();
                            that.getModel("Registration").setCaptcha($("#h1_img").parent());
                        }
                        }).fail(function() {alert("Error: jquery.validate.min.js Loading Problem");}); 
                }
                
                this.loadFPBox = function (obj){
                    if(that.getView("FPBoxVgr")==null){
                        $.get("../view/FPBoxVgr.js").done(function() {
                            that.setView("FPBoxVgr",new $.FPBoxVgr(that));
                            that.fbBox(obj);
                        }).fail(function() {alert("Error: FPBoxVgr.js Loading Problem");}); 
                    }else{
                        that.fbBox(obj);
                    }
                }
                
                this.fbBox = function (obj){
                    obj.removeLoading();
                    $("body").find(".login-box").find(".login-box-body").remove();
                    $("body").find(".login-box").find(".register-box-body").remove();
                    $("body").find(".login-box").append(that.getView("FPBoxVgr").getFPBox());
                    $.get("../../dist/js/jquery.validate.min.js").done(function() {
                        if(that.getModel("FPBox")==null){
                            $.getScript("../model/FPBox.js").done(function(){
                                    try {
                                        that.setModel("FPBox",new $.FPBox(that));
                                        that.getModel("FPBox").setValidation();
                                    }catch(e){}
                            }).fail(function() {alert("Error: FPBox.js Loading Problem");}); 
                        }else{
                            that.getModel("FPBox").setValidation();
                        }
                    }).fail(function() {alert("Error: jquery.validate.min.js Loading Problem");}); 
                }
                this.loadProfilePicBoxVgr=function (){
                    if(that.getView("ProfilePicBoxVgr")==null){
                        $.get("../view/ProfilePicBoxVgr.js").done(function() {
                        try {
                            that.setView("ProfilePicBoxVgr",new $.ProfilePicBoxVgr(that));
                            $("body").find("div:first").find("div:first").remove();
                            $("body").find("div:first").append(that.getView("ProfilePicBoxVgr").getProfilePicBox());
                            
                        }catch(e){
                                alert("ProfilePicBoxVgr.js in "+e);
                        }
                        }).fail(function() {alert("Error:ProfilePicBoxVgr.js  Loading Problem");}); 
                    }else{
                        div_box.append(that.getView("ProfilePicBoxVgr").getProfilePicBox());
                    }
                }
                
                
                
                this.setBox = function (){
                    $("body").addClass("login-page");
                    $("body").append(that.getBox());
                }       
                
                this.getBox = function (){
                    div_box=$("<div>").addClass("login-box");
//                   div_box.append($("<center>").addClass("login-logo span4").append(that.getBestLogo()));
                     div_box.append($("<center>").addClass("login-logo span4").append($("<h1>").append("SPYCIS")
                             .css({"font-family":"freedom","color":"#0d5995","font-size":"47px","font-weight":"bold","text-shadow":"rgba(255, 255, 255, 0.901961) 4px 2px 2px, rgba(0, 0, 0, 0.498039) 3px 3px 10px, rgba(255, 255, 255, 0.2) 0px 3px 4px, rgba(255, 255, 255, 0.45098) 0px 0px 20px"})));
                    return div_box;
                }
                
                
                
            //for load Main js file    
                this.loadMain=function (){
                    $.get("../model/Main.js").done(function() {
                        try {
                            that.setModel("Main",new $.Main());
                            that.loadMainVgr();
                    }catch(e){
                            alert("Error:001 Main.js Loading Problem "+e);
                    }
                    }).fail(function() {alert("Error:001 Main.js Loading Problem");}); 
                }
            //for load MainVgr js file    
                this.loadMainVgr=function (){
                    $.get("../view/MainVgr.js").done(function() {
                        try {
                            that.setView("MainVgr",new $.MainVgr(that));
                            that.loadMainMgr();
                    }catch(e){
                            alert("Error:002 Main.js Loading Problem "+e);
                    }
                    }).fail(function() {alert("Error:002 Main.js Loading Problem");}); 
                }
                
            //for load MainMgr js file    
                this.loadMainMgr=function (){
                    $.get("../controller/MainMgr.js").done(function() {
                        try {
                            that.setController("MainMgr",new $.MainMgr(that));
                            that.load();
                    }catch(e){
                            alert("Error:003 MainMgr.js Loading Problem "+e);
                    }
                    }).fail(function() {alert("Error:003 MainMgr.js Loading Problem");}); 
                }
             //for load body funtion
                this.load=function (){
                            that.getView("MainVgr").load();
                }
                
                
                
            //...............................for extra supporting method..............................    
                this.startLoadingJs =function (div){
                    var height = parseInt($( window ).height())/2-50;
                    height+="px";
                    div.css({'margin-right':'auto','margin-left':'auto','position':'relative','margin-top':height}).append(load_js);
                }
                
                this.finishLoadingJs =function (div){
                    div.css({'margin-right':'','margin-left':'','position':'','margin-top':''});;
                    div.empty();
                }
                
                this.startAnimationInfoaprk =function (div){
                    div.append(animation_infoprakr);
                }
                
                this.finishAnimationInfoaprk =function (div){
                    div.css({'margin-right':'','margin-left':'','position':'','margin-top':''});;
                    div.empty();
                }

                
                this.getLoadingFb=function (){
                    var fb = $('<img />', { 
                            class: 'img-responsive',
                            src: '../../dist/images/fb_ty.gif',
                            css:{'display':' block','margin-left':'15px','margin-right':'15px','margin-top':'auto','margin-bottom':'auto'}
                        });
                        return fb;
                }
                this.getLoading_1=function (){
                    return $("<div>").addClass("loader_1");
                }
                this.getLoading_2=function (){
                    return $("<div>").addClass("load-bar_2").append($('<div class="bar_2"></div><div class="bar_2"></div><div class="bar_2"></div>'));
                }
                this.getLoading_3=function (){
                    return $("<div>").addClass("loading_3").append($('<div class="loading-bar_3"></div><div class="loading-bar_3"></div><div class="loading-bar_3"></div><div class="loading-bar_3"></div><div class="loading-bar_3"></div>'));
                }
                this.getLoading_4=function (){
                    return $("<div>").addClass("signal_4");
                }
                this.getLoadingData  =function (){
                var load_data = $('<span />', { 
                    class: 'glyphicon glyphicon-refresh glyphicon-refresh-animate'
//                    css:{'display':' block','margin-left':'auto','margin-right':'auto','margin-top':'auto','margin-bottom':'auto'}
                });

                    return load_data;
                }
                this.getBestLogo =function (){
                    var bestlogo = $('<img />', { 
                        class: 'img-responsive',
                         src: '../../dist/images/bestlogo.png',
//                        src: '../../dist/images/log.png',
//                        css:{'height':'134px'}
                    });
                    return bestlogo;
                }
                
                this.setModel = function(key,value){
			model[key]=value;
		}
                
                this.setView = function(key,value){
			view[key]=value;
                        
                    $.each( view, function( key, value ) {
                        alert( key + ": " + value );
                    });

		}
                
                this.setController = function(key,value){
			controller[key]=value;
		}
                
                
                
                this.getModel = function(key){
			return model[key];
		}
                
                this.getView = function(key){
			return view[key];
		}
                
                this.getController = function(key){
			return controller[key];
		}
                this.setLanguage_id=function (language_id){
                    that.language_id=language_id;
                }
                
                this.getLanguage_id=function (){
                    return that.language_id;
                }
                
                this.setQuestion_id=function (id){
                    question_id=id;
                }
                
                this.getQuestion_id=function (){
                    return question_id;
                }
//                 var aaa_pp ="http://spycis.com/";
//                var aaa_pp ="http://spycis.com/sattvik";
                var aaa_pp ="http://localhost:8084/spycis/";
                this.getU = function (){
                    return aaa_pp;
                }
                this.setTitle=function (text){
                    console.log(text)
                    document.title =text;
                }
                
                
                this.c =function (){
                    return that;
                }
        },
	/**
	 * let people create listeners easily
	 */
	ConfigListener: function(list) {
		if(!list) list = {};
		return $.extend({
			setObjectValue : function() { },
			loadOneClicked : function() { },
			clearAllClicked : function() { }
		}, list);
	}
});
