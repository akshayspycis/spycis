jQuery.extend({
	AutoSuggestion: function(config){
		var cache = {};
		var cache_software_list= {};
		var cache_user_request_list= {};
		var that = this;
		var listeners = new Array();
                var check_test_submit=true;
                
		function toArray(term){
			var item = {};
                            try {
                                var value=null;;
                                $.each(cache,function (key,v){
                                    if(key.toUpperCase().startsWith(term.toUpperCase())){
                                       value= v;
                                       return value;
                                    }
                                });
                                return value;
                            }catch(e){return null;}
		}
		function toArray_status(software_id){
                    return cache_user_request_list[software_id];
		}
		
		function loadResponse(data,term){
                    cache[term]=data; 
                    $.each(data, function( key,value) {
                        cache_software_list[key]=value;
                        that.itemLoaded(value);
                    });
                    that.selLoadFinish();
		}
		function loadStatusResponse(data,software_id,obj){
                    cache_user_request_list[software_id]=data; 
                    that.itemLoaded_status(data,obj);
		}
                function updateCache_software_list(software_id){
                    cache_user_request_list[software_id]="true";
                }
		this.selAutoSuggestion = function(request){
                    that.selLoadFinish();
                    var outCache = toArray(request.term);
			if(outCache!=null) return outCache;
			$.ajax({
                                url: config.getU()+"/SelSoftwareDetails",
                                type: 'POST',
                                dataType: 'json',
                                data: {'term':request.term},
				error: function(){
//                                    that.selLoadFinish(module);
//                                    that.selLoadFail(module);
				},
				success: function(data){
                                    loadResponse(data,request.term)
				}
			});
		}
                
		this.selStatus= function(obj,software_id){
                        var outCache = toArray_status(software_id);
			if(outCache!=null) return outCache;
                        that.selLoadBegin_status(obj);
			$.ajax({
                                url: config.getU()+"/SelStatusDetailsSvr",
                                type: 'POST',
//                                dataType: 'json',
                                data: {'user_id':config.getModel("ProfileConfig").getUser_id(),'software_id':software_id}, // serializes the form's elements.
				error: function(){
                                    that.selLoadFail_status(obj);
				},
				success: function(data){
                                    that.selLoadFinish_status(obj);
                                    loadStatusResponse(data,software_id,obj)
				}
			});
		}
                
		this.sendRequest = function(obj,software_id){
                    that.selLoadBegin_In_grp(obj);
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/InsUserInSoftwareSvr",
                        data: {'user_id':config.getModel("ProfileConfig").getUser_id(),'software_id':software_id}, // serializes the form's elements.
                        success: function(data)
                        {
                            if(data.trim()=="true"){
                            that.selLoadFinish_In_grp(obj);
                            updateCache_software_list(software_id);
                            }else
                            that.selLoadFail_In_grp(obj);
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.selLoadFail_In_grp(obj);
                        }
                    });
		}
                
                this.getSectionName = function (question_id){
                    return section_details[submit_question[question_id]["section_id"]];
                }
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.selLoadFail = function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(obj);
			});
		}
                this.selLoadFail_In_grp = function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadFail_In_grp(obj);
			});
		}
                
                this.selLoadBegin_In_grp = function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin_In_grp(obj);
			});
		}
                this.selLoadFinish_In_grp = function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish_In_grp(obj);
			});
		}
                
                this.selLoadBegin_status= function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin_status(obj);
			});
		}
                this.selLoadFinish_status = function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish_status(obj);
			});
		}
                this.selLoadFail_status = function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadFail_status(obj);
			});
		}
                
		this.itemLoaded = function(value){
			$.each(listeners, function(i){
				listeners[i].loadItem(value);
			});
		}
		this.itemLoaded_status  = function(value,obj){
			$.each(listeners, function(i){
				listeners[i].loadItem_status(value,obj);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	AutoSuggestionListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { }
		}, list);
	}
});
