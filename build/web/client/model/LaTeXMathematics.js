jQuery.extend({
    LaTeXMathematics: function (config) {
        var preview = null;     // filled in by Init belo
        var that=this;
        this.Update = function (div) {
            this.preview =div[0];
            setTimeout(this.callback, 0);
        }
        this.CreatePreview = function () {
            MathJax.Hub.Queue(
                ["Typeset", MathJax.Hub, this.preview],
                ["PreviewDone", this]
            );
        }
        this.PreviewDone = function () {
//            this.maindiv.show()
        }
        var asdasd = setInterval(function (){
            if(MathJax){
                MathJax.Hub.Config({
                    showProcessingMessages: false,
                    tex2jax: { inlineMath: [['$','$'],['\\(','\\)']] }
                });    
                that.callback = MathJax.Callback(["CreatePreview",that]);
                that.callback.autoReset = true;  // make sure it can run more than once
                clearInterval(asdasd)
            }
        },50)
        
        
    }
});
