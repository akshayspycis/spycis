jQuery.extend({
	ContentWrapperAssesment: function(config){
		var cache = {};
		var that = this;
		var date ;
		var time ;
		var listeners = new Array();
		function toArray(category_id,subcategory_id){
			var item = [];
                            try {
                                    $.each(cache,function(j){
                                                top_item ;
                                                top_item = cache[j];
                                                top_item ["test_id"]= j;
                                                item.push(top_item);
                                    });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json` response from an
		 * ajax call
		 */
		function loadResponse(data){
                    $.each(data, function (personne,value) {
                            cache[personne]=value
                            value["test_id"]=personne;
                            that.testItemLoaded(value); 
                    });
		}
                this.getTestObj = function (test_id){
                    return cache[test_id];
                }
		
                this.createOrder =function (test_id){
                    return new Promise(function(resolve, reject) {
                         $.ajax({
                                url: config.getU()+"/CreateOrderSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'user_id':config.getModel("ProfileConfig").getUser_id(),'test_id':test_id},
				error: function(){
                                    reject(false);
				},
				success: function(data){
                                        resolve(data);
//                                    that.selLoadFinish();
//                                    if(data.test_details!="")
//                                    loadResponse(data.test_details) ;
				}
			});
                     });
                }
                this.checkPaymentStatus =function (test_id){
                    return new Promise(function(resolve, reject) {
                         $.ajax({
                                url: config.getU()+"/CheckPaymentStatusSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'user_id':config.getModel("ProfileConfig").getUser_id(),'test_id':test_id},
				error: function(){
                                    reject(false);
				},
				success: function(data){
                                        resolve(data);
				}
			});
                     });
                }
                
		this.selTest = function(){
                    var outCache = toArray();
			if(outCache.length) return outCache;
			that.selLoadBegin();
			$.ajax({
                                url: config.getU()+"/SelAssesmentDetails_TestSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'user_id':config.getModel("ProfileConfig").getUser_id()},
				error: function(){
                                    that.selLoadFinish();
                                    that.selLoadFail();
				},
				success: function(data){
                                    that.selLoadFinish();
                                    if(data.test_details!="")
                                    loadResponse(data.test_details) ;
				}
			});
		}
		this.getDateAndTime = function(test_id){
			that.selTestLoadBegin();
			$.ajax({
                                url: config.getU()+"/DateAndTimeSvr",
                                type: 'POST',
                                dataType: 'json',
				error: function(){
                                    that.selTestLoadFinish();
                                    that.selTestLoadFail();
				},
				success: function(data){
                                    that.selTestLoadFinish();
                                    var job_start_date = cache[test_id].e_date; // Oct 1, 2014
                                        var job_end_date = data.date; // Nov 1, 2014
                                        job_start_date = job_start_date.split('-');
                                        
                                        job_end_date = job_end_date.split('-');
                                        var new_start_date = new Date(job_start_date[2],job_start_date[0],job_start_date[1]);
                                        var new_end_date = new Date(job_end_date[2],job_end_date[0],job_end_date[1]);
                                        that.setTestSetting(test_id,data.date,data.time);
//                                    if(new_end_date >new_start_date){
//                                        that.setTestSetting(test_id,data.date,data.time);
//                                    }else if(new_end_date ==new_start_date  && cache[test_id].start_time<=data.time){
//                                        that.setTestSetting(test_id,data.date,data.time);
//                                    }else{
//                                        that.selTestLoadFail();
//                                    }
				}
			});
		}
                this.setTestSetting = function (test_id,test_date,active_time){
                    that.setLocalStorage("active_time",new String(active_time));
                    config.setActive_Time(active_time);
                    that.setLocalStorage("test_date",test_date);
                    config.setTest_Date(test_date);
                    that.setLocalStorage("testasdsaidasdasdasdasdasd",test_id);
                    config.setTest_id(test_id);
                    config.getView("MainVgr").setInstruncation();
                }
                
                this.setLocalStorage = function (key,value){
                    if(typeof(Storage) !== "undefined") {
                        localStorage.setItem(key,encodeURIComponent(value));
                    } 
                }
                
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin();
			});
		}
                this.selLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish();
			});
		}
                this.selLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadFail();
			});
		}
                this.selTestLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].selTestLoadBegin();
			});
		}
                this.selTestLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].selTestLoadFinish();
			});
		}
                this.selTestLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].selTestLoadFail();
			});
		}
                
		this.testItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].loadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	ContentWrapperAssesmentListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        selTestLoadBegin: function() { },
                        selTestLoadFinish: function() { },
                        selTestLoadFail: function() { }
		}, list);
	}
});
