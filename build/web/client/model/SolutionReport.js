jQuery.extend({
	SolutionReport: function(config){
		var cache = {};
		var submit_question = {};
		var keyset ;
		var section_details ;
		var keyset_section;
		var that = this;
		var listeners = new Array();
                var section_count={};
                var check_test_submit=true;
                
		function toArray(test_id){
			var item = [];
                            try {
                                if(cache[test_id]!=undefined)
                                item.push(cache[test_id]);
                            }catch(e){}
			return item;
		}
		/**
		 * load a json response from an
		 * ajax call
		 */
		function loadResponse(data,test_id,lang_id,module){
                    that.selLoadFinish();
                    cache[test_id]=data; 
                    keyset=data.keyset;
                    $.map(keyset, function( val, i) {
                        that.questionItemLoaded(cache[test_id]["q"][lang_id],val,module);
                    });
		}
                
		this.selSolutionReport = function(test_id,lang_id,module){
                    var outCache = toArray(test_id);
			if(outCache.length) return outCache;
                        that.selLoadBegin();
			$.ajax({
                                url: config.getU()+"/SelQuestionDetailsSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'test_id':test_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish(module);
                                    loadResponse(data,test_id,lang_id,module) ;
				}
			});
		}
                
                this.getSectionName = function (question_id){
                    return section_details[submit_question[question_id]["section_id"]];
                }
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                
		this.questionItemLoaded = function(item,key,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,key,module);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	SolutionReportListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { }
		}, list);
	}
});
