jQuery.extend({
	UpdPassword: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
                
                var password =false;
                var newpassword =false;
                var confirm_password=true;
                //.................................
                
                this.setPassword =function (value){
                    password=value;
                }
                this.getPassword =function (){
                    return password;
                }
                this.setNewPassword =function (value){
                    password=value;
                }
                this.getNewPassword =function (){
                    return password;
                }
                this.setConfirmPassword =function (value){
                    password=value;
                }
                this.getConfirmPassword =function (){
                    return password;
                }
                this.checkSqlPassword =function (){
                    setTimeout(function (){
                                if(that.getPassword()){
                                    obj={}
                                    obj['url']=config.getU()+"/SqlInjection_PasswordSvr";
                                    obj['value']=$("#password").val();
                                    obj['set']=that.setPasswordResponse;
                                    obj['element']=$("#password");
                                    that.checkSqlValidation(obj);
                                }
                            },100);
                }
                this.setPasswordResponse = function (data,element){
                    var a1;
                    $.each(data, function(key,value) {
                        if(key=="1"){
                            a1=value;
                        }
                    });
                       if(!a1){
                           that.setPassword(false);   
                           element.parent().removeClass("has-success");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-warning");
                           $("#error_password").empty();
                           $("#error_password").append("Your Password contain sql injection charector.");    
                           $("#error_password").css({'display':'block','color':'orange'});    
                        }else{
                           that.setPassword(true);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-success");    
                           $("#error_password").empty();
                        }
                        if(element.attr('id')=="password"){
                            $.ajax({
                                url: config.getU()+"/CheckPasswardSvr",
                                type: 'POST',
                                data: {'data':element.val(),'user_id':config.getModel("ProfileConfig").getUser_id()},
				success: function(data){
                                    if(data.trim()=="true"){
                                           that.setPassword(true);
                                           element.parent().removeClass("has-warning");    
                                           element.parent().removeClass("has-error");    
                                           element.parent().addClass("has-success");    
                                           $("#error_password").empty();
                                    }else{
                                       that.setPassword(false);   
                                       element.parent().removeClass("has-success");    
                                       element.parent().removeClass("has-error");    
                                       element.parent().addClass("has-warning");
                                       $("#error_password").empty();
                                       $("#error_password").append("Your Old Password Doesn't Match our DataBase. Please Provide Actual Password.");    
                                       $("#error_password").css({'display':'block','color':'orange'});    
                                    }
				}
                            });
                            
                        }
                }
                
                this.setValidation = function (){
                        $("#password").focusout(function (){
                            that.checkSqlPassword();
                        });
                        $("#newpassword").focusout(function (){
                            that.checkSqlPassword();
                        });
                    $.validator.addMethod("password_sql", function(value, element) {
                         return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
                                   && /[a-z]/.test(value) // has a lowercase letter
                                   && /\d/.test(value) // has a digit
                    }, "The password should meet some minimum requirements:<br>"+
                        "one no space between character<br>"+
                        "at least one lower-case character<br>"+
                        "at least one digit");
                    config.getView("ContentWrapperProfileVgr").getPassword_form().validate({
                        rules: {
                            password: {
                                required: true,
                                minlength: 6,
                                maxlength: 50,
                                password_sql:true
                            },
                            newpassword: {
                                required: true,
                                minlength: 6,
                                maxlength: 50,
                                password_sql:true
                            },
                            confirm_password: {
                              required: true,
                              maxlength: 50,
                              minlength: 6,
                              equalTo: "#newpassword"
                            },
                        
                        agree: "required"
                        },
                        messages: {
                        password: {
                            required: "Please provide Old password",
                            maxlength:"Your Old password must consist of max length 50 characters",
                            minlength: "Your Old password must be at least 5 characters long"
                        },
                        newpassword: {
                            required: "Please provide a password",
                            maxlength:"Your password must consist of max length 50 characters",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        confirm_password: {
                            required: "Please provide a confirm password",
                            minlength: "Your password must be at least 5 characters long",
                            maxlength:"Your password must consist of max length 50 characters",
                            equalTo: "Please enter the same password as above"
                        },
                        
                    },
                        errorElement : 'label',
                        errorLabelContainer: '#error_password',
                        highlight: function(element, errorClass) {
                            $("#error_password").empty();
                           $(element).parent().removeClass("has-warning");    
                           $(element).parent().removeClass("has-success");    
                           $(element).parent().addClass("has-error");    
                           switch($(element).attr('id')){
                               case "password":
                               that.setPassword(false);
                               case "newpassword":
                               that.setNewPassword(false);
                               case "confirm_password":
                               that.setConfirmPassword(false);
                           }
                        },
                        unhighlight: function(element, errorClass) {
                           $(element).parent().removeClass("has-warning");    
                           $(element).parent().removeClass("has-error");    
                           $(element).parent().addClass("has-success");    
                           switch($(element).attr('id')){
                               case "password":
                               that.setPassword(true);
                               case "newpassword":
                               that.setNewPassword(true);
                               case "confirm_password":
                               that.setConfirmPassword(true);
                           }
                        }, 
                        success: function(element) {
                           
                        }
                    });
                    if(config.getController("UpdPasswordMgr")==null){
                        $.getScript("../controller/UpdPasswordMgr.js").done(function() {
                                    try {
                                        config.setController("UpdPasswordMgr",new $.UpdPasswordMgr(config,config.getModel("UpdPassword"),config.getView("ContentWrapperProfileVgr")));
                                    }catch(e){
                                        alert(e);
                                    }
                        }).fail(function() {alert("Error :006 problem in registrationMgr.js file ");}); 
                    }
                    
                }
                
		this.checkSqlValidation = function (obj){
			$.ajax({
                                url: obj.url,
                                type: 'POST',
                                data: {'data':obj.value},
				success: function(data){
                                    obj.set(JSON.parse(data),obj.element);
				}
			});
                }
                
                this.updUpdPassword = function (form){
                    if(that.checkValidation()){
                        user_details ={}
                        user_details["user_id"]= config.getModel("ProfileConfig").getUser_id();
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="newpassword"){
                                user_details["password"]= $(this).val();
                            }
                        });
                        that.updPassword(JSON.stringify(user_details));
                    }
                }
                
                this.updPassword =function (user_details){
                    that.updPassLoadBegin();
                        $.ajax({
                            url: config.getU()+"/UpdPasswordSvr",
                            type: 'POST',
                            data: user_details,
                            success: function(data)
                            {
                                if(data.trim()=="false"){
                                    that.updPassLoadFail();
                                }else{
                                    that.updPassLoadFinish();
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.updPassLoadFail();
                            }
                        });
                }
                
                this.checkValidation = function (){
                    if(!that.getPassword()){
                        that.showError("Please provide a Currect Old password.")
                        return false;
                    }
                    if(!that.getNewPassword()){
                        that.showError("Please provide a password.")
                        return false;
                    }
                    return true;
                }
                
                this.showError = function (error){
                   $("#error_password").empty();
                   $("#error_password").append(error);    
                   $("#error_password").css({'display':'block','color':'red'});    
                }
                
		/**
		 * add a listener to this model
		 */
                
		this.addListener = function(list){
			listeners.push(list);
		}
		
		this.updPassLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updPassLoadBegin();
			});
		}
                
		this.updPassLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updPassLoadFinish();
			});
		}
                
		this.updPassLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].updPassLoadFail();
			});
		}
                
                
		/**
		 * tell everyone the item we've loaded
		 */
	
	},
	/**
	 * let people create listeners easily
	 */
	UpdPasswordListener: function(list) {
		if(!list) list = {};
		return $.extend({
			updPassLoadBegin : function() { },
			updPassLoadFinish : function() { },
			updPassLoadFail : function() { }
		}, list);
	}
});
