jQuery.extend({
	MockTestBasicConfiguration: function(config){
		var cache = {};
		var cache_software_list= {};
		var that = this;
		var listeners = new Array();
                var check_test_submit=true;
                var cat_list={};
                var sub_list={};
                var sec_list={};
                this.getCatList = function (){
                    return cat_list;
                }
                this.getSubList = function (){
                    return sub_list;
                }
                this.getSecList = function (){
                    return sec_list;
                }
                this.setCatList = function (key){
                    cat_list[key]="true";
                }
                this.setSubList = function (key){
                    sub_list[key]="true";
                }
                
		function toArray(obj){
			var item = [];
                        try {
                                var object=null;
                                switch(obj.module)
                                {
                                    case 2:
                                        object=cache[obj.category_id]["subcategory_details"];
                                        break;
                                    case 3:
                                        object=cache[obj.category_id]["subcategory_details"][obj.subcategory_id]["section_details"];
                                        break;
                                    case 4:
                                        object=cache[obj.category_id]["subcategory_details"][obj.subcategory_id]["section_details"][obj.section_id]["topic_details"];
                                        break;
                                    case 1:
                                        object=cache;
                                        break;
                                }
                                if(object==null){
                                            return 0;
                                }
                                $.each(object,function (key,value){
                                    item.push(value);
                                });
                                return item;
                            }catch(e){
                                return 0;
                            }
		}
		
		function loadCategoryDetails(data){
                    $.each(data["category_details"], function( key,value) {
                        cache[value["category_id"]]=value;
                        that.itemCategoryDetails(value);
                    });
                    
		}
		function loadSubCategoryDetails(data,obj){
                    $.each(data["subcategory_details"], function( key,value) {
                        if(cache[obj.category_id]["subcategory_details"]==null)
                        cache[obj.category_id]["subcategory_details"]={};
                        cache[obj.category_id]["subcategory_details"][value["subcategory_id"]]=value;
                        value["category_id"]=obj.category_id;
                        that.itemSubCategoryDetails(value);
                    });
		}
		function loadSectionDetails(data,obj){
                    $.each(data["section_details"], function( key,value) {
                        if(cache[obj.category_id]["subcategory_details"][obj.subcategory_id]["section_details"]==null)
                            cache[obj.category_id]["subcategory_details"][obj.subcategory_id]["section_details"]={};
                            cache[obj.category_id]["subcategory_details"][obj.subcategory_id]["section_details"][value["section_id"]]=value;
                            value["category_id"]=obj.category_id;
                            value["subcategory_id"]=obj.subcategory_id;
                            that.itemSectionDetails(value);
                    });
		}
                function loadTopicDetails(data,obj){
                    $.each(data["topic_details"], function( key,value) {
                        if(cache[obj.category_id]["subcategory_details"][obj.subcategory_id]["section_details"][obj.section_id]["topic_details"]=null)
                            cache[obj.category_id]["subcategory_details"][obj.subcategory_id]["section_details"][obj.section_id]["topic_details"]={}
                        cache[obj.category_id]["subcategory_details"][obj.subcategory_id]["section_details"][obj.section_id]["topic_details"][value["topic_id"]]=value;
                        value["category_id"]=obj.category_id;
                        value["subcategory_id"]=obj.subcategory_id;
                        value["section"]=obj.section;
                        that.itemTopicDetails(value);
                    });
		}
                
		this.selCategoryDetails= function(obj){
                    that.selLoadBegin();
                    var outCache = toArray(obj);
			if(outCache.length) return outCache;
			$.ajax({
                                url: config.getU()+"/SelCategoryDetailsSvr",
                                type: 'POST',
                                dataType: 'json',
//                                data: {'term':""},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish();
                                    loadCategoryDetails(data)
				}
			});
		}
		this.selSubCategoryDetails= function(obj){
                    that.selLoadFinish();
                    that.selLoadBegin();
                    var outCache = toArray(obj);
			if(outCache.length) return outCache;
			$.ajax({
                                url: config.getU()+"/SelSubCategorySvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'category_id':obj.category_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish();
                                    loadSubCategoryDetails(data,obj)
				}
			});
		}
                
		this.selSectionDetails= function(obj){
                    that.selLoadFinish();
                    that.selLoadBegin();
                    var outCache = toArray(obj);
			if(outCache.length) return outCache;
			$.ajax({
                            url: config.getU()+"/SelSectionDetailsSvr",
                            type: 'POST',
                            dataType: 'json',
                            data: {'category_id':obj.category_id,'subcategory_id':obj.subcategory_id},
                            error: function(){
                                that.selLoadFinish();
                                that.selLoadFail();
                            },
                            success: function(data){
                                that.selLoadFinish();
                                loadSectionDetails(data,obj)
                            }
			});
		}
		this.selFavSection= function(obj){
                    if(cat_list[obj.category_id]==null)cat_list[obj.category_id]="true";
                    if(sub_list[obj.subcategory_id]==null)sub_list[obj.subcategory_id]="true";
                    sec_list[obj.section_id]="true";
                    return true;
                }
		this.insBasicConfiguration= function(software_id){
                    var obj = {};
                    obj['software_id']=software_id;
                    obj['category']=cat_list;
                    obj['subcategory']=sub_list;
                    obj['section']=sec_list;
                    var a=JSON.stringify(obj);
                    that.insLoadBegin();
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/InsBasicConfigurationSvr",
                        data: a,
                        success: function(data)
                        {
                            if(data.trim()=="true"){
                                that.insLoadFinish();
                            }else{
                                that.insLoadFail();
                            }
                        }
                    });
		}
                
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                this.insLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin(module);
			});
		}
                this.insLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish(module);
			});
		}
                this.insLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].insLoadFail(module);
			});
		}
                
		this.itemLoaded = function(value){
			$.each(listeners, function(i){
				listeners[i].loadItem(value);
			});
		}
		this.itemCategoryDetails = function(value){
			$.each(listeners, function(i){
				listeners[i].loadCategoryDetails(value);
			});
		}
		this.itemSubCategoryDetails = function(value){
			$.each(listeners, function(i){
				listeners[i].loadSubCategoryDetails(value);
			});
		}
		this.itemSectionDetails = function(value){
			$.each(listeners, function(i){
				listeners[i].loadSectionDetails(value);
			});
		}
		this.itemTopicDetails = function(value){
			$.each(listeners, function(i){
				listeners[i].loadTopicDetails(value);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	MockTestBasicConfigurationListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { }
		}, list);
	}
});
