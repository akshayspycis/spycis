jQuery.extend({
	Section: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
		function toArray(){
			var item = [];
                            try {
                                    $.each(cache,function(j){
                                                sec_item = {}
                                                sec_item ["section_id"] = cache.section_id;
                                                sec_item ["section_name"]= cache.section_name;
                                                item.push(sec_item);
                                    });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json response from an
		 * ajax call
		 */
		function loadResponse(data,module){
                    $.each(data, function(item){
				cache[data[item].section_id] = data[item].section_name;
				that.sectionItemLoaded(data[item],module);
                    });
                    
		}
                
		this.getSection = function(id,category_id,subcategory_id){
                            return cache[category_id][subcategory_id][id].section_name;
		}
                
		this.selSection = function(module){
                    var outCache = toArray();
			if(outCache.length) return outCache;
			that.selLoadBegin(module);
			$.ajax({
                                url: config.getU()+"/SelSectionDetails_TestSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish(module);
                                    if(data.section_details!="")
                                    loadResponse(data.section_details,module) ;
				}
			});
		}
                
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                
		this.sectionItemLoaded = function(item,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,module);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	SectionListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { }
		}, list);
	}
});
