jQuery.extend({
	ChatPanelMgr: function(config,model, view){
		var vlist = $.ChatPanelVgrListener({
			insChatPanel : function(form){
                            model.insChatPanel(form);
			},
			selChatPanel : function(){
                            var all = model.selChatPanel();
                            if(all){
                                $.each(all, function(item){
                                    view.addChatPanel(all[item]);
                                });
                            }
			},
                        getChatPanel : function(id){
                                return model.getChatPanel(id);
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.ChatPanelListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(item){
				view.addChatPanel(item);
			},
                        updLoadItem : function(item){
				view.updChatPanel(item);
			},
                        selLoadBegin:function (){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (){
                            view.selLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
