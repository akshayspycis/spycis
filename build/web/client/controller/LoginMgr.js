jQuery.extend({
	LoginMgr: function(config,model, view){
		var vlist = $.LoginBoxVgrListener({
			checkLogin : function(modelform){
				model.checkLogin(modelform);
			}
		});
		view.addListener(vlist);

            var mlist = $.LoginListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			}
		});
		model.addListener(mlist);
	}
});
