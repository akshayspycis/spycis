jQuery.extend({
	ContentWrapperAssesmentMgr: function(config,model,view){
		var vlist = $.ContentWrapperAssesmentVgrListener({
                    selTest : function(){
                            var all = model.selTest();
                            if(all){
                                $.each(all, function(item){
                                    view.addTest(all[item]);
                                });
                            }
                    },
                    getDateAndTime : function(test_id){
                            model.getDateAndTime(test_id);
                    }
		});
            view.addListener(vlist);
            var mlist = $.ContentWrapperAssesmentListener({
			loadItem : function(item){
                                view.addTest(item);
			},
                        selLoadBegin:function (){
                                    view.selLoadBegin();
                        },
                        selLoadFail :function (){
                                    view.selLoadFail();
                        },
                        selLoadFinish :function (){
                                    view.selLoadFinish();
                        },
                        selTestLoadBegin:function (){
                                    view.selTestLoadBegin();
                        },
                        selTestLoadFail :function (){
                                    view.selTestLoadFail();
                        },
                        selTestLoadFinish :function (){
                                    view.selTestLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
