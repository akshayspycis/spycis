jQuery.extend({
	AutoSuggestionMgr: function(config,model, view){
		var vlist = $.AutoSuggestionVgrListener({
			selAutoSuggestion : function(request, response){
                            var all = model.selAutoSuggestion(request, response);
                            if(all){
                                    view.selLoadBegin();
                                    $.each(all, function(key,value){
                                        view.addAutoSuggestion(value);
                                    });
                                    view.selLoadFinish();
                            }
			},
			selStatus : function(obj,software_id){
                            var all = model.selStatus(obj,software_id);
                            if(all){
                                    view.selLoadBegin_status(obj);
                                    view.selLoadFinish_status(obj);
                                    view.updStatus(all,obj);
                                    
                            }
			},
			sendRequest : function(obj,software_id){
                            model.sendRequest(obj,software_id);
			}
		});
		view.addListener(vlist);

            var mlist = $.AutoSuggestionListener({
			loadItem : function(item){
                            view.addAutoSuggestion(item);
			},
			loadItem_status : function(item,obj){
                            view.updStatus(item,obj);
			},
                        selLoadBegin:function (){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (){
                            view.selLoadFinish();
                        },
                        selLoadBegin_In_grp:function (obj){
                            view.selLoadBegin_In_grp(obj);
                        },
                        setLoadFail_In_grp :function (obj){
                            view.setLoadFail_In_grp(obj);
                        },
                        selLoadFinish_In_grp :function (obj){
                            view.selLoadFinish_In_grp(obj);
                        },
                        selLoadBegin_status:function (obj){
                            view.selLoadBegin_status(obj);
                        },
                        setLoadFail_status :function (obj){
                            view.setLoadFail_status(obj);
                        },
                        selLoadFinish_status:function (obj){
                            view.selLoadFinish_status(obj);
                        }
		});
		model.addListener(mlist);
	}
});
