jQuery.extend({
	PerformanaceMgr: function(config,model,view){
		var vlist = $.PerformanaceVgrListener({
                    selTest : function(){
                            var all = model.selTest();
                            if(all){
                                $.each(all, function(item){
                                    view.addTest(all[item]);
                                });
                            }
                    }
		});
            view.addListener(vlist);
            var mlist = $.PerformanaceListener({
			loadItem : function(item){
                                view.addTest(item);
			},
                        selLoadBegin:function (){
                                    view.selLoadBegin();
                        },
                        selLoadFail :function (){
                                    view.selLoadFail();
                        },
                        selLoadFinish :function (){
                                    view.selLoadFinish();
                        },
                        selTestLoadBegin:function (){
                                    view.selTestLoadBegin();
                        },
                        selTestLoadFail :function (){
                                    view.selTestLoadFail();
                        },
                        selTestLoadFinish :function (){
                                    view.selTestLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
