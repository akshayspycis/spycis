jQuery.extend({
            MocktestPublicCircleMgr: function(config,model, view){
		var vlist = $.MocktestPublicCircleVgrListener({
			selMocktestPublicCircle: function(software_id,module){
                            var all = model.selMocktestPublicCircle(software_id,module);
                            if(all){
                                    view.selLoadBegin();
                                    $.each(all, function(key,value){
                                        view.addMocktestPublicCircle(value);
                                    });
                                    view.selLoadFinish();
                            }
			},
			sendRequest : function(obj,software_id){
                            model.sendRequest(obj,software_id);
			},
			acceptRequest : function(obj,software_id,user_id,user_request_id){
                            model.acceptRequest (obj,software_id,user_id,user_request_id);
			},
			addExamCircle : function(obj,software_id,user_id){
                            model.addExamCircle(obj,software_id,user_id);
			},
		});
		view.addListener(vlist);

            var mlist = $.MocktestPublicCircleListener({
			loadItem : function(item){
                            view.addMocktestPublicCircle(item);
			},
                        selLoadBegin:function (){
                            view.selLoadBegin();
                        },
                        selLoadFail:function (){
                            view.selLoadFail();
                        },
                        selLoadFinish:function (){
                            view.selLoadFinish();
                        },
                        selLoadBegin_accept:function (obj){
                            view.selLoadBegin_accept(obj);
                        },
                        selLoadFail_accept:function (obj){
                            view.selLoadFail_accept(obj);
                        },
                        selLoadFinish_accept:function (obj){
                            view.selLoadFinish_accept(obj);
                        }
		});
		model.addListener(mlist);
	}
});
