jQuery.extend({
	QuestionMgr: function(config,model,view){
		var vlist = $.QuestionVgrListener({
                    selQuestion : function(){
                            var all = model.selQuestion();
                            if(all){
                                $.each(all, function(item){
                                    view.addQuestion(all[item]);
                                });
                            }
                    }
                   
		});
            view.addListener(vlist);
            var mlist = $.QuestionListener({
			loadItem : function(item,key,module){
                            switch(module){
                                case 1:
                                    config.getView("TopicVgr").secAddInTopic(item);
                                    break;
                                case 2:
                                    config.getView("TopicVgr").secAddInTopicModel(item);
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").secAddInDirection(item);
                                    break;
                                default:
                                    view.addQuestion(item,key);
                                    break;
                            }
			},
                        selLoadBegin:function (module){
                            config.getView("ContentWrapperTestVgr").setDirection_Loading();
                            config.getView("ContentWrapperTestVgr").setQuestion_Loading();
                            config.getView("ContentWrapperTestVgr").setOption_Loading();
                            config.getView("ControlSidebarVgr").setQuestionButton_loading_begin();
                        },
                        selLoadFail :function (module){
                            
                        },
                        selLoadFinish :function (module){
                            config.getView("ControlSidebarVgr").setQuestionButton_loading_finish();
                        }
		});
		model.addListener(mlist);
	}
});
