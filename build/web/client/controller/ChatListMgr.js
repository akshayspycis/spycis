jQuery.extend({
	ChatListMgr: function(config,model,view){
		var vlist = $.ChatListVgrListener({
                    selChatList : function(test_id){
                            var all = model.selChatList(test_id);
                            if(all){
                                $.each(all, function(item){
                                    var keyset=all[item].keyset;
                                    $.map(keyset, function( val, i) {
                                         view.addChatList(all[item]["q"][val],val);
                                    });
                                });
                            }
                    }
                   
		});
            view.addListener(vlist);
            var mlist = $.ChatListListener({
			loadItem : function(item,key,module){
                            view.addChatList(item,key);
			},
                        selLoadBegin:function (module){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (module){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (module){
                            view.selLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
