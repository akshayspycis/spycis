    jQuery.extend({
	ProfileMgr: function(config){
                        var that=this;
			this.loadHeaderProfileVgr = function(){
                            if(config.getView("HeaderProfileVgr")==null){
                                $.getScript("../view/HeaderProfileVgr.js").done(function(){
                                    try {
                                        config.setView("HeaderProfileVgr",new $.HeaderProfileVgr(config));
                                        that.loadMainSidebarProfileVgr();
                                    }catch(e){
                                        console.log("Error= problem in HeaderProfileVgr.js file "+e);
                                    }
                                }).fail(function() {console.log("Error=004 problem in HeaderProfileVgr.js file ");}); 
                            }else{
                                that.loadMainSidebarProfileVgr();
                            }
			}
			this.loadMainSidebarProfileVgr = function(){
                            if(config.getView("MainSidebarProfileVgr")==null){
                                $.get("../view/MainSidebarProfileVgr.js").done(function() {
                                    try {
                                        config.setView("MainSidebarProfileVgr",new $.MainSidebarProfileVgr(config));
                                        that.loadContentWrapperProfileVgr();
                                    }catch(e){
                                        console.log("Error=MainSidebarProfileVgr.js"+e);
                                    }
                                }).fail(function() {console.log("Error=002 MainSidebarProfileVgr.js Loading Problem");}); 
                            }else{that.loadContentWrapperProfileVgr();
                            }
			}
                        this.loadContentWrapperProfileVgr = function(){
                            if(config.getView("ContentWrapperProfileVgr")==null){
                                $.get("../view/ContentWrapperProfileVgr.js").done(function() {
                                    try {
                                        config.setView("ContentWrapperProfileVgr",new $.ContentWrapperProfileVgr(config,config.getView("ContentWrapperVgr")));
                                        that.loadFooterProfileVgr();
                                    }catch(e){
                                        console.log("Error=ContentWrapperProfileVgr.js"+e);
                                    }
                                }).fail(function() {console.log("Error=ContentWrapperProfileVgr.js Loading Problem");}); 
                            }else{
                                that.loadFooterProfileVgr();
                            }
			}
                        this.loadContentWrapperAssesmentVgr = function(text){
                            that.setLoading();
                                    if(config.getView("ContentWrapperAssesmentVgr")==null){
                                        $.getScript("../view/ContentWrapperAssesmentVgr.js").done(function() {
                                        config.setView("ContentWrapperAssesmentVgr",new $.ContentWrapperAssesmentVgr(config,config.getView("ContentWrapperVgr")));
                                        config.getView("ContentWrapperAssesmentVgr").init();
                                        config.getView("ContentWrapperAssesmentVgr").selectTab(text);
                                    }).fail(function() {console.log("Error:problem in ContentWrapperAssesmentVgr.js file ");}); 
                                    }else{
                                        config.getView("ContentWrapperAssesmentVgr").init();
                                        config.getView("ContentWrapperAssesmentVgr").selectTab(text);
                                    }          
                            
			}
                        this.loadPerformanaceVgr = function(){
                            that.setLoading();
                            if(config.getView("PerformanaceVgr")==null){
                                $.getScript("../view/PerformanaceVgr.js").done(function() {
                                config.setView("PerformanaceVgr",new $.PerformanaceVgr(config,config.getView("ContentWrapperVgr")));
                                config.getView("PerformanaceVgr").init();
                            }).fail(function() {console.log("Error:problem in PerformanaceVgr.js file ");}); 
                            }else{
                                config.getView("PerformanaceVgr").init();
                            }              
			}
                        this.loadFooterProfileVgr = function(){
                            if(config.getView("FooterProfileVgr")==null){
                                $.get("../view/FooterProfileVgr.js").done(function() {
                                    try {
                                        config.setView("FooterProfileVgr",new $.FooterProfileVgr(config,config.getView("FooterVgr")));
                                        that.setBody();
                                    }catch(e){
                                        console.log("Error=FooterProfileVgr.js"+e);
                                    }
                                }).fail(function() {console.log("Error=002 FooterProfileVgr.js Loading Problem");}); 
                            }else{
                                that.setBody();
                            }
			}
                        this.loadChatlistVgr = function(obj){
                            return false;
                              if(config.getView("ChatListVgr")==null){
                                $.getScript("../view/ChatListVgr.js").done(function(){
                                    config.setView("ChatListVgr",new $.ChatListVgr(config));
                                        $.getScript("../model/ChatList.js").done(function() {
                                            config.setModel("ChatList",new $.ChatList(config));
                                            $.getScript("../controller/ChatListMgr.js").done(function() {
                                                $("body").append(config.getView("ChatListVgr").getChatList(obj));
                                                $("body").removeClass("sidebar-collapse skin-blue sidebar-mini");
                                                $("body").addClass("sidebar-collapse skin-blue sidebar-mini control-sidebar-open");
                                            }).fail(function() {console.log("Error :problem in TestUserListMgr.js file ");}); 
                                        }).fail(function() {console.log("Error:problem in TestUserListvgr.js file ");}); 
                                }).fail(function() {console.log("Error:004 problem in ChatListVgr.js file ");}); 
                             }else{
                                  $("body").append(config.getView("ChatListVgr").getChatList(obj));
                                  $("body").removeClass("sidebar-collapse skin-blue sidebar-mini");
                                  $("body").addClass("sidebar-collapse skin-blue sidebar-mini control-sidebar-open");
                             }
			}
                        
                        this.loadChatPanelVgr = function(obj){
                              if(config.getView("ChatPanelVgr")==null){
                                $.getScript("../view/ChatPanelVgr.js").done(function(){
                                    config.setView("ChatPanelVgr",new $.ChatPanelVgr(config));
                                        $.getScript("../model/ChatPanel.js").done(function() {
                                            config.setModel("ChatPanel",new $.ChatPanel(config));
                                                $.getScript("../controller/ChatPanelMgr.js").done(function() {
                                                config.setController("ChatPanelMgr",new $.ChatPanelMgr(config,config.getModel("ChatPanel"),config.getView("ChatPanelVgr")));
                                                that.callChatPanel(obj);
                                            }).fail(function() {console.log("Error :problem in ChatPanelMgr.js file ");}); 
                                        }).fail(function() {console.log("Error:problem in ChatPanel.js file ");}); 
                                }).fail(function() {console.log("Error:004 problem in ChatPanelVgr.js file ");}); 
                             }else{
                                  that.callChatPanel(obj);
                             }
			}
                        this.callChatPanel = function (obj){
                            var b=that.checkAvility(obj);
                            if(b!=null){
                                config.getView("ChatListVgr").cretaChatBox(b["chatbox"]);
                            }
                        } 
                        this.checkAvility= function (obj){
                            if(config.getModel("ChatList").getChatBoxObject(obj.user_id,"direct_chat_messages")==null){
                                return config.getView("ChatPanelVgr").getChatPanel(obj);
                            }else{
                                return null;
                            }
                        }
                        
                        this.setBody= function(){
                            config.getView("HeaderVgr").setList(config.getView("HeaderProfileVgr").getHeaderProfile());
//                            $.get("https://code.jquery.com/ui/1.12.1/jquery-ui.js").done(function() {
//                                $.getScript("../model/AutoSuggestion.js").done(function() {
//                                     config.setModel("AutoSuggestion",new $.AutoSuggestion(config));
//                                     $.getScript("../controller/AutoSuggestionMgr.js").done(function() {
//                                        config.setController("AutoSuggestionMgr",new $.AutoSuggestionMgr(config,config.getModel("AutoSuggestion"),config.getView("HeaderProfileVgr")));
//                                        config.getView("HeaderVgr").setBox(config.getView("HeaderProfileVgr").getSearchLargeBox());
//                                    }).fail(function() {console.log("Error: jquery.validate.min.js Loading Problem");}); 
//                                }).fail(function() {console.log("Error: jquery.validate.min.js Loading Problem");}); 
//                            }).fail(function() {console.log("Error: jquery/ui Loading Problem");}); 
                            config.getView("MainSidebarProfileVgr").setMainSidebarProfile();
                            config.getView("ContentWrapperVgr").hideTopBox();
                            config.getView("ContentWrapperProfileVgr").init()
                            config.getView("FooterProfileVgr").setFooter();
			}
                        this.setLoading = function (){
                            config.getView("ContentWrapperVgr").removeBoxHeader();
                            config.getView("ContentWrapperVgr").removeBoxbody();
                            config.getView("ContentWrapperVgr").removeBoxFooter();
                            config.getView("ContentWrapperVgr").setBoxBodyContent($("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin")));
                        }
                    }                           
});
