jQuery.extend({
	SectionMgr: function(config,view,model){
		var vlist = $.SectionVgrListener({
			selSection : function(){
                            var all = model.selSection();
                            if(all){
                                $.each(all, function(item){
                                    view.addSection(all[item]);
                                });
                            }
			}
		});
            view.addListener(vlist);
            var mlist = $.SectionListener({
			loadItem : function(item,module){
                            switch(module){
                                case 1:
                                    config.getView("TopicVgr").secAddInTopic(item);
                                    break;
                                case 2:
                                    config.getView("TopicVgr").secAddInTopicModel(item);
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").secAddInDirection(item);
                                    break;
                                default:
                                    view.addSection(item);
                                    break;
                            }
			},
                        selLoadBegin:function (module){
                            switch(module){
                                case 1:
                                    config.getView("TopicVgr").topicSecLoadBegin();
                                    break;    
                                case 2:
                                    config.getView("TopicVgr").topicModelSecLoadBegin();
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").directionSecLoadBegin();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                        },
                        selLoadFail :function (module){
                            switch(module){
                                 case 1:
                                    config.getView("SectionVgr").topicSecLoadFail();
                                    break;
                                 case 2:
                                    config.getView("SectionVgr").topicModelSecLoadFail();
                                    break;
                                 case 3:
                                    config.getView("DirectionVgr").directionSecLoadFail();
                                    break;
                                 default:
                                    view.selLoadFail();
                                    break;
                            }
                        },
                        selLoadFinish :function (module){
                            switch(module){
                                case 1:
                                    config.getView("TopicVgr").topicSecLoadFinish();
                                    break;
                                case 2:
                                    config.getView("TopicVgr").topicModelSecLoadFinish();
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").directionSecLoadFinish();
                                    break;
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                            view.selLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
