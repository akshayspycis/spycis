jQuery.extend({
	MockTestBasicConfigurationMgr: function(config,model, view){
            var obj_bf={};
		var vlist = $.MockTestBasicConfigurationVgrListener({
			selCategoryDetails : function(obj){
                            var all = model.selCategoryDetails(obj);
                            if(all){
                                view.selLoadFinish();
                                var sc=model.getSubList();
                                var b=false;
                                    $.each(all, function(key,value){
                                        if(model.getCatList()[value.category_id]==undefined){
                                            if(value["subcategory_details"]!=null){
                                                $.each(value["subcategory_details"], function(key,value){
                                                    if(sc[key]!=undefined && sc[key]){
                                                        b=false;    
                                                    }else{
                                                        b=true;
                                                        if(b)return false;
                                                    }
                                                });     
                                            }else{
                                                b=true;
                                            }
                                            if(b){
                                                view.addCategoryDetails(value);
                                            }
                                        }
                                        
                                    });
                            }
			},
			selSubCategoryDetails : function(obj){
                            var all = model.selSubCategoryDetails(obj);
                            if(all){
                                    view.selLoadFinish();
                                    var sc=model.getSecList();
                                    var b=false;
                                    $.each(all, function(key,value){
                                       if(model.getSubList()[value.subcategory_id]==undefined){
                                            if(value["section_details"]!=null){
                                                $.each(value["section_details"], function(key,value){
                                                    if(sc[key]!=undefined && sc[key]){
                                                        b=false;    
                                                    }else{
                                                        b=true;
                                                        if(b)return false;
                                                    }
                                                });     
                                            }else{
                                                b=true;
                                            }
                                            if(b){
                                                view.addSubCategoryDetails(value);
                                            }
                                        }
                                    });
                            }
			},
			selSectionDetails : function(obj){
                            var all = model.selSectionDetails(obj);
                            if(all){
                                    view.selLoadFinish();
                                    var sc=model.getSecList();
                                    $.each(all, function(key,value){
                                        if(!sc[value['section_id']]){
                                            view.addSectionDetails(value);
                                        }
                                    });
                            }
			},
			selFavSection : function(obj){
                            var all = model.selFavSection(obj);
			},
			insBasicConfiguration : function(software_id){
                            model.insBasicConfiguration(software_id);
			}
		});
		view.addListener(vlist);

            var mlist = $.MockTestBasicConfigurationListener({
			loadCategoryDetails : function(item){
                            view.addCategoryDetails(item);
			},
			loadSubCategoryDetails : function(item){
                            view.addSubCategoryDetails(item);
			},
			loadSectionDetails : function(item){
                            view.addSectionDetails(item);
			},
			loadTopicDetails : function(item){
                            view.addTopicDetails(item);
			},
                        selLoadBegin:function (){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (){
                            view.selLoadFinish();
                        },
                        insLoadBegin:function (){
                            view.insLoadBegin();
                        },
                        insLoadFail :function (){
                            view.insLoadFail();
                        },
                        insLoadFinish :function (){
                            view.insLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
