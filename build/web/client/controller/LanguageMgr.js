jQuery.extend({
	LanguageMgr: function(config,model,view){
		var vlist = $.LanguageVgrListener({
			selLanguage : function(module){
                            var all = model.selLanguage(module);
                            if(all){
                                $.each(all, function(item){
                                    view.addLanguage(all[item],module);
                                });
                            }
			}
		});
            view.addListener(vlist);
        }
});
