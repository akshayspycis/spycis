jQuery.extend({
	ProfilePicBoxVgr: function(config){
		var that = this;
		var listeners = new Array();
                var modelform;
                var model_footer;
                var loading_boolean=true;

                this.getProfilePicBox = function(title,model_form,call){
			var model =$("<div></div>").addClass("register-box-body");
                        model.append(that.getBoxHeader("Update Profile Picture"));
                        model.append(that.getMessage());
                        model.append(that.getUploadBoxBody());
                        model.append(that.getSubmitBoxBody());
                        if(config.getView("UploadImg")==null){
                            var obj = {
                            // Valid file formats
                            support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                            form: "update_img", // Form ID
                            dragArea: "dragAndDropFiles1", // Upload Area ID
                            multiUpload:"multiUpload1",
                            url:"/mocktest_v1//Img",
                            parent_obj:that,
                            strlenght:25,
                            module:1
                            }
                            
                            $.getScript("../model/UploadImg.js").done(function() {config.setModel("UploadImg",new $.UploadImg(obj,config));}).fail(function(e) {alert(e);}); 
                        }
                        return model;
		}

                this.getBoxHeader= function (header_title){
                    var model_header =$("<p></p>").addClass("login-box-msg").append(header_title);
                    return model_header;
                }
                
                this.getMessage= function (){
                    var center =$("<center>").addClass("form-group has-feedback").append('<a class="btn btn-default btn-sm" ><i class="fa fa-user"></i>&nbsp;</a><label>&nbsp;&nbsp;'+config.getModel("ProfileConfig").getFirstName()+',Upload Your Profile?</label>');
                    return center;
                }
                
                this.getUploadBoxBody = function (){
                    var form =$("<div>").addClass("row");
                    form.append($("<div>").addClass("col-sm-2 col-xs-1"));
                    form.append($("<div>").addClass("col-sm-8 col-xs-10").append($("<div>").addClass("box")
                                .append($("<div>").addClass("box-header")
                                        .append($("<div>").addClass("box-tools pull-right")
                                            .append($("<button></button>").addClass("btn btn-box-tool").attr({'data-toggle':'tooltip','title':'Remove'})
                                            .append($("<i>").addClass("fa fa-times"))).click(function (){
                                                config.getModel("UploadImg")._deleteFiles();
                                            })
                                            )
                                         )
                                .append($("<div>").addClass("box-body").append($("<form>").attr({'id':'update_img'}).append($("<div></div>").addClass("uploadArea").attr({'id':'dragAndDropFiles1'}).css({'min-height':'130px'})
                                    .append($('<center><i class="fa fa-user" style="font-size: 130px; padding-left:5px;"></i></center>')))
                                    .append($('<input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload1" style="width: 0px; height: 0px; overflow: hidden;">'))
                                 ))
                            ));
                    form.append($("<div>").addClass("col-sm-2 col-xs-1"));
                    
                    return form;
                }
                this.getSubmitBoxBody = function (){
                    var row = $("<div></div>").addClass("row")    
                    row.append($("<div></div>").addClass("col-xs-4 pull-left")
                            .append($("<button></button>").addClass("btn btn-default btn-block btn-flat").attr({'type':'submit','id':'submit','name':'submit'}).append("Skip")
                            .click(function (){
                                that.setloading($(this));
                                that.setDisableButton($(this));
                                config.loadMain();
                            }))
                            );
                    row.append($("<div></div>").addClass("col-xs-4 pull-right")
                            .append($("<button></button>").addClass("btn btn-primary btn-block btn-flat").attr({'type':'submit','id':'submit','name':'submit'}).append("Save")
                            .click(function (){
                                that.setloading($(this));
                                that.setDisableButton($(this));
                                that.insProfile()
                            }))
                            );
                    return row;
                }
                
                this.setloading =function (img){
                    img.empty();
                    img.append(config.getLoadingData());
                }
                
                this.removeLoading =function (img){
                    img.empty();
                    img.append("Skip");
                }
                
                this.setDisableButton =function (obj){
                    obj.prop('disabled', true);
                }
                
                this.setEnableButton =function (obj){
                    obj.prop('disabled', false);
                }
                
                this.setServerError =function (){
                    model_footer.find("#error_msg").append("Server doesn't Respond.");
                }
                
                this.removeServerError =function (){
                    model_footer.find("#error_msg").empty();
                }
                
                this.addListener = function(list){
                            listeners.push(list);
                }
                this.insProfile =function (){
                    config.getModel("UploadImg")._startUpload();
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	ProfilePicBoxVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
