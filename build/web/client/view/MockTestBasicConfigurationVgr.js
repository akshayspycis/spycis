jQuery.extend({
	MockTestBasicConfigurationVgr: function(config,model){
		var that = this;
		var listeners = new Array();
                var software_about_box_body;
                var software_about_box_body_tab_pane;
                var nav_tab_list;
                var error;
                var form ;
                var contact_no_chack=false ;
                var software_id;
                var software_basic_cofiguration;
                var module=5;
                var software_module_id;
                var next;
                var skip;
                var software_contact_details;
                var ul_list_favsection;
                var back_btn;
                var forward_btn;
                var temp_category_id;
                var b_fav_topic;
                
                this.getMockTestBasicConfiguration = function(user_id,software_module_id){
                    software_id=config.getModel("MockTestHome").getValue("1")['software_id'];
                    software_basic_cofiguration=config.getModel("MockTestHome").getValue("software_basic_cofiguration")
                    $.each(software_basic_cofiguration,function (k,v){
                        software_basic_cofiguration[k]={};
                        software_basic_cofiguration[k]["status"]=Boolean(v=="true");
                        if(k==1){
                            software_basic_cofiguration[1]["call_back"]=that.setAltObj;
                            software_basic_cofiguration[1]["message"]="Contact No ";
                            software_basic_cofiguration[1]["action"]=that.setContactDetails;
                            if(module==5)module=1;
                        }
                        if(k==2){
                            software_basic_cofiguration[2]["call_back"]=that.setWebObj
                            software_basic_cofiguration[2]["action"]=that.setWebsiteDetails;
                            if(module==5)module=2;
                        }
                        if(k==3){
                            software_basic_cofiguration[3]["message"]="Address Details ";                    
                            software_basic_cofiguration[3]["call_back"]=that.setAddObj;
                            software_basic_cofiguration[3]["action"]=that.setAddressDetails;
                            if(module==5)module=3;
                        }
                        if(k==4){
                            software_basic_cofiguration[4]["message"]="Basic Information ";                    
                            software_basic_cofiguration[4]["call_back"]=that.setBasicObj;
                            software_basic_cofiguration[4]["action"]=that.setFavDetails;
                            if(module==5)module=4;
                        }
                    });
                    $("body").find("#hidden").find("#error_msg").remove();
                    var div=$("<div>");
                        div.append(that.getBoxbody());
                        if(module!=5)
                        software_basic_cofiguration[module]["action"]();
                        else
                        return null;    
                        $.fn.modal.Constructor.prototype.enforceFocus = function () {
                            modal_this = this
                            $(document).on('focusin.modal', function (e) {
                            if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                            // add whatever conditions you need here:
                            &&
                            !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
                            modal_this.$element.focus()
                            }
                            })
                        };
                        return div;
		}
                
                this.setContactDetails = function (){
                  software_about_box_body_tab_pane.empty();
                  nav_tab_list.empty();
                  nav_tab_list.append($("<li>").addClass("active").append($("<a>").attr({'href':'#about','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa fa-phone-square")).append("&nbsp;&nbsp;Do you want Add Alternative Contact Number ")));
                  error=$("<label>").attr({'id':'error'}).addClass("col-sm-12 control-label text-red")
                  form =$("<div>").addClass("form-group no-padding")
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-3 control-label").append("Organisation Contact&nbsp;&nbsp;").append($("<i>").addClass("fa  fa-question-circle").attr({'data-toggle':'tooltip','title':'This Contact for use OTP Autentication'})))
                                            .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'alt_contact_no','name':'alt_contact_no','placeholder':'Contact No..','maxlength':'10'})
                                                .focusout(function (){                   
                                                    try {
                                                        var con=parseInt($(this).val())
                                                        software_basic_cofiguration[module]["status"]=$(this).val().length==10; // has a digit
                                                    }catch(e){
                                                        software_basic_cofiguration[module]["status"]= false;
                                                    }
                                                    error.empty();
                                                       if(!software_basic_cofiguration[module]["status"]){
                                                           error.append("Your Contact must be at least 10 characters long");
                                                       }
                                                })
                            ))
                                              ))
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append(error)));
                    
                    software_about_box_body_tab_pane.append(form);
                }
                
                this.setWebsiteDetails = function (){
                  software_about_box_body_tab_pane.empty();
                  nav_tab_list.empty();
                  nav_tab_list.append($("<li>").addClass("active").append($("<a>").attr({'href':'#about','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa  fa-globe")).append("&nbsp;&nbsp;Do you want Add Website Url")));
                  error=$("<label>").attr({'id':'error'}).addClass("col-sm-12 control-label text-red")
                  form =$("<div>").addClass("form-group no-padding")
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-3 control-label").append("Url&nbsp;&nbsp;").append($("<i>").addClass("fa  fa-question-circle").attr({'data-toggle':'tooltip','title':'This Contact for use OTP Autentication'})))
                                            .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'website','name':'website','placeholder':'www.example.com'})
                                                .focusout(function (){                   
                                                        if(!is_valid_url($(this).val())){
                                                            error.empty();
                                                            error.append("Your Url is invalid.");
                                                        }
                                                    })
                                                ))
                                              ))
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append(error)));
                    software_about_box_body_tab_pane.append(form);
                }
                
                this.setAddressDetails= function (){
                  software_about_box_body_tab_pane.empty();
                  nav_tab_list.empty();
                  nav_tab_list.append($("<li>").addClass("active").append($("<a>").attr({'href':'#about','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa fa-building")).append("&nbsp;&nbsp;Please Provide Organisation Address")));
                  error=$("<label>").attr({'id':'error'}).addClass("col-sm-12 control-label text-red")
                  form =$("<div>").addClass("form-group no-padding")
                                  .append($("<div>").addClass("form-control-group col-lg-12 no-padding")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
//                                            .append($("<label>").addClass("control-label col-sm-12").append("Search Location&nbsp;&nbsp;"))
                                             .append($("<label>").addClass("col-sm-1 control-label").append($("<span>").addClass("badge bg-blue").css({'padding': '4px','padding-right': '6px'}).append($("<i>").addClass("fa fa-google fa-2x"))))
                                            .append($("<div>").addClass("col-sm-11")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'autocomplete_search','name':'autocomplete_search','placeholder':'Search Location....'})
                                                .focus(function() {                   
                                                        that.geolocate();
                                                    })
                                                )
                                              ))
                                        )
                                  .append($("<div>").addClass("form-control-group col-lg-6 no-padding")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                             .append($("<label>").addClass("col-sm-3 control-label").append("Address&nbsp;&nbsp;"))
                                             .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'street_number','name':'street_number','placeholder':'Address','disabled':'true'}))
                                                )
                                              )
                                        )
                                  .append($("<div>").addClass("form-control-group col-lg-6 no-padding")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-3 control-label").append("Street&nbsp;&nbsp;"))
                                            .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'route','name':'route','placeholder':'Street','disabled':'true'}))
                                                )
                                              )
                                        )
                                  .append($("<div>").addClass("form-control-group col-lg-6 no-padding")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-3 control-label").append("City&nbsp;*&nbsp;"))
                                            .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'locality','name':'locality','placeholder':'City','disabled':'true'}))
                                                )
                                              )
                                        )
                                  .append($("<div>").addClass("form-control-group col-lg-6 no-padding")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-3 control-label").append("State&nbsp;*&nbsp;"))
                                            .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'administrative_area_level_1','name':'administrative_area_level_1','placeholder':'State','disabled':'true'}))
                                                )
                                              )
                                        )
                                  .append($("<div>").addClass("form-control-group col-lg-6 no-padding")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-3 control-label").append("Zip Code&nbsp;*&nbsp;"))
                                            .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'postal_code','name':'postal_code','placeholder':'Zipcode','disabled':'true'}))
                                                )
                                              )
                                        )
                                  .append($("<div>").addClass("form-control-group col-lg-6 no-padding")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-3 control-label").append("Country&nbsp;*&nbsp;"))
                                            .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'country','name':'country','placeholder':'Country','disabled':'true'}))
                                                )
                                              )
                                        )
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append(error)));
                    software_about_box_body_tab_pane.append(form);
                    $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDrIntwmIH_YuOmQ6y1X0Zi7M7AA6aE3Sw&libraries=places&amp;").done(function() {
                        that.initAutocomplete();
                    })
                }
                
                this.setFavDetails= function (){
                  software_about_box_body_tab_pane.empty();
                  nav_tab_list.empty();
                  nav_tab_list.append($("<li>").addClass("active").append($("<a>").attr({'href':'#about','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa fa-fa-star")).append("&nbsp;&nbsp;Quick Search Configuration.")));
                  error=$("<label>").attr({'id':'error'}).addClass("col-sm-12 control-label text-red")
                  form =$("<div>").addClass("form-group no-padding")
                                  .append($("<div>").attr({'id':'basicconfig'}))
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append(error)));
                    software_about_box_body_tab_pane.append(form);
                    form.find("#basicconfig").empty();
                    form.find("#basicconfig").append($("<center>").css({'margin-top': '37px'}).append(config.getLoadingFb()))
                    forward_btn=$("<button>").addClass("btn pull-right").append($("<i>").addClass("fa fa-arrow-right ").css({'cursor': 'pointer'})).click(function (){
                        obj_bf.module=obj_bf.module+1;
                        that.loadMockTestBasicConfiguration(obj_bf.module,obj_bf);
                        that.setButton();
                    });
                    back_btn=$("<button>").addClass("btn").append($("<i>").addClass("fa fa-arrow-left")).click(function (){
                        if(obj_bf.module==4){
                            obj_bf.module=3
                        }
                        obj_bf.module=obj_bf.module-1;
                        that.loadMockTestBasicConfiguration(obj_bf.module,obj_bf);
                        that.setButton();
                    });
                    next.html("Save");
                    if(config.getModel("MockTestBasicConfiguration")==null){
                        $.getScript("../model/MockTestBasicConfiguration.js").done(function() {
                            config.setModel("MockTestBasicConfiguration",new $.MockTestBasicConfiguration(config));
                            $.getScript("../controller/MockTestBasicConfigurationMgr.js").done(function() {
                                ul_list_favsection=$("<ul>").addClass("list-unstyled ");
                                b_fav_topic=$("<b>");
                                form.find("#basicconfig").empty();
                                form.find("#basicconfig").append($("<div>").addClass("col-lg-1"))
                                form.find("#basicconfig").append($("<div>").addClass("form-control-group col-lg-10 no-padding")
                                        .append($("<div>").addClass("box ").css({'border-top': '1px solid #f4f4f4'})
                                            .append($("<div>").addClass("box-header with-border")
                                                .append($("<h3>").addClass("box-title pull-left").css({'color': '#5e5959'}).append(back_btn).css({'cursor': 'pointer'}).append("&nbsp;&nbsp;&nbsp;").append("Select Your ").append(b_fav_topic))
                                                .append(forward_btn)
                                                )
                                        .append($("<div>").addClass("box-body").append(ul_list_favsection)
                                        )))
                                config.setController("MockTestBasicConfigurationMgr",new $.MockTestBasicConfigurationMgr(config,config.getModel("MockTestBasicConfiguration"),config.getView("MockTestBasicConfigurationVgr")));
                                that.loadMockTestBasicConfiguration(1,{});
                            }).fail(function() {alert("Error :problem in MockTestBasicConfigurationMgr.js file ");}); 
                        }).fail(function() {alert("Error:problem in MockTestBasicConfiguration.js file ");}); 
                   }else{
                       that.loadMockTestBasicConfiguration(1,{});
                   }
                }
                
                this.getRow = function (module,obj){
                    var value;
                    switch (module){
                        case 2:
                            value=obj.category_name;
                            break;
                        case 3:
                            obj_bf["category_id"]=obj.category_id
                            value=obj.subcategory_name;
                            break;
                        case 4:
                            obj_bf["subcategory_id"]=obj.subcategory_id
                            value=obj.section_name;
                            break;
                    }
                    return $("<li>").addClass("box collapsed-box").fadeIn(1000).append($("<div>").addClass("box-header with-border").css({'background-color': 'rgb(222, 222, 222)' })
                        .append($("<h3>").addClass("box-title pull-left").append(value))
                        .append($("<div>").addClass("box-tools pull-right")
                            .append($("<button>").addClass("btn btn-default btn-sm dropdown-toggle")
                                    .click(function (){
                                        obj_bf["module"]=module;
                                        switch (module){
                                            case 2:
                                                if(temp_category_id==null){
                                                   temp_category_id =obj_bf["category_id"];
                                                }
                                                obj_bf["category_id"]=obj.category_id
                                                if(temp_category_id!=obj_bf["category_id"]){
                                                    temp_category_id=null;
                                                    obj_bf["subcategory_id"]=null;
                                                }
                                            break;
                                            case 3:
                                                obj_bf["subcategory_id"]=obj.subcategory_id
                                                break;
                                            case 4:
                                                obj_bf["section_id"]=obj.section_id
                                                if(!software_basic_cofiguration[4]["status"]){
                                                    software_basic_cofiguration[4]["status"]=true;
                                                }
                                                break;
                                        }
                                        
                                        $(this).find("i").addClass("text-blue");
                                        $(this).parent().parent().parent().fadeOut(1000).remove();
                                        that.loadMockTestBasicConfiguration(module,obj);
                                        that.setButton();
                                    })
                            .append($("<i>").addClass("fa fa-check")))
                         )).css({'position': 'relative','left': '0px', 'top': '0px','box-shadow': '0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,.12)'});
                                            
                } 
                
                this.selLoadBegin =function (){
                    ul_list_favsection.append($("<li>").addClass("box collapsed-box").append($("<div>").addClass("box-header with-border").css({'background-color': 'rgb(222, 222, 222)' })
                        .append($("<h3>").addClass("box-title pull-left").append(config.getLoadingData()))
                        .append($("<div>").addClass("box-tools pull-right")))
                        .css({'position': 'relative','left': '0px', 'top': '0px','box-shadow': '0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,.12)'})
                        );
                }
                
                this.selLoadFinish=function (){
                    ul_list_favsection.empty();
                }

                this.selLoadFail = function() { 
                    ul_list_favsection.append($("<li>").addClass("box collapsed-box").append($("<div>").addClass("box-header with-border").css({'background-color': 'rgb(222, 222, 222)' })
                        .append($("<h3>").addClass("box-title pull-left").append("Server Doesn't Responde."))
                        .append($("<div>").addClass("box-tools pull-right")))
                        .css({'position': 'relative','left': '0px', 'top': '0px','box-shadow': '0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,.12)'})
                        );
                }
                
                this.addCategoryDetails= function (data){
                    ul_list_favsection.append(that.getRow(2,data));
                }
                
                this.addSubCategoryDetails= function (data){
                    ul_list_favsection.append(that.getRow(3,data));
                }
                
                this.addSectionDetails= function (data){
                    ul_list_favsection.append(that.getRow(4,data));
                }
                
                var obj_bf={};
                
                this.checkList= function (){
                    if(ul_list_favsection.children('li').size()==0){
                        back_btn.trigger("click");
                    }
                }
                
                this.setButton = function (){
                    switch (obj_bf['module']){
                        case 1:
                            back_btn.prop('disabled', true).find("i").css({'color':'#f4f4f4'});
                            if(obj_bf.category_id==null){
                               forward_btn.prop('disabled', true).find("i").css({'color':'#f4f4f4'}); 
                            }else{
                                forward_btn.prop('disabled', false).find("i").css({'color':'black'});
                            }
                        break;
                        case 2:
                            back_btn.prop('disabled', false).find("i").css({'color':'black'});
                            if(obj_bf.subcategory_id==null){
                                forward_btn.prop('disabled', true).find("i").css({'color':'#f4f4f4'}); 
                            }else{
                                forward_btn.prop('disabled', false).find("i").css({'color':'black'});
                            }
                        break;
                        case 3:
                            back_btn.prop('disabled', false).find("i").css({'color':'black'});
                            if(obj_bf.section_id==null){
                                forward_btn.prop('disabled', true).find("i").css({'color':'#f4f4f4'}); 
                            }else{
                                forward_btn.prop('disabled', false).find("i").css({'color':'black'});
                            }
                        break;
                    }
                }
                
                
                this.loadMockTestBasicConfiguration = function (module,obj){
                    
                    obj["module"]=module;
                    $.each(listeners, function(i){
                        try {
                            switch(module)
                            {
                                case 1:
                                    obj_bf["module"]=1;
                                    that.setButton();
                                    listeners[i].selCategoryDetails(obj);
                                    setTimeout(function (){
                                        b_fav_topic.html("Category");
                                    },200)
                                    
                                    break;
                                case 2:
                                    listeners[i].selSubCategoryDetails(obj);
                                    b_fav_topic.html("Sub Category");
                                    break;
                                case 3:
                                    listeners[i].selSectionDetails(obj);
                                    b_fav_topic.html("Subject");
                                    break;
                                case 4:
                                    listeners[i].selFavSection(obj);
                                    break;
                            }
                            return false;
                        }catch(e){alert(e)}
                    });
                }
                
                var placeSearch, autocomplete;
                
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                  };
                
                this.initAutocomplete=function () {
                    autocomplete = new google.maps.places.Autocomplete(
                        (document.getElementById('autocomplete_search')),
                        {types: ['geocode']});
                    autocomplete.addListener('place_changed', that.fillInAddress);
                  }
                
                this.fillInAddress=function () {
                    var place = autocomplete.getPlace();
                    for (var component in componentForm) {
                      document.getElementById(component).value = '';
                      document.getElementById(component).disabled = false;
                    }
                    for (var i = 0; i < place.address_components.length; i++) {
                      var addressType = place.address_components[i].types[0];
                      if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(addressType).value = val;
                      }
                    }
                    software_basic_cofiguration[module]["status"]=true;
                  }
                
                this.geolocate=function () {
                    if (navigator.geolocation) {
                      navigator.geolocation.getCurrentPosition(function(position) {
                        var geolocation = {
                          lat: position.coords.latitude,
                          lng: position.coords.longitude
                        };
                        var circle = new google.maps.Circle({
                          center: geolocation,
                          radius: position.coords.accuracy
                        });
                        autocomplete.setBounds(circle.getBounds());
                      });
                    }
                    $("body").find(".pac-container").css({'z-index': '1000000'})
                  }
                
                function is_valid_url(url) {
                    return /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(url);
                }
                
                this.setNavTabs= function (){
                    nav_tab_list=$("<ul>").addClass("nav nav-tabs")
                    return nav_tab_list;
                }
                
                this.getBoxbody = function (){
                    software_about_box_body=$("<div>").addClass("nav-tabs-custom").append(that.setNavTabs());
                    software_about_box_body_tab_pane=$("<div>").addClass("tab-pane active").attr({'id':'about'}).append(form)
                    software_about_box_body.append(software_about_box_body_tab_pane);
                    software_about_box_body.append(that.getBoxFooter());
                    return software_about_box_body;
                }
                
                this.getBoxFooter =function (){
                    var div = $("<div>").addClass("box-footer clearfix no-border");
                     next=$("<button>").addClass("btn btn-block btn-primary").attr({'data-toggle':'modal','data-target':'myModal','type':'button'})
                                    .click(function ()
                                        {
                                            if((module==1|| module==3 || module==4 ) ){
                                                if(that.checkValidation(module)){
                                                    software_basic_cofiguration[module]["call_back"]();
                                                    module++;
                                                }
                                            }else{
                                                software_basic_cofiguration[module]["call_back"]();
                                                module++;
                                            }
                                        }).append("Next");               
                     skip=$("<button>").addClass("btn btn-block btn-default").attr({'data-toggle':'modal','data-target':'myModal','type':'button'})
                                    .click(function (){
                                            $('#myModal').modal('toggle');
                                        }).append("Skip");               
                     div.append($("<div>").addClass("col-md-3 col-xs-6 pull-left").css({'margin-top':'10px'}).append(skip));
                     div.append($("<div>").addClass("col-md-3 col-xs-6 pull-right").css({'margin-top':'10px'}).append(next));
                     return div;
                }
                
                this.setAltObj = function (){
                    software_contact_details={}
                    software_contact_details["software_id"]=software_id;
                    form.find(":input").each(function() {
                            if($(this).attr("name")=="alt_contact_no"){
                                software_contact_details["alt_contact_no"] = $(this).val();
                            }
                    });
                    that.setWebsiteDetails();
                }
                
                this.setWebObj = function (){
                    form.find(":input").each(function() {
                            if($(this).attr("name")=="website"){
                                software_contact_details["website"] = $(this).val();
                            }
                    });
                    $.ajax({
                            url: config.getU()+"/InsBasicAltContactSvr",
                            type: 'POST',
                            data: JSON.stringify(software_contact_details),
                            success: function(data)
                            {
                                software_contact_details={}
                            }
                        });
                    that.setAddressDetails();
                }
                
                this.setAddObj = function (){
                    software_contact_details={}
                    software_contact_details["software_id"]=software_id;
                    form.find(":input").each(function() {
                            if($(this).attr("name")=="street_number"){
                                software_contact_details["address"] = $(this).val();
                            }
                            if($(this).attr("name")=="route"){
                                software_contact_details["street"] = $(this).val();
                            }
                            if($(this).attr("name")=="locality"){
                                software_contact_details["city"] = $(this).val();
                            }
                            if($(this).attr("name")=="administrative_area_level_1"){
                                software_contact_details["state"] = $(this).val();
                            }
                            if($(this).attr("name")=="postal_code"){
                                software_contact_details["pincode"] = $(this).val();
                            }
                            if($(this).attr("name")=="country"){
                                software_contact_details["country"] = $(this).val();
                            }
                    });
                    $.ajax({
                            url: config.getU()+"/InsBasicAddressSvr",
                            type: 'POST',
                            data: JSON.stringify(software_contact_details),
                            success: function(data)
                            {
                                software_contact_details={}
                            }
                    });
                    that.setFavDetails();
                }
                
                this.setBasicObj = function (){
                    $.each(listeners, function(i){
                        try {
                            listeners[i].insBasicConfiguration(software_id);
                        }catch(e){}
                    });    
                }
                
                this.checkValidation = function (key){
                    var msg="" ;
                        if(!(software_basic_cofiguration[key]["status"])){
                           msg=software_basic_cofiguration[key]["message"];
                        }
                        if(msg==""){
                            return true;
                        }else{
                            $("body").find("#hidden").find("#error_msg").remove();
                            msg= "Please "+msg+" should not blank.";
                            error.empty();
                            error.append(msg);
                            return false;
                        }
                }
                this.insLoadBegin =function (){
                    error.html(config.getLoadingFb());
                }
                
                this.insLoadFinish=function (){
                    error.empty();
                    $('#myModal').modal('toggle');
                }

                this.insLoadFail = function() { 
                    error.html("Server Does not Responde");
                }
                
                
                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	MockTestBasicConfigurationVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insMockTestBasicConfigurationVgr : function(from){},
			selMockTestBasicConfigurationVgr : function() { },
                        delMockTestBasicConfigurationVgr : function(id) { },
                        getMockTestBasicConfigurationVgr : function(id) { },
                        updMockTestBasicConfigurationVgr : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
