jQuery.extend({
	MainSidebarProfileVgr: function(config){
		var that = this;
                var no=false;
		var listeners = new Array();
                
                this.setMainSidebarProfile = function (){
                    config.getView("MainSidebarVgr").removeSidebarMenu();
                    config.getView("MainSidebarVgr").setSidebarMenu(that.getSideBarMenuList("Home","fa-home",that.loadHome));
                    config.getView("MainSidebarVgr").setSidebarMenu(that.getSideBarMenuList("Assessment Test","fa fa-newspaper-o",that.loadAssesment_Test));
                    config.getView("MainSidebarVgr").setSidebarMenu(that.getSideBarMenuList("Performance","fa fa-pie-chart",that.loadPerformanceTest));
                }
                
                this.getSideBarMenuList = function (title,icon,callbeak){
                    return $("<li>").addClass("treeview").css({'cursor':'pointer'})
                                    .append($("<a>").append($("<i>").addClass("fa "+icon))
                                        .append($("<span>").append(title))
//                                        .append($("<small>").addClass("label pull-right bg-green").append("1"))
                                    ).click(function (){
                                        callbeak();
                                    });
                }
                
                this.loadAssesment_Test = function (){
                    config.getController("ProfileMgr").loadContentWrapperAssesmentVgr();
                }
                this.loadPerformanceTest = function (){
                    config.getController("ProfileMgr").loadPerformanaceVgr();
                }
                this.loadHome = function (){
                    config.getView("ContentWrapperProfileVgr").init()
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	MainSidebarProfileVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
