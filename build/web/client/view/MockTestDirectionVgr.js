jQuery.extend({
	MockTestDirectionVgr: function(config,pre){
		var that = this;
		var listeners = new Array();
                var tbody_img;
                var direction;
                var direction_img;
                var row_id;
                var cat_dorp_down;
                var sub_cat_dorp_down;
                var sec_dorp_down;
                var top_dorp_down;
                var sel_sub_cat;
                var sel_sec;
                var b=true;
                var ul_question_box_body;
                discription={};
                var text_editor;
                var temp_question_button;
                var temp_save_button;
                var lang_drop_down;
                var dorp_down_for_lang_copy_setting;
                var default_lang_id;
                var software_id;
                var table;
                
                this.getRow = function (data){
                    var button;
                    if(data.pay_by_user_status == undefined){
                        button=$("<a>").addClass("btn btn-success").append($("<i>").addClass("fa fa-plus")).append("&nbsp;&nbsp;Exam Circle").css({'display':'block'});
                        button.click(function (){
                            that.addExamCircle($(this),software_id,data.user_id);
                        });
                    }else{
                        if(data.pay_by_user_status=="true"){
                            button=$("<a>").attr("disabled","disabled").addClass("btn").append($("<i>").addClass("fa fa-check")).append("&nbsp;Accepted by User").css({'display':'block'});
                        }else{
                            button=$("<center>").addClass("")
                                    .append($("<a>").addClass("btn btn-default").append($("<i>").addClass("fa fa-check")).css({'webkit-border-radius': '0%','-moz-border-radius': '0%','border-radius':'0%','margin':'3px'}))
                                    .append($("<a>").addClass("btn btn-default").append($("<i>").addClass("fa fa-rupee")).append("&nbsp;Pay ").css({'webkit-border-radius': '0%','-moz-border-radius': '0%','border-radius':'0%','margin':'3px'}));
                            
//                  <button type="button" class="btn btn-default">Action</button>
//                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
//                    <span class="caret"></span>
//                    <span class="sr-only">Toggle Dropdown</span>
//                  </button>
                        }
                    }
                    
                    var img=$("<img>").attr({'src':'../../dist/img/user2-160x160.jpg'}).addClass("img-responsive img-circle").css({'height':'85px'});
                    return $("<div>").addClass("col-lg-2 col-xs-6 col-md-2 col-sm-3")
                            .append($("<center>").addClass("small-box").css({'box-shadow':'0 2px 10px rgba(0,0,0,.2)'})
                                .append($("<div>").addClass("inner")
                                    .append($("<button>").addClass("close").append($('<span aria-hidden="true">×</span>')))
                                    .append(img)
                                    .append($("<p>").append(data.user_name))
                                    )
                                .append(button))
                }
                
                this.getDirection = function(){
                    check_lag_default=0;
                    discription={};
                    var parent_div =$("<div>").addClass("row");
                    var div=$("<div>").addClass("col-md-9");
                        direction_img =$("<div></div>").addClass("col-md-3 no-padding");
                        direction_img.append(that.getImgPanelTitle);
                        direction_img.append(that.getUploadArea(180,26,"dragAndDropFiles","demoFiler","multiUpload"));
                        direction_img.append(that.getImgBoxFooter());
                        var box_img=that.getImgBoxbody();
                        var table_img = that.getImgTable();
                        tbody_img=table_img.find("tbody");
                        box_img.append(table_img);
                        direction_img.append(box_img);
                        div.append(direction_img);
                        if(config.getView("UploadImg")==null){
                            var obj = {
                            // Valid file formats
                            support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                            form: "demoFiler", // Form ID
                            dragArea: "dragAndDropFiles", // Upload Area ID
                            multiUpload:"multiUpload",
                            url:config.getU()+"/Img",
                            parent_obj:that,
                            strlenght:25,
                            module:1
                            }
                            $.getScript("../../admin/model/UploadImg.js").done(function() {config.setModel("UploadImg",new $.UploadImg(obj));}).fail(function(e) {alert(e);}); 
                        }
                        //--------------------------------------------------------
			direction =$("<div></div>").addClass("col-md-9 no-padding");
                        direction.append(that.getPanelTitle);
                        var box=that.getBoxbody();
                        var panel_question =$("<div></div>").addClass("panel panel-default")
                        var lan_body =$("<div></div>").addClass("panel-body")
                        lan_body.append(that.getLanBox());
                            panel_question.append(lan_body);
                        panel_question.append(that.getTextEditor("editor1"));
                        $.getScript("https://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js").done(function(){        
                            
                        text_editor=CKEDITOR.replace( 'editor1', {
			extraPlugins: 'mathjax',
			mathJaxLib: 'http://cdn.mathjax.org/mathjax/2.6-latest/MathJax.js?config=TeX-AMS_HTML'
		} );
                
                        }).fail(function(e) {alert(e);}); 
                    var panel =$("<div>").addClass("panel panel-default");
                            var panel_body =$("<div></div>").addClass("panel-body ")
                            panel_body.append(that.getCategory_DropDown);
                            panel_body.append(that.getSubCategory_DropDown);
                            panel_body.append(that.getSection_DropDown);
                            panel_body.append(that.getTopic_DropDown());
                            panel.append(panel_body);
                            box.append($("<div>").addClass("col-lg-12").append(panel));
//                            var panel_question_body_row =$("<div>").addClass("row");
                            var panel_question_body =$("<div></div>").addClass("panel-body")
                            panel_question_body.append(that.getQuestionBox());
                            var lan_body =$("<div></div>").addClass("panel-body")
                            lan_body.append(that.getLanBox());
//                            panel_question_body_row.append(panel_question_body);
                            panel_question.append(that.getBoxFooter());
                            panel_question.append(panel_question_body);
                            box.append($("<div>").addClass("col-lg-12").append(panel_question));
                            direction.append(box);
                            div.append(direction);
                            parent_div.append(div)
                            parent_div.append($("<div>").addClass("col-md-3").append(that.getSponsored()));
                        //--------------------------------------------------------
                            that.loadLanguage(1);
                            that.loadCategory();
                        //-----------this code is resposible for load category and load subcatgory--------------------------
//                        that.loadCategory();
//                        cat_dorp_down.find(".cat1").trigger("change");
                        return parent_div;
		}
                this.getSponsored = function (){
                    return $("<div>").append('<div class="box" id="stickyheader" style="position: static;"><div class="box-header no-padding"><span style="color:#999;margin-left:5px;margin-right:5px;">Sponsored</span></div><div class="box-body"><div><img class="img-responsive" src="../../dist/images/banner1.png" alt="User Image" style="margin: 5px;"><a class="users-list-name" href="#">Infopark India</a><span class="users-list-date">New Product</span></div></div><div class="box-footer clearfix"><button type="button" class="pull-left btn btn-default" id="sendEmail">Send</button></div></div>')
                }
                this.getPanelTitle = function (){
                    var div =$("<div></div>").addClass("box-header ");
                    div.append($("<h3></h3>").addClass("box-title").append($("<b>Direction Details</b>")));
                    div.append(that.getDropdown());
                    div.append(that.getFooter());
                    
                    return div;
                }
                
                this.getImgPanelTitle = function (){
                    var div =$("<div></div>").addClass("box-header ");
                    div.append($("<h3></h3>").addClass("box-title").append($("<b>Direction Relative Imges</b>")));
                    return div;
                }
                
                this.getCategory_DropDown = function (){
                    cat_dorp_down =$("<div></div>").addClass("form-group col-lg-6");
                    cat_dorp_down.append($("<label>Category :</label>"))
                    cat_dorp_down.append($("<a>").attr({'id':'cat_img_loading'}));
                    cat_dorp_down.append($("<select>Category :</select>").addClass("form-control cat1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var id = cat_dorp_down.find(".cat1 option:first").val();
                            $( ".cat1 option:selected" ).each(function() {
                              id = $( this ).val();
                            });
                        that.loadSubCategory(id);
                    });
                    return cat_dorp_down;
                }
                
                this.getSubCategory_DropDown = function (){
                    sub_cat_dorp_down =$("<div></div>").addClass("form-group col-lg-6");
                    sub_cat_dorp_down.append($("<label>Sub Category :</label>"))
                    sub_cat_dorp_down.append($("<a></a>").attr({'id':'cat_img_loading'}));
                    sub_cat_dorp_down.append($("<select>Sub Category :</select>").addClass("form-control sub1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:first").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        $( ".sub1 option:selected" ).each(function() {
                          subcategory_id = $(this).val();
                        });
                        that.loadSection(category_id,subcategory_id);
                    });
                    return sub_cat_dorp_down;
                }
                
                this.getSection_DropDown = function (){
                    sec_dorp_down =$("<div></div>").addClass("form-group col-lg-6");
                    sec_dorp_down.append($("<label>Section :</label>"))
                    sec_dorp_down.append($("<a></a>").attr({'id':'cat_img_loading'}));
                    sec_dorp_down.append($("<select></select>").addClass("form-control sec1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                        $( ".sec1 option:selected" ).each(function() {
                            section_id = $( this ).val();
                        });
                            that.loadTopic(category_id,subcategory_id,section_id);
                    });
                    return sec_dorp_down;
                }
                
                this.getTopic_DropDown = function (){
                    top_dorp_down =$("<div></div>").addClass("form-group col-lg-6");
                    top_dorp_down.append($("<label>Topic :</label>"))
                    top_dorp_down.append($("<a></a>").attr({'id':'cat_img_loading'}));
                    top_dorp_down.append($("<select></select>").addClass("form-control top1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                        var topic_id = top_dorp_down.find(".top1 option:selected").val();
                        $( ".top1 option:selected" ).each(function() {
                            topic_id = $( this ).val();
                        });
                        direction["category_id"]=category_id;
                        direction["subcategory_id"]=subcategory_id;
                        direction["section_id"]=section_id;
                        direction["topic_id"]=topic_id;
                    });
                    return top_dorp_down;
                }
                
                this.getTextEditor = function (editor){
                    var text_editor =$("<div></div>").addClass("panel-body no-padding");
                    text_editor.append($("<form></form>").append($("<textarea></textarea>").attr({'id':editor,'name':editor,'rows':'8','cols':'40'})))
                    return text_editor;
                }
                
                this.getQuestionBox = function (){
                    var question_box =$("<div></div>").addClass("");
                    question_box.append($("<div></div>").addClass("box-header ").append($("<h3 class='box-title'>Relative Question</h3>")));
                    var question_box_body=$("<div></div>").addClass("box-body").append($("<div></div>").addClass("btn-group").css({'width':'100%','margin-bottom':'10px'}));
                    var question_input=$("<div></div>").addClass("input-group")
                            .append($("<input/>").addClass("form-control").attr({'id':'new-event','type':'text','placeholder':'Enter the number of Question'})
                                    .keyup(function(event) {
                                        if (event.keyCode==13) {
                                            if($("#new-event").val()!="")
                                            that.addQuestionBox(parseInt($("#new-event").val()));    
                                        }
                                    }))
                            .append($("<div></div>").addClass("input-group-btn")
                            .append($("<button>Add</button>").addClass("btn btn-primary btn-flat").attr({'id':'add-new-event','type':'button'}).css({'border-color':'rgb(0, 31, 63)','background-color':' rgb(0, 31, 63)'})
                            .click(function(){
                                that.addQuestionBox(parseInt($("#new-event").val()));    
                            })
                            ));
                    question_box_body.append(question_input);
                    question_box_body.append($("<p></p>"));
                    ul_question_box_body=$("<div></div>").addClass("text-center");
                    question_box_body.append(ul_question_box_body);
                    question_box.append(question_box_body);
                    return question_box;
                }
                
                this.getLanBox = function (){
                    lang_drop_down =$("<div></div>").addClass("form-group col-lg-12");
                    lang_drop_down.append($("<label>Select Language :</label>"))
                    lang_drop_down.append($("<select>:</select>").addClass("form-control lang1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        if(direction["language_details"][that.getLangId()]["direction_details"]!=""){
                            text_editor.setData(decodeURIComponent(direction["language_details"][that.getLangId()]["direction_details"]));
                            if($("#add_in_queue").hasClass("btn-danger")){
                                $("#add_in_queue").removeClass("btn-danger");
                                $("#add_in_queue").addClass("btn-success");
                                $("#add_in_queue").empty();
                                $("#add_in_queue").append("Remove in Queue");
                            }   
                        }else{
                            text_editor.setData("");
                            if($("#add_in_queue").hasClass("btn-success")){
                                $("#add_in_queue").removeClass("btn-success");
                                $("#add_in_queue").addClass("btn-danger");
                                $("#add_in_queue").empty();
                                $("#add_in_queue").append("Add in Queue");
                            }   
                        }
                        ul_question_box_body.find("a").each(function(){
                            if(direction["language_details"][that.getLangId()]["question_details"][$(this).attr('question_no')]["qus"]!=null){
                                if(!$(this).hasClass("btn-btn-google")){
                                    $(this).removeClass("btn-google")
                                    $(this).addClass("btn-google-green")
                                }
                            }else{
                                if($(this).hasClass("btn-google-green")){
                                    $(this).removeClass("btn-google-green")
                                    $(this).addClass("btn-google")
                                }
                            }
                            
                        }); 
                        //sub_cat_dorp_down.find(".lang1").trigger("change");
                    }).focusin(function() {
                        if(direction["language_details"][that.getLangId()]["direction_details"]==""){
                            var model = config.getView("ErrorMsgVgr").getErrorMsg("Error !","Please Add Direction in a Queue?","vsisi",null);
                            $("#hidden").empty();
                            $("#hidden").append(model);
                            model.find(".modal-footer").find("button").first().remove();
                            model.modal('show');
                        }
                    });
                   return lang_drop_down;
                }
                
                this.getLangId=function (){
                    var lang_id;
                        $( ".lang1 option:selected" ).each(function() {
                            lang_id = $( this ).val();
                        });
                        return lang_id;
                }

                this.addQuestionBox = function (no){
                    ul_question_box_body.empty();
                                $.each(direction["language_details"],function(j){
                                    delete direction["language_details"][j]["question_details"];
                                });
                            for(var kk=1;kk<=no;kk++){
                                $.each(direction["language_details"],function(j){
                                    if(direction["language_details"][j]["question_details"]==null){
                                        direction["language_details"][j]["question_details"]={}
                                    }
                                    direction["language_details"][j]["question_details"][kk]={}
                                });
                                ul_question_box_body.append(that.getButton(kk));
                            }
                }
                
                this.getButton=function (kk){
                    var a=$("<a></a>").addClass("btn btn-social-icon btn-google")
                                        .attr({'id':'ques_box','data-toggle':'tooltip','data-placement':'top','data-original-title':'Question Details','question_no':kk}) 
                                        .css({'margin-right':'5px','margin-top':'5px'})
                                        .append($("<i></i>").append(kk))
                                        .click(function(){
                                            temp_question_button=$(this);
                                            temp_question_button.empty();
                                            try {temp_question_button.tooltip('hide');}catch(e){}
                                            temp_question_button.append(config.getLoadingData());
                                            that.setQuestionForm($("#hidden"),kk);
                                        });
                    return a;
                }
                
                this.addOption = function (no){
                    ul_question_box_body.empty();
                    question={};
                    for(var i=1;i<=no;i++){
                        question[i]={}
                        ul_question_box_body.append($("<a>").addClass("btn btn-social-icon btn-bitbucket").attr({'data-toggle':'tooltip','data-placement':'top' ,'title':'Question Details'}).css({'margin-right':'5px','margin-top':'5px'}).append(i).click(function (){
//                            temp_question_button=$(this);
                            that.setInsForm($("#hidden"),i);
                        }));
                    }
                }
                
                this.getBoxbody = function (){
                    var div =$("<div></div>").addClass("box-body no-padding");
                    return div;
                }
                
                this.getImgBoxbody = function (){
                    var div =$("<div></div>").addClass("box-body");
                    return div;
                }
                                    
                this.getUploadArea = function (no,font,dragAndDropFiles,form_name,file){
                    var diva =$("<div></div>").addClass("box-body").css({'margin-top':'19px'});
                    var div =$("<div></div>").addClass("uploadArea").attr({'id':dragAndDropFiles}).css({'min-height':no+'px'});
                    div.append($("<h1></h1>").append("Drop Images Here").css({'font-size':font+'px'}));
                    div.append($("<form></form>").attr({'name':form_name,'id':form_name,'enctype':'multipart/form-data'}).append($("<input/>").attr({'type':'file','data-maxwidth':'620','data-maxheight':'620','name':'file[]','id':file}).css({'width':'0px','height':'0px','overflow':'hidden'})));
                    diva.append(div);
                    return diva;
                }
                
                this.getImgTable = function (){
                    var div =$("<table></table>").addClass("table table-bordered")
                    //set header for table 
                            .append($("<thead></thead>")
                                .append($("<tr></tr>")
                                .append($("<th></th>").append("Images"))
                                .append($("<th></th>").append("Action").css({'width':'40px'}))
                            ))
                            .append($("<tbody></tbody>").attr({'id':'direction_img'}))
                    return div;
                }
                
                this.getImgRow = function (data){
                    var row = $("<tr></tr>").attr({'id':data.url})
                            .append($("<td></td>").append(data.image))
                            .append($("<td></td>").append(that.getImgDropdown(data.url))
                    );
                    return row;            
                }
                
                this.getCatOption = function (data){
                    var row = $("<option></option>").attr({'value':data.category_id})
                              .append($("<td>"+data.category_name+"</td>"));
                    return row;            
                }
                
                var check_lag_default=0;
                
                this.getLangOption = function (data){
                    if(direction["language_details"]==null){
                        direction["language_details"]={};
                        direction["language_details"][data.language_id]={};
                        direction["language_details"][data.language_id]["direction_details"]="";
                    }else{
                        if(direction["language_details"][data.language_id]==null){
                           direction["language_details"][data.language_id]={};
                           direction["language_details"][data.language_id]["direction_details"]="";
                        }
                    }
                    var row = $("<option>").attr({'value':data.language_id})
                              .append(data.language_name);
                      if(check_lag_default>0){
                          dorp_down_for_lang_copy_setting.append(that.getPasteLangList(data.language_id,data.language_name));
                      }else{
                          default_lang_id=data.language_id;
                      }
                      check_lag_default++;
                    return row;            
                }
                
                this.getPasteLangList = function (language_id,language_name){
                    return $("<li></li>").append($("<a>"+language_name+"</a>")).click(function (){
                        if(that.checkDefault_langData()){
                            direction["language_details"][language_id]["direction_details"]=direction["language_details"][default_lang_id]["direction_details"];
                            direction["language_details"][language_id]["question_details"]=direction["language_details"][default_lang_id]["question_details"];
                        }else{
                            var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Error !","Please fill the data in default language.");
                            $("#hidden").append(error_model);
                            error_model.modal('show');
                            error_model.find(".modal-footer").find("button").first().remove();   
                            error_model.find(".modal-footer").find("button").text("Ok");   
                        }
                    });
                }
                
                this.checkDefault_langData = function (){
                                            var b=true;
                                            $.each(direction["language_details"], function(key,value){
                                                if(default_lang_id==key){
                                                    var lang=config.getModel("Language").getLanguage(key);
                                                    if(value["direction_details"]==""){
                                                        b=false;
                                                        return false;
                                                    }
                                                    if(!b){
                                                        return false;
                                                    }
                                                    if(value["question_details"]!=null){
                                                        $.each(value["question_details"], function(k,v){
                                                            if(v["qus"]==null){
                                                                b=false;
                                                                return false;
                                                            }
                                                        });
                                                    }else{
                                                        b=false;
                                                        return false;
                                                    }
                                                }
                                            });
                                            return b;
                }
                
                this.getSubOption = function (data){
                    var row = $("<option></option>").attr({'value':data.subcategory_id})
                              .append($("<td>"+data.subcategory_name+"</td>"));
                    return row;            
                }
                
                this.getSecOption = function (data){
                    var row = $("<option></option>").attr({'value':data.section_id})
                              .append($("<td>"+data.section_name+"</td>"));
                    return row;            
                }
                
                this.getTopOption = function (data){
                    var row = $("<option></option>").attr({'value':data.topic_id})
                              .append($("<td>"+data.topic_name+"</td>"));
                    return row;            
                }
                
                this.getImgDropdown = function (a){
                    var drop_down =$("<div></div>").addClass("input-group-btn")
                                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                        .append($("<span></span>").addClass("fa fa-caret-down")))
                                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li></li>").append($("<a>Copy</a>")))
                                            );
                    return that.getImgClickEvent(drop_down,a);
                }
                
                this.getImgClickEvent =function (drop_down,direction_id){
                    $(drop_down).find('ul>li:nth-child(1)').click(function (){
                        window.prompt("Copy url form clipboard.", direction_id);
                    });
                    return drop_down;
                }
                
                this.getBoxFooter =function (){
                    var div = $("<div></div>").addClass("panel-footer clearfix");
                        div.append($("<div></div>").addClass("col-md-3 col-xs-5")
                                .append($("<button></button>").addClass("btn btn-block btn-danger").attr({'data-toggle':'modal','data-target':'myModal','id':'add_in_queue'})
                                    .append("Add In Queue").click(function ()
                                        {
                                            if(text_editor.getData()!=""){
                                                if($(this).hasClass("btn-danger")){
                                                    $(this).removeClass("btn-danger");
                                                    $(this).addClass("btn-success");
                                                    $(this).empty();
                                                    $(this).append("Remove in Queue");
                                                    direction["language_details"][that.getLangId()]["direction_details"]=encodeURIComponent(text_editor.getData());
                                                }else{
                                                    $(this).removeClass("btn-success");
                                                    $(this).addClass("btn-danger");
                                                    $(this).empty();
                                                    $(this).append("Add in Queue");
                                                    direction["language_details"][that.getLangId()]["direction_details"]="";
                                                    text_editor.setData("");
                                                }
                                            }
                                        }
                                     )
                                    ))
                        div.append($("<div></div>").addClass("col-md-2 col-xs-6 pull-right")
                                .append($("<button></button>").addClass("btn btn-block btn-default")
                                    .append("Reset").click(function ()
                                        {
                                            text_editor.setData("");
                                            if($("#add_in_queue").hasClass("btn-success")){
                                                $("#add_in_queue").removeClass("btn-success");
                                                $("#add_in_queue").addClass("btn-danger");
                                                $("#add_in_queue").empty();
                                                $("#add_in_queue").append("Add in Queue");
                                                direction["language_details"][that.getLangId()]["direction_details"]="";
                                            }   
                                        }
                                     )
                                )
                        )
                        return div;
                }
                
                this.getFooter =function (){
                    var div = $("<div></div>").addClass("form-group pull-right col-md-3");
                        div.append($("<button></button>").addClass("btn btn-block btn-primary")
                                    .append("Save").click(function ()
                                        {
                                            temp_save_button=$(this);
                                            var b=true;
                                            var msg;
                                            $.each(direction["language_details"], function(key,value){
                                                var lang=config.getModel("Language").getLanguage(key);
                                                if(!b){
                                                    return false;
                                                }
                                                if(value["direction_details"]==""){
                                                    b=false;
                                                    msg="Should not blank Direction of "+lang+" language";
                                                    return false;
                                                }
                                                if(!b){
                                                    return false;
                                                }
                                                if(value["question_details"]!=null){
                                                    $.each(value["question_details"], function(k,v){
                                                        if(v["qus"]==null){
                                                            b=false;
                                                            msg="Should not blank Question no "+k+" of "+lang+" language Direction";
                                                            return false;
                                                        }
                                                    });
                                                }else{
                                                    b=false;
                                                    msg="Please Create Atleast one Question "+lang+" language Direction";
                                                    return false;
                                                }
                                                
                                            });
                                            if(b){
                                                $.each(listeners, function(i){
                                                    listeners[i].insDirection(direction);
                                                });
                                            }else{
                                                var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Error !",msg);
                                                $("#hidden").empty();
                                                $("#hidden").append(error_model);
                                                error_model.modal('show');
                                                error_model.find(".modal-footer").find("button").first().remove();
                                            }
                                            
                                        }
                                     )
                                )
                        
                        
                        return div;
                }
                
                this.getDropdown = function (a){
                    
                    dorp_down_for_lang_copy_setting=$("<ul></ul>").addClass("nav nav-list ");
                    var drop_down =$("<div></div>").addClass("input-group-btn pull-right").css({'margin-right':'30px'})
                                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                        .append($("<span></span>").addClass("fa fa-caret-down")))
                                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li>").css({'padding-left':'10px'}).append("Paste To").append(dorp_down_for_lang_copy_setting))
                                            .append($("<li></li>").addClass("divider"))
                                            .append($("<li>").css({'padding-left':'10px'}).append("Comming Soon"))
                            );
                                    
                                    
                                            return drop_down;
                }
                
                this.getImgBoxFooter =function (){
                    var div = $("<div></div>").addClass("box-footer clearfix");
                        div.append($("<div></div>").addClass("col-md-6 col-xs-6")
                                .append($("<button></button>").addClass("btn btn-block btn-primary").attr({'data-toggle':'modal','data-target':'myModal'})
                                    .append("Upload").click(function ()
                                        {
                                            config.getModel("UploadImg")._startUpload();
                                        }
                                     )
                                    ))
                        div.append($("<div></div>").addClass("col-md-6 col-xs-6 pull-right")
                                .append($("<button></button>").addClass("btn btn-block btn-primary")
                                    .append("Refresh").click(function ()
                                        {
                                            config.getModel("UploadImg")._refresh();
                                        }
                                     )
                                )
                        )
                        return div;
                }
                
                this.getQusImgBoxFooter =function (){
                    var div = $("<div></div>").addClass("box-footer clearfix");
                        div.append($("<div></div>").addClass("col-md-6 col-xs-6")
                                .append($("<button></button>").addClass("btn btn-block btn-primary")
                                    .append("Upload").click(function ()
                                        {
                                            config.getModel("QusUploadImg")._startUpload();
                                        }
                                     )
                                    ))
                        div.append($("<div></div>").addClass("col-md-6 col-xs-6 pull-right")
                                .append($("<button></button>").addClass("btn btn-block btn-primary")
                                    .append("Refresh").click(function ()
                                        {
                                            config.getModel("QusUploadImg")._refresh();
                                        }
                                     )
                                )
                        )
                        return div;
                }
                
                this.setQuestionForm =function (a,kk){
                    that.loadQuestionVgr(a,kk)
                }
                
                this.loadQuestionVgr = function (a,kk){
                    $.getScript("../../admin/view/QuestionVgr.js").done(function() {
                            config.setView("QuestionVgrM",new $.QuestionVgr(config));
                            that.setInsForm($("#hidden"),kk)
                    }).fail(function() {alert("Error:There is problem in login admin QuestionVgr.js file.");}); 
                }
                
                this.setInsForm=function (a,kk){
                    temp_question_button.empty();
                    temp_question_button.append($("<i></i>").append(kk));
                    var model = config.getView("ModelVgr").getModel("New Question",that.getInsForm(kk),"submit");
                    a.html(model);
                    model.modal('show');
                    var editor;
                    
                    setTimeout(function (){
                        if(CKEDITOR.instances.editor2 ) {
                            CKEDITOR.remove(CKEDITOR.instances.editor2);
                        }
                        editor=CKEDITOR.replace( 'editor2', {
                            extraPlugins: 'mathjax',
                            mathJaxLib: 'http://cdn.mathjax.org/mathjax/2.6-latest/MathJax.js?config=TeX-AMS_HTML'
                        });
                        config.setModel("QusUploadImg",new $.UploadImg({
                                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                                form: "demoFiler1", // Form ID
                                dragArea: "dragAndDropFiles1", // Upload Area ID
                                multiUpload:"multiUpload1",
                                url:config.getU()+"/Img",
                                parent_obj:config.getView("QuestionVgrM"),
                                strlenght:10,
                                module:2
                        }));
                    },1000);
                    
                    $.fn.modal.Constructor.prototype.enforceFocus = function () {
                        modal_this = this
                        $(document).on('focusin.modal', function (e) {
                        if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                        // add whatever conditions you need here:
                        &&
                        !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')
                        && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_textarea')) {
                        modal_this.$element.focus()
                        }
                        })
                    };
                    var obj=direction["language_details"][that.getLangId()]["question_details"][kk];
                    if(obj["qus"]!=null){
                        editor.setData(decodeURIComponent(obj["qus"]));
                        config.getView("QuestionVgrM").addOption(Object.keys(obj["option_details"]).length-1);
                        $.each(obj["option_details"],function(j){
                            $("#"+j).val(decodeURIComponent(obj["option_details"][j]));
                        });
                        $("#option").val(decodeURIComponent(obj["option_details"]["answer"]));
                        if(obj["img_details"]!=null){
                            $.each(obj["img_details"],function(key,val){
                            item={};
                            item["url"]=key;
                            item["image"]=val;
                            config.getView("QuestionVgrM").addImgQuestion(item);
                        });
                        }
                    }
                    var sub=false;
                    var direction_form =model.find('#direction_form');
                    direction_form.find("#submit").click(function(){
                                that.removeFail();
                                var check=true;
                                var msg ;
                                var option_len=0;
                                direction_form.find(":input").each(function(){
                                    if($(this).attr("name")!="op"&&$(this).attr("id")!="editor2"&&$(this).attr("name")!="file[]"&&$(this).attr("name")!=null&&$(this).attr("name")!="option"){
                                        option_len++;
                                    }
                                }); 
                                if(editor.getData()==""){
                                    check=false;
                                    msg= "Question Field should not blank";
                                }else if(option_len==0){
                                    msg= "Please Create Option Field.";
                                    check=false;
                                }else{
                                    direction_form.find(":input").each(function(){
                                        if($(this).attr("name")!="op"&&$(this).attr("id")!="editor2"&&$(this).attr("name")!="file[]"&&$(this).attr("name")!=null&&$(this).attr("name")!="option"){
                                            if($(this).val()==""){
                                                msg= "Please Option Field "+$(this).attr("name")+" should not blank.";
                                                check=false;
                                                return false;
                                            }
                                        }
                                    }); 
                                }
                                if(check){
                                    direction["language_details"][that.getLangId()]["question_details"][kk]["qus"]=encodeURIComponent(editor.getData());
                                    direction_form.find(":input").each(function(){
                                        if(direction["language_details"][that.getLangId()]["question_details"][kk]["option_details"]==null){
                                            direction["language_details"][that.getLangId()]["question_details"][kk]["option_details"]={};    
                                        }
                                        if($(this).attr("name")!="op"&&$(this).attr("id")!="editor2"&&$(this).attr("name")!="file[]"&&$(this).attr("name")!=null&&$(this).attr("name")!="option"){
                                            direction["language_details"][that.getLangId()]["question_details"][kk]["option_details"][$(this).attr("name")]=encodeURIComponent($(this).val());
                                        }
                                    }); 
                                    direction["language_details"][that.getLangId()]["question_details"][kk]["option_details"]["answer"]=encodeURIComponent(direction_form.find("#option").val());
                                    direction["language_details"][that.getLangId()]["question_details"][kk]["img_details"]={};
                                    $.each(config.getView("QuestionVgrM").getObj(),function(key,val){
                                        direction["language_details"][that.getLangId()]["question_details"][kk]["img_details"][key]=val;
                                    });
                                    $('#myModal').modal('toggle');
                                    temp_question_button.removeClass('btn-google');
                                    temp_question_button.addClass('btn-google-green');
                                }else{
                                        var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Error !",msg);
                                        $("#hidden").append(error_model);
                                        error_model.modal('show');
                                        error_model.find(".modal-footer").find("button").first().remove();
                                }
                     });
                    
                    model.find(".modal-footer").find("button").first().empty();
                    model.find(".modal-footer").find("button").first().append("Add in Queue");
                }
                
                this.getInsForm =function (kk){
//                    var div=$("<div></div>");
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'direction_form'}).submit(function(e) {e.preventDefault();})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                        .append($("<label></label>").addClass("control-label").attr({'for':'direction_name'}).append("Question No. is "+kk))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls")
                                        .append(that.getTextEditor("editor2"))
                                        )));
//                    div.append(form);
                    form.append(config.getView("QuestionVgrM").getQuestion());
                    return form;        
                }
                
                this.addImgDscription =function (data){
                   tbody_img.append(that.getImgRow(data))
                }                                                        
                
                this.addImgQuestion =function (data){
                   tbody_img.append(that.getImgRow(data))
                }                                                        
                
                this.addItem=function (data,obj){
                    var on=obj.find("select");
                    if(on.hasClass("cat1")){
                        that.addCategory(data);   
                    }else if(on.hasClass("sub1")){
                        that.addSubCategory(data);   
                    }else if(on.hasClass("sec1")){
                        that.addSection(data);   
                    }else if(on.hasClass("top1")){
                        that.addTopic(data);   
                    }
                }          
                this.addCategory=function (data){
                   cat_dorp_down.find(".select2").append(that.getCatOption(data))
                }          
                this.setLanguage =function (language){
                   $(".lang1").append(that.getLangOption(language))
                }
                
                this.addSubCategory=function (data){
                   sub_cat_dorp_down.find(".sub1").append(that.getSubOption(data))
                }          
                
                this.addSection=function (data){
                   sec_dorp_down.find(".sec1").append(that.getSecOption(data))
                }          
                
                this.addTopic=function (data){
                   top_dorp_down.find(".top1").append(that.getTopOption(data))
                }          
                
                this.subAddInDirection =function (item){
                    that.addSubCategory(item);
                } 
                
                this.secAddInDirection =function (item){
                    that.addSection(item);
                } 
                
                this.topAddInDirection =function (item){
                    that.addTopic(item);
                } 
                
                this.insLoadBegin =function (){
                    temp_save_button.empty();
                    temp_save_button.append(config.getLoadingData());
                }
                
                this.insLoadFinish =function (){
                    temp_save_button.empty();
                    temp_save_button.append("Save");
                    $("body").find("#hidden").find("#error_msg").remove();
                    var msg= "Your Directional data has been successfully saved.";
                    var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Success.!",msg);
                    error_model.find(".modal-footer").find("button").first().remove();
                    error_model.find(".modal-footer").find("button").empty();
                    error_model.find(".modal-footer").find("button").append("Ok");
                    $("#hidden").append(error_model);
                    error_model.modal('show');
                }
                
                this.insLoadFail = function() { 
                    temp_save_button.empty();
                    temp_save_button.append("Server Error");
                }
                
                this.removeFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.selLoadBegin =function (obj){
                    obj.find("#cat_img_loading").css({'margin-left':'10px'}).append(config.getLoadingData());
                }
                this.selLoadFinish=function (obj){
                    obj.find("#cat_img_loading").empty();
                    setTimeout(function (){
                        obj.trigger("change");
                    },20);
                }
                
                this.directionSecLoadFinish=function (){
                    sec_dorp_down.find("#sec_img_loading").empty();
                    setTimeout(function (){
                        sec_dorp_down.find(".sec1").trigger("change");
                    },20);
                }
                
                this.directionTopLoadFinish=function (){
                    top_dorp_down.find("#top_img_loading").empty();
                    setTimeout(function (){
                        top_dorp_down.find(".top1").trigger("change");
                    },20);
                }
                
                this.directionLangLoadFinish=function (){
                    lang_drop_down.find("#lang_img_loading").empty();
                    b=false;
                }
                
                this.directionSubLoadFail = function() { 
                    sub_cat_dorp_down.find("#sub_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.directionSecLoadFail = function() { 
                    sec_dorp_down.find("#sec_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.directionTopLoadFail = function() { 
                    top_dorp_down.find("#top_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.directionLangLoadFail = function() { 
                    lang_drop_down.find("#lang_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.selRemoveFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.updLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.updLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                }
                
                this.updLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                this.loadCategory =function (){
                    cat_dorp_down.find(".cat1").empty();
                    $.each(listeners, function(i){
                        listeners[i].selCategory(software_id,cat_dorp_down);
                    });
                } 

                this.loadLanguage =function (module){
                    lang_drop_down.find(".lang1").empty();
                    config.getView("MainVgr").getListener().loadLanguageVgr(that);
                } 
                
                this.loadSubCategory =function (category_id){
                    sub_cat_dorp_down.find(".sub1").empty();
                    $.each(listeners, function(i){
                        listeners[i].selSubCategory(software_id,category_id,sub_cat_dorp_down);
                    });
                } 
                
                this.loadSection =function (category_id,subcategory_id){
                    sec_dorp_down.find(".sec1").empty();
                    $.each(listeners, function(i){
                        listeners[i].selSection(software_id,category_id,subcategory_id,sec_dorp_down);
                    });
                } 
                //-------------------------------------------------------------------------------------------------------------------------------------
                this.loadTopic =function (category_id,subcategory_id,section_id){
                    top_dorp_down.find(".top1").empty();
                    $.each(listeners, function(i){
                        listeners[i].selTopic(software_id,category_id,subcategory_id,section_id,top_dorp_down);
                    });
                } 

                this.addListener = function(list){
                            listeners.push(list);
                }

            this.init = function (softwareid){
                software_id=softwareid;
                pre.setHeaderTitle("Direction Wise Question");
                pre.setBreadCrumbName(" fa-users text-blue","Direction Wise Question");
                pre.removeBoxHeader();
                pre.removeBoxbody();
                pre.setBoxHeaderTitle($("<div>").append($("<h3>").addClass("box-title").css({'line-height':':24px;'}).append("Direction Wise Question")));
                pre.setBoxBodyContent(that.getDirection());
//                that.loadMockTestDirection();
            }
            
        },
        	
	/**
	 * let people create listeners easily
	 */
	MockTestDirectionVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selTest:function() { }
		}, list);
	}
        
});
