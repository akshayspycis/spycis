jQuery.extend({
	FPBoxVgr: function(config){
		var that = this;
		var listeners = new Array();
                var modelform;
                var model_footer;
                var img_loading_forget;
                var img_loading_register;
                var img_loading_sigin;
                var loading_boolean=true;
                this.getFPBox = function(){
                        img_loading_forget=$("<a>");
                        img_loading_register=$("<a>");
                        img_loading_sigin=$("<span>");
			var model =$("<div>").addClass("login-box-body");
                        model.append(that.getFPBoxHeader("Enter the Email address "));
                        model.append(that.getFPBoxBody());
                        model.append($("<div>").addClass("social-auth-links text-center"));
                        model.append($("<a>").addClass("text-center").css({'cursor':'pointer'}).append("I already have a membership").append("&nbsp;").append(img_loading_forget).click(function (){
                            if(loading_boolean){
                                that.loadLoginBox();
                            }
                        }));
                        model.append($("<br>"));
                        model.append($("<a>").addClass("text-center").css({'cursor':'pointer'}).append("Register a new membership").append("&nbsp;").append(img_loading_register).click(function (){
                            if(loading_boolean){
                                that.loadRegisterBox();
                            }
                        }));
                        return model;
		}
                
                this.getFPBoxHeader= function (header_title){
                    var model_header =$("<p>").addClass("login-box-msg").append(header_title);
                    return model_header;
                }
                
                this.getFPBoxBody = function (model_form){
                    modelform=$("<form>").attr({'id':'rg_form'});
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<input/>").attr({'type':'email','id':'email','placeholder':'Email','name':'email'}).addClass("form-control"))
                                .append($("<span>").addClass("glyphicon glyphicon-envelope form-control-feedback")));
                    modelform.append($('<label id="error"></label>'));
                    var row = $("<div>").addClass("row")    
                    row.append($("<div>").addClass("col-xs-5 pull-right")
                            .append($("<a>").addClass("btn btn-primary btn-block").attr({'type':'button','id':'button','name':'button'}).append(img_loading_sigin).append("Continue").click(function (){
                                $.each(listeners, function(i){
                                    listeners[i].insRegister(modelform);
                                });
                            }))
                            );
                    
                    modelform.append(row);
                    return modelform;
                }
                
                this.getForm=function (){
                    return modelform;
                }
                
                
                this.setloading =function (img){
                    loading_boolean=false;
                    img.append(config.getLoadingData());
                }
                
                this.removeLoading =function (img){
                    loading_boolean=true;
                }
                
                this.setDisableButton =function (obj){
                    obj.prop('disabled', true);
                }
                
                this.setEnableButton =function (obj){
                    obj.prop('disabled', false);
                }
                
                this.setServerError =function (){
                    model_footer.find("#error_msg").append("Server doesn't Respond.");
                }
                
                this.removeServerError =function (){
                    model_footer.find("#error_msg").empty();
                }
                
                
                this.loadRegisterBox = function (){
                    that.setloading(img_loading_register);
                    config.loadRegistraionBoxVgr(that);
                }
                
                this.loadLoginBox = function (){
                    that.setloading(img_loading_forget);
                    if(config.getView("LoginBoxVgr")==null){
                        $.get("../view/LoginBoxVgr.js").done(function() {
                                config.setView("LoginBoxVgr",new $.LoginBoxVgr(config));
                                that.LoginBox();
                        }).fail(function() {alert("Error: LoginBoxVgr.js Loading Problem");}); 
                    }else{
                        that.LoginBox();
                    }
                }
                
                this.LoginBox = function (){
                    that.removeLoading(img_loading_forget);
                    $("body").find(".login-box").find(".login-box-body").remove();
                    $("body").find(".login-box").append(config.getView("LoginBoxVgr").getLoginBox());
                }
                this.insLoadBegin =function (){
                    modelform.find(".row").find("#button").prop('disabled', true);
                    modelform.find(".row").find("#button").empty();
                    modelform.find(".row").find("#button").append(config.getLoadingData());
                }
                
                this.insLoadFail = function() { 
                    modelform.find(".row").find("#button").prop('disabled', false);
                    modelform.find(".row").find("#button").empty();
                    modelform.find(".row").find("#button").append("Register");
                    modelform.find("#error").empty();
                    modelform.find("#error").append("Error: Server doesn't Respond.")
                    modelform.find("#error").css({'display':'block','color':'red'});    
                }
                this.insLoadFinish = function() { 
                    modelform.find(".row").find("#button").remove();
                    modelform.find(".form-group").remove();
                    modelform.find("#error").append("We've sent your account password an email to *****************@gmail.com.")
                    modelform.find("#error").css({'display':'block','color':'black'});    
                }
                
                
                this.addListener = function(list){
                            listeners.push(list);
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	FPBoxVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
