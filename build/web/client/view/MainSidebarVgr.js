jQuery.extend({
	MainSidebarVgr: function(config){
		var that = this;
                var sidebar_list;
                this.setMainSideBar= function(){
                        sidebar_list =$("<ul>").addClass("sidebar-menu");
			var aside=$("<aside>").addClass("main-sidebar").append($("<section>").addClass("sidebar").css({'height':'auto'}).append(that.getUserPanel()).append(sidebar_list));
                        return aside;
		}
                
                this.getUserPanel = function (){
                    return $("<div>").addClass("user-panel")
                             .append($("<div>").addClass("pull-left image").append(config.getModel("ProfileConfig").getProfilePic("img-circle").css({'height':'25px','width':'25px'})))
                             .append($("<div>").addClass("pull-left info")
                                .append($("<p>").append(config.getModel("ProfileConfig").getFirstName()))
                                
                    );
                }
                this.setSidebarMenu = function (div){
                    sidebar_list.append(div);
                }
                this.removeSidebarMenu = function (div){
                    sidebar_list.empty();
                }
                
        }
});
