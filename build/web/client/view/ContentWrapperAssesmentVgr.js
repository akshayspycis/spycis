jQuery.extend({
    ContentWrapperAssesmentVgr: function (config, pre) {
        var that = this;
        var row;
        var demo;
        var temp_obj;
        var listeners = new Array();
        this.getRow = function (data) {
            var button
            if (data.type != "demo") {
                if (data.eligibility != null) {
                    if (data.status == null) {
                        console.log("data.payment_status", data.payment_status)
                        if (data.payment_status == "true") {
                            button = $("<a>").addClass("btn btn-primary btn-block").append("<b>Start Test<b>").click(function () {
                                that.getDateAndTime(data.test_id, $(this));
                            });
                        } else {
                            button = $("<a>").addClass("btn btn-primary btn-block").append("<b>Pay Now<b>").click(function () {
                                that.createOrder(data, button);
                            });
                        }
                    } else {
                        button = $("<a>").addClass("btn btn-success btn-block").append("<b>Complete<b>");
                    }
                } else {
                    button = $("<a>").addClass("btn btn-danger btn-block").append("<b>Not Eligible<b>");
                }
            } else {
//                        if(data.status==null){
                button = $("<a>").addClass("btn btn-primary btn-block").append("<b>Start Test<b>").click(function () {
                    that.getDateAndTime(data.test_id, $(this));
                });
//                            }else{
//                                button=$("<a>").addClass("btn btn-success btn-block").append("<b>Complete<b>");
//                            }

            }
            return $("<div>").addClass("col-md-4 col-sm-6 col-lg-3")
                    .append($("<div>").addClass("box")
                            .append($("<div>").addClass("box-header")
                                    .append($("<center>").addClass("box-header")
                                            .append(that.getTestLogo())
                                            .append($("<h3>").addClass("profile-username text-center no-margin").append(data.test_name))
                                            .append($("<p>").addClass("text-muted text-center").append(data.unique_test_key))
                                            )
                                    )
                            .append($("<div>").addClass("box-body")
                                    .append($("<ul>").addClass("nav nav-stacked")
                                            .append(that.getList("Exam Date", data.e_date))
                                            .append(that.getList("Active Time", data.start_time))
                                            .append(that.getList("Time Duration", data.time + " min."))
                                            .append(that.getList("No of Question", data.no_qus))
                                            .append(that.getList("Total Marks", data.total_marks))
                                            .append(that.getList("Cut Off", data.cut_off))
                                            )
                                    .append(button)
                                    )
                            )

        }

        this.getTestLogo = function () {
            return $('<img />', {
                class: 'img-responsive',
                src: '../../dist/images/exam.gif',
            });
        }

        this.getList = function (title, value) {
            return $("<li>").append($("<a>").append($("<b>").append(title)).append($("<span>").addClass("pull-right").append(value)));
        }

        this.addTest = function (data) {
            if (data.type != "demo") {
                row.append(that.getRow(data));
            } else {
                demo.append(that.getRow(data));
            }

        }
        this.removeFail = function () {
            row.empty();
            demo.empty();
        }
        this.selLoadBegin = function () {
            row.append($("<center>").append(config.getLoadingData()));
            demo.append($("<center>").append(config.getLoadingData()));
        }
        this.selLoadFinish = function () {
            row.empty();
            demo.empty();
        }
        this.selLoadFail = function () {
            row.appned("<p>Server doesn't respond.</p>");
        }
        this.removeFailTest = function () {
            temp_obj.empty();
            temp_obj.append("Start Test");
        }
        this.selTestLoadBegin = function () {
            temp_obj.empty();
            temp_obj.append(config.getLoadingData());
        }
        this.selTestLoadFinish = function () {
            temp_obj.empty();
            temp_obj.append("Start Test");
        }
        this.selTestLoadFail = function () {
            temp_obj.empty();
            temp_obj.append("You doesn't start Test before Date and time");
        }

        this.loadTest = function () {
            that.removeFail();
            $.each(listeners, function (i) {
                listeners[i].selTest();
            });
        }



        this.createOrder = function (data, button) {
            if (data.test_id) {
                $.ajax({
                    type: "post",
                    url: config.getU() + "/GetUrlSvr",
                    data: {'url': 'PAYMENT_DETAILS_URL'},
                    success: function (url) {
                        if (url || url != "null") {
                            that.popup(data, button, url);
                        }
                    }, error: function () {
                    },
                });
            }
        }

        this.popup = function (data, button, url) {
            var form;
            form = "<form  action='" + url + "' name='f1' method='get' target='_blank'>";
            form += '<input type="hidden" name="asdasdid" value="' + data.test_id + '" />';
            form += '<input type="hidden" name="v1" value="' + that.getV1() + '" />';
            form += '<input type="hidden" name="v1" value="' + that.getV1() + '" />';
            form += '<input type="hidden" name="key" value="' + that.createUUID() + '" />';
            form += '<input type="hidden" name="namsddasde" value="' + data.test_name + '" />';
            form += '<input type="hidden" name="udasdasdasdid" value="' + config.getModel("ProfileConfig").getUser_id() + '" />';
            form += '<input type="hidden" name="adsdaddasdsdasd" value="' + config.getU() + '" />';
            form += '</form>';
            form = $(form);
            $('body').append(form);
            document.f1.submit()
            button.html(config.getLoadingData())
            var valu = 0;
            var checkPayment = setInterval(function () {
                if (valu > 10)
                    clearInterval(checkPayment);
                that.checkPaymentStatus(data.test_id, button, windowfocus, checkPayment);
                valu++;
            }, 10000)
            var windowfocus = $(window).focus(function () {
                that.checkPaymentStatus(data.test_id, button, windowfocus);
            });
            return false;
        }


        this.checkPaymentStatus = function (test_id, button, windowfocus, checkPayment) {
            config.getModel("ContentWrapperAssesment").checkPaymentStatus(test_id).then(function (status) {
                if (status || status == "true") {
                    button.unbind("click");
                    button.html("<b>Start Test<b>");
                    button.click(function () {
                        that.getDateAndTime(test_id, $(this));
                    });
                    if (checkPayment)
                        clearInterval(checkPayment);
                    if (windowfocus)
                        windowfocus.unbind("focus");
                }
            }).catch(function (err) {
                if (windowfocus)
                    windowfocus.unbind("focus");
            });

        }

        this.createUUID = function () {
            var s = [];
            var hexDigits = "0123456789abcdef";
            for (var i = 0; i < 36; i++) {
                s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
            s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
            s[8] = s[13] = s[18] = s[23] = "-";

            var uuid = s.join("");
            return uuid;
        }
        this.getV1 = function () {
            return Math.floor((Math.random() * 10000000) + 1);
        }
        this.getDateAndTime = function (test_id, obj) {
            temp_obj = obj;
            that.removeFailTest();
            $.each(listeners, function (i) {
                listeners[i].getDateAndTime(test_id);
            });
        }

        this.addListener = function (list) {
            listeners.push(list);
        }
        this.selectTab = function (text) {
            $('.nav-tabs a[href="#' + text + '"]').tab('show');
        }
        this.init = function () {
            config.setTitle('Online Assessment Test');
            pre.setHeaderTitle("Online Assessment Test");
            pre.setBreadCrumbName("fa-newspaper-o", "Assessment Test");
            pre.removeBoxHeader();
            pre.removeBoxbody();
            pre.setBoxHeaderTitle($("<h3>").addClass("box-title").css({'line-height': ':24px;'}).append("List of Assesment Test"));
            pre.setBoxBodyContent(that.getTab());
            if (config.getModel("ContentWrapperAssesment") == null) {
                $.getScript("../model/ContentWrapperAssesment.js").done(function () {
                    config.setModel("ContentWrapperAssesment", new $.ContentWrapperAssesment(config));
                    $.getScript("../controller/ContentWrapperAssesmentMgr.js").done(function () {
                        config.setController("ContentWrapperAssesmentMgr", new $.ContentWrapperAssesmentMgr(config, config.getModel("ContentWrapperAssesment"), config.getView("ContentWrapperAssesmentVgr")));
                        that.loadTest();
                    }).fail(function () {
                        alert("Error :problem in ContentWrapperAssesmentMgr.js file ");
                    });
                }).fail(function () {
                    alert("Error:problem in ContentWrapperAssesmentvgr.js file ");
                });
            } else {
                that.loadTest();
            }
        }
        this.getTab = function () {
            row = $("<div>").addClass("row")
            demo = $("<div>").addClass("row")
            return $("<div>").addClass("nav-tabs-custom")
                    .append($("<ul>").addClass("nav nav-tabs")
                            .append($("<li>").addClass("active").append($("<a>").attr({'href': '#demo', 'data-toggle': 'tab', 'aria-expanded': 'true'}).append("Practice Test")))
                            .append($("<li>").append($("<a>").attr({'href': '#original', 'data-toggle': 'tab', 'aria-expanded': 'true'}).append("Examination Test")))
                            )
                    .append($("<div>").addClass("tab-content")
                            .append($("<div>").addClass("tab-pane active").attr({'id': 'demo'})
                                    .append(demo))
                            .append($("<div>").addClass("tab-pane").attr({'id': 'original'})
                                    .append(row))
                            )
        }
    },
    /**
     * let people create listeners easily
     */
    ContentWrapperAssesmentVgrListener: function (list) {
        if (!list)
            list = {};
        return $.extend({
            selTest: function () {
            }
        }, list);
    }

});