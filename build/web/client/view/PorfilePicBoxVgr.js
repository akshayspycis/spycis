jQuery.extend({
	ProfilePicBoxVgr: function(config){
		var that = this;
		var listeners = new Array();
                var loading_boolean=true;

                this.getProfilePicBox = function(title,model_form,call){
			var model =$("<div></div>").addClass("register-box-body");
                        model.append(that.getProfilePicBoxHeader("Update Profile Picture"));
                        model.append(that.getMessage());
                        model.append(that.getProfilePicBoxBody());
                        model.append(that.getProfilePicFooter());
                        if(config.getView("UploadImg")==null){
                            var obj = {
                                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                                form: "demoFiler", // Form ID
                                dragArea: "dragAndDropFiles", // Upload Area ID
                                multiUpload:"multiUpload",
                                url:"/mocktest_v1//Img",
                                parent_obj:that,
                                strlenght:25,
                                module:1
                            }
                            $.getScript("../model/UploadImg.js").done(function() {config.setModel("UploadImg",new $.UploadImg(obj));}).fail(function(e) {alert(e);}); 
                        }
                        return model;
		}
                this.getProfilePicBoxHeader= function (header_title){
                    var model_header =$("<p></p>").addClass("login-box-msg").append(header_title);
                    return model_header;
                }
                this.getMessage= function (header_title){
                    var message =$("<center></center>").addClass("form-group has-feedback")
                            .append('<a class="btn btn-default btn-sm" ><i class="fa fa-user"></i>&nbsp;</a>')
                            .append($("<label>").append("&nbsp;&nbsp;"+config.getModel("ProfileConfig").getFirstName()+",Upload Your Profile?"));
                    return message;
                }
                this.getProfilePicBoxBody = function (model_form){
                    modelform =$("<div></div>").addClass("row");
                    modelform.append($("<div></div>").addClass("col-sm-2 col-xs-1"));
                    modelform.append($("<div></div>").addClass("col-sm-8 col-xs-10")
                             .append($("<div></div>").addClass("box")
                                .append($("<div></div>").addClass("box-header")
                                    .append($("<div></div>").addClass("box-tools pull-right")
                                        .append($("<button></button>").addClass("btn btn-box-tool").attr({'data-toggle':'tooltip','title':'Remove'})
                                            .append($("<i>").addClass("fa fa-times"))).click(function (){
                                        })))
                                .append(that.getUploadArea(180,"dragAndDropFiles","demoFiler","multiUpload"))
                        ));
                    modelform.append($("<div></div>").addClass("col-sm-2 col-xs-1"));
                    return modelform;
                }
                this.getProfilePicFooter = function (model_form){
                    var modelform =$("<div></div>").addClass("row");
                    modelform.append($("<div></div>").addClass("col-xs-4 pull-left")
                            .append($("<button></button>").addClass("btn btn-default btn-block btn-flat").attr({'type':'submit'})
                                            .append("Skip").click(function (){
                                            })
                                    ))
                    modelform.append($("<div></div>").addClass("col-xs-4 pull-right")
                            .append($("<button></button>").addClass("btn btn-primary btn-block btn-flat").attr({'type':'submit'})
                                            .append("Save").click(function (){
                                            })
                                    ));
                    return modelform;
                }
                this.getUploadArea = function (no,dragAndDropFiles,form_name,file){
                    var diva =$("<div></div>").addClass("box-body");
                    var div =$("<div></div>").addClass("uploadArea").attr({'id':dragAndDropFiles}).css({'min-height':no+'px'});
                    div.append($("<center>").addClass("fa fa-times").append($("<i>").addClass("fa fa-user").css({'font-size':'130px','padding-left':'5px'})))
                    div.append($("<form></form>").attr({'name':form_name,'id':form_name,'enctype':'multipart/form-data'}).append($("<input/>").attr({'type':'file','data-maxwidth':'620','data-maxheight':'620','name':'file[]','id':file}).css({'width':'0px','height':'0px','overflow':'hidden'})));
                    diva.append(div);
                    return diva;
                }
                this.setloading =function (img){
                    loading_boolean=false;
                    img.append(config.getLoadingData());
                }
                this.removeLoading =function (img){
                    loading_boolean=true;
                    img.empty();
                }
                
                this.setServerError =function (){
//                    model_footer.find("#error_msg").append("Server doesn't Respond.");
                }
                
                this.removeServerError =function (){
//                    model_footer.find("#error_msg").empty();
                }
                
                this.insLoadBegin =function (){
                    modelform.find(".row").find("#submit").prop('disabled', true);
                    modelform.find(".row").find("#submit").empty();
                    modelform.find(".row").find("#submit").append(config.getLoadingData());
                }
                
                this.insLoadFail = function() { 
                    modelform.find(".row").find("#submit").prop('disabled', false);
                    modelform.find(".row").find("#submit").empty();
                    modelform.find(".row").find("#submit").append("Register");
                    modelform.find("#error").empty();
                    modelform.find("#error").append("Error: Server doesn't Respond.")
                    modelform.find("#error").css({'display':'block','color':'red'});    
                }
                
                this.addListener = function(list){
                            listeners.push(list);
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	ProfilePicVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insProfilePic : function() { }
		}, list);
	}
});
