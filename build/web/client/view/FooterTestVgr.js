jQuery.extend({
	FooterTestVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                var no=false;
                var control_sidebar;
                var button_panel;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
            this.setFooter= function(){
                $("body").find('.main-footer').css({'padding':'0px'});
                $("body").find('.main-footer').empty();
                $(window).trigger('resize');
                config.getView("ContentWrapperVgr").removeBoxFooter();
                    if(config.getWidth()>1100){
                        config.getView("ContentWrapperVgr").setBoxFooter($("<div></div>").addClass("box-header")
                                .append(that.getButton("","btn-default","Mark for Review & Next",that.reviewAns))
                                .append(that.getButton("","btn-default","Clear Response",that.clearAns))
                                .append($("<div></div>").addClass("col-lg-3"))
                                .append(that.getButton("","btn-primary","Save & Next",that.setAns)));
                    }else if(config.getWidth()>991&&config.getWidth()<=1100){
                        config.getView("ContentWrapperVgr").setBoxFooter($("<div></div>").addClass("box-header")
                                .append(that.getButton("","btn-default","Mark for Review & Next",that.reviewAns))
                                .append(that.getButton("","btn-default","Clear Response",that.clearAns))
                                .append(that.getButton("","btn-primary","Save & Next",that.setAns)));
                    }else if(config.getWidth()>556&&config.getWidth()<=991){
                        config.getView("ContentWrapperVgr").setBoxFooter($("<div></div>").addClass("box-header")
                                .append(that.getButton("col-xs-5","btn-default","Mark for Review & Next",that.reviewAns))
                                .append(that.getButton("col-xs-4","btn-default","Clear Response",that.clearAns))
                                .append(that.getButton("col-xs-3","btn-primary","Save & Next",that.setAns)));
                    }else{
                        config.getView("ContentWrapperVgr").setBoxFooter($("<div></div>").addClass("box-header")
                                .append(that.getButton("col-xs-4","btn-default",$("<div>").append($("<i>").addClass("fa fa-1x fa-check-square-o")).append($("<i>").addClass("fa fa-arrow-right")),that.reviewAns))
                                .append(that.getButton("col-xs-4","btn-default",$("<i>").addClass("fa fa-1x fa-paint-brush"),that.clearAns))
                                .append(that.getButton("col-xs-4","btn-primary",$("<div>").append($("<i>").addClass("fa fa-1x fa-save")).append($("<i>").addClass("fa fa-arrow-right")),that.setAns)));
                    }
		}
                
                this.getButton = function (div_class,btn_class,title,callback){
                    var button=$("<div>").addClass("col-md-4 col-lg-3 "+div_class).append($("<button>").addClass("btn btn-block "+btn_class)
                            .append(title).click(function (){
                                            callback("");
                                        }));
                    return button;                    
                }
                this.setAns=function (){
                    var a=config.getView("ContentWrapperTestVgr").getSelectedOption()
                    if(a!=undefined){
                      config.getModel("Question").setAns(a);
                    }else
                    that.clearAns(a);
                }
                
                this.clearAns=function (a){
                    config.getModel("Question").clearAnsweredButton(a);
                }
                
                this.reviewAns=function (){
                    var a=config.getView("ContentWrapperTestVgr").getSelectedOption()
                    if(a!=null){
                      config.getModel("Question").reviewAns(a);
                    }else{
                      config.getModel("Question").reviewNotMark();
                    }
                }
                
                this.addListener = function(list){
                            listeners.push(list);
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	FooterTestListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});