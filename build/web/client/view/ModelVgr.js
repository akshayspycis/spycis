jQuery.extend({
	ModelVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
                var modelform;
                var model_footer;
                var c;
                                   
                this.getModel = function(title,model_form,call){
                        c=call;
                        modelform=model_form;
			var model =$("<div></div>").addClass("modal fade").attr({'id':'myModal','tabindex':'-1','role':'dialog','aria-labelledby':'myModalLabel'});
                        var model_dialog =$("<div></div>").addClass("modal-dialog").attr({'role':'document'});
                        var model_content=$("<div></div>").addClass("modal-content");
                        model_content.append(that.getModelHeader(title));
                        model_content.append(that.getModelBody(model_form.append(that.getModelFooter(call))));
                        model_dialog.append(model_content);
                        model.append(model_dialog);
                        return model;
		}

                this.getModelHeader= function (header_title){
                    var model_header =$("<div></div>").addClass("modal-header with-border");
                    model_header.append($("<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"))
                    model_header.append($("<h4 class='modal-title' id='myModalLabel'>"+header_title+"</h4>"));
                    return model_header;
                }
                
                this.getModelBody = function (model_form){
                    var model_body =$("<div></div>").addClass("modal-body");
                    model_body.append(model_form);
                    return model_body ;
                }
                this.getModelFooter = function (call){
                    model_footer =$("<div></div>").addClass("modal-footer");
                    model_footer.append($("<label></label>").addClass("error").attr({'id':'error_msg'}));
                    model_footer.append($("<button>Save</button>").addClass("btn btn-default").attr({'type':'button','id':call}));
                    model_footer.append($("<button>Close</button>").addClass("btn btn-default").attr({'type':'button','data-dismiss':'modal','id':'cancel'}));
                    model_footer.append($("<a></a>").attr({'id':'img_loading'}));
                    return model_footer;
                }
                
                this.setloading =function (){
                    model_footer.find("#img_loading").append(config.getLoadingData());
                }
                this.removeLoading =function (){
                    model_footer.find("#img_loading").empty();
                }
                
                this.setDisableButton =function (){
                    model_footer.find("#"+c).prop('disabled', true);
                    model_footer.find("#cancel").prop('disabled', true);
                }
                
                this.setEnableButton =function (){
                    model_footer.find("#"+c).prop('disabled', false);
                    model_footer.find("#cancel").prop('disabled', false);
                }
                
                this.setServerError =function (){
                    model_footer.find("#error_msg").append("Error: Server doesn't Respond.");
                }
                
                this.removeServerError =function (){
                    model_footer.find("#error_msg").empty();
                }
                
                this.addListener = function(list){
                            listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	ModelVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
