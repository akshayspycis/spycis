jQuery.extend({
	AnsSheetVgr: function(config,pre){
		var that = this;
		var row ;
		var table ;
		var temp_obj ;
		var test_id ;
		var lang_drop_down ;
		var count=0 ;
		var listeners = new Array();
                this.getRow = function (val,data){
                    count++;
                    return $("<tr>")
                                .append($("<th>").css({'width':' 10px'}).append(count))
                                .append($("<th>").append(data.answer))
                                .append($("<th>").append(data.marks))
                                .append($("<th>").append(data.select_option))
                                .append($("<th>").append(val));
                }
                this.getTestLogo =function (){
                    return $('<img />', { 
                        class: 'img-responsive',
                        src: '../../dist/images/exam.gif',
                    });
                }
                this.addAnsSheet =function (data,val){
                    row.append(that.getRow(val,data));
                   
                }                                                        
                this.removeFail = function() { 
                    row.empty();
                }
                this.selLoadBegin =function (){
                   row.append($("<center>").append(config.getLoadingData()));
                }
                this.selLoadFinish=function (){
                    row.empty();
                }
                this.selLoadFail = function() { 
                    row.append("<center>Server doesn't respond.</center>");
                }
                this.removeFailTest = function() { 
                    temp_obj.empty();
                    temp_obj.append("Start Test");
                }
                this.selTestLoadBegin =function (){
                   temp_obj.empty();
                   temp_obj.append(config.getLoadingData());
                }
                this.selTestLoadFinish=function (){
                    temp_obj.empty();
                    temp_obj.append("Start Test");
                }
                this.selTestLoadFail = function() { 
                    temp_obj.empty();
                    temp_obj.append("You doesn't start Test before Date and time");
                }
                
                this.loadTest =function (){
                    count=0;
                    that.removeFail();
                    $.each(listeners, function(i){
                        listeners[i].selAnsSheet(test_id);
                    });
                } 
                this.addListener = function(list){
                            listeners.push(list);
                }
            this.init = function (testid,value){
                test_id=testid;
                pre.setHeaderTitle("Answer Sheet");
                pre.setBreadCrumbName("fa-line-chart","Answer Sheet");
                pre.removeBoxHeader();
                pre.removeBoxbody();
                pre.setBoxHeaderTitle(that.getHeader(value));
                table=that.getTable();
                pre.setBoxBodyContent(table);
                if(config.getModel("AnsSheet")==null){
                                row.append($("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin")));
                                $.getScript("../model/AnsSheet.js").done(function() {
                                    config.setModel("AnsSheet",new $.AnsSheet(config));
                                    $.getScript("../controller/AnsSheetMgr.js").done(function() {
                                        config.setController("AnsSheetMgr",new $.AnsSheetMgr(config,config.getModel("AnsSheet"),config.getView("AnsSheetVgr")));
                                            that.selLoadFinish();
                                            that.loadTest();
                                    }).fail(function() {alert("Error :problem in AnsSheetMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in AnsSheetvgr.js file ");}); 
                 }else{
                        that.selLoadFinish();
                        that.loadTest();
                 }             
            }
            
            this.getHeader = function (value){
//                <select class="form-control lang1 select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true"><option value="1">English</option><option value="2">Hindi</option></select>
              return $("<div>").addClass("no-padding")
                        .append(that.getTestLogo().removeClass("img-responsive").css({'width':'50px'}))
                        .append($("<h2>").addClass("box-title").css({'line-height':'24px'}).append("&nbsp;&nbsp;Answer Sheet for Mock Test ").append($("<b>").append(value)))
                        .append($("<div>").addClass("form-group pull-right").css({'margin-right':'15px'})
                            .append($("<div>").addClass("pull-right")
                                .append($("<button>").addClass("btn btn-primary pull-right").attr({'type':'button'})
                                    .append("Print&nbsp;&nbsp;&nbsp;")
                                    .append($("<span>").addClass("badge").attr({'type':'button'})
                                         .append($("<i>").addClass("fa fa-print")))
                                     .click(function (){
                                         var divContents = document.getElementById(table.attr("id")).outerHTML;
                                            var printWindow = window.open('', '', '');
                                            printWindow.document.write('<html><head><title>Answer Sheet</title>');
                                            printWindow.document.write('</head><body >');
                                            printWindow.document.write(divContents);
                                            printWindow.document.write('</body></html>');
                                            printWindow.document.close();
                                            printWindow.print();
                                     })     
                                    )));
            }
            
            this.getTable = function (value){
//                <select class="form-control lang1 select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true"><option value="1">English</option><option value="2">Hindi</option></select>
              row=$("<tbody>");
              return $("<table>").addClass("table table-bordered").attr({'id':'mytable'})
                        .append($("<thead>")
                            .append($("<tr>")
                                .append($("<th>").css({'width':' 10px'}).append("S.No"))
                                .append($("<th>").append("Correct Answer"))
                                .append($("<th>").append("Marks"))
                                .append($("<th>").append("Your Answer"))
                                .append($("<th>").append("Question ID"))
                            ))
                        .append(row);
            }
        },
        	
	/**
	 * let people create listeners easily
	 */
	AnsSheetVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selTest:function() { }
		}, list);
	}
        
});