jQuery.extend({
	FooterProfileVgr: function(config){
            var that = this;
            this.setFooter= function(){
                config.getView("FooterVgr").removeFooter();
                config.getView("FooterVgr").setFooter('<div class="pull-right hidden-xs"><b>Version</b> 1.0</div>');
                config.getView("FooterVgr").setFooter('<strong>Copyright © 2015 <a href="#">Vibrant Education</a>.</strong> All rights reserved.');
            }
        }
});