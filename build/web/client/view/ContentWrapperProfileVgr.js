jQuery.extend({
	ContentWrapperProfileVgr: function(config,per){
            var that=this;
            var listeners = new Array();
            var activity;
            var settings;
            var modelform ;
            var form_cover ;
            var ul_list ;
            var pass_form ;
            var upload_img ;
            var cover_pic ;
            this.init = function (){
                config.setTitle("Welcome to Spycis");
                per.setHeaderTitle("Home");
                per.setBreadCrumbName("fa-home","Dashboard");
                per.removeBoxHeader();
                per.removeBoxbody();
                per.setBoxHeaderTitle($("<h3>").addClass("box-title").css({'line-height':':24px;'}).append("WelCome to User"));
                per.setBoxBodyContent(that.getBody());
                if(config.getModel("ContentWrapperProfile")==null){
                         $.getScript("../model/ContentWrapperProfile.js").done(function(){
                                try {
                                        config.setModel("ContentWrapperProfile",new $.ContentWrapperProfile(config));
                                        $.getScript("../controller/ContentWrapperProfileMgr.js").done(function(){
                                        try {
                                                config.setController("ContentWrapperProfileMgr",new $.ContentWrapperProfileMgr(config,config.getModel("ContentWrapperProfile"),that));
                                                that.loadContentWrapperProfile();
                                            }catch(e){
                                                alert(e+" problem in ContentWrapperProfile mgr ");
                                            }
                                        }).fail(function() {alert("Error: Problem in ContentWrapperProfileMgr.js file ");}); 
                                }catch(e){
                                        alert(e+" problem in ContentWrapperProfile.js file ");
                                }
                       }).fail(function() {alert("Error: Problem in ContentWrapperProfile.js file ");}); 
                     }else{
                              that.loadContentWrapperProfile();
                     }
            }
            this.getBody= function (){
                activity=$("<div>").addClass("tab-pane active").attr({'id':'activity'});
                settings=$("<div>").addClass("tab-pane").attr({'id':'settings'});
                return $("<div>").addClass("row")
                    .append($("<div>").addClass("col-md-12")
                        .append($("<div>").addClass("nav-tabs-custom")
                            .append($("<ul>").addClass("nav nav-tabs")
                                .append($("<li>").addClass("active")
                                    .append($("<a>").attr({'href':'#activity','data-toggle':'tab','aria-expanded':'true'}).append("Activity"))
                                )
                                .append($("<li>")
                                    .append($("<a>").attr({'href':'#settings','data-toggle':'tab','aria-expanded':'true'}).append("Settings"))
                                )
                            )
                           .append($("<div>").addClass("tab-content")
                                .append(activity)
                                .append(settings)
                            )
                       )
                    );
            }
            
            this.getSetting = function (data){
                form_cover=$("<form>").attr({'name':"demoFiler_cover",'id':"demoFiler_cover",'enctype':'multipart/form-data'}).append($("<input/>").attr({'type':'file','data-maxwidth':'1233','data-maxheight':'620','name':'file[]','id':'multiUpload_input_cover'}).css({'width':'0px','height':'0px','overflow':'hidden'}));
                cover_pic=$("<div>").addClass("widget-user-header bg-black").css({"background":"url('../../dist/img/cover_page.jpg') no-repeat center","background-size":"100%",'height':'320px'});
                var objj = {
                            support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                            form: "demoFiler_cover", // Form ID
                            dragArea: "dragAndDropFiles", // Upload Area ID
                            multiUpload:"multiUpload_input_cover",
                            upload_img:cover_pic,
                            url:"/mocktest_v1//Img",
                            parent_obj:cover_pic,
                            strlenght:25,
                            module:1
                        }
                        $.getScript("../model/UploadCoverImg.js").done(function() {
                            config.setModel("UploadCoverImg",new $.UploadCoverImg(objj,config));
                        }).fail(function(e) {alert(e);}); 
                return $("<div>").addClass("box-body box-widget widget-user")
                        .append(cover_pic
                            .append($("<div>").addClass("icon").css({'z-index':'1000'})
                                .append($("<div></div>").addClass("input-group-btn").css({'width':'0%'})
                                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                        .append($("<span></span>").addClass("fa fa-camera"))
                                        .click(function (){
                                            if($(this).attr('aria-expanded')=="false" || $(this).attr('aria-expanded')==null){
                                                $(this).parent().addClass("open");
                                                $(this).attr('aria-expanded','true')
                                            }else{
                                                $(this).parent().removeClass("open");
                                                $(this).attr('aria-expanded','false')
                                            }
                                          })
                                        )
                                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li></li>").attr({'id':'cover_a'}).append(form_cover.append($("<a>").append("Upload Photo").css({'color':'#777','margin-left':'20px'}))))
                                            .append($("<li></li>").addClass("divider"))
                                            .append($("<li></li>").append($("<a>").append("Reposition").click(function (){
                                                  var objj = {
                                                        // Valid file formats
                                                        support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                                                        url:"/mocktest_v1//Img",
                                                        parent_obj:cover_pic,
                                                        strlenght:25,
                                                        module:1
                                                        }
                                                        setTimeout(function (){
                                                            $.getScript("../model/UploadCoverImg.js").done(function() {config.setModel("UploadCoverImg",new $.UploadCoverImg(objj,config));}).fail(function(e) {alert(e);}); 
                                                            config.getModel("UploadCoverImg").loadResizeableImageCss();
                                                        },200);
                                            })))
                                        )
                                    )
                            ))
                        .append($("<div>").addClass("col-md-3").css({'margin-top': '-79px'}).append(that.getUploadArea(180,"dragAndDropFiles","demoFiler","multiUpload")))
                        .append(that.getRegistrationBoxBody(data))
                        .append($("<div>").addClass("col-md-3").css({'margin-top':'10px'}).append(that.getSponsored()));
                        

            }
            this.getSponsored = function (){
                return $("<div>").append('<div class="box" id="stickyheader" style="position: static;"><div class="box-header no-padding"><span style="color:#999;margin-left:5px;margin-right:5px;">Sponsored</span></div><div class="box-body"><div><img class="img-responsive" src="../../dist/images/banner1.png" alt="User Image" style="margin: 5px;"><a class="users-list-name" href="#">Infopark India</a><span class="users-list-date">New Product</span></div></div><div class="box-footer clearfix"><button type="button" class="pull-left btn btn-default" id="sendEmail">Send</button></div></div>')
            }
            this.getGrammer = function (){
                return $("<div>").addClass("box")
                        .append($("<div>").addClass("box-header no-padding")
                            .append($("<span>").append("Grammar").css({'color':'#999','margin-left':'5px','margin-right':'5px'}))
                        .append($("<div>").addClass("box-body")
                            .append($("<ul>").addClass("nav nav-tabs pull-left ui-sortable-handle")
                                .append($("<li>").addClass("active").attr({'data-toggle':'tooltip','data-original-title':'Word of the Day'}).append($("<a>").attr({'href':'#sales-chart','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa fa-language"))))
                                .append($("<li>").attr({'data-toggle':'tooltip','data-original-title':'Thesaurus of the Day'}).append($("<a>").attr({'href':'#sales-chart','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa fa-sort-alpha-asc"))))
                            )
                            .append($("<div>").addClass("tab-content no-padding")
                                .append($("<div>").addClass("chart tab-pane active").attr('id','sales-chart').css({'position':'relative','height':'264px'})
                                    .append($("<ul>").addClass("todo-list ui-sortable")
                                        .append($("<li>").addClass("todo-list ui-sortable")
                                            .append($("<span>").addClass("handle ui-sortable-handle")
                                                .append($("<i>").addClass("fa fa-ellipsis-v"))
                                                .append($("<i>").addClass("fa fa-ellipsis-v")))
                                            .append($("<span>").addClass("text")
                                                .append("Rillic"))
                                            .append($("<div>").addClass("tools")
                                                .append($("<i>").addClass("fa fa-star"))))
                                        .append($("<li>").addClass("todo-list ui-sortable")
                                            .append($("<span>").addClass("handle ui-sortable-handle")
                                                .append($("<i>").addClass("fa fa-ellipsis-v"))
                                                .append($("<i>").addClass("fa fa-ellipsis-v")))
                                            .append($("<span>").addClass("text")
                                                .append("Rillic"))
                                            .append($("<div>").addClass("tools")
                                                .append($("<i>").addClass("fa fa-star"))))
                                        .append($("<li>").addClass("todo-list ui-sortable")
                                            .append($("<span>").addClass("handle ui-sortable-handle")
                                                .append($("<i>").addClass("fa fa-ellipsis-v"))
                                                .append($("<i>").addClass("fa fa-ellipsis-v")))
                                            .append($("<span>").addClass("text")
                                                .append("Rillic"))
                                            .append($("<div>").addClass("tools")
                                                .append($("<i>").addClass("fa fa-star"))))
                                        .append($("<li>").addClass("todo-list ui-sortable")
                                            .append($("<span>").addClass("handle ui-sortable-handle")
                                                .append($("<i>").addClass("fa fa-ellipsis-v"))
                                                .append($("<i>").addClass("fa fa-ellipsis-v")))
                                            .append($("<span>").addClass("text")
                                                .append("Rillic"))
                                            .append($("<div>").addClass("tools")
                                                .append($("<i>").addClass("fa fa-star"))))
                                        )))));
                                    
                                
            }
            this.getUpdateImgBox = function (){
                upload_img;
                try {
                    upload_img=config.getModel("ProfileConfig").getProfilePic("user-image").removeClass("user-image").css({'width':'230px','height':'216px'});
                }catch (e){
                    upload_img = $('<img />', { 
                            src: '../../dist/img/avatar.png'
                        }).css({'width':'230px','height':'216px'});
                }
//                        if(config.getView("UpdProfielImg")==null){
        
//                        }
                return $("<div>").addClass("ih-item square effect13 left_to_right").css({'overflow':'hidden'})
                        .append($("<a>")
                            .append($("<div>").addClass("img")
                                    .append(upload_img)
                             )
                            .append($("<div>").addClass("info")
                                    .append($("<h3>").append("Update Profile Picture"))
                                    .append($("<p>").append("Click or Drop Image Here"))
                                    )
                            );
            }
            this.getUploadArea = function (no,dragAndDropFiles,form_name,file){
                    var diva =$("<div></div>").addClass("box-body");
                    var div =$("<div></div>").addClass("uploadArea").attr({'id':dragAndDropFiles}).css({'padding':'0px','border':'none'});
                    div.append(that.getUpdateImgBox())
                    div.append($("<form></form>").attr({'name':form_name,'id':form_name,'enctype':'multipart/form-data'}).append($("<input/>").attr({'type':'file','data-maxwidth':'620','data-maxheight':'620','name':'file[]','id':file}).css({'width':'0px','height':'0px','overflow':'hidden'})));
                    diva.append(div);
                    return diva;
                }
            
            
            this.getRow = function (data){
                config.getModel("ProfileConfig").setContact_no(data.contact_no)
                var img;
                try {
                    img=config.getModel("ProfileConfig").getProfilePic("user-image").addClass("profile-user-img img-responsive img-circle");
                }catch (e){
                    img = $('<img />', { 
                            src: '../../dist/img/avatar.png',
                            alt: 'User Image'
                        });
                    img.addClass("img-circle");
                }
                ul_list=$("<ul>").addClass("list-group list-group-unbordered").css({'margin-bottom':'-11px'});
                return $("<div>").addClass("row")
                   .append($("<div>").addClass("col-md-3").attr({'id':'profile_box'})
                        .append($("<div>").addClass("box")
                            .append($("<div>").addClass("box-body box-widget widget-user")
                                .append($("<div>").addClass("widget-user-header bg-black").css({"background":"url('../../dist/img/cover_page.jpg') no-repeat center","background-size":"100%"}))
                                .append($("<div>").addClass("widget-user-image")
                                .append(img)
                                .append($("<h3>").addClass("profile-username text-center").append(data.user_name))
                                )
                                .append($("<div>").addClass("box-footer").css({'margin-top':'66px'})
                                    .append($("<div>").addClass("row")
                                        .append($("<div>").addClass("col-sm-4 col-md-4 col-xs-4 border-right")
                                            .append($("<div>").addClass("description-block")
                                                .append($("<h5>").addClass("description-header").append("3,200"))
                                                .append($("<span>").addClass("description-text").append("Friend"))
                                            ))
                                        .append($("<div>").addClass("col-sm-4 col-md-4 col-xs-4 border-right")
                                            .append($("<div>").addClass("description-block")
                                                .append($("<h5>").addClass("description-header").append("3,200"))
                                                .append($("<span>").addClass("description-text").append("Follower"))
                                            ))
                                        .append($("<div>").addClass("col-sm-4 col-md-4 col-xs-4")
                                            .append($("<div>").addClass("description-block")
                                                .append($("<h5>").addClass("description-header").append("35"))
                                                .append($("<span>").addClass("description-text").append("Following"))
                                            ))
                                    )
                                .append($("<div>").addClass("row")
                                    .append(ul_list
                                        .append($("<li>").addClass("list-group-item list_profile_page list_profile_page")
                                            .append($("<b>")
                                                .append($("<span>").addClass("glyphicon glyphicon-calendar ")))
                                            .append($("<a>")
                                                .append("&nbsp;&nbsp;&nbsp;"+data.dob)
                                            )
                                        )    
                                        .append($("<li>").addClass("list-group-item list_profile_page list_profile_page")
                                            .append($("<b>")
                                                .append($("<span>").addClass("glyphicon glyphicon-envelope")))
                                            .append($("<a>")
                                                .append("&nbsp;&nbsp;&nbsp;"+data.email)
                                            )
                                        )    
                                        .append($("<li>").addClass("list-group-item list_profile_page list_profile_page")
                                            .append($("<b>")
                                                .append($("<span>").addClass("glyphicon glyphicon-phone ")))
                                            .append($("<a>")
                                                .append("&nbsp;&nbsp;&nbsp;"+data.contact_no)
                                            )
                                        )
                                    )))
                            )
                        )
                        .append($("<div>").addClass("box")
                            .append($("<div>").addClass("box-header with-border")
                                .append($("<h3>").addClass("box-title").append("Our Test"))
                            )    
                            .append($("<div>").addClass("box-body box-profile")
                                .append($("<strong>")
                                    .append($("<i>").addClass("fa fa-book margin-r-5"))
                                    .append($("<a>").css({'cursor':'pointer'})
                                            .append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start Practice Test")
//                                            .append($("<span>").addClass("badge").attr({'id':'notanswer'}).append("2"))
                                        ).click(function (){
                                          config.getController("ProfileMgr").loadContentWrapperAssesmentVgr("demo");  
                                        })
                                )
                                .append($("<hr>"))
                                .append($("<strong>")
                                    .append($("<i>").addClass("fa fa-newspaper-o margin-r-5"))
                                    .append($("<a>").css({'cursor':'pointer'})
                                            .append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start Assessment Test")
//                                            .append($("<span>").addClass("badge").attr({'id':'notanswer'}).append("2"))
                                        ).click(function (){
                                          config.getController("ProfileMgr").loadContentWrapperAssesmentVgr("original");  
                                        })
                                )
                                .append($("<hr>"))
                                .append($("<strong>")
                                    .append($("<i>").addClass("fa fa-pie-chart margin-r-5"))
                                    .append($("<a>").css({'cursor':'pointer'})
                                            .append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;View Your Performance")
//                                            .append($("<span>").addClass("badge").attr({'id':'notanswer'}).append("2"))
                                        ).click(function (){
                                          config.getController("ProfileMgr").loadPerformanaceVgr();  
                                        })
                                )
                            )
                        )
                    )
                   .append($("<div>").addClass("col-md-6 ").attr({'id':'box_post'}).append($('<div class="box box-widget"> <div class="box-header with-border"> <div class="user-block"> <img class="img-circle" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <span class="username"><a href="#">Jonathan Burke Jr.</a></span> <span class="description">Shared publicly - 7:30 PM Today</span> </div><div class="box-tools"> <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read"> <i class="fa fa-circle-o"></i></button> <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button> <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> </div></div><div class="box-body"> <img class="img-responsive pad" src="../../dist/images/banner1.png" alt="Photo"> <p>I took this photo this morning. What do you guys think?</p><button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> <span class="pull-right text-muted">127 likes - 3 comments</span> </div><div class="box-footer box-comments"> <div class="box-comment"> <img class="img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <div class="comment-text"> <span class="username"> Maria Gonzales <span class="text-muted pull-right">8:03 PM Today</span> </span> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout </div></div><div class="box-comment"> <img class="img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <div class="comment-text"> <span class="username"> Luna Stark <span class="text-muted pull-right">8:03 PM Today</span> </span> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </div></div></div><div class="box-footer"> <form action="#" method="post"> <img class="img-responsive img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="Alt Text"> <div class="img-push"> <input type="text" class="form-control input-sm" placeholder="Press enter to post comment"> </div></form> </div></div><div class="box box-widget"> <div class="box-header with-border"> <div class="user-block"> <img class="img-circle" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <span class="username"><a href="#">Jonathan Burke Jr.</a></span> <span class="description">Shared publicly - 7:30 PM Today</span> </div><div class="box-tools"> <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read"> <i class="fa fa-circle-o"></i></button> <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button> <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> </div></div><div class="box-body"> <p>Far far away behind the word mountains far from the countries Vokalia and Consonantia there live the blind texts. Separated they live in Bookmarksgrove right at</p><p>the coast of the Semantics a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country in which roasted parts of sentences fly into your mouth.</p><div class="attachment-block clearfix"> <img class="attachment-img" src="../../dist/images/banner1.png" alt="Attachment Image"> <div class="attachment-pushed"> <h4 class="attachment-heading"><a href="http://www.lipsum.com/">Lorem ipsum text generator</a></h4> <div class="attachment-text"> Description about the attachment can be placed here. Lorem Ipsum is simply dummy text of the printing and typesetting industry... <a href="#">more</a> </div></div></div><button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> <span class="pull-right text-muted">45 likes - 2 comments</span> </div><div class="box-footer box-comments"> <div class="box-comment"> <img class="img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <div class="comment-text"> <span class="username"> Maria Gonzales <span class="text-muted pull-right">8:03 PM Today</span> </span> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </div></div><div class="box-comment"> <img class="img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <div class="comment-text"> <span class="username"> Nora Havisham <span class="text-muted pull-right">8:03 PM Today</span> </span> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters as opposed to using Content here content here making it look like readable English. </div></div></div><div class="box-footer"> <form action="#" method="post"> <img class="img-responsive img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="Alt Text"> <div class="img-push"> <input type="text" class="form-control input-sm" placeholder="Press enter to post comment"> </div></form> </div></div>'))
                        
                    ).append($("<div>").addClass("col-md-3").attr({'id':'box_ads'})
                        .append(that.getGrammer())
                        .append(that.getSponsored())
                    );
            }
            
            
            
            this.getRegistrationBoxBody = function (data){
                    
                    var row =$("<div>").addClass("box-body");
                    modelform =$("<form></form>").attr({'id':'rg_form'}).addClass("form-horizontal");
                    var dob;
                    try{
                        var d=data.dob.split('-');
                    dob=d[2]+"-"+d[1]+"-"+d[0];
                    }catch(e){
                        alert(e)
                    }
                    
                    
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Name"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'text','id':'user_name','placeholder':'Full name','name':'user_name','value':data.user_name}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-user form-control-feedback"))));
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Email"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'email','id':'email','placeholder':'Email','name':'email','value':data.email,'disabled':'true'}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-envelope form-control-feedback")))
                                );
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Date Of Birth"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'date','id':'dob','title':'Please select your Date of Birth','name':'dob','value':dob}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-calendar form-control-feedback")))
                                );
                                
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Contact No."))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'text','id':'contact_no','placeholder':'Contact No..','name':'contact_no','maxlength':'10','value':data.contact_no}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-phone form-control-feedback"))));
                    modelform.append($("<div>").addClass("form-group has-feedback ").attr({'id':'update_div'})
                            .append($("<div>").addClass("col-sm-1"))
                            .append($("<div>").addClass("col-sm-3").append($('<label id="error"></label>')))
                            .append($("<div>").addClass("col-sm-2")
                            .append($("<button></button>").addClass("btn btn-primary btn-block btn-flat").attr({'type':'button','id':'submit','name':'submit'}).append("Update").click(function (){
                                $.each(listeners, function(i){
                                    try {
                                        listeners[i].updUserProfile(modelform);
                                    }catch(e){}
                                });
                            }))));
                            row.append(modelform);
                        $.get("../../dist/js/jquery.validate.min.js").done(function() {
                            if(config.getModel("UserProfile")==null){
                                $.getScript("../model/UserProfile.js").done(function(){
                                        try {
                                            config.setModel("UserProfile",new $.UserProfile(config));
                                            config.getModel("UserProfile").setValidation();
                                        }catch(e){}
                                }).fail(function() {alert("Error: UserProfile.js Loading Problem");}); 
                            }else{
                                config.getModel("UserProfile").setValidation();
                            }
                        }).fail(function() {alert("Error: jquery.validate.min.js Loading Problem");}); 
                            
                    pass_form =$("<form></form>").attr({'id':'rg_form'}).addClass("form-horizontal");
                    pass_form.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Old Password"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'password','id':'password','placeholder':'Old Paasword','name':'password'}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-lock form-control-feedback"))));
                    pass_form.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("New Password"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'password','id':'newpassword','placeholder':'new Paasword','name':'newpassword'}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-lock form-control-feedback")))
                                );
                    pass_form.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Confirm password"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'password','id':'confirm_password','placeholder':'Confirm Paasword','name':'confirm_password'}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-lock form-control-feedback")))
                                );
                        pass_form.append($("<div>").addClass("form-group has-feedback ").attr({'id':'update_div'})
                            .append($("<div>").addClass("col-sm-1"))
                            .append($("<div>").addClass("col-sm-3").append($('<label id="error_password"></label>')))
                            .append($("<div>").addClass("col-sm-2")
                            .append($("<button></button>").addClass("btn btn-primary btn-block btn-flat").attr({'type':'button','id':'submit','name':'submit'}).append("Reset Password").click(function (){
                                $.each(listeners, function(i){
                                    try{
                                        listeners[i].updUpdPassword(pass_form);    
                                    }catch(e){}
                                });
                            }))));
                            if(config.getModel("UpdPassword")==null){
                                $.getScript("../model/UpdPassword.js").done(function(){
                                        try {
                                            config.setModel("UpdPassword",new $.UpdPassword(config));
                                            config.getModel("UpdPassword").setValidation();
                                        }catch(e){}
                                }).fail(function() {alert("Error: UserProfile.js Loading Problem");}); 
                            }else{
                                config.getModel("UserProfile").setValidation();
                            }
                    row.append($("<div>").addClass("box-group ").attr({'id':'accordion'})
                                    .append($("<div>").addClass("panel box")
                                        .append($("<div>").addClass("box-header")
                                            .append($("<h4>").addClass("box-title")
                                                .append($("<a>").addClass("collapsed").attr({'data-toggle':'collapse','data-parent':'#accordion','href':'#collapseOne','aria-expanded':'false'})
                                                    .append("Do you want to change your Password.")
                                                   )
                                            )
                                        )
                                        .append($("<div>").addClass("panel-collapse collapse").attr({'id':'collapseOne','aria-expanded':'false'}).css({'height':'0px'})
                                            .append($("<div>").addClass("box-body")
                                                .append(pass_form)
                                            )
                                        )    
                                    )
                                );
                    return $("<div>").addClass("col-md-6")
                            .append($("<div>").addClass("box box-widget")
                                .append($("<div>").addClass("box-header with-border")
                                    .append($("<div>").addClass("user-block")
                                        .append($("<h4>").append("Profile Details"))))
                                .append(row));
                }
                
                this.getUserDetails_model = function (){
                    return modelform;
                }
                this.getPassword_form = function (){
                    return pass_form;
                }
                this.getUl_list = function (){
                    return ul_list;
                }
                
                this.selLoadBegin =function (){
                    activity.append($("<center>").append(config.getLoadingData()));
                }
                
                this.selLoadFinish=function (){
                    activity.empty();
                }

                this.selLoadFail = function() { 
                    activity.append($("<center>").append("Server Doesn't Responde."));
                }
                
                this.selRemoveFail = function() { 
                    activity.empty();
                }
                
                this.loadContentWrapperProfile =function (){
                    $.each(listeners, function(i){
                        try {
                            listeners[i].selContentWrapperProfile();
                            return false;
                        }catch(e){alert(e)}
                    });
                }
                this.addListener = function(list){
                            listeners.push(list);
                }
                this.addContentWrapperProfile =function (data){
                    
                   activity.append(that.getRow(data));
                   settings.append(that.getSetting(data));
                          var obj = {
                            // Valid file formats
                            support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                            form: "demoFiler", // Form ID
                            dragArea: "dragAndDropFiles", // Upload Area ID
                            multiUpload:"multiUpload",
                            upload_img:upload_img,
                            url:"/mocktest_v1//Img",
                            parent_obj:that,
                            strlenght:25,
                            module:1
                            }
                            setTimeout(function (){
                                $.getScript("../model/UpdProfielImg.js").done(function() {config.setModel("UpdProfielImg",new $.UpdProfielImg(obj,config));}).fail(function(e) {alert(e);}); 
                            },200);
                          
                }                                                        
                
                this.setDisableButton =function (obj){
                    obj.prop('disabled', true);
                }
                
                this.setEnableButton =function (obj){
                    obj.prop('disabled', false);
                }
                
                this.setServerError =function (){
                    model_footer.find("#error_msg").append("Server doesn't Respond.");
                }
                
                this.removeServerError =function (){
                    model_footer.find("#error_msg").empty();
                }
                
                this.updLoadBegin =function (){
                    modelform.find("#update_div").find("#submit").prop('disabled', true);
                    modelform.find("#update_div").find("#submit").empty();
                    modelform.find("#update_div").find("#submit").append(config.getLoadingData());
                }
                
                this.updLoadFail = function() { 
                    modelform.find("#update_div").find("#submit").prop('disabled', false);
                    modelform.find("#update_div").find("#submit").empty();
                    modelform.find("#update_div").find("#submit").append("Update");
                    modelform.find("#error").empty();
                    modelform.find("#error").append("Error: Server doesn't Respond.")
                    modelform.find("#error").css({'display':'block','color':'red'});    
                }
                this.updLoadFinish = function() { 
                    modelform.find("#update_div").find("#submit").prop('disabled', false);
                    modelform.find("#update_div").find("#submit").empty();
                    modelform.find("#update_div").find("#submit").append("Update");
                    modelform.find("#error").empty();
                    modelform.find("#error").append("Your Profile Successfully Updated")
                    modelform.find("#error").css({'display':'block','color':'blue'});    
                }
                this.updPassLoadBegin =function (){
                    pass_form.find("#update_div").find("#submit").prop('disabled', true);
                    pass_form.find("#update_div").find("#submit").empty();
                    pass_form.find("#update_div").find("#submit").append(config.getLoadingData());
                }
                
                this.updPassLoadFail = function() { 
                    pass_form.find("#update_div").find("#submit").prop('disabled', false);
                    pass_form.find("#update_div").find("#submit").empty();
                    pass_form.find("#update_div").find("#submit").append("Update");
                    pass_form.find("#error_password").empty();
                    pass_form.find("#error_password").append("Error: Server doesn't Respond.")
                    pass_form.find("#error_password").css({'display':'block','color':'red'});    
                }
                this.updPassLoadFinish = function() { 
                    pass_form.find("#update_div").find("#submit").prop('disabled', false);
                    pass_form.find("#update_div").find("#submit").empty();
                    pass_form.find("#update_div").find("#submit").append("Update");
                    pass_form.find("#error_password").empty();
                    pass_form.find("#error_password").append("Your Password Successfully Updated")
                    pass_form.find("#error_password").css({'display':'block','color':'blue'});    
                }
            
        },
        ContentWrapperProfileVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selContentWrapperProfile : function() { },
			updContentWrapperProfile : function() { }
		}, list);
	},
        UserProfileVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			updUserProfile : function() { }
		}, list);
	},
        UpdPasswordVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			updUserProfile : function() { }
		}, list);
	}
});