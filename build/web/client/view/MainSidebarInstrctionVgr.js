jQuery.extend({
	MainSidebarInstrctionVgr: function(config){
		var that = this;
                var no=false;
		var listeners = new Array();
                
                this.setMainSidebarInstrction = function (anchor){
                    config.getView("MainSidebarVgr").removeSidebarMenu();
                    config.getView("MainSidebarVgr").setSidebarMenu(that.getSideBarMenuList("Select Language","fa-language"));
                }
                this.getSideBarMenuList = function (title,icon,callbeak){
                    return $("<li>").addClass("treeview").css({'cursor':'pointer'})
                                    .append($("<a>").append($("<i>").addClass("fa "+icon))
                                        .append($("<span>").append(title))
                                        .append($("<i>").addClass("fa fa-angle-left pull-right"))
                                    ).append($("<ul>").addClass("treeview-menu").css({'display':'none'})
                                        .append(that.getList("fa fa-check","English",that.setInstruncation_english))
                                        .append(that.getList("fa fa-check","Hindi",that.setInstruncation_hindi))
                                    );
                }
                this.loadAssesment_Instrction = function (){
                    if(config.getView("ContentWrapperAssesmentVgr")==null){
                        $.getScript("../view/ContentWrapperAssesmentVgr.js").done(function() {
                            config.setView("ContentWrapperAssesmentVgr",new $.ContentWrapperAssesmentVgr(config,config.getView("ContentWrapperVgr")));
                        }).fail(function() {alert("Error:problem in ContentWrapperAssesmentVgr.js file ");}); 
                     }else{
                         config.getView("ContentWrapperAssesmentVgr").init();
                     }          
                }      
                this.getList = function (icon,title,callbeak){
                    return $("<li>").append($("<a>").append($("<i>").addClass("fa "+icon)).append(title)).click(function (){
                                        callbeak();
                    });
                }
                this.setInstruncation_english = function (){
                    if(config.getIns()==1){
                        config.getView("ContentWrapperInstruncation1EnglishVgr").init();
                    }else{
                        config.getController("InstrctionMgr").loadContentWrapperInstruncation2EnglishVgr();
                    }
                }
                this.setInstruncation_hindi = function (){
                    if(config.getIns()==1){
                        config.getController("InstrctionMgr").loadContentWrapperInstruncation1HindiVgr();
                    }else{
                        config.getController("InstrctionMgr").loadContentWrapperInstruncation2HindiVgr();
                    }
                    
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	MainSidebarInstrctionVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
