jQuery.extend({
	MainSidebarTestVgr: function(config){
		var that = this;
                var no=false;
		var listeners = new Array();
                var ul_obj_a ={};
                this.setMainSidebarTest = function (anchor){
                    config.getView("MainSidebarVgr").removeSidebarMenu();
                }
                this.setSidebarMenu = function (data){
                    var a=that.getSideBarMenuList(data.section_name,"fa fa-newspaper-o",data.section_id);
                    config.getView("MainSidebarVgr").setSidebarMenu(a);
                    ul_obj_a[data.section_id]=a;
                }
                this.setQuestion =function (id){
                    config.getModel("Question").setQuestion(id);
                }
                
                this.getSideBarMenuList = function (title,icon,id){
                    return $("<li>").addClass("treeview").css({'cursor':'pointer'})
                                    .append($("<a>").append($("<i>").addClass("fa "+icon))
                                        .append($("<span>").append(title))
                                    ).click(function (){
                                        that.setQuestion(id);
                                    })
                                    .append($("<ul>").addClass("treeview-menu")
                                        .append(that.getList("Answered","successa","0"))
                                        .append(that.getList("Not Answered","notanswera","0"))
                                        .append(that.getList("Mark for Review","reviewa","0"))
                                        .append(that.getList("Not Visited","noreviewa","0","badge pull-right"))
                                    );
                }
                this.getList = function (title,id,c,cls){
                    if(cls==undefined){
                        return $("<li>").append($("<a>").append(title+"&nbsp;").append($("<span>").addClass("badge").attr({'id':id}).append(c)));
                    }else{
                        return $("<li>").append($("<a>").append(title+"&nbsp;").append($("<span>").addClass(cls).css({'margin-right':'1px'}).attr({'id':id}).append(c)));
                    }
                    
                }
                this.updateLi =function (section_id,id,count){
                    ul_obj_a[section_id].find("#"+id).empty();
                    ul_obj_a[section_id].find("#"+id).append(count);
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	MainSidebarTestVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
