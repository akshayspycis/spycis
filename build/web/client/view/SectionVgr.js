jQuery.extend({
	SectionVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                this.setSection = function(){
                        that.loadSection();
		}
                
                
                
                this.addSection =function (data){
                   config.getView("MainSidebarVgr").setSideBarMenu(data);
                   config.getView("ContentWrapperVgr").setSection(data);
                }                                                        
                this.selLoadBegin =function (){
                    //tbody.append(config.getLoadingData());
                }
                
                this.selLoadFinish=function (){
                    //tbody.empty();
                }
                
                this.selLoadFail = function() { 
                    //tbody.append("<ts>")
                }
            
                this.loadSection =function (){
                    $.each(listeners, function(i){
                        listeners[i].selSection();
                    });
                } 

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	SectionVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                	selSection : function() { }
		}, list);
	}
});
