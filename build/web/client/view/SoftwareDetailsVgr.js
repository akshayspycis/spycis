jQuery.extend({
	SoftwareDetailsVgr: function(config,kk){
		var that = this;
		var listeners = new Array();
                var software_about_box_body;
                var software_about_box_body_tab_pane;
                var nav_tab_list;
                var error;
                var form ;
                var form_otp ;
                var upload_img;
                var email_chack=false ;
                var contact_no_chack=false ;
                var question_mark ;
                aaa={};
                var software_id;
                
                var module=1;
                var software_module_id;
                var formula_icon;
                var formula_button;
                var formula_text_box;
                var next;
                var back;
                var test_type;
                var section_order;
                this.getSoftwareDetails = function(user_id,software_module_id){
                    software_module_id=software_module_id;
                    $("body").find("#hidden").find("#error_msg").remove();
                    var div=$("<div>");
                        div.append(that.getBoxbody());
                        if(software_module_id!=11){
                            that.setGeneralDetails();
                        }else{
                            gen={}
                            gen["contact_no"]=config.getModel("ProfileConfig").getContact_no();
                            software_id=config.getModel("ProfileConfig").getSoftwareDetails()["1"]["software_id"];
                            software_about_box_body_tab_pane=$("<div>").addClass("tab-pane active").attr({'id':'about'});
                            software_about_box_body.append(software_about_box_body_tab_pane);
                            software_about_box_body.append(that.getBoxFooter());
                            module=2;
                            that.genrateOTP();
                            that.setOTPDtails();
                        }
                        
                        $.fn.modal.Constructor.prototype.enforceFocus = function () {
                            modal_this = this
                            $(document).on('focusin.modal', function (e) {
                            if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                            // add whatever conditions you need here:
                            &&
                            !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
                            modal_this.$element.focus()
                            }
                            })
                        };
                        return div;
		}
                this.setGeneralDetails = function (){
                  //software_about_box_body.empty();
                  error=$("<label>").attr({'id':'error'}).addClass("col-sm-12 control-label text-red")
                  form =$("<form>").addClass("form-group")
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-3 control-label").append("Organisation Name"))
                                            .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'orgenisation_name','name':'orgenisation_name','placeholder':'Orgenisation Name'})))
                                              ))
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-3 control-label").append("Organisation Email"))
                                            .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'email','id':'email','name':'email','placeholder':'Organisation Email'})
                                                .focusout(function (){
                                                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                                    var txt=$(this).val();
                                                       email_chack=regex.test(txt); // has a digit
                                                       error.empty();
                                                       if(!email_chack){
                                                           error.append("The password should meet some minimum requirements:<br>"+
                                                            "one no space between character<br>"+
                                                            "at least one lower-case character<br>"+
                                                            "at least one digit");
                                                       }
                                                }))
                                                )
                                              ))
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-3 control-label").append("Organisation Contact&nbsp;&nbsp;").append($("<i>").addClass("fa  fa-question-circle").attr({'data-toggle':'tooltip','title':'This Contact for use OTP Autentication'})))
                                            .append($("<div>").addClass("col-sm-9")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'contact_no','name':'contact_no','placeholder':'Contact No..','maxlength':'10'})
                                                .focusout(function (){
                                                        
                                                    try {
                                                        var con=parseInt($(this).val())
                                                        contact_no_chack=$(this).val().length<10; // has a digit
                                                    }catch(e){
                                                        contact_no_chack= true;
                                                    }
                                                    error.empty();
                                                       if(contact_no_chack){
                                                           error.append("Your Contact must be at least 10 characters long");
                                                       }
                                                })
                                                
                            ))
                                              ))
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append(error)));
                  
                    software_about_box_body_tab_pane=$("<div>").addClass("tab-pane active").attr({'id':'about'}).append(form)
                    software_about_box_body.append(software_about_box_body_tab_pane);
                    software_about_box_body.append(that.getBoxFooter());
                }
                
                this.setNavTabs= function (){
                    nav_tab_list=$("<ul>").addClass("nav nav-tabs")
                                .append($("<li>").addClass("active").append($("<a>").attr({'href':'#about','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa fa-user")).append("&nbsp;&nbsp;About")))   
                                .append($("<li>").css({'pointer-events':'none'}).append($("<a>").attr({'href':'#profile','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa fa-image")).append("&nbsp;Profile")))   
                                .append($("<li>").css({'pointer-events':'none'}).append($("<a>").attr({'href':'#otp','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa fa-mobile")).append("&nbsp;OTP message")))   ;
                    return nav_tab_list;
                }
                
                this.setProfileDetails = function (){
                            nav_tab_list.find("li:nth-child(1)").css('pointer-events','none');
                            nav_tab_list.find("li:nth-child(1)").removeClass("active");
                            nav_tab_list.find("li:nth-child(2)").css('pointer-events','auto');
                            nav_tab_list.find("li:nth-child(2)").addClass("active");
                            software_about_box_body_tab_pane.empty()
                            software_about_box_body.find(".box-footer").find("button").prop('disabled', true);
                            software_about_box_body_tab_pane.append($("<div>").addClass("col-md-3"))
                            var temp=$("<div>").addClass("col-md-6").append(config.getLoadingData());
                            software_about_box_body_tab_pane.append(temp)
                    if(config.getModel("SoftwareImg")==null){
                        $.getScript("../model/SoftwareImg.js").done(function() {
                            temp.empty();
                            temp.append(that.getUploadArea(180,"dragAndDropFiles11","demoFiler11","multiUpload11"));
                            var obj = {
                            // Valid file formats
                            support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                            form: "demoFiler11", // Form ID
                            dragArea: "dragAndDropFiles11", // Upload Area ID
                            multiUpload:"multiUpload11",
                            upload_img:upload_img,
                            url:"/mocktest_v1/ImgSoftwareImgVgr",
                            parent_obj:that,
                            strlenght:25,
                            software_id:software_id,
                            module:1
                            }
                            config.setModel("SoftwareImg",new $.SoftwareImg(obj,config));
                        }).fail(function(e) {alert(e);}); 
                    }else{
                        temp.empty();
                        temp.append(that.getUploadArea(180,"dragAndDropFiles11","demoFiler11","multiUpload11"));
                    }
                }
                
                this.setOTPDtails = function (){
                    software_about_box_body.find(".box-footer").find("button").prop('disabled', false);
                    nav_tab_list.find("li:nth-child(1)").css('pointer-events','none');
                    nav_tab_list.find("li:nth-child(1)").removeClass("active");
                    nav_tab_list.find("li:nth-child(2)").css('pointer-events','none');
                    nav_tab_list.find("li:nth-child(2)").removeClass("active");
                    nav_tab_list.find("li:nth-child(3)").css('pointer-events','auto');
                    nav_tab_list.find("li:nth-child(3)").addClass("active");
                    form_otp =$("<form>").addClass("form-group")
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append($("<label>").addClass("col-sm-12 control-label").append("Please check your phone for a text message with your otp code. Your code is 6 characters in lenght."))
                                            .append($("<div>").addClass("col-sm-12").css('height','30px'))
                                            .append($("<div>").addClass("col-sm-3")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'otp','name':'otp','placeholder':'######'})))
                                              )
                                                .append($("<label>").addClass("col-sm-4 control-label").append("We Sent your code to :").append($("<br>")).append("+91 "+gen["contact_no"]))
                                        )
                                  .append($("<div>").addClass("form-control-group col-lg-12")
                                        .append($("<p>"))
                                        .append($("<p>"))
                                        .append($("<div>").addClass("form-group")
                                            .append(error)));
                    software_about_box_body_tab_pane.empty();
                    software_about_box_body_tab_pane.append(form_otp)
                }
                
                
                this.getUploadArea = function (no,dragAndDropFiles,form_name,file){
                    var diva =$("<div>").addClass("box-body");
                    var div =$("<div>").addClass("uploadArea").attr({'id':dragAndDropFiles}).css({'padding':'0px','border':'none'});
                    div.append(that.getUpdateImgBox());
                    div.append($("<form>").attr({'name':form_name,'id':form_name,'enctype':'multipart/form-data'}).append($("<input/>").attr({'type':'file','data-maxwidth':'620','data-maxheight':'620','name':'file[]','id':file}).css({'width':'0px','height':'0px','overflow':'hidden'})));
                    diva.append(div);
                    return diva;
                }
                
                this.getUpdateImgBox = function (){
                
                    upload_img = $('<img />', { 
                            src: '../../dist/img/soft_pic.PNG'
                        }).css({'width':'225px','height':'210px'});
                return $("<div>").addClass("ih-item square effect13 left_to_right").css({'overflow':'hidden'})
                        .append($("<a>")
                            .append($("<div>").addClass("img")
                                    .append(upload_img)
                             ).append($("<div>").addClass("info")
                                    .append($("<h3>").append("Upload Software Profile Picture"))
                                    .append($("<p>").append("Click or Drop Image Here"))
                                    ));
            }
            
            
            
                
                this.getBoxbody = function (){
                    software_about_box_body=$("<div>").addClass("nav-tabs-custom").append(that.setNavTabs());
                    return software_about_box_body;
                }
                
                this.getBoxFooter =function (){
                    var div = $("<div>").addClass("box-footer clearfix no-border");
                     next=$("<button>").addClass("btn btn-block btn-default").attr({'data-toggle':'modal','data-target':'myModal','type':'button'})
                                    .click(function ()
                                        {
                                            switch(module){
                                                case 1:
                                                    if(that.checkValidation()){
                                                        that.setGneInObj();
                                                    }
                                                break;
                                                case 2:
                                                    if(that.checkOTPValidation()){
                                                        that.setOTPInOBj();
                                                        $(this).prop('disabled', false);
                                                        //module++;
                                                    }
                                                break;
                                            }
                                        })
                                                .append("Next");               
                            div.append($("<div>").addClass("col-md-3 col-xs-6 pull-right").css({'margin-top':'10px'}).append(next));
                        return div;
                }
                
                
                this.setGneInObj = function (){
                    gen={}
                    form.find(":input").each(function() {
                            if($(this).attr("name")=="orgenisation_name"){
                                gen["orgenisation_name"] = $(this).val();
                            }
                            if($(this).attr("name")=="email"){
                                gen["email"] = $(this).val();
                            }
                            if($(this).attr("name")=="contact_no"){
                                gen["contact_no"] = $(this).val();
                            }
                    });
                        gen["user_id"] = config.getModel("ProfileConfig").getUser_id();
                        gen["software_name_id"] ="1"; 
                        that.insSoftwareDetails();
                }
                this.insSoftwareDetails =function (){
                        $.ajax({
                            url: config.getU()+"/InsSoftwareDetailsSvr",
                            type: 'POST',
                            data: JSON.stringify(gen),
                            success: function(data)
                            {
                                if(data.trim()=="error"){
                                    error.empty();
                                    error.append("Server doesn't Respond.");
                                }else{
                                    module++;
                                    software_id=data.trim();
                                    that.setProfileDetails();
                                    
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError)
                            }
                        });
                }
                
                
                this.checkValidation = function (){
                    var msg="" ;
                    form.find(":input").each(function() {
                            if($(this).attr("name")=="orgenisation_name"){
                                if($(this).val()==""){
                                    msg='Orgenisation Name';
                                    return false;
                                }
                            }
                            if($(this).attr("name")=="email"){
                                if($(this).val()=="" && !email_chack){
                                    msg='Email';
                                    return false;
                                }
                            }
                            if($(this).attr("name")=="contact_no" ){
                                if($(this).val()=="" && contact_no_chack){
                                    msg='Contact No';
                                    return false;
                                }
                            }
                        });
                        if(msg==""){
                            return true;
                        }else{
                            $("body").find("#hidden").find("#error_msg").remove();
                            msg= "Please "+msg+" should not blank.";
                            error.empty();
                            error.append(msg);
                            return false;
                        }
                }
                
                this.checkOTPValidation = function (){
                    var msg="" ;
                        form_otp.find(":input").each(function() {
                                if($(this).attr("name")=="otp" ){
                                    if($(this).val()==""){
                                        msg="Please Provide the OTP code";
                                    }else{
                                        var otp_no_chack=false;
                                        try {
                                            var con=parseInt($(this).val())
                                            otp_no_chack=$(this).val().length!=6; // has a digit
                                        }catch(e){
                                            otp_no_chack= true;
                                        }
                                        if(otp_no_chack){
                                           msg="Please Provide Valid Otp code.";
                                        }
                                    }
                                }
                        });
                        if(msg==""){
                            return true;
                        }else{
                            $("body").find("#hidden").find("#error_msg").remove();
                            error.empty();
                            error.append(msg);
                            return false;
                        }
                }
                
                
                
                this.setOTPInOBj = function (){
                    var otp={};
                    error.empty();
                    form_otp.find(":input").each(function() {
                                if($(this).attr("name")=="otp" ){
                                    otp["otp"]=$(this).val();
                                }
                        });
                    otp["software_id"]=software_id;
                    that.sendOTP(otp);
                }
                this.sendOTP =function (otp){
                        $.ajax({
                            url: config.getU()+"/SendOTPSvr",
                            type: 'POST',
                            data: JSON.stringify(otp),
                            success: function(data)
                            {
                                if(data.trim()==false){
                                    error.empty();
                                    error.append("Server doesn't Respond.");
                                }else{
                                    var software_details=config.getModel("ProfileConfig").getSoftwareDetails()
                                    if(software_details==null){
                                        software_details={}
                                    }
                                    software_details[gen['software_name_id']]={}
                                    software_details[gen['software_name_id']]['software_id']=software_id;
                                    software_details[gen['software_name_id']]['orgenisation_name']=gen['orgenisation_name'];
                                    software_details[gen['software_name_id']]['tag_line']=null;
                                    config.getModel("ProfileConfig").setSoftwareDetails(JSON.stringify(software_details));
                                    var str=decodeURIComponent(localStorage.getItem("usaassassaesras_sasasdaseastaasiasls"));
                                    var obj=JSON.parse(str);
                                    obj["software_details"]=software_details;
                                    localStorage.setItem("usaassassaesras_sasasdaseastaasiasls",encodeURIComponent(JSON.stringify(obj)));
                                   $('#myModal').modal('toggle');
                                   config.getView("HeaderProfileVgr").getMock().empty() ;
                                   config.getView("HeaderProfileVgr").getMock().off() ;
                                   config.getView("HeaderProfileVgr").getMock().append("&nbsp;&nbsp;&nbsp;Open&nbsp;&nbsp;&nbsp;").click(function (){
                                       alert("ok")
                                   }) ;
                                   config.getView("HeaderProfileVgr").getMock().parent().find("p").empty() ;
                                   config.getView("HeaderProfileVgr").getMock().parent().find("p")
                                           .append($("<i>").addClass("fa fa-users").attr({'data-toggle':'tooltip','title':'Members Limit'}))
                                           .append("&nbsp;&nbsp;20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") 
                                           .append($("<i>").addClass("fa fa-user").attr({'data-toggle':'tooltip','title':'Add Member'}))
                                           .append("&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") 
                                           .append($("<i>").addClass("fa fa-user").attr({'data-toggle':'tooltip','title':'Vacant'}))
                                           .append("&nbsp;20") 
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError+" error")
                            }
                        });
                }
                
                this.genrateOTP =function (){
                    var otp={};
                    otp["software_id"]=software_id;
                    otp["contact_no"]=config.getModel("ProfileConfig").getContact_no()
                        $.ajax({
                            url: config.getU()+"/GenrateOTPSvr",
                            type: 'POST',
                            data: JSON.stringify(otp),
                            success: function(data)
                            {
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError+" error")
                            }
                        });
                }

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	SoftwareDetailsVgrVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insSoftwareDetailsVgr : function(from){},
			selSoftwareDetailsVgr : function() { },
                        delSoftwareDetailsVgr : function(id) { },
                        getSoftwareDetailsVgr : function(id) { },
                        updSoftwareDetailsVgr : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
