jQuery.extend({
	ContentWrapperInstruncation1EnglishVgr: function(config,per){
            var that=this;
            this.init = function (){
                per.setHeaderTitle("Online Assessment Portel");
                per.setBreadCrumbName("fa-dashboard"," Instructions");
                per.removeBoxHeader();
                per.removeBoxbody();
                per.setBoxHeaderTitle($("<h3>").addClass("box-title").css({'line-height':':24px;'}).append("Please read the following instructions carefully"));
                per.setBoxBodyContent($('<h4><b>General Instructions:</b></h4>questions</li><li>The clock has been set at the server and the countdown timer at the top right corner of your screen will display the time remaining for you to complete the exam. When the clock runs out the exam ends by default - you are not required to end or submit your exam. </li><li>The question palette at the right of screen shows one of the following statuses of each of the questions numbered: </li></ol><h4><b>Navigation Question:</b></h4><ol class="instruction"><li>Total of 30 minutes duration will be given to attempt all the questions</li><li>The clock has been set at the server and the countdown timer at the top right corner of your screen will display the time remaining for you to complete the exam. When the clock runs out the exam ends by default - you are not required to end or submit your exam. </li><li>The question palette at the right of screen shows one of the following statuses of each of the questions numbered: </li></ol>'));
                per.setBoxFooter(that.getFooter());
            }
            this.getFooter = function (){
                return $("<center>").append($("<a>").addClass("next").append($("<b>").append("Next")).css({'cursor':'pointer'}).click(function (){
                    config.setIns(2);
                    config.getController("InstrctionMgr").loadContentWrapperInstruncation2EnglishVgr();
                }));
            }
        }
});