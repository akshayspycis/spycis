jQuery.extend({
	HeaderTestVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
                this.getHeaderTest = function(){
                        return $("<ul>").addClass("nav navbar-nav okok")
                                .append(that.getTestTime())
                                .append($("<li>").addClass("dropdown user user-menu")
                                    .append($("<a>").addClass("dropdown-toggle").css({'cursor':'pointer'})
                                            .append(config.getModel("ProfileConfig").getProfilePic("user-image"))    
                                            .append($("<span>").addClass("hidden-xs").append(config.getModel("ProfileConfig").getFirstName()))
                                            .attr({'data-toggle':'dropdown'})
                                        )
                                    .append($("<ul>").addClass("dropdown-menu")
                                        .append($("<li>").addClass("user-header")
                                            .append(config.getModel("ProfileConfig").getProfilePic("img-circle"))
                                            .append($("<p>").append(config.getModel("ProfileConfig").getFirstName())
                                            .append($("<small>").append("Candidate Id  V"+config.getModel("ProfileConfig").getUser_id())))
                                        )
                                        .append($("<li>").addClass("user-footer")
                                            .append($("<div>").addClass("pull-right")
                                                    .append($("<button>").addClass("btn btn-default btn-flat").append("Profile")
                                                    .click(function(){
                                                        
                                                    })
                                                    )
                                             )
                                        )
                                     )
                                )
                                .append(that.getControlSidebar_Btn());
		}

                this.getTestTime =function (){
                    return $("<li>").append($("<span>").addClass("timer").append("Time Left :").append($("<b>").attr({'id':'hm_timer'})))
                }
                this.getControlSidebar_Btn =function (){
                    var icon=$("<i>").addClass("fa fa-edit").css({'font-size':'18px'});
                    return $("<li>").append($("<a>").addClass("active").attr({'data-toggle':'control-sidebar','title':'Open the sidebar'})
                                        .append(icon).click(function (){
                                    if(config.getView("ControlSidebarVgr").getAside().hasClass("control-sidebar-open")){
                                       config.getView("ControlSidebarVgr").getAside().removeClass("control-sidebar-open");
                                        icon.removeClass("fa-edit");
                                    }else{
                                        config.getView("ControlSidebarVgr").getAside().addClass("control-sidebar-open");
                                        icon
                                    }
                    }));
                }
                this.addListener = function(list){
                            listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	HeaderTestVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
