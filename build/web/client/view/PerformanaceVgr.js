jQuery.extend({
	PerformanaceVgr: function(config,pre){
		var that = this;
		var row ;
		var demo ;
		var temp_obj ;
		var listeners = new Array();
                this.getRow = function (data){
                    var total = parseFloat(data.total).toFixed(2);
                    return $("<div>").addClass("col-lg-4 col-md-6 col-sm-6 col-xs-12")
                        .append($("<div>").addClass("box box-success").css({'border-bottom':'1px solid #B7B3B3','border-left':'1px solid #B7B3B3','border-right':'1px solid #B7B3B3'})
                                .append($("<div>").addClass("box-header")
                                        .append(that.getTestLogo())
                                        .append($("<span>").addClass("info-box-number").append(data.unique_test_key))
                                        .append($("<span>").addClass("info-box-text").append(data.test_name))
                                        .append($("<div>").addClass("box-tools pull-right")
                                            .append($("<button>").css({'margin-right':'5px'}).addClass("btn btn-default btn-sm").attr({'data-toggle':'tooltip','data-original-title':'View Summary'}).append($("<i>").addClass("fa fa-file-text-o"))
                                                    .click(function (){
                                                        that.loadResult(data.test_id,$(this),total);
                                                    }))
                                            .append($("<button>").css({'margin-right':'5px'}).addClass("btn btn-default btn-sm").attr({'data-toggle':'tooltip','data-original-title':'View Question'}).append($("<i>").addClass("fa fa-list-alt"))
                                                    .click(function (){
                                                        that.loadSolutionReport(data.test_id,$(this),data.test_name);
                                                    }))
                                            .append($("<button>").css({'margin-right':'5px'}).addClass("btn btn-default btn-sm").attr({'data-toggle':'tooltip','data-original-title':'View Ans Sheet'}).append($("<i>").addClass("fa fa-newspaper-o"))
                                                    .click(function (){
                                                        that.loadAnsSheet(data.test_id,$(this),data.test_name);
                                                    }))
                                            .append($("<button>").addClass("btn btn-default tn-sm").attr({'data-toggle':'tooltip','data-original-title':'View Ans Sheet'}).append($("<i>").addClass("fa fa-pie-chart"))
                                                    .click(function (){
                                                        that.loadChartSheet(data.test_id,$(this),data.test_name);
                                                    }))
                                            )    
                                        .append($("<div>").css({'color':'rgb(255, 0, 0)'})
                                            .append($("<label>").append("Total marks :"))
                                            .append($("<label>").addClass("pull-right").append(total))
                                            )
                                    )
                                );
                }
                this.getTestLogo =function (){
                    return $('<img />', { 
                        src: '../../dist/images/exam.gif',
                    });
                }
                this.addTest =function (data){
                   if(data.type!="demo"){
                        row.append(that.getRow(data));
                    }else{
                        demo.append(that.getRow(data));
                    }
                }                                                        
                this.removeFail = function() { 
                    row.empty();
                    demo.empty();
                }
                this.selLoadBegin =function (){
                   row.append($("<center>").append(config.getLoadingData()));
                   demo.append($("<center>").append(config.getLoadingData()));
                }
                this.selLoadFinish=function (){
                    row.empty();
                    demo.empty();
                }
                this.selLoadFail = function() { 
                    row.append("<center>Server doesn't respond.</center>");
                    demo.append("<center>Server doesn't respond.</center>");
                }
                this.removeFailTest = function() { 
                    temp_obj.empty();
                    temp_obj.append("Start Test");
                }
                this.selTestLoadBegin =function (){
                   temp_obj.empty();
                   temp_obj.append(config.getLoadingData());
                }
                this.selTestLoadFinish=function (){
                    temp_obj.empty();
                    temp_obj.append("Start Test");
                }
                this.selTestLoadFail = function() { 
                    temp_obj.empty();
                    temp_obj.append("You doesn't start Test before Date and time");
                }
                
                this.loadTest =function (){
                    that.removeFail();
                    $.each(listeners, function(i){
                        listeners[i].selTest();
                    });
                } 
                this.loadResult =function (test_id,obj,value){
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("ModelVgr")==null){
                        $.getScript("../../admin/view/ModelVgr.js").done(function(){
                            config.setView("ModelVgr",new $.ModelVgr(config));
                        that.loadResultSummery(test_id,obj,value);  
                        }).fail(function() {alert("Error :problem in ModelVgr.js file ");}); 
                    }else{
                        that.loadResultSummery(test_id,obj,value);
                    }
                } 
                
                this.loadSolutionReport =function (test_id,obj,value){
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("SolutionReportVgr")==null){
                        $.getScript("../view/SolutionReportVgr.js").done(function(){
                            config.setView("SolutionReportVgr",new $.SolutionReportVgr(config,config.getView("ContentWrapperVgr")));
                            config.getView("SolutionReportVgr").init(test_id,value);
                            obj.empty();
                            obj.append($("<i>").addClass("fa fa-print"));
                        }).fail(function() {alert("Error:004 problem in SolutionReportVgr.js file ");}); 
                     }else{
                            obj.empty();
                            obj.append($("<i>").addClass("fa fa-print"));
                            config.getView("SolutionReportVgr").init(test_id,value);
                     }
                } 
                
                this.loadAnsSheet =function (test_id,obj,value){
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("AnsSheetVgr")==null){
                        $.getScript("../view/AnsSheetVgr.js").done(function(){
                            config.setView("AnsSheetVgr",new $.AnsSheetVgr(config,config.getView("ContentWrapperVgr")));
                            config.getView("AnsSheetVgr").init(test_id,value);
                            obj.empty();
                            obj.append($("<i>").addClass("fa fa-print"));
                        }).fail(function() {alert("Error:004 problem in AnsSheetVgr.js file ");}); 
                     }else{
                            obj.empty();
                            obj.append($("<i>").addClass("fa fa-print"));
                            config.getView("AnsSheetVgr").init(test_id,value);
                     }
                } 
                
                this.loadChartSheet =function (test_id,obj,value){
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("ChartSheetVgr")==null){
                        $.getScript("../view/ChartSheetVgr.js").done(function(){
                            config.setView("ChartSheetVgr",new $.ChartSheetVgr(config,config.getView("ContentWrapperVgr")));
                            config.getView("ChartSheetVgr").init(test_id,value);
                            obj.empty();
                            obj.append($("<i>").addClass("fa fa-pie-chart"));
                        }).fail(function() {alert("Error:004 problem in ChartSheetVgr.js file ");}); 
                     }else{
                            obj.empty();
                            obj.append($("<i>").addClass("fa fa-pie-chart"));
                            config.getView("ChartSheetVgr").init(test_id,value);
                     }
                } 
                
                this.loadResultSummery = function(test_id,obj,value){
                        if(config.getView("ResultSummeryVgr")==null){
                        $.getScript("../../admin/view/ResultSummeryVgr.js").done(function(){
                            config.setView("ResultSummeryVgr",new $.ResultSummeryVgr(config));
                                var model = config.getView("ModelVgr").getModel("Result summery",config.getView("ResultSummeryVgr").getResultSummery(test_id,config.getModel("ProfileConfig").getUser_id(),value,"../../admin/"),"submit");
                            model.find(".modal-dialog").addClass("modal-lg");
                            
                                $.getScript("../../admin/model/ResultSummery.js").done(function() {
                                    config.setModel("ResultSummery",new $.ResultSummery(config));
                                    $.getScript("../../admin/controller/ResultSummeryMgr.js").done(function() {
                                        config.setController("ResultSummeryMgr",new $.ResultSummeryMgr(config,config.getModel("ResultSummery"),config.getView("ResultSummeryVgr")));
                                        obj.empty();
                                        obj.append($("<i>").addClass("fa fa-edit"));
                                        $("hidden").empty();
                                        $("hidden").append(model);
                                        model.find(".modal-footer").find("button").first().remove();
                                        model.modal('show');
                                        config.getView("ResultSummeryVgr").loadUserTestDetails();
                                    }).fail(function() {alert("Error :problem in TestUserListMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in TestUserListvgr.js file ");}); 
                        }).fail(function() {alert("Error:004 problem in ResultSummeryVgr.js file ");}); 
                     }else{
                            var model = config.getView("ModelVgr").getModel("Result summery",config.getView("ResultSummeryVgr").getResultSummery(test_id,config.getModel("ProfileConfig").getUser_id(),value),"submit");
                            model.find(".modal-dialog").addClass("modal-lg");
                            obj.empty();
                            obj.append($("<i>").addClass("fa fa-edit"));
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.find(".modal-footer").find("button").first().remove();
                            model.modal('show');
                            config.getView("ResultSummeryVgr").loadUserTestDetails();
                     }
                }
                
                

                this.addListener = function(list){
                            listeners.push(list);
                }
            this.init = function (){
                config.setTitle("User Performance");
                pre.setHeaderTitle("User Performance");
                pre.setBreadCrumbName("fa-newspaper-o","User Performance");
                pre.removeBoxHeader();
                pre.removeBoxbody();
                pre.setBoxHeaderTitle($("<h3>").addClass("box-title").css({'line-height':':24px;'}).append("List of Assesment Test"));
                pre.setBoxBodyContent(that.getTab());
                if(config.getModel("Performanace")==null){
                                $.getScript("../model/Performanace.js").done(function() {
                                    config.setModel("Performanace",new $.Performanace(config));
                                    $.getScript("../controller/PerformanaceMgr.js").done(function() {
                                        config.setController("PerformanaceMgr",new $.PerformanaceMgr(config,config.getModel("Performanace"),config.getView("PerformanaceVgr")));
                                        that.loadTest();
                                    }).fail(function() {alert("Error :problem in PerformanaceMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in Performanacevgr.js file ");}); 
                     }else{
                              that.loadTest();
                     }             
            }
            this.getTab = function (){
                row=$("<div>").addClass("row")
                demo=$("<div>").addClass("row")
                return $("<div>").addClass("nav-tabs-custom")
                            .append($("<ul>").addClass("nav nav-tabs")
                                .append($("<li>").addClass("active").append($("<a>").attr({'href':'#demo','data-toggle':'tab','aria-expanded':'true'}).append("Practice Test")))
                                .append($("<li>").append($("<a>").attr({'href':'#original','data-toggle':'tab','aria-expanded':'true'}).append("Examination Test")))
                             )
                            .append($("<div>").addClass("tab-content")
                                .append($("<div>").addClass("tab-pane active").attr({'id':'demo'})
                                    .append(demo))
                                .append($("<div>").addClass("tab-pane").attr({'id':'original'})
                                    .append(row))
                                
                                )
            }
        },
        	
	/**
	 * let people create listeners easily
	 */
	PerformanaceVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selTest:function() { }
		}, list);
	}
        
});