jQuery.extend({
	MocktestPublicCircleVgr: function(config,pre){
		var that = this;
                var software_id;
                var table;
                var table;
		var count=0 ;
		var listeners = new Array();
                this.getRow = function (data){
                    var button;
                    if(data.pay_by_user_status == undefined){
                        button=$("<a>").addClass("btn btn-success").append($("<i>").addClass("fa fa-plus")).append("&nbsp;&nbsp;Exam Circle").css({'display':'block'});
                        button.click(function (){
                            that.addExamCircle($(this),software_id,data.user_id);
                        });
                    }else{
                        if(data.pay_by_user_status=="true"){
                            button=$("<a>").attr("disabled","disabled").addClass("btn").append($("<i>").addClass("fa fa-check")).append("&nbsp;Accepted by User").css({'display':'block'});
                        }else{
                            button=$("<center>").addClass("")
                                    .append($("<a>").addClass("btn btn-default").append($("<i>").addClass("fa fa-check")).css({'webkit-border-radius': '0%','-moz-border-radius': '0%','border-radius':'0%','margin':'3px'}))
                                    .append($("<a>").addClass("btn btn-default").append($("<i>").addClass("fa fa-rupee")).append("&nbsp;Pay ").css({'webkit-border-radius': '0%','-moz-border-radius': '0%','border-radius':'0%','margin':'3px'}));
                            
//                  <button type="button" class="btn btn-default">Action</button>
//                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
//                    <span class="caret"></span>
//                    <span class="sr-only">Toggle Dropdown</span>
//                  </button>
                        }
                    }
                    
                    var img=$("<img>").attr({'src':'../../dist/img/user2-160x160.jpg'}).addClass("img-responsive img-circle").css({'height':'85px'});
                    return $("<div>").addClass("col-lg-2 col-xs-6 col-md-2 col-sm-3")
                            .append($("<center>").addClass("small-box").css({'box-shadow':'0 2px 10px rgba(0,0,0,.2)'})
                                .append($("<div>").addClass("inner")
                                    .append($("<button>").addClass("close").append($('<span aria-hidden="true">×</span>')))
                                    .append(img)
                                    .append($("<p>").append(data.user_name))
                                    )
                                .append(button))
                }
                
                this.setMocktestPublicCircle = function (id,ul){
                    software_id=id;
                    table=ul;
                    that.loadMocktestPublicCircle(1);
                }
                this.addMocktestPublicCircle =function (data){
                    table.append(that.getRow(data));
                }                                                        
                
                this.selLoadBegin =function (){
                   table.html($("<center>").append(config.getLoadingData()));
                }
                
                this.selLoadFinish=function (){
                    table.empty();
                }
                this.selLoadFail = function() { 
                    table.html("<center>Server doesn't respond.</center>");
                }
                
                this.selLoadBegin_accept =function (obj){
                    obj.html($("<center>").append(config.getLoadingFb()));
                }
                this.selLoadFinish_accept=function (obj){
                        obj.empty();
                        if(obj.hasClass("btn-primary")){
                            obj.removeClass("btn-primary");
                            obj.addClass("btn-success");
                            obj.append($("<i>").addClass("fa fa-plus")).append("&nbsp;&nbsp;Exam Circle")
                        }else{
                            obj.removeClass("btn-success");
                            obj.addClass("btn-default");
                            obj.append($("<i>").addClass("fa fa-mail-reply-all")).append("&nbsp;Wait for User")
                            obj.attr("disabled","disabled")
                        }
                        
                }
                this.selLoadFail_accept = function(obj) { 
                    obj.html("Retry");
                }
                
                this.addExamCircle=function (obj,software_id,user_id){
                    $.each(listeners, function(i){
                        listeners[i].addExamCircle(obj,software_id,user_id);
                    });
                } 
                this.acceptRequest=function (obj,software_id,user_id,user_request_id){
                    $.each(listeners, function(i){
                        listeners[i].acceptRequest(obj,software_id,user_id,user_request_id);
                    });
                } 
                
                this.loadMocktestPublicCircle =function (module){
                    $.each(listeners, function(i){
                        listeners[i].selMocktestPublicCircle(software_id,module);
                    });
                } 
                this.addListener = function(list){
                            listeners.push(list);
                }
            this.init = function (softwareid){
                software_id=softwareid;
                pre.setHeaderTitle("Public Circle");
                pre.setBreadCrumbName(" fa-users text-blue","Public Circle");
                pre.removeBoxHeader();
                pre.removeBoxbody();
                pre.setBoxHeaderTitle($("<div>").append($("<h3>").addClass("box-title").css({'line-height':':24px;'}).append("List of User")).append($("<div>").addClass("pull-right")
                                    .append($("<a>").addClass("btn btn-default").append("Select All&nbsp;&nbsp;").append($("<i>").addClass("fa fa-check")))
                                    .append($("<a>").addClass("btn btn-default").append($("<i>").addClass("fa fa-rupee")).append("&nbsp;Pay").css({'margin-left':'5px'}).click(function (){
                                        window.PUM.pay();
                                    }))));
                table=$("<div>");
                pre.setBoxBodyContent(table);
                that.loadMocktestPublicCircle();
            }
            
        },
        	
	/**
	 * let people create listeners easily
	 */
	MocktestPublicCircleVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selTest:function() { }
		}, list);
	}
        
});
