jQuery.extend({
	ContentWrapperInstruncation2HindiVgr: function(config,per){
            var that=this;
            var lang_dorp_down;
            this.init = function (){
                per.setHeaderTitle("Online Assessment Portel");
                per.setBreadCrumbName("fa-dashboard"," Instructions");
                per.removeBoxHeader();
                per.setBoxHeaderTitle($("<h3>").addClass("box-title").css({'line-height':':24px;'}).append("Please read the following instructions carefully"));
                per.removeBoxbody();
                per.setBoxBodyContent(that.getBody1());
                per.setBoxBodyContent(that.getBody2());
                per.setBoxBodyContent($("<div>").addClass("clear"));
                per.setBoxBodyContent($("<ul>").addClass("instruction").append('<li style="color:red">सभी प्रश्न अपने डिफ़ॉल्ट भाषा में दिखाई देगा कृपया ध्यान दें। इस भाषा पर बाद में एक विशेष प्रश्न के लिए बदला जा सकता है</li>'));
                per.setBoxBodyContent($("<p>").css({'margin-top':'50px'}));
                per.setBoxBodyContent($("<center>").append($("<a>").addClass("preinst").append($("<b>").append("<<&nbspPrevious")).click(function (){
                    config.setIns(1);
                    config.getController("InstrctionMgr").loadContentWrapperInstruncation1HindiVgr();
                })));
                per.setBoxBodyContent($("<p>").css({'margin-top':'50px'}));
                per.setBoxBodyContent($("<ul>").addClass("instruction").css({'list-style':'none','line-height':'26px'}).append($("<li>").append($("<label>").append($("<input>").attr({'type':'checkbox'})).append("&nbsp;मैंने पढ़ा है और निर्देश समझ लिया है। मेरे लिए आवंटित सभी कंप्यूटर हार्डवेयर उचित हालत में काम कर रहे हैं। मैंने सोचा कि मैं परीक्षा हॉल में मेरे साथ आदि मोबाइल फोन की तरह किसी भी निषिद्ध गैजेट / किसी भी निषिद्ध सामग्री नहीं ले जा रहा है कि इस बात से सहमत । मैं निर्देशों का पालन नहीं करने के मामले में, मैं परीक्षा लेने से अयोग्य घोषित कर दिया जाएगा सहमत"))));
                per.setBoxBodyContent($("<p>").css({'margin-top':'50px'}));
                per.setBoxFooter(that.getFooter());
            }
            
            this.getFooter = function (){
                return $("<center>").append($("<a>").addClass("next").append($("<b>").append("Begin Test&nbsp;>>")).css({'cursor':'pointer'}).click(function (){
                    config.setTest_Time(config.getModel("ContentWrapperAssesment").getTestObj(config.getTest_id()).time);
                    that.setLocalStorage("test_time",config.getTest_Time());
                    that.setLocalStorage("lang_id",config.getLanguage_id());
                    config.getView("MainVgr").setTest();
                }));
            }
            this.setLocalStorage = function (key,value){
                if(typeof(Storage) !== "undefined") {
                    localStorage.setItem(key,encodeURIComponent(value));
                } 
            }
            this.getBody1 = function (){
                return $("<div>").addClass("col-md-3").append($("<b>").append("अपने डिफ़ॉल्ट भाषा चुनें:"));
            }
            this.getBody2 = function (){
                return $("<div>").addClass("col-md-3").append(that.getLanguage_DropDown());
            }
            this.getLanguage_DropDown = function (){
                    lang_dorp_down =$("<div>").addClass("form-group");
                    lang_dorp_down.append($("<select>भाषा चुनिए:</select>").addClass("lang form-control").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var id = lang_dorp_down.find(".lang option:first").val();
                            $( ".lang option:selected" ).each(function() {
                              id = $( this ).val();
                            });
                           var obj=config.getModel("Question").loadQuestion(null,id);
                           config.getView("ContentWrapperVgr").setDirection(obj.direction_name);
                           config.getView("ContentWrapperVgr").setQuestion(obj.question_name);
                           config.getView("ContentWrapperVgr").setOption(obj.option_name);
                    });
                    return lang_dorp_down;
                }
            this.setLanguage =function (language){
                lang_dorp_down.find(".lang").append($("<option>").attr({'value':language.language_id}).append(language.language_name));
            }
        }
        
});