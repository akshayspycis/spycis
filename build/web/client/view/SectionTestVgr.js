jQuery.extend({
	SectionTestVgr: function(config){
		var that = this;
		var listeners = new Array();
		ul_obj ={};
                this.getUiList =function (data){
                    var a=$("<ul>").addClass("nav navbar-nav").append(
                            $("<li>").addClass("dropdown")
                            .append($('<a href="#" class="dropdown-toggle" data-toggle="dropdown">'+data.section_name+'<b class="caret"></b></a>'))
                            .append($("<ul>").addClass("dropdown-menu list-group")
                                .append(that.getLi("success","Answered",0))
                                .append(that.getLi("notanswer","Not Answered",0))
                                .append(that.getLi("review","Mark for Review ",0))
                                .append(that.getLi("noreview","Not Visited",0))
                            ).hover(function (){
                                    $(this).addClass("open");
                                },
                                function (){
                                    $(this).removeClass("open");
                                }
                            ).click(function (){
                                that.setQuestion(data.section_id);
                            }));
                    ul_obj[data.section_id]=a;
                    return a;
                }                                                        
                this.getLi =function (id,title,count){
                    return $("<li>").addClass("list-group-item")
                            .append($("<span>").addClass("badge").attr('id',id).append(count))
                            .append(title);
                }
                this.setQuestion =function (id){
                    config.getModel("Question").setQuestion(id);
                }
                this.updateLi =function (section_id,id,count){
                    ul_obj[section_id].find("#"+id).empty();
                    ul_obj[section_id].find("#"+id).append(count);
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	SectionTestVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                	selSectionTest : function() { }
		}, list);
	}
});
