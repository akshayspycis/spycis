jQuery.extend({
	Section: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
		function toArray(category_id,subcategory_id){
			var item = [];
                            try {
                                    $.each(cache[category_id][subcategory_id],function(j){
                                                sec_item = {}
                                                sec_item ["section_id"] = cache[category_id][subcategory_id][j].section_id;
                                                sec_item ["section_name"]= cache[category_id][subcategory_id][j].section_name;
                                                item.push(sec_item);
                                    });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json response from an
		 * ajax call
		 */
		function loadResponse(data,module){
                    var sec_data=data.section_details;
                    var category_id=data.category_id;
                    var subcategory_id=data.subcategory_id;
                    if(cache[category_id]==null){
                        cache[category_id]={};
                        cache[category_id][subcategory_id]=getSecCache(sec_data,{},module);
                    }else{
                        if(cache[category_id][subcategory_id]==null){
                            cache[category_id][subcategory_id]=getSecCache(sec_data,{},module);
                        }else{
                            cache[category_id][subcategory_id]=getSecCache(sec_data,cache[category_id][subcategory_id],module);
                        }
                    }
		}
                
                function getSecCache(sec_data,sec_cache,module){
                    $.each(sec_data, function(i){
                                sec_item_obj = {}
                                sec_item_obj["section_name"]= sec_data[i].section_name;
                                sec_item_obj["section_id"]= sec_data[i].section_id;
                                sec_cache[sec_data[i].section_id]=sec_item_obj;
                                that.sectionItemLoaded(sec_item_obj,module);
                    });
                    return sec_cache;
                }
                
                function updResponse(data){
                    cache[data.category_id][data.subcategory_id][data.section_id].section_name=data.section_name;
                    that.updItemLoaded(data);
		}
                function delResponse(section_id,category_id,subcategory_id){
                        delete cache[category_id][subcategory_id][section_id];
		}
		/**
		 * look in the cache for a single item
		 * if it's there, get it, if not
		 * ask the server to load it
		 */
		this.getSection = function(id,category_id,subcategory_id){
                            return cache[category_id][subcategory_id][id].section_name;
		}
                
                this.insSection =function (form){
                        that.insLoadBegin();
                        $.ajax({
                            url: config.getU()+"/InsSectionDetailsSvr",
                            type: 'POST',
                            dataType: 'json',
                            data: form.serialize(),
                            success: function(data)
                            {
                                loadResponse(data) ;
                                that.insLoadFinish();
                                $("#section_form").find("#section_name").val('');
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFinish();
                                $("#section_form")[0].reset();
                                that.insLoadFail();
                            }
                        });
                }
                
                this.updSection =function (form){
                    item = {}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="subcategory_id"){
                                item ["subcategory_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="category_id"){
                                item ["category_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="section_id"){
                                item ["section_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="section_name"){
                                item ["section_name"]= $(this).val();
                            }
                        });
                        updResponse(item);
                        that.updLoadBegin();
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/UpdSectionDetailsSvr",
                        data: form.serialize(), // serializes the form's elements.
                        success: function(data)
                        {
                            that.updLoadFinish();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.updLoadFinish();
                            that.updLoadFail();
                        }
                    });
                }
		
		/**
		 * load lots of data from the server
		 * or return data from cache if it's already
		 * loaded
		 */
                
		this.selSection = function(category_id,subcategory_id,module){
                    var outCache = toArray(category_id,subcategory_id);
			if(outCache.length) return outCache;
			that.selLoadBegin(module);
			$.ajax({
                                url: config.getU()+"/SelSectionDetailsSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'category_id':category_id,'subcategory_id':subcategory_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                        that.selLoadFinish(module);
                                        if(data.section_details!="")
                                        loadResponse(data,module) ;
				}
			});
		}
                
                this.delSection = function(id,category_id,subcategory_id){
                        delResponse(id,category_id,subcategory_id);
                	that.delLoadBegin();
			$.ajax({
				url: config.getU()+"/DelSectionDetailsSvr",
				type: 'GET',
                                data : {id : id },
				error: function(){
					that.delLoadFail();
				},
				success: function(data){
                                    if(data=="ok"){
                                        that.delLoadFinish();
                                    }
				}
			});
		}
                
		/**
		 * load lots of data from the server
		 */
		this.clearAll = function(){
			cache = new Array();
		}
		/**
		 * add a listener to this model
		 */
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.delLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadBegin();
			});
		}
                this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadBegin();
			});
		}

		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.delLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFinish();
			});
		}
                this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                
		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
                
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
                
		/**
		 * tell everyone the item we've loaded
		 */
                
		this.sectionItemLoaded = function(item,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,module);
			});
		}
                this.updItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].updLoadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	SectionListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			loadItem : function() { },
                        updLoadItem: function() { },
			insLoadFail : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
