jQuery.extend({
	Language: function(config){
		/**
		 * our local cache of data
		 */
                
		var cache = new Array();
		/**
		 * a reference to ourselves
		 */
		var that = this;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
		/**
		 * get contents of cache into an array
		 */
		function toArray(){
			var a = [];
			for (var i in cache){
                            item = {}
                            item ["language_id"] = i;
                            item ["language_name"]= cache[i];
                            a.push(item);
			}
			return a;
		}
		/**
		 * load a json response from an
		 * ajax call
		 */
		function loadResponse(data,val){
                    $.each(data, function(item){
                        cache[data[item].language_id] = decodeURIComponent(data[item].language_name);
                        if(val==undefined)
                        that.languageItemLoaded(data[item]);
                    });
		}
                function updResponse(data){
                    $.each(data, function(item){
                    		cache[data[item].language_id] = data[item].language_name;
                                that.updItemLoaded(data[item]);
                    });
		}
                function delResponse(id){
                    var a = Array();
                    for (var i in cache){
                        if(i!=id)a[i] = cache[i];
                    }
                    cache=a;
		}
		/**
		 * look in the cache for a single item
		 * if it's there, get it, if not
		 * ask the server to load it
		 */
		this.getLanguage = function(id){
			if(cache[id]) return cache[id];
                        else alert("no");
		}
                
                this.insLanguage =function (form){
                    that.insLoadBegin();
                      var language_name;
                      form.find(":input").each(function() {
                            if($(this).attr("name")=="language_name"){
                                language_name= $(this).val();
                            }
                      });
                      var article = new Object();
                      article.language_name = encodeURIComponent(language_name);
                      var jsonobj=JSON.stringify(article);
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/InsLanguageDetailsSvr",
                        data: jsonobj, // serializes the form's elements.
                        success: function(data)
                        {
                            var obj = jQuery.parseJSON(data);
                            loadResponse(obj.language_details) ;
                            that.insLoadFinish();
                            $("#lang_from")[0].reset();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.insLoadFinish();
                            $("#lang_from")[0].reset();
                            that.insLoadFail();
                        }
                    });
                }
                
                this.updLanguage =function (form){
                    jsonObj = [];
                    item = {}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="language_id"){
                                item ["language_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="language_name"){
                                item ["language_name"]= encodeURIComponent($(this).val());
                            }
                        });
                        jsonObj.push(item);
                        updResponse(jsonObj);
                        that.updLoadBegin();
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/UpdLanguageDetailsSvr",
                        data: JSON.stringify(item), // serializes the form's elements.
                        success: function(data)
                        {
                            that.updLoadFinish();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.updLoadFinish();
                            that.updLoadFail();
                        }
                    });
                }
		
		/**
		 * load lots of data from the server
		 * or return data from cache if it's already
		 * loaded
		 */
                
		this.selLanguage = function(module,val){
                    var outCache = toArray();
			if(outCache.length) return outCache;
                        if(val==undefined){
                            that.selLoadBegin(module);    
                        }
			$.ajax({
				url: config.getU()+"/SelLanguageDetailsSvr",
				type: 'GET',
				error: function(){
					that.selLoadFail(module);
                                        alert("erro")
				},
				success: function(data){
                                    if(val==undefined){
                                        that.selLoadFinish(module);
                                        var obj = jQuery.parseJSON(data);
                                        loadResponse(obj.language_details) ;
                                    }else{
                                        var obj = jQuery.parseJSON(data);
                                        loadResponse(obj.language_details,val) ;
                                    }
                                    
				}
			});
		}
                
                this.delLanguage = function(id){
                        delResponse(id);
                	that.delLoadBegin();
			$.ajax({
				url: config.getU()+"/DelLanguageDetailsSvr",
				type: 'GET',
                                data : {id : id },
				error: function(){
					that.notifyLoadFail();
				},
				success: function(data){
                                    if(data=="ok"){
                                        that.delLoadFinish();
                                    }
				}
			});
		}
                
		/**
		 * load lots of data from the server
		 */
		this.clearAll = function(){
			cache = new Array();
		}
		/**
		 * add a listener to this model
		 */
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.delLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadBegin();
			});
		}
                this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadBegin();
			});
		}

		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.delLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFinish();
			});
		}
                this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                
		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
		this.languageItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].loadItem(item);
			});
		}
                this.updItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].updLoadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	LanguageListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			loadItem : function() { },
                        updLoadItem: function() { },
			insLoadFail : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
