jQuery.extend({
	ResultSummery: function(config){
		var cache = {};
		var user_cache = {};
		var that = this;
		var check= 0;
		var listeners = new Array();
		
		/**
		 * load a json` response from an
		 * ajax call
		 */
                function toArray(category_id,subcategory_id,section_id,topic_id,language_id){
			var item = [];
                            try {
                                $.each(cache[category_id][subcategory_id][section_id][topic_id],function(j){
                                        if(cache[category_id][subcategory_id][section_id][topic_id][j][language_id]!=null){
                                            top_item = {}
                                            top_item ["question_id"] = j;
                                            top_item ["discription_id"] = cache[j][language_id].discription_id;
                                            top_item ["question_bank_id"] = cache[j][language_id].question_bank_id;
                                            top_item ["question"]= cache[j][language_id].question;
                                            top_item ["discription_bank_id"]= cache[j][language_id].discription_bank_id;
                                            item.push(top_item);
                                        }
                                    });
                            }catch(e){}
			return item;
		}
		function loadResponse(data,test_id,module){
                    
                    switch (module){
                        case 1:
                            $.each(data, function (personne,value) {
                                value["user_id"]=personne;
                                cache["test_id"]={};
                                cache["test_id"]["test_details"]=value;
                                that.userItemLoaded(value,module); 
                            });
                        break;
                        default:
                            $.each(data, function (personne,value) {
                                value["question_id"]=personne;
                                cache["test_id"]={};
                                cache["test_id"]["question_details"]=value;
                                that.userItemLoaded(value,module); 
                            });
                        break;
                    }
                    
		}
                
		this.selResultSummery = function(test_id,user_id,module){
                        that.selLoadBegin(module);
			$.ajax({
                                url: config.getU()+"/SelResultSummerySvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'test_id':test_id,'user_id':user_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish(module);
                                    if(data!="")
                                    loadResponse(data,test_id,user_id,module) ;
				}
			});
		}
                
		this.selUserTestDetails = function(test_id,user_id,module){
                        that.selLoadBegin(module);
			$.ajax({
                                url: config.getU()+"/SelUserTestDetailsSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'test_id':test_id,'user_id':user_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish(module);
                                    if(data!="")
                                    loadResponse(data,test_id,module) ;
				}
			});
		}
                
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                /**
		 * we're done loading, tell everyone
		 */
		
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                
		/**
		 * tell everyone the item we've loaded
		 */
                
		this.userItemLoaded = function(item,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,module);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	ResultSummeryListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { }
		}, list);
	}
});
