jQuery.extend({
	DirectionDetails: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
		function toArray(discription_id,discription_bank_id){
			var item = [];
                            try {
                                $.each(cache[discription_id][discription_bank_id],function(j){
                                            top_item ["question"]= discription_bank_id;
                                            top_item ["discription_bank_id"]= cache[discription_id][discription_bank_id];
                                            item.push(top_item);
                                });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json` response from an
		 * ajax call
		 */
		function loadResponse(data,discription_id,discription_bank_id,obj){
                    $.each(data, function (personne,value) {
                        if(cache[discription_id]==null){
                            cache[discription_id]={}
                            cache[discription_id][discription_bank_id]={}
                            cache[discription_id][discription_bank_id]=value
                        }else{
                            cache[discription_id][discription_bank_id]={}
                            cache[discription_id][discription_bank_id]=value
                        }
                        that.directionItemLoaded(value,obj); 
                    });
		}

                this.updDirectionDetails =function (discription_bank){
                        //updResponse(discription_bank);
                        that.updLoadBegin();
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/UpdDirectionDetailsSvr",
                        data: JSON.stringify(discription_bank), // serializes the form's elements.
                        success: function(data)
                        {
                            that.updLoadFinish();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.updLoadFinish();
                            that.updLoadFail();
                        }
                    });
                }
		
		/**
		 * load lots of data from the server
		 * or return data from cache if it's already
		 * loaded
		 */
                
		this.selDirectionDetails = function(discription_id,discription_bank_id,obj){
                    var outCache = toArray(discription_id,discription_bank_id);
			if(outCache.length) return outCache;
			$.ajax({
                                url: config.getU()+"/SelDirectionDetailsSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'discription_bank_id':discription_bank_id},
				error: function(){
				},
				success: function(data){
                                    if(data!="")
                                    loadResponse(data,discription_id,discription_bank_id,obj) ;
				}
			});
		}
                
                this.delDirectionDetails = function(category_id,subcategory_id,section_id,direction_id){
                        delResponse(category_id,subcategory_id,section_id,direction_id);
                	that.delLoadBegin();
			$.ajax({
				url: config.getU()+"/DelDirectionDetailsDetailsSvr",
				type: 'GET',
                                data : {id : direction_id },
				error: function(){
					that.delLoadFail();
				},
				success: function(data){
                                    if(data=="ok"){
                                        that.delLoadFinish();
                                    }
				}
			});
		}
                
		/**
		 * load lots of data from the server
		 */
		this.clearAll = function(){
			cache = new Array();
		}
		/**
		 * add a listener to this model
		 */
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.delLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadBegin();
			});
		}
                this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadBegin();
			});
		}

		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.delLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFinish();
			});
		}
                this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                
		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
                
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
                this.updLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFail();
			});
		}
                
		/**
		 * tell everyone the item we've loaded
		 */
                
		this.directionItemLoaded = function(value,obj){
			$.each(listeners, function(i){
				listeners[i].loadItem(value,obj);
			});
		}
                
                this.updItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].updLoadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	DirectionDetailsListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			loadItem : function() { },
                        updLoadItem: function() { },
			insLoadFail : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
