jQuery.extend({
	ProfileConfig: function(config){
		var listeners = new Array();
                var FirstName ;
                var profile_pic;
                var that=this;
                var user_id;
                this.getProfilePic= function (clas){
                    if(jQuery.type(profile_pic)=="object"){
                        return $("<img/>").attr({'src':profile_pic.toDataURL()}).addClass(clas);
                    }else{
                        var profilepic = $('<img />', { 
                            src: profile_pic,
                            alt: 'User Image'
                        });
                        profilepic.addClass(clas);
                        return profilepic;
                    }
                }
                this.setProfilePic= function (canvas){
                    profile_pic= canvas;
                }
                
//                this.getProfilePicheader= function (){
//                    var profile_pic = $('<img />', { 
//                        src: '../../dist/img/user2-160x160.jpg',
//                        alt: 'User Image'
//                    });
//                    profile_pic.addClass("user-image")
//                    return profile_pic;
//                }
                
                this.getFirstName =function (){
                    return FirstName;
                }
                this.setUserName =function (value){
                    FirstName=value;
                }
                this.getUser_id =function (){
                    return user_id;
                }
                this.setUser_id =function (value){
                    user_id=value;
                }
                
                this.checkSession = function (){
                       $.ajax({
                                url: config.getU()+"/CheckSessionSvr",
                                type: 'POST',
				error: function(err,a){
                                    console.log(JSON.stringify(err))
//                                    window.location="../../client/pages/home.html";
				},
				success: function(data){
                                    try{
                                        var ob=JSON.parse(data);
                                        if(ob["ok"]=="oknonoas90asnaksjkasjjasasdjajksdjk" && ob["user_type"]=="admin"){
                                            if(that.checkLocalStorage()){
                                                config.loadMain();
                                            }
                                        }else{
//                                            window.location="../../client/pages/home.html";
                                        }
                                    }catch(e){
                                        console.log(e)
//                                        window.location="../../client/pages/home.html";
                                    }
                                    
                                }
			});
                }
                
                this.logOut = function (){
                       config.getView("HeaderVgr").setLoadBegin();
                       $.ajax({
                                url: config.getU()+"/LogOutSvr",
                                type: 'POST',
				error: function(){
                                    config.getView("HeaderVgr").setLoadFinish();
				},
				success: function(data){
                                    if(data.trim()=="oknonoas90asnaksjkasjjasasdjajksdjk"){
                                    config.getView("HeaderVgr").setLoadFinish();
                                    localStorage.removeItem("usaassassaesras_sasasdaseastaasiasls");
                                        window.location="../../client/pages/home.html";
                                    }
                                }
			});
                }
                this.checkLocalStorage = function (){
                    if(typeof(Storage) !== "undefined") {
                      if(localStorage.getItem("usaassassaesras_sasasdaseastaasiasls") === null){
                          return false;
                      }else{
                           var obj = jQuery.parseJSON(decodeURIComponent(localStorage.getItem("usaassassaesras_sasasdaseastaasiasls")));
                           config.getModel("ProfileConfig").setUserName(obj.user_name);
                           config.getModel("ProfileConfig").setUser_id(obj.user_id);
                           config.getModel("ProfileConfig").setProfilePic(obj.pic);
                           return true;
                      }
                    } else {
                        return false;
                    }
                }
                this.checkSession();
                
	}
        
});
