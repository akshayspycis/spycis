jQuery.extend({
	ExamPhase: function(config){
		/**
		 * our local cache of data
		 */
                
		var cache = new Array();
		/**
		 * a reference to ourselves
		 */
		var that = this;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
		/**
		 * get contents of cache into an array
		 */
		function toArray(){
			var a = [];
			for (var i in cache){
                            item = {}
                            item ["exam_phase_id"] = i;
                            item ["exam_phase_name"]= cache[i];
                            a.push(item);
			}
			return a;
		}
		/**
		 * load a json response from an
		 * ajax call
		 */
		function loadResponse(data,val){
                    $.each(data, function(item){
				cache[data[item].exam_phase_id] = data[item].exam_phase_name;
                                if(val==undefined)
				that.exam_phaseuageItemLoaded(data[item]);
                    });
		}
                function updResponse(data){
                    $.each(data, function(item){
                    		cache[data[item].exam_phase_id] = data[item].exam_phase_name;
                                that.updItemLoaded(data[item]);
                    });
		}
                function delResponse(id){
                    var a = Array();
                    for (var i in cache){
                        if(i!=id)a[i] = cache[i];
                    }
                    cache=a;
		}
		/**
		 * look in the cache for a single item
		 * if it's there, get it, if not
		 * ask the server to load it
		 */
		this.getExamPhase = function(id){
			if(cache[id]) return cache[id];
                        else alert("no");
		}
                
                this.insExamPhase =function (form){
                    that.insLoadBegin();
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/InsExamPhaseDetailsSvr",
                        data: form.serialize(), // serializes the form's elements.
                        success: function(data)
                        {
                            var obj = jQuery.parseJSON(data);
                            loadResponse(obj.exam_phase_details) ;
                            that.insLoadFinish();
                            $("#exam_phase_from")[0].reset();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.insLoadFinish();
                            $("#exam_phase_from")[0].reset();
                            that.insLoadFail();
                        }
                    });
                }
                
                this.updExamPhase =function (form){
                    jsonObj = [];
                    item = {}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="exam_phase_id"){
                                item ["exam_phase_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="exam_phase_name"){
                                item ["exam_phase_name"]= $(this).val();
                            }
                        });
                        jsonObj.push(item);
                        updResponse(jsonObj);
                        that.updLoadBegin();
                    $.ajax({
                        type: "POST",
                        url:config.getU()+ "/UpdExamPhaseDetailsSvr",
                        data: form.serialize(), // serializes the form's elements.
                        success: function(data)
                        {
                            that.updLoadFinish();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.updLoadFinish();
                            that.updLoadFail();
                        }
                    });
                }
		
		/**
		 * load lots of data from the server
		 * or return data from cache if it's already
		 * loaded
		 */
                
		this.selExamPhase = function(val){
                    var outCache = toArray();
			if(outCache.length) return outCache;
                        if(val==undefined){
                            that.selLoadBegin();    
                        }
			$.ajax({
				url: config.getU()+"/SelExamPhaseDetailsSvr",
				type: 'GET',
				error: function(){
					that.selLoadFail();
                                        alert("erro")
				},
				success: function(data){
                                    if(val==undefined){
                                        that.selLoadFinish();
                                        var obj = jQuery.parseJSON(data);
                                        loadResponse(obj.exam_phase_details) ;
                                    }else{
                                        var obj = jQuery.parseJSON(data);
                                        loadResponse(obj.exam_phase_details,val) ;
                                    }
                                    
				}
			});
		}
                
                this.delExamPhase = function(id){
                        delResponse(id);
                	that.delLoadBegin();
			$.ajax({
				url: config.getU()+"/DelExamPhaseDetailsSvr",
				type: 'GET',
                                data : {id : id },
				error: function(){
					that.notifyLoadFail();
				},
				success: function(data){
                                    if(data=="ok"){
                                        that.delLoadFinish();
                                    }
				}
			});
		}
                
		/**
		 * load lots of data from the server
		 */
		this.clearAll = function(){
			cache = new Array();
		}
		/**
		 * add a listener to this model
		 */
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                this.selLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin();
			});
		}
                this.delLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadBegin();
			});
		}
                this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadBegin();
			});
		}

		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                this.selLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish();
			});
		}
                this.delLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFinish();
			});
		}
                this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                
		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                this.selLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadFail();
			});
		}
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
		this.exam_phaseuageItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].loadItem(item);
			});
		}
                this.updItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].updLoadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	ExamPhaseListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			loadItem : function() { },
                        updLoadItem: function() { },
			insLoadFail : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
