jQuery.extend({
	Topic: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
		function toArray(category_id,subcategory_id,section_id){
			var item = [];
                            try {
                                    $.each(cache[category_id][subcategory_id][section_id],function(j){
                                                top_item = {}
                                                top_item ["topic_id"] = cache[category_id][subcategory_id][section_id][j].topic_id;
                                                top_item ["topic_name"]= cache[category_id][subcategory_id][section_id][j].topic_name;
                                                item.push(top_item);
                                    });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json` response from an
		 * ajax call
		 */
		function loadResponse(data,module){
                    var top_data=data.topic_details;
                    var category_id=data.category_id;
                    var subcategory_id=data.subcategory_id;
                    var section_id=data.section_id;
                    if(cache[category_id]==null){
                        cache[category_id]={};
                        cache[category_id][subcategory_id]={}
                        cache[category_id][subcategory_id][section_id]=getTopCache(top_data,{},module);
                    }else{
                        if(cache[category_id][subcategory_id]==null){
                            cache[category_id][subcategory_id]={};
                            cache[category_id][subcategory_id][section_id]=getTopCache(top_data,{},module);
                        }else{
                            if(cache[category_id][subcategory_id][section_id]==null){
                                cache[category_id][subcategory_id][section_id]=getTopCache(top_data,{},module);
                            }else{
                                cache[category_id][subcategory_id][section_id]=getTopCache(top_data,cache[category_id][subcategory_id][section_id],module);
                            }
                        }
                    }
		}
                
                function getTopCache(top_data,sec_cache,module){
                    try {
                        $.each(top_data, function(i){
                                top_item_obj = {}
                                top_item_obj["topic_name"]= top_data[i].topic_name;
                                top_item_obj["topic_id"]= top_data[i].topic_id;
                                sec_cache[top_data[i].topic_id]=top_item_obj;
                                that.topicItemLoaded (top_item_obj,module);
                    });
                    }catch(e){
                    }
                    return sec_cache;
                }
                
                function updResponse(data){
                    cache[data.category_id][data.subcategory_id][data.section_id][data.topic_id].topic_name=data.topic_name;
                    that.updItemLoaded(data);
		}
                function delResponse(category_id,subcategory_id,section_id,topic_id){
                        delete cache[category_id][subcategory_id][section_id][topic_id];
		}
		/**
		 * look in the cache for a single item
		 * if it's there, get it, if not
		 * ask the server to load it
		 */
		this.getTopic = function(category_id,subcategory_id,section_id,topic_id){
                            return cache[category_id][subcategory_id][section_id][topic_id].topic_name;
		}
                
                this.insTopic =function (form){
                        that.insLoadBegin();
                        $.ajax({
                            url: config.getU()+"/InsTopicDetailsSvr",
                            type: 'POST',
                            dataType: 'json',
                            data: form.serialize(),
                            success: function(data)
                            {
                                loadResponse(data) ;
                                that.insLoadFinish();
                                $("#topic_form").find("#topic_name").val('');
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFinish();
                                $("#topic_form")[0].reset();
                                that.insLoadFail();
                            }
                        });
                }
                
                this.updTopic =function (form){
                    item = {}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="subcategory_id"){
                                item ["subcategory_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="category_id"){
                                item ["category_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="section_id"){
                                item ["section_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="topic_id"){
                                item ["topic_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="topic_name"){
                                item ["topic_name"]= $(this).val();
                            }
                        });
                        updResponse(item);
                        that.updLoadBegin();
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/UpdTopicDetailsSvr",
                        data: form.serialize(), // serializes the form's elements.
                        success: function(data)
                        {
                            that.updLoadFinish();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.updLoadFinish();
                            that.updLoadFail();
                        }
                    });
                }
		
		/**
		 * load lots of data from the server
		 * or return data from cache if it's already
		 * loaded
		 */
                
		this.selTopic = function(category_id,subcategory_id,section_id,module){
                    var outCache = toArray(category_id,subcategory_id,section_id);
			if(outCache.length) return outCache;
			that.selLoadBegin(module);
			$.ajax({
                                url: config.getU()+"/SelTopicDetailsSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'category_id':category_id,'subcategory_id':subcategory_id,'section_id':section_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish(module);
                                    if(data.topic_details!="")
                                    loadResponse(data,module) ;
				}
			});
		}
                
                this.delTopic = function(category_id,subcategory_id,section_id,topic_id){
                        delResponse(category_id,subcategory_id,section_id,topic_id);
                	that.delLoadBegin();
			$.ajax({
				url: config.getU()+"/DelTopicDetailsSvr",
				type: 'GET',
                                data : {id : topic_id },
				error: function(){
					that.delLoadFail();
				},
				success: function(data){
                                    if(data=="ok"){
                                        that.delLoadFinish();
                                    }
				}
			});
		}
                
		/**
		 * load lots of data from the server
		 */
		this.clearAll = function(){
			cache = new Array();
		}
		/**
		 * add a listener to this model
		 */
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.delLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadBegin();
			});
		}
                this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadBegin();
			});
		}

		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.delLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFinish();
			});
		}
                this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                
		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
                
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
                
		/**
		 * tell everyone the item we've loaded
		 */
                
		this.topicItemLoaded = function(item,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,module);
			});
		}
                
                this.updItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].updLoadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	TopicListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			loadItem : function() { },
                        updLoadItem: function() { },
			insLoadFail : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
