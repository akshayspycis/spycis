jQuery.extend({
	Config: function(){
            //......................................................................................................................................
		var that = this;
		var model = new Array();
                var view = new Array();
                var controller = new Array();
//                var aaa_pp ="http://spycis.com/";
                var aaa_pp ="http://localhost:8084/spycis/";
                this.getU = function (){
                    return aaa_pp;
                }
                
                
                var load_js = $('<img />', { 
                    class: 'img-rounded',
                    src: '../../dist/images/loading_1.gif',
                    css:{'display':' block','margin-left':'auto','margin-right':'auto','margin-top':'auto','margin-bottom':'auto'}
                });
                
                var animation_infoprakr =$("<h1></h1>").addClass("loader")
                                            .append($("<span>I</span>"))
                                            .append($("<span>N</span>"))
                                            .append($("<span>F</span>"))
                                            .append($("<span>O</span>"))
                                            .append($("<span>P</span>"))
                                            .append($("<span>A</span>"))
                                            .append($("<span>R</span>"))
                                            .append($("<span>K</span>"));
//                                            
//                                            var animation_infoprakr =$("<h1></h1>").addClass("loader")
//                                            .append($("<span>V</span>"))
//                                            .append($("<span>I</span>"))
//                                            .append($("<span>B</span>"))
//                                            .append($("<span>R</span>"))
//                                            .append($("<span>A</span>"))
//                                            .append($("<span>N</span>"))
//                                            .append($("<span>T</span>"));
            //.............Contructor.........................................................................................................................                
                var width=$(window).width();
                this.Config =function (){
                    var fontsize;
                    if(width>1100){
                        fontsize="100px";
                    }else if(width>991&&width<=1100){
                        fontsize="90px";
                    }else if(width>768&&width<=991){
                        fontsize="80px";
                    }else if(width>467&&width<=693){
                        fontsize="60px";
                    }else if(width>350&&width<=467){
                        fontsize="50px";
                    }else if(width>300&&width<=350){
                        fontsize="45px";
                    }else if(width>270&&width<=300){
                        fontsize="40px";
                    }else if(width>248&&width<=270){
                        fontsize="35px";
                    }else if(width>210&&width<=248){
                        fontsize="30px";
                    }else {
                        fontsize="25px";
                    }
                    animation_infoprakr.find("span" ).css({"font-size":fontsize});
                    that.startAnimationInfoaprk($("body"));
                    that.loadProfileConfig();
                } 
            //.......................Methods...............................................................................................................    
            
            //for load Profile Config js file
                this.loadProfileConfig=function (){
                    $.get("../model/Profileconfig.js").done(function() {
                        try {
                            that.setModel("ProfileConfig",new $.ProfileConfig(that));
                            
                    }catch(e){
                            alert("Error:000 profile.js Loading Problem "+e);
                    }
                    }).fail(function() {alert("Error: profile.js  Loading Problem");}); 
                }
                
            //for load Main js file    
                this.loadMain=function (){
                    $.get("../model/Main.js").done(function() {
                        try {
                            that.setModel("Main",new $.Main());
                            that.loadMainVgr();
                    }catch(e){
                            alert("Error:001 Main.js Loading Problem "+e);
                    }
                    }).fail(function() {alert("Error:001 Main.js Loading Problem");}); 
                }
            //for load MainVgr js file    
                this.loadMainVgr=function (){
                    $.get("../view/MainVgr.js").done(function() {
                        try {
                            that.setView("MainVgr",new $.MainVgr(that));
                            that.loadMainMgr();
                    }catch(e){
                            alert("Error:002 Main.js Loading Problem "+e);
                    }
                    }).fail(function() {alert("Error:002 Main.js Loading Problem");}); 
                }
                
            //for load MainMgr js file    
                this.loadMainMgr=function (){
                    $.get("../controller/MainMgr.js").done(function() {
                        try {
                            that.setController("MainMgr",new $.MainMgr(that));
                            that.load();
                    }catch(e){
                            alert("Error:003 MainMgr.js Loading Problem "+e);
                    }
                    }).fail(function() {alert("Error:003 MainMgr.js Loading Problem");}); 
                }
             //for load body funtion
                this.load=function (){
                            that.getView("MainVgr").loadJs();
                }
                
                
                
            //...............................for extra supporting method..............................    
                this.startLoadingJs =function (div){
                    var height = parseInt($( window ).height())/2-50;
                    height+="px";
                    div.css({'margin-right':'auto','margin-left':'auto','position':'relative','margin-top':height}).append(load_js);
                }
                
                this.finishLoadingJs =function (div){
                    div.css({'margin-right':'','margin-left':'','position':'','margin-top':''});;
                    div.empty();
                }
                
                this.startAnimationInfoaprk =function (div){
                    div.append(animation_infoprakr);
                }
                
                this.finishAnimationInfoaprk =function (div){
                    div.css({'margin-right':'','margin-left':'','position':'','margin-top':''});;
                    div.empty();
                }

                
                this.getLoadingData =function (){
                    var load_data = $('<span />', { 
                        class: 'glyphicon glyphicon-refresh glyphicon-refresh-animate',
                        css:{'margin':'auto'}
                    });
                    return load_data;
                }
                
                this.setModel = function(key,value){
			model[key]=value;
		}
                
                this.setView = function(key,value){
			view[key]=value;
                        
                    $.each( view, function( key, value ) {
                        alert( key + ": " + value );
                    });

		}
                
                this.setController = function(key,value){
			controller[key]=value;
		}
                
                this.getModel = function(key){
			return model[key];
		}
                
                this.getView = function(key){
			return view[key];
		}
                
                this.getController = function(key){
			return controller[key];
		}
                
                this.c =function (){
                    return that;
                }
                
                
        },
	/**
	 * let people create listeners easily
	 */
	ConfigListener: function(list) {
		if(!list) list = {};
		return $.extend({
			setObjectValue : function() { },
			loadOneClicked : function() { },
			clearAllClicked : function() { }
		}, list);
	}
});
