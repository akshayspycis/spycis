jQuery.extend({
	CreateTest: function(config){
		var that = this;
		var listeners = new Array();
		this.insCreateTest =function (form){
                        that.insLoadBegin();
                        var jsonobj=JSON.stringify(form);
                        $.ajax({
                            url: config.getU()+"/InsCreateTestDetailsSvr",
                            type: 'POST',
                            data: jsonobj,
                            success: function(data)
                            {
                                if(data.trim()=="ok"){
                                   that.insLoadFinish(); 
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFinish();
                                //$("#topic_form")[0].reset();
                                that.insLoadFail();
                            }
                        });
                }
                
		this.addListener = function(list){
			listeners.push(list);
		}
		
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
	},
	/**
	 * let people create listeners easily
	 */
	CreateTestListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			insLoadFail : function() { }
		}, list);
	}
});
