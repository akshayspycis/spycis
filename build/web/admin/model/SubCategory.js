jQuery.extend({
	SubCategory: function(config){
		/**
		 * our local cache of data
		 */
                
		var cache = {};
		/**
		 * a reference to ourselves
		 */
		var that = this;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
		/**
		 * get contents of cache into an array
		 */
		function toArray(category_id){
			var item = [];
                        try {
                                $.each(cache[category_id],function(j){
                                        sub_item = {}
                                        sub_item ["subcategory_id"] = cache[category_id][j].subcategory_id;
                                        sub_item ["subcategory_name"]= cache[category_id][j].subcategory_name;
                                        item.push(sub_item);
                                });
                        }catch(e){}
			return item;
		}
		/**
		 * load a json response from an
		 * ajax call
		 */
		function loadResponse(data,module){
                    var sub_data=data.subcategory_details;
                    var category_id=data.category_id;
                    try{
                        if(cache[category_id]==null){
                                cache[category_id]=getSecCache(sub_data,{},module);
                        }else{
                                cache[category_id]=getSecCache(sub_data,cache[category_id],module);
                        }
                        }catch(e){
                        }

		}
                
                 function getSecCache(sec_data,sec_cache,module){
                    $.each(sec_data, function(i){
                                sub_item = {}
                                sub_item["subcategory_name"]= sec_data[i].subcategory_name;
                                sub_item["subcategory_id"]= sec_data[i].subcategory_id;
                                sec_cache[sec_data[i].subcategory_id]=sub_item;
				that.sub_categoryItemLoaded(sub_item,module);
                    });
                    return sec_cache;
                }
                
                function updResponse(data){
                    cache[data.category_id][data.subcategory_id].subcategory_name=data.subcategory_name;
                    that.updItemLoaded(data);
		}
                function delResponse(subcategory_id,category_id){
                    delete cache[category_id][subcategory_id];
		}
		/**
		 * look in the cache for a single item
		 * if it's there, get it, if not
		 * ask the server to load it
		 */
		this.getSubCategory = function(id,category_id){
                    var sub_cache=cache[category_id];
                    for (var i in sub_cache){
                        if(sub_cache[i].subcategory_id==id)
                            return sub_cache[i].subcategory_name;
                    }
		}
                
                this.insSubCategory =function (form){
                        that.insLoadBegin();
                        $.ajax({
                            url: config.getU()+"/InsSubCategorySvr",
                            type: 'POST',
                            dataType: 'json',
                            data: form.serialize(),
                            success: function(data)
                            {
                                loadResponse(data) ;
                                that.insLoadFinish();
                                $("#sub_cat_from").find("#subcategory_name").val('');
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFinish();
                                $("#sub_cat_from")[0].reset();
                                that.insLoadFail();
                            }
                        });
                }
                
                this.updSubCategory =function (form){
                    item = {}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="subcategory_id"){
                                item ["subcategory_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="category_id"){
                                item ["category_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="subcategory_name"){
                                item ["subcategory_name"]= $(this).val();
                            }
                        });
                        updResponse(item);
                        that.updLoadBegin();
                        
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/UpdSubCategoryDetailsSvr",
                        data: form.serialize(), // serializes the form's elements.
                        success: function(data)
                        {
                            that.updLoadFinish();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.updLoadFinish();
                            that.updLoadFail();
                        }
                    });
                }
		
		/**
		 * load lots of data from the server
		 * or return data from cache if it's already
		 * loaded
		 */
                
		this.selSubCategory = function(category_id,module){
                    var outCache = toArray(category_id);
			if(outCache.length) return outCache;
			that.selLoadBegin(module);
			$.ajax({
                                url: config.getU()+"/SelSubCategorySvr",
                                type: 'POST',
                                dataType: 'json',
                                data: 'category_id='+category_id,
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                        that.selLoadFinish(module);
                                        if(data.subcategory_details!="")
                                        loadResponse(data,module) ;
				}
			});
		}
                
                this.delSubCategory = function(id,category_id){
                        delResponse(id,category_id);
                	that.delLoadBegin();
			$.ajax({
				url: config.getU()+"/DelSubCategorySvr",
				type: 'GET',
                                data : {id : id },
				error: function(){
					that.notifyLoadFail();
				},
				success: function(data){
                                    if(data=="ok"){
                                        that.delLoadFinish();
                                    }
				}
			});
		}
                
		/**
		 * load lots of data from the server
		 */
		this.clearAll = function(){
			cache = new Array();
		}
		/**
		 * add a listener to this model
		 */
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.delLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadBegin();
			});
		}
                this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadBegin();
			});
		}

		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.delLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFinish();
			});
		}
                this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                
		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
                
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
                
		/**
		 * tell everyone the item we've loaded
		 */
                
		this.sub_categoryItemLoaded = function(item,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,module);
			});
		}
                this.updItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].updLoadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	SubCategoryListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			loadItem : function() { },
                        updLoadItem: function() { },
			insLoadFail : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
