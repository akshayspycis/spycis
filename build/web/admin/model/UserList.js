jQuery.extend({
	UserList: function(config){
		var cache = {};
		var user_cache = {};
		var that = this;
		var check= 0;
		var listeners = new Array();
		function toArray(test_id){
			var item = [];
                            try {
                                if(cache[test_id]!==null){
                                    $.each(user_cache,function(j){
                                                top_item ={};
                                                top_item["user_id"] = j;
                                                top_item["user_name"] = user_cache[j]["user_name"];
                                                top_item["email"] = user_cache[j]["email"];
                                                top_item["contact_no"] = user_cache[j]["contact_no"];
                                                top_item["pic"] = user_cache[j]["pic"];
                                                if(Object.keys(cache[test_id]).length>0){
                                                    $.each(cache[test_id],function(k){
                                                        if(j==k){
                                                            top_item["test_status"] = cache[test_id][k];
                                                        }
                                                    });
                                                }else{
                                                    top_item["test_status"] = "false";
                                                }
                                                item.push(top_item);
                                    });
                                }
                            }catch(e){}
			return item;
		}
		/**
		 * load a json` response from an
		 * ajax call
		 */
		function loadResponse(data,test_id,module){
                    $.each(data, function (personne,value) {
                        if(cache[test_id]==null){
                            cache[test_id]={}
                        }
                        cache[test_id][personne]=value.test_status
                        if (user_cache[personne]==null){
                            user_cache[personne]={};        
                            user_cache[personne]=value;        
                        }
                        if(value.user_name!=null){
                            value["user_id"]=personne;
                            that.userItemLoaded(value); 
                        }else{
                            user_cache[personne]["test_status"]=value.test_status
                            that.userItemLoaded(user_cache[personne]); 
                        }
                    });
		}
                
		this.updTestInUser=function (test_id,user_id,test_status){
                    cache[test_id][user_id]=test_status;
		}
                
                this.insUserInTest =function (test_id){
                        that.insLoadBegin();
                        var obj={};
                        obj["test_id"]=test_id;
                        obj["user_details"]=cache[test_id];
                        var jsonobj=JSON.stringify(obj);
                        $.ajax({
                            url: config.getU()+"/InsUserInTestSvr",
                            type: 'POST',
                            data: jsonobj,
                            success: function(data)
                            {
                                that.insLoadFinish();
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFinish();
                                that.insLoadFail();
                            }
                        });
                }
                
                this.updUserList =function (form){
                    item = {}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="subcategory_id"){
                                item ["subcategory_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="category_id"){
                                item ["category_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="section_id"){
                                item ["section_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="user_id"){
                                item ["user_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="user_name"){
                                item ["user_name"]= $(this).val();
                            }
                        });
                        updResponse(item);
                        that.updLoadBegin();
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/UpdUserListDetailsSvr",
                        data: form.serialize(), // serializes the form's elements.
                        success: function(data)
                        {
                            that.updLoadFinish();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.updLoadFinish();
                            that.updLoadFail();
                        }
                    });
                }
                
                
		this.selUserList = function(test_id,module){
                    var outCache = toArray(test_id);
                        that.selLoadBegin(module);
			if(outCache.length) {
                            that.selLoadFinish(module);
                            return outCache;
                        }
			$.ajax({
                                url: config.getU()+"/SelUserListSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'test_id':test_id,'check':check},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    if(check==0){
                                        check=1;
                                    }
                                    that.selLoadFinish(module);
                                    if(data!="")
                                    loadResponse(data,test_id,module) ;
				}
			});
		}
                
		this.updVisibility = function(category_id,subcategory_id,test_id,visible){
                    updVisibilityInCache(category_id,subcategory_id,test_id,visible)
			$.ajax({
                                url: config.getU()+"/UpdTestVisibiltySvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'test_id':test_id,'visible':visible},
				error: function(){
				},
				success: function(data){
				}
			});
		}
		
		
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(module){
                    
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                
                this.delLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadBegin();
			});
		}
                
                this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}

		/**
		 * we're done loading, tell everyone
		 */
		
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                
                this.delLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFinish();
			});
		}
                
                this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
                this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
		/**
		 * tell everyone the item we've loaded
		 */
                
		this.userItemLoaded = function(item,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,module);
			});
		}
                
                this.updItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].updLoadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	UserListListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        updLoadItem: function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        insLoadBegin: function() { },
                        insLoadFinish: function() { },
                        insLoadFail: function() { }

		}, list);
	}
});
