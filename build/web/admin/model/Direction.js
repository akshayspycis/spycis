jQuery.extend({
	Direction: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
		
		
                this.insDirection =function (form){
                        that.insLoadBegin();
                        $.each(form["language_details"],function(i){
                                    $.each(form["language_details"][i]["question_details"],function(j){
                                        form["language_details"][i]["question_details"][j]["img_details"]=null;
                                    });
                        });
                        var jsonobj=JSON.stringify(form);
                        $.ajax({
                            url: config.getU()+"/InsDirectionDetailsSvr",
                            type: 'POST',
                            data: jsonobj,
                            success: function(data)
                            {
                                if(data.trim()=="true"){
                                    that.insLoadFinish();
                                }else{
                                    that.insLoadFail();
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFinish();
                                //$("#topic_form")[0].reset();
                                that.insLoadFail();
                            }
                        });
                }
                
                
		
		this.clearAll = function(){
			cache = new Array();
		}
		/**
		 * add a listener to this model
		 */
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                
		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                
		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
	
	},
	/**
	 * let people create listeners easily
	 */
	DirectionListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			loadItem : function() { },
                        updLoadItem: function() { },
			insLoadFail : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
