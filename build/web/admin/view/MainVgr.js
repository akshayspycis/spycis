jQuery.extend({
	MainVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
                var content_wrapper;
                
                this.setBody = function(){
                     config.finishLoadingJs($("body"));
                     var div=$("<div></div>").addClass("wrapper");
                     div.append(config.getView("HeaderVgr").setHeader());
                     div.append(config.getView("MainSidebarVgr").setMainSideBar());
                     content_wrapper=config.getView("ContentWrapperVgr").setContentWrapper();
                     that.getContentWrapper_BoxBody().append(config.getView("HomeVgr").getHome());
                     div.append($("<div id ='hidden'></div>"));
                     div.append(content_wrapper);
                     $("body").append(div); 
                     config.getModel("Category").selCategory(1);
                     $.each(listeners,function(i){
                        listeners[i].loadSubCategoryVgr(1);
                     });
		}
                this.getContentWrapper_BoxBody =function (){
                    return content_wrapper.find(".content>.box>.box-body");
                }
                
                this.loadJs =function (){
                        $.each(listeners,function(i){
                            listeners[i].loadHeaderVgr();
                        });
                }
                
                this.getListener = function (){
                    var l;
                    $.each(listeners, function(i){
                            l=listeners[i];
                    });
                    return l;
                }
                
                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	MainVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadHeaderVgr : function() { },
			loadHeader: function() { },
			loadHeaderMgr : function() { },
                        loadMainSidebarVgr : function() { },
			loadMainSidebar: function() { },
			loadMainSidebarMgr : function() { },
                        loadContentWrapperVgr : function() { },
			loadContentWrapper: function() { },
			loadContentWrapperMgr : function() { },
                        loadFooterVgr : function() { },
			loadFooter: function() { },
                        loadCategoryVgr: function() { },
                        loadCategory: function() { },
                        loadCategoryMgr: function() { },
                        loadSubCategoryVgr: function() { },
			loadFooterMgr : function() { }
		}, list);
	}
});
