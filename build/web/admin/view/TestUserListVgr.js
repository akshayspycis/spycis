jQuery.extend({
	TestUserListVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
                var modelform;
                var model_footer;
                var ul;
                var model_body;
                var testid;
                var total;
                var outoff;
                var save;
                var error;
                var total_std=0;
                var outoff_std=0;
                
                                   
                this.getTestUserList = function(test_id){
                        testid=test_id;
			var model =$("<div>").addClass("box").css({'margin-top':'-16px'});
                        model.append(that.getTestUserListHeader());
                        model.append(that.getTestUserListBody());
                        return model;
		}
                

                this.getTestUserListHeader= function (){
                    var model_header =$("<div>").addClass("box-header with-border")
                                 .append($("<div>").addClass("input-group")
                                   .append($("<div>").addClass("input-group-btn")
                                        .append($("<button>").addClass("btn btn-primary dropdown-toggle").attr({'type':'button','data-toggle':'dropdown','aria-expanded':'false'}).append("Search By&nbsp;")
                                                .append($("<span>").addClass("fa fa-caret-down")))
                                        .append($("<ul>").addClass("dropdown-menu")
                                                .append($("<li>").append($("<a>").append("User Name")))
                                                .append($("<li>").append($("<a>").append("User Id")))
                                                .append($("<li>").append($("<a>").append("Email")))
                                                .append($("<li>").append($("<a>").append("Contact")))
                                        )
                                    )
                                    .append($("<input>").addClass("form-control").attr({'type':'text','disabled':'disabled'})));
                    return model_header;
                }
                
                this.getTestUserListBody = function (){
                    model_body =$("<div>").addClass("box-body").css({'display':'block'});
                    ul=$("<ul>").addClass("products-list product-list-in-box");
                    model_body.append(ul);
                    return model_body ;
                }
                
                this.getUser = function (user){
                    return $("<li>").addClass("item").attr({'id':user.user_id})
                            .append($("<div>").addClass("product-img").append($('<img>').attr({'src':user.pic})))
                            .append($("<div>").addClass("product-info")
                                .append($('<a>').addClass("product-title").append(user.user_name))
                                .append($('<a>').addClass("btn btn-primary btn-sm pull-right").css({'cursor': 'pointer'}).append(parseFloat(user.total).toFixed(2))
                                    .click(function (){
                                            that.setResultSummery(user.user_id,testid,$(this),user.total);
                                    })
                                )
                                .append($('<span>').addClass("product-description")
                                        .append($("<i>").addClass("fa fa-fw fa-mobile")).append("&nbsp;"+user.contact_no+"&nbsp;&nbsp;")));
                }
                
                 this.setResultSummery = function (user_id,test_id,obj,value){
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("ResultSummeryVgr")==null){
                        $.getScript("../view/ResultSummeryVgr.js").done(function(){
                            config.setView("ResultSummeryVgr",new $.ResultSummeryVgr(config));
                            var model = config.getView("ModelVgr").getModel("Result summery",config.getView("ResultSummeryVgr").getResultSummery(test_id,user_id,value),"submit");
                            model.find(".modal-dialog").addClass("modal-lg");
                            
                                $.getScript("../model/ResultSummery.js").done(function() {
                                    config.setModel("ResultSummery",new $.ResultSummery(config));
                                    $.getScript("../controller/ResultSummeryMgr.js").done(function() {
                                        config.setController("ResultSummeryMgr",new $.ResultSummeryMgr(config,config.getModel("ResultSummery"),config.getView("ResultSummeryVgr")));
                                        obj.empty();
                                        obj.append(value);
                                        $("hidden").empty();
                                        $("hidden").append(model);
                                        model.find(".modal-footer").find("button").first().remove();
                                        model.modal('show');
                                        config.getView("ResultSummeryVgr").loadUserTestDetails();
                                    }).fail(function() {alert("Error :problem in TestUserListMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in TestUserListvgr.js file ");}); 
                        }).fail(function() {alert("Error:004 problem in ResultSummeryVgr.js file ");}); 
                     }else{
                            var model = config.getView("ModelVgr").getModel("Result summery",config.getView("ResultSummeryVgr").getResultSummery(test_id,user_id,value),"submit");
                            model.find(".modal-dialog").addClass("modal-lg");
                            obj.empty();
                            obj.append(value);
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.find(".modal-footer").find("button").first().remove();
                            model.modal('show');
                            config.getView("ResultSummeryVgr").loadUserTestDetails();
                     }
                }
                
                this.insUserInTest = function (obj){
                    var test_status="";
                    obj.empty();
                    obj.append(config.getLoadingData());
                    that.removeServerError();
                    $.each(listeners, function(i){
                        listeners[i].insUserInTest(testid,test_status);
                    });
                }
                this.addTestUserList  =function (data){
                   ul.append(that.getUser(data));
                } 
                
                
                this.loadTestUserList =function (){
                    total_std=0;
                    outoff_std=0;
                    $.each(listeners, function(i){
                        listeners[i].selTestUserList(testid);
                    });
                } 
                
                this.selLoadBegin =function (){
                    ul.empty();
                    ul.append($("<li>").addClass("item").append(config.getLoadingData()));
                }
                this.selLoadFinish=function (){
                    ul.empty();
                }
                this.selLoadFail = function() { 
                    ul.append($("<li>").addClass("item").append("Server doesn't responce."));
                }
                
                this.setDisableButton =function (){
                    model_footer.find("#"+c).prop('disabled', true);
                    model_footer.find("#cancel").prop('disabled', true);
                }
                
                this.setEnableButton =function (){
                    model_footer.find("#"+c).prop('disabled', false);
                    model_footer.find("#cancel").prop('disabled', false);
                }
                
                this.setServerError =function (){
                    ul.append("<li>Error: Server doesn't Respond.<li>");
                }
                
                this.setError =function (message){
                    model_footer.find("#error_msg").append(message);
                }
                
                this.removeServerError =function (){
                    error.empty();
                }
                this.insLoadBegin =function (){
                    save.empty();
                    save.append(config.getLoadingData());
                }
                this.insLoadFinish =function (){
                    save.empty();
                    save.append("Save");
                }
                this.insLoadFail = function() { 
                    error.empty();
                    error.append("Server doesn't respond.");
                }
                
                
                this.addListener = function(list){
                            listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	TestUserListVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selTestUserList : function() { },
			delTestUserList: function() { },
			updTestUserList: function() { }
		}, list);
	}
});
