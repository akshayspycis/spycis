jQuery.extend({
	OnlyQuestionVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var onlyQuestion;
                var cat_dorp_down;
                var sub_cat_dorp_down;
                var sec_dorp_down;
                var top_dorp_down;
                var lang_drop_down;
                var b=true;
                var ul_question_box_body;
                discription={};
                var text_editor;
                var check=true;
                var temp_question_button;
                var temp_save_button;
                var dorp_down_for_lang_copy_setting;
                var default_lang_id;
                
                this.getOnlyQuestionVgr= function(){
                    check_lag_default=0;
                    discription={};
                    config.getView("ContentWrapperVgr").setBoxHeader("Question Management");
                    var div=$("<div></div>");
                        //--------------------------------------------------------
			onlyQuestion =$("<div></div>").addClass("col-md-9");
                        onlyQuestion.append(that.getPanelTitle);
                        var box=that.getBoxbody();
                        
                        $.getScript("https://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js").done(function(){        
                            check=false;
                            
                        }).fail(function(e) {alert(e);}); 
                    var panel =$("<div></div>").addClass("panel panel-default col-lg-5");
                            var panel_body =$("<div></div>").addClass("panel-body ")
                            panel_body.append(that.getCategory_DropDown);
                            panel_body.append(that.getSubCategory_DropDown);
                            panel_body.append(that.getSection_DropDown);
                            panel_body.append(that.getTopic_DropDown());
                            panel.append(panel_body);
                            box.append(panel);
                            box.append($("<div></div>").addClass("col-lg-1"));
                            var panel_question =$("<div></div>").addClass("panel panel-default col-lg-6")
                            var panel_question_body =$("<div></div>").addClass("panel-body")
                            panel_question_body.append(that.getQuestionBox());
                            var lan_body =$("<div></div>").addClass("panel-body")
                            lan_body.append(that.getLanBox());
                            panel_question.append(panel_question_body);
                            panel_question.append(lan_body);
                            box.append(panel_question);
                            onlyQuestion.append(box);
//                            onlyQuestion.append(that.getBoxFooter());
                            div.append(onlyQuestion);
                        //--------------------------------------------------------
                        
                         that.loadCategory();
                         that.loadLanguage(2);
                        //-----------this code is resposible for load category and load subcatgory--------------------------
                      //  that.loadCategory();
                        cat_dorp_down.find(".cat1").trigger("change");
                        return div;
		}

                this.getPanelTitle = function (){
                    var div =$("<div></div>").addClass("box-header with-border");
                    div.append($("<h3></h3>").addClass("box-title").append($("<b>OnlyQuestion Details</b>")));
                    div.append(that.getDropdown());
                    div.append(that.getFooter());
                    return div;
                }
                
                this.getCategory_DropDown = function (){
                    cat_dorp_down =$("<div></div>").addClass("form-group col-lg-12");
                    cat_dorp_down.append($("<label>Category :</label>"))
                    cat_dorp_down.append($("<select>Category :</select>").addClass("form-control cat1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var id = cat_dorp_down.find(".cat1 option:first").val();
                            $( ".cat1 option:selected" ).each(function() {
                              id = $( this ).val();
                            });
                        that.loadSubCategory(null,id,6);
                    });
                    return cat_dorp_down;
                }
                
                this.getSubCategory_DropDown = function (){
                    sub_cat_dorp_down =$("<div></div>").addClass("form-group col-lg-12");
                    sub_cat_dorp_down.append($("<label>Sub Category :</label>"))
                    sub_cat_dorp_down.append($("<a></a>").attr({'id':'sub_img_loading'}));
                    sub_cat_dorp_down.append($("<select>Sub Category :</select>").addClass("form-control sub1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:first").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        $( ".sub1 option:selected" ).each(function() {
                          subcategory_id = $(this).val();
                        });
                            that.loadSection(null,category_id,subcategory_id,4);
                            
                    });
                    return sub_cat_dorp_down;
                }
                
                this.getSection_DropDown = function (){
                    sec_dorp_down =$("<div></div>").addClass("form-group col-lg-12");
                    sec_dorp_down.append($("<label>Section :</label>"))
                    sec_dorp_down.append($("<a></a>").attr({'id':'sec_img_loading'}));
                    sec_dorp_down.append($("<select></select>").addClass("form-control sec1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                        $( ".sec1 option:selected" ).each(function() {
                            section_id = $( this ).val();
                        });
                            that.loadTopic(null,category_id,subcategory_id,section_id,2);
                    });
                    return sec_dorp_down;
                }
                
                this.getTopic_DropDown = function (){
                    top_dorp_down =$("<div></div>").addClass("form-group col-lg-12");
                    top_dorp_down.append($("<label>Topic :</label>"))
                    top_dorp_down.append($("<a></a>").attr({'id':'top_img_loading'}));
                    top_dorp_down.append($("<select></select>").addClass("form-control top1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                        var topic_id = top_dorp_down.find(".top1 option:selected").val();
                        $( ".top1 option:selected" ).each(function() {
                            topic_id = $( this ).val();
                        });
                        onlyQuestion["category_id"]=category_id;
                        onlyQuestion["subcategory_id"]=subcategory_id;
                        onlyQuestion["section_id"]=section_id;
                        onlyQuestion["topic_id"]=topic_id;
                    });
                    return top_dorp_down;
                }
                
                this.getTextEditor = function (editor){
                    var text_editor =$("<div></div>").addClass("pad col-lg-12");
                    text_editor.append($("<form></form>").append($("<textarea></textarea>").attr({'id':editor,'name':editor,'rows':'8','cols':'40'})))
                    return text_editor;
                }
                
                this.getQuestionBox = function (){
                    var question_box =$("<div></div>").addClass("box box-solid");
                    question_box.append($("<div></div>").addClass("box-header with-border").append($("<h3 class='box-title'>Create Question</h3>")));
                    var question_box_body=$("<div></div>").addClass("box-body").append($("<div></div>").addClass("btn-group").css({'width':'100%','margin-bottom':'10px'}));
                    var question_input=$("<div></div>").addClass("input-group")
                            .append($("<input/>").addClass("form-control").attr({'id':'new-event','type':'text','placeholder':'Enter the number of Question'})
                            .keyup(function(event) {
                                        if (event.keyCode==13) {
                                            if($("#new-event").val()!="")
                                            that.addQuestionBox(parseInt($("#new-event").val()));    
                                        }
                                    }))
                            .append($("<div></div>").addClass("input-group-btn")
                            .append($("<button>Add</button>").addClass("btn btn-primary btn-flat").attr({'id':'add-new-event','type':'button'}).css({'border-color':'rgb(0, 31, 63)','background-color':' rgb(0, 31, 63)'})
                            .click(function(){
                                that.addQuestionBox(parseInt($("#new-event").val()));    
                            })
                            .keyup(function(event) {
                                if (event.keyCode==13) {
                                    that.addQuestionBox(parseInt($("#new-event").val()));    
                                }
                            })
                            ));
                    question_box_body.append(question_input);
                    question_box_body.append($("<p></p>"));
                    ul_question_box_body=$("<div></div>").addClass("text-center");
                    question_box_body.append(ul_question_box_body);
                    question_box.append(question_box_body);
                    return question_box;
                }
                
                this.getLanBox = function (){
                    lang_drop_down =$("<div></div>").addClass("form-group col-lg-12");
                    lang_drop_down.append($("<label>Select Language :</label>"))
                    lang_drop_down.append($("<select>:</select>").addClass("form-control lang1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        //sub_cat_dorp_down.find(".lang1").trigger("change");
                        ul_question_box_body.find("a").each(function(){
                            if(onlyQuestion["language_details"][that.getLangId()]["question_details"][$(this).attr('question_no')]["qus"]!=null){
                                if(!$(this).hasClass("btn-btn-google")){
                                    $(this).removeClass("btn-google")
                                    $(this).addClass("btn-google-green")
                                }
                            }else{
                                if($(this).hasClass("btn-google-green")){
                                    $(this).removeClass("btn-google-green")
                                    $(this).addClass("btn-google")
                                }
                            }
                            
                        }); 
                    });
                    return lang_drop_down;
                }
                
                this.getLangId=function (){
                    var lang_id;
                        $( ".lang1 option:selected" ).each(function() {
                            lang_id = $( this ).val();
                        });
                        return lang_id;
                }

                this.addQuestionBox = function (no){
                    ul_question_box_body.empty();
                    $.each(onlyQuestion["language_details"],function(j){
                                    delete onlyQuestion["language_details"][j]["question_details"];
                    });
                            for(var kk=1;kk<=no;kk++){
                                $.each(onlyQuestion["language_details"],function(j){
                                    if(onlyQuestion["language_details"][j]["question_details"]==null){
                                        onlyQuestion["language_details"][j]["question_details"]={}
                                    }
                                    onlyQuestion["language_details"][j]["question_details"][kk]={}
                                });
                                ul_question_box_body.append(that.getButton(kk));
                            }
                }
                
                this.getButton=function (kk){
                    var a=$("<a></a>").addClass("btn btn-social-icon btn-google")
                                        .attr({'id':'ques_box','question_no':kk}) 
                                        .css({'margin-right':'5px','margin-top':'5px'})
                                        .append($("<i></i>").append(kk))
                                        .click(function(){
                                            temp_question_button=$(this);
                                            temp_question_button.empty();
                                            try {temp_question_button.tooltip('hide');}catch(e){}
                                            that.setQuestionForm($("#hidden"),kk);
                                        });
                    return a;
                }
                
                this.addOption = function (no){
                    ul_question_box_body.empty();
                    question={};
                    for(var i=1;i<=no;i++){
                        question[i]={}
                        ul_question_box_body.append($("<a class='btn btn-social-icon btn-bitbucket' id='user' data-toggle='tooltip' data-placement='top' title='' data-original-title='Question Details' style='margin-right:5px;margin-top:5px;'><i>"+i+"</i></a>").click(function (){
                            that.setInsForm($("#hidden"),i);
                        }));
                    }
                }
                
                this.getBoxbody = function (){
                    var div =$("<div></div>").addClass("box-body");
                    return div;
                }
                
                
                this.getImgRow = function (data){
                    var row = $("<tr></tr>").attr({'id':data.url})
                            .append($("<td></td>").append(data.image))
                            .append($("<td></td>").append(that.getImgDropdown(data.url))
                    );
                    return row;            
                }
                
                this.getCatOption = function (data){
                    var row = $("<option></option>").attr({'value':data.category_id})
                              .append($("<td>"+data.category_name+"</td>"));
                    return row;            
                }
                
                this.getLangOption = function (data){
                    if(onlyQuestion["language_details"]==null){
                        onlyQuestion["language_details"]={};
                        onlyQuestion["language_details"][data.language_id]={};
                    }else{
                        if(onlyQuestion["language_details"][data.language_id]==null){
                           onlyQuestion["language_details"][data.language_id]={};
                        }
                    }
                    var row = $("<option></option>").attr({'value':data.language_id})
                              .append($("<td>"+data.language_name+"</td>"));
                      if(check_lag_default>0){
                          dorp_down_for_lang_copy_setting.append(that.getPasteLangList(data.language_id,data.language_name));
                      }else{
                          default_lang_id=data.language_id;
                      }
                      check_lag_default++;
                    return row;            
                }
                
                this.getPasteLangList = function (language_id,language_name){
                    return $("<li></li>").append($("<a>"+language_name+"</a>")).click(function (){
                        if(that.checkDefault_langData()){
                            onlyQuestion["language_details"][language_id]["direction_details"]=onlyQuestion["language_details"][default_lang_id]["direction_details"];
                            onlyQuestion["language_details"][language_id]["question_details"]=onlyQuestion["language_details"][default_lang_id]["question_details"];
                        }else{
                            var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Error !","Please fill the data in default language.");
                            $("#hidden").append(error_model);
                            error_model.modal('show');
                            error_model.find(".modal-footer").find("button").first().remove();   
                            error_model.find(".modal-footer").find("button").text("Ok");   
                        }
                    });
                }
                this.checkDefault_langData = function (){
                    var b=true;
                    $.each(onlyQuestion["language_details"], function(key,value){
                        if(default_lang_id==key){
                            var lang=config.getModel("Language").getLanguage(key);
                            if(!b){
                                return false;
                            }
                            if(value["question_details"]!=null){
                                $.each(value["question_details"], function(k,v){
                                    if(v["qus"]==null){
                                        b=false;
                                        return false;
                                    }
                                });
                            }else{
                                b=false;
                                return false;
                            }
                        }
                    });
                    return b;
                }
                
                
                this.getSubOption = function (data){
                    var row = $("<option></option>").attr({'value':data.subcategory_id})
                              .append($("<td>"+data.subcategory_name+"</td>"));
                    return row;            
                }
                
                this.getSecOption = function (data){
                    var row = $("<option></option>").attr({'value':data.section_id})
                              .append($("<td>"+data.section_name+"</td>"));
                    return row;            
                }
                
                this.getTopOption = function (data){
                    var row = $("<option></option>").attr({'value':data.topic_id})
                              .append($("<td>"+data.topic_name+"</td>"));
                    return row;            
                }
                
                this.getImgDropdown = function (a){
                    var drop_down =$("<div></div>").addClass("input-group-btn")
                                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                        .append($("<span></span>").addClass("fa fa-caret-down")))
                                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li></li>").append($("<a>Copy</a>")))
                                            );
                    return that.getImgClickEvent(drop_down,a);
                }
                
                this.getImgClickEvent =function (drop_down,onlyQuestion_id){
                    $(drop_down).find('ul>li:nth-child(1)').click(function (){
                        window.prompt("Copy to clipboard: Ctrl+V, Enter", onlyQuestion_id);
                    });
                    return drop_down;
                }
                
                this.getFooter =function (){
                    var div = $("<div></div>").addClass("form-group pull-right col-md-3 no-padding");
                        div.append($("<button></button>").addClass("btn btn-block btn-primary")
                                    .append("Save").click(function ()
                                        {
                                            var b=true;
                                            temp_save_button=$(this);
                                            var msg;
                                            $.each(onlyQuestion["language_details"], function(key,value){
                                                var lang=config.getModel("Language").getLanguage(key);
                                                if(!b){
                                                    return false;
                                                }
                                                if(value["question_details"]!=null){
                                                    $.each(value["question_details"], function(k,v){
                                                        if(v["qus"]==null){
                                                            b=false;
                                                            msg="Should not blank Question no "+k+" of "+lang+" language.";
                                                            return false;
                                                        }
                                                    });
                                                }else{
                                                    b=false;
                                                    msg="Please Create Atleast one Question "+lang+" language.";
                                                    return false;
                                                }
                                            });
                                            if(b){
                                                $.each(listeners, function(i){
                                                    listeners[i].insOnlyQuestion(onlyQuestion);
                                                });
                                            }else{
                                                var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Error !",msg);
                                                $("#hidden").empty();
                                                $("#hidden").append(error_model);
                                                error_model.modal('show');
                                                error_model.find(".modal-footer").find("button").first().remove();
                                            }
                                        
                                        }
                                     )
                                )
                        return div;
                }
                
                this.getDropdown = function (a){
                    dorp_down_for_lang_copy_setting=$("<ul></ul>").addClass("nav nav-list ");
                    var drop_down =$("<div></div>").addClass("input-group-btn pull-right").css({'margin-right':'30px'})
                                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                        .append($("<span></span>").addClass("fa fa-caret-down")))
                                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li>").css({'padding-left':'10px'}).append("Paste To").append(dorp_down_for_lang_copy_setting))
                                            .append($("<li></li>").addClass("divider"))
                                            .append($("<li>").css({'padding-left':'10px'}).append("Comming Soon"))
                            );
                    return drop_down;
                }
                
                
                this.getQusImgBoxFooter =function (){
                    var div = $("<div></div>").addClass("box-footer clearfix");
                        div.append($("<div></div>").addClass("col-md-6 col-xs-6")
                                .append($("<button></button>").addClass("btn btn-block btn-primary")
                                    .append("Upload").click(function ()
                                        {
                                            config.getModel("QusUploadImg")._startUpload();
                                        }
                                     )
                                    ))
                        div.append($("<div></div>").addClass("col-md-6 col-xs-6 pull-right")
                                .append($("<button></button>").addClass("btn btn-block btn-primary")
                                    .append("Refresh").click(function ()
                                        {
                                            config.getModel("QusUploadImg")._refresh();
                                        }
                                     )
                                )
                        )
                        return div;
                }
                
                this.setQuestionForm =function (a,kk){
                    temp_question_button.empty();
                    temp_question_button.append(config.getLoadingData());
                    while(check){}
                    temp_question_button.empty();
                    temp_question_button.append($("<i>").append(kk));
                    if(config.getView("QuestionVgr")==null){
                        config.getView("MainVgr").getListener().loadQuestionVgr(kk,"");
                    }else{
                        that.setInsForm(a,kk);
                    }
                }
                
                this.setInsForm=function (a,kk){
                    temp_question_button.empty();
                    temp_question_button.append($("<i></i>").append(kk));
                    var model = config.getView("ModelVgr").getModel("New Question",that.getInsForm(kk),"submit");
                    a.empty();
                    a.append(model);
                    var editor=CKEDITOR.replace( 'editor2', {
                            extraPlugins: 'mathjax',
                            mathJaxLib: 'http://cdn.mathjax.org/mathjax/2.6-latest/MathJax.js?config=TeX-AMS_HTML'
                    });
                    $.fn.modal.Constructor.prototype.enforceFocus = function () {
                        modal_this = this
                        $(document).on('focusin.modal', function (e) {
                        if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                        // add whatever conditions you need here:
                        &&
                        !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')
                        && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_textarea')) {
                                modal_this.$element.focus()
                            }
                        })
                    };
                    var obj = {
                            // Valid file formats
                            support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                            form: "demoFiler1", // Form ID
                            dragArea: "dragAndDropFiles1", // Upload Area ID
                            multiUpload:"multiUpload1",
                            url:config.getU()+"/Img",
                            parent_obj:config.getView("QuestionVgr"),
                            strlenght:10,
                            module:2
                    }
                    config.setModel("QusUploadImg",new $.UploadImg(obj));
                    var obj=onlyQuestion["language_details"][that.getLangId()]["question_details"][kk];
                    if(obj["qus"]!=null){
                        editor.setData(decodeURIComponent(obj["qus"]));
                        config.getView("QuestionVgr").addOption(Object.keys(obj["option_details"]).length-1);
                        $.each(obj["option_details"],function(j){
                            $("#"+j).val(decodeURIComponent(obj["option_details"][j]));
                        });
                        $("#option").val(decodeURIComponent(obj["option_details"]["answer"]));
                        if(obj["img_details"]!=null){
                            $.each(obj["img_details"],function(key,val){
                            item={};
                            item["url"]=key;
                            item["image"]=val;
                            config.getView("QuestionVgr").addImgQuestion(item);
                        });
                        }
                    }
                    var sub=false;
                    var onlyQuestion_form =$('#onlyQuestion_form');
                    onlyQuestion_form.find("#submit").click(function(){
                                that.removeFail();
                                var check=true;
                                var msg ;
                                var option_len=0;
                                onlyQuestion_form.find(":input").each(function(){
                                    if($(this).attr("name")!="op"&&$(this).attr("id")!="editor2"&&$(this).attr("name")!="file[]"&&$(this).attr("name")!=null&&$(this).attr("name")!="option"){
                                        option_len++;
                                    }
                                }); 
                                if(editor.getData()==""){
                                    check=false;
                                    msg= "Question Field should not blank";
                                }else if(option_len==0){
                                    msg= "Please Create Option Field.";
                                    check=false;
                                }else{
                                    onlyQuestion_form.find(":input").each(function(){
                                        if($(this).attr("name")!="op"&&$(this).attr("id")!="editor2"&&$(this).attr("name")!="file[]"&&$(this).attr("name")!=null&&$(this).attr("name")!="option"){
                                            if($(this).val()==""){
                                                msg= "Please Option Field "+$(this).attr("name")+" should not blank.";
                                                check=false;
                                                return false;
                                            }
                                        }
                                    }); 
                                }
                                if(check){
                                    onlyQuestion["language_details"][that.getLangId()]["question_details"][kk]["qus"]=encodeURIComponent(editor.getData());
                                    onlyQuestion_form.find(":input").each(function(){
                                    if(onlyQuestion["language_details"][that.getLangId()]["question_details"][kk]["option_details"]==null){
                                       onlyQuestion["language_details"][that.getLangId()]["question_details"][kk]["option_details"]={};    
                                    }
                                    if($(this).attr("name")!="op"&&$(this).attr("id")!="editor2"&&$(this).attr("name")!="file[]"&&$(this).attr("name")!=null&&$(this).attr("name")!="option"){
                                            onlyQuestion["language_details"][that.getLangId()]["question_details"][kk]["option_details"][$(this).attr("name")]=encodeURIComponent($(this).val());
                                        }
                                    }); 
                                    onlyQuestion["language_details"][that.getLangId()]["question_details"][kk]["option_details"]["answer"]=encodeURIComponent(onlyQuestion_form.find("#option").val());
                                    onlyQuestion["language_details"][that.getLangId()]["question_details"][kk]["img_details"]={};
                                    $.each(config.getView("QuestionVgr").getObj(),function(key,val){
                                        onlyQuestion["language_details"][that.getLangId()]["question_details"][kk]["img_details"][key]=val;
                                    });
                                    $('#myModal').modal('toggle');
                                    temp_question_button.removeClass('btn-google');
                                    temp_question_button.addClass('btn-google-green');
                                }else{
                                        var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Error !",msg);
                                        $("#hidden").append(error_model);
                                        error_model.modal('show');
                                        error_model.find(".modal-footer").find("button").first().remove();
                                }
                     });
                    $('#myModal').modal('show');
                    model.find(".modal-footer").find("button").first().empty();
                    model.find(".modal-footer").find("button").first().append("Add in Queue");
                }
                
                this.getInsForm =function (kk){
//                    var div=$("<div></div>");
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'onlyQuestion_form'}).submit(function(e) {e.preventDefault();})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                        .append($("<label></label>").addClass("control-label").attr({'for':'onlyQuestion_name'}).append("Question No. is "+kk))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls")
                                        .append(that.getTextEditor("editor2"))
                                        )));
//                    div.append(form);
                    form.append(config.getView("QuestionVgr").getQuestion());
                    return form;        
                }
                
                this.addCategory=function (data){
                   cat_dorp_down.find(".select2").append(that.getCatOption(data))
                }          
                
                this.addLanguage=function (data){
                   lang_drop_down.find(".select2").append(that.getLangOption(data))
                }          
                
                this.addSubCategory=function (data){
                   sub_cat_dorp_down.find(".sub1").append(that.getSubOption(data))
                }          
                
                this.addSection=function (data){
                   sec_dorp_down.find(".sec1").append(that.getSecOption(data))
                }          
                
                this.addTopic=function (data){
                   top_dorp_down.find(".top1").append(that.getTopOption(data))
                }          
                
                this.insLoadBegin =function (){
                    temp_save_button.empty();
                    temp_save_button.append(config.getLoadingData());
                }
                
                this.insLoadFinish =function (){
                    temp_save_button.empty();
                    temp_save_button.append("Save");
                    $("body").find("#hidden").find("#error_msg").remove();
                    var msg= "Your Data has been successfully saved.";
                    var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Success.!",msg);
                    error_model.find(".modal-footer").find("button").first().remove();
                    error_model.find(".modal-footer").find("button").empty();
                    error_model.find(".modal-footer").find("button").append("Ok");
                    $("#hidden").append(error_model);
                    error_model.modal('show');
                }
                
                this.insLoadFail = function() { 
                    temp_save_button.empty();
                    temp_save_button.append("Server Error");
                }
                
                this.removeFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.onlyQuestionSubLoadBegin =function (){
                    sub_cat_dorp_down.find("#sub_img_loading").append(config.getLoadingData());
                }
                
                this.onlyQuestionSecLoadBegin =function (){
                    sec_dorp_down.find("#sec_img_loading").append(config.getLoadingData());
                }
                
                this.onlyQuestionTopLoadBegin =function (){
                    top_dorp_down.find("#top_img_loading").append(config.getLoadingData());
                }
                
                this.onlyQuestionLangLoadBegin =function (){
                    lang_drop_down.find("#lang_img_loading").append(config.getLoadingData());
                }
                
                this.onlyQuestionSubLoadFinish=function (){
                    sub_cat_dorp_down.find("#sub_img_loading").empty();
                    setTimeout(function (){
                        sub_cat_dorp_down.find(".sub1").trigger("change");
                    },20);
                }
                
                this.onlyQuestionSecLoadFinish=function (){
                    sec_dorp_down.find("#sec_img_loading").empty();
                    setTimeout(function (){
                        sec_dorp_down.find(".sec1").trigger("change");
                    },20);
                }
                
                this.onlyQuestionTopLoadFinish=function (){
                    top_dorp_down.find("#top_img_loading").empty();
                    setTimeout(function (){
                        top_dorp_down.find(".top1").trigger("change");
                    },20);
                }
                
                this.onlyQuestionLangLoadFinish=function (){
                    lang_drop_down.find("#lang_img_loading").empty();
                    b=false;
                }
                
                this.onlyQuestionSubLoadFail = function() { 
                    sub_cat_dorp_down.find("#sub_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.onlyQuestionSecLoadFail = function() { 
                    sec_dorp_down.find("#sec_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.onlyQuestionTopLoadFail = function() { 
                    top_dorp_down.find("#top_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.onlyQuestionLangLoadFail = function() { 
                    lang_drop_down.find("#lang_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.loadCategory =function (t){
                    var all = config.getModel("Category").selCategory();
                        cat_dorp_down.find(".cat1").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addCategory(all[item]);
                                });
                        }
                } 
                
                this.loadLanguage =function (module){
                    var all = config.getModel("Language").selLanguage(module);
                    lang_drop_down.find(".lang1").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addLanguage(all[item]);
                                });
                         }
                    
                } 
                
                this.loadSubCategory =function (t,category_id,module){
                    sub_cat_dorp_down.find(".sub1").empty();
                    var all = config.getModel("SubCategory").selSubCategory(category_id,module);
                    if(all){
                        $.each(all, function(item){
                            that.addSubCategory(all[item]);
                        });
                        sub_cat_dorp_down.find(".sub1").trigger("change");
                    }
                } 
                
                this.loadSection =function (t,category_id,subcategory_id,module){
                        sec_dorp_down.find(".sec1").empty();
                        var all = config.getModel("Section").selSection(category_id,subcategory_id,module);
                        if(all){
                                $.each(all, function(item){
                                    that.addSection(all[item]);
                                });
                                sec_dorp_down.find(".sec1").trigger("change");
                         }
                } 
                
                this.loadTopic =function (t,category_id,subcategory_id,section_id,module){
                        top_dorp_down.find(".top1").empty();
                        var all = config.getModel("Topic").selTopic(category_id,subcategory_id,section_id,module);
                         if(all){
                                $.each(all, function(item){
                                    that.addTopic(all[item]);
                                });
                                top_dorp_down.find(".top1").trigger("change");
                         }
                } 
                
                //this is code run when onlyQuestion subcatgory detial does not have in cache object and this funcation call inside the class manager of sub category
                this.subAddInOnlyQuestion =function (item){
                    that.addSubCategory(item);
                } 
                //this is code run when Section detial does not have in cache object and this funcation call inside the class manager of Section
                this.secAddInOnlyQuestion =function (item){
                    that.addSection(item);
                } 
                //---------------------------------------------------------------------------------------------------------------------------------
                this.topAddInOnlyQuestion =function (item){
                    that.addTopic(item);
                } 
                //-------------------------------------------------------------------------------------------------------------------------------------
                

                this.addListener = function(list){
                            listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	OnlyQuestionVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insOnlyQuestion : function(from){},
			selOnlyQuestion : function() { },
                        delOnlyQuestion : function(id) { },
                        getOnlyQuestion : function(id) { },
                        updOnlyQuestion : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
