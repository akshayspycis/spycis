jQuery.extend({
	UserDetailsVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody;
                var viewTest;
                var row_id;
                var cat_dorp_down;
                var sub_cat_dorp_down;
                var sec_dorp_down;
                var top_dorp_down;
                var lang_dorp_down;
                var sel_sub_cat;
                var sel_sec;
                var section_ul;
                var divasas;
                var b=true;
                var ul_search_box_body;
                var text_editor;
                test_obj={};
                test_obj["question_details"]={};
                
                this.getUserDetailsVgr = function(){
                    config.getView("ContentWrapperVgr").setBoxHeader("User Management");
                    var div=$("<div></div>");
                    var box=that.getBoxbody();
                    box.append(that.getSearchBox());
                    var table= that.getTable();
                    tbody=table;
                    box.append(table);
                    div.append(box);
//                    div.append(that.getBoxFooter());
                    that.loadUserDetails();
                    return div;
		}

                this.getPanelTitle = function (){
                    var div =$("<div></div>").addClass("box-header ");
                    div.append($("<h3></h3>").addClass("box-title").append($("<b>UserDetails</b>")));
                    return div;
                }
                this.getBoxbody = function (){
                    var div =$("<div></div>").addClass("col-md-12");
                    return div;
                }
                
                this.getTable = function (){
                    return $("<div>").addClass("row");
                }
                this.getSearchBox = function (){
                    var search_box_body=$("<div></div>").addClass("box-body with-border").append($("<div></div>").addClass("col-md-2").css({'padding':'5px'}).append($("")));
                    var search_input=$("<div></div>").addClass("input-group col-md-3 pull-right")
                            .append($("<input/>").addClass("search-query form-control").attr({'id':'new-event','type':'text','placeholder':'Search'}))
                            .append($("<span></span>").addClass("input-group-btn")
                            .append($("<button></button>").addClass("btn btn-primary btn-flat").attr({'id':'add-new-event','type':'button'}).css({'border-color':'#015A9F','background-color':'#015A9F'})
                            .append($("<span></span>").addClass("glyphicon glyphicon-search"))
                             ));
                    search_box_body.append(search_input);
                    search_box_body.append($("<p></p>"));
                    return search_box_body;
                }
            
            
                
                this.setTestForm =function (a,kk){
                    if(config.getView("TestVgr")==null){
                        config.getView("MainVgr").getListener().loadTestVgr(kk);
                    }else{
                        that.setInsForm(a,kk);
                    }
                }
                
                
                this.setInsForm=function (a){
//                    $('#myModal').modal('toggle');
                    var model = config.getView("ModelVgr").getModel("New Test",that.getInsForm(),"submit");
                    a.empty();
                    a.append(model);
                    var form=config.getView("CreateTestVgr").getForm();
                    var viewTest_form =$('#viewTest_form');
                    viewTest_form.find("#submit").click(function(){
                    that.removeFail();
                     var create_test_obj=config.getView("CreateTestVgr").getObj();
                     if(config.getModel("CreateTest")==null){
                         $.getScript("../model/CreateTest.js").done(function(){
                                try {
                                        config.setModel("CreateTest",new $.CreateTest());
                                        $.getScript("../controller/CreateTestMgr.js").done(function(){
                                        try {
                                                config.setController("CreateTestMgr",new $.CreateTestMgr(config,config.getModel("CreateTest"),that));
                                                $.each(listeners, function(i){try {listeners[i].insCreateTest(create_test_obj);}catch(e){}});
                                            }catch(e){
                                                alert(e+" problem in create test mgr ");
                                            }
                                        }).fail(function() {alert("Error: Problem in CreateTestMgr.js file ");}); 
                                }catch(e){
                                        alert(e+" problem in create test.js file ");
                                }
                       }).fail(function() {alert("Error: Problem in headervgr.js file ");}); 
                     }else{
                              $.each(listeners, function(i){try {listeners[i].insCreateTest(create_test_obj);}catch(e){}});
                     }
                     });
                   model.find(".modal-footer").find("button").remove();
                    $('#myModal').modal('show');
                }
                
                this.setUpdForm=function (user_id){
                    var model = config.getView("ModelVgr").getModel("View UserDetails",that.getUpdForm(user_id),"submit_user_details");
                    $("#hidden").empty();
                    $("#hidden").append(model);
                    var sub=false;
                    var submit_user_details =$('#submit_user_details');
                    submit_user_details.validate({
                            rules: {
                               user_name: {minlength: 3,required: true,required: true},
                               password: {minlength: 3,required: true,required: true},
                               dob: {minlength: 3,required: true,required: true},
                               contact_no: {minlength: 3,required: true,required: true},
                                    agree: "required"
                               },
                                highlight: function(element) {$(element).closest('.control-group').removeClass('success').addClass('error');sub=false;},
                                success: function(element) {element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');sub=true;}
                    });
                    
                    submit_user_details.find("#submit_user_details").click(function(){
                        if(sub){
                                that.removeFail();
                                $.each(listeners, function(i){
                                    listeners[i].updUserDetails(submit_user_details);
                                });
                                sub=false;
                                $('#myModal').modal('toggle');
                        }
                     });
                    $('#myModal').modal('show');
                }
                
                this.getInsForm =function (){
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'viewTest_form'}).css({'margin':'-5px'});
                    form.append(config.getView("CreateTestVgr").getCreateTest(test_obj));
                    return form;        
                }
                
                this.getUpdForm =function (user_id){
                    var data ;
                    $.each(listeners, function(i){
                        data=listeners[i].getUserDetails(user_id);
                    });
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'submit_user_details'})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-6")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'user_name'}).append("User Name"))
                                            .append($("<p></p>"))
                                            .append($("<div></div>").addClass("controls")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'user_name','placeholder':'UserName','name':'user_name','value':data.user_name}))
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'user_id','name':'user_id','value':user_id}))
                                            )
                                   )
                                  .append($("<div></div>").addClass("form-control-group col-lg-6")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'viewTest_name'}).append("Email"))
                                            .append($("<p></p>"))
                                            .append($("<div></div>").addClass("controls")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'email','id':'email','placeholder':'Email Address','name':'email','value':data.email}))
                                            )
                                   )
                                   .append($("<div></div>").addClass("form-control-group col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'viewTest_name'}).append("Date Of Birth"))
                                            .append($("<p></p>"))
                                            .append($("<div></div>").addClass("controls")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'date','id':'dob','name':'dob','value':data.dob}))
                                            )
                                   )
                                  .append($("<div></div>").addClass("form-control-group col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'viewTest_name'}).append("Contact"))
                                            .append($("<p></p>"))
                                            .append($("<div></div>").addClass("controls")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'number','id':'contact_no','placeholder':'Contact No','name':'contact_no','value':data.contact_no}))
                                            )
                                   )
                                  .append($("<div></div>").addClass("form-control-group col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'viewTest_name'}).append("Password"))
                                            .append($("<p></p>"))
                                            .append($("<div></div>").addClass("controls")
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'password','id':'password','name':'viewTest_name','value':data.password}))
                                            )
                                   )
                                 );
                    return form;        
                }
                
                this.addUserDetails =function (data){
                   tbody.append(that.getRow(data))
                }   
                
               
                this.getActionButton =function (title,dis,icon,css)    {
                    
                    return $("<a></a>").addClass("btn btn-primary btn-sm").attr({'data-toggle':'tooltip','title':dis}).css({'margin-left':css,'cursor':'pointer'}).append($("<i></i>").addClass(icon)).append("&nbsp;"+title);
                }
                
                this.getRow = function (data){
                    var row = $("<div>").addClass("col-lg-3")
                            .append($("<div>").addClass("box")
                                    .append($("<div>").addClass("box-header")
                                            .append($("<center>").addClass("box-header")
                                                .append($("<div>").css({'width':'90px','height':'90px','border-radius':'50%','display':'block','border':'1px solid rgb(94, 83, 92)','background-position-y':'25%'})
                                                    .append($("<img>").addClass("img-circle").attr({'width':'88px','height':'88px','src':data.pic}).css({'color':'#5E535C'})))
                                                    .append($("<h3>").addClass("profile-username text-center no-margin").append(data.user_name))
                                                    .append($("<p>").addClass("text-muted text-center").append(data.contact_no))
                                                  )
                                            .append($("<div>").addClass("box-tools pull-right")
                                                    .append($("<button>").addClass("btn btn-box-tool").attr({'type':'button','data-widget':'remove'}).append($("<i>").addClass("fa fa-trash")))
                                                    .append($("<button>").addClass("btn btn-box-tool").attr({'type':'button','data-toggle':'dropdown','aria-expanded':'false'})
                                                        .append($("<span>").addClass("caret"))
                                                        .append($("<span>").addClass("sr-only").append("Toggle Dropdown"))
                                                    )
                                                 .append(that.getDropdown(data.user_id))
                                                )
                                            
                                        .append($("<div>").addClass("box-body").append($("<a>").addClass("btn btn-primary btn-block").append($("<b>").append("View Profile")).click(function (){
                                            that.setUpdForm(data.user_id);
                                         })))
                                     )
                            );
                    return row;            
                }
                this.getDropdown = function (a){
                    var drop_down =$("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li></li>").append($("<a>Delete</a>")))
                                            .append($("<li></li>").addClass("divider"))
                                            .append($("<li></li>").append($("<a>Edit</a>")));
                    return drop_down;
                }
                
                this.loadDiscriptionDetails = function (a,discription_id,discription_bank_id){
                    if(config.getModel("DirectionDetails")==null){
                        $.getScript("../model/DirectionDetails.js").done(function() {
                                try{
                                    config.setModel("DirectionDetails",new $.DirectionDetails());
                                    $.getScript("../controller/DirectionDetailsMgr.js").done(function() {
                                    try{
                                        config.setController("DirectionDetailsMgr",new $.DirectionDetailsMgr(config,config.getModel("DirectionDetails"),that));
                                        $.each(listeners, function(i){
                                            try {
                                                listeners[i].selDirectionDetails(discription_id,discription_bank_id,a);
                                            }catch(e){}
                                        });
                                        
                                    }catch(e){
                                        alert(e+" in load Direction Details Mgr");
                                    }
                                    }).fail(function() {alert("Error:Direction DetailsMgr Js Loading problem");}); 
                                }catch(e){
                                    alert(e+" in load Direction Details");
                                }
                            }).fail(function() {alert("Error:Direction Details Js Loading problem");}); 
                    }else{
                        $.each(listeners, function(i){
                                            try {
                                                listeners[i].selDirectionDetails(discription_id,discription_bank_id,a);
                                            }catch(e){}
                                        });
                    }
                }
                
                this.addUser = function (test_id,obj){
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("UserListVgr")==null){
                        $.getScript("../view/UserListVgr.js").done(function(){
                            config.setView("UserListVgr",new $.UserListVgr(config));
                            var model = config.getView("ModelVgr").getModel("Invite your User to invite Test",config.getView("UserListVgr").getUserList(test_id),"submit");
                            obj.empty();
                            obj.append($('<i class="fa fa-user-plus"></i>'));
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.modal('show');
                                $.getScript("../model/UserList.js").done(function() {
                                    config.setModel("UserList",new $.UserList(config));
                                    $.getScript("../controller/UserListMgr.js").done(function() {
                                        config.setController("UserListMgr",new $.UserListMgr(config,config.getModel("UserList"),config.getView("UserListVgr")));
                                        config.getView("UserListVgr").loadUserList();
                                    }).fail(function() {alert("Error :problem in UserListMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in UserListvgr.js file ");}); 
                        }).fail(function() {alert("Error:004 problem in UserListVgr.js file ");}); 
                     }else{
                              var model = config.getView("ModelVgr").getModel("Invite your User to invite Test",config.getView("UserListVgr").getUserList(test_id),"submit");
                              obj.empty();
                              obj.append($('<i class="fa fa-user-plus"></i>'));
                              $("hidden").empty();
                              $("hidden").append(model);
                              model.modal('show');
                              config.getView("UserListVgr").loadUserList();
                              
                     }
                }
                
                this.updVisibility  =function (user_id,subuser_id,test_id,visible){
                    
                    $.each(listeners, function(i){
                        try{listeners[i].updVisibility(user_id,subuser_id,test_id,visible);}catch(e){alert(e)}
                    });
                }
                
                this.addUserDetails=function (data){
                   cat_dorp_down.find(".select2").append(that.getCatOption(data))
                }          
                
                this.addUserDetails =function (personne,value){
                   tbody.append(that.getRow(personne,value))
                }     
                
                this.addSubUserDetails=function (data){
                   sub_cat_dorp_down.find(".sub1").append(that.getSubOption(data))
                }          
                
//                this.setSelectedUserDetails =function (user_id,subuser_id,section_id){
//                    cat_dorp_down.find(".cat1").val(user_id);
//                    b=true; 
//                    that.loadSubUserDetails(null,user_id,3);
//                    while(b){}
//                    sub_cat_dorp_down.find(".sub1").val(subuser_id);
//                    sub_cat_dorp_down.find(".sub1").trigger("change");
//                }
                
                this.updUserDetailsRow =function (data){
                    $("#"+data.viewTest_id).find("td:nth-child(2)").empty();
                    $("#"+data.viewTest_id).find("td:nth-child(2)").append(data.viewTest_name);
                }                                                        
                
                this.insLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.insLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                }
                
                this.insLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                this.delLoadBegin =function (){
                    $("#"+row_id).find("td:nth-child(2)").empty();
                    $("#"+row_id).find("td:nth-child(2)").append(config.getLoadingData());
                }
                
                this.delLoadFinish =function (){
                    $("#"+row_id).remove();
                }
                
                this.delLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                this.removeFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                this.setError = function(message) { 
                    config.getView("ModelVgr").setError(message);
                }
                
                this.selLoadBegin =function (){
                    tbody.append(config.getLoadingData());
                }
                
                this.viewTestSubLoadBegin =function (){
                    sub_cat_dorp_down.find("#sub_img_loading").append(config.getLoadingData());
                }
                
                this.selLoadFinish=function (){
                    tbody.empty();
                }
                
                this.viewTestSubLoadFinish=function (){
                    sub_cat_dorp_down.find("#sub_img_loading").empty();
                    b=false;
                }
                
                this.selLoadFail = function() { 
                    tbody.append("<p>")
                }
                
                this.viewTestSubLoadFail = function() { 
                    sub_cat_dorp_down.find("#sub_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.selRemoveFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.updLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.updLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                }
                
                this.updLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
            
                
                this.loadUserDetails =function (user_id,subuser_id){
                    tbody.empty();
                    $.each(listeners, function(i){
                        try{listeners[i].selAll_UserDetails(user_id,subuser_id);}catch(e){alert(e)}
                    });
                } 
                

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                this.setloading =function (){
                    viewTest.find("#img_loading").append(config.getLoadingData());
                }
                
                this.removeLoading =function (){
                    viewTest.find("#img_loading").empty();
                }
                
                this.setDisableButton =function (){
                    viewTest.find("#cancel").prop('disabled', true);
                }
                
                this.setEnableButton =function (){
                    viewTest.find("#cancel").prop('disabled', false);
                }
                
                this.setServerError =function (){
                    viewTest.find("#error_msg").append("Error: Server doesn't Respond.");
                }
                
                this.removeServerError =function (){
                    viewTest.find("#error_msg").empty();
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	UserDetailsVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        selUserDetails : function(){},
			updUserDetails : function() { },
                        getUserDetails : function() { }
		}, list);
	}
});
