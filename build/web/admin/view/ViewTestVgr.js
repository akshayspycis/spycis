jQuery.extend({
	ViewTestVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody;
                var viewTest;
                var row_id;
                var cat_dorp_down;
                var sub_cat_dorp_down;
                var sec_dorp_down;
                var top_dorp_down;
                var lang_dorp_down;
                var sel_sub_cat;
                var sel_sec;
                var section_ul;
                var divasas;
                var b=true;
                var ul_search_box_body;
                var text_editor;
                test_obj={};
                test_obj["question_details"]={};
                
                this.getViewTestVgr = function(){
                    config.getView("ContentWrapperVgr").setBoxHeader("User Management");
                    var div=$("<div></div>");
                    div.append(that.getLeftBox())
                    var box=that.getBoxbody();
                    var table= that.getTable();
                    box.append(that.getPanelTitle())
                    box.append(that.getSearchBox());
                    var table= that.getTable();
                    tbody=table;
                    box.append(table);
                    div.append(box);
                    div.append(that.getBoxFooter());
                    that.loadCategory();
                    cat_dorp_down.find(".cat1").trigger("change");
                    return div;
		}

                this.getPanelTitle = function (){
                    var div =$("<div></div>").addClass("box-header ");
                    div.append($("<h3></h3>").addClass("box-title").append($("<b>ViewTest</b>")));
                    return div;
                }
                
                this.getCategory_DropDown = function (){
                    cat_dorp_down =$("<div></div>").addClass("form-group col-lg-12");
                    cat_dorp_down.append($("<label>Category :</label>"))
                    cat_dorp_down.append($("<select>Category :</select>").addClass("form-control cat1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var id = cat_dorp_down.find(".cat1 option:first").val();
                            $( ".cat1 option:selected" ).each(function() {
                              id = $( this ).val();
                            });
                        that.loadSubCategory(null,id,8);
                    });
                    return cat_dorp_down;
                }
                
                this.getSubCategory_DropDown = function (){
                    sub_cat_dorp_down =$("<div></div>").addClass("form-group col-lg-12");
                    sub_cat_dorp_down.append($("<label>Sub Category :</label>"))
                    sub_cat_dorp_down.append($("<a></a>").attr({'id':'sub_img_loading'}));
                    sub_cat_dorp_down.append($("<select>Sub Category :</select>").addClass("form-control sub1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:first").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        $( ".sub1 option:selected" ).each(function() {
                          subcategory_id = $(this).val();
                        });
                        that.loadTest(category_id,subcategory_id);
                    });
                    return sub_cat_dorp_down;
                }
                
                this.getLeftBox = function (){
                    var LeftBox=$("<div></div>").addClass("col-md-3");
                    var box_solid1=$("<div></div>").addClass("box box-solid")
                            .append($("<div></div>").addClass("box-body no-padding").append($("<div></div>").addClass("box box-solid")
                                    .append(that.getCategory_DropDown())
                                    .append(that.getSubCategory_DropDown())
                            ));
                    LeftBox.append(box_solid1);
                    return LeftBox;
            }
            
                this.getSearchBox = function (){
                    var search_box_body=$("<div></div>").addClass("box-body with-border").append($("<div></div>").addClass("col-md-2").css({'padding':'5px'}).append($("<p>Test No</p>")));
                    var search_input=$("<div></div>").addClass("input-group")
                            .append($("<input/>").addClass("search-query form-control").attr({'id':'new-event','type':'text','placeholder':'Search'}))
                            .append($("<span></span>").addClass("input-group-btn")
                            .append($("<button></button>").addClass("btn btn-primary btn-flat").attr({'id':'add-new-event','type':'button'}).css({'border-color':'#015A9F','background-color':'#015A9F'})
                            .append($("<span></span>").addClass("glyphicon glyphicon-search"))
                             ));
                    search_box_body.append(search_input);
                    search_box_body.append($("<p></p>"));
                    return search_box_body;
                }
                
                this.getBoxbody = function (){
                    var div =$("<div></div>").addClass("col-md-6");
                    return div;
                }
                
                this.getTable = function (){
                    return $("<ul></ul>").addClass("timeline").css({'margin-left':'-24px'});
                }
                
                this.getCatOption = function (data){
                    var row = $("<option></option>").attr({'value':data.category_id})
                              .append($("<td>"+data.category_name+"</td>"));
                    return row;            
                }
                
                this.getSubOption = function (data){
                    var row = $("<option></option>").attr({'value':data.subcategory_id})
                              .append($("<td>"+data.subcategory_name+"</td>"));
                    return row;            
                }
                
                this.getBoxFooter =function (){
                    var div = $("<div></div>").addClass("box-footer clearfix");
//                        div.append($("<a></a>").addClass("btn pull-right").attr({'id':'img_loading'}));
//                        div.append($("<ul></ul>").addClass("pagination pagination-sm no-margin pull-right")
//                                .append($("<li></li>").append($("<a></a>").attr({'href':'#'}).append($("<i></i>").addClass("fa fa-arrow-left")))
//                                    .click(function ()
//                                        {
//                                            
//                                            
//                                        }
//                                     )
//                                    )
//                                .append($("<li></li>").append($("<a></a>").attr({'href':'#'}).append($("<i></i>").addClass("fa fa-arrow-right")))
//                                    .click(function ()
//                                        {
//                                            
//                                            
//                                        }
//                                     )
//                                    )
//                        );
//                        div.append($("<a></a>").addClass("btn pull-right").attr({'id':'img_loading'}));
                        return div;
                }
                
                this.addViewTest =function (data){
                   tbody.append(that.getRow(data))
                }   
                
               
                this.getActionButton =function (title,dis,icon,css)    {
                    return $("<a></a>").addClass("btn btn-primary btn-sm").attr({'data-toggle':'tooltip','title':dis}).css({'margin-left':css,'cursor':'pointer'}).append($("<i></i>").addClass(icon)).append("&nbsp;"+title);
                }
                
                this.getRow = function (personne,value){
                    var row = $("<li></li>").attr({'id':personne})
                        //-------------------------------------------------------
                        //for check visibility mode if true then set bg-blue if not remove............
                        var  class_bg="bg-blue fa fa-newspaper-o";
                        if(value.visible=="false"){
                            class_bg="fa fa-newspaper-o";
                        }
                        row.append($("<i></i>").addClass(class_bg).attr({'data-toggle':'tooltip','title':"Test Visibility"}).css({'cursor':'pointer'}).click(function (){
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                                if($(this).hasClass("bg-blue")){
                                    $(this).removeClass("bg-blue");
                                    that.updVisibility(category_id,subcategory_id,personne,"false");
                                }else{
                                    $(this).addClass("bg-blue");
                                    that.updVisibility(category_id,subcategory_id,personne,"true");
                                }
                            
                        }));
                    var body=$("<div></div>").addClass("timeline-item");
                    var button_grp=$("<span></span>").addClass("time").append($("<div></div>").addClass("pull-right")
                            .append(that.getActionButton("","Add User","fa fa-user","0px").click(function (){
                                that.addUser(personne,$(this));
                            }))
                            .append(that.getActionButton("","View Result","fa fa-pie-chart","4px").click(function (){
                                that.testUserList(personne,$(this))
                            }))
                            .append(that.getActionButton("","View Topic","fa fa-bars","4px").click(function (){
                                that.getQusButton(value.question_bank_id)
                            }))
                            .append(that.getActionButton("","Edit","fa fa-edit","4px").click(function (){
                                that.viewTest(personne,$(this),value)
                            }))
                            .append(that.getActionButton("","Edit","fa fa-copy","4px").click(function (){
                                that.copyTest(personne,$(this),value)
                            }))
                            );
                    body.append(button_grp);  
                    body.append($("<h3></h3>").addClass("timeline-header").append($("<a></a>").append(value.unique_test_key)).append("<br>"+value.test_name))
                    row.append(body);
                    return row;            
                }
                
                this.addUser = function (test_id,obj){
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("UserListVgr")==null){
                        $.getScript("../view/UserListVgr.js").done(function(){
                            config.setView("UserListVgr",new $.UserListVgr(config));
                            var model = config.getView("ModelVgr").getModel("Invite User to Test",config.getView("UserListVgr").getUserList(test_id),"submit");
                            obj.empty();
                            obj.append($('<i class="fa fa-user"></i>'));
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.find(".modal-footer").find("button").first().remove();
                            model.modal('show');
                                $.getScript("../model/UserList.js").done(function() {
                                    config.setModel("UserList",new $.UserList(config));
                                    $.getScript("../controller/UserListMgr.js").done(function() {
                                        config.setController("UserListMgr",new $.UserListMgr(config,config.getModel("UserList"),config.getView("UserListVgr")));
                                        config.getView("UserListVgr").loadUserList();
                                    }).fail(function() {alert("Error :problem in UserListMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in UserListvgr.js file ");}); 
                        }).fail(function() {alert("Error:004 problem in UserListVgr.js file ");}); 
                     }else{
                              var model = config.getView("ModelVgr").getModel("Invite User to Test",config.getView("UserListVgr").getUserList(test_id),"submit");
                              obj.empty();
                              obj.append($('<i class="fa fa-user"></i>'));
                              $("hidden").empty();
                              $("hidden").append(model);
                              model.modal('show');
                              model.find(".modal-footer").find("button").first().remove();
                              config.getView("UserListVgr").loadUserList();
                              
                     }
                }
                
                this.testUserList = function (test_id,obj){
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("TestUserListVgr")==null){
                        $.getScript("../view/TestUserListVgr.js").done(function(){
                            config.setView("TestUserListVgr",new $.TestUserListVgr(config));
                            var model = config.getView("ModelVgr").getModel("Test Result",config.getView("TestUserListVgr").getTestUserList(test_id),"submit");
                            obj.empty();
                            obj.append($('<i class="fa fa-pie-chart"></i>'));
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.find(".modal-footer").find("button").first().remove();
                            model.modal('show');
                                $.getScript("../model/TestUserList.js").done(function() {
                                    config.setModel("TestUserList",new $.TestUserList(config));
                                    $.getScript("../controller/TestUserListMgr.js").done(function() {
                                        config.setController("TestUserListMgr",new $.TestUserListMgr(config,config.getModel("TestUserList"),config.getView("TestUserListVgr")));
                                        config.getView("TestUserListVgr").loadTestUserList();
                                    }).fail(function() {alert("Error :problem in TestUserListMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in TestUserListvgr.js file ");}); 
                        }).fail(function() {alert("Error:004 problem in TestUserListVgr.js file ");}); 
                     }else{
                              var model = config.getView("ModelVgr").getModel("Test Result",config.getView("TestUserListVgr").getTestUserList(test_id),"submit");
                              obj.empty();
                              obj.append($('<i class="fa fa fa-pie-chart"></i>'));
                              $("hidden").empty();
                              $("hidden").append(model);
                              model.find(".modal-footer").find("button").first().remove();
                              model.modal('show');
                              config.getView("TestUserListVgr").loadTestUserList();
                     }
                }
                
                this.viewTest= function (test_id,obj,value){
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("UpdTestVgr")==null){
                        $.getScript("../view/UpdTestVgr.js").done(function(){
                            config.setView("UpdTestVgr",new $.UpdTestVgr(config));
                            var model = config.getView("ModelVgr").getModel("Test Id :"+value.unique_test_key,config.getView("UpdTestVgr").getUpdTest(test_id,value),"submit");
                            obj.empty();
                            obj.append($('<i class="fa fa-edit"></i>'));
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.modal('show');
                            model.find(".modal-footer").find("button").first().remove();   
                        }).fail(function() {alert("Error:004 problem in UpdTestVgr.js file ");}); 
                     }else{
                              var model = config.getView("ModelVgr").getModel("Test Id :"+value.unique_test_key,config.getView("UpdTestVgr").getUpdTest(test_id,value),"submit");
                              obj.empty();
                              obj.append($('<i class="fa fa-edit"></i>'));
                              $("hidden").empty();
                              $("hidden").append(model);
                              model.modal('show');
//                              config.getView("UpdTestVgr").loadUpdTestVgr();
                     }
                }
                this.copyTest= function (test_id,obj,value){
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("UpdTestVgr")==null){
                        $.getScript("../view/UpdTestVgr.js").done(function(){
                            config.setView("UpdTestVgr",new $.UpdTestVgr(config));
                            var model = config.getView("ModelVgr").getModel("Test Id :"+value.unique_test_key,config.getView("UpdTestVgr").getUpdTest(test_id,value),"submit");
                            obj.empty();
                            obj.append($('<i class="fa fa-copy"></i>'));
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.modal('show');
                            model.find(".modal-footer").find("button").first().remove();   
                        }).fail(function() {alert("Error:004 problem in UpdTestVgr.js file ");}); 
                     }else{
                              var model = config.getView("ModelVgr").getModel("Test Id :"+value.unique_test_key,config.getView("UpdTestVgr").getUpdTest(test_id,value),"submit");
                              obj.empty();
                              obj.append($('<i class="fa fa-copy"></i>'));
                              $("hidden").empty();
                              $("hidden").append(model);
                              model.modal('show');
//                              config.getView("UpdTestVgr").loadUpdTestVgr();
                     }
                }
                
                this.updVisibility  =function (category_id,subcategory_id,test_id,visible){
                    $.each(listeners, function(i){
                        try{listeners[i].updVisibility(category_id,subcategory_id,test_id,visible);}catch(e){alert(e)}
                    });
                }
                
                this.addCategory=function (data){
                   cat_dorp_down.find(".select2").append(that.getCatOption(data))
                }          
                
                this.addTest =function (personne,value){
                   tbody.append(that.getRow(personne,value))
                }     
                
                this.addSubCategory=function (data){
                   sub_cat_dorp_down.find(".sub1").append(that.getSubOption(data))
                }          
                
                this.removeFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.setError = function(message) { 
                    config.getView("ModelVgr").setError(message);
                }
                
                this.selLoadBegin =function (){
                    tbody.append(config.getLoadingData());
                }
                
                this.viewTestSubLoadBegin =function (){
                    sub_cat_dorp_down.find("#sub_img_loading").append(config.getLoadingData());
                }
                
                this.selLoadFinish=function (){
                    tbody.empty();
                }
                
                this.viewTestSubLoadFinish=function (){
                    sub_cat_dorp_down.find("#sub_img_loading").empty();
                    setTimeout(function (){
                        sub_cat_dorp_down.find(".sub1").trigger("change");
                    },20);
                }
                
                this.selLoadFail = function() { 
                    tbody.append("<p>")
                }
                
                this.viewTestSubLoadFail = function() { 
                    sub_cat_dorp_down.find("#sub_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.selRemoveFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.loadCategory =function (t){
                    var all = config.getModel("Category").selCategory();
                        cat_dorp_down.find(".cat1").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addCategory(all[item]);
                                });
                         }
                } 
                
                this.loadTest =function (category_id,subcategory_id){
                    tbody.empty();
                    $.each(listeners, function(i){
                        try{listeners[i].selViewTest(category_id,subcategory_id);}catch(e){alert(e)}
                    });
                } 
                
                this.loadSubCategory =function (t,category_id,module){
                        sub_cat_dorp_down.find(".sub1").empty();
                        var all = config.getModel("SubCategory").selSubCategory(category_id,module);
                        if(all){
                                $.each(all, function(item){
                                    that.addSubCategory(all[item]);
                                });
                                sub_cat_dorp_down.find(".sub1").trigger("change");
                         }
                } 
                
                //this is code run when viewTest subcatgory detial does not have in cache object and this funcation call inside the class manager of sub category
                
                this.subAddInViewTest =function (item){
                    that.addSubCategory(item);
                } 
                //---------------------------------------------------------------------------------------------------------------------------------
                //this is code run when Section detial does not have in cache object and this funcation call inside the class manager of Section
                //-------------------------------------------------------------------------------------------------------------------------------------

                this.addListener = function(list){
                            listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	ViewTestVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        selViewTest : function(){},
			updViewTest : function() { },
                        getViewTest : function() { }
		}, list);
	}
});
