jQuery.extend({
	ViewQuestionVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody;
                var viewQuestion;
                var row_id;
                var cat_dorp_down;
                var sub_cat_dorp_down;
                var sec_dorp_down;
                var top_dorp_down;
                var lang_dorp_down;
                var sel_sub_cat;
                var sel_sec;
                var section_ul;
                var divasas;
                var b=true;
                var ul_search_box_body;
                var text_editor;
                test_obj={};
                
                var CKEDITOR_temp=null;
                
                this.getViewQuestionVgr = function(){
                    test_obj["question_details"]={};
                    order_id=0;
                    test_obj["question_details"]={};
                    var div=$("<div></div>");
                    config.getView("ContentWrapperVgr").setBoxHeader("Question Management");
                    
                    div.append(that.getLeftBox())
                    
                    var box=that.getBoxbody();
                    var table= that.getTable();
                    box.append(that.getPanelTitle())
                    box.append(that.getSearchBox());
                    var table= that.getTable();
                    tbody=table;
                    box.append(table);
                    div.append(box);
                    div.append(that.getBoxFooter());
                    that.loadCategory();
                    that.loadLanguage(3);
                    
                    cat_dorp_down.find(".cat1").trigger("change");
                    section_ul.sortable({
                        handle: '.box-header', 
                        update: function() {
                            $('.box', section_ul).each(function(index, elem) {
                                 var $listItem = $(elem),
                                     newIndex = $listItem.index();
                            });
                            that.setOrder();
                        }
                    });
                    return div;
		}

                this.getPanelTitle = function (){
                    var div =$("<div></div>").addClass("box-header ");
                    div.append($("<h3></h3>").addClass("box-title").append($("<b>ViewQuestion</b>")));
                    div.append(that.getLanBox());
                    
                    return div;
                }
                
                this.getCategory_DropDown = function (){
                    cat_dorp_down =$("<div></div>").addClass("form-group col-lg-12");
                    cat_dorp_down.append($("<label>Category :</label>"))
                    cat_dorp_down.append($("<select>Category :</select>").addClass("form-control cat1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var id = cat_dorp_down.find(".cat1 option:first").val();
                            $( ".cat1 option:selected" ).each(function() {
                              id = $( this ).val();
                            });
                        that.loadSubCategory(null,id,7);
                    });
                    return cat_dorp_down;
                }
                
                this.getSubCategory_DropDown = function (){
                    sub_cat_dorp_down =$("<div></div>").addClass("form-group col-lg-12");
                    sub_cat_dorp_down.append($("<label>Sub Category :</label>"))
                    sub_cat_dorp_down.append($("<a></a>").attr({'id':'sub_img_loading'}));
                    sub_cat_dorp_down.append($("<select>Sub Category :</select>").addClass("form-control sub1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:first").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        $( ".sub1 option:selected" ).each(function() {
                          subcategory_id = $(this).val();
                        });
                            that.loadSection(null,category_id,subcategory_id,5);
                    });
                    return sub_cat_dorp_down;
                }
                
                this.getSection_DropDown = function (){
                    sec_dorp_down =$("<div></div>").addClass("form-group col-lg-12");
                    sec_dorp_down.append($("<label>Section :</label>"))
                    sec_dorp_down.append($("<a></a>").attr({'id':'sec_img_loading'}));
                    sec_dorp_down.append($("<select></select>").addClass("form-control sec1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                        $( ".sec1 option:selected" ).each(function() {
                            section_id = $( this ).val();
                        });
                            that.loadTopic(null,category_id,subcategory_id,section_id,3);
                    });
                    return sec_dorp_down;
                }
                
                this.getTopic_DropDown = function (){
                    top_dorp_down =$("<div></div>").addClass("form-group col-lg-12");
                    top_dorp_down.append($("<label>Topic :</label>"))
                    top_dorp_down.append($("<a></a>").attr({'id':'top_img_loading'}));
                    top_dorp_down.append($("<select></select>").addClass("form-control top1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                        var topic_id = top_dorp_down.find(".top1 option:selected").val();
                        var lang_id=lang_dorp_down.find(".lang1 option:selected").val();
                        $( ".top1 option:selected" ).each(function() {
                            topic_id = $( this ).val();
                        });
                        that.loadQuestion(category_id,subcategory_id,section_id,topic_id,lang_id);    
                        test_obj["category_id"]=category_id;
                        test_obj["subcategory_id"]=subcategory_id;
                    });
                    return top_dorp_down;
                }
                
                function first(obj) {
                    for (var a in obj) return a;
                }
                this.getLeftBox = function (){
                    var LeftBox=$("<div></div>").addClass("col-md-3");
                    LeftBox.append(that.getResetButton())
                    LeftBox.append($("<a></a>").addClass("btn btn-primary btn-block margin-bottom").append("Create Test").click(function (){
                        var top,que,sec=test_obj.question_details[first(test_obj.question_details)];
                        try {
                            top=sec[first(sec)];
                            try {
                            que=top[first(top)];
                        }catch(e){}
                        }catch(e){}
                        if(Object.keys(test_obj.question_details).length!=0 && que!=null){
                            that.createTest($(this));    
                        }else{
                            $("body").find("#hidden").find("#error_msg").remove();
                            var msg= "Please add a Question for Test Creation.";
                            var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Error !",msg);
                            error_model.find(".modal-footer").find("button").first().remove();
                            error_model.find(".modal-footer").find("button").empty();
                            error_model.find(".modal-footer").find("button").append("Ok");
                            $("#hidden").append(error_model);
                            error_model.modal('show');   
                        }
                        
                    }));
                    var box_solid1=$("<div></div>").addClass("box box-solid")
                            .append($("<div></div>").addClass("box-body no-padding").append($("<div></div>").addClass("box box-solid")
                                    .append(that.getCategory_DropDown())
                                    .append(that.getSubCategory_DropDown())
                                    .append(that.getSection_DropDown())
                                    .append(that.getTopic_DropDown())
                                    
                            ));
                    var box_solid2=$("<div></div>").addClass("box box-solid ")
                    box_solid2.append($("<div></div>").addClass("box-header with-border").append($("<h3></h3>").addClass("box-title pull-left").append("Set Order by Drag Drop")));
                    section_ul=$("<ul></ul>").addClass("list-unstyled ui-sortable");
                    
                    box_solid2.append(section_ul);
                    LeftBox.append(box_solid1);
                    LeftBox.append(box_solid2);
                    return LeftBox;
            }
                this.getResetButton= function (){
                    return $("<a></a>").addClass("btn btn-default btn-block margin-bottom").append("Restore Default").click(function (){
                        if(config.getView("ViewQuestionVgr")==null){
                            config.getView("MainVgr").getListener().loadViewQuestionVgr();
                        }else{
                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("ViewQuestionVgr").getViewQuestionVgr());
                        }
                    });
                    
            }
            
                this.getSectionBox = function(section_id){
                var sec_box=$("<li></li>").addClass("box box-warning box-solid collapsed-box").attr({'id':section_id});
                sec_box.append($("<div></div>").addClass("box-header with-border").css({'background-color':'#00c0ef','cursor':'move'}).append($("<h3></h3>").addClass("box-title pull-left").append(config.getModel("Section").getSection(section_id,test_obj["category_id"],test_obj["subcategory_id"])))
                    .append($("<div></div>").addClass("box-tools pull-right")
                        .append($("<button></button>").addClass("btn btn-box-tool").append($("<i></i>").addClass("fa fa-plus")).click(function (){
                            that.collapsedBox($(this));
                        }))
//                        .append($("<button></button>").addClass("btn btn-box-tool").append($("<i></i>").addClass("fa fa-remove")).click(function (){
//                            //that.collapsedBox($(this));
//                        }))
                ));
                test_obj["question_details"][section_id]["ul"]=$("<ul></ul>").addClass("list-unstyled ui-sortable").css({'padding':'10px'});
                test_obj["question_details"][section_id]["ul"].sortable({
                        // Only make the .panel-heading child elements support dragging.
                        // Omit this to make then entire <li>...</li> draggable.
                        handle: '.box-header', 
                        update: function() {
                            $('.box', test_obj["question_details"][section_id]["ul"]).each(function(index, elem) {
                                 var $listItem = $(elem),
                                     newIndex = $listItem.index();

                                 // Persist the new indices.
                            });
                            that.setOrder();
                        }
                    });

                sec_box.append($("<div></div>").addClass("box-body no-padding").css({'display':'none'}).append(test_obj["question_details"][section_id]["ul"]));
                section_ul.append(sec_box);
                test_obj["question_details"][section_id]["obj"]=sec_box;
                return sec_box;
            }
            
                this.collapsedBox = function (obj){
                    if(obj.find("i").hasClass("fa-minus")){
                        obj.find("i").removeClass("fa-minus");
                        obj.find("i").addClass("fa-plus");
                        obj.parents().eq(2).addClass("collapsed-box");
                        obj.parents().eq(2).find(".box-body").css({'display':'none'});
                    }else{
                        obj.find("i").removeClass("fa-plus");
                        obj.find("i").addClass("fa-minus");
                        obj.parents().eq(2).removeClass("collapsed-box");
                        obj.parents().eq(2).find(".box-body").css({'display':'block'});
                    }
                }

                this.getTopicBox = function(section_id,topic_id){
                var top_box=$("<li></li>").addClass("box box-warning box-solid collapsed-box").attr({'id':topic_id});
                top_box.append($("<div></div>").addClass("box-header with-border").css({'background-color':' #00c0ef','cursor':'move'}).append($("<h3></h3>").addClass("box-title pull-left").append(config.getModel("Topic").getTopic(test_obj["category_id"],test_obj["subcategory_id"],section_id,topic_id)))
                    .append($("<div></div>").addClass("box-tools pull-right")
                        .append($("<button></button>").addClass("btn btn-box-tool").append($("<i></i>").addClass("fa fa-minus")).click(function (){
                            that.collapsedBox($(this));
                        }))
//                        .append($("<button></button>").addClass("btn btn-box-tool").append($("<i></i>").addClass("fa fa-remove")).click(function (){
//                            //that.collapsedBox($(this));
//                        }))
                ));
                test_obj["question_details"][section_id][topic_id]["ul"]=$("<ul></ul>").addClass("list-unstyled ui-sortable")
                test_obj["question_details"][section_id][topic_id]["ul"].sortable({
                        // Only make the .panel-heading child elements support dragging.
                        // Omit this to make then entire <li>...</li> draggable.
                        handle: '.input-group', 
                        update: function() {
                            $('.box', test_obj["question_details"][section_id][topic_id]["ul"]).each(function(index, elem) {
                                 var $listItem = $(elem),
                                     newIndex = $listItem.index();

                                 // Persist the new indices.
                            });
                            that.setOrder();
                        }
                    });
                top_box.append($("<div></div>").addClass("box-body no-padding").css({'display':'none'}).append(test_obj["question_details"][section_id][topic_id]["ul"]));
                test_obj["question_details"][section_id]["ul"].append(top_box);
                test_obj["question_details"][section_id][topic_id]["obj"]=topic_id;
                return top_box;
            }
            
                this.getQuestionButton=function (order,question_id,value){
                    var dis_icon="fa fa-arrow-circle-right";
                    var dis="";
                    var dis_botton;
                    if(value.discription_bank_id==null){
                        dis_icon="fa fa-exclamation-circle";
                        dis="No Discription"
                        dis_botton=$("<button></button>").addClass("btn btn-default").append($("<span></span>").addClass(dis_icon));
                    }else{
                        dis_botton=$("<button></button>").addClass("btn btn-default").append($("<span></span>").addClass(dis_icon))
                                .popover({
                                    trigger: "hover",
                                    placement: "right",
                                    style: {'max-width':'100%','position':'relative'},
                                    title: "Direction Details",
                                    content: config.getLoadingData(),
                                    html:true
                                })
                                .on('shown.bs.popover', function(){
                                         that.loadDiscriptionDetails($(this),value.discription_id,value.discription_bank_id)
                                })
                    }
                    return  $("<li></li>").attr({'id':question_id})
                            .append($("<div></div>").addClass("input-group margin").css({'cursor':'move'})
                                .append($("<div></div>").addClass("input-group-btn")
                                        .append($("<a></a>").addClass("btn btn-block btn-social").css({'color':'balck','border-color':'rgba(0, 0, 0, 0.2)'})
                                                .append($("<i></i>").append(order))
                                                .append("Qus.ID :"+question_id)
                                                .popover({
                                                    trigger: "hover",
                                                    placement: "right",
                                                    title: "Question Details",
                                                    content: config.getLoadingData(),
                                                    html:true
                                                })
                                                .on('shown.bs.popover', function(){
                                                    that.updQuestion($(this),value);
                                                })
                                            )
                                        )
                                .append($("<div></div>").addClass("input-group-btn")
                                        .append(dis_botton)
                                        .append($("<button>").addClass("btn btn-default").append($("<span></span>").addClass("fa fa-list"))
                                        .popover({
                                            trigger: "hover",
                                            placement: "right",
                                            title: "Option Details",
                                            content: config.getLoadingData(),
                                            html:true
                                        })
                                        .on('shown.bs.popover', function(){
                                            that.loadOptionDetails($(this),value.question_bank_id)
                                        }))
                                        
                                ));
                }
                
                this.setOrder=function (){
            var o=1;
            var section_order=[];
            section_ul.children().each(function () {    
                     var section_id=$(this).attr('id');
                     section_order.push(section_id);
                     var nodeText = $(this).children(':first-child').next().children().children();
                     nodeText.each(function () {    
                         var topic_id=$(this).attr('id');
                            $(this).children(':first-child').next().children().children().each(function () {    
                                var question_id=$(this).attr('id');
                                $(this).find("a").find("i").empty();
                                $(this).find("a").find("i").append(o);
                                test_obj["question_details"][section_id][topic_id][question_id]["order"]=o;
                                
                                o++;
                            });
                    });
            });
            test_obj["section_order"]=section_order;
        }
 
                this.getRightBox = function (){
                    var search_box_body=$("<div></div>").addClass("box-body").append($("<div></div>").addClass("col-md-3").css({'padding':'0px'}).append($("<p>Question No/Question./Direction/Option</p>")));
                    var search_input=$("<div></div>").addClass("input-group col-md-9")
                            .append($("<input/>").addClass("search-query form-control").attr({'id':'new-event','type':'text','placeholder':'Search'}))
                            .append($("<span></span>").addClass("input-group-btn")
                            .append($("<button></button>").addClass("btn btn-primary btn-flat").attr({'id':'add-new-event','type':'button'}).css({'border-color':'#015A9F','background-color':'#015A9F'})
                            .append($("<span></span>").addClass("glyphicon glyphicon-search"))
                             ));
                    search_box_body.append(search_input);
                    search_box_body.append($("<p></p>"));
                    return search_box_body;
                }
                
                this.getSearchBox = function (){
                    var search_box_body=$("<div></div>").addClass("box-body with-border").append($("<div></div>").addClass("col-md-3").css({'padding':'0px'}).append($("<p>Question No/Question./Direction/Option</p>")));
                    var search_input=$("<div></div>").addClass("input-group col-md-9")
                            .append($("<input/>").addClass("search-query form-control").attr({'id':'new-event','type':'text','placeholder':'Coming Soon Pro Features','disabled':'disabled'}))
                            .append($("<span></span>").addClass("input-group-btn")
                            .append($("<button></button>").addClass("btn btn-primary btn-flat").attr({'id':'add-new-event','type':'button'}).css({'border-color':'#015A9F','background-color':'#015A9F'})
                            .append($("<span></span>").addClass("glyphicon glyphicon-search")).click(function (){
                                    top_dorp_down.find(".top1").trigger("change");
                                })
                             ));
                    search_box_body.append(search_input);
                    search_box_body.append($("<p></p>"));
                    return search_box_body;
                }
                
                this.createTest = function (obj){
                    obj.prop('disabled', true);
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("CreateTestVgr")==null){
                        $.getScript("../view/CreateTestVgr.js").done(function(){
                                    try {
                                        config.setView("CreateTestVgr",new $.CreateTestVgr(config));
                                        that.setInsForm($("#hidden"),obj);
                                }catch(e){
                                        alert("Error:004 problem in headervgr.js file "+e);
                                }
                       }).fail(function() {alert("Error:004 problem in headervgr.js file ");}); 
                    }else{
                        that.setInsForm($("#hidden"),obj);
                    }
                }
                
                this.getLanBox = function (){
                    lang_dorp_down =$("<div></div>").addClass("form-group pull-right");
                    lang_dorp_down.append($("<select>:</select>").addClass("form-control lang1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var lang_id=lang_dorp_down.find(".lang1 option:first").val();
                        $( ".lang1 option:selected" ).each(function() {
                            lang_id = $( this ).val();
                        });
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                        var topic_id = top_dorp_down.find(".top1 option:selected").val();
                        that.loadQuestion(category_id,subcategory_id,section_id,topic_id,lang_id);    
                    });
                    return lang_dorp_down;
                }
                
                this.getDisButton=function (kk){
                    var icon="fa fa-close";
                    if(kk!=""){
                        icon="fa fa-check";
                    }
                    var a=$("<button></button>").addClass("btn btn-primary")
                                        .attr({'type':'button'}) 
                                        .append($("<span></span>").addClass(icon))
                                        .click(function(){
                                                
                                        }).hover(function (){
                                            
                                        });
                                    return a;
                }
                
                this.getQusButton=function (kk){
                    var a=$("<button></button>").addClass("btn btn-primary")
                                        .attr({'type':'button'}) 
                                        .append($("<span></span>").addClass("fa fa-edit"))
                                        .click(function(){
                                            that.setQuestionForm($("#hidden"),kk);
                    });
                    return a;
                }
                
                this.getOptionsButton=function (kk){
                    var a=$("<button></button>").addClass("btn btn-primary")
                                        .attr({'type':'button'}) 
                                        .append($("<span></span>").addClass("fa fa-list-alt"))
                                        .click(function(){
                                            that.setQuestionForm($("#hidden"),kk);
                    });
                    return a;
                }
                
                var order_id=0;
                
                this.setTestQuestion =function (o,kk,value){
                        var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                        var topic_id = top_dorp_down.find(".top1 option:selected").val();
                        
                    if(test_obj["question_details"][section_id]==null){
                             test_obj["question_details"][section_id]={}
                             test_obj["question_details"][section_id][topic_id]={}
                             test_obj["question_details"][section_id][topic_id][kk]={}
                             that.question_Add(o,"fa-minus-circle","Undo");
                             that.getSectionBox(section_id);
                             that.getTopicBox(section_id,topic_id);
                             order_id++;
                             var a =that.getQuestionButton(order_id,kk,value);
                             test_obj["question_details"][section_id][topic_id][kk]["obj"]=a;
                             test_obj["question_details"][section_id][topic_id][kk]["order"]=order_id;
                             test_obj["question_details"][section_id][topic_id][kk]["value"]=value;
                             test_obj["question_details"][section_id][topic_id]["ul"].append(a);
                    }else{
                             if(test_obj["question_details"][section_id][topic_id]==null){
                                 test_obj["question_details"][section_id][topic_id]={}
                                 test_obj["question_details"][section_id][topic_id][kk]={}
                                 that.question_Add(o,"fa-minus-circle","Undo");
                                 that.getTopicBox(section_id,topic_id);
                                 order_id++;
                                 var a =that.getQuestionButton(order_id,kk,value);
                                 test_obj["question_details"][section_id][topic_id][kk]["order"]=order_id;
                                 test_obj["question_details"][section_id][topic_id][kk]["value"]=value;
                                 test_obj["question_details"][section_id][topic_id][kk]["obj"]=a;
                                 test_obj["question_details"][section_id][topic_id]["ul"].append(a);
                             }else{
                                 if(test_obj["question_details"][section_id][topic_id][kk]==null){
                                     test_obj["question_details"][section_id][topic_id][kk]={}
                                     that.question_Add(o,"fa-minus-circle","Undo");
                                     order_id++;
                                     var a =that.getQuestionButton(order_id,kk,value);
                                     test_obj["question_details"][section_id][topic_id][kk]["order"]=order_id;
                                     test_obj["question_details"][section_id][topic_id][kk]["value"]=value;
                                     test_obj["question_details"][section_id][topic_id][kk]["obj"]=a;
                                     test_obj["question_details"][section_id][topic_id]["ul"].append(a);
                                 }else{
                                     that.question_Add(o,"fa-plus-circle","Add");
                                     order_id--;
                                     test_obj["question_details"][section_id][topic_id][kk]["obj"].remove();
                                     that.setOrder();
                                     test_obj["question_details"][section_id][topic_id][kk]=null;
                                 }
                             }
                    }
                    that.setOrder();
                }
                
                this.question_Add = function (o,icon,title){
                    o.empty();
                    o.append($("<i></i>").addClass("fa "+icon))
                    o.append("&nbsp;"+title);
                }
                    
                this.getBoxbody = function (){
                    var div =$("<div></div>").addClass("col-md-9");
                    return div;
                }
                
                this.getTable = function (){
                    
                    return $("<div></div>").addClass("");
                }
                
                this.getCatOption = function (data){
                    var row = $("<option></option>").attr({'value':data.category_id})
                              .append($("<td>"+data.category_name+"</td>"));
                    return row;            
                }
                
                this.getLangOption = function (data){
                    var row = $("<option></option>").attr({'value':data.language_id})
                              .append($("<td>"+data.language_name+"</td>"));
                    return row;            
                }
                
                this.getSubOption = function (data){
                    var row = $("<option></option>").attr({'value':data.subcategory_id})
                              .append($("<td>"+data.subcategory_name+"</td>"));
                    return row;            
                }
                
                this.getSecOption = function (data){
                    var row = $("<option></option>").attr({'value':data.section_id})
                              .append($("<td>"+data.section_name+"</td>"));
                    return row;            
                }
                
                this.getTopOption = function (data){
                    var row = $("<option></option>").attr({'value':data.topic_id})
                              .append($("<td>"+data.topic_name+"</td>"));
                    return row;            
                }
                
                this.getBoxFooter =function (){
                    var div = $("<div></div>").addClass("box-footer clearfix");
//                        div.append($("<a></a>").addClass("btn pull-right").attr({'id':'img_loading'}));
//                        div.append($("<ul></ul>").addClass("pagination pagination-sm no-margin pull-right")
//                                .append($("<li></li>").append($("<a></a>").attr({'href':'#'}).append($("<i></i>").addClass("fa fa-arrow-left")))
//                                    .click(function ()
//                                        {
//                                            
//                                            
//                                        }
//                                     )
//                                    )
//                                .append($("<li></li>").append($("<a></a>").attr({'href':'#'}).append($("<i></i>").addClass("fa fa-arrow-right")))
//                                    .click(function ()
//                                        {
//                                            
//                                            
//                                        }
//                                     )
//                                    )
//                        );
//                        div.append($("<a></a>").addClass("btn pull-right").attr({'id':'img_loading'}));
                        return div;
                }
                
                this.setQuestionForm =function (a,kk){
                    if(config.getView("QuestionVgr")==null){
                        config.getView("MainVgr").getListener().loadQuestionVgr(kk);
                    }else{
                        that.setInsForm(a,kk);
                    }
                }
                
                
                this.setInsForm=function (a,obj){
                    obj.prop('disabled', false);
                    obj.empty();
                    obj.append("Create Test");
//                    $('#myModal').modal('toggle');
                    var model = config.getView("ModelVgr").getModel("New Test",that.getInsForm(),"submit");
                    a.empty();
                    a.append(model);
                    var form=config.getView("CreateTestVgr").getForm();
                    var viewQuestion_form =$('#viewQuestion_form');
                    viewQuestion_form.find("#submit").click(function(){
                    that.removeFail();
                     var create_test_obj=config.getView("CreateTestVgr").getObj();
                     if(config.getModel("CreateTest")==null){
                         $.getScript("../model/CreateTest.js").done(function(){
                                try {
                                        config.setModel("CreateTest",new $.CreateTest(config));
                                        $.getScript("../controller/CreateTestMgr.js").done(function(){
                                        try {
                                                config.setController("CreateTestMgr",new $.CreateTestMgr(config,config.getModel("CreateTest"),that));
                                                $.each(listeners, function(i){try {listeners[i].insCreateTest(create_test_obj);}catch(e){}});
                                            }catch(e){
                                                alert(e+" problem in create test mgr ");
                                            }
                                        }).fail(function() {alert("Error: Problem in CreateTestMgr.js file ");}); 
                                }catch(e){
                                        alert(e+" problem in create test.js file ");
                                }
                       }).fail(function() {alert("Error: Problem in headervgr.js file ");}); 
                     }else{
                              $.each(listeners, function(i){try {listeners[i].insCreateTest(create_test_obj);}catch(e){}});
                     }
                     });
                     config.getView("CreateTestVgr").setBackDisable();
                     config.getView("ModelVgr").setDisableButton();
                    $('#myModal').modal('show');
                }
                
                this.setUpdDirection=function (obj,discription_id,discription_bank_id){
                    var a=config.getLoadingData();
                    obj.append(a);
                    if(CKEDITOR_temp==null){
                        $.getScript("https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js").done(function(){        
                            a.remove();
                            that.setUpdDirectionForm($("#hidden"),discription_id,discription_bank_id);
                        }).fail(function() {alert("Error:004 problem in UserListVgr.js file ");}); 
                    }else{
                        a.remove();
                        that.setUpdDirectionForm($("#hidden"),discription_id,discription_bank_id);
                    }
                        
                     
                }
                
                this.updQuestion=function (obj,question_bank_id,question,body){
                    if(obj.hasClass("btn")){
                        obj.attr('data-content', decodeURIComponent(question_bank_id.question));
                        obj.data('bs.popover').setContent();
                        $("#"+obj.attr("aria-describedby")).css({'min-width':'400%'});
                    }else{
                        var a=config.getLoadingData();
                        obj.append(a);
                        if(CKEDITOR_temp==null){
                            $.getScript("https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js").done(function(){        
                                a.remove();
                                that.setUpdQuestionForm($("#hidden"),question_bank_id,question,body);
                            }).fail(function() {alert("Error:004 problem in UserListVgr.js file ");}); 
                        }else{
                            a.remove();
                            that.setUpdQuestionForm($("#hidden"),question_bank_id,question,body);
                        }
                    }
                    
                }
                
                this.setUpdOption=function (obj,question_bank_id){
                    var a=config.getLoadingData();
                    obj.append(a);
                    a.remove();
                    that.setUpdOptionForm($("#hidden"),question_bank_id);
                }
                
                this.setUpdDirectionForm=function (a,discription_id,discription_bank_id){
                    var form=that.getUpdForm(discription_bank_id);
                    var model = config.getView("ModelVgr").getModel("View Driection",form,"submit_direction");
                    a.empty();
                    a.append(model);
                    var sub=false;
                    var submit_direction =$('#submit_direction');
                    submit_direction.click(function(){
                        if(text_editor.getData()!=""){
                                var discription_bank={};
                                discription_bank["discription"]=encodeURIComponent(text_editor.getData());
                                form.find(":input").each(function() {
                                    if($(this).attr("name")=="discription_bank_id"){
                                        discription_bank["discription_bank_id"]=$(this).val();
                                    }
                                });
                                $.each(listeners, function(i){
                                    listeners[i].updDirectionDetails(discription_bank);
                                });
                                sub=false;
                                $('#myModal').modal('toggle');
                        }else{
                            var model = config.getView("ErrorMsgVgr").getErrorMsg("Error !","Direction Details should not blank?","vsisi",null);
                            $("#hidden").append(model);
                            model.find(".modal-footer").find("button").first().remove();
                            model.modal('show');
                        }
                     });
                    $('#myModal').modal('show');
                    text_editor=CKEDITOR.replace('editor1');
                    CKEDITOR_temp=text_editor;
                    that.loadDiscriptionDetails(null,discription_id,discription_bank_id);
                    $.fn.modal.Constructor.prototype.enforceFocus = function () {
                        modal_this = this
                        $(document).on('focusin.modal', function (e) {
                        if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                        // add whatever conditions you need here:
                        &&
                        !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
                        modal_this.$element.focus()
                        }
                        })
                    };
                    
                }
                
                this.getUpdForm =function (id){
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'cat_form_upd'})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls")
                                        .append(that.getTextEditor("editor1"))
                                        .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'discription_bank_id','name':'discription_bank_id','value':id})))
                                         )
                                 );
                    return form;        
                }
                
                this.setUpdQuestionForm=function (a,question_bank_id,question,body){
                    var form=that.getQustionForm(question_bank_id,question);
                    var model = config.getView("ModelVgr").getModel("View Question",form,"submit_direction");
                    a.empty();
                    a.append(model);
                    var sub=false;
                    var submit_direction =$('#submit_direction');
                    submit_direction.click(function(){
                        if(text_editor.getData()!=""){
                                var question_bank={};
                                question_bank["question"]=encodeURIComponent(text_editor.getData());
                                form.find(":input").each(function() {
                                    if($(this).attr("name")=="question_bank_id"){
                                        question_bank["question_bank_id"]=$(this).val();
                                    }
                                });
                                config.getModel("Question").updQuestionDetails(question_bank);
                                body.empty();
                                body.append(text_editor.getData());
                        }else{
                            var model = config.getView("ErrorMsgVgr").getErrorMsg("Error !","Question Details should not blank?","vsisi",null);
                            $("#hidden").append(model);
                            model.find(".modal-footer").find("button").first().remove();
                            model.modal('show');
                        }
                     });
                    $('#myModal').modal('show');
                    text_editor=CKEDITOR.replace('editor1');
                    CKEDITOR_temp=text_editor;
                    text_editor.setData(question);
                    $.fn.modal.Constructor.prototype.enforceFocus = function () {
                        modal_this = this
                        $(document).on('focusin.modal', function (e) {
                        if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                        // add whatever conditions you need here:
                        &&
                        !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
                        modal_this.$element.focus()
                        }
                        })
                    };
                    
                }
                
                this.setUpdOptionForm=function (a,question_bank_id){
                    var form=that.getUpdOptionForm(question_bank_id,config.getModel("OptionDetails").getObj(question_bank_id));
                    var model = config.getView("ModelVgr").getModel("View Option",form,"submit_direction");
                    a.empty();
                    a.append(model);
                    var submit_direction =$('#submit_direction');
                    submit_direction.click(function(){
                        var check=true;
                        var msg="";
                        var option_bank={};
                        form.find(":input").each(function(){
                                if($(this).attr("name")!=null){
                                    if($(this).val()!=""){
                                        option_bank[$(this).attr("name")]=encodeURIComponent($(this).val());
                                    }else{
                                        msg= "Please Option Field "+$(this).attr("name")+" should not blank.";
                                        check=false;
                                        return false;
                                    }
                                }
                                
                        }); 
                        if(check){
                                config.getModel("OptionDetails").updOptionDetails(option_bank);
                        }else{
                            var model = config.getView("ErrorMsgVgr").getErrorMsg("Error !",msg,"vsisi",null);
                            $("#hidden").append(model);
                            model.find(".modal-footer").find("button").first().remove();
                            model.modal('show');
                        }
                     });
                    $('#myModal').modal('show');
                }
                
                this.getQustionForm =function (id){
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'cat_form_upd'})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls")
                                        .append(that.getTextEditor("editor1"))
                                        .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'question_bank_id','name':'question_bank_id','value':id})))
                                         )
                                 );
                    return form;        
                }
                
                this.getUpdOptionForm =function (id,obj){
                    var keys = [];
                    for(var k in obj) keys.push(k);
                    var div=$("<div></div>").addClass("form-group")
                    div.append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'question_bank_id','name':'question_bank_id','value':id}));
                    var answer = $("<select></select>").addClass("answer input-xlarge form-control").attr({'id':'answer','placeholder':'Select','name':'answer'});
                    for (var i=0;i<keys.length;i++){
                        if(keys[i]=="answer"){
                            answer.val(obj[keys[i]]);
                        }else{
                            answer.append($("<option></option>").attr({'value':keys[i]}).append($("<td>"+keys[i]+"</td>")));
                            div.append($("<div></div>").addClass("form-control-group col-lg-12")
                                .append($("<label></label>").addClass("control-label").append("Option "+keys[i]))
                                .append($("<div></div>").addClass("controls")
                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':keys[i],'name':keys[i],'value':decodeURIComponent(obj[keys[i]])}))
                                .append($("<p></p>"))
                                ));
                        }
                    }
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'cat_form_upd'})
                            .append(div)
                            .append($("<div></div>").addClass("form-control-group col-lg-12  no-padding")
                                .append($("<label></label>").addClass("control-label").append("Answer"))
                                .append($("<div></div>").addClass("controls")
                                .append(answer)
                                .append($("<p></p>"))
                                ));
                    return form;        
                }
                
                this.getTextEditor = function (editor){
                    var text_editor =$("<div></div>").addClass("pad col-lg-12");
                    text_editor.append($("<form></form>").append($("<textarea></textarea>").attr({'id':editor,'name':editor,'rows':'8','cols':'40'})))
                    return text_editor;
                }
                
                this.getInsForm =function (){
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'viewQuestion_form'}).css({'margin':'-5px'});
                    form.append(config.getView("CreateTestVgr").getCreateTest(test_obj));
                    return form;        
                }
                
                this.addViewQuestion =function (data){
                   tbody.append(that.getRow(data))
                }   
                
                this.getActionButton =function (title,dis,icon,css)    {
                    return $("<a></a>").addClass("small-box-footer").attr({'data-toggle':'tooltip','title':dis}).css({'margin-left':css,'cursor':'pointer'}).append($("<i></i>").addClass(icon)).append("&nbsp;"+title);
                }
                
                this.getRow = function (personne,value){
                    var row = $("<div></div>").addClass("box").attr({'id':personne})
                    var heder=$("<div></div>").addClass("box-header with-border")
                            .append($("<h3></h3>").addClass("box-title").append(personne))
//                            .append($("<div></div>").addClass("box-tools pull-right").append($("<button></button>").addClass("btn btn-box-tool").attr({'data-widget':'remove','data-toggle':'tooltip','title':'Remove'}).append($("<i></i>").addClass("fa fa-times")).click(function (){
//                                alert(personne);
//                            })));
                    var body=$("<div></div>").addClass("box-body").append(decodeURIComponent(value.question));
                    var dis_icon="fa fa-arrow-circle-right";
                    var dis="";
                    var dis_botton;
                    if(value.discription_bank_id==null){
                        dis_icon="fa fa-exclamation-circle";
                        dis="No Discription"
                        dis_botton=that.getActionButton("Direction",dis,dis_icon,"");
                    }else{
                        dis_botton=that.getActionButton("Direction",dis,dis_icon,"")
                                .popover({
                                    trigger: "hover",
                                    placement: "right",
                                    title: "Direction Details",
                                    content: config.getLoadingData(),
                                    html:true
                                })
                                .on('shown.bs.popover', function(){
                                         that.loadDiscriptionDetails($(this),value.discription_id,value.discription_bank_id)
                                })
                                .click(function (){
                                    that.setUpdDirection($(this),value.discription_id,value.discription_bank_id);
                                })
                    }
                    var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                    var topic_id = top_dorp_down.find(".top1 option:selected").val();
                    var temp_obj={};
                    if(test_obj["question_details"][section_id]==null ||test_obj["question_details"][section_id][topic_id]==null || test_obj["question_details"][section_id][topic_id][personne]==null){
                             temp_obj["title"]="Add";
                             temp_obj["dis"]="Add Question";
                             temp_obj["icon"]="fa fa-plus-circle";
                    }else{
                             temp_obj["title"]="Undo";
                             temp_obj["dis"]="Remove Question";
                             temp_obj["icon"]="fa fa-minus-circle";
                    }
                    var footer=$("<div></div>").addClass("box-footer")
                            .append(dis_botton)
                            .append(that.getActionButton("Option","","fa fa-list-alt","5px")
                                        .popover({
                                            trigger: "hover",
                                            placement: "right",
                                            title: "Option Details",
                                            content: config.getLoadingData(),
                                            html:true
                                        })
                                        .on('shown.bs.popover', function(){
                                            that.loadOptionDetails($(this),value.question_bank_id)
                                        })
                                        .click(function (){
                                            that.setUpdOption($(this),value.question_bank_id);
                                        }))
                            .append(that.getActionButton("Edit","Edit Question","fa fa-edit","6px").click(function (){
                                that.updQuestion($(this),value.question_bank_id,body.html(),body);
                            }))
                            .append(that.getActionButton(temp_obj["title"],temp_obj["dis"],temp_obj["icon"],"").addClass("pull-right").click(function (){
                                that.setTestQuestion($(this),personne,value);
                            }))
                    row.append(heder);
                    row.append(body);
                    row.append(footer);
                    return row;            
                }
                
                this.loadDiscriptionDetails = function (a,discription_id,discription_bank_id){
                    if(config.getModel("DirectionDetails")==null){
                        $.getScript("../model/DirectionDetails.js").done(function() {
                                try{
                                    config.setModel("DirectionDetails",new $.DirectionDetails(config));
                                    $.getScript("../controller/DirectionDetailsMgr.js").done(function() {
                                    try{
                                        config.setController("DirectionDetailsMgr",new $.DirectionDetailsMgr(config,config.getModel("DirectionDetails"),that));
                                        $.each(listeners, function(i){
                                            try {
                                                listeners[i].selDirectionDetails(discription_id,discription_bank_id,a);
                                            }catch(e){}
                                        });
                                        
                                    }catch(e){
                                        alert(e+" in load Direction Details Mgr");
                                    }
                                    }).fail(function() {alert("Error:Direction DetailsMgr Js Loading problem");}); 
                                }catch(e){
                                    alert(e+" in load Direction Details");
                                }
                            }).fail(function() {alert("Error:Direction Details Js Loading problem");}); 
                    }else{
                        $.each(listeners, function(i){
                                            try {
                                                listeners[i].selDirectionDetails(discription_id,discription_bank_id,a);
                                            }catch(e){}
                                        });
                    }
                }
                
                this.loadOptionDetails = function (a,question_bank_id){
                    if(config.getModel("OptionDetails")==null){
                        $.getScript("../model/OptionDetails.js").done(function() {
                                try{
                                    config.setModel("OptionDetails",new $.OptionDetails(config));
                                    $.getScript("../controller/OptionDetailsMgr.js").done(function() {
                                    try{
                                        config.setController("OptionDetailsMgr",new $.OptionDetailsMgr(config,config.getModel("OptionDetails"),that));
                                        $.each(listeners, function(i){
                                            try {
                                                listeners[i].selOptionDetails(question_bank_id,a);
                                            }catch(e){}
                                        });
                                        
                                    }catch(e){
                                        alert(e+" in load Direction Details Mgr");
                                    }
                                    }).fail(function() {alert("Error:Direction DetailsMgr Js Loading problem");}); 
                                }catch(e){
                                    alert(e+" in load Direction Details");
                                }
                            }).fail(function() {alert("Error:Direction Details Js Loading problem");}); 
                    }else{
                        $.each(listeners, function(i){
                                            try {
                                                listeners[i].selOptionDetails(question_bank_id,a);
                                            }catch(e){}
                                        });
                    }
                }
                
                this.addDirection_PopOver = function (dis,obj){
                    obj.attr('data-content', decodeURIComponent(dis));
                    obj.data('bs.popover').setContent();
                    if(obj.hasClass("btn")){
                        $("#"+obj.attr("aria-describedby")).css({'min-width':'400%'});
                    }else{
                        $("#"+obj.attr("aria-describedby")).css({'max-width':'100%'});
                    }
                    
                }
                
                this.addOption_PopOver = function (dis,obj){
                    var keys = [];
                    for(var k in dis) keys.push(k);
                    var div="";
                    var ans;
                    for (var i=0;i<keys.length;i++){
                        if(keys[i]=="answer"){
                           ans=dis[keys[i]];
                        }else{
                           div+="<div><a class='text-black'><label>"+keys[i]+"&nbsp;&nbsp;"+decodeURIComponent(dis[keys[i]])+"</label></a></div>" 
                        }
                    }
                    div+="<div class='box-footer' style='padding-left:0px'><a><label>Correct Answered</label><span class='pull-right'><b>"+"&nbsp;&nbsp;"+ans+"</b></span><a></div>";
                    obj.attr('data-content', div);
                    obj.data('bs.popover').setContent();
                    if(obj.hasClass("btn")){
                        $("#"+obj.attr("aria-describedby")).css({'min-width':'400%'});
                    }else{
                        $("#"+obj.attr("aria-describedby")).css({'max-width':'100%'});
                    }
                }
                
                this.addDirection_Editor = function (dis,obj){
                    setTimeout(function (){
                        text_editor.setData(decodeURIComponent(dis));
                    },1000)
                }
                
                this.addCategory=function (data){
                   cat_dorp_down.find(".select2").append(that.getCatOption(data))
                }          
                
                this.addQuestion =function (personne,value){
                   tbody.append(that.getRow(personne,value))
                }     
                
                this.addLanguage=function (data){
                   lang_dorp_down.find(".select2").append(that.getLangOption(data))
                }          
                
                this.addSubCategory=function (data){
                   sub_cat_dorp_down.find(".sub1").append(that.getSubOption(data))
                }          
                
                this.addSection=function (data){
                   sec_dorp_down.find(".sec1").append(that.getSecOption(data))
                }          
                
                this.addTopic=function (data){
                   top_dorp_down.find(".top1").append(that.getTopOption(data))
                }          
                
                
                this.insLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.insLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                    msg= "Test successfully created.";
                    var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Success.!",msg);
                    error_model.find(".modal-footer").find("button").first().remove();
                    error_model.find(".modal-footer").find("button").empty();
                    error_model.find(".modal-footer").find("button").append("Ok");
                    $("#hidden").append(error_model);
                    error_model.modal('show');
                    return false;
                }
                
                this.insLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                
                this.removeFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.setError = function(message) { 
                    config.getView("ModelVgr").setError(message);
                }
                
                this.selLoadBegin =function (){
                    tbody.append(config.getLoadingData());
                }
                this.selLoadFail =function (){
                    tbody.append("Error: Server doesn't Respond.");
                }
                this.selLoadFinish =function (){
                    tbody.empty();
                }
                
                this.viewQuestionSubLoadBegin =function (){
                    sub_cat_dorp_down.find("#sub_img_loading").append(config.getLoadingData());
                }
                
                this.viewQuestionSecLoadBegin =function (){
                    sec_dorp_down.find("#sec_img_loading").append(config.getLoadingData());
                }
                
                this.viewQuestionTopLoadBegin =function (){
                    top_dorp_down.find("#top_img_loading").append(config.getLoadingData());
                }
                
                this.viewQuestionLangLoadBegin =function (){
                    lang_drop_down.find("#lang_img_loading").append(config.getLoadingData());
                }
                
                this.viewQuestionModelSecLoadBegin =function (){
                    $("#model_sec_img_loading").append(config.getLoadingData());
                }
                
                this.viewQuestionSubLoadFinish=function (){
                    sub_cat_dorp_down.find("#sub_img_loading").empty();
                    setTimeout(function (){
                        sub_cat_dorp_down.find(".sub1").trigger("change");
                    },20);
                }
                
                this.viewQuestionSecLoadFinish=function (){
                    sec_dorp_down.find("#sec_img_loading").empty();
                    setTimeout(function (){
                        sec_dorp_down.find(".sec1").trigger("change");
                    },20);
                }
                
                this.viewQuestionTopLoadFinish=function (){
                    top_dorp_down.find("#top_img_loading").empty();
                    setTimeout(function (){
                        top_dorp_down.find(".top1").trigger("change");
                    },20);
                }
                
                this.viewQuestionLangLoadFinish=function (){
                    lang_drop_down.find("#lang_img_loading").empty();
                    b=false;
                }
                
                this.viewQuestionSubLoadFail = function() { 
                    sub_cat_dorp_down.find("#sub_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.viewQuestionSecLoadFail = function() { 
                    sec_dorp_down.find("#sec_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.viewQuestionTopLoadFail = function() { 
                    top_dorp_down.find("#top_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.viewQuestionLangLoadFail = function() { 
                    lang_drop_down.find("#lang_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.selRemoveFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.updLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.updLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                    
                }
                
                this.updLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                this.loadCategory =function (t){
                    var all = config.getModel("Category").selCategory();
                        cat_dorp_down.find(".cat1").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addCategory(all[item]);
                                });
                         }
                } 
                
                this.loadQuestion =function (category_id,subcategory_id,section_id,topic_id,language_id){
                    tbody.empty();
                    $.each(listeners, function(i){
                        try {
                            listeners[i].selQuestion(category_id,subcategory_id,section_id,topic_id,language_id);
                        }catch(e){}
                    });
                } 
                
                this.loadLanguage =function (module){
                    var all = config.getModel("Language").selLanguage(module);
                    lang_dorp_down.find(".lang1").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addLanguage(all[item]);
                                });
                         }
                } 
                
                this.loadSubCategory =function (t,category_id,module){
                        sub_cat_dorp_down.find(".sub1").empty();
                        var all = config.getModel("SubCategory").selSubCategory(category_id,module);
                        if(all){
                                $.each(all, function(item){
                                    that.addSubCategory(all[item]);
                                });
                                sub_cat_dorp_down.find(".sub1").trigger("change");
                         }
                } 
                
                this.loadSection =function (t,category_id,subcategory_id,module){
                        sec_dorp_down.find(".sec1").empty();
                        var all = config.getModel("Section").selSection(category_id,subcategory_id,module);
                        if(all){
                                $.each(all, function(item){
                                    that.addSection(all[item]);
                                });
                                sec_dorp_down.find(".sec1").trigger("change");
                         }
                } 
                
                this.loadTopic =function (t,category_id,subcategory_id,section_id,module){
                        top_dorp_down.find(".top1").empty();
                        var all = config.getModel("Topic").selTopic(category_id,subcategory_id,section_id,module);
                        if(all){
                                $.each(all, function(item){
                                    that.addTopic(all[item]);
                                });
                                top_dorp_down.find(".top1").trigger("change");
                         }
                } 
                
                this.subAddInViewQuestion =function (item){
                    that.addSubCategory(item);
                } 
                
                this.secAddInViewQuestion =function (item){
                    that.addSection(item);
                } 
                
                this.topAddInViewQuestion =function (item){
                    that.addTopic(item);
                } 
                
                //-------------------------------------------------------------------------------------------------------------------------------------
                

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                this.setloading =function (){
                    viewQuestion.find("#img_loading").append(config.getLoadingData());
                }
                
                this.removeLoading =function (){
                    viewQuestion.find("#img_loading").empty();
                }
                
                this.setDisableButton =function (){
                    viewQuestion.find("#cancel").prop('disabled', true);
                }
                
                this.setEnableButton =function (){
                    viewQuestion.find("#cancel").prop('disabled', false);
                }
                
                this.setServerError =function (){
                    viewQuestion.find("#error_msg").append("Error: Server doesn't Respond.");
                }
                this.removeServerError =function (){
                    viewQuestion.find("#error_msg").empty();
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	ViewQuestionVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        selDirectionDetails : function(){},
			updDirectionDetails : function() { },
                        getDirectionDetails : function() { }
		}, list);
	},
	CreateTestVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        intCreateTest : function(){}
		}, list);
	}
});
