jQuery.extend({
	UserListVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
                var modelform;
                var model_footer;
                var ul;
                var model_body;
                var testid;
                var total;
                var outoff;
                var save;
                var error;
                var total_std=0;
                var outoff_std=0;
                
                                   
                this.getUserList = function(test_id){
                        testid=test_id;
			var model =$("<div>").addClass("box").css({'margin-top':'-16px'});
                        model.append(that.getUserListHeader());
                        model.append(that.getUserListBody());
                        return model;
		}
                

                this.getUserListHeader= function (){
                    total=$("<b>");
                    outoff=$("<b>");
                    save=$("<button>").addClass("btn btn-primary btn-sm").attr({'type':'button','data-toggle':'tooltip','title':'Save User List'}).css({'margin-left':'4px'}).append("Save")
                                            .click(function (){
                                                that.insUserInTest($(this));
                                            });
                    error=$("<a>").addClass("btn red").css({'color':'red'});
                    var model_header =$("<div>").addClass("box-header with-border")
                                 .append($("<div>").addClass("input-group")
                                   .append($("<div>").addClass("input-group-btn")
                                        .append($("<button>").addClass("btn btn-primary dropdown-toggle").attr({'type':'button','data-toggle':'dropdown','aria-expanded':'false'}).append("Search By&nbsp;")
                                                .append($("<span>").addClass("fa fa-caret-down")))
                                        .append($("<ul>").addClass("dropdown-menu")
                                                .append($("<li>").append($("<a>").append("User Name")))
                                                .append($("<li>").append($("<a>").append("User Id")))
                                                .append($("<li>").append($("<a>").append("Email")))
                                                .append($("<li>").append($("<a>").append("Contact")))
                                        )
                                    )
                                    .append($("<input>").addClass("form-control").attr({'type':'text','disabled':'disabled'})))
                                    .append($("<div>").addClass("mailbox-controls")
                                        .append($("<button>").addClass("btn btn-default btn-sm").attr({'type':'button','data-toggle':'tooltip','title':'Select All'}).append($("<i>").addClass("fa fa fa-refresh")))
                                        .append(save)
                                        .append(error)
                                        .append($("<div>").addClass("pull-right")
                                            .append(outoff)
                                            .append("/")
                                            .append(total)
                                            .append($("<button>").addClass("btn btn-default btn-sm checkbox-toggle").attr({'type':'button','data-toggle':'tooltip','title':'Select All'}).css({'margin-left':'4px'})
                                                .click(function (){
                                                        
                                                        if($(this).hasClass("btn-primary")){
                                                           $(this).removeClass("btn-primary"); 
                                                           $(this).addClass("btn-default"); 
                                                           outoff_std=0;
                                                           that.removeAllUser();
                                                        }else{
                                                           outoff_std=0;
                                                           $(this).addClass("btn-primary"); 
                                                           $(this).removeClass("btn-default"); 
                                                           that.addAllUser();
                                                        }
                                                    })
                                            .append($("<i>").addClass("fa fa-user"))
                                            )
                                         )
                                   );
                    return model_header;
                }
                
                this.getUserListBody = function (){
                    model_body =$("<div>").addClass("box-body").css({'display':'block'});
                    ul=$("<ul>").addClass("products-list product-list-in-box");
                    model_body.append(ul);
                    return model_body ;
                }
                
                this.getUser = function (user){
                    var icon="fa-check";
                    if(user.test_status=="true"){
                        icon="fa-trash";
                        outoff_std++;
                    }
                    return $("<li>").addClass("item").attr({'id':user.user_id})
                            .append($("<div>").addClass("product-img").append($('<img>').attr({'src':user.pic})))
                            .append($("<div>").addClass("product-info")
                                .append($('<a>').addClass("product-title").append(user.user_name))
                                .append($('<a>').addClass("btn btn-primary btn-sm pull-right").css({'cursor': 'pointer'}).append($("<i>").addClass("fa "+icon))
                                    .click(function (){
                                        that.addUser(user.user_id,$(this));
                                    })
                                )
                                .append($('<span>').addClass("product-description")
                                        .append($("<i>").addClass("fa fa-user")).append("&nbsp;"+user.user_id+"&nbsp;&nbsp;")
                                        .append($("<i>").addClass("fa fa-fw fa-mobile")).append("&nbsp;"+user.contact_no+"&nbsp;&nbsp;")
                                        .append($("<i>").addClass("fa fa-envelope")).append("&nbsp;"+user.email+"&nbsp;&nbsp;")
                                       )
                            );
                }
                
                this.insUserInTest = function (obj){
                    var test_status="";
                    obj.empty();
                    obj.append(config.getLoadingData());
                    that.removeServerError();
                    $.each(listeners, function(i){
                        listeners[i].insUserInTest(testid,test_status);
                    });
                }
                this.addUser = function (user_id,obj){
                    var test_status="";
                    if(obj.find("i").hasClass("fa-check")){
                        obj.find("i").removeClass("fa-check");
                        obj.find("i").addClass("fa-trash");
                        outoff_std++;
                        test_status="true";
                    }else{
                        obj.find("i").removeClass("fa-trash");
                        obj.find("i").addClass("fa-check");
                        outoff_std--;
                        test_status="false";
                    }
                    that.setTotal();
                    that.setOutOff();
                    $.each(listeners, function(i){
                        listeners[i].updTestInUser(testid,user_id,test_status);
                    });
                }
                this.addAllUser = function (){
                    ul.children().each(function() {
                        var user_id=$(this).attr("id");
                        $(this).find(".btn").find("i").removeClass("fa-check");
                        $(this).find(".btn").find("i").addClass("fa-trash");
                        $.each(listeners, function(i){
                             listeners[i].updTestInUser(testid,user_id,"true");
                        });
                        outoff_std++;
                    });
                    that.setTotal();
                    that.setOutOff();
                }
                this.removeAllUser = function (){
                    ul.children().each(function() {
                        var user_id=$(this).attr("id");
                        $(this).find(".btn").find("i").removeClass("fa-trash");
                        $(this).find(".btn").find("i").addClass("fa-check");
                        $.each(listeners, function(i){
                             listeners[i].updTestInUser(testid,user_id,"false");
                        });
                    });
                    that.setTotal();
                    that.setOutOff();
                }
                
                this.loadUserList =function (){
                    total_std=0;
                    outoff_std=0;
                    $.each(listeners, function(i){
                        listeners[i].selUserList(testid);
                    });
                } 
                
                this.addUserList =function (data){
                   ul.append(that.getUser(data));
                   total_std++;
                   that.setTotal();
                   that.setOutOff();
                } 
                this.setTotal = function (){
                    total.empty();
                    total.append(total_std);
                }
                this.setOutOff = function (){
                    outoff.empty();
                    outoff.append(outoff_std);
                }
                this.selLoadBegin =function (){
                    ul.empty();
                    ul.append($("<li>").addClass("item").append(config.getLoadingData()));
                }
                this.selLoadFinish=function (){
                    ul.empty();
                }
                this.selLoadFail = function() { 
                    ul.append($("<li>").addClass("item").append("Server doesn't responce."));
                }
                
                this.setDisableButton =function (){
                    model_footer.find("#"+c).prop('disabled', true);
                    model_footer.find("#cancel").prop('disabled', true);
                }
                
                this.setEnableButton =function (){
                    model_footer.find("#"+c).prop('disabled', false);
                    model_footer.find("#cancel").prop('disabled', false);
                }
                
                this.setServerError =function (){
                    ul.append("<li>Error: Server doesn't Respond.<li>");
                }
                
                this.setError =function (message){
                    model_footer.find("#error_msg").append(message);
                }
                
                this.removeServerError =function (){
                    error.empty();
                }
                this.insLoadBegin =function (){
                    save.empty();
                    save.append(config.getLoadingData());
                }
                this.insLoadFinish =function (){
                    save.empty();
                    save.append("Save");
                }
                this.insLoadFail = function() { 
                    error.empty();
                    error.append("Server doesn't respond.");
                }
                
                
                this.addListener = function(list){
                            listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	UserListVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selUserList : function() { },
			delUserList: function() { },
			updUserList: function() { }
		}, list);
	}
});
