jQuery.extend({
	ResultSummeryVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody;
                var direction;
                var test_id;
                var user_id;
                var test_details_panel;
                var question_box_body;
                var optained;
                var lang_drop_down;
                var path;
                
                this.getResultSummery = function(test_id,user_id,value,path){
                    that.test_id=test_id;
                    that.user_id=user_id;
                    optained=value;
                    that.path=path;
                    discription={};
                    var div=$("<div></div>");
                        //--------------------------------------------------------
			direction =$("<div></div>");
                        var box=that.getBoxbody();
                            var panel =$("<div></div>").addClass("panel panel-default col-lg-3 no-padding");
                            panel.append($("<div>").addClass("box-header with-border").append($("<h3>").addClass("box-title").append($("<b>").append("Test Details"))));
                            test_details_panel=$("<div>").addClass("panel-body no-padding")
                            panel.append(test_details_panel);
                            box.append(panel);
                            box.append($("<div></div>").addClass("col-lg-1"));
                            var panel_question =$("<div></div>").addClass("panel panel-default col-lg-8 no-padding")
                            panel_question.append($("<div>").addClass("box-header with-border").append($("<h3 class='box-title'>Question</h3>")));
                            question_box_body=$("<div></div>").addClass("box-body");
                            panel_question.append(question_box_body);
                            box.append(panel_question);
                            direction.append(box);
                            div.append(direction);
                            i=0;
                        return div;
		}
                
                this.setTestDetails= function (data){
                    test_details_panel.append(that.getLabel("Test Id :",data.unique_test_key));
                    test_details_panel.append(that.getLabel("Test Name :",data.test_name));
                    test_details_panel.append(that.getLabel("Test date :",data.e_date));
                    test_details_panel.append(that.getLabel("Test Time :",data.start_time));
                    test_details_panel.append(that.getLabel("Active date :",data.test_date));
                    test_details_panel.append(that.getLabel("Active Time :",data.active_time));
                    test_details_panel.append(that.getLabel("Cut Off :",data.cut_off));
                    test_details_panel.append(that.getLabel("Test Mark :",data.sum));
                    test_details_panel.append(that.getLabel("Obtained marks :",parseFloat(data.obtained).toFixed(2)));
                    test_details_panel.append(that.getLabel("Negative :",parseFloat(data.negative).toFixed(2)));
                    test_details_panel.append(that.getLabel("Total marks :",optained).css({'color':'red'}));
                }
                
                this.getLabel = function (title,value){
                    return $("<div>").addClass("form-group col-lg-12")
                            .append($("<label>").append(title))
                            .append($("<label>").addClass("pull-right").append(value))
                }
                var i=0;
                this.setQuestionBox = function (data){
                        if(data["select_option"]!=null){
                            if(data["answer"]==data["select_option"]){
                                question_box_body.append(that.getQuestionButton("fa-check",i+1,data.question_id,data["answer"],data["select_option"]))
                            }else
                                question_box_body.append(that.getQuestionButton("fa-times",i+1,data.question_id,data["answer"],data["select_option"]))
                        }else
                            question_box_body.append(that.getQuestionButton("",i+1,data.question_id,data["answer"]))
                        
                        i++;
                }
                this.getQuestionButton = function (icon,value,question_id,answer,select){
                  return $("<div>").addClass("btn-group")
                            .append($("<button>").addClass("btn btn-primary dropdown-toggle")
                                .append(value)
                                .append($("<span>").addClass("fa "+icon))
                                .click(function (){
                                    that.showSelected_Question($(this),value,icon,question_id,answer,select)
                                })
                            );              
                }
                
                this.showSelected_Question= function (obj,value,icon,question_id,answer,select){
                    if(config.getModel("Language")==null){
                        $.getScript("../model/Language.js").done(function() {
                                try{
                                        config.setModel("Language",new $.Language(config));
                                        config.getModel("Language").selLanguage();
                                }catch(e){
                                    alert(e);
                                }
                        }).fail(function() {alert("Error:002 js");}); 
                    }
                    if(that.path==null){
                        that.path="../"
                    }
                    obj.empty();
                    obj.append(config.getLoadingData());
                    if(config.getView("SelectedQusVgr")==null){
                        $.getScript(that.path+"view/SelectedQusVgr.js").done(function(){
                            config.setView("SelectedQusVgr",new $.SelectedQusVgr(config));
                            var model = config.getView("ModelVgr").getModel("Question No."+value,config.getView("SelectedQusVgr").getSelectedQus(question_id,answer,select,that.path),"submit");
                            model.find(".modal-header").addClass(" no-border");
                            model.find(".modal-header").find("h4").append(that.getLanBox());
                                $.getScript(that.path+"model/SelectedQus.js").done(function() {
                                    config.setModel("SelectedQus",new $.SelectedQus(config));
                                    $.getScript(that.path+"controller/SelectedQusMgr.js").done(function() {
                                        config.setController("SelectedQusMgr",new $.SelectedQusMgr(config,config.getModel("SelectedQus"),config.getView("SelectedQusVgr")));
                                        obj.empty();
                                        obj.append(value);
                                        obj.append($("<span>").addClass("fa "+icon));
                                        $("hidden").empty();
                                        $("hidden").append(model);
                                        model.find(".modal-footer").remove();
                                        model.modal('show');
                                        setTimeout(function (){that.loadLanguage();},1000)
                                    }).fail(function() {alert("Error :problem in TestUserListMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in TestUserListvgr.js file ");}); 
                        }).fail(function() {alert("Error:004 problem in SelectedQusVgr.js file ");}); 
                     }else{
                            var model = config.getView("ModelVgr").getModel("Question No."+value,config.getView("SelectedQusVgr").getSelectedQus(question_id,answer,select,path),"submit");
                            model.find(".modal-header").addClass(" no-border");
                            model.find(".modal-header").find("h4").append(that.getLanBox());
                            obj.empty();
                            obj.append(value);
                            obj.append($("<span>").addClass("fa "+icon));
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.find(".modal-footer").remove();
                            model.modal('show');
                            setTimeout(function (){that.loadLanguage();},200)
                     }   
                }
                
                this.getLanBox = function (){
                    lang_drop_down =$("<div>").addClass("form-group pull-right").css({'margin-right':'15px'});
                    lang_drop_down.append($("<select>").addClass("form-control lang1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        config.getView("SelectedQusVgr").loadSelectedQus();
                    });
                    return lang_drop_down;
                }
                this.getLangId=function (){
                    var lang_id;
                        $( ".lang1 option:selected" ).each(function() {
                            lang_id = $( this ).val();
                        });
                        return lang_id;
                }
                this.loadLanguage =function (module){
                    var all = config.getModel("Language").selLanguage(module);
                    lang_drop_down.find(".lang1").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addLanguage(all[item]);
                                });
                         }
                         config.getView("SelectedQusVgr").loadSelectedQus();
                } 
                 this.addLanguage=function (data){
                   lang_drop_down.find(".select2").append(that.getLangOption(data))
                }          
                this.getLangOption = function (data){
                    var row = $("<option></option>").attr({'value':data.language_id})
                              .append($("<td>"+data.language_name+"</td>"));
                    return row;            
                }
                
                this.loadUserTestDetails =function (){
                    $.each(listeners, function(i){
                        listeners[i].selUserTestDetails(that.test_id,that.user_id,1);
                    });
                    that.loadResultSummery();
                } 
                this.loadResultSummery =function (){
                    $.each(listeners, function(i){
                        listeners[i].selResultSummery(that.test_id,that.user_id);
                    });
                } 
                
                this.getBoxbody = function (){
                    var div =$("<div></div>").addClass("box-body");
                    return div;
                }
                this.addResultSummery =function (data){
                   that.setQuestionBox(data);
                }                                                        
                this.addUserTestDetails =function (data){
                    that.setTestDetails(data)
                }                                                        
                this.addImgDscription =function (data){
                   tbody_img.append(that.getImgRow(data))
                }                                                        
                this.addImgQuestion =function (data){
                   tbody_img.append(that.getImgRow(data))
                }                                                        
                
//                this.addResultSummery=function (data){
//                   cat_dorp_down.find(".select2").append(that.getOption(data))
//                }          
                this.removeFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.selLoadBegin =function (){
                    question_box_body.append(config.getLoadingData());
                }
                this.selLoadFinish=function (){
                    question_box_body.empty();
                }
                this.selLoadFail = function() { 
                    question_box_body.append($("<p>").append("Server Doesn't Response"));
                }
                this.selLoadBegin_TestDetails =function (){
                    test_details_panel.append(config.getLoadingData());
                }
                this.selLoadFinish_TestDetails=function (){
                    test_details_panel.empty();
                }
                this.selLoadFail_TestDetails = function() { 
                    test_details_panel.append($("<p>").append("Server Doesn't Response"));
                }
                this.selRemoveFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                this.addListener = function(list){
                            listeners.push(list);
                }
                this.setloading =function (){
                    direction.find("#img_loading").append(config.getLoadingData());
                }
                this.removeLoading =function (){
                    direction.find("#img_loading").empty();
                }
                this.setDisableButton =function (){
                    direction.find("#cancel").prop('disabled', true);
                }
                this.setEnableButton =function (){
                    direction.find("#cancel").prop('disabled', false);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	ResultSummeryVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insResultSummery : function(from){},
			selResultSummery : function() { },
                        delResultSummery : function(id) { },
                        getResultSummery : function(id) { },
                        updResultSummery : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
