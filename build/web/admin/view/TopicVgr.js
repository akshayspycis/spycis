jQuery.extend({
	TopicVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody;
                var topic;
                var row_id;
                var cat_dorp_down;
                var sub_cat_dorp_down;
                var sec_dorp_down;
                var sel_sub_cat;
                var sel_sec;
                var b=true;
                var count=0;
                
                this.getTopic = function(){
                    config.getView("ContentWrapperVgr").setBoxHeader("Configuration");
			topic =$("<div></div>").addClass("col-md-6");
                        topic.append(that.getPanelTitle);
                        var box=that.getBoxbody();
                        var table = that.getTable();
                        tbody=table.find("tbody");
                        box.append(that.getCategory_DropDown);
                        box.append(that.getSubCategory_DropDown);
                        box.append(that.getSection_DropDown);
                        box.append(table);
                        topic.append(box);
                        var box1=that.getBoxbody1();
                        topic.append(box1);
                        topic.append(that.getBoxFooter());
                        //that.loadTopic();
                        //-----------this code is resposible for load category and load subcatgory--------------------------
                        that.loadCategory();
                        cat_dorp_down.find(".cat1").trigger("change");
                        return topic;
		}

                this.getPanelTitle = function (){
                    var div =$("<div></div>").addClass("box-header with-border");
                    div.append($("<h3></h3>").addClass("box-title").append($("<b>Topic Details</b>")));
                    return div;
                }
                
                this.getCategory_DropDown = function (){
                    tbody.empty();
                    cat_dorp_down =$("<div></div>").addClass("form-group");
                    cat_dorp_down.append($("<label>Category :</label>"))
                    cat_dorp_down.append($("<p></p>"))
                    cat_dorp_down.append($("<select>Category :</select>").addClass("form-control cat1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var id = cat_dorp_down.find(".cat1 option:first").val();
                            $( ".cat1 option:selected" ).each(function() {
                              id = $( this ).val();
                            });
                        that.loadSubCategory(null,id,3);
                        
                    });
                    return cat_dorp_down;
                }
                
                this.getSubCategory_DropDown = function (){
                    sub_cat_dorp_down =$("<div></div>").addClass("form-group");
                    sub_cat_dorp_down.append($("<label>Sub Category :</label>"))
                    sub_cat_dorp_down.append($("<a></a>").attr({'id':'sub_img_loading'}));
                    sub_cat_dorp_down.append($("<p></p>"))
                    sub_cat_dorp_down.append($("<select>Sub Category :</select>").addClass("form-control sub1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:first").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        $( ".sub1 option:selected" ).each(function() {
                          subcategory_id = $( this ).val();
                        });
                            that.loadSection(null,category_id,subcategory_id,1);
                    });
                    return sub_cat_dorp_down;
                }
                
                this.getSection_DropDown = function (){
                    sec_dorp_down =$("<div></div>").addClass("form-group");
                    sec_dorp_down.append($("<label>Section :</label>"))
                    sec_dorp_down.append($("<a></a>").attr({'id':'sec_img_loading'}));
                    sec_dorp_down.append($("<p></p>"))
                    sec_dorp_down.append($("<select></select>").addClass("form-control sec1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var section_id = sec_dorp_down.find(".sec1 option:first").val();
                        $( ".sec1 option:selected" ).each(function() {
                            section_id = $( this ).val();
                        });
                        that.loadTopic(category_id,subcategory_id,section_id);
                    });
                    return sec_dorp_down;
                }

                this.getBoxbody = function (){
                    var div =$("<div></div>").addClass("box-body").attr({'id':'sub'});
                    return div;
                }
                
                this.getBoxbody1 = function (){
                    var div =$("<div></div>").addClass("box-body").attr({'id':'sub_my'});
                    return div;
                }
                
                this.getTable = function (){
                    var div =$("<table></table>").addClass("table table-bordered")
                    //set header for table 
                            .append($("<thead></thead>")
                                .append($("<tr></tr>")
                                .append($("<th></th>").append("#").css({'width':'10px'}))
                                .append($("<th></th>").append("Topic Name"))
                                .append($("<th></th>").append("Action").css({'width':'40px'}))
                            ))
                            .append($("<tbody></tbody>").attr({'id':'topic_tb'}))
                    return div;
                }
                
                this.getRow = function (data){
                    count++;
                    var row = $("<tr></tr>").attr({'id':data.topic_id})
                            .append($("<td>"+count+"</td>"))
                            .append($("<td>"+data.topic_name+"</td>"))
                            .append($("<td></td>").append(that.getDropdown(data.topic_id))
                    );
                    return row;            
                }
                
                this.getCatOption = function (data){
                    var row = $("<option></option>").attr({'value':data.category_id})
                              .append($("<td>"+data.category_name+"</td>"));
                    return row;            
                }
                
                this.getSubOption = function (data){
                    
                    var row = $("<option></option>").attr({'value':data.subcategory_id})
                              .append($("<td>"+data.subcategory_name+"</td>"));
                    return row;            
                }
                
                this.getSecOption = function (data){
                    var row = $("<option></option>").attr({'value':data.section_id})
                              .append($("<td>"+data.section_name+"</td>"));
                    return row;            
                }
                
                this.getDropdown = function (a){
                    var drop_down =$("<div></div>").addClass("input-group-btn")
                                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                        .append($("<span></span>").addClass("fa fa-caret-down")))
                                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li></li>").append($("<a>Delete</a>")))
                                            .append($("<li></li>").addClass("divider"))
                                            .append($("<li></li>").append($("<a>Edit</a>"))));
                    
                    return that.getClickEvent(drop_down,a);
                }
                
                this.getClickEvent =function (drop_down,topic_id){
                    $(drop_down).find('ul>li:nth-child(1)').click(function (){
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                        var obj={};
                        obj["topic_id"]=topic_id;
                        obj["category_id"]=category_id;
                        obj["subcategory_id"]=subcategory_id;
                        obj["section_id"]=section_id;
                        var model = config.getView("ErrorMsgVgr").getErrorMsg("Delete Topic","Are you sure you want to delete this Topic?",obj,that.delTopic);
                        $("#hidden").empty();
                        $("#hidden").append(model);
                        model.modal('show');
                    });
                    
                    $(drop_down).find('ul>li:nth-child(3)').click(function (){
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var section_id = sec_dorp_down.find(".sec1 option:selected").val();
                        that.updTopic($("#hidden"),category_id,subcategory_id,section_id,topic_id);
                    });
                    return drop_down;
                }
                
                this.delTopic =function (obj){
                    row_id =obj["topic_id"];
                    $.each(listeners, function(i){
                        listeners[i].delTopic(obj["category_id"],obj["subcategory_id"],obj["section_id"],obj["topic_id"]);
                    });
                    return true;
                }
                
                this.updTopic =function (div,category_id,subcategory_id,section_id,topic_id){
                    that.setUpdForm(div,category_id,subcategory_id,section_id,topic_id);
                }
                
                this.getBoxFooter =function (){
                    var div = $("<div></div>").addClass("box-footer clearfix");
                        div.append($("<div></div>").addClass("col-md-3 col-xs-5")
                                .append($("<button></button>").addClass("btn btn-block btn-primary").attr({'data-toggle':'modal','data-target':'myModal'})
                                    .append("New").click(function ()
                                        {
                                            that.setInsForm($("#hidden"))
                                        }
                                     )
                                    ))
                        div.append($("<div></div>").addClass("col-md-3 col-xs-6 pull-right")
                                .append($("<button></button>").addClass("btn btn-block btn-primary")
                                    .append("Refresh").click(function ()
                                        {
                                            sec_dorp_down.find(".sec1").trigger("change");
                                        }
                                     )
                                )
                        )
                        return div;
                }
                
                this.setInsForm=function (a){
                    var model = config.getView("ModelVgr").getModel("New Topic",that.getInsForm(),"submit");
                    a.empty();
                    a.append(model);
                    var sub=false;
                    var topic_form =$('#topic_form');
                    topic_form.validate({
                            rules: {
                                topic_name: {minlength: 3,required: true,required: true},
                                      agree: "required"
                                  },
                                highlight: function(element) {$(element).closest('.control-group').removeClass('success').addClass('error');sub=false;},
                                success: function(element) {element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');sub=true;}
                    });
                    topic_form.find("#submit").click(function(){
                        if(sub){
                                that.removeFail();
                                var category_id;
                                var subcategory_id;
                                var section_id;
                                topic_form.find(":input").each(function() {
                                        if($(this).attr("name")=="category_id"){
                                            category_id= $(this).val();
                                        }
                                        if($(this).attr("name")=="subcategory_id"){
                                            subcategory_id= $(this).val();
                                        }
                                        if($(this).attr("name")=="section_id"){
                                            section_id= $(this).val();
                                        }
                                }); 
                                that.setSelectedTopic(category_id,subcategory_id,section_id);
                                $.each(listeners, function(i){
                                        listeners[i].insTopic(topic_form);
                                });
                                sub=false;
                        }
                     });
                    $('#myModal').modal('show');
                }
                
                this.setUpdForm=function (a,category_id,subcategory_id,section_id,topic_id){
                    var model = config.getView("ModelVgr").getModel("View Topic",that.getUpdForm(category_id,subcategory_id,section_id,topic_id),"submit_topic");
                    a.empty();
                    a.append(model);
                    var sub=false;
                    var topic_form_upd =$('#topic_form_upd');
                    topic_form_upd.validate({
                            rules: {
                               topic_name: {minlength: 3,required: true,required: true},
                               agree: "required"
                               },
                                highlight: function(element) {$(element).closest('.control-group').removeClass('success').addClass('error');sub=false;},
                                success: function(element) {element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');sub=true;}
                    });
                    
                    topic_form_upd.find("#submit_topic").click(function(){
                        if(sub){
                                that.removeFail();
                                $.each(listeners, function(i){
                                    listeners[i].updTopic(topic_form_upd);
                                });
                                sub=false;
                                $('#myModal').modal('toggle');
                        }
                     });
                    $('#myModal').modal('show');
                }
                
                this.getInsForm =function (){
                    var sel_cat = $("<select></select>").addClass("cat2 input-xlarge form-control").attr({'id':'category_id','placeholder':'Select','name':'category_id'}).change(function () {
                        var id = sel_cat.find("option:first").val();
                            sel_cat.find("option:selected" ).each(function() {
                              id = $( this ).val();
                            });
                        that.loadSubCategory(sel_sub_cat,id,4);
                    });
                    //call the function load category data form cache in model category dropdown list..................
                    that.loadCategory(sel_cat);
                    sel_sub_cat = $("<select></select>").addClass("sub2 input-xlarge form-control").attr({'id':'subcategory_id','placeholder':'Select','name':'subcategory_id'}).change(function () {
                        var subcategory_id = sel_sub_cat.find("option:first").val();
                        var category_id = sel_cat.find("option:selected").val();
                            sel_sub_cat.find("option:selected" ).each(function() {
                              subcategory_id = $( this ).val();
                            });
                        that.loadSection(sel_sec,category_id,subcategory_id,2);
                    });
                    sel_sec = $("<select></select>").addClass("input-xlarge form-control").attr({'id':'section_id','placeholder':'Select Section','name':'section_id'});
                    //trigger of category dropdown change function which call loadSubcategory 
                    setTimeout(function (){sel_cat.trigger("change");},20)
                    
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'topic_form'})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                        .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Category Name"))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls").append(sel_cat))
                                        .append($("<label></label>").addClass("control-label").attr({'for':'Sub category'}).append("Sub Category "))
                                        .append($("<a></a>").attr({'id':'model_sub_img_loading'}))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls").append(sel_sub_cat))
                                        .append($("<label></label>").addClass("control-label").attr({'for':'Section'}).append("Section "))
                                        .append($("<a></a>").attr({'id':'model_sec_img_loading'}))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls").append(sel_sec))
                                        .append($("<label></label>").addClass("control-label").attr({'for':'topic_name'}).append("Topic Name"))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls")
                                        .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'topic_name','placeholder':'Topic Name','name':'topic_name'})))
                                         )
                                 );
                    return form;        
                }
                
                this.getUpdForm =function (category_id,subcategory_id,section_id,topic_id){
                    var topic_name ;
                    $.each(listeners, function(i){
                        topic_name=listeners[i].getTopic(category_id,subcategory_id,section_id,topic_id);
                    });
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'topic_form_upd'})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'topic_name'}).append("Topic Name"))
                                            .append($("<p></p>"))
                                            .append($("<div></div>").addClass("controls")
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'topic_name','placeholder':'Topic Name','name':'topic_name','value':topic_name}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'category_id','name':'category_id','value':category_id}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'subcategory_id','name':'subcategory_id','value':subcategory_id}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'section_id','name':'section_id','value':section_id}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'topic_id','name':'topic_id','value':topic_id})))
                                         )
                                 );
                    return form;        
                }
                
                this.addTopic =function (data){
                   tbody.append(that.getRow(data))
                }                                                        
                
                this.addCategory=function (data){
                   cat_dorp_down.find(".select2").append(that.getCatOption(data))
                }          
                
                this.addSubCategory=function (data){
                   sub_cat_dorp_down.find(".sub1").append(that.getSubOption(data))
                }          
                
                this.addSection=function (data){
                   sec_dorp_down.find(".sec1").append(that.getSecOption(data))
                }          
                
                this.addSubCategoryInModel=function (data){
                   sel_sub_cat.append(that.getSubOption(data))
                }          
                
                this.addSectionInModel=function (data){
                   sel_sec.append(that.getSecOption(data))
                }          
                
                this.setSelectedTopic =function (category_id,subcategory_id,section_id){
                    cat_dorp_down.find(".cat1").val(category_id);
                    that.loadSubCategory(null,category_id,3);
                    sub_cat_dorp_down.find(".sub1").val(subcategory_id);
                    that.loadSection(null,category_id,subcategory_id,3);
                    sec_dorp_down .find(".sec1").val(section_id);
                    sec_dorp_down .find(".sec1").trigger("change");
                }
                
                this.updTopicRow =function (data){
                    $("#"+data.topic_id).find("td:nth-child(2)").empty();
                    $("#"+data.topic_id).find("td:nth-child(2)").append(data.topic_name);
                }                                                        
                
                this.insLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.insLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                }
                
                this.insLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                this.delLoadBegin =function (){
                    $("#"+row_id).find("td:nth-child(2)").empty();
                    $("#"+row_id).find("td:nth-child(2)").append(config.getLoadingData());
                }
                
                this.delLoadFinish =function (){
                    $("#"+row_id).remove();
                }
                
                this.delLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                this.removeFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.selLoadBegin =function (){
                    tbody.append(config.getLoadingData());
                }
                
                this.topicSubLoadBegin =function (){
                    sub_cat_dorp_down.find("#sub_img_loading").append(config.getLoadingData());
                }
                
                this.topicModelSubLoadBegin =function (){
                    $("#model_sub_img_loading").append(config.getLoadingData());
                }
                
                this.topicSecLoadBegin =function (){
                    sec_dorp_down.find("#sec_img_loading").append(config.getLoadingData());
                }
                
                this.topicModelSecLoadBegin =function (){
                    $("#model_sec_img_loading").append(config.getLoadingData());
                }

                this.selLoadFinish=function (){
                    tbody.empty();
                }
                
                this.topicSubLoadFinish=function (){
                    sub_cat_dorp_down.find("#sub_img_loading").empty();
                    setTimeout(function (){
                        sub_cat_dorp_down.find(".sub1").trigger("change");
                    },20);
                    
                }
                
                this.topicModelSubLoadFinish=function (){
                    $("#model_sub_img_loading").empty();
                    setTimeout(function (){
                        sel_sub_cat.trigger("change");
                    },20);
                }
                
                this.topicSecLoadFinish=function (){
                    sec_dorp_down.find("#sec_img_loading").empty();
                    setTimeout(function (){
                        sec_dorp_down.find(".sec1").trigger("change");
                    },20);
                    
                }
                
                this.topicModelSecLoadFinish=function (){
                    $("#model_sec_img_loading").empty();
                    setTimeout(function (){
                        sel_sec.trigger("change");
                    },20);
                }

                this.selLoadFail = function() { 
                    tbody.append("<ts>")
                }
                
                this.topicSubLoadFail = function() { 
                    sub_cat_dorp_down.find("#sub_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.topicModelSubLoadFail = function() { 
                    $("#model_sub_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.topicSecLoadFail = function() { 
                    sec_dorp_down.find("#sec_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.topicModelSubLoadFail = function() { 
                    $("#model_sec_img_loading").append("Error: Server doesn't Respond.");
                    b=false;
                }
                
                this.selRemoveFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.updLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.updLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                }
                
                this.updLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
            
                this.loadTopic =function (t){
                    var all = config.getModel("Topic").selTopic();
                    if(t==null){
                        cat_dorp_down.find(".select2").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addTopic(all[item]);
                                });
                         }
                    }else{
                        if(all){
                                $.each(all, function(item){
                                    t.append(that.getOption(all[item]))
                                });
                         }
                    }
                } 
                
                this.loadCategory =function (t){
                    var all = config.getModel("Category").selCategory();
                    if(t==null){
                        cat_dorp_down.find(".cat1").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addCategory(all[item]);
                                });
                         }
                    }else{
                        if(all){
                                $.each(all, function(item){
                                    t.append(that.getCatOption(all[item]))
                                });
                         }
                    }
                } 
                
                this.loadSubCategory =function (t,category_id,module){
                    if(t==null){
                        sub_cat_dorp_down.find(".sub1").empty();
                        var all = config.getModel("SubCategory").selSubCategory(category_id,module);
                        if(all){
                                $.each(all, function(item){
                                    that.addSubCategory(all[item]);
                                });
                                sub_cat_dorp_down.find(".sub1").trigger("change");
                         }
                    }else{
                        t.empty();
                        var all = config.getModel("SubCategory").selSubCategory(category_id,module);
                        if(all){
                                $.each(all, function(item){
                                    t.append(that.getSubOption(all[item]))
                                });
                                sel_sub_cat.trigger("change");
                         }
                    }
                } 
                
                this.loadSection =function (t,category_id,subcategory_id,module){
                    if(t==null){
                        sec_dorp_down.find(".sec1").empty();
                        var all = config.getModel("Section").selSection(category_id,subcategory_id,module);
                        if(all){
                                $.each(all, function(item){
                                    that.addSection(all[item]);
                                });
                                sec_dorp_down.find(".sec1").trigger("change");
                         }
                         b=false;
                         
                    }else{
                        sel_sec.empty();
                        var all = config.getModel("Section").selSection(category_id,subcategory_id,module);
                        if(all){
                                $.each(all, function(item){
                                    t.append(that.getSecOption(all[item]))
                                });
                                sel_sec.trigger("change");
                         }
                         
                    }
                } 
                
                
                //this is code run when topic subcatgory detial does not have in cache object and this funcation call inside the class manager of sub category
                this.subAddInTopic =function (item){
                    that.addSubCategory(item);
                } 
                
                this.subAddInTopicModel =function (item){
                    that.addSubCategoryInModel(item);
                } 
                //---------------------------------------------------------------------------------------------------------------------------------
                //this is code run when Section detial does not have in cache object and this funcation call inside the class manager of Section
                this.secAddInTopic =function (item){
                    that.addSection(item);
                } 
                
                this.secAddInTopicModel =function (item){
                    that.addSectionInModel(item);
                } 
                //---------------------------------------------------------------------------------------------------------------------------------
                this.loadTopic =function (category_id,subcategory_id,section_id){
                    count=0;
                    tbody.empty();
                    $.each(listeners, function(i){
                        listeners[i].selTopic(category_id,subcategory_id,section_id);
                    });
                } 

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	TopicVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insTopic : function(from){},
			selTopic : function() { },
                        delTopic : function(id) { },
                        getTopic : function(id) { },
                        updTopic : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
