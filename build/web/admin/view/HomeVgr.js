jQuery.extend({
	HomeVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody;
                var home_div;
                var row_id;
                var count=0;
                
                this.getHome = function(){
                    config.getView("ContentWrapperVgr").setBoxHeader("Home");
                    home_div =$("<div></div>").addClass("row");
                    var data={
                        "title":"Configuration",
                        "box_clss":"box-primary",
                        "clss":"fa fa-gears text-capitalize",
                        "list":{
                            "1":{"clss":"fa fa-list text-blue","title":"Category Details","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(2)").find("ul li:nth-child(1)").find("a").trigger("click");
                            }},
                            "2":{"clss":"fa fa-list-ul text-green","title":"Sub Category Details","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(2)").find("ul li:nth-child(2)").find("a").trigger("click");
                            }},
                            "3":{"clss":"fa fa-th-list text-maroon","title":"Section Details","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(2)").find("ul li:nth-child(3)").find("a").trigger("click");
                            }},
                            "4":{"clss":"fa fa-align-justify text-orange","title":"Topic Details","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(2)").find("ul li:nth-child(4)").find("a").trigger("click");
                            }},
                            "5":{"clss":"fa fa-language text-olive","title":"Language Details","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(2)").find("ul li:nth-child(5)").find("a").trigger("click");
                            }},
                            "6":{"clss":"fa fa-certificate","title":"Exam Phase Details","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(2)").find("ul li:nth-child(6)").find("a").trigger("click");
                            }},
                        }
                    }
                    home_div.append(that.getBoxbody(data));
                    data={
                        "title":"Question Management",
                        "box_clss":"box-warning",
                        "clss":"fa fa-pencil-square-o",
                        "list":{
                            "1":{"clss":"fa fa-question-circle text-maroon","title":"Direction wise Question","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(3)").find("ul li:nth-child(1)").find("a").trigger("click");
                            }},
                            "2":{"clss":"fa fa-question text-purple","title":"Question Without Dire.","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(3)").find("ul li:nth-child(2)").find("a").trigger("click");
                            }},
                            "3":{"clss":"fa fa-file-text-o text-olive","title":"View Question Bank","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(3)").find("ul li:nth-child(3)").find("a").trigger("click");
                            }}
                        }
                    }
                    home_div.append(that.getBoxbody(data));
                    data={
                        "title":"User Management",
                        "box_clss":"box-success",
                        "clss":"fa fa-user",
                        "list":{
                            "1":{"clss":"fa fa-arrow-circle-o-right text-orange","title":"View Test","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(4)").find("ul li:nth-child(1)").find("a").trigger("click");
                            }},
                            "2":{"clss":"fa fa-users text-blue","title":"User Details","callback":function (){
                               config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(4)").find("ul li:nth-child(2)").find("a").trigger("click");
                            }}
                        }
                    }
                    home_div.append(that.getBoxbody(data));
                    return home_div;
		}

                this.getBoxbody = function (data){
                    var ul=$("<ul>").addClass("nav nav-pills nav-stacked");
                    $.each(data.list,function (key,value){
                        ul.append(that.getButton(value.clss,value.title,value.callback));
                    });
                    var div =$("<div>").addClass("col-md-3")  
                            .append($("<div>").addClass("box box-solid "+data.box_clss)
                                .append($("<div>").addClass("box-header with-border")
                                    .append($("<i>").addClass(data.clss))
                                    .append($("<h3>").addClass("box-title").append(data.title))
                                )
                                .append($("<div>").addClass("box-body")
                                    .append(ul)
                                )
                            );
                    return div;
                }
                
                this.getButton = function (clss,title,callback){
                    var row = $("<li>")
                            .append($("<a>").addClass("btn btn-app")
                                .append($("<i>").css({'font-size':'44px'}).addClass(clss))
                                .append(title)
                                .click(function (){
                                    callback();
                                })
                            );
                    return row;            
                }
                
                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	HomeVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insHome : function(from){}
		}, list);
	}
});
