jQuery.extend({
	ResultSummeryMgr: function(config,model,view){
		var vlist = $.ResultSummeryVgrListener({
			selResultSummery : function(test_id,user_id,module){
                            var all = model.selResultSummery(test_id,user_id,module);
                            if(all){
                                $.each(all, function(item){
                                    view.addResultSummery(all[item]);
                                });
                            }
			},
			selUserTestDetails : function(test_id,user_id,module){
                            
                            var all = model.selUserTestDetails(test_id,user_id,module);
                            if(all){
                                $.each(all, function(item){
                                    view.addUserTestDetails(all[item]);
                                });
                            }
			}
		});
		view.addListener(vlist);

            var mlist = $.ResultSummeryListener({
			loadItem : function(item,module){
                                switch(module){
                                    case 1:
                                        view.addUserTestDetails(item);
                                    break;    
                                    default:
                                        view.addResultSummery(item);
                                    break;    
                                }
                                
			},
                        selLoadBegin:function (module){
                            switch(module){
                                    case 1:
                                        view.selLoadBegin_TestDetails()
                                    break;    
                                    default:
                                        view.selLoadBegin();
                                    break;    
                                }
                                
                        },
                        selLoadFail :function (module){
                                switch(module){
                                    case 1:
                                        view.selLoadFail_TestDetails();
                                    break;    
                                    default:
                                        view.selLoadFail();
                                    break;    
                                }    
                        },
                        selLoadFinish :function (module){
                            switch(module){
                                    case 1:
                                        view.selLoadFinish_TestDetails();
                                    break;    
                                    default:
                                        view.selLoadFinish();
                                    break;    
                                }    
                        }
		});
		model.addListener(mlist);
	}
});
