jQuery.extend({
	SubCategoryMgr: function(config,model, view){
		var vlist = $.SubCategoryVgrListener({
			insSubCategory : function(form){
				model.insSubCategory(form);
			},
			selSubCategory : function(id,category_id,module){
                            var all = model.selSubCategory(id,category_id,module);
                            if(all){
                                $.each(all, function(item){
                                    view.addSubCategory(all[item]);
                                });
                            }
			},
                        delSubCategory : function(id,category_id){
                                model.delSubCategory(id,category_id);
			},
                        updSubCategory : function(form){
                                model.updSubCategory(form);
			},
                        getSubCategory : function(id,category_id){
                                return model.getSubCategory(id,category_id);
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.SubCategoryListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
                        
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(item,module){
                            switch(module){
                                case 1:
                                    config.getView("SectionVgr").subAddInSction(item);
                                    break;
                                case 2:
                                    config.getView("SectionVgr").subAddInSctionModel(item);
                                    break;
                                case 3:
                                    config.getView("TopicVgr").subAddInTopic(item);
                                    break;
                                case 4:
                                    config.getView("TopicVgr").subAddInTopicModel(item);
                                    break;
                                case 5:
                                    config.getView("DirectionVgr").subAddInDirection(item);
                                    break;
                                case 6:
                                    config.getView("OnlyQuestionVgr").subAddInOnlyQuestion(item);
                                    break;
                                case 7:
                                    config.getView("ViewQuestionVgr").subAddInViewQuestion(item);
                                    break;
                                case 8:
                                    config.getView("ViewTestVgr").subAddInViewTest(item);
                                    break;
                                default:
                                    view.addSubCategory(item);
                                    break;
                            }
			},
                        updLoadItem : function(item){
				view.updSubCategory(item);
			},
                        
                        selLoadBegin:function (module){
                            switch(module){
                                case 1:
                                    config.getView("SectionVgr").sectionSubLoadBegin();
                                    break;
                                case 2:
                                    config.getView("SectionVgr").sectionModelSubLoadBegin();
                                    break;
                                case 3:
                                    config.getView("TopicVgr").topicSubLoadBegin();
                                    break;
                                case 4:
                                    config.getView("TopicVgr").topicModelSubLoadBegin();
                                    break;
                                case 5:
                                    config.getView("DirectionVgr").directionSubLoadBegin();
                                    break;
                                case 6:
                                    config.getView("OnlyQuestionVgr").onlyQuestionSubLoadBegin();
                                    break;
                                case 7:
                                    config.getView("ViewQuestionVgr").viewQuestionSubLoadBegin();
                                    break;
                                case 8:
                                    config.getView("ViewTestVgr").viewTestSubLoadBegin();
                                    break;
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                        },
                        
                        selLoadFail :function (module){
                            switch(module){
                                case 1:
                                    config.getView("SectionVgr").sectionSubLoadFail();
                                    break;
                                 case 2:
                                    config.getView("SectionVgr").sectionModelSubLoadFail();
                                    break;
                                 case 3:
                                    config.getView("TopicVgr").topicSubLoadFail();
                                    break;
                                 case 4:
                                    config.getView("TopicVgr").topicModelSubLoadFail();
                                    break;   
                                 case 5:
                                    config.getView("DirectionVgr").directionSubLoadFail();
                                    break;   
                                 case 6:
                                    config.getView("OnlyQuestionVgr").onlyQuestionSubLoadFail();
                                    break;   
                                 case 7:
                                    config.getView("ViewQuestionVgr").viewQuestionSubLoadFail();
                                    break;   
                                 case 8:
                                    config.getView("ViewTestVgr").viewTestSubLoadFail();
                                    break;   
                                default:
                                    view.selLoadFail();
                                    break;
                            }
                        },
                        
                        selLoadFinish :function (module){
                            switch(module){
                                case 1:
                                    config.getView("SectionVgr").sectionSubLoadFinish();
                                    break;
                                case 2:
                                    config.getView("SectionVgr").sectionModelSubLoadFinish();
                                    break;
                                case 3:
                                    config.getView("TopicVgr").topicSubLoadFinish();
                                    break;
                                case 4:
                                    config.getView("TopicVgr").topicModelSubLoadFinish();
                                    break;    
                                case 5:
                                    config.getView("DirectionVgr").directionSubLoadFinish();
                                    break;    
                                case 6:
                                    config.getView("OnlyQuestionVgr").onlyQuestionSubLoadFinish();
                                    break;    
                                case 7:
                                    config.getView("ViewQuestionVgr").viewQuestionSubLoadFinish();
                                    break;    
                                case 8:
                                    config.getView("ViewTestVgr").viewTestSubLoadFinish();
                                    break;    
                                default:
                                    view.selLoadFinish();
                                    break;
                            }
                        },
                        
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
