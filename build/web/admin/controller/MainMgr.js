
jQuery.extend({
	MainMgr: function(config){
		var vlist = $.MainVgrListener({
			loadHeaderVgr : function(){
                                $.getScript("../view/HeaderVgr.js").done(function(){
                                    try {
                                        config.setView("HeaderVgr",new $.HeaderVgr(config));
                                        vlist.loadMainSidebarVgr();
                                }catch(e){
                                        alert("Error:004 problem in headervgr.js file "+e);
                                }
                                }).fail(function() {alert("Error:004 problem in headervgr.js file ");}); 
			},
			loadHeader : function(){  
				$.getScript("../model/Header.js").done(function() {
                                    try{
                                        config.setModel("Header",new $.Header(config));
                                        vlist.loadNotificationMgr();
                                }catch(e){
                                    alert("Error:005 problem in header.js file "+e);
                                }
                                }).fail(function() {alert("Error:005 problem in headervgr.js file ");}); 
			},          
                        loadHeaderMgr : function(){
				$.getScript("controller/HeaderMgr.js").done(function() {
                                    try {
                                        config.setController("NotificationMgr",new $.NotificationMgr(config.getScriptModel("Notification"),config.getView("NotificationVgr")));
                                        vlist.loadNotificationData();
                                    }catch(e){
                                        alert(e);
                                    }
                                }).fail(function() {alert("Error :006 problem in headerMgr.js file ");}); 
                                model.notifyLoadFinish();
			},
                        loadMainSidebarVgr : function(){
                                $.getScript("../view/MainSidebarVgr.js").done(function() {
                                    try {
                                    config.setView("MainSidebarVgr",new $.MainSidebarVgr(config));
                                    vlist.loadContentWrapperVgr();
                                        
                                }catch(e){
                                    alert("Error:007 problem in MainSideBarVgr.js file "+e);
                                }
                                }).fail(function() {alert("Error :007 problem in MainSidebarVgr.js file ");}); 
			},
			loadMainSidebar : function(){  
				$.getScript("model/Notification.js").done(function() {
                                    try{config.setModel("Notification",new $.Notification());
                                    vlist.loadNotificationMgr();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error :008 problem in MainSidebar.js file ");}); 
			},          
                        loadMainSidebarMgr : function(){
				$.getScript("controller/notificationMgr.js").done(function() {
                                    try {
                                        config.setController("NotificationMgr",new $.NotificationMgr(config.getScriptModel("Notification"),config.getView("NotificationVgr")));
                                        vlist.loadNotificationData();
                                    }catch(e){
                                        alert(e);
                                    }
                                }).fail(function() {alert("Error :009 problem in MainSidebarMgr.js file ");}); 
                                model.notifyLoadFinish();
			},
                        loadContentWrapperVgr : function(){
                                $.getScript("../view/ContentWrapperVgr.js").done(function() {
                                    try {
                                    config.setView("ContentWrapperVgr",new $.ContentWrapperVgr(config));
                                    vlist.loadHomeVgr();
                                }catch(e){
                                    alert("Error:010 problem in ContentWrapperVgr.js file "+e);
                                }
                                }).fail(function() {alert("Error :010 problem in ContentWrapperVgr.js file ");}); 
			},
			loadContentWrapper : function(){  
				$.getScript("model/Notification.js").done(function() {
                                    try{config.setModel("Notification",new $.Notification());
                                    vlist.loadNotificationMgr();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error :011 problem in ContentWrapper.js file ");}); 
			},          
                        loadContentWrapperMgr : function(){
				$.getScript("controller/notificationMgr.js").done(function() {
                                    try {
                                        config.setController("NotificationMgr",new $.NotificationMgr(config.getScriptModel("Notification"),config.getView("NotificationVgr")));
                                        vlist.loadNotificationData();
                                    }catch(e){
                                        alert(e);
                                    }
                                }).fail(function() {alert("Error :012 problem in ContentWrapperMgr.js file ");}); 
                                model.notifyLoadFinish();
			},
                        loadFooterVgr : function(){
                            model.notifyLoadBegin();
                                $.getScript("view/FooterVgr.js").done(function() {
                                    try {
                                    config.setView("FooterVgr",new $.FooterVgr());
                                    vlist.loadNotification();
                                        
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error :013 problem in FooterVgr.js file ");}); 
			},
			loadFooter : function(){  
				$.getScript("model/Notification.js").done(function() {
                                    try{config.setModel("Notification",new $.Notification());
                                    vlist.loadNotificationMgr();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},          
                        loadHomeVgr: function(){  
				$.getScript("../view/HomeVgr.js").done(function() {
                                    try{
                                        config.setView("HomeVgr",new $.HomeVgr(config));
                                        vlist.loadCategoryVgr();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadCategoryVgr: function(){  
				$.getScript("../view/CategoryVgr.js").done(function() {
                                    try{
                                        config.setView("CategoryVgr",new $.CategoryVgr(config));
                                        vlist.loadCategory();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadCategory: function(){  
				$.getScript("../model/Category.js").done(function() {
                                    try{
                                        config.setModel("Category",new $.Category(config));
                                        vlist.loadCategoryMgr();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadCategoryMgr: function(){  
				$.getScript("../controller/CategoryMgr.js").done(function() {
                                    try{
                                        config.setController("CategoryMgr",new $.CategoryMgr(config.getModel("Category"),config.getView("CategoryVgr")));
                                        vlist.loadModelVgr();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadSubCategoryVgr: function(val){  
                            
				$.getScript("../view/SubCategoryVgr.js").done(function() {
                                    try{
                                        config.setView("SubCategoryVgr",new $.SubCategoryVgr(config));
                                        vlist.loadSubCategory(val);
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadSubCategory: function(val){  
				$.getScript("../model/SubCategory.js").done(function() {
                                    try{
                                        config.setModel("SubCategory",new $.SubCategory(config));
                                        vlist.loadSubCategoryMgr(val);
                                }catch(e){
                                    alert(e);
                                }   
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadSubCategoryMgr: function(val){  
				$.getScript("../controller/SubCategoryMgr.js").done(function() {
                                    try{
                                        config.setController("SubCategoryMgr",new $.SubCategoryMgr(config,config.getModel("SubCategory"),config.getView("SubCategoryVgr")));
                                        if(val==undefined){
                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("SubCategoryVgr").getSubCategory());
                                        }else{
                                            vlist.loadLanguageVgr(val);
                                        }
                                        
                                    }catch(e){
                                        alert(e);
                                    }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadLanguageVgr: function(val){  
                            
				$.getScript("../view/LanguageVgr.js").done(function() {
                                    try{
                                        config.setView("LanguageVgr",new $.LanguageVgr(config));
                                        vlist.loadLanguage(val);
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002 Vgr");}); 
			},         
                        loadLanguage: function(val){  
				$.getScript("../model/Language.js").done(function() {
                                    try{
                                        config.setModel("Language",new $.Language(config));
                                        vlist.loadLanguageMgr(val);
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002 js");}); 
			},         
                        loadLanguageMgr: function(val){  
				$.getScript("../controller/LanguageMgr.js").done(function() {
                                    try{
                                        config.setController("LanguageMgr",new $.LanguageMgr(config,config.getModel("Language"),config.getView("LanguageVgr")));
                                        if(val==undefined){
                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("LanguageVgr").getLanguage());
                                        }else{
                                            vlist.loadExamPhaseVgr(val)
                                        }
                                        
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002 Mgr");}); 
			},         
                        loadExamPhaseVgr: function(val){  
				$.getScript("../view/ExamPhaseVgr.js").done(function() {
                                    try{
                                        config.setView("ExamPhaseVgr",new $.ExamPhaseVgr(config));
                                        vlist.loadExamPhase(val);
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadExamPhase: function(val){  
				$.getScript("../model/ExamPhase.js").done(function() {
                                    try{
                                        config.setModel("ExamPhase",new $.ExamPhase(config));
                                        vlist.loadExamPhaseMgr(val);
                                    }catch(e){
                                        alert(e);
                                    }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadExamPhaseMgr: function(val){  
				$.getScript("../controller/ExamPhaseMgr.js").done(function() {
                                    try{
                                        config.setController("ExamPhaseMgr",new $.ExamPhaseMgr(config.getModel("ExamPhase"),config.getView("ExamPhaseVgr")));
                                        if(val==undefined){
                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("ExamPhaseVgr").getExamPhase());
                                        }else{
                                            vlist.loadSectionVgr(val)
                                        }
                                        
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadSectionVgr: function(val){  
				$.getScript("../view/SectionVgr.js").done(function() {
                                    try{
                                        config.setView("SectionVgr",new $.SectionVgr(config));
                                        vlist.loadSection(val);
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadSection: function(val){  
				$.getScript("../model/Section.js").done(function() {
                                    try{
                                        config.setModel("Section",new $.Section(config));
                                        vlist.loadSectionMgr(val);
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadSectionMgr: function(val){  
				$.getScript("../controller/SectionMgr.js").done(function() {
                                    try{
                                        config.setController("SectionMgr",new $.SectionMgr(config,config.getModel("Section"),config.getView("SectionVgr")));
                                        if(val==undefined){
                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("SectionVgr").getSection());
                                        }else{
                                            vlist.loadTopicVgr(val)
                                        }
                                        
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadTopicVgr: function(val){  
				$.getScript("../view/TopicVgr.js").done(function() {
                                    try{
                                        config.setView("TopicVgr",new $.TopicVgr(config));
                                        vlist.loadTopic(val);
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadTopic: function(val){  
				$.getScript("../model/Topic.js").done(function() {
                                    try{
                                        config.setModel("Topic",new $.Topic(config));
                                        vlist.loadTopicMgr(val);
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadTopicMgr: function(val){  
				$.getScript("../controller/TopicMgr.js").done(function() {
                                    try{
                                        config.setController("TopicMgr",new $.TopicMgr(config,config.getModel("Topic"),config.getView("TopicVgr")));
                                        if(val==undefined){
                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("TopicVgr").getTopic());
                                        }else{
                                           if($("body").hasClass("control-sidebar-open")){
                                                  $("body").removeClass("control-sidebar-open") ;
                                             } 
                                            config.getModel("Language").selLanguage(null,val);
                                            config.getModel("ExamPhase").selExamPhase(val);
                                        }
//                                        
                                }catch(e){
                                    alert("yahi "+e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadDirectionVgr: function(){  
				$.getScript("../view/DirectionVgr.js").done(function() {
                                    try{
                                        config.setView("DirectionVgr",new $.DirectionVgr(config));
                                        vlist.loadDirection();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadDirection: function(){  
				$.getScript("../model/Direction.js").done(function() {
                                    try{
                                        config.setModel("Direction",new $.Direction(config));
                                        vlist.loadDirectionMgr();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadDirectionMgr: function(){  
				$.getScript("../controller/DirectionMgr.js").done(function() {
                                    try{
                                        config.setController("DirectionMgr",new $.DirectionMgr(config,config.getModel("Direction"),config.getView("DirectionVgr")));
                                        config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                        config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("DirectionVgr").getDirection());
                                }catch(e){
                                    alert("yahi "+e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadQuestionVgr: function(kk,a){  
				$.getScript("../view/QuestionVgr.js").done(function() {
                                    try{
                                        if(a==null){
                                            config.setView("QuestionVgr",new $.QuestionVgr(config));
                                            config.getView("DirectionVgr").setInsForm($("#hidden"),kk)
                                        }else{
                                            config.setView("QuestionVgr",new $.QuestionVgr(config));
                                            config.getView("OnlyQuestionVgr").setInsForm($("#hidden"),kk)
                                        }
                                    }catch(e){
                                        alert(e);
                                    }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadOnlyQuestionVgr: function(kk){  
				$.getScript("../view/OnlyQuestionVgr.js").done(function() {
                                    try{
                                        config.setView("OnlyQuestionVgr",new $.OnlyQuestionVgr(config));
                                        vlist.loadOnlyQuestion();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadOnlyQuestion: function(){  
				$.getScript("../model/OnlyQuestion.js").done(function() {
                                    try{
                                        config.setModel("OnlyQuestion",new $.OnlyQuestion(config));
                                        vlist.loadOnlyQuestionMgr();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadOnlyQuestionMgr: function(){  
				$.getScript("../controller/OnlyQuestionMgr.js").done(function() {
                                    try{
                                    config.setView("OnlyQuestionMgr",new $.OnlyQuestionMgr(config,config.getModel("OnlyQuestion"),config.getView("OnlyQuestionVgr")));
                                    config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("OnlyQuestionVgr").getOnlyQuestionVgr());
                                }catch(e){
                                    alert("yahi hai call"+e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadQuestion: function(){  
				$.getScript("../model/Question.js").done(function() {
                                    try{
                                        config.setModel("Question",new $.Question(config));
                                        vlist.loadQuestionMgr();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadQuestionMgr: function(){  
				$.getScript("../controller/QuestionMgr.js").done(function() {
                                    try{
                                    config.setView("QuestionMgr",new $.QuestionMgr(config,config.getModel("Question"),config.getView("ViewQuestionVgr")));
                                    config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("ViewQuestionVgr").getViewQuestionVgr());
                                }catch(e){
                                    alert("yahi hai call"+e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},         
                        loadViewQuestionVgr: function(kk){  
                            $.getScript("../view/ViewQuestionVgr.js").done(function() {
                                try{
                                    config.setView("ViewQuestionVgr",new $.ViewQuestionVgr(config));
                                    vlist.loadQuestion();
                                }catch(e){
                                    alert(e);
                                }
                            }).fail(function() {alert("Error:View Question vgr js loading problem");}); 
                        },         
                        loadViewTestVgr: function(kk){  
                            $.getScript("../view/ViewTestVgr.js").done(function() {
                                try{
                                    config.setView("ViewTestVgr",new $.ViewTestVgr(config));
                                    vlist.loadViewTest();
                                }catch(e){
                                    alert(e+" in ViewTestVgr");
                                }
                            }).fail(function() {alert("Error:ViewTest.js loading problem");}); 
                        },         
                        loadViewTest: function(){  
				$.getScript("../model/ViewTest.js").done(function() {
                                    try{
                                        config.setModel("ViewTest",new $.ViewTest(config));
                                        vlist.loadViewTestMgr();
                                }catch(e){
                                    alert(e+" in ViewTest");
                                }
                                }).fail(function() {alert("Error:ViewTest.js loading problem");}); 
			},         
                        loadViewTestMgr: function(){  
				$.getScript("../controller/ViewTestMgr.js").done(function() {
                                    try{
                                    config.setView("ViewTestMgr",new $.ViewTestMgr(config,config.getModel("ViewTest"),config.getView("ViewTestVgr")));
                                    config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("ViewTestVgr").getViewTestVgr());
                                }catch(e){
                                    alert(e+" in ViewTestMgr");
                                }
                                }).fail(function() {alert("Error:ViewTestMgr.js loading problem");}); 
			},         
                        loadUserDetailsVgr: function(kk){  
                            $.getScript("../view/UserDetailsVgr.js").done(function() {
                                try{
                                    config.setView("UserDetailsVgr",new $.UserDetailsVgr(config));
                                    vlist.loadUserDetails();
                                }catch(e){
                                    alert(e+" in UserDetailsVgr");
                                }
                            }).fail(function() {alert("Error:UserDetails.js loading problem");}); 
                        },         
                        loadUserDetails: function(){  
				$.getScript("../model/UserDetails.js").done(function() {
                                    try{
                                        config.setModel("UserDetails",new $.UserDetails(config));
                                        vlist.loadUserDetailsMgr();
                                }catch(e){
                                    alert(e+" in UserDetails");
                                }
                                }).fail(function() {alert("Error:UserDetails.js loading problem");}); 
			},         
                        loadUserDetailsMgr: function(){  
				$.getScript("../controller/UserDetailsMgr.js").done(function() {
                                    try{
                                    config.setView("UserDetailsMgr",new $.UserDetailsMgr(config,config.getModel("UserDetails"),config.getView("UserDetailsVgr")));
                                    config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("UserDetailsVgr").getUserDetailsVgr());
                                }catch(e){
                                    alert(e+" in UserDetailsMgr");
                                }
                                }).fail(function() {alert("Error:UserDetailsMgr.js loading problem");}); 
			},         
                        loadFooterMgr : function(){
				$.getScript("../controller/CategoryMgr.js").done(function() {
                                    try {
                                        config.setController("CategoryMgr",new $.NotificationMgr(config.getModel("Category"),config.getView("CategoryVgr")));
                                        vlist.loadModelMgr();
                                    }catch(e){
                                        alert(e);
                                    }
                                }).fail(function() {alert("Error:003");}); 
                                model.notifyLoadFinish();
			},
                        setBodyData : function(){
                            config.getView("MainVgr").setBody();
                            vlist.loadOtherJs();
			},
                        loadModelVgr : function(){
                            $.getScript("../view/ModelVgr.js").done(function() {
                                    try{
                                        config.setView("ModelVgr",new $.ModelVgr(config));
                                        vlist.loadErrorMsgVgr();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},
                        loadErrorMsgVgr : function(){
                            $.getScript("../view/ErrorMsgVgr.js").done(function() {
                                    try{
                                        config.setView("ErrorMsgVgr",new $.ErrorMsgVgr(config));
                                        vlist.setBodyData();
                                }catch(e){
                                    alert(e);
                                }
                                }).fail(function() {alert("Error:002");}); 
			},
                        loadOtherJs : function(){
                                $.getScript("../../dist/js/jquery-ui.min.js");  
				$.getScript("../../bootstrap/js/bootstrap.min.js"); 
                                $.getScript("../../plugins/slimScroll/jquery.slimscroll.min.js"); 
                                $.getScript("./../plugins/fastclick/fastclick.min.js"); 
                                $.getScript("../../dist/js/app.js"); 
                                $.getScript("../../dist/js/jquery.validate.min.js"); 
                                $.getScript("../model/Resize.js"); 
                                $.getScript("../model/UploadImg.js"); 
                                
			}
                        
		});
		config.getView("MainVgr").addListener(vlist);

            var mlist = $.MainListener({
			loadBegin : function() {
				view.setLodingJs($("#data_box"));
			},
			loadFail : function() {
                            
			},
			loadFinish : function() {
				view.finishLoadinJs($("#data_box"));
			},
			loadItem : function(item){
				view.message("from ajax: " + item.name);
			}
		});
		config.getModel("Main").addListener(mlist);
	}
});
