jQuery.extend({
	SectionMgr: function(config,model, view){
		var vlist = $.SectionVgrListener({
			insSection : function(form){
				model.insSection(form);
			},
			selSection : function(category_id,subcategory_id){
                            var all = model.selSection(category_id,subcategory_id);
                            if(all){
                                $.each(all, function(item){
                                    view.addSection(all[item]);
                                });
                            }
			},
                        delSection : function(id,category_id,subcategory_id){
                                model.delSection(id,category_id,subcategory_id);
			},
                        updSection : function(form){
                                model.updSection(form);
			},
                        getSection : function(id,category_id,subcategory_id){
                                return model.getSection(id,category_id,subcategory_id);
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.SectionListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
                        
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(item,module){
                            switch(module){
                                case 1:
                                    config.getView("TopicVgr").secAddInTopic(item);
                                    break;
                                case 2:
                                    config.getView("TopicVgr").secAddInTopicModel(item);
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").secAddInDirection(item);
                                    break;
                                case 4:
                                    config.getView("OnlyQuestionVgr").secAddInOnlyQuestion(item);
                                    break;
                                case 5:
                                    config.getView("ViewQuestionVgr").secAddInViewQuestion(item);
                                    break;
                                default:
                                    view.addSection(item);
                                    break;
                            }
			},
                        updLoadItem : function(item){
				view.updSection(item);
			},
                        
                        selLoadBegin:function (module){
                            switch(module){
                                case 1:
                                    config.getView("TopicVgr").topicSecLoadBegin();
                                    break;    
                                case 2:
                                    config.getView("TopicVgr").topicModelSecLoadBegin();
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").directionSecLoadBegin();
                                    break;    
                                case 4:
                                    config.getView("OnlyQuestionVgr").onlyQuestionSecLoadBegin();
                                    break;    
                                case 5:
                                    config.getView("ViewQuestionVgr").viewQuestionSecLoadBegin();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                        },
                        
                        selLoadFail :function (module){
                            switch(module){
                                 case 1:
                                    config.getView("SectionVgr").topicSecLoadFail();
                                    break;
                                 case 2:
                                    config.getView("SectionVgr").topicModelSecLoadFail();
                                    break;
                                 case 3:
                                    config.getView("DirectionVgr").directionSecLoadFail();
                                    break;
                                 case 4:
                                    config.getView("OnlyQuestionVgr").onlyQuestionSecLoadFail();
                                    break;
                                 case 5:
                                    config.getView("ViewQuestionVgr").viewQuestionSecLoadFail();
                                    break;
                                 default:
                                    view.selLoadFail();
                                    break;
                            }
                        },
                        
                        selLoadFinish :function (module){
                            switch(module){
                                case 1:
                                    config.getView("TopicVgr").topicSecLoadFinish();
                                    break;
                                case 2:
                                    config.getView("TopicVgr").topicModelSecLoadFinish();
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").directionSecLoadFinish();
                                    break;
                                case 4:
                                    config.getView("OnlyQuestionVgr").onlyQuestionSecLoadFinish();
                                    break;
                                case 5:
                                    config.getView("ViewQuestionVgr").viewQuestionSecLoadFinish();
                                    break;
                                default:
                                    view.selLoadFinish();
                                    break;
                            }
                            
                        },
                        
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
