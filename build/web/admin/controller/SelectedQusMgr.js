jQuery.extend({
	SelectedQusMgr: function(config,model, view){
		var vlist = $.SelectedQusVgrListener({
			selSelectedQus : function(language_id,question_id){
                            var all = model.selSelectedQus(language_id,question_id);
                            if(all){
                                $.each(all, function(item){
                                    view.setQuestion(all[item]["test_id"],all[item]);
                                });
                            }
			}
		});
		view.addListener(vlist);

            var mlist = $.SelectedQusListener({
			loadItem : function(value){
                                view.setQuestion(value);
			},
                        selLoadBegin:function (module){
                                view.selLoadBegin();
                        },
                        selLoadFail :function (module){
                                view.selLoadFail();
                        },
                        selLoadFinish :function (module){
                                view.selLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
