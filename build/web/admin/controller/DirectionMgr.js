jQuery.extend({
	DirectionMgr: function(config,model, view){
		var vlist = $.DirectionVgrListener({
			insDirection : function(form){
				model.insDirection(form);
			},
			selDirection : function(category_id,subcategory_id,section_id){
                            var all = model.selDirection(category_id,subcategory_id,section_id);
                            if(all){
                                $.each(all, function(item){
                                    view.addDirection(all[item]);
                                });
                            }
			},
                        delDirection : function(category_id,subcategory_id,section_id,topic_id){
                                model.delDirection(category_id,subcategory_id,section_id,topic_id);
			},
                        updDirection : function(form){
                                model.updDirection(form);
			},
                        getDirection : function(category_id,subcategory_id,section_id,topic_id){
                                return model.getDirection(category_id,subcategory_id,section_id,topic_id);
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.DirectionListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
                        
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(item,module){
                            switch(module){
                                case 1:
                                    config.getView("DirectionVgr").subAddInSction(item);
                                    break;
                                case 2:
                                    config.getView("DirectionVgr").subAddInSctionModel(item);
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").subAddInDirection(item);
                                    break;
                                case 4:
                                    config.getView("DirectionVgr").subAddInDirectionModel(item);
                                    break;    
                                default:
                                    view.addDirection(item);
                                    break;
                            }
			},
                        updLoadItem : function(item){
				view.updDirectionRow(item);
			},
                        selLoadBegin:function (module){
                            switch(module){
                                case 1:
                                    config.getView("DirectionVgr").sectionSubLoadBegin();
                                    break;
                                case 2:
                                    config.getView("DirectionVgr").sectionModelSubLoadBegin();
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").topicSubLoadBegin();
                                    break;    
                                case 4:
                                    config.getView("DirectionVgr").topicModelSubLoadBegin();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                        },
                        selLoadFail :function (module){
                            switch(module){
                                 case 1:
                                    config.getView("DirectionVgr").sectionSubLoadFail();
                                    break;
                                 case 2:
                                    config.getView("DirectionVgr").sectionModelSubLoadFail();
                                    break;
                                 case 3:
                                    config.getView("DirectionVgr").topicSubLoadFail();
                                    break;
                                 case 4:
                                    config.getView("DirectionVgr").topicModelSubLoadFail();
                                    break;    
                                 default:
                                    view.selLoadFail();
                                    break;
                            }
                        },
                        selLoadFinish :function (module){
                            switch(module){
                                case 1:
                                    config.getView("DirectionVgr").sectionSubLoadFinish();
                                    break;
                                case 2:
                                    config.getView("DirectionVgr").sectionModelSubLoadFinish();
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").topicSubLoadFinish();
                                    break;
                                case 4:
                                    config.getView("DirectionVgr").topicModelSubLoadFinish();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                            view.selLoadFinish();
                        },
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
