jQuery.extend({
	LanguageMgr: function(config,model, view){
		var vlist = $.LanguageVgrListener({
			insLanguage : function(form){
				model.insLanguage(form);
			},
			selLanguage : function(){
                            var all = model.selLanguage();
                            if(all){
                                $.each(all, function(item){
                                    view.addLanguage(all[item]);
                                });
                            }
			},
                        delLanguage : function(id){
                                model.delLanguage(id);
			},
                        updLanguage : function(form){
                                model.updLanguage(form);
			},
                        getLanguage : function(id){
                                return model.getLanguage(id);
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.LanguageListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(item){
                                
				view.addLanguage(item);
			},
                        updLoadItem : function(item){
				view.updLanguage(item);
			},
                        selLoadBegin:function (module){
                            switch(module){
                                case 1:
                                    config.getView("DirectionVgr").directionLangLoadBegin();
                                    break;    
                                case 2:
                                    config.getView("OnlyQuestionVgr").onlyQuestionLangLoadBegin();
                                    break;
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                            
                        },
                        selLoadFail :function (module){
                            switch(module){
                                 case 1:
                                    config.getView("DirectionVgr").directionLangLoadFail();
                                    
                                    break;
                                 case 2:
                                    config.getView("OnlyQuestionVgr").onlyQuestionLangLoadFail();
                                    break;
                                 case 3:
                                    config.getView("DirectionVgr").directionSecLoadFail();
                                    break;
                                 default:
                                    view.selLoadFail();
                                    break;
                            }
                            
                        },
                        selLoadFinish :function (module){
                            switch(module){
                                case 1:
                                    config.getView("DirectionVgr").directionLangLoadFinish();
                                    break;
                                case 2:
                                    config.getView("OnlyQuestionVgr").onlyQuestionLangLoadFinish();
                                    break;
                                case 3:
                                    config.getView("DirectionVgr").directionSecLoadFinish();
                                    break;
                                default:
                                    view.selLoadFinish();
                                    break;
                            }
                            
                        },
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
