jQuery.extend({
	OnlyQuestionMgr: function(config,model, view){
		var vlist = $.OnlyQuestionVgrListener({
			insOnlyQuestion : function(form){
				model.insOnlyQuestion(form);
			}
		});
		view.addListener(vlist);

            var mlist = $.OnlyQuestionListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
                        
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			}

		});
		model.addListener(mlist);
	}
});
