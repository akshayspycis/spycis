/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.subcategory_detiails;

import com.class_manager.mocktest_details.MocktestFavSubcategoryMgr;
import com.class_manager.subcategory_details.SubCategoryDetailsMgr;
import com.data_manager.subcategory_details.SubCategoryDetails;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class SCDMgr {
        public JSONObject loadRecentSubCategory(){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    SubCategoryDetails cd = new SubCategoryDetailsMgr().loadRecentSubCategory();
                         JSONObject  j = new JSONObject();
                            j.put("subcategory_id",cd.getSubcategory_id());
                            j.put("subcategory_name",cd.getSubcategory_name());
                            ja.put(j);
                     js.put("subcategory_details",ja);   
                     js.put("category_id",cd.getCategory_id());   
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
    
    public JSONObject loadCategoryDetails(String category_id){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    ArrayList<SubCategoryDetails> cd = new SubCategoryDetailsMgr().loadSubCategory(category_id);
                    String subcategory_id;
                     for (SubCategoryDetails subcategory_details : cd) {
                         JSONObject  j = new JSONObject();
                         subcategory_id=subcategory_details.getSubcategory_id();
                            j.put("subcategory_id",subcategory_details.getSubcategory_id());
                            j.put("subcategory_name",subcategory_details.getSubcategory_name());
                         ja.put(j);
                     }
                     js.put("subcategory_details",ja);   
                     js.put("category_id",category_id);   
                     System.out.println(js);
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
    
    public JSONObject loadMocktestFavSubCategory(String software_id,String category_id){
        JSONObject js=null;
                try {
                    return new MocktestFavSubcategoryMgr().loadFavSubcategory(software_id, category_id);
                }catch(Exception e){
                    e.printStackTrace();
                }
        return js;
    }
}
