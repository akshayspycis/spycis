/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.create_test;

import com.class_manager.test_details.TestDetailsMgr;
import com.class_manager.test_question_details.TestQuestionDetailsMgr;
import com.class_manager.test_section.TestSectionMgr;
import com.class_manager.test_topic.TestTopicMgr;
import com.class_manager.test_user_details.TestUserDetailsMgr;
import com.data_manager.test_details.TestDetails;
import com.data_manager.test_question_details.TestQuestionDetails;
import com.data_manager.test_section.TestSection;
import com.data_manager.test_topic.TestTopic;
import com.data_manager.test_user_details.TestUserDetails;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class CTMgr {
    
public boolean parse(String json) throws JSONException{
            try {
    JSONObject jObject = new JSONObject(json);
            JSONObject general_details = jObject.getJSONObject("general_details");
            JSONObject question_details = jObject.getJSONObject("question_details");
            JSONObject section_details = jObject.getJSONObject("section_details");
                JSONArray section_order = jObject.getJSONArray("section_order");
            //---------------------set genral details inside the Test Details Object------------------------------
            TestDetails td = new TestDetails();
            td.setExam_phase_id(general_details.getString("exam_phase_id"));
            td.setCategory_id(general_details.getString("category_id"));
            td.setSubcategory_id(general_details.getString("subcategory_id"));
            td.setTime(general_details.getString("time"));
            td.setCut_off(general_details.getString("cut_off"));
            td.setE_date(general_details.getString("e_date"));
            td.setStart_time(general_details.getString("start_time"));
            td.setUnique_test_key(general_details.getString("unique_test_key"));
            td.setTest_name(general_details.getString("test_name"));
            td.setMeditory_test_id(general_details.getString("meditory_test_id"));
            td.setNegative_ratio(general_details.getString("negative_ratio"));
            td.setType(general_details.getString("type"));
                System.out.println(general_details.getString("type"));
            
            if(new TestDetailsMgr().insTestDetails(td)){
                Iterator<?> keys = question_details.keys();
                ArrayList<TestQuestionDetails> question_list=new ArrayList<TestQuestionDetails>();
                List<Integer> list=new ArrayList<Integer>();
                while(keys.hasNext()) {
                    String key = (String)keys.next();
                    list.add(Integer.parseInt(key));
                    JSONObject s = question_details.getJSONObject(key);
                    TestQuestionDetails t = new TestQuestionDetails();
                    t.setQuestion_id(s.getString("question_id"));
                    t.setMarks(s.getString("mark"));
                    t.setOrder(Integer.parseInt(key));
                    question_list.add(t);
                }
                Collections.sort(list); 
                if(new TestQuestionDetailsMgr().insTestQuestionDetails(question_list,list)){
                        Iterator<?> key = section_details.keys();
                        ArrayList<TestSection> section_list=new ArrayList<TestSection>();
                        ArrayList<TestTopic> topic_list=new ArrayList<TestTopic>();
                        while(key.hasNext()) {
                            String k = (String)key.next();
                            JSONObject s = section_details.getJSONObject(k);
                            TestSection t = new TestSection();
                            t.setSection_id(k);
                            Iterator<?> topic_key = s.keys();
                                while(topic_key.hasNext()) {
                                    String topic = (String)topic_key.next();
                                    TestTopic tt= new TestTopic();
                                    tt.setTopic_id(topic);
                                    topic_list.add(tt);
                                }
                            section_list.add(t);
                        }
                        if(new TestSectionMgr().insTestSection(section_list,section_order) && new TestTopicMgr().insTestTopic(topic_list)){
                            return true;
                        }else{
                            return false;
                        }
                }else{
                    return false;
                }
            }else{
                    return false;
            }
        
    } catch (Exception e) {
        e.printStackTrace();
                    return false;
    }
                    
}    

public JSONObject loadTestDetails(String category_id,String subcategory_id){
    JSONObject js=null;
                try {
                    js = new JSONObject();
                    ResultSet rs = new TestDetailsMgr().loadTestDetails(category_id, subcategory_id);
                    HashMap<String,HashMap> root = new HashMap<String,HashMap>();
                    HashMap<String,String> question_details=null;
                    while(rs.next()){
                        try {
                            question_details=new HashMap<String,String>();
                            question_details.put("exam_phase_id", rs.getString("exam_phase_id"));
                            question_details.put("time", rs.getString("time"));
                            question_details.put("cut_off", rs.getString("cut_off"));
                            question_details.put("e_date", rs.getString("e_date"));
                            question_details.put("start_time", rs.getString("start_time"));
                            question_details.put("unique_test_key", rs.getString("unique_test_key"));
                            question_details.put("test_name", rs.getString("test_name"));
                            question_details.put("meditory_test_id", rs.getString("meditory_test_id"));
                            question_details.put("visible", rs.getString("visible"));
                            root.put(rs.getString("test_id"), question_details);
                        } catch (Exception e) {
                        }
                        }
                js.put("test_details", new JSONObject(root));
                return js;
                }catch(Exception e)   {
                    e.printStackTrace();
                    return null;
                }
}

public JSONObject loadAssesmentDetails_Test(String user_id){
    JSONObject js=null;
                try {
                    js = new JSONObject();
                    ResultSet rs = new TestDetailsMgr().loadAssesmentDetails_Test(user_id);
                    HashMap<String,HashMap> root = new HashMap<String,HashMap>();
                    HashMap<String,String> question_details=null;
                    while(rs.next()){
                        try {
                            question_details=new HashMap<String,String>();
                            question_details.put("exam_phase_id", rs.getString("exam_phase_id"));
                            question_details.put("time", rs.getString("time"));
                            question_details.put("cut_off", rs.getString("cut_off"));
                            question_details.put("e_date", rs.getString("e_date"));
                            question_details.put("start_time", rs.getString("start_time"));
                            question_details.put("unique_test_key", rs.getString("unique_test_key"));
                            question_details.put("test_name", rs.getString("test_name"));
                            question_details.put("meditory_test_id", rs.getString("meditory_test_id"));
                            question_details.put("visible", rs.getString("visible"));
                            question_details.put("no_qus", rs.getString("no_qus"));
                            question_details.put("total_marks", rs.getString("total_marks"));
                            question_details.put("type", rs.getString("type"));
                            question_details.put("eligibility", rs.getString("eligibility"));
                            question_details.put("status", rs.getString("status"));
                            question_details.put("payment_status", rs.getString("payment_status"));
                            root.put(rs.getString("test_id"), question_details);
                        } catch (Exception e) {
                        }
                        }
                js.put("test_details", new JSONObject(root));
                return js;
                }catch(Exception e)   {
                    e.printStackTrace();
                    return null;
                }
}

public boolean insUserInTest(String json) throws JSONException{
            try {
            JSONObject jObject = new JSONObject(json);
            String test_id = jObject.getString("test_id");
            JSONObject test_user_details = jObject.getJSONObject("user_details");
                //---------------------set genral details inside the Test Details Object------------------------------
                Iterator<?> keys = test_user_details.keys();
                ArrayList<TestUserDetails> test_user_details_list=new ArrayList<TestUserDetails>();
                while(keys.hasNext()) {
                    String key = (String)keys.next();
                    if(test_user_details.getString(key).equals("true")){
                        TestUserDetails t = new TestUserDetails();
                        t.setUser_id(key);
                        t.setTest_id(test_id);
                        test_user_details_list.add(t);
                    }
                    
                    
                }
                if(new TestUserDetailsMgr().insTestUserDetails(test_user_details_list,test_id)){
                    return true;    
                }else{
                    return false;
                }
    } catch (Exception e) {
        e.printStackTrace();
                    return false;
    }
}    

}
