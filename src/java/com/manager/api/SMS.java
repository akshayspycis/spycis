/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.api;

import com.class_manager.software_details.SoftwareDetailsMgr;
import com.data_manager.config.GlobalData;
import com.data_manager.software_details.SoftwareDetails;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Random;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class SMS {
    public boolean send(String software_id,String contact_no) throws IOException{
        try {
            if(contact_no==null){
            JSONObject j=GlobalData.otp_software.get(software_id);
            j.put("otp",generatePassword());
            GlobalData.otp_software.put(software_id,j);
            System.out.println("hello"+j.get("otp"));
            String requestUrl="http://sms.bangsms.com/api/sendhttp.php?authkey=728ATWLpBnu57da37e3&mobiles="+j.get("contact_no")+"&message="+j.get("otp")+" is your Infopark otp code.&sender=swrnkr&route=4\"";
            URL url = new URL(requestUrl);
            HttpURLConnection uc = (HttpURLConnection)url.openConnection();
            uc.disconnect();
            }else{
                JSONObject jj= new JSONObject();
                jj.put("contact_no", contact_no);
                if(GlobalData.otp_software==null){
                    GlobalData.otp_software= new HashMap<String, JSONObject>();
                    GlobalData.otp_software.put(software_id, jj);
                }else{
                    if(!GlobalData.otp_software.containsKey(software_id)){
                        GlobalData.otp_software.put(software_id, jj);
                    }
                }
                JSONObject j=GlobalData.otp_software.get(software_id);
            j.put("otp",generatePassword());
            GlobalData.otp_software.put(software_id,j);
            String requestUrl="http://sms.bangsms.com/api/sendhttp.php?authkey=728ATWLpBnu57da37e3&mobiles="+j.get("contact_no")+"&message="+j.get("otp")+" is your Infopark otp code.&sender=swrnkr&route=4\"";
            System.out.println(requestUrl);
            URL url = new URL(requestUrl);
            HttpURLConnection uc = (HttpURLConnection)url.openConnection();
            System.out.println(uc.getResponseMessage());
            uc.disconnect();
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean parse(String json) throws IOException{
        try {
            JSONObject jObject = new JSONObject(json);
            String software_id=jObject.getString("software_id");
            String otp=jObject.getString("otp");
            System.out.println(otp);      
            JSONObject j=GlobalData.otp_software.get(software_id);
            System.out.println(j.get("otp"));
            if(j.get("otp").equals(otp)){
                SoftwareDetails s=new SoftwareDetails();
                s.setSoftware_id(software_id);
                s.setOtp_authentication("true");
                if(new SoftwareDetailsMgr().updSoftwareDetails(s, "otp_authentication")){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    public static String generatePassword() {
        String chars = "0123456789";
        final int PW_LENGTH = 6;
        Random rnd = new SecureRandom();
        StringBuilder pass = new StringBuilder();
        for (int i = 0; i < PW_LENGTH; i++)
            pass.append(chars.charAt(rnd.nextInt(chars.length())));
            return pass.toString();
    }
}
