/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.discription_bank;

import com.class_manager.discription_bank.DiscriptionBankMgr;
import com.class_manager.discription_details.DiscriptionDetailsMgr;
import com.class_manager.option_bank.OptionBankMgr;
import com.class_manager.question_bank.QuestionBankMgr;
import com.class_manager.question_details.QuestionDetailsMgr;
import com.class_manager.question_lang_details.QuestionLangDetailsMgr;
import com.data_manager.config.GlobalData;
import com.data_manager.discription_bank.DiscriptionBank;
import com.data_manager.discription_details.DiscriptionDetails;
import com.data_manager.option_bank.OptionBank;
import com.data_manager.question_bank.QuestionBank;
import com.data_manager.question_details.QuestionDetails;
import com.data_manager.question_lang_details.QuestionLangDetails;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class DBankMgr {
    public String addDiscription(Map<Integer, DiscriptionBank> question,Map<Integer,Long> unique,String category_id,String subcategory_id,String section_id,String topic_id) {
        try {
            String discription_id=String.valueOf(new DiscriptionDetailsMgr().loadMaxId()+1);
            for (Integer language_id : question.keySet()) {
                    if(new DiscriptionBankMgr().insDiscriptionBank(question.get(language_id))){
                        for (Object question_id : question.get(language_id).getQuestion_list().keySet()) {
                            if(new QuestionBankMgr().insQuestionBank(question.get(language_id).getQuestion_list().get(question_id))){
                                for (int i = 0; i < question.get(language_id).getQuestion_list().get(question_id).getOptionlist().size(); i++) {
                                    new OptionBankMgr().insOptionBank(question.get(language_id).getQuestion_list().get(question_id).getOptionlist().get(i));
                                }
                                QuestionLangDetails q= new QuestionLangDetails();
                                q.setLanguage_id(String.valueOf(language_id));
                                q.setQuestion_id(String.valueOf(question_id));
                                new QuestionLangDetailsMgr().insQuestionLangDetails(q);
                            }
                        }
                    }
                    DiscriptionDetails dd= new DiscriptionDetails();
                    dd.setDiscription_id(discription_id);
                    dd.setLanguage_id(String.valueOf(language_id));
                    new DiscriptionDetailsMgr().insDiscriptionDetails(dd);
            }
            for (Object question_id : unique.keySet()) {
                QuestionDetails dd= new QuestionDetails();
                dd.setDiscription_id(discription_id);
                    dd.setQuestion_id(String.valueOf(unique.get(question_id)));
                    dd.setCategory_id(category_id);
                    dd.setSubcategory_id(subcategory_id);
                    dd.setSection_id(section_id);
                    dd.setTopic_id(topic_id);
                    new QuestionDetailsMgr().insQuestionDetails(dd);
            }
            
            
        } catch (Exception e) {
            e.printStackTrace(GlobalData.ps);
                GlobalData.ps.close();
            return null;
        }
        return null;
    }
    
    
 public boolean parse(String json) {
     try {
            JSONObject jObject = new JSONObject(json);
            
            JSONObject info = jObject.getJSONObject("language_details");
            String category_id=jObject.getString("category_id");
            String subcategory_id=jObject.getString("subcategory_id");
            String section_id=jObject.getString("section_id");
            String topic_id=jObject.getString("topic_id");
            
            Iterator<?> keys = info.keys();
            Map<Integer,Long> unique=null;
            unique=new HashMap<Integer,Long>();
            Map<Integer, DiscriptionBank> question = new HashMap<Integer, DiscriptionBank>();
            long sid=new QuestionDetailsMgr().loadMaxId()+1;
            while(keys.hasNext()) {
                String key = (String)keys.next();
                JSONObject s=info.getJSONObject(key);
                Iterator<?> k = s.keys();
                    while(k.hasNext()) {
                        String kkk=(String)k.next();
                        if(kkk.equals("question_details")){
                            if(question.get(Integer.parseInt(key))==null){
                                question.put(Integer.parseInt(key), new DiscriptionBank());
                            }
                            JSONObject ll=s.getJSONObject(kkk);
                            Iterator<?> ke = ll.keys();
                            Map<Long, QuestionBank> question_list = new HashMap<Long, QuestionBank>();
                            while(ke.hasNext()) {
                                String kl = (String)ke.next();
                                
                                long question_id;
                                        if(unique.get(Integer.parseInt(kl))!=null){
                                            question_id=unique.get(Integer.parseInt(kl));
                                        }else{
                                            question_id=sid+unique.size();
                                            unique.put(Integer.parseInt(kl), question_id);
                                        }
                                JSONObject kkll=ll.getJSONObject(kl);
                                Iterator<?> kaa = kkll.keys();
                                QuestionBank qb = new QuestionBank();
                                while(kaa.hasNext()) {
                                    String nm=(String)kaa.next();
                                    ArrayList<OptionBank> option = new ArrayList<OptionBank>();
                                    if(nm.equals("option_details")){
                                        JSONObject lll=kkll.getJSONObject(nm);
                                        Iterator<?> kee = lll.keys();
                                        while(kee.hasNext()) {
                                            String ml=(String)kee.next();
                                            if(ml.equals("answer")){
                                                qb.setAnswer(lll.getString(ml));
                                            }else{
                                                OptionBank op=new OptionBank();
                                                op.setOption(lll.getString(ml));
                                                op.setOption_name(ml);
                                                option.add(op );
                                            }
                                        }
                                        qb.setOptionlist(option);
                                    }
                                    if(nm.equals("qus")){
                                        qb.setQuestion(kkll.getString(nm));
                                        question_list.put(question_id, qb);
                                        question.get(Integer.parseInt(key)).setQuestion_list(question_list);
                                    }
                                }
                            }
                        }
                        if(kkk.equals("direction_details")){
                            if(question.get(Integer.parseInt(key))==null){
                                question.put(Integer.parseInt(key), new DiscriptionBank());
                                
                            }
                            question.get(Integer.parseInt(key)).setDiscription(s.getString(kkk));
                        }
                    }
            }
                    this.addDiscription(question, unique,category_id,subcategory_id,section_id,topic_id);
                        return true;
        } catch (Exception e) {
                e.printStackTrace(GlobalData.ps);
                GlobalData.ps.close();
            return false;
        }
}
 
public JSONObject loadQuestion(String category_id,String subcategory_id,String section_id,String topic_id,String language_id){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    ResultSet rs = new QuestionDetailsMgr().loadQuestion(category_id, subcategory_id, section_id, topic_id, language_id);
                    HashMap<String,HashMap> root = new HashMap<String,HashMap>();
                    HashMap<String,String> question_details=null;
                    while(rs.next()){
                        try {
                            question_details=new HashMap<String,String>();
                            question_details.put("discription_id", rs.getString("discription_id"));
                            question_details.put("question_bank_id", rs.getString("question_bank_id"));
                            question_details.put("question", rs.getString("question"));
                            question_details.put("discription_bank_id", rs.getString("discription_bank_id"));
                            root.put(rs.getString("question_id"), question_details);
                        } catch (Exception e) {
                        }
                            
                            
                        }
                js.put("q", new JSONObject(root));
                return js;
                }catch(Exception e)   {
                    e.printStackTrace();
                    return null;
                }
}

}
