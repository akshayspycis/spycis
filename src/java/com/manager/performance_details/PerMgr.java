/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.performance_details;

import com.class_manager.test_details.TestDetailsMgr;
import java.sql.ResultSet;
import java.util.HashMap;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class PerMgr {
    public JSONObject loadUserPerformance(String user_id){
    JSONObject js=null;
                try {
                    js = new JSONObject();
                    ResultSet rs = new TestDetailsMgr().loadUserPerformance(user_id);
                    HashMap<String,HashMap> root = new HashMap<String,HashMap>();
                    HashMap<String,String> question_details=null;
                    while(rs.next()){
                        try {
                            question_details=new HashMap<String,String>();
                            question_details.put("unique_test_key", rs.getString("unique_test_key"));
                            question_details.put("test_name", rs.getString("test_name"));
                            question_details.put("total", rs.getString("total"));
                            question_details.put("type", rs.getString("type"));
                            root.put(rs.getString("test_id"), question_details);
                        } catch (Exception e) {
                        }
                        }
                js.put("test_details", new JSONObject(root));
                return js;
                }catch(Exception e)   {
                    e.printStackTrace();
                    return null;
                }
}
}
