/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.software_details;

import com.class_manager.mocktest_details.MocktestFavCategoryMgr;
import com.class_manager.mocktest_details.MocktestFavSectionMgr;
import com.class_manager.mocktest_details.MocktestFavSubcategoryMgr;
import com.data_manager.mocktest_details.MocktestFavCategory;
import com.data_manager.mocktest_details.MocktestFavSection;
import com.data_manager.mocktest_details.MocktestFavSubcategory;
import java.util.ArrayList;
import java.util.Iterator;
import static org.apache.coyote.http11.Constants.a;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class BasicMgr {
    public String setBasic(String json) throws JSONException{
        System.out.println(json);
        JSONObject jObject = new JSONObject(json);
            String     software_id = jObject.getString("software_id");
            JSONObject category    =jObject.getJSONObject("category");
            JSONObject subcategory =jObject.getJSONObject("subcategory");
            JSONObject section     =jObject.getJSONObject("section");
            Iterator<?> keys = category.keys();
            ArrayList<MocktestFavCategory> mocktest_fav_category =new ArrayList<MocktestFavCategory>();
            while(keys.hasNext()) {
                MocktestFavCategory m=new MocktestFavCategory();
                m.setCategory_id((String)keys.next());
                m.setSoftware_id(software_id);
                mocktest_fav_category.add(m);
            }
            if(new MocktestFavCategoryMgr().insMocktestFavCategory(mocktest_fav_category)){
                keys = subcategory.keys();
                ArrayList<MocktestFavSubcategory> mocktest_fav_subcategory =new ArrayList<MocktestFavSubcategory>();
                while(keys.hasNext()) {
                    MocktestFavSubcategory m=new MocktestFavSubcategory();
                    m.setSubcategory_id((String)keys.next());
                    m.setSoftware_id(software_id);
                    mocktest_fav_subcategory.add(m);
                    
                }
                if(new MocktestFavSubcategoryMgr().insMocktestFavSubcategory(mocktest_fav_subcategory)){
                    keys = section.keys();
                    ArrayList<MocktestFavSection> mocktest_fav_section =new ArrayList<MocktestFavSection>();
                    while(keys.hasNext()) {
                       String kkk=(String)keys.next();
                        MocktestFavSection m=new MocktestFavSection();
                        m.setSection_id(kkk);
                        m.setSoftware_id(software_id);
                        mocktest_fav_section.add(m);
                    }
                    if(new MocktestFavSectionMgr().insMocktestFavSection(mocktest_fav_section)){
                        return "true";
                    }else{
                        return "error";
                    }
                }else{
                    return "error";
                }
            }else{
                return "error";
            }
    } 
    
}
