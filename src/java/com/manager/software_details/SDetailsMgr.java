/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.software_details;

import com.class_manager.software_details.SoftwareDetailsMgr;
import com.class_manager.user_details.UserRequestMgr;
import com.data_manager.config.GlobalData;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.tomcat.jni.Global;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class SDetailsMgr {
    
public String parse(String json) throws JSONException{
            JSONObject jObject = new JSONObject(json);
            String[]a=new String[5];
            a[0]=jObject.getString("user_id");
            a[1]=jObject.getString("software_name_id");
            a[2]=jObject.getString("orgenisation_name");
            a[3]=jObject.getString("contact_no");
            a[4]=jObject.getString("email");
            if(new SoftwareDetailsMgr().insSoftwareDetails(a)){
                String software_id= new SoftwareDetailsMgr().maxSoftwareId();
                JSONObject j= new JSONObject();
                j.put("contact_no", a[3]);
                if(GlobalData.otp_software==null){
                    GlobalData.otp_software= new HashMap<String, JSONObject>();
                    GlobalData.otp_software.put(software_id, j);
                }else{
                    if(!GlobalData.otp_software.containsKey(software_id)){
                        GlobalData.otp_software.put(software_id, j);
                    }
                }
                return software_id;
            }else{
                return "error";
            }
}    
    public JSONObject loadSoftwareDetails(String term) throws JSONException{
        return  new SoftwareDetailsMgr().loadSuggestionSoftwareDetails(term);
    }
    public String loadStatus(String user_id, String software_id) throws JSONException{
        return  new UserRequestMgr().loadStatus(user_id, software_id);
    }
    public JSONObject selSoftwareDetails(String term) throws JSONException{
        JSONObject retun_item = null;
        String temp_term = "";
        try {
            if(GlobalData.software_details_list==null){
                GlobalData.software_details_list = new HashMap<String, JSONObject>();
                JSONObject software_details_list = this.loadSoftwareDetails(term);
                if(software_details_list.length()>0){
                    Iterator<?> keys = software_details_list.keys();
                    while( keys.hasNext() ){
                        if(retun_item==null){retun_item=new JSONObject();}
                        String key = (String)keys.next();
                        GlobalData.software_details_list.put(key, software_details_list.getJSONObject(key));
                        retun_item.put(key, software_details_list.getJSONObject(key));
                    }
                }
            }else{
                for (String key: GlobalData.software_details_list.keySet()) {
                    if(((String) GlobalData.software_details_list.get(key).get("orgenisation_name")).toLowerCase().startsWith(term.toLowerCase())){
                        if(retun_item==null){retun_item=new JSONObject();}
                        retun_item.put(key,GlobalData.software_details_list.get(key));
                    }
                }
                if(retun_item==null){
                    JSONObject software_details_list = this.loadSoftwareDetails(term);
                    if(software_details_list.length()>0){
                        Iterator<?> keys = software_details_list.keys();
                        while( keys.hasNext() ){
                            if(retun_item==null){retun_item=new JSONObject();}
                            String key = (String)keys.next();
                            JSONObject value = software_details_list.getJSONObject(key); 
                            if(software_details_list.get(key)==null){
                                GlobalData.software_details_list.put(key, value);
                            }
                            retun_item.put(key, software_details_list.getJSONObject(key));
                        }
                    }
                }
            }
        } catch (Exception ex) {
                ex.printStackTrace();
        }
        return retun_item;
    }
    
 

    public String selStatusDetails(String user_id, String software_id) {
        String status="";
        try {
            if(GlobalData.software_user_request_list==null){
                GlobalData.software_user_request_list = new HashMap<String, HashMap>();
                status = this.loadStatus(user_id,software_id);
                if(!status.equals("")){
                    HashMap<String,String> child= new HashMap<String,String>();
                    child.put(software_id, status);
                    GlobalData.software_user_request_list.put(user_id, child);
                }
            }else{
                status=(String) (GlobalData.software_user_request_list.get(user_id)!=null ? GlobalData.software_user_request_list.get(user_id).get(software_id) :null);
                if(status==null){
                    status = this.loadStatus(user_id,software_id);
                    if(GlobalData.software_user_request_list.get(user_id)==null){
                        HashMap<String,String> child= new HashMap<String,String>();
                        child.put(software_id, status);
                        GlobalData.software_user_request_list.put(user_id, child);
                    }else{
                        HashMap<String,String> child= GlobalData.software_user_request_list.get(user_id);  
                        child.put(software_id, status);
                        GlobalData.software_user_request_list.put(user_id, child);
                    }
                }
            }
        } catch (Exception ex) {
                ex.printStackTrace();
        }
        return status;
    }
}
