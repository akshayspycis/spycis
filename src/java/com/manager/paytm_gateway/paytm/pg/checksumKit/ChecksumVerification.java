package com.manager.paytm_gateway.paytm.pg.checksumKit;



import com.data_manager.config.GlobalData;
import static com.data_manager.config.GlobalData.MercahntKey;
import com.paytm.pg.merchant.CheckSumServiceHelper;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ChecksumVerification {

    public boolean checksumVerification(TreeMap<String, String> paytmParams,String paytmChecksum) {
        try {
            return CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(GlobalData.MercahntKey, paytmParams, paytmChecksum);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
