package com.manager.paytm_gateway.paytm.pg.checksumKit;

import com.data_manager.config.GlobalData;
import com.paytm.pg.merchant.CheckSumServiceHelper;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.TreeMap;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

public class ChecksumGeneration {
    public String createChecksum(TreeMap<String,String> paramMap) {
        try {
            String checkSum = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(GlobalData.MercahntKey, paramMap.toString());
            return checkSum;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
