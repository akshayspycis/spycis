/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.language_details;

import com.class_manager.language_details.LanguageDetailsMgr;
import com.data_manager.language_details.LanguageDetails;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class LDMgr {
    public JSONObject loadRecentLanguage(){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    LanguageDetails cd = new LanguageDetailsMgr().loadRecentLanguage();
                         JSONObject  j = new JSONObject();
                            j.put("language_id",cd.getLanguage_id());
                            j.put("language_name",cd.getLanguage_name());
                            ja.put(j);
                     js.put("language_details",ja);   
                     System.out.println(js);
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
    
    public JSONObject loadLanguageDetails(){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    ArrayList<LanguageDetails> cd = new LanguageDetailsMgr().loadLanguage();
                     for (LanguageDetails languagedetails : cd) {
                         JSONObject  j = new JSONObject();
                            j.put("language_id",languagedetails.getLanguage_id());
                            j.put("language_name",languagedetails.getLanguage_name());
                         ja.put(j);
                     }
                     js.put("language_details",ja);   
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
        
}
