/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.user_details;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Akshay
 */
public class SetSessionCookie {
    HttpSession session;
    public HttpSession setSession(HttpServletRequest request){
        session = request.getSession(true);
        return session;
    }
    public Cookie getCookie(String cookie_name,String cookie_value,int age){
        Cookie ck=new Cookie(cookie_name,cookie_value);
        ck.setMaxAge(age);
        return ck;
    }
}
