/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.user_details;

import com.class_manager.test_result.TestResultMgr;
import com.class_manager.test_user_details.TestUserDetailsMgr;
import com.class_manager.user_details.UserDetailsMgr;
import com.data_manager.config.GlobalData;
import com.data_manager.test_details.TestDetails;
import com.data_manager.test_user_details.TestUserDetails;
import com.data_manager.user_details.User;
import com.data_manager.user_details.UserDetails;
import java.sql.ResultSet;
import java.util.ArrayList;
import junit.framework.TestResult;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class UserMgr {
    public JSONObject loadUserList(String test_id,String check){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    GlobalData.cd = new UserDetailsMgr().loadUserDetails();    
                    ArrayList<TestUserDetails> tud = new TestUserDetailsMgr().loadTestUserDetails(test_id);
                    for (int i = 0; i < GlobalData.cd.size(); i++) {
                        JSONObject  jo = new JSONObject();
                        if(check.equals("0")){
                            jo.put("user_name",GlobalData.cd.get(i).getUser_name());
                            jo.put("email",GlobalData.cd.get(i).getEmail());
                            jo.put("contact_no",GlobalData.cd.get(i).getContact_no());
                            jo.put("pic",GlobalData.cd.get(i).getPic());
                        }
                        if(tud.size()>0){
                            for (int j = 0; j < tud.size(); j++) {
                                 if(GlobalData.cd.get(i).getUser_id().equals(tud.get(j).getUser_id())){
                                    jo.put("test_status","true");    
                                 }
                            }
                            if(!jo.has("test_status")){
                                jo.put("test_status","false");    
                            }
                        }else{
                            jo.put("test_status","false");     
                        }
                         js.put(GlobalData.cd.get(i).getUser_id(),jo);   
                     }
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }    
    
    public JSONObject loadTestUserList(String test_id,String check){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    TestResultMgr trm=new TestResultMgr();
                        
                        ResultSet tud = trm.loadTestResultNumber(test_id);
                        while (tud.next()) {
                            JSONObject  jo = new JSONObject();
                            jo.put("user_name",tud.getString("user_name"));
                            jo.put("email",tud.getString("email"));
                            jo.put("contact_no",tud.getString("contact_no"));
                            jo.put("pic",tud.getString("pic"));
                            jo.put("total",tud.getString("total"));    
                            js.put(tud.getString("user_id"),jo);   
                     }
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }    
        public JSONObject loadAllUser(){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    GlobalData.cd = new UserDetailsMgr().loadUserDetails();
                    for (int i = 0; i < GlobalData.cd.size(); i++) {
                        JSONObject  jo = new JSONObject();
                            jo.put("user_name",GlobalData.cd.get(i).getUser_name());
                            jo.put("dob",GlobalData.cd.get(i).getDob());
                            jo.put("email",GlobalData.cd.get(i).getEmail());
                            jo.put("contact_no",GlobalData.cd.get(i).getContact_no());
                            jo.put("password",GlobalData.cd.get(i).getPassword());
                            jo.put("pic",GlobalData.cd.get(i).getPic());
                         js.put(GlobalData.cd.get(i).getUser_id(),jo);   
                     }
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }    

    public JSONObject loadUserDetails(String user_id) {
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    GlobalData.cd = new UserDetailsMgr().loadUserProfile(user_id);
                    for (int i = 0; i < GlobalData.cd.size(); i++) {
                        JSONObject  jo = new JSONObject();
                            jo.put("user_name",GlobalData.cd.get(i).getUser_name());
                            jo.put("dob",GlobalData.cd.get(i).getDob());
                            jo.put("email",GlobalData.cd.get(i).getEmail());
                            jo.put("contact_no",GlobalData.cd.get(i).getContact_no());
                            jo.put("password",GlobalData.cd.get(i).getPassword());
                            jo.put("pic",GlobalData.cd.get(i).getPic());
                         js.put(GlobalData.cd.get(i).getUser_id(),jo);   
                     }
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
}
