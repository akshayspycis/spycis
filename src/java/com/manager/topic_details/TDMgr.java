/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.topic_details;

import com.class_manager.topic_details.TopicDetailsMgr;
import com.data_manager.topic_details.TopicDetails;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class TDMgr {
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    
        public JSONObject loadRecentTopic(){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    TopicDetails cd = new TopicDetailsMgr().loadRecentTopic();
                         JSONObject  j = new JSONObject();
                            j.put("topic_id",cd.getTopic_id());
                            j.put("topic_name",cd.getTopic_name());
                            ja.put(j);
                     js.put("topic_details",ja);   
                     js.put("section_id",cd.getSection_id());   
                     js.put("category_id",cd.getCategory_id());   
                     js.put("subcategory_id",cd.getSubcategory_id());   
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
    
    public JSONObject loadTopicDetails(String category_id,String subcategory_id,String section_id){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    ArrayList<TopicDetails> cd = new TopicDetailsMgr().loadTopicDetails(category_id,subcategory_id,section_id);
                     for (TopicDetails subcategory_details : cd) {
                         JSONObject  j = new JSONObject();
                            j.put("topic_id",subcategory_details.getTopic_id());
                            j.put("topic_name",subcategory_details.getTopic_name());
                         ja.put(j);
                     }
                     js.put("topic_details",ja);   
                     js.put("section_id",section_id);   
                     js.put("category_id",category_id);   
                     js.put("subcategory_id",subcategory_id);   
                     System.out.println(js);
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
}
    