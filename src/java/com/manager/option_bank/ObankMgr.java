/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.option_bank;

import com.class_manager.option_bank.OptionBankMgr;
import com.class_manager.question_bank.QuestionBankMgr;
import com.data_manager.config.GlobalData;
import com.data_manager.option_bank.OptionBank;
import com.data_manager.question_bank.QuestionBank;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class ObankMgr {
    public boolean addOption(Map<String, String> map) {
        try {
            for (int i = 0; i < map.size(); i++) {
                Set<String> keySet= map.keySet();
                for (String string : keySet) {
                    OptionBank d = new OptionBank();
                    d.setOption(map.get(string));
                    d.setOption_name(string);
                    if(!new OptionBankMgr().insOptionBank(d)){
                        throw new Exception();
                    }
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean parse(String json) {
         try {
                JSONObject jObject = new JSONObject(json);
                Iterator<?> keys = jObject.keys();
                ArrayList<OptionBank> option = new ArrayList<OptionBank>();
                String question_bank_id="";
                String answer="";
                while(keys.hasNext()) {
                    String key = (String)keys.next();
                    String value=jObject.getString(key);
                    if(key.equals("question_bank_id")){
                        question_bank_id=value;
                    }else if(key.equals("answer")){
                        answer=value;
                    }else{
                        OptionBank ob=new OptionBank();
                        ob.setOption_name(key);
                        ob.setOption(value);
                        option.add(ob);
                    }
                }
                QuestionBank q= new QuestionBank();
                q.setAnswer(answer);
                q.setQuestion_bank_id(question_bank_id);
                if(new OptionBankMgr().updOptionBankMgr(option,question_bank_id)&& new QuestionBankMgr().updAnswerInQuestionBank(q)){
                    return true;
                }else{
                    return false;
                }
        } catch (Exception e) {
                e.printStackTrace(GlobalData.ps);
                GlobalData.ps.close();
            return false;
        }
    }
    
    public JSONObject loadQuestionBank(String option_bank_id){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    ResultSet rs = new OptionBankMgr().loadOptionBank(option_bank_id);
                     while(rs.next()){
                         JSONObject  j = new JSONObject();
                         js.put(rs.getString("option_name"),rs.getString("option_detail"));   
                     } 
                     rs = new OptionBankMgr().loadAnswerBank(option_bank_id);
                     while(rs.next()){
                         JSONObject  j = new JSONObject();
                         js.put("answer",rs.getString("answer"));   
                     } 
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
    
    
}
