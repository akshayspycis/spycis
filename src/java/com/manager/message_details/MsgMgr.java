/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.message_details;

import com.class_manager.message_details.MessageDetailsMgr;
import com.data_manager.message_details.MessageDetails;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class MsgMgr {
    MsgMgr that=this;
    public boolean parseData(String json) throws JSONException{
     try {
            JSONObject jObject = new JSONObject(json);
            MessageDetails uta = new MessageDetails();
            uta.setSender_id(jObject.getString("sender_id"));
            uta.setReceiver_id(jObject.getString("receiver_id"));
            uta.setSubject(jObject.getString("subject"));
            uta.setBody(jObject.getString("body"));
            uta.setMsg_data(jObject.getString("msg_data"));
            uta.setMsg_time(jObject.getString("msg_time"));
            if(new MessageDetailsMgr().insMessageDetails(uta)){
                    return true;    
            }else{
                return false;
            }
    } catch (Exception e) {
        e.printStackTrace();
        return false;
    }
}
 public boolean parse(final String json){
     Thread t1 = new Thread() {
         public void run() {
                loadAutoData();
         }
         private void loadAutoData() {
             try {
                 that.parseData(json);
             } catch (JSONException ex) {
                 Logger.getLogger(MsgMgr.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
     };
     t1.start();
     return true;
 }
 
    
}
