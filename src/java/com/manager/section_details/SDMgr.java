/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.section_details;

import com.class_manager.mocktest_details.MocktestFavSectionMgr;
import com.class_manager.section_details.SectionDetailsMgr;
import com.data_manager.section_details.SectionDetails;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class SDMgr {
            public JSONObject loadRecentSection(){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    SectionDetails cd = new SectionDetailsMgr().loadRecentSection();
                         JSONObject  j = new JSONObject();
                            j.put("section_id",cd.getSection_id());
                            j.put("section_name",cd.getSection_name());
                            ja.put(j);
                     js.put("section_details",ja);   
                     js.put("category_id",cd.getCategory_id());   
                     js.put("subcategory_id",cd.getSubcategory_id());   
                     System.out.println(js);
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
    
    public JSONObject loadSectionDetails(String category_id,String subcategory_id){
        JSONObject js=null;
            try {
                js = new JSONObject();
                JSONArray ja = new JSONArray();
                ArrayList<SectionDetails> cd = new SectionDetailsMgr().loadSection(category_id,subcategory_id);
                 for (SectionDetails subcategory_details : cd) {
                     JSONObject  j = new JSONObject();
                        j.put("section_id",subcategory_details.getSection_id());
                        j.put("section_name",subcategory_details.getSection_name());
                     ja.put(j);
                 }
                 js.put("section_details",ja);   
                 js.put("category_id",category_id);   
                 js.put("subcategory_id",subcategory_id);   
                 System.out.println(js);
            }catch(Exception e){
                    e.printStackTrace();
            }
        return js;
    }
    
    public JSONObject loadFavSectionDetails(String software_id,String category_id,String subcategory_id){
        return new MocktestFavSectionMgr().loadFavSection(software_id,category_id,subcategory_id);
    }

    public JSONObject loadSectionDetails_Test() {
        JSONObject js=null;
            try {
                js = new JSONObject();
                JSONArray ja = new JSONArray();
                ArrayList<SectionDetails> cd = new SectionDetailsMgr().loadSection_Test();
                 for (SectionDetails subcategory_details : cd) {
                     JSONObject  j = new JSONObject();
                        j.put("section_id",subcategory_details.getSection_id());
                        j.put("section_name",subcategory_details.getSection_name());
                     ja.put(j);
                 }
                 js.put("section_details",ja);   
                 System.out.println(js);
            }catch(Exception e){
                    e.printStackTrace();
            }
        return js;
    }
}
