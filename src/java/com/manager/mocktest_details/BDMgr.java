/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.mocktest_details;

import com.class_manager.software_details.SoftwareAddressDetailsMgr;
import com.class_manager.software_details.SoftwareContactDetailsMgr;
import com.data_manager.software_details.SoftwareAddressDetails;
import com.data_manager.software_details.SoftwareContactDetails;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class BDMgr {
public boolean parse(String json) throws JSONException{
    JSONObject jObject = new JSONObject(json);
    SoftwareContactDetails s= new SoftwareContactDetails();
    s.setSoftware_id(jObject.getString("software_id"));
    s.setAlt_contact_no(jObject.getString("alt_contact_no"));
    s.setWebsite(jObject.getString("website"));
    if(new SoftwareContactDetailsMgr().updSoftwareContactDetails(s)){
        return true;
    }else{
        return false;
    }
}        

    public boolean parseAddress(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);
            SoftwareAddressDetails s= new SoftwareAddressDetails();
            s.setSoftware_id(jObject.getString("software_id"));
            s.setAddress(jObject.getString("address"));
            s.setStreet(jObject.getString("street"));
            s.setPincode(jObject.getString("pincode"));
            s.setCity(jObject.getString("city"));
            s.setState(jObject.getString("state"));
            s.setCountry(jObject.getString("country"));
            
            if(new SoftwareAddressDetailsMgr().insSoftwareAddressDetails(s)){
                return true;
            }else{
                return false;
            }
    }
}
