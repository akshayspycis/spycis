/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.mocktest_details;

import com.class_manager.software_details.SoftwareDetailsMgr;
import com.data_manager.config.GlobalData;
import java.util.ArrayList;
import org.json.JSONObject;
import org.json.Test;

/**
 *
 * @author Akshay
 */
public class MTMgr {
    public JSONObject loadSoftwareDetails(String user_id) {
        try {
            return new SoftwareDetailsMgr().loadSoftwareDetails(user_id);
        }catch(Exception e){
                e.printStackTrace();
                return null;
        }
    }
}

