/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.other_settings.sql_injection;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.login_details.LoginDetails;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class SqlInjectionCheck {
    public JSONObject check(String str){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        JSONObject  j = new JSONObject();
       try {
           String sql = "select * from login_details where contact_no=?";
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, str);
            j.put("1",true);
            return j;
        } catch (Exception e) {
            e.printStackTrace();
            db.closeConection(config);
            try {
               j.put("1",false);
            } catch (JSONException ex) {}
            return j;
        }
    }
    public boolean checkPassword(String pass,String user_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        JSONObject  j = new JSONObject();
       try {
           String sql = "select 1 from login_details where user_id=? and password=?";
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, user_id);
            config.getPstmt().setString(2, pass);
            config.setRs(config.getPstmt().executeQuery());
            if(config.getRs().next()){
                db.closeConection(config);
                return true;   
            }else{
                db.closeConection(config);
                return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            db.closeConection(config);
            return false;   
        }
    }
    public boolean updPassword(LoginDetails logindetails){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "update login_details set password =? where user_id =?";
           config.setPstmt(config.getConn().prepareStatement(sql));
            
            config.getPstmt().setString(1, logindetails.getPassword());
            config.getPstmt().setString(2, logindetails.getUser_id());
            System.out.println(config.getPstmt());
            int x=config.getPstmt().executeUpdate();
            System.out.println(x);
            if(x>0){
                db.closeConection(config);
                return true;   
            }else{
                db.closeConection(config);
                return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            db.closeConection(config);
            return false;   
        }
    }
}
