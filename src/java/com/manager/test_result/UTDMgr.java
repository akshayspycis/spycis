/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.test_result;

import com.class_manager.test_result.TestResultMgr;
import java.sql.ResultSet;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class UTDMgr {
    
public JSONObject loadUserTestDetails(String test_id,String user_id){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    TestResultMgr trm=new TestResultMgr();
                        ResultSet tud = trm.loadUserTestDetails(test_id,user_id);
                        while (tud.next()) {
                            JSONObject  jo = new JSONObject();
                            jo.put("unique_test_key",tud.getString("unique_test_key"));
                            jo.put("test_name",tud.getString("test_name"));
                            jo.put("e_date",tud.getString("e_date"));
                            jo.put("start_time",tud.getString("start_time"));
                            jo.put("cut_off",tud.getString("cut_off"));    
                            jo.put("sum",tud.getString("sum"));    
                            jo.put("test_date",tud.getString("test_date"));    
                            jo.put("active_time",tud.getString("active_time"));    
                            jo.put("negative",tud.getString("negative"));    
                            jo.put("obtained",tud.getString("obtained"));    
                            js.put(user_id,jo);   
                     }
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }        

public JSONObject loadResultSummery(String test_id,String user_id){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    TestResultMgr trm=new TestResultMgr();
                        ResultSet tud = trm.loadResultSummery(test_id,user_id);
                        while (tud.next()) {
                            JSONObject  jo = new JSONObject();
                            jo.put("answer",tud.getString("answer"));
                            jo.put("select_option",tud.getString("select_option"));
                            js.put(tud.getString("question_id"),jo);   
                     }
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }        
}
