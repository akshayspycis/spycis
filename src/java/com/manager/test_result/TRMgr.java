/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.test_result;

import com.class_manager.user_test_account.UserTestAccountMgr;
import com.class_manager.user_test_question_submit_details.UserTestQuestion_SubmitDetailsMgr;
import com.data_manager.config.GlobalData;
import com.data_manager.user_test_account.UserTestAccount;
import com.data_manager.user_test_question_submit_details.UserTestQuestion_SubmitDetails;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class TRMgr {
    TRMgr that=this;
    public boolean parseData(String json) throws JSONException{
     try {
            JSONObject jObject = new JSONObject(json);
            System.out.println(json);
            UserTestAccount uta = new UserTestAccount();
            uta.setUser_id(jObject.getString("user_id"));
            uta.setTest_id(jObject.getString("test_id"));
            uta.setActive_time(jObject.getString("active_time"));
            uta.setLeave_time(jObject.getString("leave_time"));
            uta.setTest_date(jObject.getString("test_date"));
            if(new UserTestAccountMgr().insUserTestAccount(uta)){
                JSONObject ins_test_ques_details = jObject.getJSONObject("question_details");
                System.out.println(ins_test_ques_details);
                //---------------------set option details inside the Question Details Object------------------------------
                Iterator<?> keys = ins_test_ques_details.keys();
                ArrayList<UserTestQuestion_SubmitDetails> user_test_question_submit_details=new ArrayList<UserTestQuestion_SubmitDetails>();
                while(keys.hasNext()) {
                    String key = (String)keys.next();
                    UserTestQuestion_SubmitDetails t = new UserTestQuestion_SubmitDetails();
                    t.setQuestion_id(key);
                    t.setSelect_option(ins_test_ques_details.getString(key));
                    user_test_question_submit_details.add(t);
                }
                if(new UserTestQuestion_SubmitDetailsMgr().insUserTestQuestion_SubmitDetails(user_test_question_submit_details)){
                    return true;    
                }else{
                    return false;
                }
            }else{
                return false;
            }
                
    } catch (Exception e) {
        e.printStackTrace();
                    return false;
    }
}
 public boolean parse(final String json){
     Thread t1 = new Thread() {
         public void run() {
                loadAutoData();
         }
         private void loadAutoData() {
             try {
                 that.parseData(json);
             } catch (JSONException ex) {
                 Logger.getLogger(TRMgr.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
     };
     t1.start();
     return true;
 }
 
 
}
