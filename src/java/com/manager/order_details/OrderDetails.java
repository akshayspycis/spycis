/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manager.order_details;

import com.class_manager.test_details.TestDetailsMgr;
import com.data_manager.config.GlobalData;

import java.sql.ResultSet;
import java.util.Random;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class OrderDetails {

    public static String getRandomString() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();
        return generatedString;
    }

    public String getUrl(String url) {
        if (url != null) {
            switch (url) {
                case "PAYMENT_DETAILS_URL":
                    return GlobalData.PAYMENT_DETAILS_URL;
                case "CREATE_ORDER_URL":
                    return GlobalData.CREATE_ORDER_URL;
            }
        }
        return null;
    }

    public boolean checkPaymentStatus(String user_id, String test_id) {
        try {
            if (user_id != null && test_id != null) {
                ResultSet rs = new TestDetailsMgr().checkPaymentStatus(user_id, test_id);
                while (rs.next()) {
                    return rs.getString("status").equals("true");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
