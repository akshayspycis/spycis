/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manager.request_details;

import com.class_manager.mocktest_details.MocktestPublicCircleMgr;
import com.class_manager.mocktest_details.MocktestTestCircleMgr;
import com.class_manager.user_details.UserRequestMgr;
import com.data_manager.config.GlobalData;
import com.data_manager.mocktest_details.MocktestPublicCircle;
import com.data_manager.mocktest_details.MocktestTestCircle;
import com.data_manager.user_details.UserRequest;
import com.manager.test_result.TRMgr;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author akshay
 */
public class RequestMgr {
    
public JSONObject selUserRequest(String software_id) throws JSONException{
    return  new UserRequestMgr().loadUserRequest(software_id);
}
public boolean insUserRequest(String user_id,String software_id) throws JSONException{
      final UserRequest mocktest_test_circle= new UserRequest();
      mocktest_test_circle.setSoftware_id(software_id);
      mocktest_test_circle.setUser_id(user_id);
      Thread t1 = new Thread() {
         public void run() {
                loadAutoData();
         }
         private void loadAutoData() {
             try {
                if(new UserRequestMgr().insUserRequest(mocktest_test_circle)){
                    GlobalData.software_user_request_list.put(mocktest_test_circle.getUser_id(), (HashMap) GlobalData.software_user_request_list.get(mocktest_test_circle.getUser_id()).put(mocktest_test_circle.getSoftware_id(),"true"));
                }
             } catch (Exception ex) {
                     Logger.getLogger(RequestMgr.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
     };
     t1.start();
    return true;
}        
public boolean insUserInPublicCircle(String user_id,String software_id,String user_r_id) throws JSONException{
      final String mocktest_test_circle_id= user_r_id;
      final MocktestPublicCircle mocktest_public_circle= new MocktestPublicCircle();
      mocktest_public_circle.setSoftware_id(software_id);
      mocktest_public_circle.setUser_id(user_id);
      Thread t1 = new Thread() {
         public void run() {
                loadAutoData();
         }
         private void loadAutoData() {
             try {
                if(new MocktestPublicCircleMgr().insMocktestPublicCircle(mocktest_public_circle,mocktest_test_circle_id)){
                    //GlobalData.software_mocktest_test_circle_list.put(mocktest_test_circle.getUser_id(), (HashMap) GlobalData.software_mocktest_test_circle_list.get(mocktest_test_circle.getUser_id()).put(mocktest_test_circle.getSoftware_id(),"true"));
                }
             } catch (Exception ex) {
                     Logger.getLogger(RequestMgr.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
     };
     t1.start();
    return true;
}        

public boolean insUserInTestCircle(String user_id,String software_id) throws JSONException{
      final MocktestTestCircle mocktest_test_circle= new MocktestTestCircle();
      mocktest_test_circle.setSoftware_id(software_id);
      mocktest_test_circle.setUser_id(user_id);
      Thread t1 = new Thread() {
         public void run() {
                loadAutoData();
         }
         private void loadAutoData() {
             try {
                if(new MocktestTestCircleMgr().insMocktestTestCircle(mocktest_test_circle)){
                    //GlobalData.software_mocktest_test_circle_list.put(mocktest_test_circle.getUser_id(), (HashMap) GlobalData.software_mocktest_test_circle_list.get(mocktest_test_circle.getUser_id()).put(mocktest_test_circle.getSoftware_id(),"true"));
                }
             } catch (Exception ex) {
                     Logger.getLogger(RequestMgr.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
     };
     t1.start();
    return true;
}        
}
