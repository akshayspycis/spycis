/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.exam_phase_details;

import com.class_manager.exam_phase_details.ExamPhaseDetailsMgr;
import com.data_manager.exam_phase_details.ExamPhaseDetails;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class EPDMgr {
    public JSONObject loadRecentExamPhase(){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    ExamPhaseDetails cd = new ExamPhaseDetailsMgr().loadRecentExamPhase();
                         JSONObject  j = new JSONObject();
                            j.put("exam_phase_id",cd.getExam_phase_id());
                            j.put("exam_phase_name",cd.getExam_phase_name());
                            ja.put(j);
                     js.put("exam_phase_details",ja);   
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
    
    public JSONObject loadExamPhaseDetails(){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    ArrayList<ExamPhaseDetails> cd = new ExamPhaseDetailsMgr().loadExamPhase();
                     for (ExamPhaseDetails exam_phase_details : cd) {
                         JSONObject  j = new JSONObject();
                            j.put("exam_phase_id",exam_phase_details.getExam_phase_id());
                            j.put("exam_phase_name",exam_phase_details.getExam_phase_name());
                         ja.put(j);
                     }
                     js.put("exam_phase_details",ja);   
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }    
}
