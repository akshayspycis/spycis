/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.category_details;

import com.class_manager.category_details.CategoryDetailsMgr;
import com.class_manager.mocktest_details.MocktestFavCategoryMgr;
import com.data_manager.category_details.CategoryDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CDMgr {
 
    public JSONObject loadRecentCategory(){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    CategoryDetails cd = new CategoryDetailsMgr().loadRecentCategory();
                         JSONObject  j = new JSONObject();
                            j.put("category_id",cd.getCategory_id());
                            j.put("category_name",cd.getCategory_name());
                            ja.put(j);
                     js.put("category_details",ja);   
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
    
    public JSONObject loadCategoryDetails(String user_id){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    JSONArray ja = new JSONArray();
                    ArrayList<CategoryDetails> cd = new CategoryDetailsMgr().loadCategory(user_id);
                     for (CategoryDetails categoryDetails : cd) {
                         JSONObject  j = new JSONObject();
                            j.put("category_id",categoryDetails.getCategory_id());
                            j.put("category_name",categoryDetails.getCategory_name());
                         ja.put(j);
                     }
                     js.put("category_details",ja);   
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
    
    public JSONObject loadMocktestFavCategory(String software_id){
        JSONObject js=null;
                try {
                    return new MocktestFavCategoryMgr().loadFavCategory(software_id);
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
}
