/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manager.question_details;

import com.class_manager.option_bank.OptionBankMgr;
import com.class_manager.question_bank.QuestionBankMgr;
import com.class_manager.question_details.QuestionDetailsMgr;
import com.class_manager.question_lang_details.QuestionLangDetailsMgr;
import com.class_manager.test_question.TestQuestionMgr;
import com.data_manager.option_bank.OptionBank;
import com.data_manager.question_bank.QuestionBank;
import com.data_manager.question_details.QuestionDetails;
import com.data_manager.question_lang_details.QuestionLangDetails;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class QBankMgr {
    
   public String addOnlyQuestion(Map<Integer, Map> question,Map<Integer,Long> unique,String category_id,String subcategory_id,String section_id,String topic_id) {
        try {
            for (Integer language_id : question.keySet()) {
                        for (Object question_id : question.get(language_id).keySet()) {
                            QuestionBank qb=(QuestionBank) question.get(language_id).get(question_id);
                            if(new QuestionBankMgr().insQuestionBank(qb)){
                                for (int i = 0; i < qb.getOptionlist().size(); i++) {
                                    new OptionBankMgr().insOptionBank(qb.getOptionlist().get(i));
                                }
                                QuestionLangDetails q= new QuestionLangDetails();
                                q.setLanguage_id(String.valueOf(language_id));
                                q.setQuestion_id(String.valueOf(question_id));
                                new QuestionLangDetailsMgr().insQuestionLangDetails(q);
                            }
                        }
            }
            for (Object question_id : unique.keySet()) {
                QuestionDetails dd= new QuestionDetails();
                    dd.setDiscription_id("");
                    dd.setQuestion_id(String.valueOf(unique.get(question_id)));
                    dd.setCategory_id(category_id);
                    dd.setSubcategory_id(subcategory_id);
                    dd.setSection_id(section_id);
                    dd.setTopic_id(topic_id);
                    new QuestionDetailsMgr().insQuestionDetails(dd);
            }
        } catch (Exception e) {
            return null;
        }
        System.out.println("done");
        return null;
    }
    
    
   public boolean parse(String json) throws JSONException{
            JSONObject jObject = new JSONObject(json);
            JSONObject info = jObject.getJSONObject("language_details");
            String category_id=jObject.getString("category_id");
            String subcategory_id=jObject.getString("subcategory_id");
            String section_id=jObject.getString("section_id");
            String topic_id=jObject.getString("topic_id");
            
            Iterator<?> keys = info.keys();
            Map<Integer,Long> unique=null;
            unique=new HashMap<Integer,Long>();
            Map<Integer, Map> question = new HashMap<Integer, Map>();
            long sid=new QuestionDetailsMgr().loadMaxId()+1;
            while(keys.hasNext()) {
                String key = (String)keys.next();
                JSONObject s=info.getJSONObject(key);
                Iterator<?> k = s.keys();
                    while(k.hasNext()) {
                        String kkk=(String)k.next();
                        if(kkk.equals("question_details")){
//                            if(question.get(Integer.parseInt(key))==null){
//                                question.put(Integer.parseInt(key), new HashMap<Long, QuestionBank>());
//                            }
                            JSONObject ll=s.getJSONObject(kkk);
                            Iterator<?> ke = ll.keys();
                            Map<Long, QuestionBank> question_list = new HashMap<Long, QuestionBank>();
                            while(ke.hasNext()) {
                                String kl = (String)ke.next();
                                
                                long question_id;
                                        if(unique.get(Integer.parseInt(kl))!=null){
                                            question_id=unique.get(Integer.parseInt(kl));
                                        }else{
                                            question_id=sid+unique.size();
                                            unique.put(Integer.parseInt(kl), question_id);
                                        }
                                JSONObject kkll=ll.getJSONObject(kl);
                                Iterator<?> kaa = kkll.keys();
                                QuestionBank qb = new QuestionBank();
                                while(kaa.hasNext()) {
                                    String nm=(String)kaa.next();
                                    ArrayList<OptionBank> option = new ArrayList<OptionBank>();
                                    if(nm.equals("option_details")){
                                        JSONObject lll=kkll.getJSONObject(nm);
                                        Iterator<?> kee = lll.keys();
                                        while(kee.hasNext()) {
                                            String ml=(String)kee.next();
                                            if(ml.equals("answer")){
                                                qb.setAnswer(lll.getString(ml));
                                            }else{
                                                OptionBank op=new OptionBank();
                                                op.setOption(lll.getString(ml));
                                                op.setOption_name(ml);
                                                option.add(op );
                                            }
                                        }
                                        qb.setOptionlist(option);
                                    }
                                    if(nm.equals("qus")){
                                        qb.setQuestion(kkll.getString(nm));
                                        question_list.put(question_id, qb);
                                        question.put(Integer.parseInt(key), question_list);
                                    }
                                }
                            }
                        }
                    }
            }
                        this.addOnlyQuestion(question, unique,category_id,subcategory_id,section_id,topic_id);
                        return true;
}
 
   public JSONObject loadSeletedQus(String question_id,String language_id){
     JSONObject js=null;
                try {
                    js = new JSONObject();
                    ResultSet rs = new TestQuestionMgr().loadSelectedQus(question_id,language_id);
                    HashMap<String,String> option_details=null;
                    while(rs.next()){
                        if(js.isNull("question")){
                            option_details=new HashMap<String,String>();
                            js.put("question",rs.getString("qb.question"));    
                            js.put("discription_id",rs.getString("discription_id"));    
                            js.put("discription_bank_id",rs.getString("discription_bank_id"));    
                            option_details.put(rs.getString("ob.option_name"), rs.getString("ob.option_detail"));
                        }else{
                            option_details.put(rs.getString("ob.option_name"), rs.getString("ob.option_detail"));
                        }
                    }
                    js.put("option_details", option_details);
                    System.out.println(js);
                return js;
                }catch(Exception e)   {
                    e.printStackTrace();
                    return null;
                }
 }
   
   public boolean updQuestionBank(String json) throws JSONException{
       System.out.println(json);
        JSONObject jObject = new JSONObject(json);
        QuestionBank q= new QuestionBank();
        q.setQuestion_bank_id(jObject.getString("question_bank_id"));
        q.setQuestion(jObject.getString("question"));
        if(new QuestionBankMgr().updQuestionBank(q)){
            return true;
        }else{
            return false;
        }
   }
 
}
