/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.direction_details;

import com.class_manager.discription_bank.DiscriptionBankMgr;
import com.data_manager.discription_bank.DiscriptionBank;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class DirBankMgr {
    public JSONObject loadDirectionBank(String discription_bank_id){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    DiscriptionBank cd = new DiscriptionBankMgr().loadDiscription(discription_bank_id);
                         JSONObject  j = new JSONObject();
                         js.put("direction_bank",cd.getDiscription());   
                }catch(Exception e){
                        e.printStackTrace();
                }
        return js;
    }
    public boolean updDirectionDetails(String json){
            JSONObject jObject;
            try {
                jObject = new JSONObject(json);
                DiscriptionBank db=new DiscriptionBank();
                db.setDiscription_bank_id(jObject.getString("discription_bank_id"));
                db.setDiscription(jObject.getString("discription"));
                if(new DiscriptionBankMgr().updDiscriptionBank(db)){
                    return true;
                }else{
                    return false;
                }
            } catch (JSONException ex) {
                return false;
            }
    }
}
