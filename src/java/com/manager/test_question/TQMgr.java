/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manager.test_question;

import com.class_manager.test_question.TestQuestionMgr;
import com.data_manager.config.GlobalData;
import java.sql.ResultSet;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class TQMgr {
    
    
public JSONObject loadTest(String test_id){
    try {
        if(GlobalData.test_question==null){
            JSONObject j= this.loadTestQuestion(test_id);
            GlobalData.test_question= new HashMap<String, JSONObject>();
            GlobalData.test_question.put(test_id, j);
            return j;
        }else{
            if(GlobalData.test_question.containsKey(test_id)){
                return (JSONObject) GlobalData.test_question.get(test_id);
            }else{
                JSONObject j= this.loadTestQuestion(test_id);
                GlobalData.test_question.put(test_id, j);
                return j;
            }
        }
    } catch (Exception ex) {
            ex.printStackTrace();
            return null;
    }
}

public JSONObject loadAnsSheet(String test_id,String user_id){
            JSONObject js=null;
                try {
                    js = new JSONObject();
                    ResultSet rs = new TestQuestionMgr().loadAnsSheet(test_id,user_id);
                    HashMap<String,JSONObject> question = new LinkedHashMap<String,JSONObject>();
                    Set<String> s=null;
                    while(rs.next()){
                        JSONObject j = new JSONObject();
                        j.put("marks", rs.getString("marks"));
                        j.put("answer", rs.getString("answer"));
                        j.put("select_option", rs.getString("select_option"));
                        question.put(rs.getString("question_id"), j);
                    }
                    if(s==null){
                        s=question.keySet();
                    }
                js.put("q", new JSONObject(question));
                js.put("keyset", s);
                return js;
                }catch(Exception e)   {
                    e.printStackTrace();
                    return null;
                }
}

public JSONObject loadChartSheet(String test_id,String user_id){
            JSONObject js=null;
                try {
                    js = new JSONObject();
                    ResultSet rs = new TestQuestionMgr().loadAnsSheet(test_id,user_id);
                    rs.last();
                    js.put("total_question", rs.getRow());
                    rs.beforeFirst();
                    int total_attempted=0;
                    int total_correct=0;
                    while(rs.next()){
                        if(rs.getString("select_option")!=null){
                            if(rs.getString("select_option").equals(rs.getString("answer"))){
                                total_correct++;
                            }
                            total_attempted++;
                        }
                    }
                    js.put("total_correct", total_correct);
                    js.put("total_attempted", total_attempted);
                return js;
                }catch(Exception e)   {
                    e.printStackTrace();
                    return null;
                }
}

public JSONObject loadTestQuestion(String test_id){
        JSONObject js=null;
                try {
                    js = new JSONObject();
                    ResultSet rs = new TestQuestionMgr().loadTestQuestion(test_id);
                    HashMap<String,HashMap> root = new HashMap<String,HashMap>();
                    HashMap<String,HashMap> sub_root = null;
                    HashMap<String,String> discription=null;
                    HashMap<String,HashMap> question=null;
                    HashMap<String,HashMap> option=null;
                    HashMap<String,String> question_details=null;
                    HashMap<String,String> option_details=null;
                    HashMap<String,String> section_details=null;
                    HashMap<String,String> question_section_details=null;
                    Set<String> s=null;
                    Set<String> keyset_section=null;
                    List<String> kfd=new ArrayList<String>();
                    while(rs.next()){
                        String language_id=rs.getString("ql.language_id");
                        if(!root.containsKey(language_id)){
                            sub_root = new HashMap<String,HashMap>();
                            option_details=new HashMap<String,String>();
                            option_details.put(rs.getString("ob.option_name"), rs.getString("ob.option_detail"));
                            option=new HashMap<String,HashMap>();
                            option.put(rs.getString("q.question_id"), option_details);
                            sub_root.put("option_details", option);
                            //...........................................................................
                            
                            question = new LinkedHashMap<String,HashMap>();
                            question_details = new HashMap<String,String>();
                            question_details.put(rs.getString("q.discription_id"), rs.getString("qb.question"));
                            question.put(rs.getString("q.question_id"), question_details);
                            sub_root.put("question_details", question);
                            //...........................................................................
                            discription=new HashMap<String,String>();
                            discription.put(rs.getString("q.discription_id"), rs.getString("dis"));
                            sub_root.put("discription_details", discription);
                            //---------------------------------------------------------------------------------
                            question_section_details = new HashMap<String,String>();
                            question_section_details.put(rs.getString("q.question_id"),rs.getString("q.section_id"));
                            sub_root.put("question_section_details", question_section_details);
                            //..................................................................................
                            boolean b=true;
                            for (int i = 0; i < kfd.size(); i++) {
                                if(kfd.get(i).equals(rs.getString("q.section_id"))){
                                    b=false;
                                    break;
                                }
                            }
                            if(b){
                                kfd.add(rs.getString("q.section_id"));
                            }
                            section_details= new HashMap<String,String>();
                            section_details.put(rs.getString("section_id"),rs.getString("section_name"));
                        }else{
                            sub_root=(HashMap<String, HashMap>) root.get(language_id);
                            question=sub_root.get("question_details");
                            option=sub_root.get("option_details");
                            if(question.containsKey(rs.getString("q.question_id"))){
                                option_details=option.get(rs.getString("q.question_id"));
                                    if(!option_details.containsKey(rs.getString("ob.option_name"))){
                                        option_details.put(rs.getString("ob.option_name"), rs.getString("ob.option_detail"));
                                        option.put(rs.getString("q.question_id"),option_details);
                                        sub_root.put("option_details", option);
                                    }
                                    if(!question_section_details.containsKey(rs.getString("q.question_id"))){
                                        question_section_details.put(rs.getString("q.question_id"),rs.getString("q.section_id"));
                                        sub_root.put("question_section_details", question_section_details);
                                    }
                                    if(!section_details.containsKey(rs.getString("section_id"))){
                                        section_details.put(rs.getString("section_id"),rs.getString("section_name"));
                                            boolean b=true;
                                            for (int i = 0; i < kfd.size(); i++) {
                                                if(kfd.get(i).equals(rs.getString("q.section_id"))){
                                                    b=false;
                                                    break;
                                                }
                                            }
                                            if(b){
                                                kfd.add(rs.getString("q.section_id"));
                                            }
                                    }
                            }else{
                                    option_details=new HashMap<String,String>();
                                    option_details.put(rs.getString("ob.option_name"), rs.getString("ob.option_detail"));
                                    option.put(rs.getString("q.question_id"), option_details);
                                    sub_root.put("option_details", option);
                                    //.........................................................
                                    question_details = new HashMap<String,String>();
                                    question_details.put(rs.getString("q.discription_id"), rs.getString("qb.question"));
                                    question.put(rs.getString("q.question_id"), question_details);
                                    sub_root.put("question_details", question);
                            }
                            discription=sub_root.get("discription_details");
                                if(!discription.containsKey(rs.getString("q.discription_id"))){
                                    discription.put(rs.getString("q.discription_id"), rs.getString("dis"));
                                    sub_root.put("discription_details", discription);
                                }
                            }
                            root.put(language_id, sub_root);
                        }
                    if(s==null){
                        s=question.keySet();
                    }
                    if(keyset_section==null){
                        keyset_section=section_details.keySet();
                    }
                    System.out.println(kfd);
                js.put("q", new JSONObject(root));
                js.put("keyset", s);
                js.put("section_details", section_details);
                js.put("keyset_section", kfd);
                return js;
                }catch(Exception e)   {
                    e.printStackTrace();
                    return null;
                }
}
   
}


