/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.discription_bank;

import com.data_manager.question_bank.QuestionBank;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Akshay
 */
public class DiscriptionBank {
    String discription_bank_id="";
    String discription="";
    Map<Long,QuestionBank> question_list=new HashMap<Long,QuestionBank>();

    public String getDiscription_bank_id() {
        return discription_bank_id;
    }

    public void setDiscription_bank_id(String discription_bank_id) {
        this.discription_bank_id = discription_bank_id;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public Map<Long, QuestionBank> getQuestion_list() {
        return question_list;
    }

    public void setQuestion_list(Map<Long, QuestionBank> question_list) {
        this.question_list = question_list;
    }

        
}
