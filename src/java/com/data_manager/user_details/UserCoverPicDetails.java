/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.user_details;

/**
 *
 * @author Akshay
 */
public class UserCoverPicDetails {
String user_cover_pic_id="";    
String user_id="";    
String pic="";    
String date="";    

    public String getUser_cover_pic_id() {
        return user_cover_pic_id;
    }

    public void setUser_cover_pic_id(String user_cover_pic_id) {
        this.user_cover_pic_id = user_cover_pic_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    
}
