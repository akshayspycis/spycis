/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.mocktest_details;

/**
 *
 * @author Akshay
 */
public class MocktestFavSubcategory {
    String mocktest_fav_subcategory_id="";
    String subcategory_id="";
    String category_id="";
    String software_id="";

    public String getMocktest_fav_subcategory_id() {
        return mocktest_fav_subcategory_id;
    }

    public void setMocktest_fav_subcategory_id(String mocktest_fav_subcategory_id) {
        this.mocktest_fav_subcategory_id = mocktest_fav_subcategory_id;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }
}
