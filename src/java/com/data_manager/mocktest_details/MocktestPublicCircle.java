/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.mocktest_details;

/**
 *
 * @author Akshay
 */
public class MocktestPublicCircle {
String mocktest_public_circle_id="";
String software_id="";
String user_id="";
String status="";

    public String getMocktest_public_circle_id() {
        return mocktest_public_circle_id;
    }

    public void setMocktest_public_circle_id(String mocktest_public_circle_id) {
        this.mocktest_public_circle_id = mocktest_public_circle_id;
    }

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    

}
