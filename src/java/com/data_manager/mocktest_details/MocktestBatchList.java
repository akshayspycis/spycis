/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.mocktest_details;

/**
 *
 * @author Akshay
 */
public class MocktestBatchList {
String mocktest_batch_list_id    ="" ;
String software_id ="" ;
String mocktest_batch_name ="" ;
String status ="" ;

    public String getMocktest_batch_list_id() {
        return mocktest_batch_list_id;
    }

    public void setMocktest_batch_list_id(String mocktest_batch_list_id) {
        this.mocktest_batch_list_id = mocktest_batch_list_id;
    }

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    public String getMocktest_batch_name() {
        return mocktest_batch_name;
    }

    public void setMocktest_batch_name(String mocktest_batch_name) {
        this.mocktest_batch_name = mocktest_batch_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
}
