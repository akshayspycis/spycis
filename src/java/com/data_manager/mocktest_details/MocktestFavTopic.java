/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.mocktest_details;

/**
 *
 * @author Akshay
 */
public class MocktestFavTopic {
String software_fav_topic_id ="" ;
String software_id ="" ;
String section_id ="" ;
String topic_id ="" ;

    public String getSoftware_fav_topic_id() {
        return software_fav_topic_id;
    }

    public void setSoftware_fav_topic_id(String software_fav_topic_id) {
        this.software_fav_topic_id = software_fav_topic_id;
    }

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }


}
