/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.mocktest_details;

/**
 *
 * @author Akshay
 */
public class MocktestQuestionBank {
String mocktest_question_bank_id ="";
String software_id ="";
String question_id ="";
String date ="";

    public String getMocktest_question_bank_id() {
        return mocktest_question_bank_id;
    }

    public void setMocktest_question_bank_id(String mocktest_question_bank_id) {
        this.mocktest_question_bank_id = mocktest_question_bank_id;
    }

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    

}
