/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.mocktest_details;

/**
 *
 * @author Akshay
 */
public class MocktestQuickCircle {
    String mocktest_quick_circle_id="";
    String mocktest_batch_list_id="";
    String user_id="";

    public String getMocktest_quick_circle_id() {
        return mocktest_quick_circle_id;
    }

    public void setMocktest_quick_circle_id(String mocktest_quick_circle_id) {
        this.mocktest_quick_circle_id = mocktest_quick_circle_id;
    }

    public String getMocktest_batch_list_id() {
        return mocktest_batch_list_id;
    }

    public void setMocktest_batch_list_id(String mocktest_batch_list_id) {
        this.mocktest_batch_list_id = mocktest_batch_list_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

        
}
