/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.mocktest_details;

/**
 *
 * @author Akshay
 */
public class MocktestFavSection {
String mocktest_fav_section_id="";    
String section_id="";    
String software_id="";    

    public String getMocktest_fav_section_id() {
        return mocktest_fav_section_id;
    }

    public void setMocktest_fav_section_id(String mocktest_fav_section_id) {
        this.mocktest_fav_section_id = mocktest_fav_section_id;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    
}
