/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.user_test_question_submit_details;

/**
 *
 * @author Akshay
 */
public class UserTestQuestion_SubmitDetails {
    String user_test_id ="";
    String question_id ="";
    String select_option ="";

    public String getUser_test_id() {
        return user_test_id;
    }

    public void setUser_test_id(String user_test_id) {
        this.user_test_id = user_test_id;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getSelect_option() {
        return select_option;
    }

    public void setSelect_option(String select_option) {
        this.select_option = select_option;
    }
    
}
