/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.question_bank;

import com.data_manager.option_bank.OptionBank;
import java.util.ArrayList;


/**
 *
 * @author Akshay
 */
public class QuestionBank {
    String question_bank_id="";
    String question="";
    String answer="";
    ArrayList<OptionBank> optionlist= new ArrayList<OptionBank>();

    public String getQuestion_bank_id() {
        return question_bank_id;
    }

    public void setQuestion_bank_id(String question_bank_id) {
        this.question_bank_id = question_bank_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public ArrayList<OptionBank> getOptionlist() {
        return optionlist;
    }

    public void setOptionlist(ArrayList<OptionBank> optionlist) {
        this.optionlist = optionlist;
    }
}
