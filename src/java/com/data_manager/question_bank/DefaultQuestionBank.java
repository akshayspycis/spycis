/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.question_bank;

/**
 *
 * @author Akshay
 */
public class DefaultQuestionBank {
String default_question_bank_id =""    ;
String section_id =""    ;
String topic_id =""    ;
String question_id =""    ;
String status =""    ;

    public String getDefault_question_bank_id() {
        return default_question_bank_id;
    }

    public void setDefault_question_bank_id(String default_question_bank_id) {
        this.default_question_bank_id = default_question_bank_id;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
}
