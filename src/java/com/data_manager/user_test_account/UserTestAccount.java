/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.user_test_account;

/**
 *
 * @author Akshay
 */
public class UserTestAccount {
    String user_test_id ="";
    String user_id ="";
    String test_id ="";
    String active_time ="";
    String leave_time ="";
    String test_date ="";

    public String getUser_test_id() {
        return user_test_id;
    }

    public void setUser_test_id(String user_test_id) {
        this.user_test_id = user_test_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getActive_time() {
        return active_time;
    }

    public void setActive_time(String active_time) {
        this.active_time = active_time;
    }

    public String getLeave_time() {
        return leave_time;
    }

    public void setLeave_time(String leave_time) {
        this.leave_time = leave_time;
    }

    public String getTest_date() {
        return test_date;
    }

    public void setTest_date(String test_date) {
        this.test_date = test_date;
    }
    
}
