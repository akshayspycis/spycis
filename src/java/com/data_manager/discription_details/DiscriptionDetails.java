/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.discription_details;

/**
 *
 * @author Akshay
 */
public class DiscriptionDetails {
  String discription_id="";
  String discription_bank_id="";
  String language_id="";

    public String getDiscription_id() {
        return discription_id;
    }

    public void setDiscription_id(String discription_id) {
        this.discription_id = discription_id;
    }

    public String getDiscription_bank_id() {
        return discription_bank_id;
    }

    public void setDiscription_bank_id(String discription_bank_id) {
        this.discription_bank_id = discription_bank_id;
    }

    public String getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(String language_id) {
        this.language_id = language_id;
    }
  
}
