/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.exam_phase_details;

/**
 *
 * @author Akshay
 */
public class ExamPhaseDetails {
    String exam_phase_id ="";
    String exam_phase_name ="";

    public String getExam_phase_id() {
        return exam_phase_id;
    }

    public void setExam_phase_id(String exam_phase_id) {
        this.exam_phase_id = exam_phase_id;
    }

    public String getExam_phase_name() {
        return exam_phase_name;
    }

    public void setExam_phase_name(String exam_phase_name) {
        this.exam_phase_name = exam_phase_name;
    }
    
}
