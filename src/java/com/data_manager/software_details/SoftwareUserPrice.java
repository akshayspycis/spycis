/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.software_details;

/**
 *
 * @author Akshay
 */
public class SoftwareUserPrice {
    String software_user_price_id ="";
    String software_id ="";
    String date ="";

    public void setSoftware_user_price_id(String software_user_price_id) {
        this.software_user_price_id = software_user_price_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
}
