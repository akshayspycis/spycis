/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.software_details;

/**
 *
 * @author Akshay
 */
public class SoftwareProfileDetails {
String software_prodile_id="";
String software_id="";
String pic="";
String date="";

    public String getSoftware_prodile_id() {
        return software_prodile_id;
    }

    public void setSoftware_prodile_id(String software_prodile_id) {
        this.software_prodile_id = software_prodile_id;
    }

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
