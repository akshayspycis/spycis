/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.software_details;

/**
 *
 * @author Akshay
 */
public class SoftwareUserPrivilegs {
String software_user_privilegs_id ="";
String software_id ="";
String user_id ="";
String date ="";
String status ="";

    public String getSoftware_user_privilegs_id() {
        return software_user_privilegs_id;
    }

    public void setSoftware_user_privilegs_id(String software_user_privilegs_id) {
        this.software_user_privilegs_id = software_user_privilegs_id;
    }

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
