/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.software_details;

/**
 *
 * @author Akshay
 */
public class SoftwareDetails {
String software_id="";
String user_id="";
String software_name_id="";
String orgenisation_name="";
String tag_line="";
String otp_authentication="";

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSoftware_name_id() {
        return software_name_id;
    }

    public void setSoftware_name_id(String software_name_id) {
        this.software_name_id = software_name_id;
    }

    public String getOrgenisation_name() {
        return orgenisation_name;
    }

    public void setOrgenisation_name(String orgenisation_name) {
        this.orgenisation_name = orgenisation_name;
    }

    public String getTag_line() {
        return tag_line;
    }

    public void setTag_line(String tag_line) {
        this.tag_line = tag_line;
    }

    public String getOtp_authentication() {
        return otp_authentication;
    }

    public void setOtp_authentication(String otp_authentication) {
        this.otp_authentication = otp_authentication;
    }
}
