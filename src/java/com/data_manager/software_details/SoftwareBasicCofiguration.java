/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.data_manager.software_details;

/**
 *
 * @author akshay
 */
public class SoftwareBasicCofiguration {
    String software_basic_cofiguration_id="";
    String status="";
    String software_id="";

    public String getSoftware_basic_cofiguration_id() {
        return software_basic_cofiguration_id;
    }

    public void setSoftware_basic_cofiguration_id(String software_basic_cofiguration_id) {
        this.software_basic_cofiguration_id = software_basic_cofiguration_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }
    
}
