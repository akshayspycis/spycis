/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.software_details;

/**
 *
 * @author Akshay
 */
public class SoftwareContactDetails {
String software_id= ""    ;
String contact_no= ""    ;
String alt_contact_no= ""    ;
String email= ""    ;
String website= ""    ;

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getAlt_contact_no() {
        return alt_contact_no;
    }

    public void setAlt_contact_no(String alt_contact_no) {
        this.alt_contact_no = alt_contact_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

}
