/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.software_details;

/**
 *
 * @author Akshay
 */
public class SoftwareCoverPicDetails {
    String software_cover_pic_details_id ="";
    String software_id ="";
    String cover_pic ="";
    String date ="";

    public String getSoftware_cover_pic_details_id() {
        return software_cover_pic_details_id;
    }

    public void setSoftware_cover_pic_details_id(String software_cover_pic_details_id) {
        this.software_cover_pic_details_id = software_cover_pic_details_id;
    }

    public String getSoftware_id() {
        return software_id;
    }

    public void setSoftware_id(String software_id) {
        this.software_id = software_id;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
}
