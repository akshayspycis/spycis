/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.test_details;

/**
 *
 * @author Akshay
 */
public class TestDetails {
    String test_id ="";
    String exam_phase_id ="";
    String category_id ="";
    String subcategory_id ="";
    String time ="";
    String cut_off ="";
    String e_date ="";
    String start_time ="";
    String unique_test_key ="";
    String test_name ="";
    String meditory_test_id ="";
    String visible ="";
    String negative_ratio ="";
    String type ="";

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getExam_phase_id() {
        return exam_phase_id;
    }

    public void setExam_phase_id(String exam_phase_id) {
        this.exam_phase_id = exam_phase_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCut_off() {
        return cut_off;
    }

    public void setCut_off(String cut_off) {
        this.cut_off = cut_off;
    }

    public String getE_date() {
        return e_date;
    }

    public void setE_date(String e_date) {
        this.e_date = e_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getUnique_test_key() {
        return unique_test_key;
    }

    public void setUnique_test_key(String unique_test_key) {
        this.unique_test_key = unique_test_key;
    }

    public String getTest_name() {
        return test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public String getMeditory_test_id() {
        return meditory_test_id;
    }

    public void setMeditory_test_id(String meditory_test_id) {
        this.meditory_test_id = meditory_test_id;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getNegative_ratio() {
        return negative_ratio;
    }

    public void setNegative_ratio(String negative_ratio) {
        this.negative_ratio = negative_ratio;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    
}
