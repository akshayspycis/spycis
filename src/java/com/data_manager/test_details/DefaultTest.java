/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.test_details;

/**
 *
 * @author Akshay
 */
public class DefaultTest {
String default_test_id="";
String test_id="";
String status="";

    public String getDefault_test_id() {
        return default_test_id;
    }

    public void setDefault_test_id(String default_test_id) {
        this.default_test_id = default_test_id;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
