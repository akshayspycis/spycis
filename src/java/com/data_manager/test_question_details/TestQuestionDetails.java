/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.test_question_details;

/**
 *
 * @author Akshay
 */
public class TestQuestionDetails {
    String test_question_id="";
    String test_id="";
    String question_id="";
    String marks="";
    String answer="";
    int order;

    public String getTest_question_id() {
        return test_question_id;
    }

    public void setTest_question_id(String test_question_id) {
        this.test_question_id = test_question_id;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }


        
}
