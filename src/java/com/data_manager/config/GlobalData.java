/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_manager.config;

import com.data_manager.user_details.User;
import com.manager.discription_bank.DBankMgr;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.jasper.tagplugins.jstl.core.Catch;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class GlobalData {
    public static HashMap<String, JSONObject> test_question =null;
    public static HashMap<String, JSONObject> otp_software =null;
    public static HashMap<String, String> online_user =null;
    public static HashMap<String, String> paytm_random =null;
    public static HashMap<String, JSONObject> online_user_details =null;
    public static HashMap<String, JSONObject> software_details_list =null;
    public static HashMap<String, HashMap> software_user_request_list =null;
    public static HashMap<String, List> mocktest_public_circle_list =null;
    public static JSONObject mocktest_public_circle_list_all =null;
    public static HashMap<String, JSONObject> software_search_list =null;
    public static ArrayList<User> cd = null;
    public static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    public static File file = new File("test.txt");
    public static PrintStream ps = null;
    public static String environment = "sandbox";
    //---------------path details--------------------
    public static boolean isPathAssigned = false;
    public static String profile_pic_path = null;
    public static String envv_path = null;
    //----------------
//    public static String root = "jsiacademy";
  public static String root = "spycis";
    public static String dburl;
    public static String userName;
    public static String password;
    public static String MID;
    public static String MercahntKey;
    public static String INDUSTRY_TYPE_ID;
    public static String CHANNEL_ID;
    public static String WEBSITE;
    public static String CALLBACK_URL;
    public static String TransactionStatusURL;
    public static String TransactionURL;
    public static HashMap<String, TreeMap<String, String>> paytm_checksum_detials;
    public static String PAYMENT_DETAILS_URL;
    public static String CREATE_ORDER_URL;
}

