/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.test_question;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import java.sql.ResultSet;

/**
 *
 * @author Akshay
 */
public class TestQuestionMgr {
    public ResultSet loadTestQuestion(String test_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "SELECT\n" +
                        "q.question_id,q.discription_id,ql.language_id,ql.question_bank_id,qb.question,dd.discription_bank_id,ob.option_detail,option_name,\n" +
                        "(SELECT discription FROM discription_bank db where db.discription_bank_id=dd.discription_bank_id) as dis,\n" +
                        "q.section_id,\n" +
                        "(SELECT section_name FROM section_details sd where sd.section_id=q.section_id) as section_name\n" +
                        "  FROM question_details q\n" +
                        "  inner join  test_question_details tqd on tqd.question_id=q.question_id\n" +
                        "  inner join  test_details td on td.test_id=tqd.test_id\n" +
                        "  inner join  test_section ts on ts.test_id=td.test_id\n" +
                        "  inner join question_lang_details ql on q.question_id=ql.question_id\n" +
                        "  inner join question_bank qb on qb.question_bank_id=ql.question_bank_id\n" +
                        "  inner join option_bank ob on ob.question_bank_id=ql.question_bank_id\n" +
                        "  left join discription_details dd on dd.discription_id=q.discription_id and dd.language_id=ql.language_id\n" +
                        "  where td.test_id='"+test_id+"' \n"+                  
                        "  order by tqd.test_question_id asc ";
           config.setRs(config.getStmt().executeQuery(sql));
           return config.getRs();
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                    
    public ResultSet loadAnsSheet(String test_id,String user_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "SELECT\n" +
                        "*," +
                        "(SELECT select_option FROM user_test_question_submit_details utqsd\n" +
                        "inner join user_test_account uta on uta.user_test_id=utqsd.user_test_id\n" +
                        "where uta.test_id=tqd.test_id and uta.user_id='"+user_id+"' and utqsd.question_id=tqd.question_id limit 1) as select_option" +
                        " FROM test_question_details tqd\n" +
                        "where tqd.test_id='"+test_id+"'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           return config.getRs();
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                    
    
    public ResultSet loadSelectedQus(String question_id,String language_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "SELECT\n" +
                        "qb.question,\n" +
                        "ob.option_detail,\n" +
                        "(select discription_id FROM question_details qd where qd.question_id='"+question_id+"' limit 1) as discription_id,\n" +
                        "(select discription_bank_id FROM discription_details dd where dd.discription_id=(select discription_id FROM question_details qd where qd.question_id='"+question_id+"') and ql.language_id='"+language_id+"' limit 1) as discription_bank_id,\n" +
                        "ob.option_name\n" +
                        "FROM question_lang_details ql\n" +
                        "inner join question_bank qb on qb.question_bank_id=ql.question_bank_id\n" +
                        "inner join option_bank ob on ob.question_bank_id=ql.question_bank_id\n" +
                        "where ql.question_id='"+question_id+"' and ql.language_id='"+language_id+"'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           return config.getRs();
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                    
}
