/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.test_section;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.test_section.TestSection;
import java.util.ArrayList;
import org.json.JSONArray;

/**
 *
 * @author Akshay
 */
public class TestSectionMgr {
public boolean insTestSection(ArrayList<TestSection> test_section,JSONArray section_order) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            config.getConn().setAutoCommit(false);        
            String sql = "insert into test_section ("                   
                    + "test_id,"
                    + "section_id"
                    + ")"
                    + " values ((select max(test_id) from test_details),?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            for (int i = 0; i < section_order.length(); i++) {
                config.getPstmt().setString(1, (String)section_order.get(i));
                config.getPstmt().addBatch();
            }
            int x[] = config.getPstmt().executeBatch();
            if (x.length==test_section.size()) {
                config.getConn().commit();
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }    
}
