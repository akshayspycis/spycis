/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.message_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.message_details.MessageDetails;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class MessageDetailsMgr {
    public boolean insMessageDetails(MessageDetails message_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into message_details ("                   
                    + "sender_id,"
                    + "receiver_id,"
                    + "subject,"
                    + "body,"
                    + "msg_data,"
                    + "msg_time)"
                    + " values (?,?,?,?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, message_details.getSender_id());
            config.getPstmt().setString(2, message_details.getReceiver_id());
            config.getPstmt().setString(3, message_details.getSubject());
            config.getPstmt().setString(4, message_details.getBody());
            config.getPstmt().setString(5, message_details.getMsg_data());
            config.getPstmt().setString(6, message_details.getMsg_time());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
   //method to delete MessageDetails in database
   public boolean delMessageDetails(String question_bank_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from message_details where question_bank_id= '"+question_bank_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ResultSet loadMessageDetails(String question_bank_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select option_name,option_detail from message_details where question_bank_id='"+question_bank_id+"'";
           return config.getStmt().executeQuery(sql);
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   public ResultSet loadAnswerBank(String question_bank_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select answer from question_bank where question_bank_id='"+question_bank_id+"'";
           return config.getStmt().executeQuery(sql);
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    

    public boolean updMessageDetailsMgr(ArrayList<MessageDetails> option,String question_bank_id) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            config.getConn().setAutoCommit(false);        
            
            String sql = "update message_details set"                   
                    + " option_detail=? "
                    + " where question_bank_id=? and option_name=?";
            config.setPstmt(config.getConn().prepareStatement(sql));
//            for (MessageDetails optionbank : option) {
//                config.getPstmt().setString(1, optionbank.getOption());
//                config.getPstmt().setString(2, question_bank_id);
//                config.getPstmt().setString(3, optionbank.getOption_name());
//                config.getPstmt().addBatch();
//            }
            int x[] = config.getPstmt().executeBatch();
            if (x.length==option.size()) {
                config.getConn().commit();
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    
}
