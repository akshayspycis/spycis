/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.language_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.language_details.LanguageDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class LanguageDetailsMgr {
            //method to insert coustomerprofile in database
             //method to insert LanguageDetails in database
    public boolean insLanguageDetails(LanguageDetails language_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into language_details ("                   
                    + "language_name)"
                    + " values (?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, language_details.getLanguage_name());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updLanguageDetails(LanguageDetails language_details) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update language_details set"
                    + " language_name = ? "
                    + " where language_id= '"+language_details.getLanguage_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, language_details.getLanguage_name());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete LanguageDetails in database
   public boolean delLanguageDetails(String language_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from language_details where language_id= '"+language_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadLanguage(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<LanguageDetails> rt = new ArrayList<>();
           String sql = "select * from language_details ";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               LanguageDetails r = new LanguageDetails();
               r.setLanguage_id(config.getRs().getString("language_id"));
               r.setLanguage_name(config.getRs().getString("language_name"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   
   public LanguageDetails loadRecentLanguage(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           LanguageDetails rt = new LanguageDetails();
           String sql = "select * from language_details ORDER BY language_id DESC LIMIT 1";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               rt.setLanguage_id(config.getRs().getString("language_id"));
               rt.setLanguage_name(config.getRs().getString("language_name"));
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
    
}
