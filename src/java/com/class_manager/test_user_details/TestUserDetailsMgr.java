/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.test_user_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.test_user_details.TestUserDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class TestUserDetailsMgr {
    public boolean insTestUserDetails(ArrayList<TestUserDetails> test_user_details,String test_id) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {
                this.delTestUserDetails(test_id);
                String sql = "insert into test_user_details ("                   
                        + "user_id,"
                        + "test_id"
                        + ")"
                        + " values (?,?)";
                config.setPstmt(config.getConn().prepareStatement(sql));
                for (int i = 0; i < test_user_details.size(); i++) {
                    config.getPstmt().setString(1, test_user_details.get(i).getUser_id());
                    config.getPstmt().setString(2, test_user_details.get(i).getTest_id());
                    config.getPstmt().addBatch();
                }
                int x[] = config.getPstmt().executeBatch();
                if (x.length==test_user_details.size()) {
                    db.closeConection(config);
                    return true;
                } else {
                    db.closeConection(config);
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    public boolean delTestUserDetails(String test_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from test_user_details where test_id= '"+test_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
    public ArrayList<TestUserDetails> loadTestUserDetails(String test_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select user_id from test_user_details where test_id='"+test_id+"'";
           config.setRs(config.getStmt().executeQuery(sql));
           ArrayList<TestUserDetails> tud =new ArrayList<TestUserDetails>();
           while(config.getRs().next()){
               TestUserDetails rt=new TestUserDetails();
               rt.setUser_id(config.getRs().getString("user_id"));
               tud.add(rt);
           }
           return tud;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                
    
    
}
