/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.test_question_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.test_question_details.TestQuestionDetails;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Akshay
 */
public class TestQuestionDetailsMgr {
    public boolean insTestQuestionDetails(ArrayList<TestQuestionDetails> test_question_details,List<Integer> list) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
         try {   
            config.getConn().setAutoCommit(false);        
               String sql = "insert into test_question_details ("                   
                    + "test_id,"
                    + "question_id,"
                    + "marks,"
                    + "answer"
                    + ")"
                    + " values ((select max(test_id) from test_details),?,?,(SELECT (select answer from question_bank where question_bank_id=q.question_bank_id ) FROM question_lang_details q where q.question_id=? limit 1))";
            config.setPstmt(config.getConn().prepareStatement(sql));
             for (int i = 0; i < list.size(); i++) {
                 for (int j = 0; j < test_question_details.size(); j++) {
                     TestQuestionDetails test=test_question_details.get(j);
                     if(test.getOrder()==list.get(i)){
                        config.getPstmt().setString(1, test.getQuestion_id());
                        config.getPstmt().setString(2, test.getMarks());
                        config.getPstmt().setString(3, test.getQuestion_id());
                        config.getPstmt().addBatch();
                        break;
                     }
                 }
             }
            int x[] = config.getPstmt().executeBatch();
            if (x.length==test_question_details.size()) {
                config.getConn().commit();
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        } 
    }    
}
