/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.test_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.test_details.DefaultTest;
import java.sql.ResultSet;

/**
 *
 * @author Akshay
 */
public class DefaultTestMgr {
    
    public boolean insDefaultTest(DefaultTest default_test) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into default_test ("                   
                    + "test_id,"
                    + "status"
                    + ")"
                    + " values (?,?)";
            
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, default_test.getTest_id());
            config.getPstmt().setString(2, default_test.getStatus());
            
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updDefaultTestVisibilty(String default_test_id,String status) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
        try {
            String sql = "update default_test set"
                        + " status = '"+status
                        + "' where default_test_id= '"+default_test_id+"'";
            config.setPstmt(config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
                if (x>0) {
                    db.closeConection(config);
                    return true;
                } else {
                    db.closeConection(config);
                    return false;
                }
            } catch (Exception ex) {
                db.closeConection(config);
                ex.printStackTrace();
                return false;
            }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete DefaultTest in database
   public boolean delDefaultTest(String default_test_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from default_test where default_test_id= '"+default_test_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ResultSet loadDefaultTest(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select * from default_test order by test_id DESC";
           config.setRs(config.getStmt().executeQuery(sql));
           return config.getRs();
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
}
