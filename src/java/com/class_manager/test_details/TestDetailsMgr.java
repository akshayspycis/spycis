/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.class_manager.test_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.config.GlobalData;
import com.data_manager.test_details.TestDetails;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Akshay
 */
public class TestDetailsMgr {
             //method to insert TestDetails in database

    public boolean insTestDetails(TestDetails test_details) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
            String sql = "insert into test_details ("
                    + "exam_phase_id,"
                    + "category_id,"
                    + "subcategory_id,"
                    + "time,"
                    + "cut_off,"
                    + "e_date,"
                    + "start_time,"
                    + "unique_test_key,"
                    + "test_name,"
                    + "meditory_test_id,"
                    + "visible,"
                    + "negative_ratio,"
                    + "type"
                    + ")"
                    + " values (?,?,?,?,?,?,?,?,?,?,'true',?,?)";

            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, test_details.getExam_phase_id());
            config.getPstmt().setString(2, test_details.getCategory_id());
            config.getPstmt().setString(3, test_details.getSubcategory_id());
            config.getPstmt().setString(4, test_details.getTime());
            config.getPstmt().setString(5, test_details.getCut_off());
            config.getPstmt().setString(6, GlobalData.sdf.format((Date) new SimpleDateFormat("yyyy-MM-dd").parse(test_details.getE_date())));
            config.getPstmt().setString(7, test_details.getStart_time());
            config.getPstmt().setString(8, test_details.getUnique_test_key());
            config.getPstmt().setString(9, test_details.getTest_name());
            config.getPstmt().setString(10, test_details.getMeditory_test_id());
            config.getPstmt().setString(11, test_details.getNegative_ratio());
            config.getPstmt().setString(12, test_details.getType());

            int x = config.getPstmt().executeUpdate();
            if (x > 0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------

    //method to RateList in database 
    public boolean updTestVisibilty(String test_id, String visible) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
            String sql = "update test_details set"
                    + " visible = '" + visible
                    + "' where test_id= '" + test_id + "'";
            config.setPstmt(config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x > 0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------

//   //method to delete TestDetails in database
//   public boolean delTestDetails(String topic_id)
//   {    DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//        try {          
//            String sql = "delete from test_details where topic_id= '"+topic_id+"'";
//            config.setPstmt( config.getConn().prepareStatement(sql));
//            int x = config.getPstmt().executeUpdate();
//            if (x>0) {
//                db.closeConection(config);
//                return true;
//            } else {
//                db.closeConection(config);
//                return false;
//            }
//        } catch (Exception ex) {
//            db.closeConection(config);
//            ex.printStackTrace();
//            return false;
//        }
//   }
    public ResultSet loadTestDetails(String category_id, String subcategory_id) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
            String sql = "select * from test_details where category_id='" + category_id + "' and subcategory_id='" + subcategory_id + "' order by test_id DESC";
            config.setRs(config.getStmt().executeQuery(sql));
            return config.getRs();
        } catch (Exception e) {
            db.closeConection(config);
            e.printStackTrace();
            return null;
        }
    }

    public ResultSet loadAssesmentDetails_Test(String user_id) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
            String sql = "SELECT *,"
                    + "(select count(*) from test_question_details tqd where tqd.test_id=td.test_id ) as no_qus,"
                    + "(select COALESCE(sum(marks),0) from test_question_details tqd where tqd.test_id=td.test_id ) as total_marks,"
                    + "(select user_id from test_user_details tud where tud.test_id=td.test_id and tud.user_id='" + user_id + "' ) as eligibility,"
                    + "(select user_id from user_test_account uta where uta.test_id=td.test_id and uta.user_id='" + user_id + "' limit 1 ) as status ,"
                    + "(select status from checksum_details cd where cd.test_id=td.test_id and cd.user_id='" + user_id + "'  order by checksum_details_id desc limit 1) as payment_status "
                    + "FROM test_details td where td.visible='true' order by td.test_id DESC";
            System.out.println(sql);
            config.setRs(config.getStmt().executeQuery(sql));
            return config.getRs();
        } catch (Exception e) {
            db.closeConection(config);
            e.printStackTrace();
            return null;
        }
    }

    public ResultSet loadUserPerformance(String user_id) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
            String sql = "select\n"
                    + "uta.test_id,"
                    + "(COALESCE(\n"
                    + "      (select\n"
                    + "           COALESCE(sum(marks),0)\n"
                    + "           from test_question_details tqd\n"
                    + "           inner join user_test_question_submit_details utqsd\n"
                    + "				   on tqd.question_id = utqsd.question_id\n"
                    + "           where tqd.test_id=uta.test_id and utqsd.user_test_id=uta.user_test_id and utqsd.select_option=tqd.answer\n"
                    + "				   group by utqsd.user_test_id\n"
                    + "      )\n"
                    + ",0)-\n"
                    + "COALESCE(\n"
                    + "      (select\n"
                    + "           count(*)\n"
                    + "           from test_question_details tqd\n"
                    + "           inner join user_test_question_submit_details utqsd\n"
                    + "				   on tqd.question_id = utqsd.question_id\n"
                    + "           where tqd.test_id=uta.test_id and utqsd.user_test_id=uta.user_test_id and utqsd.select_option!=tqd.answer\n"
                    + "				   group by utqsd.user_test_id\n"
                    + "      )\n"
                    + ",0)/COALESCE((select td.negative_ratio  from test_details td where td.test_id=uta.test_id),0)) as total,\n"
                    + "(select td.test_name  from test_details td where td.test_id=uta.test_id) as test_name,\n"
                    + "(select td.type from test_details td where td.test_id=uta.test_id) as type,\n"
                    + "(select td.unique_test_key  from test_details td where td.test_id=uta.test_id) as unique_test_key\n"
                    + "from user_test_account uta\n"
                    + "inner join user_details ud\n"
                    + "on uta.user_id=ud.user_id\n"
                    + "where uta.user_id='" + user_id + "'";
            System.out.println(sql);
            config.setRs(config.getStmt().executeQuery(sql));
            return config.getRs();
        } catch (Exception e) {
            db.closeConection(config);
            e.printStackTrace();
            return null;
        }
    }

    public ResultSet loadTestDetailsForPayment(String user_id, String test_id) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
//           String sql = "SELECT email,contact_no,td.* FROM login_details ld, test_details td where ld.user_id='"+user_id+"' and td.test_id='"+test_id+"' limit 1";
            String sql = "SELECT email,contact_no,td.* FROM login_details ld, test_details td where ld.user_id='" + user_id + "'  limit 1";
            config.setRs(config.getStmt().executeQuery(sql));
            return config.getRs();
        } catch (Exception e) {
            db.closeConection(config);
            e.printStackTrace();
            return null;
        }
    }

    public ResultSet checkPaymentStatus(String user_id, String test_id) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
            String sql = "SELECT status FROM checksum_details  where user_id='" + user_id + "' and test_id='"+test_id+"' order by checksum_details_id desc limit 1 ";
            System.out.println(sql);
            config.setRs(config.getStmt().executeQuery(sql));
            return config.getRs();
        } catch (Exception e) {
            db.closeConection(config);
            e.printStackTrace();
            return null;
        }
    }

}
