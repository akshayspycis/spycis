/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.test_result;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.user_details.User;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class TestResultMgr {

    public ArrayList loadTestUser(String test_id){
             DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<User> rt = new ArrayList<>();
           String sql = "SELECT *,(select contact_no from login_details l where l.user_id=ud.user_id) as contact_no,(select pic from user_pic_details l where l.user_id=ud.user_id) as pic FROM user_test_account uta inner join user_details ud on ud.user_id=uta.user_id where test_id='"+test_id+"'";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               User r = new User();
               r.setUser_id(config.getRs().getString("user_id"));
               r.setUser_name(config.getRs().getString("user_name"));
               r.setDob(config.getRs().getString("dob"));
               r.setContact_no(config.getRs().getString("contact_no"));
               r.setPic(config.getRs().getString("pic"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
    }
    public ResultSet loadTestResultNumber(String test_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select\n" +
"COALESCE(\n" +
"      (select\n" +
"           COALESCE(sum(marks),0)\n" +
"           from test_question_details tqd\n" +
"           inner join user_test_question_submit_details utqsd\n" +
"				   on tqd.question_id = utqsd.question_id\n" +
"           where tqd.test_id=uta.test_id and utqsd.user_test_id=uta.user_test_id and utqsd.select_option=tqd.answer\n" +
"				   group by utqsd.user_test_id\n" +
"      )\n" +
",0)-\n" +
"COALESCE(\n" +
"      (select\n" +
"           count(*)\n" +
"           from test_question_details tqd\n" +
"           inner join user_test_question_submit_details utqsd\n" +
"				   on tqd.question_id = utqsd.question_id\n" +
"           where tqd.test_id=uta.test_id and utqsd.user_test_id=uta.user_test_id and utqsd.select_option!=tqd.answer\n" +
"				   group by utqsd.user_test_id\n" +
"      )\n" +
",0)/COALESCE((select td.negative_ratio  from test_details td where td.test_id=uta.test_id),0) as total\n" +
",ud.user_id\n" +
",ud.user_name\n" +
",(select ld.email from login_details ld where ld.user_id=uta.user_id) as email\n" +
",(select ld.contact_no from login_details ld where ld.user_id=uta.user_id) as contact_no\n" +
",(select ld.pic from user_pic_details ld where ld.user_id=uta.user_id) as pic\n" +
"\n" +
"from user_test_account uta\n" +
"inner join user_details ud\n" +
"on uta.user_id=ud.user_id\n" +
"where uta.test_id='"+test_id+"'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           return config.getRs();
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                    
    
    public ResultSet loadUserTestDetails(String test_id,String user_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select\n" +
                        "*,\n" +
                        "COALESCE(\n" +
                        "      (select\n" +
                        "           COALESCE(sum(marks),0)\n" +
                        "           from test_question_details tqd\n" +
                        "           where tqd.test_id=uta.test_id\n" +
                        "				   group by tqd.test_id\n" +
                        "      )\n" +
                        ",0) as sum,\n" +
                        "COALESCE(\n" +
                        "      (select\n" +
                        "           COALESCE(sum(marks),0)\n" +
                        "           from test_question_details tqd\n" +
                        "           inner join user_test_question_submit_details utqsd\n" +
                        "				   on tqd.question_id = utqsd.question_id\n" +
                        "           where tqd.test_id=uta.test_id and utqsd.user_test_id=uta.user_test_id and utqsd.select_option=tqd.answer\n" +
                        "				   group by utqsd.user_test_id\n" +
                        "      )\n" +
                        ",0) as obtained," +
                        "IF(td.negative_ratio!='0', COALESCE(\n" +
                        "      (select\n" +
                        "           count(*)\n" +
                        "           from test_question_details tqd\n" +
                        "           inner join user_test_question_submit_details utqsd\n" +
                        "				   on tqd.question_id = utqsd.question_id\n" +
                        "           where tqd.test_id=uta.test_id and utqsd.user_test_id=uta.user_test_id and utqsd.select_option!=tqd.answer\n" +
                        "				   group by utqsd.user_test_id\n" +
                        " )\n" +
                        ",0)/COALESCE((select td.negative_ratio  from test_details td where td.test_id=uta.test_id),0), 0)\n" +
                        "as negative " +
                        "from user_test_account uta\n" +
                        "inner join test_details td\n" +
                        "on uta.test_id=td.test_id\n" +
                        "where td.test_id='"+test_id+"' and uta.user_id='"+user_id+"'";
           System.out.println(sql);       
           config.setRs(config.getStmt().executeQuery(sql));
           return config.getRs();
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                    
    public ResultSet loadResultSummery(String test_id,String user_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select\n" +
                        "question_id,\n" +
                        "answer,\n" +
                        "(select select_option from user_test_question_submit_details utqsd where utqsd.user_test_id=uta.user_test_id and utqsd.question_id=tqd.question_id) as select_option\n" +
                        "from test_question_details tqd\n" +
                        "inner join user_test_account uta\n" +
                        "on uta.test_id=tqd.test_id\n" +
                        "where tqd.test_id='"+test_id+"' and uta.user_id='"+user_id+"'";
           System.out.println(sql);       
           config.setRs(config.getStmt().executeQuery(sql));
           return config.getRs();
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                    
}
    

