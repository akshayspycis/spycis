/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.question_bank;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.config.GlobalData;
import com.data_manager.question_bank.QuestionBank;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class QuestionBankMgr {
        public boolean insQuestionBank(QuestionBank question_bank) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into question_bank ("                   
                    + "question,"
                    + "answer"
                    + ")"
                    + " values (?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, question_bank.getQuestion());
            config.getPstmt().setString(2, question_bank.getAnswer());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace(GlobalData.ps);
            GlobalData.ps.close();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updQuestionBank(QuestionBank question_bank) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update question_bank set"
                    + " question = ? "
                    + " where question_bank_id= '"+question_bank.getQuestion_bank_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, question_bank.getQuestion());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   
   public boolean updAnswerInQuestionBank(QuestionBank question_bank) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update question_bank set"
                    + " answer = ? "
                    + " where question_bank_id= '"+question_bank.getQuestion_bank_id()+"'";
            
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, question_bank.getAnswer());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete QuestionBank in database
   public boolean delQuestionBank(String question_bank_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from question_bank where question_bank_id= '"+question_bank_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadQuestionBank(String category_id,String subcategory_id,String section_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<QuestionBank> rt = new ArrayList<>();
           String sql = "select * from question_bank where category_id='"+category_id+"' and subcategory_id='"+subcategory_id+"' and section_id='"+section_id+"'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               QuestionBank r = new QuestionBank();
               r.setQuestion_bank_id(config.getRs().getString("question_bank_id"));
               r.setQuestion(config.getRs().getString("question"));
               r.setAnswer(config.getRs().getString("answer"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   
   public QuestionBank loadRecentTopic(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           QuestionBank rt = new QuestionBank();
           String sql = "select * from question_bank ORDER BY question_bank_id DESC LIMIT 1";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               rt.setQuestion_bank_id(config.getRs().getString("question_bank_id"));
               rt.setQuestion(config.getRs().getString("question"));
               rt.setAnswer(config.getRs().getString("answer"));
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                
}
