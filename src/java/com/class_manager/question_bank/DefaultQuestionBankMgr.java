/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.question_bank;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.question_bank.DefaultQuestionBank;
import java.sql.ResultSet;

/**
 *
 * @author Akshay
 */
public class DefaultQuestionBankMgr {
    public boolean insDefaultQuestionBank(DefaultQuestionBank default_question_bank) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into default_question_bank ("                   
                    + "section_id,"
                    + "topic_id,"
                    + "question_id,"
                    + "status"
                    + ")"
                    + " values (?,?,?,?)";
            
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, default_question_bank.getSection_id());
            config.getPstmt().setString(2, default_question_bank.getTopic_id());
            config.getPstmt().setString(3, default_question_bank.getQuestion_id());
            config.getPstmt().setString(5, default_question_bank.getStatus());
            
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   
   //method to delete DefaultQuestionBank in database
   public boolean delDefaultQuestionBank(String default_question_bank_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from default_question_bank where default_question_bank_id= '"+default_question_bank_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ResultSet loadDefaultQuestionBank(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select * from default_question_bank software_id="+software_id+" order by software_id DESC";
           config.setRs(config.getStmt().executeQuery(sql));
           return config.getRs();
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                
}
