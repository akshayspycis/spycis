/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.section_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.section_details.SectionDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class SectionDetailsMgr {
             //method to insert SectionDetails in database
    public boolean insSectionDetails(SectionDetails section_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into section_details ("                   
                    + "category_id,"
                    + "subcategory_id,"
                    + "section_name"
                    + ")"
                    + " values (?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, section_details.getCategory_id());
            config.getPstmt().setString(2, section_details.getSubcategory_id());
            config.getPstmt().setString(3, section_details.getSection_name());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updSectionDetails(SectionDetails section_details) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update section_details set"
                    + " section_name = ? "
                    + " where section_id= '"+section_details.getSection_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, section_details.getSection_name());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete SectionDetails in database
   public boolean delSectionDetails(String section_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from section_details where section_id= '"+section_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadSection(String category_id,String subcategory_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<SectionDetails> rt = new ArrayList<>();
           String sql = "select * from section_details where category_id='"+category_id+"' and subcategory_id='"+subcategory_id+"'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               SectionDetails r = new SectionDetails();
               r.setSection_id(config.getRs().getString("section_id"));
               r.setSection_name(config.getRs().getString("section_name"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   
   public SectionDetails loadRecentSection(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           SectionDetails rt = new SectionDetails();
           String sql = "select * from section_details ORDER BY section_id DESC LIMIT 1";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               
               rt.setSection_id(config.getRs().getString("section_id"));
               rt.setCategory_id(config.getRs().getString("category_id"));
               rt.setSubcategory_id(config.getRs().getString("subcategory_id"));
               rt.setSection_name(config.getRs().getString("section_name"));
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }            

    public ArrayList<SectionDetails> loadSection_Test() {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<SectionDetails> rt = new ArrayList<>();
           String sql = "select * from section_details where category_id='32'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               SectionDetails r = new SectionDetails();
               r.setSection_id(config.getRs().getString("section_id"));
               r.setSection_name(config.getRs().getString("section_name"));
               rt.add(r);
           }
           return rt;

       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
    }
}
