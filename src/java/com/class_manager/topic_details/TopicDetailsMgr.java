/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.topic_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.topic_details.TopicDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class TopicDetailsMgr {
             //method to insert TopicDetails in database
    public boolean insTopicDetails(TopicDetails topic_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into topic_details ("                   
                    + "section_id,"
                    + "category_id,"
                    + "subcategory_id,"
                    + "topic_name"
                    + ")"
                    + " values (?,?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, topic_details.getSection_id());
            config.getPstmt().setString(2, topic_details.getCategory_id());
            config.getPstmt().setString(3, topic_details.getSubcategory_id());
            config.getPstmt().setString(4, topic_details.getTopic_name());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updTopicDetails(TopicDetails topic_details) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update topic_details set"
                    + " topic_name = ? "
                    + " where topic_id= '"+topic_details.getTopic_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, topic_details.getTopic_name());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete TopicDetails in database
   public boolean delTopicDetails(String topic_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from topic_details where topic_id= '"+topic_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadTopicDetails(String category_id,String subcategory_id,String section_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<TopicDetails> rt = new ArrayList<>();
           String sql = "select * from topic_details where category_id='"+category_id+"' and subcategory_id='"+subcategory_id+"' and section_id='"+section_id+"'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               TopicDetails r = new TopicDetails();
               r.setTopic_id(config.getRs().getString("topic_id"));
               r.setTopic_name(config.getRs().getString("topic_name"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   
   public TopicDetails loadRecentTopic(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           TopicDetails rt = new TopicDetails();
           String sql = "select * from topic_details ORDER BY topic_id DESC LIMIT 1";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               
               rt.setSection_id(config.getRs().getString("section_id"));
               rt.setCategory_id(config.getRs().getString("category_id"));
               rt.setSubcategory_id(config.getRs().getString("subcategory_id"));
               rt.setTopic_id(config.getRs().getString("topic_id"));
               rt.setTopic_name(config.getRs().getString("topic_name"));
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                
}
