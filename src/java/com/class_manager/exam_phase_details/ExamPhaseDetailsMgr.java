/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.exam_phase_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.exam_phase_details.ExamPhaseDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class ExamPhaseDetailsMgr {
            //method to insert coustomerprofile in database
    public boolean insExamPhaseDetails(ExamPhaseDetails exam_phase_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into exam_phase_details ("                   
                    + "exam_phase_name)"
                    + " values (?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, exam_phase_details.getExam_phase_name());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updExamPhaseDetails(ExamPhaseDetails exam_phase_details) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update exam_phase_details set"
                    + " exam_phase_name = ? "
                    + " where exam_phase_id= '"+exam_phase_details.getExam_phase_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, exam_phase_details.getExam_phase_name());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete ExamPhaseDetails in database
   public boolean delExamPhaseDetails(String exam_phase_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from exam_phase_details where exam_phase_id= '"+exam_phase_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadExamPhase(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<ExamPhaseDetails> rt = new ArrayList<>();
           String sql = "select * from exam_phase_details ";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               ExamPhaseDetails r = new ExamPhaseDetails();
               r.setExam_phase_id(config.getRs().getString("exam_phase_id"));
               r.setExam_phase_name(config.getRs().getString("exam_phase_name"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   
   public ExamPhaseDetails loadRecentExamPhase(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ExamPhaseDetails rt = new ExamPhaseDetails();
           String sql = "select * from exam_phase_details ORDER BY exam_phase_id DESC LIMIT 1";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               rt.setExam_phase_id(config.getRs().getString("exam_phase_id"));
               rt.setExam_phase_name(config.getRs().getString("exam_phase_name"));
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
}
