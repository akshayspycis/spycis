/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.user_test_question_submit_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.user_test_question_submit_details.UserTestQuestion_SubmitDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class UserTestQuestion_SubmitDetailsMgr {    
    public boolean insUserTestQuestion_SubmitDetails(ArrayList<UserTestQuestion_SubmitDetails> user_test_question_submit_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
//            config.getConn().setAutoCommit(true);        
            String sql = "insert into user_test_question_submit_details ("                   
                    + "user_test_id,"
                    + "question_id,"
                    + "select_option"
                    + ")"
                    + " values ((select max(user_test_id) from user_test_account),?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            for (UserTestQuestion_SubmitDetails testSection : user_test_question_submit_details) {
                config.getPstmt().setString(1, testSection.getQuestion_id());
                config.getPstmt().setString(2, testSection.getSelect_option());
                config.getPstmt().addBatch();
                System.out.println("a");
            }
            int x[] = config.getPstmt().executeBatch();
            System.out.println(x.length);
            if (x.length==user_test_question_submit_details.size()) {
//                config.getConn().commit();
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }        
}
