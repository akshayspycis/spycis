/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.question_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.question_details.QuestionDetails;
import java.sql.ResultSet;
import javax.naming.spi.DirStateFactory;

/**
 *
 * @author Akshay
 */
public class QuestionDetailsMgr {
             //method to insert QuestionDetails in database
    public boolean insQuestionDetails(QuestionDetails question_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into question_details ("                   
                    + "question_id,"
                    + "topic_id,"
                    + "section_id,"
                    + "category_id,"
                    + "subcategory_id,"
                    + "discription_id"
                    + ")"
                    + " values (?,?,?,?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, question_details.getQuestion_id());
            config.getPstmt().setString(2, question_details.getTopic_id());
            config.getPstmt().setString(3, question_details.getSection_id());
            config.getPstmt().setString(4, question_details.getCategory_id());
            config.getPstmt().setString(5, question_details.getSubcategory_id());
            config.getPstmt().setString(6, question_details.getDiscription_id());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
//   public boolean updQuestionDetails(QuestionDetails question_details) {
//       DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase(); 
//       try {
//            String sql = "update question_details set"
//                    + " topic_name = ? "
//                    + " where topic_id= '"+question_details.getTopic_id()+"'";
//            
//           config.setPstmt(config.getConn().prepareStatement(sql));
//            config.getPstmt().setString(1, question_details.getTopic_name());
//            int x = config.getPstmt().executeUpdate();
//            
//            if (x>0) {
//                db.closeConection(config);
//                return true;
//            } else {
//                db.closeConection(config);
//                return false;
//            }
//        } catch (Exception ex) {
//            db.closeConection(config);
//            ex.printStackTrace();
//            return false;
//        }
//    }
//   //----------------------------------------------------------------------------------------------
//   
//   //method to delete QuestionDetails in database
//   public boolean delQuestionDetails(String topic_id)
//   {    DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//        try {          
//            String sql = "delete from question_details where topic_id= '"+topic_id+"'";
//            config.setPstmt( config.getConn().prepareStatement(sql));
//            int x = config.getPstmt().executeUpdate();
//            if (x>0) {
//                db.closeConection(config);
//                return true;
//            } else {
//                db.closeConection(config);
//                return false;
//            }
//        } catch (Exception ex) {
//            db.closeConection(config);
//            ex.printStackTrace();
//            return false;
//        }
//   }
//   
   public ResultSet loadQuestion(String category_id,String subcategory_id,String section_id,String topic_id,String language_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "SELECT \n" +
                        "q.question_id,\n" +
                        "q.discription_id,\n" +
                        "ql.question_bank_id,\n" +
                        "qb.question,\n" +
                        "dd.discription_bank_id \n" +
                        "FROM question_details q\n" +
                        "inner join question_lang_details ql on q.question_id=ql.question_id\n" +
                        "inner join question_bank qb on qb.question_bank_id=ql.question_bank_id\n" +
                        "left join discription_details dd on dd.discription_id=q.discription_id and dd.language_id=ql.language_id\n" +
                        "where q.category_id='"+category_id+"' and q.subcategory_id ='"+subcategory_id+"' and q.section_id='"+section_id+"' and q.topic_id='"+topic_id+"' and ql.language_id='"+language_id+"'\n" +
                        "order by q.question_id asc";
           System.out.println(sql);
           return config.getStmt().executeQuery(sql);
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
//   
//   public QuestionDetails loadRecentTopic(){
//        DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//       try {
//           QuestionDetails rt = new QuestionDetails();
//           String sql = "select * from question_details ORDER BY topic_id DESC LIMIT 1";
//           config.setRs(config.getStmt().executeQuery(sql));
//           while(config.getRs().next()){
//               
//               rt.setSection_id(config.getRs().getString("section_id"));
//               rt.setCategory_id(config.getRs().getString("category_id"));
//               rt.setSubcategory_id(config.getRs().getString("subcategory_id"));
//               rt.setTopic_id(config.getRs().getString("topic_id"));
//               rt.setTopic_name(config.getRs().getString("topic_name"));
//           }
//           return rt;
//       } catch (Exception e) {
//           db.closeConection(config);
//           e.printStackTrace();
//           return null;
//       }
//   }                    
     public long loadMaxId(){
         long id=0;
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
     String sql = "SELECT max(question_id) as question_id FROM question_details q";

           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               if(config.getRs().getString("question_id")!=null){
                   id=Long.parseLong(config.getRs().getString("question_id"));
               }
           }
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           
       }
       return id;
   }                    
     
}
    
