/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package com.class_manager.category_details;
import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.category_details.CategoryDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class CategoryDetailsMgr {
        //method to insert coustomerprofile in database
             //method to insert CategoryDetails in database
    public boolean insCategoryDetails(CategoryDetails category_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into category_details ("                   
                    + "category_name,"
                    + "user_id"
                    + ")"
                    + " values (?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, category_details.getCategory_name());
            config.getPstmt().setString(2, category_details.getUser_id());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updCategoryDetails(CategoryDetails category_details) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update category_details set"
                    + " category_name = ? "
                    + " where category_id= '"+category_details.getCategory_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, category_details.getCategory_name());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete CategoryDetails in database
   public boolean delCategoryDetails(String category_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from category_details where category_id= '"+category_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadCategory(String user_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<CategoryDetails> rt = new ArrayList<>();
           String sql = "select * from category_details where user_id="+user_id;
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               CategoryDetails r = new CategoryDetails();
               r.setCategory_id(config.getRs().getString("category_id"));
               r.setCategory_name(config.getRs().getString("category_name"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   
   public CategoryDetails loadRecentCategory(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           CategoryDetails rt = new CategoryDetails();
           String sql = "select * from category_details ORDER BY category_id DESC LIMIT 1";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               rt.setCategory_id(config.getRs().getString("category_id"));
               rt.setCategory_name(config.getRs().getString("category_name"));
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
}
