/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.mocktest_details;

import com.class_manager.software_details.SoftwareBasicCofigurationMgr;
import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.mocktest_details.MocktestFavSection;
import com.data_manager.software_details.SoftwareBasicCofiguration;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class MocktestFavSectionMgr {
    public boolean insMocktestFavSection(ArrayList<MocktestFavSection> mocktest_fav_section) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        String software_id="";
        try {   
            String sql = "insert into mocktest_fav_section ("                   
                    + "section_id,"
                    + "category_id,"
                    + "subcategory_id,"
                    + "software_id"
                    + ")"
+ " values (?,(select category_id from section_details where section_id=?),(select subcategory_id from section_details where section_id=?),?)";
            
            config.setPstmt(config.getConn().prepareStatement(sql));
            for (int i = 0; i < mocktest_fav_section.size(); i++) {
                if(software_id=="")software_id=mocktest_fav_section.get(i).getSoftware_id();
                config.getPstmt().setString(1, mocktest_fav_section.get(i).getSection_id());
                config.getPstmt().setString(2, mocktest_fav_section.get(i).getSection_id());
                config.getPstmt().setString(3, mocktest_fav_section.get(i).getSection_id());
                config.getPstmt().setString(4, mocktest_fav_section.get(i).getSoftware_id());
                config.getPstmt().addBatch();
            }
            int x[] = config.getPstmt().executeBatch();
            if (x.length==mocktest_fav_section.size()) {
                SoftwareBasicCofiguration sbc = new SoftwareBasicCofiguration();
                sbc.setSoftware_basic_cofiguration_id("4");
                sbc.setSoftware_id(software_id);
                sbc.setStatus("true");
                if(new SoftwareBasicCofigurationMgr().updSoftwareBasicCofiguration(sbc)){
                    db.closeConection(config);
                    return true;
                }else{
                    db.closeConection(config);
                    return false;
                }
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   
   //method to delete MocktestFavSection in database
   public boolean delMocktestFavSection(String mocktest_fav_section_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from mocktest_fav_section where mocktest_fav_section_id= '"+mocktest_fav_section_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public JSONObject loadFavSection(String software_id,String category_id,String subcategory_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select *,(select section_name from section_details sd where sd.section_id=mfs.section_id) as section_name from mocktest_fav_section mfs where mfs.software_id='"+software_id+"' and mfs.category_id='"+category_id+"' and mfs.subcategory_id='"+subcategory_id+"'";
           config.setRs(config.getStmt().executeQuery(sql));
           JSONObject mocktest_fav_category =null; 
           while(config.getRs().next()){
               if(mocktest_fav_category==null)mocktest_fav_category=new JSONObject();
               JSONObject j = new JSONObject();
               j.put("mocktest_fav_section_id",config.getRs().getString("mocktest_fav_section_id"));
               j.put("section_id",config.getRs().getString("section_id"));
               j.put("section_name",config.getRs().getString("section_name"));
               mocktest_fav_category.put(config.getRs().getString("section_id"),j);
           }
           return mocktest_fav_category;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
}
