/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.mocktest_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.mocktest_details.MocktestQuestionBank;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class SoftwareQuestionBankMgr {
    public boolean insSoftwareQuestionBank(MocktestQuestionBank software_question_bank) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_question_bank ("                   
                    + "software_id,"
                    + "question_id,"
                    + "date"
                    + ")"
                    + " values (?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_question_bank.getSoftware_id());
            config.getPstmt().setString(2, software_question_bank.getQuestion_id());
            config.getPstmt().setString(3, software_question_bank.getDate());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updSoftwareQuestionBank(MocktestQuestionBank software_question_bank) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update software_question_bank set"
                    + " status = ? "
                    + " where software_question_bank_id= '"+software_question_bank.getMocktest_question_bank_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
            //config.getPstmt().setString(1, software_question_bank.getStatus());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete SoftwareQuestionBank in database
   public boolean delSoftwareQuestionBank(String software_question_bank_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from software_question_bank where software_question_bank_id= '"+software_question_bank_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadSection(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<MocktestQuestionBank> rt = new ArrayList<>();
           String sql = "select * from software_question_bank where software_id='"+software_id+"' and status='true'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               MocktestQuestionBank r = new MocktestQuestionBank();
               r.setMocktest_question_bank_id(config.getRs().getString("software_question_bank_id"));
               r.setSoftware_id(config.getRs().getString("software_id"));
               r.setQuestion_id(config.getRs().getString("question_id"));
               r.setDate(config.getRs().getString("date"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }     
      
}
