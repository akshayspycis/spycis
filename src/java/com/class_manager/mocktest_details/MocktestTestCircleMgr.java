/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.mocktest_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.mocktest_details.MocktestTestCircle;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class MocktestTestCircleMgr {
    public boolean insMocktestTestCircle(MocktestTestCircle mocktest_test_circle) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into mocktest_test_circle ("                   
                    + "software_id,"
                    + "user_id,"
                    + "status,"
                    + "pay_by_user_status"
                    + ")"
                    + " values (?,?,'true','false')";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, mocktest_test_circle.getSoftware_id());
            config.getPstmt().setString(2, mocktest_test_circle.getUser_id());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updMocktestTestCircle(MocktestTestCircle mocktest_test_circle) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update mocktest_test_circle set"
                    + " status = ? "
                    + " where mocktest_test_circle_id= '"+mocktest_test_circle.getMocktest_test_circle_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, mocktest_test_circle.getStatus());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete MocktestTestCircle in database
   public boolean delMocktestTestCircle(String mocktest_test_circle_id){    
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from mocktest_test_circle where mocktest_test_circle_id= '"+mocktest_test_circle_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadMocktestTestCircle(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<MocktestTestCircle> rt = new ArrayList<>();
           String sql = "select * from mocktest_test_circle where software_id='"+software_id+"' and status='true'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               MocktestTestCircle r = new MocktestTestCircle();
               r.setSoftware_id(config.getRs().getString("software_id"));
               r.setUser_id(config.getRs().getString("user_id"));
               r.setStatus(config.getRs().getString("status"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }        
}
