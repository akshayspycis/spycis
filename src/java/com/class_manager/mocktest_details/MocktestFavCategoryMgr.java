/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.mocktest_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.mocktest_details.MocktestFavCategory;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class MocktestFavCategoryMgr {
//method to insert coustomerprofile in database
             //method to insert MocktestFavCategory in database
    public boolean insMocktestFavCategory(ArrayList<MocktestFavCategory> mocktest_fav_category) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into mocktest_fav_category ("                   
                    + "category_id,"
                    + "software_id)"
                    + " values (?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            for (int i = 0; i < mocktest_fav_category.size(); i++) {
                config.getPstmt().setString(1, mocktest_fav_category.get(i).getCategory_id());
                config.getPstmt().setString(2, mocktest_fav_category.get(i).getSoftware_id());
                config.getPstmt().addBatch();
            }
            int x[] = config.getPstmt().executeBatch();
            if (x.length==mocktest_fav_category.size()) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   
   //method to delete MocktestFavCategory in database
   public boolean delMocktestFavCategory(String category_id){    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from mocktest_fav_category where category_id= '"+category_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public JSONObject loadFavCategory(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select *,(select category_name from category_details where category_id=mfc.category_id) as category_name from mocktest_fav_category mfc where mfc.software_id='"+software_id+"'";
           config.setRs(config.getStmt().executeQuery(sql));
           JSONObject mocktest_fav_category =null; 
           while(config.getRs().next()){
               if(mocktest_fav_category==null)mocktest_fav_category=new JSONObject();
               JSONObject j = new JSONObject();
               j.put("mocktest_fav_category_id",config.getRs().getString("mocktest_fav_category_id"));
               j.put("category_id",config.getRs().getString("category_id"));
               j.put("category_name",config.getRs().getString("category_name"));
               mocktest_fav_category.put(config.getRs().getString("category_id"),j);
           }
           return mocktest_fav_category;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   
   public MocktestFavCategory loadRecentCategory(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           MocktestFavCategory rt = new MocktestFavCategory();
           String sql = "select * from mocktest_fav_category ORDER BY category_id DESC LIMIT 1";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               rt.setCategory_id(config.getRs().getString("category_id"));
               rt.setSoftware_id(config.getRs().getString("category_name"));
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }        
}
