/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.mocktest_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.mocktest_details.MocktestFavTopic;
import java.sql.ResultSet;

/**
 *
 * @author Akshay
 */
public class SoftwareFavTopicMgr {
    public boolean insSoftwareFavTopic(MocktestFavTopic software_fav_topic) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_fav_topic ("                   
                    + "software_id,"
                    + "section_id,"
                    + "topic_id"
                    + ")"
                    + " values (?,?,?)";
            
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_fav_topic.getSoftware_id());
            config.getPstmt().setString(2, software_fav_topic.getSection_id());
            config.getPstmt().setString(3, software_fav_topic.getTopic_id());
            
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   
   //method to delete SoftwareFavTopic in database
   public boolean delSoftwareFavTopic(String software_fav_topic_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from software_fav_topic where software_fav_topic_id= '"+software_fav_topic_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ResultSet loadSoftwareFavTopic(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select * from software_fav_topic software_id="+software_id+" order by software_id DESC";
           config.setRs(config.getStmt().executeQuery(sql));
           return config.getRs();
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }            
}
