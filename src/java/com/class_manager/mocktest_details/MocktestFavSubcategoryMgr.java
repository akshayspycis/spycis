/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.mocktest_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.mocktest_details.MocktestFavSubcategory;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class MocktestFavSubcategoryMgr {
             //method to insert MocktestFavSubcategory in database
    public boolean insMocktestFavSubcategory(ArrayList<MocktestFavSubcategory> mocktest_fav_subcategory) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into mocktest_fav_subcategory ("                   
                    + "subcategory_id,"
                    + "category_id,"
                    + "software_id)"
                    + " values (?,(select category_id from subcategory_details where subcategory_id=?),?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            for (int i = 0; i < mocktest_fav_subcategory.size(); i++) {
                config.getPstmt().setString(1, mocktest_fav_subcategory.get(i).getSubcategory_id());
                config.getPstmt().setString(2, mocktest_fav_subcategory.get(i).getSubcategory_id());
                config.getPstmt().setString(3, mocktest_fav_subcategory.get(i).getSoftware_id());
                config.getPstmt().addBatch();
            }
            int x[] = config.getPstmt().executeBatch();
            if (x.length==mocktest_fav_subcategory.size()) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   
   //method to delete MocktestFavSubcategory in database
   public boolean delMocktestFavSubcategory(String category_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from mocktest_fav_subcategory where category_id= '"+category_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public JSONObject loadFavSubcategory(String software_id,String category_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<MocktestFavSubcategory> rt = new ArrayList<>();
           String sql = "select *,(select subcategory_name from subcategory_details sd where sd.subcategory_id=mfs.subcategory_id) as subcategory_name from mocktest_fav_subcategory mfs where mfs.software_id='"+software_id+"' and mfs.category_id='"+category_id+"'";
           config.setRs(config.getStmt().executeQuery(sql));
           JSONObject mocktest_fav_category =null; 
           while(config.getRs().next()){
               if(mocktest_fav_category==null)mocktest_fav_category=new JSONObject();
               JSONObject j = new JSONObject();
               j.put("mocktest_fav_subcategory_id",config.getRs().getString("mocktest_fav_subcategory_id"));
               j.put("subcategory_id",config.getRs().getString("subcategory_id"));
               j.put("subcategory_name",config.getRs().getString("subcategory_name"));
               mocktest_fav_category.put(config.getRs().getString("subcategory_id"),j);
           }
           return mocktest_fav_category;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
}
