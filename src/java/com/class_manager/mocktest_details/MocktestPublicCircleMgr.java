/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.mocktest_details;

import com.class_manager.user_details.UserRequestMgr;
import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.mocktest_details.MocktestPublicCircle;
import com.data_manager.user_details.UserRequest;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class MocktestPublicCircleMgr {
    public boolean insMocktestPublicCircle(MocktestPublicCircle mocktest_public_circle,String user_request_id) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into mocktest_public_circle ("                   
                    + "software_id,"
                    + "user_id,"
                    + "status"
                    + ")"
                    + " values (?,?,'true')";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, mocktest_public_circle.getSoftware_id());
            config.getPstmt().setString(2, mocktest_public_circle.getUser_id());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                UserRequest user_request= new UserRequest();
                user_request.setUser_request_id(user_request_id);
                user_request.setStatus("done");
                if (new UserRequestMgr().updUserRequest(user_request)) {
                    db.closeConection(config);
                    return true;
                } else {
                    db.closeConection(config);
                    return false;
                }
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updMocktestPublicCircle(MocktestPublicCircle mocktest_public_circle) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update mocktest_public_circle set"
                    + " status = ? "
                    + " where mocktest_public_circle_id= '"+mocktest_public_circle.getMocktest_public_circle_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, mocktest_public_circle.getStatus());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete MocktestPublicCircle in database
   public boolean delMocktestPublicCircle(String mocktest_public_circle_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from mocktest_public_circle where mocktest_public_circle_id= '"+mocktest_public_circle_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadSection(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<MocktestPublicCircle> rt = new ArrayList<>();
           String sql = "select * from mocktest_public_circle where software_id='"+software_id+"' and status='true'";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               MocktestPublicCircle r = new MocktestPublicCircle();
               r.setSoftware_id(config.getRs().getString("software_id"));
               r.setUser_id(config.getRs().getString("user_id"));
               r.setStatus(config.getRs().getString("status"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }     
   
   public JSONObject loadAllMocktestPublicCircle(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {
           ArrayList<MocktestPublicCircle> rt = new ArrayList<>();
           String sql = "select *,"
                   + "(select user_name from user_details where user_id=mpc.user_id) as user_name,"
                   + "(select pay_by_user_status from mocktest_test_circle where user_id=mpc.user_id order by mocktest_test_circle_id limit 1) as pay_by_user_status,"
                   + "(select pic from user_pic_details where user_id=mpc.user_id) as pic"
                   + " from mocktest_public_circle mpc where software_id='"+software_id+"' and status='true'";
           config.setRs(config.getStmt().executeQuery(sql));
            System.out.println(sql);
           JSONObject software =null; 
           while(config.getRs().next()){
               if(software==null)software=new JSONObject();
               JSONObject j = new JSONObject();
               j.put("mocktest_public_circle_id",config.getRs().getString("mocktest_public_circle_id"));
               j.put("software_id",config.getRs().getString("software_id"));
               j.put("user_id",config.getRs().getString("user_id"));
               j.put("status",config.getRs().getString("status"));
               j.put("user_name",config.getRs().getString("user_name"));
               j.put("pic",config.getRs().getString("pic"));
               j.put("pay_by_user_status",config.getRs().getString("pay_by_user_status"));
               software.put(config.getRs().getString("mocktest_public_circle_id"),j);
           }
           return software;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }     
  
}
