/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.mocktest_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.mocktest_details.MocktestBatchList;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class MockTestBatchListMgr {
    public boolean insMockTestBatchList(MocktestBatchList mocktest_batch_list) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into mocktest_batch_list ("                   
                    + "software_id,"
                    + "mocktest_batch_name,"
                    + "status"
                    + ")"
                    + " values (?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, mocktest_batch_list.getSoftware_id());
            config.getPstmt().setString(2, mocktest_batch_list.getMocktest_batch_name());
            config.getPstmt().setString(3, mocktest_batch_list.getStatus());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   
   //----------------------------------------------------------------------------------------------
   
   //method to delete MockTestBatchList in database
   public boolean delMockTestBatchList(String mocktest_batch_list_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from mocktest_batch_list where mocktest_batch_list_id= '"+mocktest_batch_list_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadMockTestBatchList(String category_id,String subcategory_id,String section_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<MocktestBatchList> rt = new ArrayList<>();
           String sql = "select * from mocktest_batch_list where category_id='"+category_id+"' and subcategory_id='"+subcategory_id+"' and section_id='"+section_id+"'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               MocktestBatchList r = new MocktestBatchList();
               r.setMocktest_batch_list_id(config.getRs().getString("mocktest_batch_list_id"));
               r.setMocktest_batch_name(config.getRs().getString("user_id"));
               r.setStatus(config.getRs().getString("status"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }        
}
