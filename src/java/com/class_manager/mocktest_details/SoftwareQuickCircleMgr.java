/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.mocktest_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.mocktest_details.MocktestQuickCircle;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class SoftwareQuickCircleMgr {
    public boolean insSoftwareQuickCircle(MocktestQuickCircle software_quick_circle) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_quick_circle ("                   
                    + "software_batch_list_id,"
                    + "user_id"
                    + ")"
                    + " values (?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_quick_circle.getMocktest_batch_list_id());
            config.getPstmt().setString(2, software_quick_circle.getUser_id());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   
   //----------------------------------------------------------------------------------------------
   
   //method to delete SoftwareQuickCircle in database
   public boolean delSoftwareQuickCircle(String software_quick_circle_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from software_quick_circle where software_quick_circle_id= '"+software_quick_circle_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadSoftwareQuickCircle(String category_id,String subcategory_id,String section_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<MocktestQuickCircle> rt = new ArrayList<>();
           String sql = "select * from software_quick_circle where category_id='"+category_id+"' and subcategory_id='"+subcategory_id+"' and section_id='"+section_id+"'";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               MocktestQuickCircle r = new MocktestQuickCircle();
               r.setMocktest_quick_circle_id(config.getRs().getString("software_quick_circle_id"));
               r.setUser_id(config.getRs().getString("user_id"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
}
