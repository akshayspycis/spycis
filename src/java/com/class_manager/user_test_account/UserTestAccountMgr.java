/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.user_test_account;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.user_test_account.UserTestAccount;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class UserTestAccountMgr {
    public boolean insUserTestAccount(UserTestAccount user_test_account) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into user_test_account ("                   
                    + "user_id,"
                    + "test_id,"
                    + "active_time,"
                    + "leave_time,"
                    + "test_date)"
                    + " values (?,?,?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, user_test_account.getUser_id());
            config.getPstmt().setString(2, user_test_account.getTest_id());
            config.getPstmt().setString(3, user_test_account.getActive_time());
            config.getPstmt().setString(4, user_test_account.getLeave_time());
            config.getPstmt().setString(5, user_test_account.getTest_date());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
}
