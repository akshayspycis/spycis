/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.discription_bank;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.config.GlobalData;
import com.data_manager.discription_bank.DiscriptionBank;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class DiscriptionBankMgr {
             //method to insert DiscriptionBank in database
    public boolean insDiscriptionBank(DiscriptionBank discription_bank) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into discription_bank (discription)"
                    + " values (?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, discription_bank.getDiscription());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace(GlobalData.ps);
            GlobalData.ps.close();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updDiscriptionBank(DiscriptionBank discription_bank) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update discription_bank set"
                    + " discription = ? "
                    + " where discription_bank_id= '"+discription_bank.getDiscription_bank_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, discription_bank.getDiscription());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete DiscriptionBank in database
   public boolean delDiscriptionBank(String discription_bank_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from discription_bank where discription_bank_id= '"+discription_bank_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public DiscriptionBank loadDiscription(String discription_bank_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           DiscriptionBank rt = new DiscriptionBank();
           String sql = "select * from discription_bank where discription_bank_id='"+discription_bank_id+"'";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               rt.setDiscription(config.getRs().getString("discription"));
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }        
}
