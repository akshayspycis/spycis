/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.discription_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.discription_details.DiscriptionDetails;

/**
 *
 * @author Akshay
 */
public class DiscriptionDetailsMgr {
             //method to insert DiscriptionDetails in database
    public boolean insDiscriptionDetails(DiscriptionDetails discription_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into discription_details ("                   
                    + "discription_id,"
                    + "discription_bank_id,"
                    + "language_id)"
                    + " values (?,(select max(discription_bank_id) from discription_bank),?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, discription_details.getDiscription_id());
            config.getPstmt().setString(2, discription_details.getLanguage_id());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
   //method to delete DiscriptionDetails in database
   public boolean delDiscriptionDetails(String discription_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from discription_details where discription_id= '"+discription_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
public long loadMaxId(){
         long id=0;
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
                      String sql = "SELECT max(discription_id) as discription_id FROM discription_details d";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               if(config.getRs().getString("discription_id")!=null){
                   id=Integer.parseInt(config.getRs().getString("discription_id"));
               }
           }
       }catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
       }
       return id;
   }    
}
