/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.login_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.login_details.LoginDetails;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class LoginDetailsMgr {
    public boolean insLoginDetails(LoginDetails login_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into login_details ("                   
                    + "user_id,"
                    + "email,"
                    + "contact_no,"
                    + "password,"
                    + "user_type"
                    + ")"
                    + " values ((select max(user_id) from user_details),?,?,?,'user')";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, login_details.getEmail());
            config.getPstmt().setString(2, login_details.getContact_no());
            config.getPstmt().setString(3, login_details.getPassword());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    

   public boolean updLoginDetails(LoginDetails login_details) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update login_details set"
                    + " email = ? ,"
                    + " contact_no = ?"
                    + " where user_id= '"+login_details.getUser_id()+"'";
            
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, login_details.getEmail());
            config.getPstmt().setString(2, login_details.getContact_no());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete LoginDetails in database
//   public boolean delLoginDetails(String user_id){    DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//        try {          
//            String sql = "delete from login_details where user_id= '"+user_id+"'";
//            config.setPstmt( config.getConn().prepareStatement(sql));
//            int x = config.getPstmt().executeUpdate();
//            if (x>0) {
//                db.closeConection(config);
//                return true;
//            } else {
//                db.closeConection(config);
//                return false;
//            }
//        } catch (Exception ex) {
//            db.closeConection(config);
//            ex.printStackTrace();
//            return false;
//        }
//   }
   
   public JSONObject loadLoginDetails(LoginDetails login_details){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select user_id,user_type,contact_no,email,\n" +
                        "(select user_name from user_details ud where ud.user_id=l.user_id) as user_name,\n" +
                        "(select dob from user_details ud where ud.user_id=l.user_id) as dob,\n" +
                        "(select pic from user_pic_details upd where upd.user_id=l.user_id) as pic\n" +
                        "from login_details l where email=? and password=?";
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, login_details.getEmail());
           config.getPstmt().setString(2, login_details.getPassword());
           config.setRs(config.getPstmt().executeQuery());
           while(config.getRs().next()){
               login_details.setUser_id(config.getRs().getString("user_id"));
               JSONObject  j = new JSONObject();
               j.put("user_id",config.getRs().getString("user_id"));
               j.put("user_name",config.getRs().getString("user_name"));
               j.put("email",config.getRs().getString("email"));
               j.put("pic",config.getRs().getString("pic"));
               j.put("dob",config.getRs().getString("dob"));
               j.put("contact_no",config.getRs().getString("contact_no"));
               j.put("user_type",config.getRs().getString("user_type"));
               j.put("email",login_details.getEmail());
               j.put("password",login_details.getPassword());
               return j;
           }
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
       }
       return null;
   }    
   
   public String[] loadForgotDetails(LoginDetails login_details){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        String [] a=null;
       try {
           String sql = "select password,\n" +
                        "(select user_name from user_details ud where ud.user_id=l.user_id) as user_name\n" +
                        "from login_details l where email=?";
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, login_details.getEmail());
           config.setRs(config.getPstmt().executeQuery());
           if(config.getRs().next()){
               a=new String[2];
               a[0]=config.getRs().getString("password");
               a[1]=config.getRs().getString("user_name");
               return a;
           }
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
       }
       return null;
   }    
//   
//   public LoginDetails loadRecentUser(){
//        DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//       try {
//           LoginDetails rt = new LoginDetails();
//           String sql = "select * from login_details ORDER BY user_id DESC LIMIT 1";
//           config.setRs(config.getStmt().executeQuery(sql));
//           while(config.getRs().next()){
//               
//               rt.setSection_id(config.getRs().getString("section_id"));
//               rt.setCategory_id(config.getRs().getString("category_id"));
//               rt.setSubcategory_id(config.getRs().getString("subcategory_id"));
//               rt.setUser_id(config.getRs().getString("user_id"));
//               rt.setUser_name(config.getRs().getString("topic_name"));
//           }
//           return rt;
//       } catch (Exception e) {
//           db.closeConection(config);
//           e.printStackTrace();
//           return null;
//       }        
}
