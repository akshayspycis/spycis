/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.user_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.config.GlobalData;
import com.data_manager.user_details.User;
import com.data_manager.user_details.UserDetails;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
;

/**
 *
 * @author Akshay
 */
public class UserDetailsMgr {
             //method to insert UserDetails in database
    public boolean insUserDetails(UserDetails user_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        
        try {  
//            System.out.println(GlobalData.sdf.format(new SimpleDateFormat("yyyy-MM-dd").parse(user_details.getDob())));
            String sql = "insert into user_details ("                   
                    + "user_name,"
                    + "dob,"
                    + "registration_date"
                    + ")"
                    + " values (?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, user_details.getUser_name());
            config.getPstmt().setString(2, GlobalData.sdf.format(new SimpleDateFormat("yyyy-MM-dd").parse(user_details.getDob())));
            config.getPstmt().setString(3, user_details.getRegistration_date());
//            config.getPstmt().setString(4, user_details.getRegistration_date());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updUserDetails(UserDetails user_details) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update user_details set"
                    + " user_name = ? ,"
                    + " dob = ? "
                    + " where user_id= '"+user_details.getUser_id()+"'";
            
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, user_details.getUser_name());
            config.getPstmt().setString(2, user_details.getDob());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete UserDetails in database
   public boolean delUserDetails(String user_id){    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from user_details where user_id= '"+user_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadUserDetails(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<User> rt = new ArrayList<>();
           String sql = "select *,"
                   + "(select email from login_details l where l.user_id=ud.user_id limit 1) as email,"
                   + "(select password from login_details l where l.user_id=ud.user_id limit 1) as password,"
                   + "(select contact_no from login_details l where l.user_id=ud.user_id limit 1) as contact_no,"
                   + "(select pic from user_pic_details l where l.user_id=ud.user_id limit 1) as pic from user_details ud";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               User r = new User();
               r.setUser_id(config.getRs().getString("user_id"));
               r.setUser_name(config.getRs().getString("user_name"));
               r.setDob(config.getRs().getString("dob"));
               r.setEmail(config.getRs().getString("email"));
               r.setPassword(config.getRs().getString("password"));
               r.setContact_no(config.getRs().getString("contact_no"));
               r.setPic(config.getRs().getString("pic"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   public ArrayList loadUserProfile(String user_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<User> rt = new ArrayList<>();
           String sql = "select *,(select email from login_details l where l.user_id=ud.user_id) as email,(select password from login_details l where l.user_id=ud.user_id) as password,(select contact_no from login_details l where l.user_id=ud.user_id) as contact_no,(select pic from user_pic_details l where l.user_id=ud.user_id) as pic from user_details ud where ud.user_id="+user_id ;
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               User r = new User();
               r.setUser_id(config.getRs().getString("user_id"));
               r.setUser_name(config.getRs().getString("user_name"));
               r.setDob(config.getRs().getString("dob"));
               r.setEmail(config.getRs().getString("email"));
               r.setPassword(config.getRs().getString("password"));
               r.setContact_no(config.getRs().getString("contact_no"));
               r.setPic(config.getRs().getString("pic"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   
   public JSONObject checkUserEmail(String email){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        JSONObject  j = new JSONObject();
       try {
           
           String sql = "select * from login_details where email=?";
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, email);
            if (config.getPstmt().executeQuery().next()) {
                db.closeConection(config);
                j.put("1",true);
                j.put("2",true);
                return j;
            } else {
                db.closeConection(config);
                j.put("1",true);
                j.put("2",false);
                return j;
            }
       } catch (Exception e) {
           db.closeConection(config);
           try {
               j.put("1",false);
               j.put("2",false);
           } catch (JSONException ex) {}
           return j;
       }    
}
   
   public JSONObject checkUserContact(String contact_no){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        JSONObject  j = new JSONObject();
       try {
           
           String sql = "select * from login_details where contact_no=?";
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, contact_no);
            if (config.getPstmt().executeQuery().next()) {
                db.closeConection(config);
                j.put("1",true);
                j.put("2",true);
                return j;
            } else {
                db.closeConection(config);
                j.put("1",true);
                j.put("2",false);
                return j;
            }
       } catch (Exception e) {
           db.closeConection(config);
           try {
               j.put("1",false);
               j.put("2",false);
           } catch (JSONException ex) {}
           return j;
       }    
}

    public JSONObject getUser_id() {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select user_id,user_name from user_details ORDER BY user_id DESC LIMIT 1";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               JSONObject  j = new JSONObject();
               j.put("user_id",config.getRs().getString("user_id"));
               j.put("user_name",config.getRs().getString("user_name"));
               return j;
           }
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
       }   
       return null;
    }
   
}