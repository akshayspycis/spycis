/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.user_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.config.GlobalData;
import com.data_manager.user_details.UserCoverPicDetails;

/**
 *
 * @author Akshay
 */
public class UserCoverPicDetailsMgr {
    public boolean insUserCoverPicDetails(UserCoverPicDetails user_cover_pic_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into user_cover_pic_details ("                   
                    + "user_id,"
                    + "cover_pic,"
                    + "date"
                    + ")"
                    + " values (?,?,now())";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, user_cover_pic_details.getUser_id());
            config.getPstmt().setString(2, user_cover_pic_details.getPic());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace(GlobalData.ps);
            db.closeConection(config);
            return false;
        }
    }    
}
