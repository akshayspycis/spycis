/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.user_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.user_details.UserRequest;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class UserRequestMgr {
        public boolean insUserRequest(UserRequest user_request) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into user_request ("                   
                    + "software_id,"
                    + "user_id,"
                    + "date,"
                    + "status"
                    + ")"
                    + " values (?,?,now(),'true')";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, user_request.getSoftware_id());
            config.getPstmt().setString(2, user_request.getUser_id());
            System.out.println(config.getPstmt().toString());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updUserRequest(UserRequest user_request) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update user_request set"
                    + " status = ? "
                    + " where user_request_id= '"+user_request.getUser_request_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, user_request.getStatus());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete UserRequest in database
   public boolean delUserRequest(String user_request_id){    
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from user_request where user_request_id= '"+user_request_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public JSONObject loadUserRequest(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<UserRequest> rt = new ArrayList<>();
           String sql = "select *,(select pic from user_pic_details user_id where user_id=ur.user_id) as pic from user_request ur INNER JOIN user_details ud ON ur.user_id = ud.user_id where ur.software_id='"+software_id+"' and status='true'";
           config.setRs(config.getStmt().executeQuery(sql));
            JSONObject software =null; 
           while(config.getRs().next()){
               if(software==null)software=new JSONObject();
               JSONObject  j = new JSONObject();
               j.put("user_request_id",config.getRs().getString("user_request_id"));
               j.put("user_id",config.getRs().getString("user_id"));
               j.put("date",config.getRs().getString("date"));
               j.put("status",config.getRs().getString("status"));
               j.put("user_name",config.getRs().getString("user_name"));
               j.put("dob",config.getRs().getString("dob"));
               j.put("pic",config.getRs().getString("pic"));
               software.put(config.getRs().getString("user_request_id"),j);
           }
           return software;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }     
   public String loadStatus(String user_id,String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<UserRequest> rt = new ArrayList<>();
           String sql = "select IFNULL(status, 'false') as status  from user_request where software_id='"+software_id+"' and user_id='"+user_id+"'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           String status ="false"; 
           if(config.getRs().next()){
               status=config.getRs().getString("status");
           }
           return status;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }     

}
