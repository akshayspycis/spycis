/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.class_manager.paytm_response_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author user
 */
public class PaytmResponseDetailsMgr {

    public boolean insPaytmResponseDetails(TreeMap<String, String> paytm_response_details) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
            String sql = "insert into paytm_response_details ("
                    + "CURRENCY,"
                    + "CUST_ID,"
                    + "GATEWAYNAME,"
                    + "RESPMSG,"
                    + "BANKNAME,"
                    + "PAYMENTMODE,"
                    + "MID,"
                    + "RESPCODE,"
                    + "TXNID,"
                    + "TXNAMOUNT,"
                    + "ORDERID,"
                    + "STATUS,"
                    + "BANKTXNID,"
                    + "TXNDATE,"
                    + "CHECKSUMHASH,"
                    + "createdAt"
                    + ")"
                    + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, paytm_response_details.get("CURRENCY"));
            config.getPstmt().setString(2, paytm_response_details.get("CUST_ID"));
            config.getPstmt().setString(3, paytm_response_details.get("GATEWAYNAME"));
            config.getPstmt().setString(4, paytm_response_details.get("RESPMSG"));
            config.getPstmt().setString(5, paytm_response_details.get("BANKNAME"));
            config.getPstmt().setString(6, paytm_response_details.get("PAYMENTMODE"));
            config.getPstmt().setString(7, paytm_response_details.get("MID"));
            config.getPstmt().setString(8, paytm_response_details.get("RESPCODE"));
            config.getPstmt().setString(9, paytm_response_details.get("TXNID"));
            config.getPstmt().setString(10, paytm_response_details.get("TXNAMOUNT"));
            config.getPstmt().setString(11, paytm_response_details.get("ORDERID"));
            config.getPstmt().setString(12, paytm_response_details.get("STATUS"));
            config.getPstmt().setString(13, paytm_response_details.get("BANKTXNID"));
            config.getPstmt().setString(14, paytm_response_details.get("TXNDATE"));
            config.getPstmt().setString(15, paytm_response_details.get("CHECKSUMHASH"));
            
            System.out.println(config.getPstmt());
            int x = config.getPstmt().executeUpdate();
            if (x > 0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
        }
        return false;
    }
    //----------------------------------------------------------------------------------------------

    //method to RateList in database 
//   public boolean updChecksumDetails(ChecksumDetails paytm_response_details) {
//       DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase(); 
//       try {
//            String sql = "update paytm_response_details set"
//                    + " topic_name = ? "
//                    + " where topic_id= '"+paytm_response_details.getTopic_id()+"'";
//            
//           config.setPstmt(config.getConn().prepareStatement(sql));
//            config.getPstmt().setString(1, paytm_response_details.getTopic_name());
//            int x = config.getPstmt().executeUpdate();
//            
//            if (x>0) {
//                db.closeConection(config);
//                return true;
//            } else {
//                db.closeConection(config);
//                return false;
//            }
//        } catch (Exception ex) {
//            db.closeConection(config);
//            ex.printStackTrace();
//            return false;
//        }
//    }
//   //----------------------------------------------------------------------------------------------
//   
//   //method to delete ChecksumDetails in database
//   public boolean delChecksumDetails(String topic_id)
//   {    DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//        try {          
//            String sql = "delete from paytm_response_details where topic_id= '"+topic_id+"'";
//            config.setPstmt( config.getConn().prepareStatement(sql));
//            int x = config.getPstmt().executeUpdate();
//            if (x>0) {
//                db.closeConection(config);
//                return true;
//            } else {
//                db.closeConection(config);
//                return false;
//            }
//        } catch (Exception ex) {
//            db.closeConection(config);
//            ex.printStackTrace();
//            return false;
//        }
//   }
//   
//   public ResultSet loadQuestion(String category_id,String subcategory_id,String section_id,String topic_id,String language_id){
//        DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//       try {
//           String sql = "SELECT \n" +
//                        "q.checksum_id,\n" +
//                        "q.discription_id,\n" +
//                        "ql.checksum_bank_id,\n" +
//                        "qb.checksum,\n" +
//                        "dd.discription_bank_id \n" +
//                        "FROM paytm_response_details q\n" +
//                        "inner join checksum_lang_details ql on q.checksum_id=ql.checksum_id\n" +
//                        "inner join checksum_bank qb on qb.checksum_bank_id=ql.checksum_bank_id\n" +
//                        "left join discription_details dd on dd.discription_id=q.discription_id and dd.language_id=ql.language_id\n" +
//                        "where q.category_id='"+category_id+"' and q.subcategory_id ='"+subcategory_id+"' and q.section_id='"+section_id+"' and q.topic_id='"+topic_id+"' and ql.language_id='"+language_id+"'\n" +
//                        "order by q.checksum_id asc";
//           System.out.println(sql);
//           return config.getStmt().executeQuery(sql);
//       } catch (Exception e) {
//           db.closeConection(config);
//           e.printStackTrace();
//           return null;
//       }
//}
//   
//   public ChecksumDetails loadRecentTopic(){
//        DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//       try {
//           ChecksumDetails rt = new ChecksumDetails();
//           String sql = "select * from paytm_response_details ORDER BY topic_id DESC LIMIT 1";
//           config.setRs(config.getStmt().executeQuery(sql));
//           while(config.getRs().next()){
//               
//               rt.setSection_id(config.getRs().getString("section_id"));
//               rt.setCategory_id(config.getRs().getString("category_id"));
//               rt.setSubcategory_id(config.getRs().getString("subcategory_id"));
//               rt.setTopic_id(config.getRs().getString("topic_id"));
//               rt.setTopic_name(config.getRs().getString("topic_name"));
//           }
//           return rt;
//       } catch (Exception e) {
//           db.closeConection(config);
//           e.printStackTrace();
//           return null;
//       }
//   }                    
//     public long loadMaxId(){
//         long id=0;
//        DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//       try {
//     String sql = "SELECT max(checksum_id) as checksum_id FROM paytm_response_details q";
//
//           config.setRs(config.getStmt().executeQuery(sql));
//           while(config.getRs().next()){
//               if(config.getRs().getString("checksum_id")!=null){
//                   id=Long.parseLong(config.getRs().getString("checksum_id"));
//               }
//           }
//       } catch (Exception e) {
//           db.closeConection(config);
//           e.printStackTrace();
//           
//       }
//       return id;
//   }        
}
