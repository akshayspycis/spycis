/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.test_topic;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.test_topic.TestTopic;
import java.util.ArrayList;
/**
 *
 * @author Akshay
 */
public class TestTopicMgr {
    public boolean insTestTopic(ArrayList<TestTopic> test_topic) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            config.getConn().setAutoCommit(false);        
            String sql = "insert into test_topic ("                   
                    + "test_id,"
                    + "topic_id"
                    + ")"
                    + " values ((select max(test_id) from test_details),?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            for (TestTopic testtopic : test_topic) {
                config.getPstmt().setString(1, testtopic.getTopic_id());
                config.getPstmt().addBatch();
            }
            int x[] = config.getPstmt().executeBatch();
            if (x.length==test_topic.size()) {
                config.getConn().commit();
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
            

    }
}
