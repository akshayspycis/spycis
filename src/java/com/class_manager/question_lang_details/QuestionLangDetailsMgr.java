/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.question_lang_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.question_lang_details.QuestionLangDetails;

/**
 *
 * @author Akshay
 */
public class QuestionLangDetailsMgr {
             //method to insert QuestionLangDetails in database
    public boolean insQuestionLangDetails(QuestionLangDetails question_lang_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into question_lang_details ("                   
                    + "question_id,"
                    + "question_bank_id,"
                    + "language_id)"
                    + " values (?,(select max(question_bank_id) from question_bank),?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, question_lang_details.getQuestion_id());
            config.getPstmt().setString(2, question_lang_details.getLanguage_id());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
   //method to delete QuestionLangDetails in database
   public boolean delQuestionLangDetails(String question_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from question_lang_details where question_id= '"+question_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
    
}
