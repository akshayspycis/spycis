/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.subcategory_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.subcategory_details.SubCategoryDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class SubCategoryDetailsMgr {
             //method to insert SubCategoryDetails in database
    public boolean insSubCategoryDetails(SubCategoryDetails subcategory_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into subcategory_details ("                   
                    + "category_id,"
                    + "subcategory_name"
                    + ")"
                    + " values (?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, subcategory_details.getCategory_id());
            config.getPstmt().setString(2, subcategory_details.getSubcategory_name());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updSubCategoryDetails(SubCategoryDetails subcategory_details) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update subcategory_details set"
                    + " subcategory_name = ? "
                    + " where subcategory_id= '"+subcategory_details.getSubcategory_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, subcategory_details.getSubcategory_name());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete SubCategoryDetails in database
   public boolean delSubCategoryDetails(String subcategory_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from subcategory_details where subcategory_id= '"+subcategory_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadSubCategory(String category_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<SubCategoryDetails> rt = new ArrayList<>();
           String sql = "select * from subcategory_details where category_id="+category_id;
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               SubCategoryDetails r = new SubCategoryDetails();
               r.setSubcategory_id(config.getRs().getString("subcategory_id"));
               r.setSubcategory_name(config.getRs().getString("subcategory_name"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   
   public SubCategoryDetails loadRecentSubCategory(){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           SubCategoryDetails rt = new SubCategoryDetails();
           String sql = "select * from subcategory_details ORDER BY subcategory_id DESC LIMIT 1";
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               
               rt.setSubcategory_id(config.getRs().getString("subcategory_id"));
               rt.setCategory_id(config.getRs().getString("category_id"));
               rt.setSubcategory_name(config.getRs().getString("subcategory_name"));
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }        
}
