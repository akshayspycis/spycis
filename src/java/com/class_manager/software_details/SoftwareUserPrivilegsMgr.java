/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.software_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.software_details.SoftwareUserPrivilegs;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class SoftwareUserPrivilegsMgr {
    public boolean insSoftwareUserPrivilegs(SoftwareUserPrivilegs software_user_privilegs) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_user_privilegs ("                   
                    + "software_id,"
                    + "user_id,"
                    + "date,"
                    + "status"
                    + ")"
                    + " values (?,?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_user_privilegs.getSoftware_id());
            config.getPstmt().setString(2, software_user_privilegs.getUser_id());
            config.getPstmt().setString(3, software_user_privilegs.getDate());
            config.getPstmt().setString(3, software_user_privilegs.getStatus());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updSoftwareUserPrivilegs(SoftwareUserPrivilegs software_user_privilegs) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update software_user_privilegs set"
                    + " status = ? "
                    + " where software_user_privilegs_id= '"+software_user_privilegs.getSoftware_user_privilegs_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_user_privilegs.getStatus());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete SoftwareUserPrivilegs in database
   public boolean delSoftwareUserPrivilegs(String software_user_privilegs_id){    
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from software_user_privilegs where software_user_privilegs_id= '"+software_user_privilegs_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadSoftwareUserPrivilegs(String user_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<SoftwareUserPrivilegs> rt = new ArrayList<>();
           String sql = "select * from software_user_privilegs where user_id='"+user_id+"' and status='true'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               SoftwareUserPrivilegs r = new SoftwareUserPrivilegs();
               r.setSoftware_id(config.getRs().getString("software_id"));
               r.setUser_id(config.getRs().getString("user_id"));
               r.setDate(config.getRs().getString("date"));
               r.setStatus(config.getRs().getString("status"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }         
}
