/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.software_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.software_details.SoftwareAddressDetails;
import com.data_manager.software_details.SoftwareBasicCofiguration;

/**
 *
 * @author Akshay
 */
public class SoftwareAddressDetailsMgr {
public boolean insSoftwareAddressDetails(SoftwareAddressDetails software_address_details ) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_address_details ("                   
                    + "software_id,"
                    + "address,"
                    + "street,"
                    + "pincode,"
                    + "city,"
                    + "state,"
                    + "country"
                    + ")"
                    + " values (?,?,?,?,?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_address_details.getSoftware_id());
            config.getPstmt().setString(2, software_address_details.getAddress());
            config.getPstmt().setString(3, software_address_details.getStreet());
            config.getPstmt().setString(4, software_address_details.getPincode());
            config.getPstmt().setString(5, software_address_details.getCity());
            config.getPstmt().setString(6, software_address_details.getState());
            config.getPstmt().setString(7, software_address_details.getCountry());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                SoftwareBasicCofiguration sbc = new SoftwareBasicCofiguration();
                sbc.setSoftware_basic_cofiguration_id("3");
                sbc.setSoftware_id(software_address_details.getSoftware_id());
                sbc.setStatus("true");
                if(new SoftwareBasicCofigurationMgr().updSoftwareBasicCofiguration(sbc)){
                    db.closeConection(config);
                    return true;
                }else{
                    db.closeConection(config);
                    return false;
                }
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    
    public boolean updSoftwareAddressDetails(SoftwareAddressDetails software_address_details) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update software_address_details set"
                    + " street = ? "
                    + " where software_id= '"+software_address_details.getSoftware_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, software_address_details.getSoftware_id());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }    
}
