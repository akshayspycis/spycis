/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.software_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.software_details.SoftwareProfilePicDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class SoftwareProfilePicDetailsMgr {
public boolean insSoftwareProfilePicDetails(SoftwareProfilePicDetails software_profile_pic_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_profile_pic_details ("                   
                    + "software_id,"
                    + "pic,"
                    + "date"
                    + ")"
                    + " values (?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_profile_pic_details.getSoftware_id());
            config.getPstmt().setString(2, software_profile_pic_details.getPic());
            config.getPstmt().setString(3, software_profile_pic_details.getDate());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   
   //method to delete SoftwareProfilePicDetails in database
   public boolean delSoftwareProfilePicDetails(String software_profile_pic_details_id){    
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from software_profile_pic_details where software_profile_pic_details_id= '"+software_profile_pic_details_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadSoftwareProfilePicDetails(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<SoftwareProfilePicDetails> rt = new ArrayList<>();
           String sql = "select * from software_profile_pic_details where software_id='"+software_id+"' ";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               SoftwareProfilePicDetails r = new SoftwareProfilePicDetails();
               r.setSoftware_id(config.getRs().getString("software_id"));
               r.setPic(config.getRs().getString("pic"));
               r.setDate(config.getRs().getString("date"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }                
}
