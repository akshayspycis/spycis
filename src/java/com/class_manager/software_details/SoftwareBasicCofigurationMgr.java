/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.software_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.mocktest_details.MocktestBatchList;
import com.data_manager.software_details.SoftwareBasicCofiguration;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class SoftwareBasicCofigurationMgr {
    public boolean insSoftwareBasicCofiguration() {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
         String sql = "insert into software_basic_cofiguration ("                   
                    + "software_basic_cofiguration_id,"
                    + "status,"
                    + "software_id"
                    + ")"
                    + " values (?,?,(select max(software_id) from software_details))";
         config.setPstmt(config.getConn().prepareStatement(sql));
                for (int i = 1; i < 7; i++) {
                    config.getPstmt().setString(1, String.valueOf(i));
                    config.getPstmt().setString(2, "false");
                    config.getPstmt().addBatch();
                }
            int x[] = config.getPstmt().executeBatch();
            if (x.length==6) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   
   public JSONObject loadSoftwareBasicCofiguration(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<MocktestBatchList> rt = new ArrayList<>();
           String sql = "select * from software_basic_cofiguration where software_id='"+software_id+"' and status='false'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           JSONObject software_basic_cofiguration =null; 
           while(config.getRs().next()){
               if(software_basic_cofiguration==null){
                   software_basic_cofiguration=new JSONObject();
               }
               software_basic_cofiguration.put(config.getRs().getString("software_basic_cofiguration_id"),config.getRs().getString("status"));
           }
           return software_basic_cofiguration;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }
   public boolean updSoftwareBasicCofiguration(SoftwareBasicCofiguration software_basic_cofiguration) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update software_basic_cofiguration set"
                    + " status = ? "
                    + " where software_id= '"+software_basic_cofiguration.getSoftware_id()+"' and software_basic_cofiguration_id='"+software_basic_cofiguration.getSoftware_basic_cofiguration_id()+"'";
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, software_basic_cofiguration.getStatus());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
}
