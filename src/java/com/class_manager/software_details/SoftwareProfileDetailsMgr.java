/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.software_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.config.GlobalData;
import com.data_manager.software_details.SoftwareProfileDetails;

/**
 *
 * @author Akshay
 */
public class SoftwareProfileDetailsMgr {
public boolean insSoftwareProfileDetails(SoftwareProfileDetails software_profile_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_profile_details ("                   
                    + "software_id,"
                    + "pic,"
                    + "date"
                    + ")"
                    + " values (?,?,NOW())";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_profile_details.getSoftware_id());
            config.getPstmt().setString(2, software_profile_details.getPic());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace(GlobalData.ps);
            db.closeConection(config);
            return false;
        }
    }    
}
