/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.software_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.software_details.SoftwareUserPrivilegsAction;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class SoftwareUserPrivilegsActionMgr {
    public boolean insSoftwareUserPrivilegsAction(SoftwareUserPrivilegsAction software_user_privilegs_action) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_user_privilegs_action ("                   
                    + "user_id,"
                    + "action_module_id,"
                    + "status"
                    + ")"
                    + " values (?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_user_privilegs_action.getUser_id());
            config.getPstmt().setString(2, software_user_privilegs_action.getAction_module_id());
            config.getPstmt().setString(3, software_user_privilegs_action.getStatus());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updSoftwareUserPrivilegsAction(SoftwareUserPrivilegsAction software_user_privilegs_action) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update software_user_privilegs_action set"
                    + " status = ? "
                    + " where software_user_privilegs_action_id= '"+software_user_privilegs_action.getSoftware_user_privilegs_id()+"'";
            
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_user_privilegs_action.getStatus());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete SoftwareUserPrivilegsAction in database
   public boolean delSoftwareUserPrivilegsAction(String software_user_privilegs_action_id){    
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from software_user_privilegs_action where software_user_privilegs_action_id= '"+software_user_privilegs_action_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadSoftwareUserPrivilegsAction(String user_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<SoftwareUserPrivilegsAction> rt = new ArrayList<>();
           String sql = "select * from software_user_privilegs_action where user_id='"+user_id+"' and status='true'";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               SoftwareUserPrivilegsAction r = new SoftwareUserPrivilegsAction();
               r.setSoftware_user_privilegs_id(config.getRs().getString("software_user_privilegs_id"));
               r.setSoftware_id(config.getRs().getString("software_id"));
               r.setUser_id(config.getRs().getString("user_id"));
               r.setAction_module_id(config.getRs().getString("action_module_id"));
               r.setStatus(config.getRs().getString("status"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }             
}
