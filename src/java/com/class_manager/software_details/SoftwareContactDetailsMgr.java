/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.software_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.software_details.SoftwareBasicCofiguration;
import com.data_manager.software_details.SoftwareContactDetails;

/**
 *
 * @author Akshay
 */
public class SoftwareContactDetailsMgr {
             //method to insert SoftwareContactDetails in database
    public boolean insSoftwareContactDetails(String[]a) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_contact_details ("                   
                    + "software_id,"
                    + "contact_no,"
                    + "email"
                    + ")"
                    + " values ((select max(software_id) from software_details),?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, a[3]);
            config.getPstmt().setString(2, a[4]);
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    
    public boolean updSoftwareContactDetails(SoftwareContactDetails software_contact_details) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update software_contact_details set"
                    + " alt_contact_no = ? ,"
                    + " website = ? "
                    + " where software_id= '"+software_contact_details.getSoftware_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, software_contact_details.getAlt_contact_no());
           config.getPstmt().setString(2, software_contact_details.getWebsite());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                SoftwareBasicCofiguration sbc = new SoftwareBasicCofiguration();
                sbc.setSoftware_basic_cofiguration_id("1");
                sbc.setSoftware_id(software_contact_details.getSoftware_id());
                sbc.setStatus("true");
                if(new SoftwareBasicCofigurationMgr().updSoftwareBasicCofiguration(sbc)){
                    sbc.setSoftware_basic_cofiguration_id("2");
                    sbc.setSoftware_id(software_contact_details.getSoftware_id());
                    sbc.setStatus("true");
                    if(new SoftwareBasicCofigurationMgr().updSoftwareBasicCofiguration(sbc)){
                        db.closeConection(config);
                        return true;
                    }else{
                        db.closeConection(config);
                        return false;
                    }
                } else {
                    db.closeConection(config);
                    return false;
                }
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
    
    //----------------------------------------------------------------------------------------------
    

    
}
