/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.software_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.software_details.SoftwareDetails;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class SoftwareDetailsMgr {
   //method to insert SoftwareDetails in database
         public boolean insSoftwareDetails(String[]a) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_details ("                   
                    + "user_id,"
                    + "software_name_id,"
                    + "orgenisation_name,"
                    + "otp_authentication"
                    + ")"
                    + " values (?,?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, a[0]);
            config.getPstmt().setString(2, a[1]);
            config.getPstmt().setString(3, a[2]);
            config.getPstmt().setString(4, "false");
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                if(new SoftwareContactDetailsMgr().insSoftwareContactDetails(a)){
//                    if(new SoftwareBasicCofigurationMgr().insSoftwareBasicCofiguration()){
                        db.closeConection(config);
                        return true;
//                    }else{
//                        db.closeConection(config);
//                        return false;    
//                    }
                }else{
                    db.closeConection(config);
                    return false;    
                }
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
         public boolean updSoftwareDetails(SoftwareDetails software_details,String name) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update software_details set"
                    + " ? = ? "
                    + " where category_id= ?";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, name);
            config.getPstmt().setString(1, software_details.getOtp_authentication());
            config.getPstmt().setString(1, software_details.getSoftware_id());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------  
         public JSONObject loadSoftwareDetails(String user_id ){
            DBConfiguration db =new DBConfiguration();
            Config config = db.loadDatabase();
            try {
                String sql = "select * ,(select pic from software_profile_pic_details spd where spd.software_id=s.software_id) as pic\n" +
                        "from software_details s where s.user_id =?";
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, user_id);
           config.setRs(config.getPstmt().executeQuery());
           JSONObject software =null; 
           String software_id =null; 
           while(config.getRs().next()){
               if(software==null){
                   software=new JSONObject();
                   software_id=config.getRs().getString("software_id");
               }
               JSONObject  j = new JSONObject();
               j.put("software_id",config.getRs().getString("software_id"));
               j.put("software_name_id",config.getRs().getString("software_name_id"));
               j.put("orgenisation_name",config.getRs().getString("orgenisation_name"));
               j.put("tag_line",config.getRs().getString("tag_line"));
               j.put("pic",config.getRs().getString("pic"));
               software.put(config.getRs().getString("software_name_id"),j);
           }
           if(software_id!=null){
               software.put("software_basic_cofiguration",new SoftwareBasicCofigurationMgr().loadSoftwareBasicCofiguration(software_id));
           }
           return software;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
       }
       return null;
   }    
    //----------------------------------------------------------------------------------------------   
         public JSONObject loadSuggestionSoftwareDetails(String term){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select * ,(select pic from software_profile_pic_details spd where spd.software_id=s.software_id) as pic "
//                      + "IFNULL((select status from user_request ur where ur.software_id=s.software_id ), 'false') as status " 
                      +  " from software_details s where LOWER(s.orgenisation_name) like ?";
           
           config.setPstmt(config.getConn().prepareStatement(sql));
           config.getPstmt().setString(1, term.toLowerCase()+"%");
           config.setRs(config.getPstmt().executeQuery());
           JSONObject software =null; 
           while(config.getRs().next()){
               if(software==null){
                   software=new JSONObject();
               }
               JSONObject  j = new JSONObject();
               j.put("software_id",config.getRs().getString("software_id"));
               j.put("software_name_id",config.getRs().getString("software_name_id"));
               j.put("orgenisation_name",config.getRs().getString("orgenisation_name"));
               j.put("tag_line",config.getRs().getString("tag_line"));
               j.put("pic",config.getRs().getString("pic"));
//               j.put("status",config.getRs().getString("status"));
               software.put(config.getRs().getString("software_id"),j);
           }
           return software;
       } catch (Exception e) {
           db.closeConection(config);
           //e.printStackTrace();
       }
       return null;
   }    
    //----------------------------------------------------------------------------------------------

        public String maxSoftwareId() {
            DBConfiguration db =new DBConfiguration();
            Config config = db.loadDatabase();
           try {
               String sql = "SELECT max(software_id) as software_id FROM software_details q";
               config.setRs(config.getStmt().executeQuery(sql));
               while(config.getRs().next()){
                   if(config.getRs().getString("software_id")!=null){
                       return config.getRs().getString("software_id");
                   }
               }
           } catch (Exception e) {
               db.closeConection(config);
               e.printStackTrace();

           }
           return null;
       }                    

}
