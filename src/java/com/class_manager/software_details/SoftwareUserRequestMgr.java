/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.software_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.software_details.SoftwareUserRequest;
import java.sql.ResultSet;

/**
 *
 * @author Akshay
 */
public class SoftwareUserRequestMgr {
             //method to insert SoftwareUserRequest in database
    public boolean insSoftwareUserRequest(SoftwareUserRequest software_user_request) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_user_request ("                   
                    + "software_id,"
                    + "user_id,"
                    + "date,"
                    + "status"
                    + ")"
                    + " values (?,?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_user_request.getSoftware_id());
            config.getPstmt().setString(2, software_user_request.getUser_id());
            config.getPstmt().setString(3, software_user_request.getDate());
            config.getPstmt().setString(4, software_user_request.getStatus());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to RateList in database 
   public boolean updSoftwareUserRequest(SoftwareUserRequest software_user_request) {
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase(); 
       try {
            String sql = "update software_user_request set"
                    + " status = ? "
                    + " where software_user_request_id= '"+software_user_request.getSoftware_user_request_id()+"'";
            
           config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_user_request.getStatus());
            int x = config.getPstmt().executeUpdate();
            
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
    }
//   //----------------------------------------------------------------------------------------------
//   
//   //method to delete SoftwareUserRequest in database
   public boolean delSoftwareUserRequest(String software_user_request_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from software_user_request where software_user_request_id= '"+software_user_request_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ResultSet loadUserRequest(String category_id,String subcategory_id,String section_id,String software_user_request_id,String language_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "SELECT \n" +
                        "q.question_id,\n" +
                        "q.discription_id,\n" +
                        "ql.question_bank_id,\n" +
                        "qb.question,\n" +
                        "dd.discription_bank_id \n" +
                        "FROM software_user_request q\n" +
                        "inner join question_lang_details ql on q.question_id=ql.question_id\n" +
                        "inner join question_bank qb on qb.question_bank_id=ql.question_bank_id\n" +
                        "left join discription_details dd on dd.discription_id=q.discription_id and dd.language_id=ql.language_id\n" +
                        "where q.category_id='"+category_id+"' and q.subcategory_id ='"+subcategory_id+"' and q.section_id='"+section_id+"' and q.software_user_request_id='"+software_user_request_id+"' and ql.language_id='"+language_id+"'\n" +
                        "order by q.question_id asc";
           System.out.println(sql);
           return config.getStmt().executeQuery(sql);
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
         
}
