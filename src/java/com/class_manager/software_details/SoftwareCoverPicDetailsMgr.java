/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.software_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.software_details.SoftwareCoverPicDetails;
import java.util.ArrayList;

/**
 *
 * @author Akshay
 */
public class SoftwareCoverPicDetailsMgr {
    public boolean insSoftwareCoverPicDetails(SoftwareCoverPicDetails software_cover_pic_details) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into software_cover_pic_details ("                   
                    + "software_id,"
                    + "cover_pic,"
                    + "date"
                    + ")"
                    + " values (?,?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, software_cover_pic_details.getSoftware_id());
            config.getPstmt().setString(2, software_cover_pic_details.getCover_pic());
            config.getPstmt().setString(3, software_cover_pic_details.getDate());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   
   //method to delete SoftwareCoverPicDetails in database
   public boolean delSoftwareCoverPicDetails(String software_cover_pic_details_id){    
       DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from software_cover_pic_details where software_cover_pic_details_id= '"+software_cover_pic_details_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ArrayList loadSoftwareCoverPicDetails(String software_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           ArrayList<SoftwareCoverPicDetails> rt = new ArrayList<>();
           String sql = "select * from software_cover_pic_details where software_id='"+software_id+"' ";
           System.out.println(sql);
           config.setRs(config.getStmt().executeQuery(sql));
           while(config.getRs().next()){
               SoftwareCoverPicDetails r = new SoftwareCoverPicDetails();
               r.setSoftware_id(config.getRs().getString("software_id"));
               r.setCover_pic(config.getRs().getString("cover_pic"));
               r.setDate(config.getRs().getString("date"));
               rt.add(r);
           }
           return rt;
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }            
}
