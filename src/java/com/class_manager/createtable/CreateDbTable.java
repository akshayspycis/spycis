/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.class_manager.createtable;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;

/**
 *
 * @author user
 */
public class CreateDbTable {
    public boolean createTable(String sql) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
            config.setPstmt(config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x > 0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception se) {
            se.printStackTrace();
            return false;
        }
    }
    public static void main(String[] args) {
        new CreateDbTable().createTable("CREATE TABLE `mocktest`.`order_details` (\n" +
"  `order_details_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,\n" +
"  `user_id` VARCHAR(200) NOT NULL,\n" +
"  `test_id` VARCHAR(200) NOT NULL,\n" +
"  `createdAt` VARCHAR(45) NOT NULL,\n" +
"  `grossamount` VARCHAR(45) NOT NULL,\n" +
"  `orderid` VARCHAR(45) NOT NULL,\n" +
"  `orderstatus` VARCHAR(45) NOT NULL,\n" +
"  `paid` VARCHAR(45) NOT NULL,\n" +
"  `paidamount` VARCHAR(45) NOT NULL,\n" +
"  `paidtime` VARCHAR(45) NOT NULL,\n" +
"  `paymentgateway` VARCHAR(45) NOT NULL,\n" +
"  `paymenttype` VARCHAR(45) NOT NULL,\n" +
"  `placedtime` VARCHAR(45) NOT NULL,\n" +
"  `discountamount` VARCHAR(45) NOT NULL,\n" +
"  `taxamount` VARCHAR(45) NOT NULL,\n" +
"  `totalprice` VARCHAR(45) NOT NULL,\n" +
"  PRIMARY KEY (`order_details_id`)\n" +
")");
            new CreateDbTable().createTable("CREATE TABLE `mocktest`.`checksum_details` (\n" +
"  `checksum_details_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,\n" +
"  `CALLBACK_URL` VARCHAR(1500),\n" +
"  `CHANNEL_ID` VARCHAR(1500),\n" +
"  `CHECKSUMHASH` VARCHAR(1500),\n" +
"  `CUST_ID` VARCHAR(1500),\n" +
"  `EMAIL` VARCHAR(1500),\n" +
"  `INDUSTRY_TYPE_ID` VARCHAR(1500),\n" +
"  `MID` VARCHAR(1500),\n" +
"  `MOBILE_NO` VARCHAR(1500),\n" +
"  `ORDER_ID` VARCHAR(1500),\n" +
"  `TXN_AMOUNT` VARCHAR(1500),\n" +
"  `WEBSITE` VARCHAR(1500),\n" +
"  `test_id` VARCHAR(1500),\n" +
"  `user_id` VARCHAR(1500),\n" +
"  PRIMARY KEY (`checksum_details_id`)\n" +
")\n" +
"ENGINE = InnoDB");
            new CreateDbTable().createTable("CREATE TABLE `mocktest`.`paytm_response_details` (\n" +
"  `paytm_response_details_id` INTEGER UNSIGNED AUTO_INCREMENT,\n" +
"  `CURRENCY` VARCHAR(200),\n" +
"  `CUST_ID` VARCHAR(200),\n" +
"  `GATEWAYNAME` VARCHAR(200),\n" +
"  `RESPMSG` VARCHAR(200),\n" +
"  `BANKNAME` VARCHAR(200),\n" +
"  `PAYMENTMODE` VARCHAR(200),\n" +
"  `MID` VARCHAR(200),\n" +
"  `RESPCODE` VARCHAR(200),\n" +
"  `TXNID` VARCHAR(200),\n" +
"  `TXNAMOUNT` VARCHAR(200),\n" +
"  `ORDERID` VARCHAR(200),\n" +
"  `STATUS` VARCHAR(200),\n" +
"  `BANKTXNID` VARCHAR(200),\n" +
"  `TXNDATE` VARCHAR(200),\n" +
"  `CHECKSUMHASH` VARCHAR(2000),\n" +
"  PRIMARY KEY (`paytm_response_details_id`)\n" +
")\n" +
"ENGINE = InnoDB;");
    }

}
