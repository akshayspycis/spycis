/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.class_manager.option_bank;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import com.data_manager.option_bank.OptionBank;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.naming.spi.DirStateFactory;

/**
 *
 * @author Akshay
 */
public class OptionBankMgr {
//method to insert OptionBank in database
    public boolean insOptionBank(OptionBank option_bank) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            String sql = "insert into option_bank ("                   
                    + "question_bank_id,"
                    + "option_detail,"
                    + "option_name)"
                    + " values ((select max(question_bank_id) from question_bank),?,?)";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, option_bank.getOption());
            config.getPstmt().setString(2, option_bank.getOption_name());
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
   //method to delete OptionBank in database
   public boolean delOptionBank(String question_bank_id)
   {    DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {          
            String sql = "delete from option_bank where question_bank_id= '"+question_bank_id+"'";
            config.setPstmt( config.getConn().prepareStatement(sql));
            int x = config.getPstmt().executeUpdate();
            if (x>0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
            return false;
        }
   }
   
   public ResultSet loadOptionBank(String question_bank_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select option_name,option_detail from option_bank where question_bank_id='"+question_bank_id+"'";
           return config.getStmt().executeQuery(sql);
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    
   public ResultSet loadAnswerBank(String question_bank_id){
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
       try {
           String sql = "select answer from question_bank where question_bank_id='"+question_bank_id+"'";
           return config.getStmt().executeQuery(sql);
       } catch (Exception e) {
           db.closeConection(config);
           e.printStackTrace();
           return null;
       }
   }    

    public boolean updOptionBankMgr(ArrayList<OptionBank> option,String question_bank_id) {
        DBConfiguration db =new DBConfiguration();
        Config config = db.loadDatabase();
        try {   
            config.getConn().setAutoCommit(false);        
            
            String sql = "update option_bank set"                   
                    + " option_detail=? "
                    + " where question_bank_id=? and option_name=?";
            config.setPstmt(config.getConn().prepareStatement(sql));
            for (OptionBank optionbank : option) {
                config.getPstmt().setString(1, optionbank.getOption());
                config.getPstmt().setString(2, question_bank_id);
                config.getPstmt().setString(3, optionbank.getOption_name());
                config.getPstmt().addBatch();
            }
            int x[] = config.getPstmt().executeBatch();
            if (x.length==option.size()) {
                config.getConn().commit();
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
            return false;
        }
    }
   
    
}
