/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.class_manager.checksum_details;

import com.data_base_configuration.DBConfiguration;
import com.data_manager.Config;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author user
 */
public class ChecksumDetailsMgr {

    public String insChecksumDetails(TreeMap<String,String> checksum_detials) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
            String sql = "insert into checksum_details ("
                    + "CALLBACK_URL,"
                    + "CHANNEL_ID,"
                    + "CHECKSUMHASH,"
                    + "CUST_ID,"
                    + "EMAIL,"
                    + "INDUSTRY_TYPE_ID,"
                    + "MID,"
                    + "MOBILE_NO,"
                    + "ORDER_ID,"
                    + "TXN_AMOUNT,"
                    + "WEBSITE,"
                    + "test_id,"
                    + "user_id,"
                    + "createdAt,"
                    + "updatedAt,"
                    + "status"
                    + ")"
                    + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,now(),'','false')";
            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, checksum_detials.get("CALLBACK_URL"));
            config.getPstmt().setString(2, checksum_detials.get("CHANNEL_ID"));
            config.getPstmt().setString(3, checksum_detials.get("CHECKSUMHASH"));
            config.getPstmt().setString(4, checksum_detials.get("CUST_ID"));
            config.getPstmt().setString(5, checksum_detials.get("EMAIL"));
            config.getPstmt().setString(6, checksum_detials.get("INDUSTRY_TYPE_ID"));
            config.getPstmt().setString(7, checksum_detials.get("MID"));
            config.getPstmt().setString(8, checksum_detials.get("MOBILE_NO"));
            config.getPstmt().setString(9, checksum_detials.get("ORDER_ID"));
            config.getPstmt().setString(10, checksum_detials.get("TXN_AMOUNT"));
            config.getPstmt().setString(11, checksum_detials.get("WEBSITE"));
            config.getPstmt().setString(12, checksum_detials.get("test_id"));
            config.getPstmt().setString(13, checksum_detials.get("user_id"));

            int x = config.getPstmt().executeUpdate();
            if (x > 0) {
                sql = "select checksum_details_id from checksum_details order by checksum_details_id desc limit 1";
                config.setRs(config.getStmt().executeQuery(sql));
                if (config.getRs().next()) {
                    String checksum_details_id = config.getRs().getString("checksum_details_id");
                    return checksum_details_id;
                }
                db.closeConection(config);
            } else {
                db.closeConection(config);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            db.closeConection(config);
        }
        return null;
    }
    //----------------------------------------------------------------------------------------------

    //method to RateList in database 
    public boolean updChecksumDetails(boolean status, String ORDER_ID) {
        DBConfiguration db = new DBConfiguration();
        Config config = db.loadDatabase();
        try {
            String sql = "update checksum_details set"
                    + " status = ? ,"
                    + " updatedAt = now()"
                    + " where ORDER_ID = '" + ORDER_ID + "'";

            config.setPstmt(config.getConn().prepareStatement(sql));
            config.getPstmt().setString(1, String.valueOf(status));
            int x = config.getPstmt().executeUpdate();
            if (x > 0) {
                db.closeConection(config);
                return true;
            } else {
                db.closeConection(config);
            }
        } catch (Exception ex) {
            db.closeConection(config);
            ex.printStackTrace();
        }
        return false;
    }
//   //----------------------------------------------------------------------------------------------
//   
//   //method to delete ChecksumDetails in database
//   public boolean delChecksumDetails(String topic_id)
//   {    DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//        try {          
//            String sql = "delete from checksum_details where topic_id= '"+topic_id+"'";
//            config.setPstmt( config.getConn().prepareStatement(sql));
//            int x = config.getPstmt().executeUpdate();
//            if (x>0) {
//                db.closeConection(config);
//                return true;
//            } else {
//                db.closeConection(config);
//                return false;
//            }
//        } catch (Exception ex) {
//            db.closeConection(config);
//            ex.printStackTrace();
//            return false;
//        }
//   }
//   
//   public ResultSet loadQuestion(String category_id,String subcategory_id,String section_id,String topic_id,String language_id){
//        DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//       try {
//           String sql = "SELECT \n" +
//                        "q.checksum_id,\n" +
//                        "q.discription_id,\n" +
//                        "ql.checksum_bank_id,\n" +
//                        "qb.checksum,\n" +
//                        "dd.discription_bank_id \n" +
//                        "FROM checksum_details q\n" +
//                        "inner join checksum_lang_details ql on q.checksum_id=ql.checksum_id\n" +
//                        "inner join checksum_bank qb on qb.checksum_bank_id=ql.checksum_bank_id\n" +
//                        "left join discription_details dd on dd.discription_id=q.discription_id and dd.language_id=ql.language_id\n" +
//                        "where q.category_id='"+category_id+"' and q.subcategory_id ='"+subcategory_id+"' and q.section_id='"+section_id+"' and q.topic_id='"+topic_id+"' and ql.language_id='"+language_id+"'\n" +
//                        "order by q.checksum_id asc";
//           System.out.println(sql);
//           return config.getStmt().executeQuery(sql);
//       } catch (Exception e) {
//           db.closeConection(config);
//           e.printStackTrace();
//           return null;
//       }
//}
//   
//   public ChecksumDetails loadRecentTopic(){
//        DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//       try {
//           ChecksumDetails rt = new ChecksumDetails();
//           String sql = "select * from checksum_details ORDER BY topic_id DESC LIMIT 1";
//           config.setRs(config.getStmt().executeQuery(sql));
//           while(config.getRs().next()){
//               
//               rt.setSection_id(config.getRs().getString("section_id"));
//               rt.setCategory_id(config.getRs().getString("category_id"));
//               rt.setSubcategory_id(config.getRs().getString("subcategory_id"));
//               rt.setTopic_id(config.getRs().getString("topic_id"));
//               rt.setTopic_name(config.getRs().getString("topic_name"));
//           }
//           return rt;
//       } catch (Exception e) {
//           db.closeConection(config);
//           e.printStackTrace();
//           return null;
//       }
//   }                    
//     public long loadMaxId(){
//         long id=0;
//        DBConfiguration db =new DBConfiguration();
//        Config config = db.loadDatabase();
//       try {
//     String sql = "SELECT max(checksum_id) as checksum_id FROM checksum_details q";
//
//           config.setRs(config.getStmt().executeQuery(sql));
//           while(config.getRs().next()){
//               if(config.getRs().getString("checksum_id")!=null){
//                   id=Long.parseLong(config.getRs().getString("checksum_id"));
//               }
//           }
//       } catch (Exception e) {
//           db.closeConection(config);
//           e.printStackTrace();
//           
//       }
//       return id;
//   }    
}
