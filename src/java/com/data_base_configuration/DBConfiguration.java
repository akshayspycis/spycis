/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.data_base_configuration;
import com.data_manager.Config;
import com.data_manager.config.GlobalData;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author akshay
 */
public class DBConfiguration {
    public Config loadDatabase() {
        try {
            Config config =new Config();
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            config.setConn(DriverManager.getConnection(GlobalData.dburl, GlobalData.userName, GlobalData.password));
            config.setStmt( config.getConn().createStatement());            
            return config;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public  void closeConection(Config config)
	{
            try {
                config.getConn().close();
            } catch (SQLException e) {
		e.printStackTrace();
            }
	}
}
