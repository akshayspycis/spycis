/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.servlet_manager.login_details;  
import com.class_manager.login_details.LoginDetailsMgr;
import com.class_manager.software_details.SoftwareContactDetailsMgr;
import com.class_manager.software_details.SoftwareDetailsMgr;
import com.data_manager.config.GlobalData;
import com.data_manager.login_details.LoginDetails;
import com.data_manager.software_details.SoftwareDetails;
import com.manager.user_details.SetSessionCookie;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class CheckLoginSvr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PrintWriter pw=null;
            try{     
                pw = response.getWriter(); 
                BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
                 String json = "";
                    if(br != null){json  += br.readLine();}
                    JSONObject jObject = new JSONObject(json);
                    LoginDetails login_details = new LoginDetails(); 
                    login_details.setEmail(jObject.getString("email"));
                    login_details.setPassword(jObject.getString("password"));
                    LoginDetailsMgr mgr = new LoginDetailsMgr();
                    JSONObject j=mgr.loadLoginDetails(login_details);
                if (j!=null) {
                        SetSessionCookie ssc = new SetSessionCookie();
                        HttpSession session = request.getSession();
                        if (request.getParameter("JSESSIONID") != null) {
                            Cookie userCookie = new Cookie("JSESSIONID", request.getParameter("JSESSIONID"));
                            response.addCookie(userCookie);
                            session.setAttribute("user_type", j.get("user_type"));
                        } else {
                            String sessionId = session.getId();
                            Cookie userCookie = new Cookie("JSESSIONID", sessionId);
                            response.addCookie(userCookie);
                            session.setAttribute("user_type", j.get("user_type"));
                        }
                        if(jObject.getString("checkbox").equals("true")){
                            response.addCookie(ssc.getCookie("krapofniidresuardowssap",j.get("password")+"ak!)*akkis&*&2918218&*&*&*&kjasdjdj2918239891239",60*60*24*7));    
                        }
                        if(GlobalData.online_user_details==null){
                            GlobalData.online_user_details= new HashMap<String, JSONObject>();
                            GlobalData.online_user_details.put((String)j.get("user_id"), j);
                        }else{
                            GlobalData.online_user_details.put((String)j.get("user_id"), j);
                        }
                        j.put("software_details", new SoftwareDetailsMgr().loadSoftwareDetails((String)j.get("user_id")));
                        pw.print(j);
                        pw.close();
                    } else {
                        pw.print("error");
                        pw.close();
                    }
                } catch (Exception e) {
                    pw.print("error");
                    pw.close();
                    e.printStackTrace();
                }
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
