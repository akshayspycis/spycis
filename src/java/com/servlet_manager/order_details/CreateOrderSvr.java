/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servlet_manager.order_details;

import com.class_manager.checksum_details.ChecksumDetailsMgr;
import com.class_manager.test_details.TestDetailsMgr;
import com.data_manager.config.GlobalData;
import com.manager.order_details.OrderDetails;
import static com.manager.order_details.OrderDetails.getRandomString;
import com.paytm.pg.merchant.CheckSumServiceHelper;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
public class CreateOrderSvr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final String TXN_AMOUNT = "100";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = null;
        try {
            String user_id = request.getParameter("user_id");
            String test_id = request.getParameter("test_id");
            if (user_id != null && test_id != null) {
                String modified_user_id = "CUST" + user_id;
                pw = response.getWriter();
                String orderid = UUID.randomUUID().toString();
                TreeMap<String, String> parameters = new TreeMap<String, String>();
                parameters.put("MID", GlobalData.MID);
                parameters.put("ORDER_ID", orderid);
                parameters.put("CHANNEL_ID", GlobalData.CHANNEL_ID);
                parameters.put("INDUSTRY_TYPE_ID", GlobalData.INDUSTRY_TYPE_ID);
                parameters.put("CUST_ID", modified_user_id);
                parameters.put("TXN_AMOUNT", TXN_AMOUNT);
                parameters.put("WEBSITE", GlobalData.WEBSITE);
                try {
                    TestDetailsMgr mgr = new TestDetailsMgr();
                    ResultSet rs = mgr.loadTestDetailsForPayment(user_id, test_id);
                    while (rs.next()) {
                        System.out.println(rs.getString("email"));
                        System.out.println(rs.getString("contact_no"));
                        parameters.put("EMAIL", rs.getString("email"));
                        parameters.put("MOBILE_NO", rs.getString("contact_no"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                parameters.put("MOBILE_NO", "7777777777");
//                parameters.put("EMAIL", "test@gmail.com");
                parameters.put("CALLBACK_URL", GlobalData.CALLBACK_URL);

                String checksum = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum("NuCU1k%3B9tQNL#Y", parameters);

//            TreeMap<String, String> checksum_detials = new OrderDetails().createOrder("", "");
                StringBuilder outputHtml = new StringBuilder();
                outputHtml.append("<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>");
                outputHtml.append("<html>");
                outputHtml.append("<head>");
                outputHtml.append("<title>Merchant Check Out Page</title>");
                outputHtml.append("</head>");
                outputHtml.append("<body>");
                outputHtml.append("<center><h1>Please do not refresh this page...</h1></center>");
                outputHtml.append("<form method='post' action='"+GlobalData.TransactionURL+"' name='f1'>");
                outputHtml.append("<table border='1'>");
                outputHtml.append("<tbody>");
                System.out.println("-------------------");
//            for (Map.Entry<String, String> entry : checksum_detials.entrySet()) {
//                String key = entry.getKey();
//                String value = entry.getValue();
//                System.out.println(key + " " + value);
//            }
//            System.out.println("-------------------");
                outputHtml.append("<input type='hidden' name='MID' value='" + GlobalData.MID + "'>");
                outputHtml.append("<input type='hidden' name='CHANNEL_ID' value='" + GlobalData.CHANNEL_ID + "'>");
                outputHtml.append("<input type='hidden' name='INDUSTRY_TYPE_ID' value='" + GlobalData.INDUSTRY_TYPE_ID + "'>");
                outputHtml.append("<input type='hidden' name='WEBSITE' value='" + GlobalData.WEBSITE + "'>");
                outputHtml.append("<input type='hidden' name='TXN_AMOUNT' value='" + TXN_AMOUNT + "'>");
                outputHtml.append("<input type='hidden' name='ORDER_ID' value='" + orderid + "'>");
                outputHtml.append("<input type='hidden' name='MOBILE_NO' value='" + parameters.get("MOBILE_NO") + "'>");
                outputHtml.append("<input type='hidden' name='CUST_ID' value='" + parameters.get("CUST_ID") + "'>");
                outputHtml.append("<input type='hidden' name='EMAIL' value='" + parameters.get("EMAIL") + "'>");
                outputHtml.append("<input type='hidden' name='CALLBACK_URL' value='" + GlobalData.CALLBACK_URL + "'>");
                outputHtml.append("<input type='hidden' name='CHECKSUMHASH' value='" + checksum + "'>");
                outputHtml.append("</tbody>");
                outputHtml.append("</table>");
                outputHtml.append("<script type='text/javascript'>");
                outputHtml.append("document.f1.submit();");
                outputHtml.append("</script>");
                outputHtml.append("</form>");
                outputHtml.append("</body>");
                outputHtml.append("</html>");
                System.out.println(outputHtml);
                pw.println(outputHtml);
                pw.close();
                if (checksum != null) {
                    parameters.put("user_id", user_id);
                    parameters.put("test_id", test_id);
                    parameters.put("CHECKSUMHASH", checksum);
                    String checksum_details_id = new ChecksumDetailsMgr().insChecksumDetails(parameters);
                    if (checksum_details_id != null) {
//                    checksum_detials.put("checksum_details_id", checksum_details_id);
                        String random = getRandomString();
//                    checksum_detials.put("random", random);
                        final String ORDER_ID = parameters.get("ORDER_ID");
                        if (GlobalData.paytm_random == null) {
                            GlobalData.paytm_random = new HashMap<String, String>();
                            GlobalData.paytm_checksum_detials = new HashMap<String, TreeMap<String, String>>();
                        }
                        GlobalData.paytm_random.put(parameters.get("ORDER_ID"), random);
                        GlobalData.paytm_checksum_detials.put(parameters.get("ORDER_ID"), parameters);
                        new Thread() {
                            public void run() {
                                loadAutoData();
                            }

                            private void loadAutoData() {
                                try {
                                    Thread.sleep(1000 * 60 * 10);
                                    if (GlobalData.paytm_random != null && ORDER_ID != null && GlobalData.paytm_random.get(ORDER_ID) != null) {
                                        GlobalData.paytm_random.remove(ORDER_ID);
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }.start();

                    }
                }
            }
        } catch (Exception e) {
            pw.print("error");
            pw.close();
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
