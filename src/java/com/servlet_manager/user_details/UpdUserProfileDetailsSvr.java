/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.servlet_manager.user_details;

import com.class_manager.login_details.LoginDetailsMgr;
import com.class_manager.user_details.UserDetailsMgr;
import com.data_manager.login_details.LoginDetails;
import com.data_manager.user_details.UserDetails;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class UpdUserProfileDetailsSvr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        PrintWriter pw=null;
            try{     
                pw = response.getWriter(); 
                BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
                 String json = "";
                    if(br != null){json  += br.readLine();}
                    JSONObject jObject = new JSONObject(json);
                    UserDetails user_details = new UserDetails(); 
                    user_details.setUser_id(jObject.getString("user_id"));
                    user_details.setUser_name(jObject.getString("user_name"));
                    user_details.setDob(jObject.getString("dob"));
                    UserDetailsMgr mgr = new UserDetailsMgr();
                if (mgr.updUserDetails(user_details)) {
                    LoginDetails logindetails = new LoginDetails();
                    logindetails.setUser_id(jObject.getString("user_id"));
                    logindetails.setEmail(jObject.getString("email"));
                    logindetails.setContact_no(jObject.getString("contact_no"));
                    LoginDetailsMgr mh= new LoginDetailsMgr();
                    if (mh.updLoginDetails(logindetails)) {
                        pw.print("true");
                        pw.close();
                    }else{
                        pw.print("error");
                        pw.close();
                    }
                } else {
                    pw.print("error");
                    pw.close();
                }
                } catch (Exception e) {
                    pw.print("error");
                    pw.close();
                    e.printStackTrace();
                }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
