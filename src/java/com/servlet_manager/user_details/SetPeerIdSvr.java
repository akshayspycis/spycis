/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.servlet_manager.user_details;

import com.data_manager.config.GlobalData;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */
public class SetPeerIdSvr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
            PrintWriter pw=null;
            try{     
                pw = response.getWriter(); 
                String user_id=(request.getParameter("user_id") != null) ? request.getParameter("user_id") : "";
                String keyid=(request.getParameter("keyid") != null) ? request.getParameter("keyid") : "";
                System.out.println(user_id);
                if(!keyid.equals("")){
                    if(GlobalData.online_user_details==null){
                            GlobalData.online_user_details= new HashMap<String, JSONObject>();
                            JSONObject j=GlobalData.online_user_details.get(user_id);
                            j.put("keyid", keyid);
                            j.remove("password");
                            GlobalData.online_user_details.put(user_id, j);
                        }else{
                            JSONObject j=GlobalData.online_user_details.get(user_id);
                            j.put("keyid", keyid);
                            j.remove("password");
                            GlobalData.online_user_details.put(user_id, j);
                        }
                    pw.print(new JSONObject(GlobalData.online_user_details));
                    pw.close();
                }else{
                    pw.print("false");
                }
                } catch (Exception e) {
                    pw.print("error");
                    pw.close();
                    e.printStackTrace();
                }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
