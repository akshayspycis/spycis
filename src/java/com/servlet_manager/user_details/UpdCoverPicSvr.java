/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.servlet_manager.user_details;

import com.class_manager.user_details.UserPicDetailsMgr;
import com.data_manager.config.GlobalData;
import com.data_manager.user_details.UserPicDetails;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Akshay
 */
public class UpdCoverPicSvr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final String DATA_DIRECTORY = "cover_pic";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
                PrintWriter pw=null;
        try {
                pw = response.getWriter();
                long file_name=new Date().getTime();
                
        InputStream filecontent = null;
        String str =request.getParameter("images");
        String user_id =request.getParameter("user_id");
        String old_pic =request.getParameter("old_pic");
        
        if(old_pic==null){
//            String uploadFolder = "D://Project/web/mocktest_v/mocktest_v1/build/web/"+DATA_DIRECTORY+File.separator+file_name+ ".png";
          String uploadFolder = "/home/onlinebankexams/webapps/mocktest_v1/"+DATA_DIRECTORY+File.separator+file_name+ ".png";
            String t=str.replaceAll("cover_pic:image/jpeg;base64,", "");
            if(this.uploadFile(t, uploadFolder)){
                UserPicDetails upd = new UserPicDetails();
                upd.setUser_id(user_id);
               upd.setPic("http://43.242.215.132:8061/mocktest_v1/cover_pic/"+file_name+".png");
                UserPicDetailsMgr mgr = new UserPicDetailsMgr();
                if(mgr.insUserPicDetails(upd)) {
                    pw.print("http://43.242.215.132:8061/mocktest_v1/cover_pic/"+file_name+".png");
                }else{
                    pw.print("false");
                }
            }else{
                pw.print("false");
            }
        }else{
                String oldname=old_pic.substring(old_pic.length()-17, old_pic.length());
//                String uploadFolder = "D://Project/web/mocktest_v/mocktest_v1/build/web/"+DATA_DIRECTORY+File.separator+oldname+ ".png";
              String uploadFolder = "/home/onlinebankexams/webapps/mocktest_v1/"+DATA_DIRECTORY+File.separator+oldname;
//        	File olfile = new File("D://Project/web/mocktest_v/mocktest_v1/build/web/cover_pic/"+oldname);
        	File olfile = new File("/home/onlinebankexams/webapps/mocktest_v1/cover_pic/"+oldname);
    		if(olfile.delete()){
                    String t=str.replaceAll("cover_pic:image/jpeg;base64,", "");
                    if(this.uploadFile(t, uploadFolder)){
                        pw.print("true");
                    }else{
                        pw.print("false");
                    }
    		}else{
                    pw.print("false");
                }
        }
        }catch(Exception e){
            e.printStackTrace(GlobalData.ps);
            pw.print("false");
        }
    }
    public boolean uploadFile (String imgurl,String uploadFolder){
            try {
                byte[] decodedBytes = DatatypeConverter.parseBase64Binary(imgurl);
                FileOutputStream fos = new FileOutputStream(uploadFolder);
                fos.write(decodedBytes);
                fos.close();
                return true;
            } catch (Exception ex) {
                return false;
            }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
