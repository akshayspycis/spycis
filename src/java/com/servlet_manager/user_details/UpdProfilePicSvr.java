/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servlet_manager.user_details;

import com.class_manager.user_details.UserPicDetailsMgr;
import com.data_manager.config.GlobalData;
import com.data_manager.user_details.UserPicDetails;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.filechooser.FileSystemView;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Akshay
 */
public class UpdProfilePicSvr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static String DATA_DIRECTORY;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DATA_DIRECTORY = GlobalData.profile_pic_path;
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            long file_name = new Date().getTime();
            InputStream filecontent = null;
            String str = request.getParameter("images");
            String user_id = request.getParameter("user_id");
            String old_pic = request.getParameter("old_pic");
            String t = str.replaceAll("data:image/jpeg;base64,", "");
            if (old_pic == null) {
                String uploadFolder = "/" + DATA_DIRECTORY + File.separator + file_name + ".png";
                if (this.uploadFile(t, uploadFolder, pw)) {
                    UserPicDetails upd = new UserPicDetails();
                    upd.setUser_id(user_id);
                    upd.setPic("../../data/" + file_name + ".png");
                    UserPicDetailsMgr mgr = new UserPicDetailsMgr();
                    if (mgr.insUserPicDetails(upd)) {
                        pw.print("/" + file_name + ".png");
                    } else {
                        pw.print("false");
                    }
                } else {
                    pw.print("false");
                }
            } else {
                String oldname = old_pic.substring(old_pic.length() - 17, old_pic.length());
                String uploadFolder = "/" + DATA_DIRECTORY + File.separator + oldname;
                File olfile = new File(uploadFolder + "/" + oldname);
                if (olfile.delete()) {
                    if (this.uploadFile(t, uploadFolder, pw)) {
                        pw.print("true");
                    } else {
                        pw.print("false");
                    }
                } else {
                    if (this.uploadFile(t, uploadFolder, pw)) {
                        pw.print("true");
                    } else {
                        pw.print("false");
                    }
                }
            }
        } catch (Exception e) {
            pw.print(e.getMessage());
        }
    }

    public boolean uploadFile(String imgurl, String uploadFolder, PrintWriter pw) {
        try {
            byte[] decodedBytes = DatatypeConverter.parseBase64Binary(imgurl);
            FileOutputStream fos = new FileOutputStream(uploadFolder);
            fos.write(decodedBytes);
            fos.close();
            return true;
        } catch (Exception ex) {
            pw.print(ex.getMessage());
            return false;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
