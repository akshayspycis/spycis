/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.servlet_manager.subcategory_details;

import com.class_manager.subcategory_details.SubCategoryDetailsMgr;
import com.data_manager.subcategory_details.SubCategoryDetails;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.manager.section_details.SDMgr;
import com.manager.subcategory_detiails.SCDMgr;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;


/**
 *
 * @author Akshay
 */

public class InsSubCategorySvr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                //response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
               //request.setCharacterEncoding("UTF-8");
        PrintWriter pw=null;
            try{     
//                 BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
//                 String json = "";
//                 if(br != null){
//                        json = br.readLine();
//                    }
//                 System.out.println(json);
//                JSONObject jObject = new JSONObject(json);
                // 3. Convert received JSON to Article
//               response.setContentType("application/json");            
                // 5. Add article to List<Article>
                pw = response.getWriter(); 
                SubCategoryDetails subcatetory_details = new SubCategoryDetails(); 
                subcatetory_details.setCategory_id(request.getParameter("category_id"));
                subcatetory_details.setSubcategory_name(request.getParameter("subcategory_name"));
                SubCategoryDetailsMgr mgr = new SubCategoryDetailsMgr();
                if (mgr.insSubCategoryDetails(subcatetory_details)) {
                        pw.print(new SCDMgr().loadRecentSubCategory());
                        pw.close();
                } else {
                    pw.print("error");
                    pw.close();
                }
                } catch (Exception e) {
                    pw.print("error");
                    pw.close();
                    e.printStackTrace();
                }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
