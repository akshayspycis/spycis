/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.servlet_manager.language_details;

import com.class_manager.language_details.LanguageDetailsMgr;
import com.data_manager.language_details.LanguageDetails;
import com.manager.language_details.LDMgr;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */

public class InsLanguageDetailsSvr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw=null;
            try{     
                pw = response.getWriter(); 
                BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
                 String json = "";
                    if(br != null){json  += br.readLine();}
                    System.out.println(json);
                    JSONObject jObject = new JSONObject(json);
                    LanguageDetails language_details = new LanguageDetails(); 
                    
                    language_details.setLanguage_name(jObject.getString("language_name"));
                    LanguageDetailsMgr mgr = new LanguageDetailsMgr();
                if (mgr.insLanguageDetails(language_details)) {
                        pw.print(new LDMgr().loadRecentLanguage());
                        pw.close();
                } else {
                    pw.print("error");
                    pw.close();
                }
                } catch (Exception e) {
                    pw.print("error");
                    pw.close();
                    e.printStackTrace();
                }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
