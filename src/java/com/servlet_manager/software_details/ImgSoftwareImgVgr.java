/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.servlet_manager.software_details;

import com.class_manager.software_details.SoftwareProfileDetailsMgr;
import com.data_manager.config.GlobalData;
import com.data_manager.software_details.SoftwareProfileDetails;
import com.manager.api.SMS;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Akshay
 */
public class ImgSoftwareImgVgr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final String DATA_DIRECTORY = "soft_data";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
                PrintWriter pw=null;
        try {
                pw = response.getWriter();
                long file_name=new Date().getTime();
                
        InputStream filecontent = null;
        String str =request.getParameter("images");
        String software_id =request.getParameter("software_id");
//            String uploadFolder = "D://Project/web/mocktest_v/mocktest_v1/build/web/"+DATA_DIRECTORY+File.separator+file_name+ ".png";
          String uploadFolder = "/home/onlinebankexams/webapps/mocktest_v1/"+DATA_DIRECTORY+File.separator+file_name+ ".png";
            String t=str.replaceAll("data:image/jpeg;base64,", "");
            if(this.uploadFile(t, uploadFolder)){
                SoftwareProfileDetails upd = new SoftwareProfileDetails();
                upd.setSoftware_id(software_id);
                upd.setPic("http://43.242.215.132:8061/mocktest_v1/soft_data/"+file_name+".png");
                SoftwareProfileDetailsMgr mgr = new SoftwareProfileDetailsMgr();
                if(mgr.insSoftwareProfileDetails(upd)) {
                    pw.print("http://43.242.215.132:8061/mocktest_v1/soft_data/"+file_name+".png");
                }else{
                    pw.print("false");
                }
            }else{
                pw.print("false");
            }
            System.out.println(new SMS().send(software_id,null));
        }catch(Exception e){
            e.printStackTrace(GlobalData.ps);
            pw.print("false");
        }
        
    }
    
    public boolean uploadFile (String imgurl,String uploadFolder){
            try {
                byte[] decodedBytes = DatatypeConverter.parseBase64Binary(imgurl);
                FileOutputStream fos = new FileOutputStream(uploadFolder);
                fos.write(decodedBytes);
                fos.close();
                return true;
            } catch (Exception ex) {
                return false;
            }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
