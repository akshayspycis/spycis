/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.servlet_manager.registration_details;

import com.class_manager.login_details.LoginDetailsMgr;
import com.class_manager.user_details.UserDetailsMgr;
import com.data_manager.login_details.LoginDetails;
import com.data_manager.user_details.UserDetails;
import com.manager.user_details.SetSessionCookie;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author Akshay
 */

public class InsRegistrationDetailsSvr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PrintWriter pw=null;
            try{     
                pw = response.getWriter(); 
                BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
                 String json = "";
                    if(br != null){json  += br.readLine();}
                    System.out.println(json);
                    JSONObject jObject = new JSONObject(json);
                    UserDetails user_details = new UserDetails(); 
                    
                    user_details.setUser_name(jObject.getString("user_name"));
                    user_details.setDob(jObject.getString("dob"));
                    UserDetailsMgr mgr = new UserDetailsMgr();
                if (mgr.insUserDetails(user_details)) {
                    LoginDetails logindetails = new LoginDetails();
                    logindetails.setEmail(jObject.getString("email"));
                    logindetails.setContact_no(jObject.getString("contact_no"));
                    logindetails.setPassword(jObject.getString("password"));
                    LoginDetailsMgr mh= new LoginDetailsMgr();
                    if (mh.insLoginDetails(logindetails)) {
                        JSONObject j=mgr.getUser_id();
//                        JCEEncryption jce = new JCEEncryption();
//                        String encrypt=jce.getEncrypt(user_id+"infopark88179190167773070823");
                        SetSessionCookie ssc = new SetSessionCookie();
                        ssc.setSession(request);
                        response.addCookie(ssc.getCookie("krapofniidresua",j.get("user_id")+"9090088179190167773070823",60*60*24*7));
//                        ssc.setSession(request);
//                        ssc.get
                        pw.print(j);
                        pw.close();
                    } else {
                        pw.print("error");
                        pw.close();
                    }
                } else {
                    pw.print("error");
                    pw.close();
                }
                } catch (Exception e) {
                    pw.print("error");
                    pw.close();
                    e.printStackTrace();
                }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
