/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servlet_manager.setting;

import com.data_manager.config.GlobalData;
import static com.data_manager.config.GlobalData.envv_path;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Akshay
 */
public class CheckSessionSvr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = null;
        try {
            if (!GlobalData.isPathAssigned) {
                pathAssigner();
            }
            pw = response.getWriter();
            if (request.getSession() == null) {
                pw.print("nonoas90asnaksjkasjjasasdjajksdjk");
            } else {
                if (request.getSession().getAttribute("user_type") != null && request.getSession().getAttribute("user_type") != null) {
                    JSONObject jss = new JSONObject();
                    jss.put("ok", "oknonoas90asnaksjkasjjasasdjajksdjk");
                    jss.put("user_type", request.getSession().getAttribute("user_type"));
                    pw.print(jss);
                } else {
                    pw.print("nonoas90asnaksjkasjjasasdjajksdjk");
                }
            }
            pw.close();
        } catch (Exception e) {
            pw.print(e.getMessage());
            pw.close();
            e.printStackTrace();
        }

    }

    public static void pathAssigner() {
//        GlobalData.envv_path = System.getProperty("catalina.base") + "webapps/" + GlobalData.root + "/WEB-INF/classes/com/res/"+GlobalData.environment+".json";
        GlobalData.envv_path = "F:\\project\\spyics\\spycis\\build\\web\\WEB-INF\\classes\\com\\res\\" + GlobalData.environment + ".json";
        GlobalData.profile_pic_path = System.getProperty("catalina.base") + "webapps/" + GlobalData.root + "/data/";
        JSONParser parser = new JSONParser();
        try {
            File file = new File(GlobalData.envv_path);
            if (file.exists()) {
                System.out.println(GlobalData.environment + ".json File is Exits");
                String json = new String(Files.readAllBytes(Paths.get(envv_path)), StandardCharsets.UTF_8);
                JSONObject obj = new JSONObject(json);
                GlobalData.root=obj.getString("root").toString();
                GlobalData.dburl=obj.getString("dburl").toString();
                GlobalData.userName=obj.getString("userName").toString();
                GlobalData.password=obj.getString("password").toString();
                GlobalData.MID=obj.getString("MID").toString();
                GlobalData.MercahntKey=obj.getString("MercahntKey").toString();
                GlobalData.CHANNEL_ID=obj.getString("CHANNEL_ID").toString();
                GlobalData.WEBSITE=obj.getString("WEBSITE").toString();
                GlobalData.CALLBACK_URL=obj.getString("CALLBACK_URL").toString();
                GlobalData.INDUSTRY_TYPE_ID=obj.getString("INDUSTRY_TYPE_ID").toString();
                GlobalData.TransactionURL=obj.getString("TransactionURL").toString();
                GlobalData.TransactionStatusURL=obj.getString("TransactionStatusURL").toString();
                GlobalData.PAYMENT_DETAILS_URL=obj.getString("PAYMENT_DETAILS_URL").toString();
                GlobalData.CREATE_ORDER_URL=obj.getString("CREATE_ORDER_URL").toString();
                GlobalData.isPathAssigned = !GlobalData.isPathAssigned;
                System.out.println("PathAssigned "+GlobalData.isPathAssigned);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
