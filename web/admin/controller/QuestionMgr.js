jQuery.extend({
	QuestionMgr: function(config,model, view){
		var vlist = $.ViewQuestionVgrListener({
			selQuestion : function(category_id,subcategory_id,section_id,topic_id,language_id){
                            var all = model.selQuestion(category_id,subcategory_id,section_id,topic_id,language_id);
                            if(all){
                                $.each(all, function(item){
                                    view.addQuestion(all[item]["question_id"],all[item]);
                                });
                            }
			},
                        delQuestion : function(category_id,subcategory_id,section_id,topic_id){
                                model.delQuestion(category_id,subcategory_id,section_id,topic_id);
			},
                        updQuestion : function(form){
                                model.updQuestion(form);
			},
                        getQuestion : function(category_id,subcategory_id,section_id,topic_id){
                                return model.getQuestion(category_id,subcategory_id,section_id,topic_id);
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.QuestionListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
                        
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(personne,value,module){
                                    view.addQuestion(personne,value);
			},
                        
                        updLoadItem : function(item){
				view.updQuestionRow(item);
			},
                        
                        selLoadBegin:function (module){
                                    view.selLoadBegin();
                        },
                        
                        selLoadFail :function (module){
                                    view.selLoadFail();
                        },
                        
                        selLoadFinish :function (module){
                                    view.selLoadFinish();
                        },
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
