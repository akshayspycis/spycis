jQuery.extend({
	UserListMgr: function(config,model,view){
		var vlist = $.UserListVgrListener({
			selUserList : function(test_id,module){
                            var all = model.selUserList(test_id,module);
                            if(all){
                                $.each(all, function(item){
                                    view.addUserList(all[item]);
                                });
                            }
			},
                        updTestInUser : function(test_id,user_id,test_status){
                                model.updTestInUser(test_id,user_id,test_status);
			},
                        insUserInTest : function(test_id){
                                model.insUserInTest(test_id);
			}
		});
		view.addListener(vlist);

            var mlist = $.UserListListener({
			loadItem : function(item,module){
                                view.addUserList(item);
			},
                        selLoadBegin:function (module){
                                view.selLoadBegin();
                        },
                        selLoadFail :function (module){
                                view.selLoadFail();
                        },
                        selLoadFinish :function (module){
                                view.selLoadFinish();
                        },
                        insLoadBegin:function (){
                            view.insLoadBegin();
                        },
                        insLoadFail :function (){
                            view.insLoadFail();
                        },
                        insLoadFinish :function (){
                            view.insLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
