jQuery.extend({
	DirectionDetailsMgr: function(config,model,view){
            
		var vlist = $.ViewQuestionVgrListener({
			selDirectionDetails : function(discription_id,discription_bank_id,obj){
                            var all = model.selDirectionDetails(discription_id,discription_bank_id,obj);
                            if(all){
                                $.each(all, function(item){
                                    if(module==null){
                                        view.addDirection_Editor(all[item],obj);
                                    }else{
                                        if(obj==2){
                                            config.getView("SelectedQusVgr").addDirection_PopOver(item,obj);
                                        }else{
                                            view.addDirection_PopOver(item,obj);
                                        }
                                    }
                                });
                            }
			},
                        updDirectionDetails : function(discription_bank){
                                model.updDirectionDetails(discription_bank);
			},
                        getDirectionDetails : function(category_id,subcategory_id,section_id,topic_id){
                                return model.getDirectionDetails(category_id,subcategory_id,section_id,topic_id);
			}
		});
        view.addListener(vlist);

            var mlist = $.DirectionDetailsListener({
			loadItem : function(item,obj){
                            
                            if(obj==null){
                                view.addDirection_Editor(item,obj);
                            }else{
                                if(obj==2){
                                    config.getView("SelectedQusVgr").addDirection_PopOver(item,obj);
                                }else{
                                    view.addDirection_PopOver(item,obj);
                                }
                                
                                
                            }
			},
                        updLoadItem : function(item){
				view.updDirectionDetailsRow(item);
			},
                        selLoadBegin:function (module){
                            switch(module){
                                case 1:
                                    config.getView("DirectionDetailsVgr").sectionSubLoadBegin();
                                    break;
                                case 2:
                                    config.getView("DirectionDetailsVgr").sectionModelSubLoadBegin();
                                    break;
                                case 3:
                                    config.getView("DirectionDetailsVgr").topicSubLoadBegin();
                                    break;    
                                case 4:
                                    config.getView("DirectionDetailsVgr").topicModelSubLoadBegin();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                        },
                        selLoadFail :function (module){
                            switch(module){
                                 case 1:
                                    config.getView("DirectionDetailsVgr").sectionSubLoadFail();
                                    break;
                                 case 2:
                                    config.getView("DirectionDetailsVgr").sectionModelSubLoadFail();
                                    break;
                                 case 3:
                                    config.getView("DirectionDetailsVgr").topicSubLoadFail();
                                    break;
                                 case 4:
                                    config.getView("DirectionDetailsVgr").topicModelSubLoadFail();
                                    break;    
                                 default:
                                    view.selLoadFail();
                                    break;
                            }
                        },
                        selLoadFinish :function (module){
                            switch(module){
                                case 1:
                                    config.getView("DirectionDetailsVgr").sectionSubLoadFinish();
                                    break;
                                case 2:
                                    config.getView("DirectionDetailsVgr").sectionModelSubLoadFinish();
                                    break;
                                case 3:
                                    config.getView("DirectionDetailsVgr").topicSubLoadFinish();
                                    break;
                                case 4:
                                    config.getView("DirectionDetailsVgr").topicModelSubLoadFinish();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                            view.selLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
