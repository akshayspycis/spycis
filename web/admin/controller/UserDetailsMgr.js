jQuery.extend({
	UserDetailsMgr: function(config,model, view){
		var vlist = $.UserDetailsVgrListener({
			selAll_UserDetails : function(module){
                            var all = model.selAll_UserDetails(module);
                            if(all){
                                $.each(all, function(item){
                                    view.addUserDetails(all[item]["question_id"],all[item]);
                                });
                            }
			},
			selUserDetails : function(module){
                            var all = model.selUserDetails(module);
                            if(all){
                                $.each(all, function(item){
                                    view.addUserDetails(all[item]["question_id"],all[item]);
                                });
                            }
			},
                        delUserDetails : function(category_id,subcategory_id,section_id,topic_id){
                                model.delUserDetails(category_id,subcategory_id,section_id,topic_id);
			},
                        updUserDetails : function(form){
                                model.updUserDetails(form);
			},
                        getUserDetails : function(category_id,subcategory_id,section_id,topic_id){
                                return model.getUserDetails(category_id,subcategory_id,section_id,topic_id);
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.UserDetailsListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
                        
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(value,module){
                            switch(module){
                                case 1:
                                    config.getView("UserDetailsVgr").subAddInSction(item);
                                    break;
                                case 2:
                                    config.getView("UserDetailsVgr").subAddInSctionModel(item);
                                    break;
                                case 3:
                                    config.getView("UserDetailsVgr").subAddInUserDetails(item);
                                    break;
                                case 4:
                                    config.getView("UserDetailsVgr").subAddInUserDetailsModel(item);
                                    break;    
                                default:
                                    view.addUserDetails(value);
                                    break;
                            }
			},
                        updLoadItem : function(item){
				view.updUserDetailsRow(item);
			},
                        selLoadBegin:function (module){
                            switch(module){
                                case 1:
                                    config.getView("UserDetailsVgr").sectionSubLoadBegin();
                                    break;
                                case 2:
                                    config.getView("UserDetailsVgr").sectionModelSubLoadBegin();
                                    break;
                                case 3:
                                    config.getView("UserDetailsVgr").topicSubLoadBegin();
                                    break;    
                                case 4:
                                    config.getView("UserDetailsVgr").topicModelSubLoadBegin();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                        },
                        selLoadFail :function (module){
                            switch(module){
                                 case 1:
                                    config.getView("UserDetailsVgr").sectionSubLoadFail();
                                    break;
                                 case 2:
                                    config.getView("UserDetailsVgr").sectionModelSubLoadFail();
                                    break;
                                 case 3:
                                    config.getView("UserDetailsVgr").topicSubLoadFail();
                                    break;
                                 case 4:
                                    config.getView("UserDetailsVgr").topicModelSubLoadFail();
                                    break;    
                                 default:
                                    view.selLoadFail();
                                    break;
                            }
                        },
                        selLoadFinish :function (module){
                            switch(module){
                                case 1:
                                    config.getView("UserDetailsVgr").sectionSubLoadFinish();
                                    break;
                                case 2:
                                    config.getView("UserDetailsVgr").sectionModelSubLoadFinish();
                                    break;
                                case 3:
                                    config.getView("UserDetailsVgr").topicSubLoadFinish();
                                    break;
                                case 4:
                                    config.getView("UserDetailsVgr").topicModelSubLoadFinish();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                            view.selLoadFinish();
                        },
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
