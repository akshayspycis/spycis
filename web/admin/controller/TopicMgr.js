jQuery.extend({
	TopicMgr: function(config,model, view){
		var vlist = $.TopicVgrListener({
			insTopic : function(form){
				model.insTopic(form);
			},
			selTopic : function(category_id,subcategory_id,section_id){
                            var all = model.selTopic(category_id,subcategory_id,section_id);
                            if(all){
                                $.each(all, function(item){
                                    view.addTopic(all[item]);
                                });
                            }
			},
                        delTopic : function(category_id,subcategory_id,section_id,topic_id){
                                model.delTopic(category_id,subcategory_id,section_id,topic_id);
			},
                        updTopic : function(form){
                                model.updTopic(form);
			},
                        getTopic : function(category_id,subcategory_id,section_id,topic_id){
                                return model.getTopic(category_id,subcategory_id,section_id,topic_id);
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.TopicListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
                        
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(item,module){
                            switch(module){
                                case 1:
                                    config.getView("DirectionVgr").topAddInDirection(item);
                                    break;
                                case 2:
                                    config.getView("OnlyQuestionVgr").topAddInOnlyQuestion(item);
                                    break;
                                case 3:
                                    config.getView("ViewQuestionVgr").topAddInViewQuestion(item);
                                    break;
                                case 4:
                                    config.getView("ViewQuestionVgr").subAddInTopicModel(item);
                                    break;    
                                default:
                                    view.addTopic(item);
                                    break;
                            }
			},
                        updLoadItem : function(item){
				view.updTopicRow(item);
			},
                        selLoadBegin:function (module){
                            switch(module){
                                case 1:
                                    config.getView("DirectionVgr").directionTopLoadBegin();
                                    
                                    break;
                                case 2:
                                    config.getView("OnlyQuestionVgr").onlyQuestionTopLoadBegin();
                                    break;
                                case 3:
                                    config.getView("ViewQuestionVgr").viewQuestionTopLoadBegin();
                                    break;    
                                case 4:
                                    config.getView("TopicVgr").topicModelSubLoadBegin();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                        },
                        selLoadFail :function (module){
                            switch(module){
                                 case 1:
                                    config.getView("DirectionVgr").directionTopLoadFail();
                                    
                                    break;
                                 case 2:
                                    config.getView("OnlyQuestionVgr").onlyQuestionTopLoadFail();
                                    break;
                                 case 3:
                                    config.getView("ViewQuestionVgr").viewQuestionTopLoadFail();
                                    break;
                                 case 4:
                                    config.getView("TopicVgr").topicModelSubLoadFail();
                                    break;    
                                 default:
                                    view.selLoadFail();
                                    break;
                            }
                        },
                        selLoadFinish :function (module){
                            switch(module){
                                case 1:
                                    config.getView("DirectionVgr").directionTopLoadFinish();
                                    break;
                                case 2:
                                    config.getView("OnlyQuestionVgr").onlyQuestionTopLoadFinish();
                                    break;
                                case 3:
                                    config.getView("ViewQuestionVgr").viewQuestionTopLoadFinish();
                                    break;
                                case 4:
                                    config.getView("TopicVgr").topicModelSubLoadFinish();
                                    break;    
                                default:
                                    view.selLoadFinish();
                                    break;
                            }
                            
                        },
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
