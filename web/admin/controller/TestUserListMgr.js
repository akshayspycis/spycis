jQuery.extend({
	TestUserListMgr: function(config,model,view){
		var vlist = $.TestUserListVgrListener({
			selTestUserList : function(test_id,module){
                            var all = model.selTestUserList(test_id,module);
                            if(all){
                                $.each(all, function(item){
                                    view.addTestUserList(all[item]);
                                });
                            }
			},
                        updTestInUser : function(test_id,user_id,test_status){
                                model.updTestInUser(test_id,user_id,test_status);
			},
                        insUserInTest : function(test_id){
                                model.insUserInTest(test_id);
			}
		});
		view.addListener(vlist);

            var mlist = $.TestUserListListener({
			loadItem : function(item,module){
                                view.addTestUserList(item);
			},
                        selLoadBegin:function (module){
                                view.selLoadBegin();
                        },
                        selLoadFail :function (module){
                                view.selLoadFail();
                        },
                        selLoadFinish :function (module){
                                view.selLoadFinish();
                        },
                        insLoadBegin:function (){
                            view.insLoadBegin();
                        },
                        insLoadFail :function (){
                            view.insLoadFail();
                        },
                        insLoadFinish :function (){
                            view.insLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
