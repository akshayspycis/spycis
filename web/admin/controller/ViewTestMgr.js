jQuery.extend({
	ViewTestMgr: function(config,model, view){
		var vlist = $.ViewTestVgrListener({
			selViewTest : function(category_id,subcategory_id,module){
                            var all = model.selViewTest(category_id,subcategory_id,module);
                            if(all){
                                $.each(all, function(item){
                                    view.addTest(all[item]["test_id"],all[item]);
                                });
                            }
			},
                        delViewTest : function(category_id,subcategory_id,section_id,topic_id){
                                model.delTest(category_id,subcategory_id,section_id,topic_id);
			},
                        updViewTest : function(form){
                                model.updTest(form);
			},
                        updVisibility : function(category_id,subcategory_id,test_id,visible){
                                model.updVisibility(category_id,subcategory_id,test_id,visible);
			},
                        getViewTest : function(category_id,subcategory_id){
                                return model.getTest(category_id,subcategory_id);
                        }
		});
		view.addListener(vlist);

            var mlist = $.ViewTestListener({
			loadItem : function(personne,value,module){
                                view.addTest(personne,value);
			},
                        updLoadItem : function(item){
				view.updTestRow(item);
			},
                        selLoadBegin:function (module){
                                view.selLoadBegin();
                        },
                        selLoadFail :function (module){
                                view.selLoadFail();
                        },
                        selLoadFinish :function (module){
                                view.selLoadFinish();
                        },
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
