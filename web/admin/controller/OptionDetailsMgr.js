jQuery.extend({
	OptionDetailsMgr: function(config,model,view){
            
		var vlist = $.ViewQuestionVgrListener({
                            selOptionDetails : function(question_bank_id,obj){
                            var all = model.selOptionDetails(question_bank_id,obj);
//                            if(all){
//                                $.each(all, function(item){
//                                    if(module==null){
//                                        view.addDirection_Editor(all[item],obj);
//                                    }else{
//                                        view.addOption_PopOver(all[item],obj);
//                                    }
//                                });
//                            }
			},
                        updOptionDetails : function(discription_bank){
                                model.updOptionDetails(discription_bank);
			},
                        getOptionDetails : function(category_id,subcategory_id,section_id,topic_id){
                                return model.getOptionDetails(category_id,subcategory_id,section_id,topic_id);
			}
		});
        view.addListener(vlist);

            var mlist = $.OptionDetailsListener({
			loadItem : function(item,obj){
                            if(obj==null){
                                view.addDirection_Editor(item,obj);
                            }else{
                                view.addOption_PopOver(item,obj);
                            }
			},
                        updLoadItem : function(item){
				view.updOptionDetailsRow(item);
			},
                        selLoadBegin:function (module){
                            switch(module){
                                case 1:
                                    config.getView("OptionDetailsVgr").sectionSubLoadBegin();
                                    break;
                                case 2:
                                    config.getView("OptionDetailsVgr").sectionModelSubLoadBegin();
                                    break;
                                case 3:
                                    config.getView("OptionDetailsVgr").topicSubLoadBegin();
                                    break;    
                                case 4:
                                    config.getView("OptionDetailsVgr").topicModelSubLoadBegin();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                        },
                        selLoadFail :function (module){
                            switch(module){
                                 case 1:
                                    config.getView("OptionDetailsVgr").sectionSubLoadFail();
                                    break;
                                 case 2:
                                    config.getView("OptionDetailsVgr").sectionModelSubLoadFail();
                                    break;
                                 case 3:
                                    config.getView("OptionDetailsVgr").topicSubLoadFail();
                                    break;
                                 case 4:
                                    config.getView("OptionDetailsVgr").topicModelSubLoadFail();
                                    break;    
                                 default:
                                    view.selLoadFail();
                                    break;
                            }
                        },
                        selLoadFinish :function (module){
                            switch(module){
                                case 1:
                                    config.getView("OptionDetailsVgr").sectionSubLoadFinish();
                                    break;
                                case 2:
                                    config.getView("OptionDetailsVgr").sectionModelSubLoadFinish();
                                    break;
                                case 3:
                                    config.getView("OptionDetailsVgr").topicSubLoadFinish();
                                    break;
                                case 4:
                                    config.getView("OptionDetailsVgr").topicModelSubLoadFinish();
                                    break;    
                                default:
                                    view.selLoadBegin();
                                    break;
                            }
                            view.selLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
