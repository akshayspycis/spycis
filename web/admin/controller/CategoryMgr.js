jQuery.extend({
	CategoryMgr: function(model, view){
		var vlist = $.CategoryVgrListener({
			insCategory : function(form){
				model.insCategory(form);
			},
			selCategory : function(){
                            var all = model.selCategory();
                            if(all){
                                $.each(all, function(item){
                                    view.addCategory(all[item]);
                                });
                            }
			},
                        delCategory : function(id){
                                model.delCategory(id);
			},
                        updCategory : function(form){
                                model.updCategory(form);
			},
                        getCategory : function(id){
                                return model.getCategory(id);
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.CategoryListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(item){
				view.addCategory(item);
			},
                        updLoadItem : function(item){
				view.updCategory(item);
			},
                        selLoadBegin:function (){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (){
                            view.selLoadFinish();
                        },
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
