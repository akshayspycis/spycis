jQuery.extend({
	ExamPhaseMgr: function(model, view){
		var vlist = $.ExamPhaseVgrListener({
			insExamPhase : function(form){
				model.insExamPhase(form);
			},
			selExamPhase : function(){
                            var all = model.selExamPhase();
                            if(all){
                                $.each(all, function(item){
                                    view.addExamPhase(all[item]);
                                });
                            }
			},
                        delExamPhase : function(id){
                                model.delExamPhase(id);
			},
                        updExamPhase : function(form){
                                model.updExamPhase(form);
			},
                        getExamPhase : function(id){
                                return model.getExamPhase(id);
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.ExamPhaseListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(item){
				view.addExamPhase(item);
			},
                        updLoadItem : function(item){
				view.updExamPhase(item);
			},
                        selLoadBegin:function (){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (){
                            view.selLoadFinish();
                        },
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
