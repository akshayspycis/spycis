jQuery.extend({
	SelectedQus: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
                
		function toArray(){
			var item = [];
                            try {
                                    $.each(cache,function(j){
                                                sec_item = {}
                                                sec_item ["question_id"] = cache.question_id;
                                                sec_item ["question_name"]= cache.question_name;
                                                item.push(sec_item);
                                    });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json response from an
		 * ajax call
		 */
		function loadResponse(data){
                    that.selLoadFinish();
                    that.questionItemLoaded(data);
                }
                
		this.selSelectedQus = function(language_id,question_id){
                    that.selLoadFinish();
                    that.selLoadBegin();
			$.ajax({
                                url: config.getU()+"/SelSelectedQusDetailsSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'question_id':question_id,'language_id':language_id},
				error: function(){
                                    that.selLoadFinish();
                                    that.selLoadFail();
				},
				success: function(data){
                                    that.selLoadFinish();
                                    loadResponse(data) ;
				}
			});
		}
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin();
			});
		}
                this.selLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish();
			});
		}
                this.selLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadFail();
			});
		}
                
		this.questionItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].loadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	SelectedQusListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { }
		}, list);
	}
});
