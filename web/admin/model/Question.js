jQuery.extend({
	Question: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
		function toArray(category_id,subcategory_id,section_id,topic_id,language_id){
			var item = [];
                            try {
                                $.each(cache[category_id][subcategory_id][section_id][topic_id],function(j){
                                        if(cache[category_id][subcategory_id][section_id][topic_id][j][language_id]!=null){
                                            top_item = {}
                                            top_item ["question_id"] = j;
                                            top_item ["discription_id"] = cache[j][language_id].discription_id;
                                            top_item ["question_bank_id"] = cache[j][language_id].question_bank_id;
                                            top_item ["question"]= cache[j][language_id].question;
                                            top_item ["discription_bank_id"]= cache[j][language_id].discription_bank_id;
                                            item.push(top_item);
                                        }
                                    });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json` response from an
		 * ajax call
		 */
		function loadResponse(data,category_id,subcategory_id,section_id,topic_id,language_id,module){
                    $.each(data.q, function (personne,value) {
                        if(cache[category_id]==null){
                            cache[category_id]={}
                            cache[category_id][subcategory_id]={}
                            cache[category_id][subcategory_id][section_id]={}
                            cache[category_id][subcategory_id][section_id][topic_id]={}
                            cache[category_id][subcategory_id][section_id][topic_id][personne]={}
                            cache[category_id][subcategory_id][section_id][topic_id][personne][language_id]=value
                        }else{
                            if(cache[category_id][subcategory_id]==null){
                                cache[category_id][subcategory_id]={}
                                cache[category_id][subcategory_id][section_id]={}
                                cache[category_id][subcategory_id][section_id][topic_id]={}
                                cache[category_id][subcategory_id][section_id][topic_id][personne]={}
                                cache[category_id][subcategory_id][section_id][topic_id][personne][language_id]=value
                            }else{
                                if(cache[category_id][subcategory_id][section_id]==null){
                                    cache[category_id][subcategory_id][section_id]={}
                                    cache[category_id][subcategory_id][section_id][topic_id]={}
                                    cache[category_id][subcategory_id][section_id][topic_id][personne]={}
                                    cache[category_id][subcategory_id][section_id][topic_id][personne][language_id]=value
                                }else{
                                    if(cache[category_id][subcategory_id][section_id][topic_id]==null){
                                        cache[category_id][subcategory_id][section_id][topic_id]={}
                                        cache[category_id][subcategory_id][section_id][topic_id][personne]={}
                                        cache[category_id][subcategory_id][section_id][topic_id][personne][language_id]=value
                                    }else{
                                        cache[category_id][subcategory_id][section_id][topic_id][personne]={}
                                        cache[category_id][subcategory_id][section_id][topic_id][personne][language_id]=value
                                    }
                                }
                            }
                        }
                        that.topicItemLoaded(personne,value); 
                    });
		}
                
                
                this.updQuestionDetails =function (question_bank){
                    that.updLoadBegin();
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/UpdQuestionDetailsSvr",
                        data: JSON.stringify(question_bank),
                        success: function(data)
                        {
                            if(data.trim()=="true"){
                                that.updLoadFinish();
                            }else{
                                that.updLoadFail();
                            }
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.updLoadFinish();
                            that.updLoadFail();
                        }
                    });
                }
		
		/**
		 * load lots of data from the server
		 * or return data from cache if it's already
		 * loaded
		 */
                
		this.selQuestion = function(category_id,subcategory_id,section_id,topic_id,language_id,module){
                    var outCache = toArray(category_id,subcategory_id,section_id,topic_id,language_id);
//			if(outCache.length) return outCache;
			that.selLoadBegin(module);
			$.ajax({
                                url: config.getU()+"/SelQuestionSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'category_id':category_id,'subcategory_id':subcategory_id,'section_id':section_id,'topic_id':topic_id,'language_id':language_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish(module);
                                    if(data.q!="")
                                    loadResponse(data,category_id,subcategory_id,section_id,topic_id,language_id,module) ;
				}
			});
		}
                
                this.delQuestion = function(category_id,subcategory_id,section_id,topic_id){
                        delResponse(category_id,subcategory_id,section_id,topic_id);
                	that.delLoadBegin();
			$.ajax({
				url: config.getU()+"/DelQuestionDetailsSvr",
				type: 'GET',
                                data : {id : topic_id },
				error: function(){
					that.delLoadFail();
				},
				success: function(data){
                                    if(data=="ok"){
                                        that.delLoadFinish();
                                    }
				}
			});
		}
                
		/**
		 * load lots of data from the server
		 */
		this.clearAll = function(){
			cache = new Array();
		}
		/**
		 * add a listener to this model
		 */
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.delLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadBegin();
			});
		}
                this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadBegin();
			});
		}

		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.delLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFinish();
			});
		}
                this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                
		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
                
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
                
		/**
		 * tell everyone the item we've loaded
		 */
                
		this.topicItemLoaded = function(personne,value,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(personne,value,module);
			});
		}
                
                this.updItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].updLoadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	QuestionListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			loadItem : function() { },
                        updLoadItem: function() { },
			insLoadFail : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
