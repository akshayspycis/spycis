jQuery.extend({
	UploadImg: function(obja){
                this.up_obj = obja;
                var items = "";
                this.all = {}
                var that = this;
		var listeners = new Array();
                var fileinput = document.getElementById(this.up_obj.multiUpload);
                var max_width = fileinput.getAttribute('data-maxwidth');
                var max_height = fileinput.getAttribute('data-maxheight');
                var form = document.getElementById(this.up_obj.form);
		/**
		 * get contents of cache into an array
		 */
                this._init = function(){
                    if (window.File && window.FileReader && window.FileList && window.Blob) {		
                             var inputId = $("#"+that.up_obj.form).find("input[type='file']").eq(0).attr("id");
                             document.getElementById(inputId).addEventListener("change", that._read, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("dragover", function(e){ e.stopPropagation(); e.preventDefault(); }, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("drop", that._dropFiles, false);
                             document.getElementById(that.up_obj.form).addEventListener("submit", that._submit, false);
                    } else console.log("Browser supports failed");
                    
                }
                
                $("#"+this.up_obj.dragArea).click(function() {
                    fileinput.click();
                });
                
                this._submit = function(e){
                    e.stopPropagation(); 
                    e.preventDefault();
                    that._startUpload();
            	}
                
                this._preview = function(data){
                    this.items = data;
                    if(this.items.length > 0 && this.items.length <10){
                        var html;		
                        var uId = "";
                        for(var i = 0; i<this.items.length; i++){
                            uId = this.items[i].name._unique();
                            obj={};
                            obj["file"]=this.items[i];
                            that.all[uId]=obj;
                            var kk=$('<a></a>').append($("<i></i>").addClass('fa  fa-close ')).css({'position':'absolute','right':'5px','top':'2px'}).click(function (e){e.stopPropagation();
                                that._deleteFiles($(this).parent().parent().attr('rel'));
                                $(this).parent().parent().remove();
                            });
                            var sampleIcon = 'fa fa-image';
                            var errorClass = "";
                            if(typeof this.items[i] != undefined){
                                if(that._validate(this.items[i].type) <= 0) {
                                    sampleIcon = 'fa fa-exclamation';
                                    errorClass =" invalid";
                                } 
                                html=$('<div></div>').addClass('dfiles'+errorClass).attr({'rel':uId,'id':'as'+uId})
                                
                                    .append($('<h5></h5>').append('<i class="'+sampleIcon+'"></i>&nbsp&nbsp'+this.items[i].name.substring(1, this.up_obj.strlenght)+'"</i>')
                                    .append('<img src="../../dist/images/loading_1.gif" style="position:absolute;right:20px;top:2px;height:20px;width:20px;"/>')
                                    .append(kk))
                                    .click(function (e){
                                        e.stopPropagation();
                                });
                            }
                            $("#"+this.up_obj.dragArea).append(html);
                                that.readfiles(this.items[i],uId)
                            }
                            }else{
                                alert("Image file select limit maximum is 10 files.")
                            }
                        }
                        
                this._refresh =function (){
                    $("#"+this.up_obj.dragArea).append(html);
                    this.all={};
                }        
                        
                this.setStream = function(stream,sid){
                    that.all[sid]["byte_stream"]=stream;
                    $(".dfiles[rel='"+sid+"'] >h5>img").remove();
                }
        
                this._read = function(evt){
                        if(evt.target.files){
                            that._preview(evt.target.files);
                            //that._preview(evt.target.files);
                        } else 
                            console.log("Failed file reading");
                }
	
                this._validate = function(format){
                        var arr = this.up_obj.support.split(",");
                        return arr.indexOf(format);
                }
	
                this._dropFiles = function(e){
                        e.stopPropagation(); 
                        e.preventDefault();
                        that._preview(e.dataTransfer.files);
                }
                
                this._deleteFiles = function(key){
                    delete that.all[key];
                }
                
                this._uploader = function(file,key){
                    if(typeof file != undefined && that._validate(file["file"].type) > 0){
			var data = new FormData();
			var ids = file["file"].name._unique();
			data.append('images',file["byte_stream"]);
			data.append('index',ids);
			$(".dfiles[rel='"+ids+"'] >h5").append('<img src="../../dist/images/fb_ty.gif" style="position:absolute;right:20px;top:2px;height:20px;width:20px;"/>')
			$.ajax({
				type:"POST",
                                url:that.up_obj.url,
				data:{'images':file["byte_stream"]},
				success:function(url){
                                    $(".dfiles[rel='"+ids+"']").remove();
                                    that.all[key]["url"]=url;
                                    switch(that.up_obj.module){
                                        case 1:
                                            that.up_obj.parent_obj.addImgDscription(that.all[key]);
                                            break;
                                        case 2:
                                            that.up_obj.parent_obj.addImgQuestion(that.all[key]);
                                            break;
                                        case 3:
                                            break;
                                        
                                    }
                                    
                                    
                                    that._deleteFiles(key);
				}
			});
                    }else {
                        console.log("Invalid file format - "+file.name);
                    }
                }
                
                this._startUpload = function(){
                    
                    $.each(that.all,function(key,value){
                        that._uploader(value,key);
                    });
                } 
        
                String.prototype._unique = function(){
                        return this.replace(/[a-zA-Z]/g, function(c){
                            return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
                        });
                }
                
                this.processfile =function(file,sid) {
            if( !( /image/i ).test( file.type )){
                alert( "File "+ file.name +" is not an image." );
                return false;
            }
            // read the files
              var reader = new FileReader();
              reader.readAsArrayBuffer(file);
              reader.onload = function (event) {
              var blob = new Blob([event.target.result]); // create blob...
              window.URL = window.URL || window.webkitURL;
              var blobURL = window.URL.createObjectURL(blob); // and get it's URL
              var image = new Image();
              image.src = blobURL;
              image.onload = function() {
              that.setStream(that.resizeMe(image,sid),sid)
              }
            };
        }

                this.readfiles=function(files,sid) {
                    that.processfile(files,sid); // process each file at once
                }

                this.resizeMe=function (img,sid) {
           var canvas = document.createElement('canvas');
           var width = img.width;
           var height = img.height;
          // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                  //height *= max_width / width;
                  height = Math.round(height *= max_width / width);
                  width = max_width;
                }
            } else {
                if (height > max_height) {
                  //width *= max_height / height;
                  width = Math.round(width *= max_height / height);
                  height = max_height;
                }
            }
  
             // resize the canvas and draw the image data into it
              canvas.width = width;
              canvas.height = height;
              var ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0, width, height);
              var canvas1 = document.createElement('canvas');
              canvas1.width = 50;
              canvas1.height = 50;
              var ctx = canvas1.getContext("2d");
              ctx.drawImage(img, 0, 0, 50, 50);
              that.all[sid]["image"]=canvas1;
             // preview.appendChild(canvas1); // do the actual resized preview
              return canvas.toDataURL("image/jpeg",0.7); // get the data from canvas as 70% JPG (can be also PNG, etc.)

        }
        
                this._init();
        }
});
