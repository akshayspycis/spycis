jQuery.extend({
	ViewTest: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
		function toArray(category_id,subcategory_id){
			var item = [];
                            try {
                                    $.each(cache[category_id][subcategory_id][section_id],function(j){
                                                top_item ;
                                                top_item = cache[category_id][subcategory_id][j];
                                                top_item ["test_id"]= j;
                                                item.push(top_item);
                                    });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json` response from an
		 * ajax call
		 */
		function loadResponse(data,category_id,subcategory_id,module){
                    $.each(data.test_details, function (personne,value) {
                        if(cache[category_id]==null){
                            cache[category_id]={}
                            cache[category_id][subcategory_id]={}
                            cache[category_id][subcategory_id][personne]=value
                        }else{
                            if(cache[category_id][subcategory_id]==null){
                                cache[category_id][subcategory_id]={}
                                cache[category_id][subcategory_id][personne]=value
                            }else{
                                cache[category_id][subcategory_id][personne]=value
                            }
                        }
                        that.topicItemLoaded(personne,value); 
                    });
		}
                
                function updVisibilityInCache(category_id,subcategory_id,test_id,visible){
                    cache[category_id][subcategory_id][test_id]["visible"]=visible;
                }
                
                function updResponse(data){
                    cache[data.category_id][data.subcategory_id][data.section_id][data.topic_id].topic_name=data.topic_name;
                    that.updItemLoaded(data);
		}
                function delResponse(category_id,subcategory_id,section_id,topic_id){
                        delete cache[category_id][subcategory_id][section_id][topic_id];
		}
		/**
		 * look in the cache for a single item
		 * if it's there, get it, if not
		 * ask the server to load it
		 */
		this.getViewTest = function(category_id,subcategory_id,section_id,topic_id){
                            return cache[category_id][subcategory_id][section_id][topic_id].topic_name;
		}
                this.updViewTest =function (form){
                    item = {}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="subcategory_id"){
                                item ["subcategory_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="category_id"){
                                item ["category_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="section_id"){
                                item ["section_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="topic_id"){
                                item ["topic_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="topic_name"){
                                item ["topic_name"]= $(this).val();
                            }
                        });
                        updResponse(item);
                        that.updLoadBegin();
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/UpdViewTestDetailsSvr",
                        data: form.serialize(), // serializes the form's elements.
                        success: function(data)
                        {
                            that.updLoadFinish();
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.updLoadFinish();
                            that.updLoadFail();
                        }
                    });
                }
                
                
		this.selViewTest = function(category_id,subcategory_id,module){
                    that.selLoadBegin(module);
                    var outCache = toArray(category_id,subcategory_id);
			if(outCache.length) return outCache;
			$.ajax({
                                url: config.getU()+"/SelTestDetailsSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'category_id':category_id,'subcategory_id':subcategory_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish(module);
                                    if(data!="")
                                    loadResponse(data,category_id,subcategory_id,module) ;
				}
			});
		}
                
		this.updVisibility = function(category_id,subcategory_id,test_id,visible){
                    updVisibilityInCache(category_id,subcategory_id,test_id,visible)
			$.ajax({
                                url: config.getU()+"/UpdTestVisibiltySvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'test_id':test_id,'visible':visible},
				error: function(){
				},
				success: function(data){
				}
			});
		}
		
		
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(module){
                    
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                
                this.delLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadBegin();
			});
		}
                
                this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadBegin();
			});
		}

		/**
		 * we're done loading, tell everyone
		 */
		
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                
                this.delLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFinish();
			});
		}
                
                this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
                
		/**
		 * tell everyone the item we've loaded
		 */
                
		this.topicItemLoaded = function(item,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,module);
			});
		}
                
                this.updItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].updLoadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	ViewTestListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        updLoadItem: function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
