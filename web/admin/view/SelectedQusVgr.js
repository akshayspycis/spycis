jQuery.extend({
	SelectedQusVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody;
                var direction;
                var question_id;
                var question;
                var option;
                var correct_ans;
                var direction_statuc;
                var discription_bank_id;
                var discription_id;
                var path;
                var temp_obj_for_popover;
                
                this.getSelectedQus = function(question_id,answer,select,path){
                    that.question_id=question_id;
                    that.answer=answer;
                    that.select=select;
                    that.path=path;
                    correct_ans=$("<b>").addClass("fa-2x");
                        var box=that.getBoxbody();
                            box.append(that.getQuestionDetails());
                            box.append(that.getOptionDetails());
                            box.append($("<a>").addClass("btn btn-primary btn-xs")
                                    .append("?")
                               );
                            box.append($("<a>")
                                    .append($("<label>").append("&nbsp;Correct Answered"))
                                    .append($("<span>").addClass("pull-right").append(correct_ans))
                               );
                       
                        return box;
		}
                
                this.getQuestionDetails= function (){
                    question=$("<b>");
                    direction_statuc=$("<span>").addClass("fa fa-exclamation-circle");
                    return $("<div>").addClass("panel box box-primary")
                              .append($("<div>").addClass("box-header with-border")
                                    .append($("<h4>").addClass("box-title")
                                        .append($("<a>").append("Question ID # "+that.question_id))
                                    )
                                    .append($("<div>").addClass("pull-right")   
                                        .append($("<button>").addClass("btn btn-default").append(direction_statuc)
                                            .popover({
                                                trigger: "hover",
                                                placement: "left",
                                                title: "Direction Details",
                                                content: config.getLoadingData(),
                                                html:true
                                            })
                                            .on('shown.bs.popover', function(){
                                                     that.loadDiscriptionDetails($(this),discription_id,discription_bank_id)
                                            })
                                        )
                                    )
                               )
                              .append($("<div>").addClass("panel-collapse")
                                    .append($("<div>").addClass("box-body")   
                                        .append(question)
                                    )
                               );
                }
                this.getOptionDetails= function (){
                    option=$("<div>");
                    return $("<div>").addClass("panel box box-danger")
                              .append($("<div>").addClass("panel-collapse")
                                    .append($("<div>").addClass("box-body")   
                                        .append(option)
                                    )
                               )
                              ;
                }
                
                this.getLabel = function (fontcolor,option_name,options,icon){
                    return $("<div>")
                            .append($("<a>").addClass(fontcolor)
                            .append($("<label>")
                                .append(option_name+"&nbsp;&nbsp;")
                                .append(options+"&nbsp;&nbsp;")
                                )
                            .append($("<span>").addClass("fa pull-right "+icon)));
                }
                this.setQuestion= function (data){
                    discription_bank_id=data.discription_bank_id;
                    discription_id=data.discription_id;
                    if(data.discription_bank_id==null){
                        direction_statuc.removeClass("fa-arrow-circle-right");
                        direction_statuc.addClass("fa-exclamation-circle");
                    }else{
                        direction_statuc.removeClass("fa-exclamation-circle");
                        direction_statuc.addClass("fa-arrow-circle-right");
                    }
                    question.append(decodeURIComponent(data["question"]));
                    var keys = [];
                    var o=data["option_details"];
                    for (var k in o) {
                        if (o.hasOwnProperty(k)) {
                            keys.push(k);
                        }
                    }
                    keys.sort();
                    for (i = 0; i < keys.length; i++) {
                    var key = keys[i];
                        var value=o[key];
                        if(that.select!=null){
                            if(key==that.select){
                                if(key==that.answer){
                                    option.append(that.getLabel("text-green",key,decodeURIComponent(value),"fa-check"))
                                }else{
                                    option.append(that.getLabel("text-red",key,decodeURIComponent(value),"fa-times"))
                                }
                            }else{
                                option.append(that.getLabel("text-black",key,decodeURIComponent(value),""))
                            }
                        }else{
                            option.append(that.getLabel("text-black",key,decodeURIComponent(value),""))
                        }
                    }
                    correct_ans.append(that.answer);

                }
                this.loadDiscriptionDetails = function (a,discription_id,discription_bank_id){
                    temp_obj_for_popover=a;
                    if(discription_bank_id==null){
                        temp_obj_for_popover.attr('data-content', "No Discription");
                        temp_obj_for_popover.data('bs.popover').setContent();
                    }else{
                            if(config.getModel("DirectionDetails")==null){
                                
                            $.getScript(that.path+"model/DirectionDetails.js").done(function() {
                                    try{
                                        config.setModel("DirectionDetails",new $.DirectionDetails(config));
                                        $.getScript(that.path+"/controller/DirectionDetailsMgr.js").done(function() {
                                        try{
                                            config.setController("DirectionDetailsMgr",new $.DirectionDetailsMgr(config,config.getModel("DirectionDetails"),that));
                                            $.each(listeners, function(i){
                                            try {
                                                listeners[i].selDirectionDetails(discription_id,discription_bank_id,2);
                                                return;
                                            }catch(e){}
                                            });
                                        }catch(e){
                                            alert(e+" in load Direction Details Mgr");
                                        }
                                        }).fail(function() {alert("Error:Direction DetailsMgr Js Loading problem");}); 
                                    }catch(e){
                                        alert(e+" in load Direction Details");
                                    }
                                }).fail(function() {alert("Error:Direction Details Js Loading problem");}); 
                        }else{
                            $.each(listeners, function(i){
                                                try {
                                                    listeners[i].selDirectionDetails(discription_id,discription_bank_id,a);
                                                }catch(e){}
                                            });
                        }
                    }
                    
                }
                
                
                this.addDirection_PopOver = function (dis,obj){
                    var a="<div style='width:400px;'>"+decodeURIComponent(dis)+"<div>"
                    temp_obj_for_popover.attr('data-content', a);
                    temp_obj_for_popover.data('bs.popover').setContent();
                }
                
                
                this.loadSelectedQus =function (){
                    $.each(listeners, function(i){
                        listeners[i].selSelectedQus(config.getView("ResultSummeryVgr").getLangId(),that.question_id);
                    });
                } 
                
                this.getBoxbody = function (){
                    var div =$("<div>").addClass("no-padding");
                    return div;
                }
                
                this.selLoadBegin =function (){
                    question.append(config.getLoadingData());
                    option.append(config.getLoadingData());
                }
                this.selLoadFinish=function (){
                    correct_ans.empty();
                    question.empty();
                    option.empty();
                }
                this.selLoadFail = function() { 
                    question.append($("<p>").append("Server Doesn't Response"));
                    option.append($("<p>").append("Server Doesn't Response"));
                }
                this.addListener = function(list){
                            listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	SelectedQusVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insSelectedQus : function(from){},
			selDirectionDetails : function() { },
                        delSelectedQus : function(id) { },
                        getSelectedQus : function(id) { },
                        updSelectedQus : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	},
	ViewQuestionVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selDirectionDetails : function() { }
		}, list);
	}
});
