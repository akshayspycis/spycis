jQuery.extend({
	HeaderVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
		var temp_obj ;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
                this.setHeader = function(){
			var header =$("<header></header>").addClass("main-header");
                        header.append(that.setLogo);
                        var nav =$("<nav></nav>").addClass("navbar navbar-static-top").attr("role","navigation");
                        nav.append(that.setMenuIcon());
                        nav.append(that.setProfile());
                        header.append(nav);
                        return header;
		}

                this.setLogo = function (){
                    var logo_anchaor =$("<a></a>").addClass("logo");
                    logo_anchaor.append($("<span>").addClass("logo-mini").append($("<b>").append($("<img>").attr({'src':'../../dist/images/log.png'})).css({'font-family': 'freedom'})));
                    logo_anchaor.append($("<span>").addClass("logo-lg").css({'margin-top':'-5px'}).append($("<img>").attr({'src':'../../dist/img/logo_1.png','width':'200px'})));
                    return logo_anchaor;
                }
                
                this.setMenuIcon = function (){
                    var anchor =$("<a></a>").addClass("sidebar-toggle").attr({'data-toggle':'offcanvas','role':'button'})
                            .append($("<span>Toggle navigation</span>").addClass("sr-only"))
                            .append($("<span></span>").addClass("icon-bar"))
                            .append($("<span></span>").addClass("icon-bar"))
                            .append($("<span></span>").addClass("icon-bar"));
                    return anchor;
                }
                
                this.setProfile = function (){
                    var div =$("<div></div>").addClass("navbar-custom-menu");
                    div.append($("<ul>").addClass("nav navbar-nav")
                                .append($("<li>")
                                    .append($("<a>").css({'cursor':'pointer'})
                                            .append("Home")
                                            .click(function (){
                                                config.getView("MainSidebarVgr").getSidebarMenu().find("li:nth-child(1)").find("#dash_home").trigger("click");
                                            })
                                        ))
                                .append($("<li>").addClass("dropdown user user-menu")
                                    .append($("<a>").addClass("dropdown-toggle").css({'cursor':'pointer'})
                                            .append(config.getModel("ProfileConfig").getProfilePic("user-image"))    
                                            .append($("<span>").addClass("hidden-xs").append(config.getModel("ProfileConfig").getFirstName()))
                                            .attr({'data-toggle':'dropdown'})
                                        )
                                    .append($("<ul>").addClass("dropdown-menu")
                                        .append($("<li>").addClass("user-header")
                                            .append(config.getModel("ProfileConfig").getProfilePic("img-circle"))
                                            .append($("<p>").append(config.getModel("ProfileConfig").getFirstName())
                                            .append($("<small>").append("Candidate Id  V"+config.getModel("ProfileConfig").getUser_id())))
                                        )
                                        .append($("<li>").addClass("user-footer")
                                            .append($("<div>").addClass("pull-left")
                                                    .append(that.getButton("Profile","profile",that.callback))
                                             )
                                            .append($("<div>").addClass("pull-right")
                                                    .append(that.getButton("LogOut","logout",that.logOut))
                                             )
                                        )
                                     )
                                ));
                    return div;
		}
                
                this.getButton = function (title,id,callback){
                  return $("<button>").addClass("btn btn-default btn-flat").append(title).attr({'id':id})
                     .click(function(){
                         callback($(this));
                      });
                }
                this.callback = function (title,id,callback){
                  
                }
                this.logOut = function (obj){
                  temp_obj=obj;
                  config.getModel("ProfileConfig").logOut();
                }
                this.setLoadBegin =function (){
                    temp_obj.prop('disabled', false);
                    temp_obj.empty();
                    temp_obj.append(config.getLoadingData());
                }
                this.setLoadFinish =function (){
                    temp_obj.prop('disabled', true);
                    temp_obj.empty();
                    temp_obj.append("LogOut");
                }
                this.addListener = function(list){
                    listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	HeaderVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
