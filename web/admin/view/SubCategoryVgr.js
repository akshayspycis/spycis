jQuery.extend({
	SubCategoryVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody;
                var sub_cat;
                var row_id;
                var cat_dorp_down;
                var count=0;
                
                this.getSubCategory = function(){
                    config.getView("ContentWrapperVgr").setBoxHeader("Configuration");
			sub_cat =$("<div></div>").addClass("col-md-6");
                        sub_cat.append(that.getPanelTitle);
                        var box=that.getBoxbody();
                        var table = that.getTable();
                        tbody=table.find("tbody");
                        box.append(that.getCategory_DropDown);
                        box.append(table);
                        sub_cat.append(box);
                        sub_cat.append(that.getBoxFooter());
                        //that.loadSubCategory();
                        //-----------this code is resposible for load category and load subcatgory--------------------------
                        that.loadCategory();
                        cat_dorp_down.find(".select2").trigger("change");
                        return sub_cat;
		}

                this.getPanelTitle = function (){
                    var div =$("<div></div>").addClass("box-header with-border");
                    div.append($("<h3></h3>").addClass("box-title").append($("<b>Sub Category</b>")));
                    return div;
                }
                
                this.getCategory_DropDown = function (){
                    cat_dorp_down =$("<div></div>").addClass("form-group");
                    cat_dorp_down.append($("<label>Category :</label>"))
                    cat_dorp_down.append($("<select>Category :</select>").addClass("form-control select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var id = cat_dorp_down.find(".select2 option:first").val();
                        $( "select option:selected" ).each(function() {
                          id = $( this ).val();
                        });
                        
                        that.loadSubCategory (id);
                    });
                    return cat_dorp_down;
                }

                this.getBoxbody = function (){
                    var div =$("<div></div>").addClass("box-body").attr({'id':'sub'});
                    return div;
                }
                
                this.getTable = function (){
                    var div =$("<table></table>").addClass("table table-bordered")
                    //set header for table 
                            .append($("<thead></thead>")
                                .append($("<tr></tr>")
                                .append($("<th></th>").append("#").css({'width':'10px'}))
                                .append($("<th></th>").append("SubCategory Name"))
                                .append($("<th></th>").append("Action").css({'width':'40px'}))
                            ))
                            //set table data
                            .append($("<tbody></tbody>").attr({'id':'sub_cat_tb'}))
                    return div;
                }
                
                this.getRow = function (data){
                    count++;
                    var row = $("<tr></tr>").attr({'id':data.subcategory_id})
                            .append($("<td>"+count+"</td>"))
                            .append($("<td>"+data.subcategory_name+"</td>"))
                            .append($("<td></td>").append(that.getDropdown(data.subcategory_id))
                    );
                    return row;            
                }
                
                this.getOption = function (data){
                    var row = $("<option></option>").attr({'value':data.category_id})
                              .append($("<td>"+data.category_name+"</td>"));
                    return row;            
                }
                
                this.getDropdown = function (a){
                    var drop_down =$("<div></div>").addClass("input-group-btn")
                                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                        .append($("<span></span>").addClass("fa fa-caret-down")))
                                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li></li>").append($("<a>Delete</a>")))
                                            .append($("<li></li>").addClass("divider"))
                                            .append($("<li></li>").append($("<a>Edit</a>"))));
                    
                    return that.getClickEvent(drop_down,a);
                }
                
                this.getClickEvent =function (drop_down,id){
                    $(drop_down).find('ul>li:nth-child(1)').click(function (){
                        var category_id = cat_dorp_down.find(".select2 option:selected").val();
                        var obj={};
                        obj["id"]=id
                        obj["category_id"]=category_id
                        var model = config.getView("ErrorMsgVgr").getErrorMsg("Delete SubCategory","Are you sure you want to delete this SubCategory?",obj,that.delSubCatgory);
                        $("#hidden").empty();
                        $("#hidden").append(model);
                        model.modal('show');
                    });
                    $(drop_down).find('ul>li:nth-child(3)').click(function (){
                        var category_id = cat_dorp_down.find(".select2 option:selected").val();
                        that.updCatgory($("#hidden"),id,category_id);
                    });
                    return drop_down;
                }
                
                this.delSubCatgory =function (obj){
                    row_id =obj["id"];
                    $.each(listeners, function(i){
                        listeners[i].delSubCategory(obj["id"],obj["category_id"]);
                    });
                    return true;
                }
                
                this.updCatgory =function (div,id,category_id){
                    that.setUpdForm(div,id,category_id);
                }
                
                this.getBoxFooter =function (){
                    var div = $("<div></div>").addClass("box-footer clearfix");
                        div.append($("<div></div>").addClass("col-md-3 col-xs-5")
                                .append($("<button></button>").addClass("btn btn-block btn-primary").attr({'data-toggle':'modal','data-target':'myModal'})
                                    .append("New").click(function ()
                                        {
                                            that.setInsForm($("#hidden"))
                                        }
                                     )
                                    ))
                        div.append($("<div></div>").addClass("col-md-3 col-xs-6 pull-right")
                                .append($("<button></button>").addClass("btn btn-block btn-primary")
                                    .append("Refresh").click(function ()
                                        {
                                            var category_id = cat_dorp_down.find(".select2 option:selected").val();
                                            that.loadSubCategory(category_id);
                                        }
                                     )
                                )
                        )
                        return div;
                }
                
                this.setInsForm=function (a){
                    var model = config.getView("ModelVgr").getModel("New SubCategory",that.getInsForm(),"submit");
                    a.empty();
                    a.append(model);
                    var sub=false;
                    var sub_cat_form =$('#sub_cat_from');
                    sub_cat_form.validate({
                            rules: {
                               subcategory_name: {minlength: 6,required: true,required: true},
                               agree: "required"
                               },
                                highlight: function(element) {$(element).closest('.control-group').removeClass('success').addClass('error');sub=false;},
                                success: function(element) {element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');sub=true;}
                    });
                    
                    sub_cat_form.find("#submit").click(function(){
                        if(sub){
                                that.removeFail();
                                var category_id;
                                sub_cat_form.find(":input").each(function() {
                                        if($(this).attr("name")=="category_id"){
                                            category_id= $(this).val();
                                        }
                                }); 
                                that.setSelectedCategory(category_id)
                                cat_dorp_down.find(".select2").trigger("change");
                                $.each(listeners, function(i){
                                    listeners[i].insSubCategory(sub_cat_form);
                                });
                                sub=false;
                        }
                     });
                    $('#myModal').modal('show');
                }
                
                this.setUpdForm=function (a,id,category_id){
                    var model = config.getView("ModelVgr").getModel("View SubCategory",that.getUpdForm(id,category_id),"submit_sub_cat");
                    a.empty();
                    a.append(model);
                    var sub=false;
                    var sub_cat_form_upd =$('#sub_cat_form_upd');
                    sub_cat_form_upd.validate({
                            rules: {
                               subcategory_name: {minlength: 6,required: true,required: true},
                               agree: "required"
                               },
                                highlight: function(element) {$(element).closest('.control-group').removeClass('success').addClass('error');sub=false;},
                                success: function(element) {element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');sub=true;}
                    });
                    
                    sub_cat_form_upd.find("#submit_sub_cat").click(function(){
                        if(sub){
                                that.removeFail();
                                $.each(listeners, function(i){
                                    listeners[i].updSubCategory(sub_cat_form_upd);
                                });
                                
                                sub=false;
                                $('#myModal').modal('toggle');
                        }
                     });
                    $('#myModal').modal('show');
                }
                
                this.getInsForm =function (){
                    var sel_cat = $("<select></select>").addClass("input-xlarge form-control").attr({'type':'text','id':'category_id','placeholder':'Select Category','name':'category_id'});
                    that.loadCategory(sel_cat);
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'sub_cat_from'})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                        .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Category Name"))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls")
                                        .append(sel_cat)
                                        .append($("<label></label>").addClass("control-label").attr({'for':'subcategory_name'}).append("Sub Category Name"))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls")
                                        .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'subcategory_name','placeholder':'Sub Category Name','name':'subcategory_name'})))
                                         )
                                 ));
                    return form;        
                }
                
                this.getUpdForm =function (id,category_id){
                    var sub_cat_name ;
                    $.each(listeners, function(i){
                        sub_cat_name=listeners[i].getSubCategory(id,category_id);
                    });
                    
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'sub_cat_form_upd'})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'subcategory_name'}).append("Sub Category Name"))
                                            .append($("<p></p>"))
                                            .append($("<div></div>").addClass("controls")
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'subcategory_name','placeholder':'SubCategory Name','name':'subcategory_name','value':sub_cat_name}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'subcategory_id','name':'subcategory_id','value':id}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'subcategory_id','name':'category_id','value':category_id})))
                                         )
                                 );
                    return form;        
                }
                
                this.addSubCategory =function (data){
                   tbody.append(that.getRow(data))
                }                                                        
                
                this.addCategory=function (data){
                   cat_dorp_down.find(".select2").append(that.getOption(data))
                }          
                
                this.setSelectedCategory =function (category_id){
                    cat_dorp_down.find(".select2").val(category_id);
                }
                
                this.updSubCategory =function (data){
                    $("#"+data.subcategory_id).find("td:nth-child(2)").empty();
                    $("#"+data.subcategory_id).find("td:nth-child(2)").append(data.subcategory_name);
                }                                                        
                
                this.insLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.insLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                }
                
                this.insLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                this.delLoadBegin =function (){
                    $("#"+row_id).find("td:nth-child(2)").empty();
                    $("#"+row_id).find("td:nth-child(2)").append(config.getLoadingData());
                }
                
                this.delLoadFinish =function (){
                    $("#"+row_id).remove();
                }
                
                this.delLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                this.removeFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.selLoadBegin =function (){
                    tbody.append(config.getLoadingData());
                }
                
                this.selLoadFinish=function (){
                    tbody.empty();
                }

                this.selLoadFail = function() { 
                    tbody.append("<ts>")
                }
                
                this.selRemoveFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.updLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.updLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                }
                
                this.updLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
            
                this.loadCategory =function (t){
                    
                    var all = config.getModel("Category").selCategory();
                    if(t==null){
                        cat_dorp_down.find(".select2").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addCategory(all[item]);
                                });
                         }
                    }else{
                        if(all){
                                $.each(all, function(item){
                                    t.append(that.getOption(all[item]))
                                });
                         }
                    }
                } 
                
                this.loadSubCategory =function (id){
                    count=0;
                    tbody.empty();
                    $.each(listeners, function(i){
                        listeners[i].selSubCategory(id);
                    });
                } 

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	SubCategoryVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insSubCategory : function(from){},
			selSubCategory : function() { },
                        delSubCategory : function(id) { },
                        getSubCategory : function(id) { },
                        updSubCategory : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
