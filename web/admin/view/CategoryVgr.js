jQuery.extend({
    CategoryVgr: function (config) {
        /**
         * keep a reference to ourselves
         */
        var that = this;

        /**
         * who is listening to us?
         */
        var listeners = new Array();
        var tbody;
        var cat;
        var row_id;
        var count = 0;

        this.getCategory = function () {
            config.getView("ContentWrapperVgr").setBoxHeader("Configuration");
            cat = $("<div></div>").addClass("col-md-6");
            cat.append(that.getPanelTitle);
            var box = that.getBoxbody();
            var table = that.getTable();
            tbody = table.find("tbody");
            box.append(table);
            cat.append(box);
            cat.append(that.getBoxFooter());
            that.loadCategory();
            return cat;
        }

        this.getPanelTitle = function () {
            var div = $("<div></div>").addClass("box-header with-border");
            div.append($("<h3></h3>").addClass("box-title").append($("<b>Category</b>")));
            return div;
        }

        this.getBoxbody = function () {
            var div = $("<div></div>").addClass("box-body").attr({'id': 'cat'});
            return div;
        }

        this.getTable = function () {
            var div = $("<table></table>").addClass("table table-bordered")
                    //set header for table 
                    .append($("<thead></thead>")
                            .append($("<tr></tr>")
                                    .append($("<th></th>").append("#").css({'width': '10px'}))
                                    .append($("<th></th>").append("Category Name"))
                                    .append($("<th></th>").append("Action").css({'width': '40px'}))
                                    ))
                    //set table data
                    .append($("<tbody></tbody>").attr({'id': 'cat_tb'}))
            return div;
        }

        this.getRow = function (data) {
            count++;
            var row = $("<tr></tr>").attr({'id': data.category_id})
                    .append($("<td>" + count + "</td>"))
                    .append($("<td>" + data.category_name + "</td>"))
                    .append($("<td></td>").append(that.getDropdown(data.category_id))
                            );
            return row;
        }

        this.getDropdown = function (a) {
            var drop_down = $("<div></div>").addClass("input-group-btn")
                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type': 'button', 'data-toggle': 'dropdown'})
                            .append($("<span></span>").addClass("fa fa-caret-down")))
                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                            .append($("<li></li>").append($("<a>Delete</a>")))
                            .append($("<li></li>").addClass("divider"))
                            .append($("<li></li>").append($("<a>Edit</a>"))));

            return that.getClickEvent(drop_down, a);
        }

        this.getClickEvent = function (drop_down, id) {
            $(drop_down).find('ul>li:nth-child(1)').click(function () {
                var obj = {};
                obj["id"] = id
                var model = config.getView("ErrorMsgVgr").getErrorMsg("Delete Category", "Are you sure you want to delete this Category?", obj, that.delCatgory);
                $("#hidden").empty();
                $("#hidden").append(model);
                model.modal('show');
            });
            $(drop_down).find('ul>li:nth-child(3)').click(function () {
                that.updCatgory($("#hidden"), id);
            });
            return drop_down;
        }

        this.delCatgory = function (obj) {
            row_id = obj["id"];
            $.each(listeners, function (i) {
                listeners[i].delCategory(obj["id"]);
            });
            return true;
        }

        this.updCatgory = function (div, id) {
            that.setUpdForm(div, id);
        }

        this.getBoxFooter = function () {
            var div = $("<div></div>").addClass("box-footer clearfix");
            div.append($("<div></div>").addClass("col-md-3 col-xs-5")
                    .append($("<button></button>").addClass("btn btn-block btn-primary").attr({'data-toggle': 'modal', 'data-target': 'myModal'})
                            .append("New").click(function ()
                    {
                        that.setInsForm($("#hidden"))
                    }
                    )
                            ))
            div.append($("<div></div>").addClass("col-md-3 col-xs-6 pull-right")
                    .append($("<button></button>").addClass("btn btn-block btn-primary")
                            .append("Refresh").click(function ()
                    {
                        that.loadCategory();
                    }
                    )

                            )
                    )

            return div;
        }

        this.setInsForm = function (a) {
            var model = config.getView("ModelVgr").getModel("New Category", that.getInsForm(), "submit");
            a.empty();
            a.append(model);
            var sub = false;
            var cat_form = $('#cat_from');
            cat_form.validate({
                rules: {
                    category_name: {minlength: 6, required: true, required: true},
                    agree: "required"
                },
                highlight: function (element) {
                    $(element).closest('.control-group').removeClass('success').addClass('error');
                    sub = false;
                },
                success: function (element) {
                    element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');
                    sub = true;
                }
            });
            cat_form.find("#submit").click(function () {
                if (sub) {
                    that.removeFail();
                    $.each(listeners, function (i) {
                        listeners[i].insCategory(cat_form);
                    });
                    sub = false;
                }
            });

            $('#myModal').modal('show');

        }

        this.setUpdForm = function (a, id) {
            var model = config.getView("ModelVgr").getModel("View Category", that.getUpdForm(id), "submit_cat");
            a.empty();
            a.append(model);
            var sub = false;
            var cat_form_upd = $('#cat_form_upd');
            cat_form_upd.validate({
                rules: {
                    category_name: {minlength: 6, required: true, required: true},
                    agree: "required"
                },
                highlight: function (element) {
                    $(element).closest('.control-group').removeClass('success').addClass('error');
                    sub = false;
                },
                success: function (element) {
                    element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');
                    sub = true;
                }
            });

            cat_form_upd.find("#submit_cat").click(function () {
                if (sub) {
                    that.removeFail();
                    $.each(listeners, function (i) {
                        listeners[i].updCategory(cat_form_upd);
                    });
                    sub = false;
                    $('#myModal').modal('toggle');
                }
            });
            $('#myModal').modal('show');
        }

        this.getInsForm = function () {
            var form = $("<form></form>").addClass("form-horizontal").attr({'id': 'cat_from'})
                    .append($("<div></div>").addClass("form-group")
                            .append($("<div></div>").addClass("form-control-group col-lg-12")
                                    .append($("<label></label>").addClass("control-label").attr({'for': 'category_name'}).append("Category Name"))
                                    .append($("<p></p>"))
                                    .append($("<div></div>").addClass("controls")
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type': 'text', 'id': 'category_name', 'placeholder': 'Category Name', 'name': 'category_name'}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type': 'hidden', 'id': 'user_id', 'placeholder': 'Category Name', 'name': 'user_id','value':config.getModel('ProfileConfig').getUser_id()}))
                                            )
                                    )
                            );
            return form;
        }

        this.getUpdForm = function (id) {
            var cat_name;
            $.each(listeners, function (i) {
                cat_name = listeners[i].getCategory(id);
            });
            var form = $("<form></form>").addClass("form-horizontal").attr({'id': 'cat_form_upd'})
                    .append($("<div></div>").addClass("form-group")
                            .append($("<div></div>").addClass("form-control-group col-lg-12")
                                    .append($("<label></label>").addClass("control-label").attr({'for': 'category_name'}).append("Category Name"))
                                    .append($("<p></p>"))
                                    .append($("<div></div>").addClass("controls")
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type': 'text', 'id': 'category_name', 'placeholder': 'Category Name', 'name': 'category_name', 'value': cat_name}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type': 'hidden', 'id': 'category_id', 'placeholder': 'Category Id', 'name': 'category_id', 'value': id})))
                                    )
                            );
            return form;
        }

        this.addCategory = function (data) {
            tbody.append(that.getRow(data))
        }

        this.updCategory = function (data) {
            $("#" + data.category_id).find("td:nth-child(2)").empty();
            $("#" + data.category_id).find("td:nth-child(2)").append(data.category_name);
        }

        this.insLoadBegin = function () {
            config.getView("ModelVgr").setloading();
            config.getView("ModelVgr").setDisableButton();
        }

        this.insLoadFinish = function () {
            config.getView("ModelVgr").removeLoading();
            config.getView("ModelVgr").setEnableButton();
        }

        this.insLoadFail = function () {
            config.getView("ModelVgr").setServerError();
        }

        this.delLoadBegin = function () {
            $("#" + row_id).find("td:nth-child(2)").empty();
            $("#" + row_id).find("td:nth-child(2)").append(config.getLoadingData());
        }

        this.delLoadFinish = function () {
            $("#" + row_id).remove();
        }

        this.delLoadFail = function () {
            config.getView("ModelVgr").setServerError();
        }

        this.removeFail = function () {
            config.getView("ModelVgr").removeServerError();
        }

        this.selLoadBegin = function () {
            tbody.append(config.getLoadingData());
        }

        this.selLoadFinish = function () {
            tbody.empty();
        }

        this.selLoadFail = function () {
            config.getView("ModelVgr").setServerError();
        }

        this.selRemoveFail = function () {
            config.getView("ModelVgr").removeServerError();
        }

        this.updLoadBegin = function () {
            config.getView("ModelVgr").setloading();
            config.getView("ModelVgr").setDisableButton();
        }

        this.updLoadFinish = function () {
            config.getView("ModelVgr").removeLoading();
            config.getView("ModelVgr").setEnableButton();
        }

        this.updLoadFail = function () {
            config.getView("ModelVgr").setServerError();
        }

        this.loadCategory = function () {
            count = 0;
            tbody.empty();
            $.each(listeners, function (i) {
                listeners[i].selCategory();
            });
        }

        this.addListener = function (list) {
            listeners.push(list);
        }


    },
    /**
     * let people create listeners easily
     */
    CategoryVgrListener: function (list) {
        if (!list)
            list = {};
        return $.extend({
            insCategory: function (from) {
            },
            selCategory: function () {
            },
            delCategory: function (id) {
            },
            getCategory: function (id) {
            },
            updCategory: function (from) {
            },
            loadNotificationMgr: function () {
            }
        }, list);
    }
});
