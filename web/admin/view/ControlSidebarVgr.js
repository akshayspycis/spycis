jQuery.extend({
	ControlSidebarVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                var no=false;
                var control_sidebar;
                var button_panel;
                var ques_no={};
                var aa=0;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
            this.getControlSidebar= function(){
			var aside=$("<aside></aside>").addClass("control-sidebar control-sidebar-dark control-sidebar-open");
                        aside.append($("<div></div>").addClass("tab-content")
                                        .append($("<div></div>").addClass("tab-pane active").attr({'id':'control-sidebar-home-tab'})
                                                .append($("<h3></h3>").addClass("control-sidebar-heading").append("Question Palette"))
                                                .append($("<div></div>").css({'width':'220px','height':'320px','overflow':'auto','position':'relative','left':'-10px'})
                                                        .append(that.getButtonPanel()))
                                                )
                                );
 
                        return aside;
		}

            this.getButtonPanel =function (){
                        button_panel =$("<div></div>").addClass("text-center").attr({'id':'button_panel'});
                        for(var i=0;i<2;i++){
                            button_panel.append(that.getSectionBox("lksajd"));
                        }
                        return button_panel;
            }
  
  
            this.getSectionBox = function(section){
                var sec_box=$("<div></div>").addClass("box box-default box-solid");
                sec_box.append($("<div></div>").addClass("box-header with-border").append($("<h3></h3>").addClass("box-title pull-left").append(section))
                    .append($("<div></div>").addClass("box-tools pull-right").append($("<button></button>").addClass("btn btn-box-tool").attr({'data-widget':'collapse'}).append($("<i></i>").addClass("fa fa-minus")))
                    .append($("<button></button>").addClass("btn btn-box-tool").attr({'data-widget':'remove'}).append($("<i></i>").addClass("fa fa-times")))));
                sec_box.append($("<div></div>").addClass("box-body no-padding").css({'display':'block'}).append($("<ul></ul>").addClass("list-unstyled ui-sortable").append(that.getTopicBox(section))));
                return sec_box;
            }
            
            
            this.getTopicBox = function(topic){
                var sec_box=$("<div></div>").addClass("box box-default");
                sec_box.append($("<div></div>").addClass("box-header with-border").append($("<h3></h3>").addClass("box-title pull-left").append(topic))
                    .append($("<div></div>").addClass("box-tools pull-right").append($("<button></button>").addClass("btn btn-box-tool").attr({'data-widget':'collapse'}).append($("<i></i>").addClass("fa fa-minus")))
                    .append($("<button></button>").addClass("btn btn-box-tool").attr({'data-widget':'remove'}).append($("<i></i>").addClass("fa fa-times")))));
                sec_box.append($("<div></div>").addClass("box-body no-padding").css({'display':'block'}).append($("<ul></ul>").addClass("nav nav-pills nav-stacked").append()));
                return $("<li></li>").append(sec_box);    
            }
                
 
                
                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	ControlSidebarListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});