jQuery.extend({
	SectionVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody;
                var section;
                var row_id;
                var cat_dorp_down;
                var sub_cat_dorp_down;
                var sel_sub_cat;
                var b=true;
                var count=0;
                
                this.getSection = function(){
                    config.getView("ContentWrapperVgr").setBoxHeader("Configuration");
			section =$("<div></div>").addClass("col-md-6");
                        section.append(that.getPanelTitle);
                        var box=that.getBoxbody();
                        var table = that.getTable();
                        tbody=table.find("tbody");
                        box.append(that.getCategory_DropDown);
                        box.append(that.getSubCategory_DropDown);
                        box.append(table);
                        section.append(box);
                        var box1=that.getBoxbody1();
                        section.append(box1);
                        section.append(that.getBoxFooter());
                        //that.loadSection();
                        //-----------this code is resposible for load category and load subcatgory--------------------------
                        that.loadCategory();
                        cat_dorp_down.find(".cat1").trigger("change");
                        return section;
		}

                this.getPanelTitle = function (){
                    var div =$("<div></div>").addClass("box-header with-border");
                    div.append($("<h3></h3>").addClass("box-title").append($("<b>Section Details</b>")));
                    return div;
                }
                
                this.getCategory_DropDown = function (){
                    tbody.empty();
                    cat_dorp_down =$("<div></div>").addClass("form-group");
                    cat_dorp_down.append($("<label>Category :</label>"))
                    cat_dorp_down.append($("<p></p>"))
                    cat_dorp_down.append($("<select>Category :</select>").addClass("form-control cat1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var id = cat_dorp_down.find(".cat1 option:first").val();
                            $( ".cat1 option:selected" ).each(function() {
                              id = $( this ).val();
                            });
                        that.loadSubCategory(null,id,1);
                        
                    });
                    return cat_dorp_down;
                }
                
                this.getSubCategory_DropDown = function (){
                    sub_cat_dorp_down =$("<div></div>").addClass("form-group");
                    sub_cat_dorp_down.append($("<label>Sub Category :</label>"))
                    sub_cat_dorp_down.append($("<a></a>").attr({'id':'sub_img_loading'}));
                    sub_cat_dorp_down.append($("<p></p>"))
                    sub_cat_dorp_down.append($("<select>Sub Category :</select>").addClass("form-control sub1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:first").val();
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        $( ".sub1 option:selected" ).each(function() {
                          subcategory_id = $( this ).val();
                        });
                        that.loadSection(category_id,subcategory_id);
                    });
                    return sub_cat_dorp_down;
                }

                this.getBoxbody = function (){
                    var div =$("<div></div>").addClass("box-body").attr({'id':'sub'});
                    return div;
                }
                this.getBoxbody1 = function (){
                    var div =$("<div></div>").addClass("box-body").attr({'id':'sub_my'});
                    return div;
                }
                
                this.getTable = function (){
                    var div =$("<table></table>").addClass("table table-bordered")
                    //set header for table 
                            .append($("<thead></thead>")
                                .append($("<tr></tr>")
                                .append($("<th></th>").append("#").css({'width':'10px'}))
                                .append($("<th></th>").append("Section Name"))
                                .append($("<th></th>").append("Action").css({'width':'40px'}))
                            ))
                            //set table data
                            .append($("<tbody></tbody>").attr({'id':'section_tb'}))
                    return div;
                }
                
                this.getRow = function (data){
                    count++;
                    var row = $("<tr></tr>").attr({'id':data.section_id})
                            .append($("<td>"+count+"</td>"))
                            .append($("<td>"+data.section_name+"</td>"))
                            .append($("<td></td>").append(that.getDropdown(data.section_id))
                    );
                    return row;            
                }
                
                this.getCatOption = function (data){
                    var row = $("<option></option>").attr({'value':data.category_id})
                              .append($("<td>"+data.category_name+"</td>"));
                    return row;            
                }
                this.getSubOption = function (data){
                    var row = $("<option></option>").attr({'value':data.subcategory_id})
                              .append($("<td>"+data.subcategory_name+"</td>"));
                    return row;            
                }
                
                this.getDropdown = function (a){
                    var drop_down =$("<div></div>").addClass("input-group-btn")
                                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                        .append($("<span></span>").addClass("fa fa-caret-down")))
                                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li></li>").append($("<a>Delete</a>")))
                                            .append($("<li></li>").addClass("divider"))
                                            .append($("<li></li>").append($("<a>Edit</a>"))));
                    
                    return that.getClickEvent(drop_down,a);
                }
                
                this.getClickEvent =function (drop_down,id){
                    $(drop_down).find('ul>li:nth-child(1)').click(function (){
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        var obj={};
                        obj["id"]=id;
                        obj["category_id"]=category_id;
                        obj["subcategory_id"]=subcategory_id;
                        var model = config.getView("ErrorMsgVgr").getErrorMsg("Delete Section","Are you sure you want to delete this Section?",obj,that.delSection);
                        $("#hidden").empty();
                        $("#hidden").append(model);
                        model.modal('show');
                    });
                    $(drop_down).find('ul>li:nth-child(3)').click(function (){
                        var category_id = cat_dorp_down.find(".cat1 option:selected").val();
                        var subcategory_id = sub_cat_dorp_down.find(".sub1 option:selected").val();
                        that.updCatgory($("#hidden"),id,category_id,subcategory_id);
                    });
                    return drop_down;
                }
                
                this.delSection =function (obj){
                    row_id =obj["id"];
                    $.each(listeners, function(i){
                        listeners[i].delSection(obj["id"],obj["category_id"],obj["subcategory_id"]);
                    });
                    return true;
                }
                
                this.updCatgory =function (div,id,category_id,subcategory_id){
                    that.setUpdForm(div,id,category_id,subcategory_id);
                }
                
                this.getBoxFooter =function (){
                    var div = $("<div></div>").addClass("box-footer clearfix");
                        div.append($("<div></div>").addClass("col-md-3 col-xs-5")
                                .append($("<button></button>").addClass("btn btn-block btn-primary").attr({'data-toggle':'modal','data-target':'myModal'})
                                    .append("New").click(function ()
                                        {
                                            that.setInsForm($("#hidden"))
                                        }
                                     )
                                    ))
                        div.append($("<div></div>").addClass("col-md-3 col-xs-6 pull-right")
                                .append($("<button></button>").addClass("btn btn-block btn-primary")
                                    .append("Refresh").click(function ()
                                        {
                                            sub_cat_dorp_down.find(".sub1").trigger("change");
                                        }
                                     )
                                )
                        )
                        return div;
                }
                
                this.setInsForm=function (a){
                    var model = config.getView("ModelVgr").getModel("New Section",that.getInsForm(),"submit");
                    a.empty();
                    a.append(model);
                    var sub=false;
                    var section_form =$('#section_form');
                    section_form.validate({
                            rules: {
                                section_name: {minlength: 3,required: true,required: true},
                                      agree: "required"
                                  },
                                highlight: function(element) {$(element).closest('.control-group').removeClass('success').addClass('error');sub=false;},
                                success: function(element) {element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');sub=true;}
                    });
                    section_form.find("#submit").click(function(){
                        if(sub){
                                that.removeFail();
                                var category_id;
                                var subcategory_id;
                                section_form.find(":input").each(function() {
                                        if($(this).attr("name")=="category_id"){
                                            category_id= $(this).val();
                                        }
                                        if($(this).attr("name")=="subcategory_id"){
                                            subcategory_id= $(this).val();
                                        }
                                }); 
                                that.setSelectedSection(category_id,subcategory_id)
                                $.each(listeners, function(i){
                                        listeners[i].insSection(section_form);
                                });
                                sub=false;
                        }
                     });
                    $('#myModal').modal('show');
                }
                
                this.setUpdForm=function (a,id,category_id,subcategory_id){
                    var model = config.getView("ModelVgr").getModel("View Section",that.getUpdForm(id,category_id,subcategory_id),"submit_section");
                    a.empty();
                    a.append(model);
                    var sub=false;
                    var section_form_upd =$('#section_form_upd');
                    section_form_upd.validate({
                            rules: {
                               section_name: {minlength: 3,required: true,required: true},
                               agree: "required"
                               },
                                highlight: function(element) {$(element).closest('.control-group').removeClass('success').addClass('error');sub=false;},
                                success: function(element) {element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');sub=true;}
                    });
                    
                    section_form_upd.find("#submit_section").click(function(){
                        if(sub){
                                that.removeFail();
                                $.each(listeners, function(i){
                                    listeners[i].updSection(section_form_upd);
                                });
                                sub=false;
                                $('#myModal').modal('toggle');
                        }
                     });
                    $('#myModal').modal('show');
                }
                
                this.getInsForm =function (){
                    var sel_cat = $("<select></select>").addClass("cat2 input-xlarge form-control").attr({'id':'category_id','placeholder':'Select','name':'category_id'}).change(function () {
                        var id = sel_cat.find("option:first").val();
                            sel_cat.find("option:selected" ).each(function() {
                              id = $( this ).val();
                            });
                        that.loadSubCategory(sel_sub_cat,id,2);
                    });
                    //call the function load category data form cache in model category dropdown list..................
                    that.loadCategory(sel_cat);
                    sel_sub_cat = $("<select></select>").addClass("input-xlarge form-control").attr({'id':'subcategory_id','placeholder':'Select Section','name':'subcategory_id'});
                    //trigger of category dropdown change function which call loadSubcategory 
                    sel_cat.trigger("change");
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'section_form'})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                        .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Category Name"))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls").append(sel_cat))
                                        .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Sub Category "))
                                        .append($("<a></a>").attr({'id':'model_sub_img_loading'}))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls").append(sel_sub_cat))
                                        .append($("<label></label>").addClass("control-label").attr({'for':'section_name'}).append("Section Name"))
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls")
                                        .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'section_name','placeholder':'Section Name','name':'section_name'})))
                                         )
                                 );
                    return form;        
                }
                
                this.getUpdForm =function (id,category_id,subcategory_id){
                    var section_name ;
                    $.each(listeners, function(i){
                        section_name=listeners[i].getSection(id,category_id,subcategory_id);
                    });
                    var form =$("<form></form>").addClass("form-horizontal").attr({'id':'section_form_upd'})
                            .append($("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'section_name'}).append("Section Name"))
                                            .append($("<p></p>"))
                                            .append($("<div></div>").addClass("controls")
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'section_name','placeholder':'Section Name','name':'section_name','value':section_name}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'category_id','name':'category_id','value':category_id}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'subcategory_id','name':'subcategory_id','value':subcategory_id}))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'hidden','id':'section_id','name':'section_id','value':id})))
                                         )
                                 );
                    return form;        
                }
                
                this.addSection =function (data){
                   tbody.append(that.getRow(data))
                }                                                        
                
//                this.addSection=function (data){
//                   cat_dorp_down.find(".select2").append(that.getOption(data))
//                }          
                this.addCategory=function (data){
                   cat_dorp_down.find(".select2").append(that.getCatOption(data))
                }          
                
                this.addSubCategory=function (data){
                   sub_cat_dorp_down.find(".sub1").append(that.getSubOption(data))
                }          
                
                this.addSubCategoryInModel=function (data){
                   sel_sub_cat.append(that.getSubOption(data))
                }          
                
                this.setSelectedSection =function (category_id,subcategory_id){
                    cat_dorp_down.find(".cat1").val(category_id);
                    that.loadSubCategory(null,category_id,1);
                    sub_cat_dorp_down.find(".sub1").val(subcategory_id);
                    sub_cat_dorp_down.find(".sub1").trigger("change");
                }
                
                this.updSection =function (data){
                    $("#"+data.section_id).find("td:nth-child(2)").empty();
                    $("#"+data.section_id).find("td:nth-child(2)").append(data.section_name);
                }                                                        
                
                this.insLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.insLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                }
                
                this.insLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                this.delLoadBegin =function (){
                    $("#"+row_id).find("td:nth-child(2)").empty();
                    $("#"+row_id).find("td:nth-child(2)").append(config.getLoadingData());
                }
                
                this.delLoadFinish =function (){
                    $("#"+row_id).remove();
                }
                
                this.delLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                
                this.removeFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.selLoadBegin =function (){
                    tbody.append(config.getLoadingData());
                }
                
                this.sectionSubLoadBegin =function (){
                    sub_cat_dorp_down.find("#sub_img_loading").append(config.getLoadingData());
                }
                
                this.sectionModelSubLoadBegin =function (){
                    
                    $("#model_sub_img_loading").append(config.getLoadingData());
                }
                
                this.selLoadFinish=function (){
                    tbody.empty();
                }
                
                this.sectionSubLoadFinish=function (){
                    sub_cat_dorp_down.find("#sub_img_loading").empty();
                    setTimeout(function (){
                        sub_cat_dorp_down.find(".sub1").trigger("change");
                    },20);
                }
                
                this.sectionModelSubLoadFinish=function (){
                    $("#model_sub_img_loading").empty();
                }

                this.selLoadFail = function() { 
                    tbody.append("<ts>")
                }
                
                this.sectionSubLoadFail = function() { 
                    sub_cat_dorp_down.find("#sub_img_loading").append("Error: Server doesn't Respond.");
                }
                
                this.sectionModelSubLoadFail = function() { 
                    $("#model_sub_img_loading").append("Error: Server doesn't Respond.");
                }
                
                this.selRemoveFail = function() { 
                    config.getView("ModelVgr").removeServerError();
                }
                
                this.updLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.updLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                }
                
                this.updLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
            
                this.loadSection =function (t){
                    var all = config.getModel("Section").selSection();
                    if(t==null){
                        cat_dorp_down.find(".select2").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addSection(all[item]);
                                });
                         }
                    }else{
                        if(all){
                                $.each(all, function(item){
                                    t.append(that.getOption(all[item]))
                                });
                         }
                    }
                } 
                
                this.loadCategory =function (t){
                    var all = config.getModel("Category").selCategory();
                    if(t==null){
                        cat_dorp_down.find(".cat1").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addCategory(all[item]);
                                });
                         }
                    }else{
                        if(all){
                                $.each(all, function(item){
                                    t.append(that.getCatOption(all[item]))
                                });
                         }
                    }
                } 
                
                this.loadSubCategory =function (t,category_id,module){
                    if(t==null){
                        sub_cat_dorp_down.find(".sub1").empty();
                        var all = config.getModel("SubCategory").selSubCategory(category_id,module);
                        if(all){
                                $.each(all, function(item){
                                    that.addSubCategory(all[item]);
                                });
                                sub_cat_dorp_down.find(".sub1").trigger("change");
                         }
                    }else{
                        sel_sub_cat.empty();
                        var all = config.getModel("SubCategory").selSubCategory(category_id,module);
                        if(all){
                                $.each(all, function(item){
                                    t.append(that.getSubOption(all[item]))
                                });
                         }
                    }
                } 
                
                
                //this is code run when section subcatgory detial does not have in cache object and this funcation call inside the class manager of sub category
                this.subAddInSction =function (item){
                    that.addSubCategory(item);
                } 
                this.subAddInSctionModel =function (item){
                    that.addSubCategoryInModel(item);
                } 
                
                this.loadSection =function (category_id,subcategory_id){
                    count=0;
                    tbody.empty();
                    $.each(listeners, function(i){
                        listeners[i].selSection(category_id,subcategory_id);
                    });
                } 

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	SectionVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insSection : function(from){},
			selSection : function() { },
                        delSection : function(id) { },
                        getSection : function(id) { },
                        updSection : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
