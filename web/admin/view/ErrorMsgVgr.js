jQuery.extend({
	ErrorMsgVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
                var modelform;
                var model_footer;
                    var model;               
                this.getErrorMsg = function(title,model_form,id,collback){
                    modelform=model_form;
                    model =$("<div>").addClass("modal fade").attr({'id':'error_msg','tabindex':'-1','role':'dialog','aria-labelledby':'myModalLabel'});
                    var model_dialog =$("<div>").addClass("modal-dialog").attr({'role':'document'});
                    var model_content=$("<div>").addClass("modal-content");
                    model_content.append(that.getErrorMsgHeader(title));
                    model_content.append(that.getErrorMsgBody(model_form));
                    model_content.append(that.getErrorMsgFooter(id,collback));
                    model_dialog.append(model_content);
                    model.append(model_dialog);
                    model.on('hidden.bs.modal', function (e) {
                        if($('.modal').hasClass('in')) {
                             $('body').addClass('modal-open');
                        }    
                    });
                    model.on('shown.bs.modal', function() {
                       var modal = this;
                       var hash = modal.id;
                       window.location.hash = hash;
                       window.onhashchange = function() {
                            if (!location.hash){
                                $(modal).modal('hide');
                            }
                       }
                    });  
                    model.on('hide.bs.modal', function() {
                        var hash = this.id;
                        history.pushState('', document.title, window.location.pathname);
                        if($('.modal').hasClass('in')) {
                             $('body').addClass('modal-open');
                        }
                    });
                    return model;
		}
                

                this.getErrorMsgHeader= function (header_title){
                    var model_header =$("<div>").addClass("modal-header with-border bg-primary");
                    model_header.append($("<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"))
                    model_header.append($("<h4 class='modal-title' id='myModalLabel'>"+header_title+"</h4>"));
                    return model_header;
                }
                
                this.getErrorMsgBody = function (model_form){
                    var model_body =$("<div>").addClass("modal-body");
                    model_body.append(model_form);
                    return model_body ;
                }
                
                this.getErrorMsgFooter = function (id,collback){
                    model_footer =$("<div>").addClass("modal-footer");
                    model_footer.append($("<label></label>").addClass("error").attr({'id':'error_msg'}));
                    model_footer.append($("<button>").append("Confirm").addClass("btn btn-primary").attr({'type':'button'}).click(function (){
                        if(collback(id)){
                            model.modal('toggle');
                        }
                    }));
                    model_footer.append($("<button>").append("Cancel").addClass("btn btn-default").attr({'type':'button','data-dismiss':'modal','id':'cancel'}));
                    model_footer.append($("<a></a>").attr({'id':'img_loading'}));
                    return model_footer;
                }
                
        }
});
