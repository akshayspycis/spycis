jQuery.extend({
	ContentWrapperVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                var no=false;
                var section;
                var box_header;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();

            
                this.setContentWrapper= function(){
			var aside=$("<div></div>").addClass("content-wrapper");
                        aside.append(that.getContentHeader())
                        aside.append(that.getContent());
                        return aside;
		}
                this.getContentHeader =function (){
                        section =$("<section></section>").addClass("content-header")
                                        .append($("<p> </p>"))
                                        .append($("<ol></ol>").addClass("breadcrumb")
//                                                .append($("<li></li>").append($("<a></a>").append($("<i></i>").addClass("fa fa-dashboard")).append(" Home")))
//                                                .append($("<li></li>").append($("<a></a>").append(" Forms")))
//                                                .append($("<li></li>").append($("<a></a>").addClass("active").append(" Home")))
                                        )
                                        .append($("<p> </p>"));
                        return section;
                }
                this.getContent=function (){
                        section =$("<section></section>").addClass("content").append(that.getBox());
                        return section;
                }
                
                this.getBox = function (){
                    var box=$("<div></div>").addClass("box");
                    box.append(that.getBoxHeader());
                    box.append(that.getBoxBody());
                    return box;
                }
                
                this.getBoxHeader=function (){
                    box_header = $("<div></div").addClass("box-header with-border")
                            .append($("<h3></h3>").addClass("box-title").append("Home"))
                            ;
                    return box_header;
                }
                this.setBoxHeader =function (title){
                    box_header.find("h3").empty();
                    box_header.find("h3").append(title);
                }
                
                this.getBoxBody=function (){
                    var box_header = $("<div></div").addClass("box-body");
                    return box_header;
                }
                
                
                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	ContentWrapperListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
