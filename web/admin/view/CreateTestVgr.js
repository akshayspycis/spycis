jQuery.extend({
	CreateTestVgr: function(config,kk){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody_img;
                var question;
                var panel_title ;
                var create_test_box_body;
                var ul_question_box_body;
                var form ;
                var question_mark ;
                question={};
                object={};
                aaa={};
                gen={};
                sec={};
                var module=1;
                var formula_icon;
                var formula_button;
                var formula_text_box;
                var next;
                var back;
                var test_type;
                var section_order;
                this.getCreateTest = function(obj){
                    section_order=obj["section_order"];
                    aaa={};
                    gen={};
                    sec={};
                    $("body").find("#hidden").find("#error_msg").remove();
                    module=1
                    question=obj;
                    gen["negative_ratio"]=1;
                    $.each(obj["question_details"],function (section_id,value){
                         sec[section_id]={};
                         $.each(value,function (topic_id,val){
                            if(!(topic_id=="ul" || topic_id=="obj")){
                                sec[section_id][topic_id]={};
                            }
                            $.each(val,function (question_id,va){
                                if($.isNumeric(question_id)&&question_id!=0){
                                    aaa[va.order]={}
                                    aaa[va.order]["question_id"]=question_id
                                    aaa[va.order]["value"]=va.value;
                                }
                            });
                        });
                    });
                    gen["mark"]=null;
                    gen["type"]="⇩";
                    var div=$("<div></div>");
                        div.append(that.getBoxbody());
                        that.setGeneralDetails();
                        $.fn.modal.Constructor.prototype.enforceFocus = function () {
                        modal_this = this
                        $(document).on('focusin.modal', function (e) {
                        if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                        // add whatever conditions you need here:
                        &&
                        !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
                        modal_this.$element.focus()
                        }
                        })
                    };
                        return div;
                        
		}
                this.setBackDisable= function (){
                    back.prop('disabled', true);
                }
                
                this.getObj=function (){
                    object["general_details"]=gen;
                    object["question_details"]=aaa;
                    object["section_details"]=sec;
                    object["section_order"]=section_order;
                    return object;
                }
                
                this.getForm = function (){
                    return form;
                }
                this.getPanelTitle = function (title){
                    panel_title =$("<div></div>").addClass("box-header with-border");
                    panel_title.append($("<h3></h3>").addClass("box-title").append($("<b></b>").append(title)));
                    return panel_title;
                }
                
                this.setGeneralDetails = function (){
                    create_test_box_body.empty();
                    create_test_box_body.append(that.getPanelTitle("General Details"));
                    var sel_phase = $("<select></select>").addClass("sel_phase input-xlarge form-control").attr({'id':'exam_phase_id','placeholder':'Select','name':'exam_phase_id'});
                    test_type=$("<select></select>").addClass("sel_phase input-xlarge form-control").attr({'id':'test_type','placeholder':'Select','name':'test_type'}).css({'padding-left':'0px','padding-right':'0px'})
                                                    .append($("<option>").append("⇩").attr({'value':'⇩'}))
                                                    .append($("<option>").append("Demo").attr({'value':'demo'}))
                                                    .append($("<option>").append("Original").attr({'value':'original'}))
                                                        .change(function(){
                                                           gen["type"]=$(this).val();
                                                        });
                                                 
                    var all = config.getModel("ExamPhase").selExamPhase();
                        sel_phase.find(".sel_phase").empty();
                        if(all){
                            $.each(all, function(item){
                                sel_phase.append($("<option></option>").attr({'value':all[item].exam_phase_id}).append($("<td>"+all[item].exam_phase_name+"</td>")));
                            });
                        }
                    sel_phase.val((gen["exam_phase_id"]!=null ) ? gen["exam_phase_id"] : "");
                  formula_icon=$("<i>").addClass("fa  fa-check-square");
                  formula_button=$("<button>").addClass("btn btn-block btn-default btn-sm").attr({'data-toggle':'modal','data-target':'myModal','type':'button'}).append("Set")
                          .click(function (){
                                that.setFormulaModel();
                          });
                  form =$("<form></form>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                        .append($("<p></p>"))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Test ID"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'number','id':'unique_test_key','name':'unique_test_key','value':$.now()})))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Test Name"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'test_name','placeholder':'Test Name','name':'test_name','value':(gen["test_name"]!=null ) ? gen["test_name"] : ""})))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Exam Phase"))
                                            .append(sel_phase))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Category"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'topic_name','placeholder':'Topic Name','name':'topic_name','value':config.getModel("Category").getCategory(question["category_id"]),'disabled':'disabled'})))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Sub Category "))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'topic_name','placeholder':'Topic Name','name':'topic_name','value':config.getModel("SubCategory").getSubCategory(question["subcategory_id"],question["category_id"]),'disabled':'disabled'})))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<div></div>").addClass("controls col-lg-6 no-padding")
                                                .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Time(Minite)"))
                                                .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'number','id':'time','name':'time','value':(gen["time"]!=null ) ? gen["time"] : ""}))
                                                )
                                            .append($("<div></div>").addClass("controls col-lg-5 no-padding").css({'margin-left':'13px'})
                                                .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Type"))
                                                .append(test_type)
                                                ))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Exam Date"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'date','id':'e_date','name':'e_date','value':(gen["e_date"]!=null ) ? gen["e_date"] : ""})))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Start Time"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'time','id':'start_time','name':'start_time','value':(gen["start_time"]!=null ) ? gen["start_time"] : ""})))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Cut Off"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'number','id':'cut_off','placeholder':'Cut off','name':'cut_off','value':(gen["cut_off"]!=null ) ? gen["cut_off"] : ""})))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'})
                                                            .append($("<input/>").addClass("minimal").attr({'type':'checkbox','id':'check','name':'check'}).prop('checked',(gen["check"]!=null ) ? gen["check"] : "").change(function() {
                                                                    if(this.checked) {
                                                                        gen["check"] = this.checked;
                                                                        form.find("#mark").prop('disabled', false);
                                                                    }else{
                                                                        gen["check"] = this.checked;
                                                                        form.find("#mark").prop('disabled', true);
                                                                        form.find("#mark").val('');
                                                                        gen["mark"] =null ;
                                                                    }
                                                                }))
                                                            .append("&nbsp;Same Marks")
                                                           )
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'mark','placeholder':'Mark in Digit','name':'mark','value':(gen["mark"]!=null) ? gen["mark"] : ""}))
                                            )
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Meditory Test Id"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'meditory_test_id','placeholder':'Test ID','name':'meditory_test_id','value':(gen["meditory_test_id"]!=null ) ? gen["meditory_test_id"] : ""}))
                                            )
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'})
                                                            .append($("<input/>").addClass("minimal").attr({'type':'checkbox','id':'check','name':'check'}).prop('checked',(gen["check"]!=null ) ? gen["check"] : "").change(function() {
                                                                    if(this.checked) {
                                                                        formula_button.prop('disabled', false);
                                                                        formula_icon.removeClass("fa-times")
                                                                        formula_icon.addClass("fa-check-square")
                                                                        gen["negative_ratio"]=1;
                                                                    }else{
                                                                        formula_button.prop('disabled', true);
                                                                        formula_icon.removeClass("fa-check-square")
                                                                        formula_icon.addClass("fa-times")
                                                                        gen["negative_ratio"]=0;
                                                                    }
                                                                }))
                                                            .append("&nbsp;Negative Marks")
                                             )
                                          .append($("<div>").addClass("row").append($("<div>").addClass("col-lg-5")
                                                .append(formula_button).css({'margin-top':'3px'})
                                            )
                                            .append($("<div>").addClass("col-lg-7").css({'padding':'6px'})
                                                .append("Formula&nbsp;").append(formula_icon)
                                            )
                                          )
                                          )
                                         );
                    form.find("#check").trigger("change");
                    create_test_box_body.append(form);
                    create_test_box_body.append(that.getBoxFooter());

                }
                this.setFormulaModel= function (){
                    var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Formula Configuration",that.setFormulaBox(),null,that.setFormulaDigit);
                    error_model.find(".modal-dialog").addClass("modal-sm");
                    $("#hidden").append(error_model);
                    error_model.modal('show');
                }
                this.setFormulaDigit= function (){
                    if((formula_text_box.val()!="" && parseInt(formula_text_box.val())>0)){
                        gen["negative_ratio"]=formula_text_box.val();   
                        return true;
                    }else{
                        alert("Please Set ratio of wrong ans is greater then 0");
                        return false;
                    }
                }
            
                this.setFormulaBox = function (){
                    formula_text_box=$('<input class="form-control input-sm" type="text" placeholder="1" value="'+gen["negative_ratio"]+'">');
                    return $("<ul>").addClass("products-list product-list-in-box")
                                    .append($("<li>").addClass("item")
                                       .append($("<div>").addClass("row")
                                            .append($("<div>").addClass("col-sm-4")
                                                .append($("<div>").addClass("description-block")
                                                    .append($("<h5>").addClass("description-header").append("Total Write Ans"))
                                                )
                                            )    
                                            .append($("<div>").addClass("col-sm-1")
                                                .append($("<div>").addClass("description-block")
                                                    .append($("<h5>").addClass("description-header").append("-"))
                                                )
                                            )    
                                            .append($("<div>").addClass("col-sm-6")
                                                .append($("<div>").addClass("description-block")
                                                    .append($("<h5>").addClass("description-header").append("Wrong Ans /"))
                                                    .append(formula_text_box)
                                                )           
                                            )
                                        )
                           
                    );
                }
                
                
                this.getSBox=function (section_id){
                    return $("<li></li>")
                                .append($("<a></a>")
                                    .append($("<i></i>").addClass("fa fa-circle-o text-light-blue"))
                                    .append(config.getModel("Section").getSection(section_id,question["category_id"],question["subcategory_id"]))
                                    .click(function (){
                                        create_test_box_body.find("#topic").empty();
                                        $.each(question["question_details"][section_id],function (topic_id){
                                                create_test_box_body.find("#topic").append(that.getTBox(section_id,topic_id));
                                        })
                                    })
                                );
                }
                
                this.getTBox=function (section_id,topic_id){
                    return $("<li></li>")
                                .append($("<a></a>")
                                    .append($("<i></i>").addClass("fa fa-circle-o text-light-blue"))
                                    .append(config.getModel("Topic").getTopic(question["category_id"],question["subcategory_id"],section_id,topic_id))
                                );
                }
                
                this.getQuestionBox=function (order,question_id,mark,value){
                    var dis_icon="fa fa-arrow-circle-right";
                    var dis="";
                    var dis_botton;
                    if(value.discription_bank_id==null){
                        dis_icon="fa fa-exclamation-circle";
                        dis="No Discription"
                        dis_botton=$("<button></button>").addClass("btn btn-default").append($("<span></span>").addClass(dis_icon));
                    }else{
                        dis_botton=$("<button></button>").addClass("btn btn-default").append($("<span></span>").addClass(dis_icon))
                                .popover({
                                    trigger: "hover",
                                    placement: "right",
                                    title: "Direction Details",
                                    content: config.getLoadingData(),
                                    html:true
                                })
                                .on('shown.bs.popover', function(){
                                         config.getView("ViewQuestionVgr").loadDiscriptionDetails($(this),value.discription_id,value.discription_bank_id)
                                })
                    }
                    return $("<div></div>").addClass("col-lg-6 col-sm-6 col-md-6")
                            .append($("<div></div>").addClass("input-group margin")
                                .append($("<div></div>").addClass("input-group-btn")
                                        .append($("<a></a>").addClass("btn btn-block btn-social").css({'color':'balck','border-color':'rgba(0, 0, 0, 0.2)'})
                                                .append($("<i></i>").append(order))
                                                .append("Question ID :"+question_id)
                                                .popover({
                                                    trigger: "hover",
                                                    placement: "right",
                                                    title: "Question Details",
                                                    content: config.getLoadingData(),
                                                    html:true
                                                })
                                                .on('shown.bs.popover', function(){
                                                    config.getView("ViewQuestionVgr").updQuestion($(this),value);
                                                }).click(function (e){
                                                    e.stopPropagation();
                                                })
                                            )
                                        )
                                .append($("<div></div>").addClass("input-group-btn")
                                        .append($("<input/>").addClass("input-xlarge form-control").css({'width':'40px','height': '33px'}).attr({'type':'text','id':order,'placeholder':'Mark','name':order,'value':(gen["mark"]!=null ) ? gen["mark"] : mark}).prop('disabled',(gen["check"]!=null ) ? gen["check"] : false))
                                        .append(dis_botton)
                                        .append($("<button></button>").addClass("btn btn-default").attr({'type':'button'})
                                            .append($("<span></span>").addClass("fa fa-list"))
                                                .popover({
                                                    trigger: "hover",
                                                    placement: "right",
                                                    title: "Option Details",
                                                    content: config.getLoadingData(),
                                                    html:true
                                                })
                                                .on('shown.bs.popover', function(){
                                                    config.getView("ViewQuestionVgr").loadOptionDetails($(this),value.question_bank_id)
                                                })
                                            )
                                    ));
                }
                
                this.setSectio_TopicDetails = function (){
                    $("body").find("#hidden").find("#error_msg").remove();
                    create_test_box_body.empty();
                    create_test_box_body.append(that.getPanelTitle("General Details"));
                    var ul_section=$("<ul></ul>").addClass("nav nav-pills nav-stacked");
                    $.map(section_order, function( val, i) {
                        ul_section.append(that.getSBox(val));
                    });
                    var ul_topic=$("<ul></ul>").addClass("nav nav-pills nav-stacked").attr({'id':'topic'});
                    var form =$("<div></div>").addClass("form-group")
                                  .append($("<div></div>").addClass("form-control-group col-lg-12")
                                      .append($("<div></div>").addClass("col-lg-6")
                                            .append($("<div></div>").addClass("box box-primary").css({'margin-top':'15px'})
                                                .append($("<div></div>").addClass("box-header with-border")
                                                        .append($("<h4></h4>").append("Section Details"))
                                                        )
                                                .append($("<div></div>").addClass("box-body no-padding")
                                                        .append(ul_section)
                                                        )
                                                )
                                            )
                                    .append($("<div></div>").addClass("col-lg-6")
                                        .append($("<div></div>").addClass("box box-primary").css({'margin-top':'15px'})
                                            .append($("<div></div>").addClass("box-header with-border")
                                                    .append($("<h4></4>").append("Topic Details"))
                                                    )
                                            .append($("<div></div>").addClass("box-body no-padding")
                                                    .append(ul_topic)
                                                    )
                                            )
                                         )
                                    );
                    create_test_box_body.append(form);
                    create_test_box_body.append(that.getBoxFooter());

                }
                
                this.setQuestionDetails = function (){
                    $("body").find("#hidden").find("#error_msg").remove();
                    create_test_box_body.empty();
                    create_test_box_body.append(that.getPanelTitle("Order of Question And It's marks"));
                    question_mark =$("<div></div>").addClass("form-group").append($("<div></div>").addClass("form-control-group col-lg-12 col-sm-12 col-md-12 no-padding").css({'margin-left':'-16px'}));
                    for (var key in aaa) {
                            question_mark.find(".form-control-group").append(that.getQuestionBox(key,aaa[key]["question_id"],aaa[key]["mark"],aaa[key]["value"]));
                    }
                    create_test_box_body.append(question_mark);
                    create_test_box_body.append(that.getBoxFooter());
                }
                
                this.getBoxbody = function (){
                    create_test_box_body =$("<div></div>").addClass("box-body");
                    return create_test_box_body;
                }
                
                this.getBoxbody1 = function (){
                    var div =$("<div></div>").addClass("box-body").attr({'id':'sub_my'});
                    return div;
                }
                
                
                this.getBoxFooter =function (){
                    var div = $("<div></div>").addClass("box-footer clearfix");
                    back=$("<button>").addClass("btn btn-block btn-primary").attr({'data-toggle':'modal','data-target':'myModal','type':'button'})
                                    .click(function ()
                                        {
                                            switch(module){
                                                case 1:
                                                    $(this).prop('disabled', true);
                                                    that.setGeneralDetails();
                                                    that.setMarkInQuestion();
                                                break;
                                                case 2:
                                                    $(this).prop('disabled', false);
                                                    that.setQuestionDetails();
                                                    module--;
                                                break;
                                            }
                                        }
                                     )
                                     .append("Back");
                     next=$("<button>").addClass("btn btn-block btn-primary").attr({'data-toggle':'modal','data-target':'myModal','type':'button'})
                                    .click(function ()
                                        {
                                            switch(module){
                                                case 1:
                                                    if(that.checkValidation()){
                                                        that.setGneInObj();
                                                        $(this).prop('disabled', true);
                                                        that.setQuestionDetails();
                                                        module++;
                                                    }
                                                break;
                                                case 2:
                                                    if(that.checkQusValidation()){
                                                        that.setMarkInQuestion();
                                                        that.setSectio_TopicDetails();
                                                        $(this).prop('disabled', false);
                                                        module++;
                                                    }
                                                break;
                                                case 3:
                                                    config.getView("ModelVgr").setEnableButton ();  
                                                break;
                                            }
                                        })
                                                .append("Next");               
                        div.append($("<div></div>").addClass("col-md-3 col-xs-6 pull-left")
                                .append(back));
                            div.append($("<div></div>").addClass("col-md-3 col-xs-6 pull-right")
                                .append(next)
                                );
                        return div;
                }
                
                
                this.setGneInObj = function (){
                    form.find(":input").each(function() {
                            if($(this).attr("name")=="unique_test_key"){
                                gen["unique_test_key"] = $(this).val();
                            }
                            if($(this).attr("name")=="test_name"){
                                gen["test_name"] = $(this).val();
                            }
                            if($(this).attr("name")=="time"){
                                gen["time"] = $(this).val();
                            }
                            if($(this).attr("name")=="cut_off"){
                                gen["cut_off"] = $(this).val();
                            }
                            if($(this).attr("name")=="e_date"){
                                gen["e_date"] = $(this).val();
                            }
                            if($(this).attr("name")=="start_time"){
                                gen["start_time"] = $(this).val();
                            }
                            if($(this).attr("name")=="meditory_test_id"){
                                gen["meditory_test_id"] = $(this).val();
                            }
                            if($(this).attr("name")=="mark"){
                                if(gen["check"]==true){
                                    gen["mark"] = $(this).val();
                                }else{
                                    gen["mark"]=null;
                                }
                            }
                        });
                        
                        gen["exam_phase_id"] = form.find(".sel_phase option:selected").val();
                        gen["category_id"] = question["category_id"];
                        gen["subcategory_id"] = question["subcategory_id"];
                }
                
                this.checkValidation = function (){
                    var msg="" ;
                    
                    form.find(":input").each(function() {
                            if($(this).attr("name")=="test_name"){
                                if($(this).val()==""){
                                    msg='Test Name';
                                    return false;
                                }
                            }
                            if($(this).attr("name")=="time"){
                                if($(this).val()==""){
                                    msg='Time';
                                    return false;
                                }
                            }
                            if($(this).attr("name")=="e_date"){
                                if($(this).val()==""){
                                    msg='Exam Date';
                                    return false;
                                }
                            }
                            if($(this).attr("name")=="start_time"){
                                if($(this).val()==""){
                                    msg='Start Time';
                                    return false;
                                }
                            }
                        });
                        if(gen["type"]=="⇩"){
                                    msg='Test Type';
                        }
                        if(msg==""){
                            return true;
                        }else{
                            $("body").find("#hidden").find("#error_msg").remove();
                            msg= "Please "+msg+" should not blank.";
                            var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Error !",msg);
                            error_model.find(".modal-footer").find("button").first().remove();
                            $("#hidden").append(error_model);
                            error_model.modal('show');
                            return false;
                        }
                            
                        
                }
                
                this.checkQusValidation = function (){
                    var msg="" ;
                        question_mark.find(":input").each(function() {
                            try {
                                if(parseInt($(this).attr("name"))&&$(this).val()==""){
                                    msg="Question No "+$(this).attr("name");
                                    return false;
                                }
                            }catch(e){
                            }
                        });
                        if(msg==""){
                            return true;
                        }else{
                            $("body").find("#hidden").find("#error_msg").remove();
                            msg= "Please "+msg+" mark should not blank.";
                            var error_model = config.getView("ErrorMsgVgr").getErrorMsg("Error !",msg);
                            error_model.find(".modal-footer").find("button").first().remove();
                            $("#hidden").append(error_model);
                            error_model.modal('show');
                            return false;
                        }
                }
                
                
                
                this.setMarkInQuestion = function (){
                    question_mark.find(":input").each(function() {
                        try {
                            aaa[$(this).attr("name")]["mark"]=$(this).val();
                        }catch(e){
                        }
                    });
                }

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	CreateTestVgrVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insCreateTestVgr : function(from){},
			selCreateTestVgr : function() { },
                        delCreateTestVgr : function(id) { },
                        getCreateTestVgr : function(id) { },
                        updCreateTestVgr : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
