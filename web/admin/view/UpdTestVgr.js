jQuery.extend({
	UpdTestVgr: function(config,kk){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody_img;
                var question;
                var panel_title ;
                var create_test_box_body;
                var ul_question_box_body;
                var form ;
                var question_mark ;
                object={};
                aaa={};
                gen={};
                sec={};
                var module=1;
                var formula_icon;
                var formula_button;
                var formula_text_box;
                this.getUpdTest = function(test_id,value){
                    gen=value;
                    var div=$("<div></div>");
                        div.append(that.getBoxbody());
                        that.setGeneralDetails();
                        return div;
		}
                
                this.getForm = function (){
                    return form;
                }
                
                this.setGeneralDetails = function (){
                  create_test_box_body.empty();
                  formula_icon=$("<i>").addClass("fa  fa-check-square");
                  formula_button=$("<button>").addClass("btn btn-block btn-default btn-sm").attr({'data-toggle':'modal','data-target':'myModal','type':'button'}).append("Set")
                          .click(function (){
                                that.setFormulaModel();
                          });
                  form =$("<form></form>").addClass("form-group")
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Test Name"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'test_name','placeholder':'Test Name','name':'test_name','disabled':'disabled','value':(gen["test_name"]!=null ) ? gen["test_name"] : ""})))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Time Duration(Min.)"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'number','id':'time','name':'time','disabled':'disabled','value':(gen["time"]!=null ) ? gen["time"] : ""})))
                                        .append($("<div></div>").addClass("controls col-lg-4")
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Exam Date"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'e_date','name':'e_date','disabled':'disabled','value':(gen["e_date"]!=null ) ? gen["e_date"] : ""})))
                                        .append($("<div></div>").addClass("controls col-lg-4").css({'margin-top':'5px'})
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Start Time"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'time','id':'start_time','name':'start_time','disabled':'disabled','value':(gen["start_time"]!=null ) ? gen["start_time"] : ""})))
                                        .append($("<div></div>").addClass("controls col-lg-4").css({'margin-top':'5px'})
                                            .append($("<label></label>").addClass("control-label").attr({'for':'category'}).append("Meditory Test Id"))
                                            .append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':'meditory_test_id','placeholder':'Test ID','name':'meditory_test_id','disabled':'disabled','value':(gen["meditory_test_id"]!=null ) ? gen["meditory_test_id"] : ""}))
                                            )
                                        .append($("<div></div>").addClass("controls col-lg-4").css({'margin-top':'5px'}));
                    form.find("#check").trigger("change");
                    create_test_box_body.append(form);

                }
                
                this.getBoxbody = function (){
                    create_test_box_body =$("<div></div>").addClass("box-body no-padding");
                    return create_test_box_body;
                }
                
                this.setMarkInQuestion = function (){
                    question_mark.find(":input").each(function() {
                        try {
                            aaa[$(this).attr("name")]["mark"]=$(this).val();
                        }catch(e){
                        }
                    });
                }

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	UpdTestVgrVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insUpdTestVgr : function(from){},
			selUpdTestVgr : function() { },
                        delUpdTestVgr : function(id) { },
                        getUpdTestVgr : function(id) { },
                        updUpdTestVgr : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
