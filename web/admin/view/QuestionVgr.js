jQuery.extend({
	QuestionVgr: function(config,kk){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                var tbody_img;
                var question;
                var question_img;
                var ul_question_box_body;
                question={};
                
                this.getQuestion = function(){
                    question={};
                    
                    var div=$("<div></div>");
                        question_img =$("<div></div>").addClass("col-md-5");
                        question_img.append(that.getImgPanelTitle);
                        question_img.append(that.getUploadArea(100,18,"dragAndDropFiles1","demoFiler1","multiUpload1"));
                        question_img.append(that.getImgBoxFooter());
                        var box_img=that.getImgBoxbody();
                        var table_img = that.getImgTable();
                        tbody_img=table_img.find("tbody");
                        box_img.append(table_img);
                        question_img.append(box_img);
                        div.append(question_img);
                        //--------------------------------------------------------
			var panel_question =$("<div></div>").addClass("panel panel-default col-md-7")
                        var panel_question_body =$("<div></div>").addClass("panel-body")
                        panel_question_body.append(that.getQuestionBox().append(that.getAwsBox()));
                        panel_question.append(panel_question_body);
                        div.append(panel_question);
                        div.append(panel_question);
                        return div;
		}
                this.getObj=function (){
                    return question;
                }

                this.getImgPanelTitle = function (){
                    var div =$("<div></div>").addClass("box-header with-border");
                    div.append($("<h3></h3>").addClass("box-title").append($("<b>Question Relative Imges</b>")));
                    return div;
                }
                
                this.getTextEditor = function (editor){
                    var text_editor =$("<div></div>").addClass("pad col-lg-12");
                    text_editor.append($("<form></form>").append($("<textarea></textarea>").attr({'id':editor,'name':editor,'rows':'8','cols':'40'})))
                    return text_editor;
                }
                
                this.getQuestionBox = function (){
                    var question_box_body=$("<div></div>").addClass("box-body");
                    var question_input=$("<div></div>").addClass("input-group")
                            .append($("<input/>").addClass("form-control").attr({'id':'new-event-option','type':'text','placeholder':'Enter the number of Option'})
                            .keyup(function(event) {
                                if (event.keyCode==13) {
                                    if($("#new-event-option").val()!="")
                                    that.addOption(parseInt($("#new-event-option").val()));    
                                }
                            })
                                    )
                            .append($("<div></div>").addClass("input-group-btn")
                            .append($("<button>Add</button>").addClass("btn btn-primary btn-flat").attr({'id':'add-new-event','type':'button'}).css({'border-color':'rgb(0, 31, 63)','background-color':' rgb(0, 31, 63)'})
                                .click(function(){
                                    that.addOption(parseInt($("#new-event-option").val()));    
                                })
                            ));
                    question_box_body.append(question_input);
                    ul_question_box_body=$("<div></div>").addClass("text-center");
                    question_box_body.append(ul_question_box_body);
                    return question_box_body;
                }
                
                this.getAwsBox = function (){
                    var question_input=$("<div></div>").addClass("input-group-btn")
                            .append($("<p></p>"))
                            .append($("<select></select>").addClass("input-xlarge form-control valid").attr({'type':'text','id':'option','name':'option'})
                            );
                    return question_input;
                }

                this.addOption = function (no){
                    $("#option").empty();
                    ul_question_box_body.empty();
                    for(var i=1;i<=no;i++){
                        ul_question_box_body.append(that.getOptionText(String.fromCharCode(i+64)));
                        $("#option").append(that.getOption(String.fromCharCode(i+64)))
                    }
                }
                
                this.getOption = function (data){
                    var row = $("<option></option>").attr({'value':data})
                              .append($("<td>"+data+"</td>"));
                    return row;            
                }
                
                this.getOptionText = function (ch){
                    var div=$("<div></div>").addClass("controls").append($("<p></p>")).append($("<input/>").addClass("input-xlarge form-control").attr({'type':'text','id':ch,'placeholder':'Option is '+ch,'name':ch}));
                    return div;
                }
                
                this.getBoxbody = function (){
                    var div =$("<div></div>").addClass("box-body");
                    return div;
                }
                
                this.getImgBoxbody = function (){
                    var div =$("<div></div>").addClass("box-body");
                    return div;
                }
                                    
                this.getUploadArea = function (no,font,dragAndDropFiles,form_name,file){
                    var diva =$("<div></div>").addClass("box-body");
                    var div =$("<div></div>").addClass("uploadArea").attr({'id':dragAndDropFiles}).css({'min-height':no+'px'});
                    div.append($("<h1></h1>").append("Drop Images Here").css({'font-size':font+'px'}));
                    div.append($("<form></form>").attr({'name':form_name,'id':form_name,'enctype':'multipart/form-data'}).append($("<input/>").attr({'type':'file','data-maxwidth':'620','data-maxheight':'620','name':'file[]','id':file}).css({'width':'0px','height':'0px','overflow':'hidden'})));
                    diva.append(div);
                    return diva;
                }
                
                this.getBoxbody1 = function (){
                    var div =$("<div></div>").addClass("box-body").attr({'id':'sub_my'});
                    return div;
                }
                
                this.getTable = function (){
                    var div =$("<table></table>").addClass("table table-bordered")
                    //set header for table 
                            .append($("<thead></thead>")
                                .append($("<tr></tr>")
                                .append($("<th></th>").append("#").css({'width':'10px'}))
                                .append($("<th></th>").append("Question Name"))
                                .append($("<th></th>").append("Action").css({'width':'40px'}))
                            ))
                            .append($("<tbody></tbody>").attr({'id':'question_tb'}))
                    return div;
                }
                this.getImgTable = function (){
                    var div =$("<table></table>").addClass("table table-bordered")
                    //set header for table 
                            .append($("<thead></thead>")
                                .append($("<tr></tr>")
                                .append($("<th></th>").append("Images"))
                                .append($("<th></th>").append("Action").css({'width':'40px'}))
                            ))
                            .append($("<tbody></tbody>").attr({'id':'question_img'}))
                    return div;
                }
                
                this.getImgRow = function (data){
                    question[data.url]=data.image;
                    var row = $("<tr></tr>").attr({'id':data.url})
                            .append($("<td></td>").append(data.image))
                            .append($("<td></td>").append(that.getImgDropdown(data.url))
                    );
                    return row;            
                }
                
                this.getImgDropdown = function (a){
                    var drop_down =$("<div></div>").addClass("input-group-btn")
                                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                        .append($("<span></span>").addClass("fa fa-caret-down")))
                                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li></li>").append($("<a>Copy</a>")))
                                            );
                    return that.getImgClickEvent(drop_down,a);
                }
                
                
                this.getImgClickEvent =function (drop_down,question_id){
                    $(drop_down).find('ul>li:nth-child(1)').click(function (){
                        window.prompt("Copy to clipboard: Ctrl+V, Enter", question_id);
                    });
                    return drop_down;
                }
                
                this.getImgBoxFooter =function (){
                    var div = $("<div></div>").addClass("box-footer clearfix");
                        div.append($("<div></div>").addClass("col-md-12 col-xs-12")
                                .append($("<button></button>").addClass("btn btn-block btn-primary").attr({'data-toggle':'modal','data-target':'myModal','type':'button'})
                                    .append("Upload").click(function ()
                                        {
                                            config.getModel("QusUploadImg")._startUpload();
                                        }
                                     )
                                    ))
                        return div;
                }
                
                
                
                this.addImgQuestion =function (data){
                   tbody_img.append(that.getImgRow(data))
                }                                                        
                
                
                this.insLoadBegin =function (){
                    config.getView("ModelVgr").setloading();
                    config.getView("ModelVgr").setDisableButton();
                }
                
                this.insLoadFinish =function (){
                    config.getView("ModelVgr").removeLoading();
                    config.getView("ModelVgr").setEnableButton();
                }
                
                this.insLoadFail = function() { 
                    config.getView("ModelVgr").setServerError();
                }
                

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	QuestionVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                        insQuestion : function(from){},
			selQuestion : function() { },
                        delQuestion : function(id) { },
                        getQuestion : function(id) { },
                        updQuestion : function(from) { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
