jQuery.extend({
	MainSidebarVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                var no=false;
                var anchor;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
            
                this.setMainSideBar= function(){
			var aside=$("<aside></aside>").addClass("main-sidebar");
                        var section =$("<section></section>").addClass("sidebar");
                        section.append(that.setSidebarMenu());
                        aside.append(section);
                        return aside;
		}
                
                this.getSidebarMenu = function (){
                    return anchor;
                }
                this.setSidebarMenu = function (){
                    anchor =$("<ul></ul>").addClass("sidebar-menu");
                    anchor.append($("<li></li>").addClass("treeview")
                                    .append($("<a></a>").css({'cursor':'pointer'}).attr({'id':'dash_home'})
                                        .append($("<i></i>").addClass("fa fa-home"))
                                        .append($("<span></span>").append("Home"))
                                        .append($("<i></i>").addClass("fa fa-angle-left pull-right"))
                                        .click(function (){
                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                            if(config.getView("HomeVgr")==null){
                                                config.getView("MainVgr").getListener().loadHomeVgr();
                                                config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("HomeVgr").getHome());
                                            }else{
                                                config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("HomeVgr").getHome());
                                            }
                                         })
                                    )
                                                
                                )
                    .append($("<li></li>").addClass("treeview")
                                    .append($("<a></a>")
                                        .append($("<i></i>").addClass("fa fa-cogs"))
                                        .append($("<span></span>").append("Configuration"))
                                        .append($("<i></i>").addClass("fa fa-angle-left pull-right"))
                                    )
                                    .append($("<ul></ul>").addClass("treeview-menu")
                                            .append($("<li></li>").append($("<a></a>").append("Category")
                                                .click(function (){config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                    if(config.getView("CategoryVgr")==null){
                                                            config.getView("MainVgr").getListener().loadCategoryVgr();
                                                    }else{
                                                        config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                        config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("CategoryVgr").getCategory());
                                                    }

                                                    })
                                                ))
                                            .append($("<li></li>").append($("<a><//a>").append("Sub Category")
                                                .click(function (){
                                                    config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    
                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                        if(config.getView("SubCategoryVgr")==null){
                                                            config.getView("MainVgr").getListener().loadSubCategoryVgr();
                                                        }else{
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("SubCategoryVgr").getSubCategory());
                                                        }
                                                   })
                                                ))
                                            
                                                .append($("<li></li>").append($("<a></a>").append("Section Details")
                                                .click(function (){config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                        if(config.getView("SectionVgr")==null){
                                                            config.getView("MainVgr").getListener().loadSectionVgr();
                                                        }else{
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("SectionVgr").getSection());
                                                        }
                                                   })
                                                ))

                                                .append($("<li></li>").append($("<a></a>").append("Topic Details")
                                                .click(function (){config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                        if(config.getView("TopicVgr")==null){
                                                            config.getView("MainVgr").getListener().loadTopicVgr();
                                                        }else{
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("TopicVgr").getTopic());
                                                        }
                                                   })
                                                ))
                                                .append($("<li></li>").append($("<a></a>").append("Language")
                                                .click(function (){config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                        if(config.getView("LanguageVgr")==null){
                                                            config.getView("MainVgr").getListener().loadLanguageVgr();
                                                        }else{
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("LanguageVgr").getLanguage());
                                                        }
                                                   })
                                                ))
                                                .append($("<li></li>").append($("<a></a>").append("Exam Phase")
                                                .click(function (){config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                        if(config.getView("ExamPhaseVgr")==null){
                                                            config.getView("MainVgr").getListener().loadExamPhaseVgr();
                                                        }else{
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("ExamPhaseVgr").getExamPhase());
                                                        }
                                                   })
                                                ))
                                    )
                                )
                                .append($("<li></li>").addClass("treeview")
                                    .append($("<a></a>")
                                        .append($("<i></i>").addClass("fa fa-pencil-square-o"))
                                        .append($("<span></span>").append("Question Management"))
                                        .append($("<i></i>").addClass("fa fa-angle-left pull-right"))
                                    )
                                    .append($("<ul></ul>").addClass("treeview-menu")
                                            .append($("<li></li>").append($("<a></a>").append("Direction Wise Question")
                                                .click(function (){config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                        if(config.getView("DirectionVgr")==null){
                                                            config.getView("MainVgr").getListener().loadDirectionVgr();
                                                        }else{
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("DirectionVgr").getDirection());
                                                        }
                                                   })
                                            ))
                                            .append($("<li></li>").append($("<a></a>").append("Question without Direction")
                                                .click(function (){config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                        if(config.getView("OnlyQuestionVgr")==null){
                                                            config.getView("MainVgr").getListener().loadOnlyQuestionVgr();
                                                        }else{
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("OnlyQuestionVgr").getOnlyQuestionVgr());
                                                        }
                                                   })
                                            ))
                                            .append($("<li></li>").append($("<a></a>").append("View Question Bank")
                                                .click(function (){config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                        if(config.getView("ViewQuestionVgr")==null){
                                                            config.getView("MainVgr").getListener().loadViewQuestionVgr();
                                                        }else{
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("ViewQuestionVgr").getViewQuestionVgr());
                                                        }
                                                   })
                                            ))
                                            
                                    )
                                )
                                .append($("<li></li>").addClass("treeview")
                                    .append($("<a></a>")
                                        .append($("<i></i>").addClass("fa fa-list"))
                                        .append($("<span></span>").append("User Management"))
                                        .append($("<i></i>").addClass("fa fa-angle-left pull-right"))
                                    )
                                    .append($("<ul></ul>").addClass("treeview-menu")
                                            .append($("<li></li>").append($("<a></a>").append("View Test")
                                                .click(function (){config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                        if(config.getView("ViewTestVgr")==null){
                                                            config.getView("MainVgr").getListener().loadViewTestVgr();
                                                        }else{
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("ViewTestVgr").getViewTestVgr());
                                                        }
                                                   })
                                            ))
                                            .append($("<li></li>").append($("<a></a>").append("User Details")
                                                .click(function (){config.getView("MainVgr").getContentWrapper_BoxBody().empty();                                                    config.getView("MainVgr").getContentWrapper_BoxBody().append(that.setViewLoading());
                                                        if(config.getView("UserDetailsVgr")==null){
                                                            config.getView("MainVgr").getListener().loadUserDetailsVgr();
                                                        }else{
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().empty();
                                                            config.getView("MainVgr").getContentWrapper_BoxBody().append(config.getView("UserDetailsVgr").getUserDetailsVgr());
                                                        }
                                                   })
                                            ))
                                            
                                    )
                                );
                    
                    return anchor;
                }

                this.addListener = function(list){
                            listeners.push(list);
                }
                this.setViewLoading = function (){
                    return $("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin"));
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	MainSidebarVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
