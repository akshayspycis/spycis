jQuery.extend({
	MainVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
                var wrapper;
                
                this.setBody = function(){
                         config.finishLoadingJs($("body"));
                         $("body").removeClass("login-page");
                         wrapper=$("<div>").addClass("wrapper");
                         wrapper.append(config.getView("HeaderVgr").setHeader());
                         wrapper.append(config.getView("MainSidebarVgr").setMainSideBar());
                         wrapper.append(config.getView("ContentWrapperVgr").setContentWrapper());
                         wrapper.append(config.getView("FooterVgr").getFooter());
                         $("body").append(wrapper); 
                         if(config.getWidth()>991){
                             $( window ).scroll(function() {
                             var st = $(this).scrollTop();
                               var window_top = $(window).scrollTop();
                                    if (window_top > 160) {
                                        var nav_tabs_custom=$('.nav-tabs-custom').find('#activity').find("div:first-child").width();
                                        var lsl=nav_tabs_custom*25/100;
                                        var lsi=(lsl*100)/(config.getWidth())+.25
                                        $('#profile_box').css({'position':'fixed','top':'55px','left':'70px','width':lsi+'%'});
                                        $('#box_post').css({'position':'relative','left':$('#profile_box').width()+31});
                                        lsl=nav_tabs_custom*25/100;
                                        lsi=(lsl*100)/(config.getWidth())+.35
                                        $('#box_ads').css({'position':'fixed','right':(config.getWidth() - ($(".content").find(".box:nth-child(2)").offset().left + $(".content").find(".box:nth-child(2)").outerWidth()))-14+'px','width':lsi+'%','top':'55px'});
                                    } else {
                                        $('#profile_box').css({'position':'','z-index':'','top':'','left':'','width':''});
                                        $('#box_post').css({'position':'','left':''});
                                        $('#box_ads').css({'position':'','right':'','width':'','top':''});
                                    }
                               lastScrollTop = st;
                            });
                             
                        }else{
                            var lastScrollTop=0;
                            $( window ).scroll(function() {
                             var st = $(this).scrollTop();
                               if (st > lastScrollTop){
                                   // downscroll code
                                   $(".logo").hide();
                               } else {
                                   if( st==0 && !$('.main-header').find(".navbar").find(".navbar-nav").hasClass("okok")){
                                       $(".logo").show();
                                    }
                               }
                               lastScrollTop = st;
                            });
                            
                        }
                         
                         if(that.checkLocal("testasdsaidasdasdasdasdasd",config.setTest_id)){
                             if(that.checkLocal("test_time",config.setTest_Time) && that.checkLocal("lang_id",config.setLanguage_id)){
                                that.setTest();
                             }else{
                                that.setProfile();
                             }
                         }else{
                             that.setProfile();
                         }
		}
                this.setLoading = function (){
                            $("body").find('.main-footer').css({'padding':'15px'});
                            config.getView("ContentWrapperVgr").removeBoxHeader();
                            config.getView("ContentWrapperVgr").removeBoxbody();
                            config.getView("ContentWrapperVgr").removeBoxFooter();
                            config.getView("ContentWrapperVgr").setBoxBodyContent($("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin")));
                }
                
                this.checkLocal = function (key,set){
                    if(typeof(Storage) !== "undefined") {
                        if(localStorage.getItem(key)!=null){
                            set(decodeURIComponent(localStorage.getItem(key)));
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    } 
                }
                this.setInstruncation=function (){
                    that.setLoading();
                    config.getView("MainVgr").getListener().loadLanguageVgr();
                    if(config.getController("InstrctionMgr")==null){
                        $.getScript("../controller/InstrctionMgr.js").done(function() {
                                    try{
                                        config.setController("InstrctionMgr",new $.InstrctionMgr(config));
                                        config.getController("InstrctionMgr").loadHeaderProfileVgr();
                                }catch(e){
                                    alert("Error in InstrctionMgr "+e);
                                }
                                }).fail(function() {alert("Error:002 InstrctionMgr loading problem");}); 
                    }else{
                        config.getController("InstrctionMgr").loadHeaderProfileVgr();
                    }
                }
                this.setTest=function (){
                    that.setLoading();
                    if(config.getController("TestMgr")==null){
                        $.getScript("https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML").done(function() {
                            $.getScript("../model/LaTeXMathematics.js").done(function() {
                                config.setModel("LaTeXMathematics",new $.LaTeXMathematics(config));
                                $.getScript("../controller/TestMgr.js").done(function() {
                                    try{
                                        config.setController("TestMgr",new $.TestMgr(config));
                                        config.getController("TestMgr").loadControlSidebarVgr();
                                    }catch(e){
                                        alert("Error in TestMgr"+e);
                                    }
                                }).fail(function() {alert("Error:002 TestMgr loading problem");}); 
                            }).fail(function() {alert("Error:002 LaTeXMathematics loading problem");}); 
                        }).fail(function() {alert("Error:002 cloudflareloading problem");}); 
                    }else{
                        config.getController("TestMgr").loadControlSidebarVgr();
                    }
                }
                this.setProfile=function (){
                    if($("body").hasClass("control-sidebar")){
                       $("body").find(".control-sidebar").remove() ;
                    }
                    that.setLoading();
                    if(config.getController("ProfileMgr")==null){
                        $.getScript("../controller/ProfileMgr.js").done(function() {
                            try{
                                config.setController("ProfileMgr",new $.ProfileMgr(config));
                                config.getController("ProfileMgr").loadHeaderProfileVgr();
                            }catch(e){
                                alert("Error in ProfileMgr"+e);
                            }
                                }).fail(function() {alert("Error:002 ProfileMgr loading problem");}); 
                    }else{
                        config.getController("ProfileMgr").loadHeaderProfileVgr();
                    }
                }
                
                
                
                this.setControlSidebar = function (div){
                    $("body").append(div); 
                }
                this.setFooter = function (div){
                    $("body").append(div); 
                }
                this.load =function (){
                        $.each(listeners,function(i){
                            listeners[i].loadHeaderVgr();
                        });
                }
                
                this.getListener = function (){
                    var l;
                    $.each(listeners, function(i){
                            l=listeners[i];
                    });
                    return l;
                }
                
                this.addListener = function(list){
                            listeners.push(list);
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	MainVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadHeaderVgr : function() { },
                        loadMainSidebarVgr : function() { },
                        loadContentWrapperVgr : function() { },
                        loadFooterVgr : function() { },
			loadFooterMgr : function() { }
		}, list);
	}
});
