jQuery.extend({
	ContentWrapperVgr: function(config){
            var that = this;
            var section;
            var content;
            var listeners = new Array();
            var that = this;
            var li ;
            var b ;
            var  box_header;
            var  box_footer;
            var  box_body;
            var  box_top_body_collapse;
            var  box_top_body;
            this.setContentWrapper= function(){
                var aside=$("<div>").addClass("content-wrapper");
                aside.append(that.getContentHeader())
                aside.append(that.getContent());
                return aside;
            }
            
            this.setHeaderTitle =function(section_name){
                b.empty();
                b.append(section_name);
            }
            this.setBreadCrumbName =function (icon,title){
                li.empty();
                li.append($("<i>").addClass("fa "+icon)).append("&nbsp;&nbsp;"+title);
            }
            this.setBoxHeaderTitle = function (title){
                box_header.empty();
                box_header.append(title);
            }
            this.setBoxBodyContent= function (con){
                box_body.append(con);
            }
            this.getContentHeader =function (section_name){
                b=$("<p>");
                li=$("<li>");
                return $("<section>").addClass("content-header").append(b).append($("<ol>").addClass("breadcrumb").append(li));
            }
            this.getContent=function (){
                    content=$("<section>").addClass("content");
                    if(config.getWidth()>1100){
                        content.append(that.getTopBox())
                    }else{
                        content.append(that.getTopBox().css('display','none'))
                    }
                    content.append(that.getBox())
                    return content;
            }
            this.getBox = function (){
                var high;
                if(config.getWidth()>1100){
                        high=high-310-25;
                    }else if(config.getWidth()>991&&config.getWidth()<1100){
                        high=high+75-310-25;
                    }
                box_header=$("<div>").addClass("box-header with-border");
                if(config.getWidth()>769){
                    box_body=$('<div>').addClass("box-body").css({'height':high+'px'});
                }else{
                    box_body=$('<div>').addClass("box-body");
                }
                
                box_footer=$('<div>').addClass("box-footer");
                return $("<div>").addClass("box").append(box_header).append(box_body).append(box_footer);
            }
            this.getTopBox = function (){
                box_top_body_collapse=$('<div>').addClass("collapse navbar-collapse").attr({'id':'bs-example-navbar-collapse-1'});
                box_top_body= $("<div>").addClass("box").append($('<nav>').addClass("navbar navbar-default").attr({'role':'navigation'})
                        .append($('<div class="navbar-header"><a class="navbar-brand" href="#">Sections</a></div>'))
                        .append(box_top_body_collapse));
                return box_top_body;
            }
            this.hideTopBox = function (){
                box_top_body.css({'display':'none'});
            }
            this.showTopBox = function (){
                    if(config.getWidth()>1100){
                        box_top_body.css({'display':'block'});
                    }else{
                        box_top_body.css({'display':'none'});
                    }
            }
            this.removeBoxHeader = function (){
                box_header.empty();
            }
            this.removeTopBox= function (){
                box_header.css({'display':'none'});
            }
            this.setTopBox= function (){
                box_header.css({'display':'block'});
            }
            this.setTopBoxContent= function (div){
                box_top_body_collapse.append(div);
            }
            this.removeTopBoxContent= function (){
                box_top_body_collapse.empty();
            }
            this.setBoxHeader = function (div){
                box_header.append(div);
            }
            this.removeBoxbody= function (div){
                box_body.empty();
            }
            this.removeBoxFooter = function (div){
                box_footer.empty();
            }
            this.setBoxFooter = function (div){
                box_footer.empty();
                box_footer.append(div);
            }
         
        }
});