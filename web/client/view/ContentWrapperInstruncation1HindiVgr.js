jQuery.extend({
	ContentWrapperInstruncation1HindiVgr: function(config,per){
            var that=this;
            this.init = function (){
                per.setHeaderTitle("ऑनलाइन आकलन टेस्ट");
                per.setBreadCrumbName("fa-dashboard","अनुदेश");
                per.removeBoxHeader();
                per.removeBoxbody();
                per.setBoxHeaderTitle($("<h3>").addClass("box-title").css({'line-height':':24px;'}).append("कृपया ध्यान से निम्नलिखित निर्देश पढ़ें"));
                per.setBoxBodyContent($('<h4><b>सामान्य निर्देश:</b></h4><ol class="instruction"><li>मिनट की अवधि का कुल सब सवालों का प्रयास करने के लिए दी जाएगी</li><li>घड़ी सर्वर पर स्थापित किया गया है और अपनी स्क्रीन के ऊपरी दाएँ कोने पर उलटी गिनती घड़ी समय आप परीक्षा को पूरा करने के लिए शेष को प्रदर्शित करेगा। घड़ी बाहर चलाता है जब परीक्षा डिफ़ॉल्ट रूप से समाप्त होता है - आप अंत या अपने परीक्षा प्रस्तुत करने की आवश्यकता नहीं कर रहे हैं ।</li><li>स्क्रीन के अधिकार पर सवाल पैलेट गिने सवालों में से प्रत्येक के निम्न स्थितियों में से एक से पता चलता है</li></ol><h4><b>नेविगेशन प्रश्न:</b></h4><ol class="instruction"><li>मिनट की अवधि का कुल सब सवालों का प्रयास करने के लिए दी जाएगी</li><li>घड़ी सर्वर पर स्थापित किया गया है और अपनी स्क्रीन के ऊपरी दाएँ कोने पर उलटी गिनती घड़ी समय आप परीक्षा को पूरा करने के लिए शेष को प्रदर्शित करेगा। घड़ी बाहर चलाता है जब परीक्षा डिफ़ॉल्ट रूप से समाप्त होता है - आप अंत या अपने परीक्षा प्रस्तुत करने की आवश्यकता नहीं कर रहे हैं ।</li><li>स्क्रीन के अधिकार पर सवाल पैलेट गिने सवालों में से प्रत्येक के निम्न स्थितियों में से एक से पता चलता है</li></ol>'));
                per.setBoxFooter(that.getFooter());
            }
            this.getFooter = function (){
                return $("<center>").append($("<a>").addClass("next").append($("<b>").append("Next")).css({'cursor':'pointer'}).click(function (){
                    config.setIns(2);
                    config.getController("InstrctionMgr").loadContentWrapperInstruncation2HindiVgr();
                }));
            }
        }
});