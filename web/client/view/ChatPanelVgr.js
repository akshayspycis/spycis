jQuery.extend({
	ChatPanelVgr: function(config){
		var that = this;
		var listeners = new Array();
                var chatbox;
                var direct_chat_messages;
                var direct_chat_text_box;
                
                this.getChatPanel = function(obj){
                        chatbox=$("<div>").addClass("col-lg-3 col-md-3 col-sm-4 col-xs-10").css({'position':'fixed','bottom':'-20px','float':'right'}).attr({'id':obj.user_id});
                        
                        var content=$("<div>").addClass("box direct-chat");
                        config.getModel("ChatList").setChatBoxObject(obj.user_id,"chat_img_typing",$('<img />', { 
                                    class:'direct-chat-img',
                                    css:{'width':'66px','height':'66px','top':'-61px','position': 'absolute','right':'10px','display':'none'},
                                    src: '../../dist/img/chat.gif',
                                    alt: 'User Image'
                                    }));
                        content.append(config.getModel("ChatList").getChatBoxObject(obj.user_id,"chat_img_typing"));
                        content.append(that.getChatPanelHeader(obj));
                        content.append(that.getChatPanelBody());
                        config.getModel("ChatList").setChatBoxObject(obj.user_id,"direct_chat_messages",direct_chat_messages);
                        content.append(that.getChatPanelFooter(obj));
                        config.getModel("ChatList").setChatBoxObject(obj.user_id,"direct_chat_text_box",direct_chat_text_box);
                        config.getModel("ChatList").setChatBoxObject(obj.user_id,"chack_user_left",true);
                        config.getModel("ChatList").setChatBoxObject(obj.user_id,"chack_user_left_type",true);
                        config.getModel("ChatList").setChatBoxObject(obj.user_id,"chack_user_right",true);
                        chatbox.append(content);
                        obj["chatbox"]=chatbox;
                        return obj;
		}
                this.getChatPanelHeader= function (obj){
                    var header =$("<div>").addClass("box-header with-border").css({'border': '1px solid rgb(210, 214, 222)'});
                    header.append($('<img />', { 
                                    class:'direct-chat-img',
                                    css:{'width':'25px','height':'25px','left': '-5px','top':'-4px','position': 'relative'},
                                    src: obj.pic,
                                    alt: 'User Image'
                                    }).on('error', function() { $(this).attr({'src':'../../dist/img/avatar.png'}) }))
                    header.append($("<h3>").addClass("box-title").append(obj.user_name));
                    header.append($("<div>").addClass("box-tools pull-right")
                        .append($("<span>").addClass("badge bg-light-blue").attr({'data-toggle':'tooltip','data-original-title':'3 New Messages'}).append("3"))
                        .append($("<button>").addClass("btn btn-box-tool").attr({'data-widget':'collapse'}).append($("<i>").addClass("fa fa-minus"))
                                .click(function (){
                                    if($(this).find("i").hasClass("fa-plus")){
                                        $(this).parent().parent().parent().removeClass("collapsed-box")
                                        $(this).parent().parent().parent().find(".box-body").css({'display': 'block'})
                                        $(this).parent().parent().parent().find(".box-footer").css({'display': 'block'})
                                        $(this).find("i").removeClass("fa-plus")
                                        $(this).find("i").addClass("fa-minus")
                                    }else{
                                        $(this).parent().parent().parent().addClass("collapsed-box")
                                        $(this).find("i").removeClass("fa-minus")
                                        $(this).parent().parent().parent().find(".box-body").css({'display': 'none'})
                                        $(this).parent().parent().parent().find(".box-footer").css({'display': 'none'})
                                        $(this).find("i").addClass("fa-plus")
                                    }
                                })
                            )
                        .append($("<button>").addClass("btn btn-box-tool").attr({'data-widget':'collapse'}).append($("<i>").addClass("fa fa-comment")))
                        .append($("<button>").addClass("btn btn-box-tool").attr({'data-widget':'remove','id':'remove'}).append($("<i>").addClass("fa fa-times"))
                                .click(function (){
                                    config.getModel("ChatList").removeChatBoxObject(obj.user_id,"direct_chat_messages");
                                    config.getModel("ChatList").removeChatBoxObject(obj.user_id,"direct_chat_text_box");
                                    config.getView("ChatListVgr").deleteChatBox(obj.user_id)
                                })
                            ))
                    return header;
                }
                this.getChatPanelBody = function (){
                    direct_chat_messages=$("<div>").addClass("direct-chat-messages");
                    return $("<div>").addClass("box-body").css({'display':'block','height':'270px'}).css({'border': '1px solid rgb(210, 214, 222)'}).append(direct_chat_messages);
                }
                this.getChatPanelFooter = function (obj){
                    direct_chat_text_box=$("<div>").append($("<input>").addClass("form-control").attr({'type':'text'}).css({'border': '1px solid rgb(210, 214, 222)'})
                                .keyup(function(e){
                                   var code = e.keyCode ? e.keyCode : e.which;
                                   if (code == 13 && $(this).val().trim()!="") {
                                       console.log("client="+config.getModel("ChatList").getChatBoxObject(obj.user_id,"chack_user_left") + " = " +config.getModel("ChatList").getChatBoxObject(obj.user_id,"chack_user_left_type"))
                                 if(!config.getModel("ChatList").getChatBoxObject(obj.user_id,"chack_user_left")){
                                    if(!config.getModel("ChatList").getChatBoxObject(obj.user_id,"chack_user_left_type")){
                                            if(config.getModel("ChatList").getChatBoxObject(obj.user_id,"left_chat_msg").children("div").size()>2){
                                                config.getModel("ChatList").getChatBoxObject(obj.user_id,"left_chat_msg").find("div:last-child").remove();
                                            }
                                    }else{
                                        config.getModel("ChatList").setChatBoxObject(obj.user_id,"chack_user_left",true);           
                                    }
                                 }
                                     that.setDirectChatMsgRight(obj,$(this).val().trim());  
                                     $(this).val("");
                                   }
                                })
                                .focus(function (){
                                    config.getModel("ChatList").getChatBoxObject(obj.user_id,"chat_img_typing").css('display','block');
                                    var message_details={}
                                        message_details['sender_id']=config.getModel("ProfileConfig").getUser_id();
                                        message_details['receiver_id']=obj.user_id;
                                        message_details['subject']="chat_img_typing_focus_in";
                                        message_details['body']='';
                                        config.getModel("ChatList").getUserConn(config.getModel("ChatList").getUserDetails(obj.user_id).keyid).send(JSON.stringify(message_details));
                                })        
                                .focusout(function (){
                                    config.getModel("ChatList").getChatBoxObject(obj.user_id,"chat_img_typing").css('display','none');
                                    var message_details={}
                                        message_details['sender_id']=config.getModel("ProfileConfig").getUser_id();
                                        message_details['receiver_id']=obj.user_id;
                                        message_details['subject']="chat_img_typing_focus_out";
                                        message_details['body']='';
                                        config.getModel("ChatList").getUserConn(config.getModel("ChatList").getUserDetails(obj.user_id).keyid).send(JSON.stringify(message_details));
                                })        
                            );
                    
                    return $("<div>").addClass("box-footer with-border").css({'display':'block'}).css({'border': '1px solid rgb(210, 214, 222)'})
                            .append(direct_chat_text_box)
                            .append($("<div>").addClass("box-tools pull-left").css({'margin-top':'10px'})
                                    .append($("<span>").addClass("badge bg-light-blue").attr({'data-toggle':'tooltip','data-original-title':'Video Calling'}).css({'cursor':'pointer'})
                                            .click(function (){
                                                config.getModel("ChatList").callVideo(obj);
                                               
                                            }).append($("<i>").addClass("fa fa-video-camera").css({'font-size':'1.5em'}))
                                    ));
                }
                
                
                this.setDirectChatMsgLeft=function (obj,msg){
                    if(config.getModel("ChatList").getChatBoxObject(obj.sender_id,"chack_user_left")){
                        config.getModel("ChatList").setChatBoxObject(obj.sender_id,"left_chat_msg",that.getDirectChatMsgLeft(obj,msg));
                        config.getModel("ChatList").getChatBoxObject(obj.sender_id,"direct_chat_messages")
                                    .append(config.getModel("ChatList").getChatBoxObject(obj.sender_id,"left_chat_msg"));
                        config.getModel("ChatList").setChatBoxObject(obj.sender_id,"chack_user_left",false);
                    }else{
                        if(config.getModel("ChatList").getChatBoxObject(obj.sender_id,"chack_user_left_type")){
                            config.getModel("ChatList").getChatBoxObject(obj.sender_id,"left_chat_msg").append(that.getDirectChatText(msg));
                        }else{
                            config.getModel("ChatList").getChatBoxObject(obj.sender_id,"left_chat_msg").find("div:last-child").empty();
                            config.getModel("ChatList").getChatBoxObject(obj.sender_id,"left_chat_msg").find("div:last-child").append(msg);
                        }
                    }
                    config.getModel("ChatList").setChatBoxObject(obj.sender_id,"chack_user_left_type",true);
                    config.getModel("ChatList").setChatBoxObject(obj.sender_id,"chack_user_right",true);
                    var scrollTo_int = config.getModel("ChatList").getChatBoxObject(obj.sender_id,"direct_chat_messages").prop('scrollHeight') + 'px';
                    config.getModel("ChatList").getChatBoxObject(obj.sender_id,"direct_chat_messages").slimScroll({
                        scrollTo : scrollTo_int,
                        height: '200px',
                        start: 'bottom',
                        alwaysVisible: true
                    });
                }
                this.getTypeImg= function (){
                    return $('<img />', { 
                                    css:{'height':'61px'},
                                    src: '../../dist/img/chat_type.gif',
                                    alt: 'User Image'
                                    })
                }
                this.setTypingMsgLeft=function (obj){
                    if(config.getModel("ChatList").getChatBoxObject(obj.sender_id,"chack_user_left")){
                        config.getModel("ChatList").setChatBoxObject(obj.sender_id,"left_chat_msg",that.getDirectChatMsgLeft(obj,that.getTypeImg()));
                        config.getModel("ChatList").getChatBoxObject(obj.sender_id,"direct_chat_messages")
                                    .append(config.getModel("ChatList").getChatBoxObject(obj.sender_id,"left_chat_msg"));
                        config.getModel("ChatList").setChatBoxObject(obj.sender_id,"chack_user_left",false);
                        config.getModel("ChatList").setChatBoxObject(obj.sender_id,"chack_user_left_type",false);
                    }else{
                        if(config.getModel("ChatList").getChatBoxObject(obj.sender_id,"chack_user_left_type")){
                            config.getModel("ChatList").getChatBoxObject(obj.sender_id,"left_chat_msg").append(that.getDirectChatText(that.getTypeImg()));
                            config.getModel("ChatList").setChatBoxObject(obj.sender_id,"chack_user_left_type",false);
                        }else{
                            config.getModel("ChatList").getChatBoxObject(obj.sender_id,"left_chat_msg").find("div:last-child").empty();
                            config.getModel("ChatList").getChatBoxObject(obj.sender_id,"left_chat_msg").find("div:last-child").append(that.getTypeImg());
                        }
                    }
                    var scrollTo_int = config.getModel("ChatList").getChatBoxObject(obj.sender_id,"direct_chat_messages").prop('scrollHeight') + 'px';
                    config.getModel("ChatList").getChatBoxObject(obj.sender_id,"direct_chat_messages").slimScroll({
                        scrollTo : scrollTo_int,
                        height: '200px',
                        start: 'bottom',
                        alwaysVisible: true
                    });
                }
                this.setDirectChatMsgRight=function (obj,msg){
                    if(config.getModel("ChatList").getChatBoxObject(obj.user_id,"chack_user_right")){
                        config.getModel("ChatList").setChatBoxObject(obj.user_id,"right_chat_msg",that.getDirectChatMsgRight(msg));
                        config.getModel("ChatList").getChatBoxObject(obj.user_id,"direct_chat_messages")
                                    .append(config.getModel("ChatList").getChatBoxObject(obj.user_id,"right_chat_msg"));
                        config.getModel("ChatList").setChatBoxObject(obj.user_id,"chack_user_right",false);
                    }else{
                        config.getModel("ChatList").getChatBoxObject(obj.user_id,"right_chat_msg").append(that.getDirectChatText(msg));
                    }
                    var scrollTo_int = config.getModel("ChatList").getChatBoxObject(obj.user_id,"direct_chat_messages").prop('scrollHeight') + 'px';
                    config.getModel("ChatList").getChatBoxObject(obj.user_id,"direct_chat_messages").slimScroll({
                        scrollTo : scrollTo_int,
                        height: '200px',
                        start: 'bottom',
                        alwaysVisible: true
                    });
                    config.getModel("ChatList").setChatBoxObject(obj.user_id,"chack_user_left",true);
                    var message_details={}
                    message_details['sender_id']=config.getModel("ProfileConfig").getUser_id();
                    message_details['receiver_id']=obj.user_id;
                    message_details['subject']="Message";
                    message_details['body']=msg;
                    message_details['msg_data']=new Date();
                    message_details['msg_time']=new Date();
                    config.getModel("ChatList").getUserConn(config.getModel("ChatList").getUserDetails(obj.user_id).keyid).send(JSON.stringify(message_details));
                    $.each(listeners, function(i){
                        listeners[i].insChatPanel(JSON.stringify(message_details));
                    });
                }
                
                this.getDirectChatMsgLeft=function (obj,msg){
                    return $("<div>").addClass("direct-chat-msg")
                            .append($("<div>").addClass("direct-chat-info clearfix")
                            .append($("<span >").addClass("direct-chat-timestamp pull-right").append("23 Jan 2:00 pm")))
                            .append($('<img />', { 
                                    class:'menu-icon direct-chat-img',
                                    src: obj.pic,
                                }).on('error', function() { $(this).attr({'src':'../../dist/img/avatar.png'}) }))
                            .append(that.getDirectChatText(msg));
                }
                this.getDirectChatMsgRight=function (msg){
                    return $("<div>").addClass("direct-chat-msg right")
                            .append($("<div>").addClass("direct-chat-info clearfix")
                            .append($("<span >").addClass("direct-chat-timestamp pull-left").append("23 Jan 2:00 pm")))
                            .append(config.getModel("ProfileConfig").getProfilePic("img-circle").addClass("direct-chat-img"))
                            .append(that.getDirectChatText(msg));
                    
                }
                this.getDirectChatText = function (msg){
                    return $("<div>").addClass("direct-chat-text").append(msg);
                }
                
                this.setloading =function (){
                    footer.find("#img_loading").append(config.getLoadingData());
                }
                this.removeLoading =function (){
                    footer.find("#img_loading").empty();
                }
                
                this.setDisableButton =function (){
                    footer.find("#"+c).prop('disabled', true);
                    footer.find("#cancel").prop('disabled', true);
                }
                
                this.setEnableButton =function (){
                    footer.find("#"+c).prop('disabled', false);
                    footer.find("#cancel").prop('disabled', false);
                }
                
                this.setServerError =function (){
                    footer.find("#error_msg").append("Error: Server doesn't Respond.");
                }
                
                this.removeServerError =function (){
                    footer.find("#error_msg").empty();
                }
                
                this.addListener = function(list){
                            listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	ChatPanelVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
