jQuery.extend({
	HeaderProfileVgr: function(config){
		var that = this;
		var logout ;
		var ul_profile ;
		var temp_obj ;
		var mock ;
		var autolist ;
		var search_box_div ;
                this.getMock= function (){
                    return mock;
                }
		
		var listeners = new Array();
                
                this.getHeaderProfile = function(){
                        var software_details=config.getModel("ProfileConfig").getSoftwareDetails();
                        var limit=$("<p>").css({'color':'#b8c7ce','font-size':'14px'}).append($("<i>").addClass("fa fa-users").attr({'data-toggle':'tooltip','title':'Members Limit'}))
                                    .append("&nbsp;Member Limit 20"); 
                        mock=$("<span>").addClass("badge bg-light-blue pull-right").css({'font-size':'14px','cursor':'pointer'})
                                                                .append("&nbsp;&nbsp;&nbsp;Get&nbsp;&nbsp;&nbsp;")
                                                                .click(function (){
                                                                    that.loadModel(1,$(this));
                                                                });
                        if(software_details!=null){
                            mock=$("<span>").addClass("badge bg-light-blue pull-right").css({'font-size':'14px','cursor':'pointer'})
                                                                .append("&nbsp;&nbsp;&nbsp;Open&nbsp;&nbsp;&nbsp;")
                                                                .click(function (){
//                                                                    if(software_details["1"]["otp_authentication"]=="false"){
//                                                                        that.loadModel(11,$(this));
//                                                                    }else{
                                                                        if(config.getView("MockTestHomeVgr")==null){
                                                                            $.getScript("../view/MockTestHomeVgr.js").done(function(){
                                                                                config.setView("MockTestHomeVgr",new $.MockTestHomeVgr(config,config.getView("ContentWrapperVgr")));
                                                                                config.getView("MockTestHomeVgr").init();
                                                                                    $.getScript("../model/MockTestHome.js").done(function() {
                                                                                        config.setModel("MockTestHome",new $.MockTestHome(config));
                                                                                        $.getScript("../controller/MockTestHomeMgr.js").done(function() {
                                                                                            config.setController("MockTestHomeMgr",new $.MockTestHomeMgr(config,config.getModel("MockTestHome"),config.getView("MockTestHomeVgr")));
                                                                                            config.getView("MockTestHomeVgr").loadMockTestHome();
                                                                                        }).fail(function() {alert("Error :problem in MockTestHomeMgr.js file ");}); 
                                                                                    }).fail(function() {alert("Error:problem in MockTestHome.js file ");}); 
                                                                            }).fail(function() {alert("Error:004 problem in MockTestHomeVgr.js file ");}); 
                                                                         }else{
                                                                                config.getView("MockTestHomeVgr").loadMockTestHome();
                                                                         }
//                                                                    }
                                                                    
                                                                });
                            limit=$("<p>").css({'color':'#b8c7ce','font-size':'14px'}).append($("<i>").addClass("fa fa-users").attr({'data-toggle':'tooltip','title':'Members Limit'}))
                                    .append("&nbsp;&nbsp;20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") 
                                    .append($("<i>").addClass("fa fa-user").attr({'data-toggle':'tooltip','title':'Add Member'}))
                                    .append("&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") 
                                    .append($("<i>").addClass("fa fa-user").attr({'data-toggle':'tooltip','title':'Vacant'}))
                                    .append("&nbsp;20"); 
                            
                        }                                        
                        ul_profile=$("<ul>").addClass("nav navbar-nav")
                        .append($("<li>").addClass("no-padding").append($("<a>").css('cursor','pointer').append($("<i>").addClass("fa fa-rocket"))))
                        .append($("<li>").append($("<a>").attr({'data-toggle':'dropdown','aria-expanded':'false'}).append($("<i>").addClass("fa fa-th"))
                                            .click(function (){
                                                if($(this).parent().hasClass("open")){
                                                   $(this).parent().removeClass("open") 
                                                }else{
                                                    $(this).parent().addClass("open") 
                                                }
                                            })
                                    )
                                    .append($("<ul>").addClass("dropdown-menu").css({'width':'300px'})
                                        .append($("<li>").addClass("beeperNub"))
                                        .append($("<li>").addClass("header").append("You have 1 Utility"))
                                        .append($("<li>")
                                            .append($("<ul>").addClass("control-sidebar-menu menu").css({'overflow': 'hidden', 'width': 'auto', 'height': '250px'})
                                            .append($("<li>")
                                                .append($("<a>")
                                                    .append($("<img>").addClass("menu-icon direct-chat-img").attr('src','../../dist/img/lay1.png').css({'border-radius':'7%'}))
                                                    .append($("<div>").addClass("menu-info")
                                                        .append(mock)
                                                        .append($("<h3>").addClass("control-sidebar-subheading")
                                                            .append("&nbsp;Mock Test"))
                                                        .append(limit)
                                                         )
                                                 ))
                                                 ))
                                                 )
                                )
                                .append($("<li>").addClass("no-padding").append($("<a>").css('cursor','pointer').append("Home")))
                                .append($("<li>").addClass("dropdown user user-menu")
                                    .click(function (){
                                        $(this).addClass("open");
                                    })
                                    .append($("<a>").addClass("dropdown-toggle").css({'cursor':'pointer'})
                                            .append(config.getModel("ProfileConfig").getProfilePic("user-image"))    
                                            .append($("<span>").addClass("hidden-xs").append(config.getModel("ProfileConfig").getFirstName()))
                                            .attr({'data-toggle':'dropdown'})
                                        )
                                    .append($("<ul>").addClass("dropdown-menu")
                                        .append($("<li>").addClass("user-header")
                                            .append(config.getModel("ProfileConfig").getProfilePic("img-circle"))
                                            .append($("<p>").append(config.getModel("ProfileConfig").getFirstName())
                                            .append($("<small>").append("Candidate Id  V"+config.getModel("ProfileConfig").getUser_id())))
                                        )
                                        .append($("<li>").addClass("user-footer")
                                            .append($("<div>").addClass("pull-left")
                                                    .append(that.getButton("Profile","profile",that.callback))
                                             )
                                            .append($("<div>").addClass("pull-right")
                                                    .append(that.getButton("LogOut","logout",that.logOut))
                                             )
                                        )
                                     ))
                                
                                .append(that.getControlSidebar_Btn());
                    return ul_profile;
		}
                this.getSearchLargeBox = function (){
                    autolist=$("<ul>").addClass("control-sidebar-menu").css({'display':'block','position':'absolute','z-index':'1000'}).attr({'id':'autosuggestion'}).focusout(function(e){
                     })
                     $(window).click(function() {
                        if(autolist.children().size()>0)
                        autolist.empty();
                     });
                     var cod=0;
                    search_box_div=$("<div>").addClass("col-sm-5 col-md-4 col-xs-7").attr({'id':'search_box_div'})
                            .append($("<div>").addClass("input-group").css({'margin-top':'8px'})
                                .append($("<a>").addClass("input-group-addon").css({'font-size':'20px','border-radius':'4px'}).append($("<i>").addClass("fa fa-search"))
                                    .click(function (){
                                        if(config.getWidth()<460){
                                            if($(this).next().hasClass("search_box_scroll")){
                                                $(this).next().removeClass("search_box_scroll");
                                                $(this).next().css({'display':'none','width':'0px'});
                                            }else{
                                                var width=config.getWidth()-105;
                                                $(this).next().addClass("search_box_scroll");
                                                $(this).next().css({'display':'block','-webkit-transition':'width 0.4s ease-in-out','transition':'width 0.4s ease-in-out','width':width+'px','position':'absolute','z-index':'1000000'});
                                            }
                                        }
                                    })
                                )
                                .append($("<input>").addClass("form-control").attr({'type':'text','placeholder':'Search Infopark','autocomplete':'off'}).css({'font-size':'20px','border-left':'none','margin-left':'-7px'})
                                .keydown(function(e){
                                   var code = e.keyCode ? e.keyCode : e.which;
                                   cod=code;
                                   if (code == 13 && $(this).val().trim()!="") {
                                       
                                   }else if(code == 38 || code == 40 ){
                                       if(code==40){
                                           if($(this).parent().parent().find("ul li.active").length!=0) {
                                                var storeTarget= $(this).parent().parent().find("ul li.active").next();
                                                $(this).parent().parent().find("ul li.active").removeClass("active");
                                                storeTarget.focus().addClass("active");
                                           }else {
                                               $(this).parent().parent().find("ul li:first").focus().addClass("active");
                                           }     
                                           return ;
                                       }else{
                                           if($(this).parent().parent().find("ul li.active").length!=0) {
                                                var storeTarget	= $(this).parent().parent().find("ul li.active").prev();
                                                $(this).parent().parent().find("ul li.active").removeClass("active");
                                                storeTarget.focus().addClass("active");
                                            }else {
                                                $(this).parent().parent().find("ul li:first").focus().addClass("active");
                                            }
                                            return ;
                                       }
                                   }else{
                                           autolist.empty();
                                   }
                                })
                                        .autocomplete({
                                    source: function (request, response) {
                                        
                                        if(request.term!=""){
//                                            alert(co)
                                            if(cod== 38 || cod== 40 ){
                                                return false;
                                            }
                                            
                                            try {
                                                $.each(listeners, function(i){
                                                    listeners[i].selAutoSuggestion(request, response);
                                                 });
                                             }catch(e){}
                                        }else{
                                            autolist.empty();
                                        }
                                         
                                    }
                                })
                                )
                                ).append(autolist);    
                        if(config.getWidth()>=529 && config.getWidth()<570){
                           search_box_div.removeClass("col-xs-7")
                           search_box_div.addClass("col-xs-6")
                       }else if(config.getWidth()>460&&config.getWidth()<529){
                           search_box_div.removeClass("col-xs-7")
                           search_box_div.addClass("no-padding")
                           search_box_div.addClass("col-xs-5")
                       }else if(config.getWidth()<460){
                           search_box_div.removeClass("col-xs-7")
                           search_box_div.addClass("no-padding")
                           search_box_div.addClass("col-xs-1")
                           search_box_div.find(".input-group").find(".form-control").css({'display':'none'});
                           search_box_div.find(".input-group").find(".input-group-addon").css({'background':'linear-gradient(120deg,#0162a9,#016ab3)','border':'none','color':"white",'cursor':'pointer'});
                           $(".beeperNub").css({'left':'120px'});
                       }
                        return search_box_div;
                        
                }
                that.selLoadBegin=function (){
                    autolist.empty();
                }
                that.selLoadFinish=function (){
                    that.setResizelist();
                }
                that.addAutoSuggestion = function (data) {
                    autolist.append(that.getAutolist(data));
                }
                
                this.getAutolist = function (obj){
                    var button;
                        button=$("<a>").addClass("pull-right").append(config.getLoading_4());
                        button.click(function (e){
                                e.stopPropagation();
                                that.joinGrp($(this),obj.software_id);
                        });
                        button.prop('disabled', true);
                    var pic = $('<img />', { 
                            src: '../../dist/img/avatar.png',
                            class:'menu-icon direct-chat-img',
                            alt: 'User Image'
                        }).on('error', function() { $(this).attr({'src':'../../dist/img/avatar.png'}) });
                        that.loadStatus(button,obj.software_id);
                    return $("<li>").addClass("list-group-item")
                            .append($("<a>")
                                .append(pic.css({'border-radius':'7%'}))
                                .append($("<div>").addClass("menu-info")
                                    .append(button)
                                    .append($("<h3>").addClass("control-sidebar-subheading").append(obj.orgenisation_name))
                                ));
                        
                        
                }
                this.loadStatus= function (obj,software_id){
                    $.each(listeners, function(i){
                        listeners[i].selStatus(obj,software_id);
                    });
                }
                this.updStatus= function (data,obj){
                    setTimeout(function (){
                   obj.css({'padding': '3px'});     
                    },1)
                   
                   if(data=="false"){
                        obj.addClass("btn btn-primary btn-xs").append($("<i>").addClass("fa fa-plus")).append("&nbsp;Join&nbsp;&nbsp;&nbsp;")
                        obj.prop('disabled', false);                            
                    }else{
                        obj.addClass("btn btn-default btn-xs pull-right").append("Pending");
                        obj.prop('disabled', true);
                    }
                }
                
                
                this.joinGrp = function (obj,software_id){
                    try {
                            $.each(listeners, function(i){
                                listeners[i].sendRequest(obj,software_id);
                             });
                    }catch(e){}
                }
                
                this.setResizelist = function (){
                    if(config.getWidth()<460){
                        autolist.width(autolist.parent().find(".search_box_scroll").width()+23);
                        autolist.css({'margin-left':'36px'});
                    }else{
                        autolist.width(autolist.parent().find(".input-group").width()-8);
                        autolist.css({'margin-left':'0px'});
                    }
                    
                    autolist.children().width("auto");
                    autolist.children().each(function (){
                       $(this).find("a").css({'padding':'0px'})
                    })
                       
                }
                
                this.loadModel =function (software_module_id,obj){
                    $.get("../../dist/js/jquery.validate.min.js").done(function() {
                        $("body").css('cursor','wait');
                            if(config.getView("ModelVgr")==null){
                                $.getScript("../../admin/view/ModelVgr.js").done(function(){
                                config.setView("ModelVgr",new $.ModelVgr(config));
                                that.loadSoftwareDetails(software_module_id,obj);  
                            }).fail(function() {alert("Error :problem in ModelVgr.js file ");}); 
                            }else{
                                that.loadSoftwareDetails(software_module_id,obj);
                            }
                        }).fail(function() {alert("Error: jquery.validate.min.js Loading Problem");}); 
                } 
                this.loadSoftwareDetails = function(software_module_id,obj){
                        if(config.getView("SoftwareDetailsVgr")==null){
                        $.getScript("../view/SoftwareDetailsVgr.js").done(function(){
                            config.setView("SoftwareDetailsVgr",new $.SoftwareDetailsVgr(config));
                            var model = config.getView("ModelVgr").getModel("Mock Test",config.getView("SoftwareDetailsVgr").getSoftwareDetails(config.getModel("ProfileConfig").getUser_id(),software_module_id),"submit");
                            model.modal({
                                    backdrop: 'static',
                                    keyboard: true, 
                                    show: true
                            });
                            model.find(".modal-header").find(".close").remove();
                            model.find(".modal-footer").find("#cancel").remove();
                            model.find(".modal-dialog").addClass("modal-md");
                                        $("body").css('cursor','');
                                        $("hidden").empty();
                                        $("hidden").append(model);
                                        model.find(".modal-body").addClass("no-padding");
                                        model.find(".modal-footer").remove();
                                        model.modal('show');
                        }).fail(function() {alert("Error:004 problem in SoftwareDetailsVgr.js file ");}); 
                     }else{
                            var model = config.getView("ModelVgr").getModel("Mock Test",config.getView("SoftwareDetailsVgr").getSoftwareDetails(config.getModel("ProfileConfig").getUser_id(),software_module_id),"submit");
                            model.find(".modal-dialog").addClass("modal-md");
                            $("body").css('cursor','');
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.find(".modal-body").addClass("no-padding");
                            model.find(".modal-footer").remove();
                            model.modal('show');
                     }
                }
                
                
                this.getControlSidebar_Btn =function (){
                    return $("<li>").append($("<a>").addClass("active").attr({'data-toggle':'control-sidebar','title':'Open the sidebar'})
                            .append($("<i>").addClass("fa fa-th-large")).click(function (){
//                                if(config.getView("ControlSidebarVgr")==null){
                                    if(config.getView("ChatListVgr").getAside().hasClass("control-sidebar-open")){
                                        config.getView("ChatListVgr").getAside().removeClass("control-sidebar-open");
                                    }else{
                                        config.getView("ChatListVgr").getAside().addClass("control-sidebar-open");
                                    }
//                                }else{
//                                    if(config.getView("ControlSidebarVgr").getAside().hasClass("control-sidebar-open")){
//                                        config.getView("ControlSidebarVgr").getAside().removeClass("control-sidebar-open");
//                                    }else{
//                                        config.getView("ControlSidebarVgr").getAside().addClass("control-sidebar-open");
//                                    }
//                                }
                            }));
                }
                
                this.getButton = function (title,id,callback){
                  return $("<button>").addClass("btn btn-default btn-flat").append(title).attr({'id':id})
                     .click(function(){
                         callback($(this));
                      });
                }
                this.callback = function (title,id,callback){
                  config.getView("ContentWrapperProfileVgr").init()
                }
                this.logOut = function (obj){
                  temp_obj=obj;
                  config.getModel("ProfileConfig").logOut();
                }
                this.setLoadBegin =function (){
                    temp_obj.prop('disabled', false);
                    temp_obj.empty();
                    temp_obj.append(config.getLoadingData());
                }
                this.setLoadFinish =function (){
                    temp_obj.prop('disabled', true);
                    temp_obj.html("LogOut");
                }
                this.selLoadBegin_In_grp =function (obj){
                    obj.prop('disabled', true);
                    obj.html(config.getLoadingFb());
                }
                this.selLoadFinish_In_grp =function (obj){
                    obj.prop('disabled', true);
                    obj.removeClass("btn-primary");
                    obj.addClass("btn-default");
                    obj.html("Pending");
                }
                this.selLoadFail_In_grp =function (obj){
                    obj.prop('disabled', false);
                    obj.html("Retry");
                }
                this.selLoadBegin_status=function (obj){
                    obj.html(config.getLoading_4());
                }
                this.selLoadFinish_status =function (obj){
                    obj.empty();
                }
                this.selLoadFail_status =function (obj){
                    obj.addClass("btn btn-primary btn-xs");
                    obj.prop('disabled', false);
                    obj.html("Retry");
                }
                this.addListener = function(list){
                    listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	HeaderProfileVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	},
	AutoSuggestionVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selAutoSuggestion : function() { }
		}, list);
	}
});
