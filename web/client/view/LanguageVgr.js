jQuery.extend({
	LanguageVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
                this.setLanguage = function(module){
                    that.loadLanguage(module);
		}
                
                this.addLanguage =function (data,module){
                    try {
                        module.setLanguage(data);
                    }catch (e){console.log(e)}
                }                                                        
                
                this.selLoadBegin =function (){
                    //tbody.append(config.getLoadingData());
                }
                
                this.selLoadFinish=function (){
                    //tbody.empty();
                }
                
                this.selLoadFail = function() { 
                    //tbody.append("<ts>")
                }
            
                this.loadLanguage =function (module){
                    $.each(listeners, function(i){
                        listeners[i].selLanguage(module);
                    });
                } 

                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	LanguageVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                	selLanguage : function() { }
		}, list);
	}
});
