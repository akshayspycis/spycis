jQuery.extend({
	MockTestHomeVgr: function(config,per){
            var that=this;
            var listeners = new Array();
            var activity;
            var settings;
            var modelform ;
            var ul_list ;
            var ul_requested_members ;
            var pass_form ;
            var upload_img ;
            this.init = function (){
                per.setHeaderTitle("Exam Portal");
                per.setBreadCrumbName(" fa-user-secret","Exam Portel");
                per.removeBoxHeader();
                per.removeBoxbody();
                per.setBoxHeaderTitle($("<h3>").addClass("box-title").css({'line-height':':24px;'}).append("WelCome to Exam Panel"));
                per.setBoxBodyContent(that.getBody());
                that.loadMockTestHome();
            }
            this.getBody= function (){
                activity=$("<div>").addClass("tab-pane active").attr({'id':'activity'});
                settings=$("<div>").addClass("tab-pane").attr({'id':'settings'});
                return $("<div>").addClass("row")
                    .append($("<div>").addClass("col-md-12")
                        .append($("<div>").addClass("nav-tabs-custom")
                            .append($("<ul>").addClass("nav nav-tabs")
                                .append($("<li>").addClass("active")
                                    .append($("<a>").attr({'href':'#activity','data-toggle':'tab','aria-expanded':'true'}).append("Activity"))
                                )
                                .append($("<li>")
                                    .append($("<a>").attr({'href':'#settings','data-toggle':'tab','aria-expanded':'true'}).append("Settings"))
                                )
                            )
                           .append($("<div>").addClass("tab-content")
                                .append(activity)
                                .append(settings)
                            )
                       )
                    );
            }
            
            this.getSetting = function (data){
                return $("<div>").addClass("box-body box-widget widget-user")
                        .append($("<div>").addClass("widget-user-header bg-black").css({"background":"url('../../dist/img/cover_page.jpg') no-repeat center","background-size":"100%",'height':'320px'})
                            .append($("<div>").addClass("icon")
                                .append($("<div></div>").addClass("input-group-btn").css({'width':'0%'})
                                    .append($("<button></button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                        .append($("<span></span>").addClass("fa fa-camera"))
                                        .click(function (){
                                            if($(this).attr('aria-expanded')=="false" || $(this).attr('aria-expanded')==null){
                                                $(this).parent().addClass("open");
                                                $(this).attr('aria-expanded','true')
                                            }else{
                                                $(this).parent().removeClass("open");
                                                $(this).attr('aria-expanded','false')
                                            }
                                          })
                                        )
                                    .append($("<ul></ul>").addClass("dropdown-menu dropdown-menu-right")
                                            .append($("<li></li>").append($("<a>").append("Upload Photo"))).click(function (){
                                                  
                                            })
                                            .append($("<li></li>").addClass("divider"))
                                            .append($("<li></li>").append($("<a>Edit</a>")))))
                            )).append($("<h3>").addClass("profile-username text-center").append(data.orgenisation_name).css({'position': 'absolute','font-size':' 32px','margin-top': '-42px','margin-left': '277px','color': '#fff','z-index': '1000','font-weight':' bold'}))
                        .append($("<div>").addClass("col-md-4").css({'margin-top': '-181px'}).append(that.getUploadArea(180,"dragAndDropFiles","demoFiler","multiUpload"))
                           .append($("<div>").addClass("box")
                            .append($("<div>").addClass("box-body")
                                .append($("<div>").addClass("box-footer no-border").css({'margin-top':'0px'})
                                    .append($("<div>").addClass("row")
                                        .append($("<div>").addClass("col-sm-6 col-md-6 col-xs-6 border-right")
                                            .append($("<div>").addClass("description-block")
                                                .append($("<h5>").addClass("description-header").append("3,200"))
                                                .append($("<span>").addClass("description-text").append("Member"))
                                            ))
                                        .append($("<div>").addClass("col-sm-6 col-md-6 col-xs-6")
                                            .append($("<div>").addClass("description-block")
                                                .append($("<h5>").addClass("description-header").append("3,200"))
                                                .append($("<span>").addClass("description-text").append("Exam Circle"))
                                            ))
                                    )
                                .append($("<div>").addClass("row")
                                    .append(ul_list
                                        .append($("<li>").addClass("list-group-item list_profile_page list_profile_page")
                                            .append($("<b>")
                                                .append($("<span>").addClass("fa fa-users text-blue")))
                                            .append($("<a>").append("&nbsp;&nbsp;&nbsp;Public Circle").css({'cursor':'pointer'}).click(function (){
                                                that.loadMocktestPublicCircleVgr(data.software_id);
                                            }))
                                        )    
                                        .append($("<li>").addClass("list-group-item list_profile_page list_profile_page")
                                            .append($("<b>")
                                                .append($("<span>").addClass("fa fa-user-plus  text-orange ")))
                                            .append($("<a>")
                                                .append("&nbsp;&nbsp;&nbsp;Exams Circle")
                                            )
                                        )    
                                        .append($("<li>").addClass("list-group-item list_profile_page list_profile_page")
                                            .append($("<b>")
                                                .append($("<span>").addClass("fa fa-bank text-olive")))
                                            .append($("<a>")
                                                .append("&nbsp;&nbsp;&nbsp;Question Bank").css({'cursor':'pointer'}).click(function (){
                                                    that.loadMockTestDirectionVgr(data.software_id);
                                                })
                                            )
                                        )    
                                        .append($("<li>").addClass("list-group-item list_profile_page list_profile_page")
                                            .append($("<b>")
                                                .append($("<span>").addClass("fa fa-question-circle text-maroon")))
                                            .append($("<a>")
                                                .append("&nbsp;&nbsp;&nbsp;&nbsp;Direction wise Question")
                                            )
                                        )    
                                        .append($("<li>").addClass("list-group-item list_profile_page list_profile_page")
                                            .append($("<b>")
                                                .append($("<span>").addClass("fa fa-question text-purple")))
                                            .append($("<a>")
                                                .append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Without Direction Question")
                                            )
                                        )    
                                        .append($("<li>").addClass("list-group-item list_profile_page list_profile_page")
                                            .append($("<b>")
                                                .append($("<span>").addClass("fa fa-newspaper-o text-maroon")))
                                            .append($("<a>")
                                                .append("&nbsp;&nbsp;&nbsp;Test Details")
                                            )
                                        )    
                                    )
                                    )
                                    )
                                    )
                        )
                    )
                    .append($("<div>").addClass("col-lg-8 no-padding").append($('<div class="box box-widget"> <div class="box-header with-border"> <div class="user-block"> <img class="img-circle" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <span class="username"><a href="#">Jonathan Burke Jr.</a></span> <span class="description">Shared publicly - 7:30 PM Today</span> </div><div class="box-tools"> <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read"> <i class="fa fa-circle-o"></i></button> <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button> <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> </div></div><div class="box-body"> <img class="img-responsive pad" src="../../dist/images/banner1.png" alt="Photo"> <p>I took this photo this morning. What do you guys think?</p><button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> <span class="pull-right text-muted">127 likes - 3 comments</span> </div><div class="box-footer box-comments"> <div class="box-comment"> <img class="img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <div class="comment-text"> <span class="username"> Maria Gonzales <span class="text-muted pull-right">8:03 PM Today</span> </span> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout </div></div><div class="box-comment"> <img class="img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <div class="comment-text"> <span class="username"> Luna Stark <span class="text-muted pull-right">8:03 PM Today</span> </span> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </div></div></div><div class="box-footer"> <form action="#" method="post"> <img class="img-responsive img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="Alt Text"> <div class="img-push"> <input type="text" class="form-control input-sm" placeholder="Press enter to post comment"> </div></form> </div></div><div class="box box-widget"> <div class="box-header with-border"> <div class="user-block"> <img class="img-circle" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <span class="username"><a href="#">Jonathan Burke Jr.</a></span> <span class="description">Shared publicly - 7:30 PM Today</span> </div><div class="box-tools"> <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read"> <i class="fa fa-circle-o"></i></button> <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button> <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> </div></div><div class="box-body"> <p>Far far away behind the word mountains far from the countries Vokalia and Consonantia there live the blind texts. Separated they live in Bookmarksgrove right at</p><p>the coast of the Semantics a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country in which roasted parts of sentences fly into your mouth.</p><div class="attachment-block clearfix"> <img class="attachment-img" src="../../dist/images/banner1.png" alt="Attachment Image"> <div class="attachment-pushed"> <h4 class="attachment-heading"><a href="http://www.lipsum.com/">Lorem ipsum text generator</a></h4> <div class="attachment-text"> Description about the attachment can be placed here. Lorem Ipsum is simply dummy text of the printing and typesetting industry... <a href="#">more</a> </div></div></div><button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> <span class="pull-right text-muted">45 likes - 2 comments</span> </div><div class="box-footer box-comments"> <div class="box-comment"> <img class="img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <div class="comment-text"> <span class="username"> Maria Gonzales <span class="text-muted pull-right">8:03 PM Today</span> </span> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </div></div><div class="box-comment"> <img class="img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="User Image"> <div class="comment-text"> <span class="username"> Nora Havisham <span class="text-muted pull-right">8:03 PM Today</span> </span> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters as opposed to using Content here content here making it look like readable English. </div></div></div><div class="box-footer"> <form action="#" method="post"> <img class="img-responsive img-circle img-sm" src="../../dist/img/user2-160x160.jpg" alt="Alt Text"> <div class="img-push"> <input type="text" class="form-control input-sm" placeholder="Press enter to post comment"> </div></form> </div></div>')))            
            }
            
            this.loadMocktestPublicCircleVgr= function (software_id){
                 if(config.getView("MocktestPublicCircleVgr")==null){
                        $.getScript("../view/MocktestPublicCircleVgr.js").done(function(){
                            config.setView("MocktestPublicCircleVgr",new $.MocktestPublicCircleVgr(config,config.getView("ContentWrapperVgr")));
                                $.getScript("../model/MocktestPublicCircle.js").done(function() {
                                    config.setModel("MocktestPublicCircle",new $.MocktestPublicCircle(config));
                                    $.getScript("../controller/MocktestPublicCircleMgr.js").done(function() {
                                        config.setController("MocktestPublicCircleMgr",new $.MocktestPublicCircleMgr(config,config.getModel("MocktestPublicCircle"),config.getView("MocktestPublicCircleVgr")));
                                        config.getView("MocktestPublicCircleVgr").init(software_id);
                                    }).fail(function() {alert("Error :problem in TestUserListMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in TestUserListvgr.js file ");}); 
                        }).fail(function() {alert("Error:004 problem in MocktestPublicCircleVgr.js file ");}); 
                     }else{
                            config.getView("MocktestPublicCircleVgr").loadUserTestDetails();
                     }
                
            }
            
            this.loadMockTestDirectionVgr= function (software_id){
                 if(config.getView("MockTestDirectionVgr")==null){
                        $.getScript("../view/MockTestDirectionVgr.js").done(function(){
                            config.setView("MockTestDirectionVgr",new $.MockTestDirectionVgr(config,config.getView("ContentWrapperVgr")));
                                $.getScript("../model/MockTestDirection.js").done(function() {
                                    config.setModel("MockTestDirection",new $.MockTestDirection(config));
                                    $.getScript("../controller/MockTestDirectionMgr.js").done(function() {
                                        config.setController("MockTestDirectionMgr",new $.MockTestDirectionMgr(config,config.getModel("MockTestDirection"),config.getView("MockTestDirectionVgr")));
                                        config.getView("MockTestDirectionVgr").init(software_id);
                                    }).fail(function() {alert("Error :problem in TestUserListMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in TestUserListvgr.js file ");}); 
                        }).fail(function() {alert("Error:004 problem in MockTestDirectionVgr.js file ");}); 
                     }else{
                            config.getView("MockTestDirectionVgr").loadUserTestDetails();
                     }
                
            }
            
            this.getSponsored = function (){
                return $("<div>").append('<div class="box" id="stickyheader" style="position: static;"><div class="box-header no-padding"><span style="color:#999;margin-left:5px;margin-right:5px;">Sponsored</span></div><div class="box-body"><div><img class="img-responsive" src="../../dist/images/banner1.png" alt="User Image" style="margin: 5px;"><a class="users-list-name" href="#">Infopark India</a><span class="users-list-date">New Product</span></div></div><div class="box-footer clearfix"><button type="button" class="pull-left btn btn-default" id="sendEmail">Send</button></div></div>')
            }
            
            this.getGrammer = function (){
                return $("<div>").addClass("box")
                        .append($("<div>").addClass("box-header no-padding")
                            .append($("<span>").append("Grammar").css({'color':'#999','margin-left':'5px','margin-right':'5px'}))
                        .append($("<div>").addClass("box-body")
                            .append($("<ul>").addClass("nav nav-tabs pull-left ui-sortable-handle")
                                .append($("<li>").addClass("active").attr({'data-toggle':'tooltip','data-original-title':'Word of the Day'}).append($("<a>").attr({'href':'#sales-chart','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa fa-language"))))
                                .append($("<li>").attr({'data-toggle':'tooltip','data-original-title':'Thesaurus of the Day'}).append($("<a>").attr({'href':'#sales-chart','data-toggle':'tab','aria-expanded':'true'}).append($("<i>").addClass("fa fa-sort-alpha-asc"))))
                            )
                            .append($("<div>").addClass("tab-content no-padding")
                                .append($("<div>").addClass("chart tab-pane active").attr('id','sales-chart').css({'position':'relative','height':'250px'})
                                    .append($("<ul>").addClass("todo-list ui-sortable")
                                        .append($("<li>").addClass("todo-list ui-sortable")
                                            .append($("<span>").addClass("handle ui-sortable-handle")
                                                .append($("<i>").addClass("fa fa-ellipsis-v"))
                                                .append($("<i>").addClass("fa fa-ellipsis-v"))).attr({'data-toggle':'tooltip','data-original-title':'Thesaurus of the Day'})
                                            .append($("<span>").addClass("text")
                                                .append("Faustian"))
                                            .append($("<div>").addClass("tools")
                                                .append($("<i>").addClass("fa fa-star"))))
                                        .append($("<li>").addClass("todo-list ui-sortable")
                                            .append($("<span>").addClass("handle ui-sortable-handle")
                                                .append($("<i>").addClass("fa fa-ellipsis-v"))
                                                .append($("<i>").addClass("fa fa-ellipsis-v")))
                                            .append($("<span>").addClass("text")
                                                .append("laicism"))
                                            .append($("<div>").addClass("tools")
                                                .append($("<i>").addClass("fa fa-star"))))
                                        .append($("<li>").addClass("todo-list ui-sortable")
                                            .append($("<span>").addClass("handle ui-sortable-handle")
                                                .append($("<i>").addClass("fa fa-ellipsis-v"))
                                                .append($("<i>").addClass("fa fa-ellipsis-v")))
                                            .append($("<span>").addClass("text")
                                                .append("quinquennium"))
                                            .append($("<div>").addClass("tools")
                                                .append($("<i>").addClass("fa fa-star"))))
                                        .append($("<li>").addClass("todo-list ui-sortable")
                                            .append($("<span>").addClass("handle ui-sortable-handle")
                                                .append($("<i>").addClass("fa fa-ellipsis-v"))
                                                .append($("<i>").addClass("fa fa-ellipsis-v")))
                                            .append($("<span>").addClass("text")
                                                .append("toggery"))
                                            .append($("<div>").addClass("tools")
                                                .append($("<i>").addClass("fa fa-star"))))
                                        )))));
                                    
                                
            }
            
            this.getUpdateImgBox = function (){
                upload_img;
                try {
                    upload_img=config.getModel("ProfileConfig").getProfilePic("user-image").removeClass("user-image").css({'width':'230px','height':'216px'});
                }catch (e){
                    upload_img = $('<img />', { 
                            src: '../../dist/img/avatar.png'
                        }).css({'width':'230px','height':'216px'});
                }
                return $("<div>").addClass("ih-item square effect13 left_to_right").css({'overflow':'hidden'})
                        .append($("<a>")
                            .append($("<div>").addClass("img")
                                    .append(upload_img)
                             )
                            .append($("<div>").addClass("info")
                                    .append($("<h3>").append("Update Profile Picture"))
                                    .append($("<p>").append("Click or Drop Image Here"))
                                    )
                            );
            }
            
            this.getUploadArea = function (no,dragAndDropFiles,form_name,file){
                    var diva =$("<div></div>").addClass("box-body");
                    var div =$("<div></div>").addClass("uploadArea").attr({'id':dragAndDropFiles}).css({'padding':'0px','border':'none'});
                    div.append(that.getUpdateImgBox())
                    div.append($("<form></form>").attr({'name':form_name,'id':form_name,'enctype':'multipart/form-data'}).append($("<input/>").attr({'type':'file','data-maxwidth':'620','data-maxheight':'620','name':'file[]','id':file}).css({'width':'0px','height':'0px','overflow':'hidden'})));
                    diva.append(div);
                    return diva;
                }
            
            this.getRow = function (data){
                ul_list=$("<ul>").addClass("list-group list-group-unbordered");
                ul_requested_members=$("<ul>").addClass("users-list clearfix");
                  that.loadUserRequest(data.software_id);
                return $("<div>").addClass("row")
                   .append($("<div>").addClass("col-md-9 col-lg-9")
                        .append(that.getSetting(data))
                        .append($(""))
                        )
                   .append($("<div>").addClass("col-md-3")
                        .append(that.getSponsored())
                            .append($("<div>").addClass("box box-danger")
                                .append($("<div>").addClass("box-header with-border")
                                    .append($("<h3>").addClass("box-title").append("Requested Members"))
                                    .append($("<div>").addClass("box-tools pull-right")
                                        .append($("<span>").addClass("label label-danger").append("New Members"))
                                    )
                                )
                                .append($("<div>").addClass("box-body no-padding")
                                    .append(ul_requested_members)
                                 )));
                              
                 that.loadUserRequest(data.software_id)             
            }
            
            this.loadUserRequest = function (software_id){
                      if(config.getView("UserRequestVgr")==null){
                        $.getScript("../view/UserRequestVgr.js").done(function(){
                            config.setView("UserRequestVgr",new $.UserRequestVgr(config));
                                $.getScript("../model/UserRequest.js").done(function() {
                                    config.setModel("UserRequest",new $.UserRequest(config));
                                    $.getScript("../controller/UserRequestMgr.js").done(function() {
                                        config.setController("UserRequestMgr",new $.UserRequestMgr(config,config.getModel("UserRequest"),config.getView("UserRequestVgr")));
                                        config.getView("UserRequestVgr").setUserRequest(software_id,ul_requested_members);
                                    }).fail(function() {alert("Error :problem in TestUserListMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in TestUserListvgr.js file ");}); 
                        }).fail(function() {alert("Error:004 problem in UserRequestVgr.js file ");}); 
                     }else{
                            config.getView("UserRequestVgr").loadUserTestDetails();
                     }
            }
            
            
            this.getRegistrationBoxBody = function (data){
                    var row =$("<div>").addClass("box-body");
                    modelform =$("<form></form>").attr({'id':'rg_form'}).addClass("form-horizontal");
                    var dob;
                    try{
                        var d=data.dob.split('-');
                    dob=d[2]+"-"+d[1]+"-"+d[0];
                    }catch(e){
                        alert(e)
                    }
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Name"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'text','id':'user_name','placeholder':'Full name','name':'user_name','value':data.user_name}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-user form-control-feedback"))));
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Email"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'email','id':'email','placeholder':'Email','name':'email','value':data.email}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-envelope form-control-feedback")))
                                );
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Date Of Birth"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'date','id':'dob','title':'Please select your Date of Birth','name':'dob','value':dob}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-calendar form-control-feedback")))
                                );
                                
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Contact No."))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'text','id':'contact_no','placeholder':'Contact No..','name':'contact_no','maxlength':'10','value':data.contact_no}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-phone form-control-feedback"))));
                    modelform.append($("<div>").addClass("form-group has-feedback ").attr({'id':'update_div'})
                            .append($("<div>").addClass("col-sm-1"))
                            .append($("<div>").addClass("col-sm-3").append($('<label id="error"></label>')))
                            .append($("<div>").addClass("col-sm-2")
                            .append($("<button></button>").addClass("btn btn-primary btn-block btn-flat").attr({'type':'button','id':'submit','name':'submit'}).append("Update").click(function (){
                                $.each(listeners, function(i){
                                    try {
                                        listeners[i].updUserProfile(modelform);
                                    }catch(e){}
                                });
                            }))));
                            row.append(modelform);
                        $.get("../../dist/js/jquery.validate.min.js").done(function() {
                            if(config.getModel("UserProfile")==null){
                                $.getScript("../model/UserProfile.js").done(function(){
                                        try {
                                            config.setModel("UserProfile",new $.UserProfile(config));
                                            config.getModel("UserProfile").setValidation();
                                        }catch(e){}
                                }).fail(function() {alert("Error: UserProfile.js Loading Problem");}); 
                            }else{
                                config.getModel("UserProfile").setValidation();
                            }
                        }).fail(function() {alert("Error: jquery.validate.min.js Loading Problem");}); 
                            
                    pass_form =$("<form></form>").attr({'id':'rg_form'}).addClass("form-horizontal");
                    pass_form.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Old Password"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'password','id':'password','placeholder':'Old Paasword','name':'password'}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-lock form-control-feedback"))));
                    pass_form.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("New Password"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'password','id':'newpassword','placeholder':'new Paasword','name':'newpassword'}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-lock form-control-feedback")))
                                );
                    pass_form.append($("<div>").addClass("form-group has-feedback")
                                .append($("<label>").addClass("col-sm-2 control-label").append("Confirm password"))
                                .append($("<div>").addClass("col-sm-9")
                                .append($("<input/>").attr({'type':'password','id':'confirm_password','placeholder':'Confirm Paasword','name':'confirm_password'}).addClass("form-control"))
                                .append($("<span></span>").addClass("glyphicon glyphicon-lock form-control-feedback")))
                                );
                        pass_form.append($("<div>").addClass("form-group has-feedback ").attr({'id':'update_div'})
                            .append($("<div>").addClass("col-sm-1"))
                            .append($("<div>").addClass("col-sm-3").append($('<label id="error_password"></label>')))
                            .append($("<div>").addClass("col-sm-2")
                            .append($("<button></button>").addClass("btn btn-primary btn-block btn-flat").attr({'type':'button','id':'submit','name':'submit'}).append("Reset Password").click(function (){
                                $.each(listeners, function(i){
                                    try{
                                        listeners[i].updUpdPassword(pass_form);    
                                    }catch(e){}
                                });
                            }))));
                            if(config.getModel("UpdPassword")==null){
                                $.getScript("../model/UpdPassword.js").done(function(){
                                        try {
                                            config.setModel("UpdPassword",new $.UpdPassword(config));
                                            config.getModel("UpdPassword").setValidation();
                                        }catch(e){}
                                }).fail(function() {alert("Error: UserProfile.js Loading Problem");}); 
                            }else{
                                config.getModel("UserProfile").setValidation();
                            }
                    row.append($("<div>").addClass("box-group ").attr({'id':'accordion'})
                                    .append($("<div>").addClass("panel box")
                                        .append($("<div>").addClass("box-header")
                                            .append($("<h4>").addClass("box-title")
                                                .append($("<a>").addClass("collapsed").attr({'data-toggle':'collapse','data-parent':'#accordion','href':'#collapseOne','aria-expanded':'false'})
                                                    .append("Do you want to change your Password.")
                                                   )
                                            )
                                        )
                                        .append($("<div>").addClass("panel-collapse collapse").attr({'id':'collapseOne','aria-expanded':'false'}).css({'height':'0px'})
                                            .append($("<div>").addClass("box-body")
                                                .append(pass_form)
                                            )
                                        )    
                                    )
                                );
                    return $("<div>").addClass("col-md-6")
                            .append($("<div>").addClass("box box-widget")
                                .append($("<div>").addClass("box-header with-border")
                                    .append($("<div>").addClass("user-block")
                                        .append($("<h4>").append("Profile Details"))))
                                .append(row));
                }
                
            this.getUserDetails_model = function (){
                return modelform;
            }
            this.getPassword_form = function (){
                return pass_form;
            }
            this.getUl_list = function (){
                return ul_list;
            }

            this.selLoadBegin =function (){
                activity.append($("<center>").append(config.getLoadingData()));
            }
                
            this.selLoadFinish=function (){
                activity.empty();
            }

                this.selLoadFail = function() { 
                    activity.append($("<center>").append("Server Doesn't Responde."));
                }
                
                this.selRemoveFail = function() { 
                    activity.empty();
                }
                
                this.loadMockTestHome =function (){
                    $.each(listeners, function(i){
                        try {
                            listeners[i].selMockTestHome();
                            return false;
                        }catch(e){alert(e)}
                    });
                }
                this.addListener = function(list){
                            listeners.push(list);
                }
                this.addMockTestHome =function (key,data){
                   if(key!="software_basic_cofiguration") {
                        activity.append(that.getRow(data));
                   $('body').animate({
                        scrollTop: '110px'},
                    'slow');
                    that.loadModel();
                }
                }
                this.loadModel =function (){
                    if(config.getModel("MockTestHome").getValue("software_basic_cofiguration")==null){
                        $.get("../../dist/js/jquery.validate.min.js").done(function() {
                            $("body").css('cursor','wait');
                            if(config.getView("ModelVgr")==null){
                                $.getScript("../../admin/view/ModelVgr.js").done(function(){
                                    config.setView("ModelVgr",new $.ModelVgr(config));
                                    that.loadCofigurationDetails();  
                                }).fail(function() {alert("Error :problem in ModelVgr.js file ");}); 
                            }else{
                                that.loadCofigurationDetails();  
                            }
                        }).fail(function() {alert("Error: jquery.validate.min.js Loading Problem");}); 
                    }else{
                        that.loadSoftwareDetails();
                    }
                } 
                this.loadCofigurationDetails = function(){
                        if(config.getView("MockTestBasicConfigurationVgr")==null){
                        $.getScript("../view/MockTestBasicConfigurationVgr.js").done(function(){
                            config.setView("MockTestBasicConfigurationVgr",new $.MockTestBasicConfigurationVgr(config));
                            var check_config=config.getView("MockTestBasicConfigurationVgr").getMockTestBasicConfiguration();
                            alert(check_config)
                            if(check_config==null){
                                $("body").css('cursor','');
                                return false;
                            }
                            var model = config.getView("ModelVgr").getModel("Mock Test",check_config,"submit");
                            config.setModel("model",model);
                            model.modal({
                                    backdrop: 'static',
                                    keyboard: true, 
                                    show: true
                            });
                            model.find(".modal-header").find(".close").remove();
                            model.find(".modal-footer").find("#cancel").remove();
                            model.find(".modal-dialog").addClass("modal-md");
                            $("body").css('cursor','');
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.find(".modal-body").addClass("no-padding");
                            model.find(".modal-footer").remove();
                            model.modal('show');
                        }).fail(function() {alert("Error:004 problem in SoftwareDetailsVgr.js file ");}); 
                     }else{
                            var model = config.getView("ModelVgr").getModel("Mock Test",config.getView("SoftwareDetailsVgr").getSoftwareDetails(config.getModel("ProfileConfig").getUser_id(),software_module_id),"submit");
                            model.find(".modal-dialog").addClass("modal-md");
                            $("body").css('cursor','');
                            $("hidden").empty();
                            $("hidden").append(model);
                            model.find(".modal-body").addClass("no-padding");
                            model.find(".modal-footer").remove();
                            model.modal('show');
                     }
                }
                
                this.setDisableButton =function (obj){
                    obj.prop('disabled', true);
                }
                
                this.setEnableButton =function (obj){
                    obj.prop('disabled', false);
                }
                
                this.setServerError =function (){
                    model_footer.find("#error_msg").append("Server doesn't Respond.");
                }
                
                this.removeServerError =function (){
                    model_footer.find("#error_msg").empty();
                }
                
                this.updLoadBegin =function (){
                    modelform.find("#update_div").find("#submit").prop('disabled', true);
                    modelform.find("#update_div").find("#submit").empty();
                    modelform.find("#update_div").find("#submit").append(config.getLoadingData());
                }
                
                this.updLoadFail = function() { 
                    modelform.find("#update_div").find("#submit").prop('disabled', false);
                    modelform.find("#update_div").find("#submit").empty();
                    modelform.find("#update_div").find("#submit").append("Update");
                    modelform.find("#error").empty();
                    modelform.find("#error").append("Error: Server doesn't Respond.")
                    modelform.find("#error").css({'display':'block','color':'red'});    
                }
                this.updLoadFinish = function() { 
                    modelform.find("#update_div").find("#submit").prop('disabled', false);
                    modelform.find("#update_div").find("#submit").empty();
                    modelform.find("#update_div").find("#submit").append("Update");
                    modelform.find("#error").empty();
                    modelform.find("#error").append("Your Profile Successfully Updated")
                    modelform.find("#error").css({'display':'block','color':'blue'});    
                }
                this.updPassLoadBegin =function (){
                    pass_form.find("#update_div").find("#submit").prop('disabled', true);
                    pass_form.find("#update_div").find("#submit").empty();
                    pass_form.find("#update_div").find("#submit").append(config.getLoadingData());
                }
                
                this.updPassLoadFail = function() { 
                    pass_form.find("#update_div").find("#submit").prop('disabled', false);
                    pass_form.find("#update_div").find("#submit").empty();
                    pass_form.find("#update_div").find("#submit").append("Update");
                    pass_form.find("#error_password").empty();
                    pass_form.find("#error_password").append("Error: Server doesn't Respond.")
                    pass_form.find("#error_password").css({'display':'block','color':'red'});    
                }
                this.updPassLoadFinish = function() { 
                    pass_form.find("#update_div").find("#submit").prop('disabled', false);
                    pass_form.find("#update_div").find("#submit").empty();
                    pass_form.find("#update_div").find("#submit").append("Update");
                    pass_form.find("#error_password").empty();
                    pass_form.find("#error_password").append("Your Password Successfully Updated")
                    pass_form.find("#error_password").css({'display':'block','color':'blue'});    
                }
            
        },
        MockTestHomeVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selMockTestHome : function() { },
			updMockTestHome : function() { }
		}, list);
	},
        UserProfileVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			updUserProfile : function() { }
		}, list);
	},
        UpdPasswordVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			updUserProfile : function() { }
		}, list);
	}
});