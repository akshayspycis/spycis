jQuery.extend({
	RegistrationBoxVgr: function(config){
		var that = this;
		var listeners = new Array();
                var modelform;
                var model_footer;
                var img_loading_sigin;
                var loading_boolean=true;
                var captch_word;
                this.getRegistrationBox = function(title,model_form,call){
                        img_loading_sigin=$("<a>");
			var model =$("<div>").addClass("register-box-body");
                        model.append(that.getRegistrationBoxHeader("Register a new membership"));
                        model.append(that.getRegistrationBoxBody());
                        model.append($("<div>").addClass("social-auth-links text-center"));
                        model.append($("<a>").addClass("text-center").css({'cursor':'pointer'}).append("I already have a membership").append("&nbsp;").append(img_loading_sigin).click(function (){
                            if(loading_boolean){
                                that.loadLoginBox();
                            }
                        }));
                        model.append($("<br>"));
                        return model;
		}
                this.getRegistrationBoxHeader= function (header_title){
                    var model_header =$("<p>").addClass("login-box-msg").append(header_title);
                    return model_header;
                }
                this.getRegistrationBoxBody = function (model_form){
                    modelform =$("<form>").attr({'id':'rg_form'});
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<input/>").attr({'type':'text','id':'user_name','placeholder':'Full name','name':'user_name'}).addClass("form-control"))
                                .append($("<span>").addClass("glyphicon glyphicon-user form-control-feedback")));
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<input/>").attr({'type':'email','id':'email','placeholder':'Email','name':'email'}).addClass("form-control"))
                                .append($("<span>").addClass("glyphicon glyphicon-envelope form-control-feedback"))
                                );
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<input/>").attr({'type':'date','id':'dob','title':'Please select your Date of Birth','name':'dob'})
                                        
                                    .addClass("form-control"))
                                .append($("<span>").addClass("glyphicon glyphicon-calendar form-control-feedback")));
                                
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<input/>").attr({'type':'text','id':'contact_no','placeholder':'Contact No..','name':'contact_no','maxlength':'10'}).addClass("form-control"))
                                .append($("<span>").addClass("glyphicon glyphicon-phone form-control-feedback")));
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<input/>").attr({'type':'password','id':'password','placeholder':'Password','name':'password'}).addClass("form-control"))
                                .append($("<span>").addClass("glyphicon glyphicon-lock form-control-feedback")));
                    modelform.append($("<div>").addClass("form-group has-feedback")
                                .append($("<input/>").attr({'type':'password','id':'repassword','placeholder':'Retype password','name':'repassword'}).addClass("form-control"))
                                .append($("<span>").addClass("glyphicon glyphicon-log-in form-control-feedback")));
                    modelform.append(that.getCaptcha());
                    modelform.append($('<label id="error"></label>'));
                    var row = $("<div>").addClass("row")    
                    row.append($("<div>").addClass("col-xs-7").append($("<div>").addClass("checkbox").append($("<label></label>")
                            .append($("<input/>").attr({'type':'checkbox','id':'checkbox','name':'checkbox'})).append("I agree to the").append($("<a>").append("&nbsp; terms"))
                            )));
                    row.append($("<div>").addClass("col-xs-5")
                            .append($("<button>").addClass("btn btn-primary btn-block btn-flat").attr({'type':'button','id':'submit','name':'submit'}).append("Register").click(function (){
                                $.each(listeners, function(i){
                                    listeners[i].insRegister(modelform);
                                });
                            })));
                    modelform.append(row);
                    return modelform;
                }
                this.getForm = function (){
                    return modelform;
                }
                this.loadLoginBox = function (){
                    that.setloading(img_loading_sigin);
                    config.loadLoginBoxVgr(that);
                }
                this.getCaptcha =function (){
                    captch_word =$("<h2>").attr({'id':'h1_img'}).css({'color': 'white','font': 'bold 40px Palace Script MT','position': 'absolute','top': '-18px','left': '35px'});
                   return $("<div>").addClass("box box-primary").append($("<div>").addClass("box-header with-border").append($("<h3>").addClass("box-title").append("Security Check")))
                        .append($("<div>").addClass("box-body").append(
                    $("<div>").addClass("col-xs-5").css({'position':'relative','cursor': 'pointer'})
                        .append($('<img src="../../dist/img/captcha.jpg" style="margin-left: -21px;" height="60px" width="110px">'))
                        .append(captch_word)
                        .click(function (){
                            config.getModel("Registration").setCaptcha($("#h1_img").parent());
                        }))
                    .append($("<div>").addClass("col-xs-7")
                        .append($("<label>").append("Type the sum."))
                        .append($("<div>").addClass("form-group has-feedback")
                            .append($("<input/>").attr({'type':'text','id':'captcha','placeholder':'Sum','name':'captcha'}).addClass("form-control"))
                            .append($("<span>").addClass("glyphicon glyphicon-plus-sign form-control-feedback"))
                     )));
              
                }    
                this.setloading =function (img){
                    loading_boolean=false;
                    img.append(config.getLoadingData());
                }
                this.removeLoading =function (){
                    loading_boolean=true;
                }
                
                this.setDisableButton =function (obj){
                    obj.prop('disabled', true);
                }
                
                this.setEnableButton =function (obj){
                    obj.prop('disabled', false);
                }
                
                this.setServerError =function (){
                    model_footer.find("#error_msg").append("Server doesn't Respond.");
                }
                
                this.removeServerError =function (){
                    model_footer.find("#error_msg").empty();
                }
                
                this.insLoadBegin =function (){
                    modelform.find(".row").find("#submit").prop('disabled', true);
                    modelform.find(".row").find("#submit").empty();
                    modelform.find(".row").find("#submit").append(config.getLoadingData());
                }
                
                this.insLoadFail = function() { 
                    modelform.find(".row").find("#submit").prop('disabled', false);
                    modelform.find(".row").find("#submit").empty();
                    modelform.find(".row").find("#submit").append("Register");
                    modelform.find("#error").empty();
                    modelform.find("#error").append("Error: Server doesn't Respond.")
                    modelform.find("#error").css({'display':'block','color':'red'});    
                }
                
                this.addListener = function(list){
                            listeners.push(list);
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	RegistrationVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insRegister : function() { }
		}, list);
	}
});
