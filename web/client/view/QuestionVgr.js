jQuery.extend({
	QuestionVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
		var that = this;
                
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                this.setQuestion = function(){
                        that.loadQuestion();
                        
		}
                
                
                
                this.addQuestion =function (data,key){
                   config.getView("ControlSidebarVgr").setQuestionButton(data,key);
                }                                                        
                this.selLoadBegin =function (){
                    //tbody.append(config.getLoadingData());
                }
                
                this.selLoadFinish=function (){
                    //tbody.empty();
                }
                
                this.selLoadFail = function() { 
                    //tbody.append("<ts>")
                }
            
                this.loadQuestion =function (){
                    $.each(listeners, function(i){
                        listeners[i].selQuestion();
                    });
                } 

                this.addListener = function(list){
                            listeners.push(list);
                }
                
        },
	
	/**
	 * let people create listeners easily
	 */
	QuestionVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
                	selQuestion : function() { }
		}, list);
	}
});
