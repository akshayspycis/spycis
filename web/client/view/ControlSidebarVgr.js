jQuery.extend({
	ControlSidebarVgr: function(config){
		/**
		 * keep a reference to ourselves
		 */
                var overlayaa;
		var that = this;
                var no=false;
                var control_sidebar;
                var button_panel;
                var ques_no={};
                var aa=0;
                var aside;
                this.getAside=function (){
                    return aside;
                }
		/**
		 * who is listening to us?
		 */
		var listeners = new Array();
                
            this.getControlSidebar= function(){
                aa=0;
                overlayaa =$("<div>").addClass("overlayaa").css({'display':'none'});
                var high=config.getHeight();
                var palter;
                if(config.getWidth()>770 ){
                    if(high>549){
                        high=high-350;
                        palter=$("<div>").css({'width':'220px','height':high+'px','overflow':'auto','position':'relative','left':'-10px'})
                                                        .append(that.getButtonPanel());
                        aside=$("<aside></aside>").addClass("control-sidebar control-sidebar-dark control-sidebar-open text-black").css({'position':'fixed'});
                        aside.append($("<div>").addClass("tab-content")
                                        .append($("<div>").addClass("tab-pane active").attr({'id':'control-sidebar-home-tab'})
                                                .append($("<h3></h3>").addClass("control-sidebar-heading no-padding").append(config.getBestLogo()))
                                                .append($("<h3></h3>").addClass("control-sidebar-heading no-padding" ).append("Question Palette"))
                                                .append(palter)
                                                )
                                        .append($("<h3></h3>").addClass("control-sidebar-heading no-padding").append("Legend"))
                                );
                        aside.append($("<div>").addClass("btn-group").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'success'})));
                        aside.append("Answered");
                        aside.append($("<div>").addClass("btn-group").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'notanswer'})));
                        aside.append("Not Answered");
                        aside.append($("<div>").addClass("btn-group").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'review'})));
                        aside.append("Marked &nbsp; &nbsp;&nbsp; ");
                        aside.append($("<div>").addClass("btn-group").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'defaultb'})));
                        aside.append("Not Visited");
                    }else{
                        high=high-270;
                        palter=$("<div>").css({'width':'220px','height':high+'px','overflow':'auto','position':'relative','left':'-10px'})
                                                        .append(that.getButtonPanel());
                        aside=$("<aside></aside>").addClass("control-sidebar control-sidebar-dark control-sidebar-open text-black").css({'position':'fixed'});
                        aside.append($("<div>").addClass("tab-content").css({'margin-top':'-20px'})
                                        .append($("<div>").addClass("tab-pane active").attr({'id':'control-sidebar-home-tab'})
                                                .append($("<h3></h3>").addClass("control-sidebar-heading no-padding").append(config.getBestLogo()))
                                                .append($("<h3></h3>").addClass("control-sidebar-heading no-padding").css({'margin-top': '3px'}).append("Question Palette"))
                                                .append(palter)
                                                )
                                        .append($("<h3></h3>").addClass("control-sidebar-heading no-padding").append("Legend"))
                                );
                        aside.append($("<div>").attr({'data-toggle':'tooltip','title':'Answered'}).addClass("btn-group col-xs-3").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'success'})));
                        aside.append($("<div>").attr({'data-toggle':'tooltip','title':'Not Answered'}).addClass("btn-group col-xs-3").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'notanswer'})));
                        aside.append($("<div>").attr({'data-toggle':'tooltip','title':'Marked '}).addClass("btn-group col-xs-3").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'review'})));
                        aside.append($("<div>").attr({'data-toggle':'tooltip','title':'Not Visited'}).addClass("btn-group col-xs-3").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'defaultb'})));
                    }  
                }else{
                    $(".logo").hide();
                        high=high-220;
                        palter=$("<div>").css({'width':'220px','height':high+'px','overflow':'auto','position':'relative','left':'-10px'})
                                                        .append(that.getButtonPanel());
                        aside=$("<aside></aside>").addClass("control-sidebar control-sidebar-dark control-sidebar-open text-black").css({'position':'fixed'});
                        aside.append($("<div>").addClass("tab-content").css({'margin-top': '-50px'})
                                        .append($("<div>").addClass("tab-pane active").attr({'id':'control-sidebar-home-tab'})
                                                .append($("<h3></h3>").addClass("control-sidebar-heading no-padding").css({'margin-top': '3px'}).append("Question Palette"))
                                                .append(palter)
                                                )
                                        .append($("<h3></h3>").addClass("control-sidebar-heading no-padding").append("Legend"))
                                );
                        aside.append($("<div>").attr({'data-toggle':'tooltip','title':'Answered'}).addClass("btn-group col-xs-3").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'success'})));
                        aside.append($("<div>").attr({'data-toggle':'tooltip','title':'Not Answered'}).addClass("btn-group col-xs-3").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'notanswer'})));
                        aside.append($("<div>").attr({'data-toggle':'tooltip','title':'Marked '}).addClass("btn-group col-xs-3").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'review'})));
                        aside.append($("<div>").attr({'data-toggle':'tooltip','title':'Not Visited'}).addClass("btn-group col-xs-3").append($("<button>").addClass("btn_isn").attr({'type':'button','id':'defaultb'})));
                        setTimeout(function (){
                                $(window).trigger('resize');
                                if(aside.hasClass("control-sidebar-open")){
                                       aside.removeClass("control-sidebar-open");
                                }
                        },2000);
                        $(".content-wrapper").css({'padding-top':'50px'});
                }
                        aside.append(that.getBoxFooter());
                        config.getView("MainVgr").getListener().loadQuestionVgr();
                        return aside;
		}

            this.getButtonPanel =function (){
                        button_panel =$("<div>").addClass("text-center").attr({'id':'button_panel'});
                        return button_panel;
            }
                
            this.getBoxFooter=function (){
                var a=$("<div>").addClass("box-header").css({'position':'initial'})
                            .append($("<div>").addClass("text-center").css({'background-color':'#f0f0f0'})
                                        .append($("<a>").addClass("btn btn-default col-md-12 col-sm-12 col-lg-12 col-xs-12").attr({'id':'user'}).append("Submit").click(function (){
                                            config.getModel("Question").insTest("ok");
                                        }))
                            );
                        return a;
            }
                
                this.setQuestionButton =function (data,key){
                        button_panel.append(that.getButton(key));
                }
                this.setQuestionButton_loading_begin =function (data,key){
                        button_panel.append($("<center>").append(config.getLoadingData()));
                }
                this.setQuestionButton_loading_finish =function (data,key){
                        button_panel.empty();
                }
                this.setTestSubmit_begin =function (){
                        overlayaa.append($("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin fa-3x")).append($("<h4>").append("Submission..").css({'margin-left':'-25px'})).css({'top':'50%','left':'50%','position': 'absolute'}));
                        $("body").append(overlayaa);
                        overlayaa.toggle();
                }
                this.setTestSubmit_finish =function (){
                        overlayaa.remove();
                }
                this.setTestSubmit_fail =function (){
                        overlayaa.remove();
                }
                
                this.getButton =function (key){
                   aa++;
                   ques_no[aa]=key;
                   config.getView("ContentWrapperTestVgr").setTeXMML();
                   return $("<div>").addClass("btn-group").attr({'id':key}).append($("<button>").addClass("btn").attr({'type':'button','id':'defaultb','aria-expanded':'true','vav':aa}).append(aa).click(function (){
                       config.setQuestion_id(key);
                       var obj=config.getModel("Question").loadQuestion(key,null);
                       config.getView("ContentWrapperTestVgr").setDirection(obj.direction_name);
                       config.getView("ContentWrapperTestVgr").setQuestion(obj.question_name);
                       config.getView("ContentWrapperTestVgr").setOption(obj.option_name);
                       config.getView("ContentWrapperTestVgr").setLanguage_DropDown();
                       config.getModel("Question").setAns();
                       config.getView("ContentWrapperTestVgr").setQuestionNO($(this).attr('vav'));
                       config.getView("ContentWrapperTestVgr").setSectionTitle(config.getModel("Question").getSectionName(key));
                       config.getView("ContentWrapperTestVgr").setTeXMML();
                       if(config.getModel("Question").getQuestion_Submit_Responce(key)){
                           config.getModel("Question").clearAnsweredButton("");
                       }
                  }));
                }
                
                this.setAnsweredButton = function (question_id,a){
                    that.removeSpan(question_id);
                    $("#"+question_id).find(".btn").attr({'id':'success'});
                    if(a!=""){
                        that.setNextQus(question_id);
                    }
                }
                
                this.setNotAnsweredButton = function (question_id,a){
                    that.removeSpan(question_id);
                    $("#"+question_id).find(".btn").attr({'id':'notanswer'});
                    if(a!=""){
                        that.setNextQus(question_id);
                    }
                }
                this.setMarkedAnsweredButton = function (question_id,a){
                    that.removeSpan(question_id);
                    $("#"+question_id).find(".btn").attr({'id':'review'});
                    $("#"+question_id).find(".btn").append($("<span></span>").addClass("cart-count default-bg").append($("<i>").addClass("fa  fa-check")))
                    if(a!=""){
                        that.setNextQus(question_id);
                    }
                }
                this.setNotMarkedAnsweredButton=function (question_id,a){
                    that.removeSpan(question_id);
                    $("#"+question_id).find(".btn").attr({'id':'review'});
                    if(a!=""){
                        that.setNextQus(question_id);
                    }
                }
                this.setNextQus = function (question_id){
                    var a;
                    $.each(ques_no, function (i) {
                        if(question_id==ques_no[i]){
                         a=i;   
                        }
                    });
                    if(a==Object.keys(ques_no).length){
                        $("#"+ques_no[1]).find(".btn").trigger("click");
                    }else{
                        $("#"+ques_no[parseInt(a)+1]).find(".btn").trigger("click");
                    }
                }
                this.setQusByList = function (question_id){
                    $("#"+question_id).find(".btn").trigger("click");
                }
                this.removeSpan=function (question_id){
                    $("#"+question_id).find(".btn").find("span").remove();
                }
                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	ControlSidebarListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});