jQuery.extend({
	ChatListVgr: function(config){
                var overlayaa;
		var that = this;
                var no=false;
                var control_sidebar;
                var user_list;
                //--------------for chat box -----------------
                var dropup_list=null;
                var temp_last_id="";//for store last box id
                var order=0;//for count
//for counting space of window--------------------------------------------------------------
                var a_value=0;
                var list_value=0;
                var list_obj;
                var aside;
//------------------------------------------------------------------------------------------
                
		var listeners = new Array();
                this.getAside=function (){
                    return aside;
                }
            this.getChatList= function(obj){
                list_obj={};
                order=0;
                var high=config.getHeight();
                var palter;
                user_list=$("<ul>").addClass("control-sidebar-menu");
                if(config.getWidth()>770){
                    //where 50 px is menu bar list
                    //where 230 px is control side bar list
                    //where 10 px is gape b/w control side bar and first creta chat box
                    a_value=config.getWidth()-50-230-5;
                    list_value=230;
                }else{
                    //where 10 px is gape b/w control side bar and first creta chat box
                    a_value=config.getWidth()-10;
                }
                var postion_chat=''
                if(config.getWidth()>991){
                    high=high-215;
                    palter=$("<div>").css({'width':'220px','height':high+'px','overflow':'auto','position':'relative','left':'-10px'}).append(user_list);
                    postion_chat='fixed';
                }else{
                    palter=$("<div>").css({'width':'220px','position':'relative','left':'-10px'}).append(user_list);
                }
                aside=$("<aside></aside>").addClass("control-sidebar control-sidebar-dark control-sidebar-open text-black").css({'position':postion_chat}).attr({'id':'chatlist'});
                        aside.append($("<div>").addClass("tab-content")
                                        .append($("<div>").addClass("tab-pane active").attr({'id':'control-sidebar-home-tab'})
                                                .append($("<h3></h3>").addClass("control-sidebar-heading").append(config.getBestLogo()))
                                                .append($("<h3></h3>").addClass("control-sidebar-heading").append("Active User"))
                                                .append(palter)
                                                )
                                );
                        $.each(JSON.parse(obj),function (key,value){
                            if(key!=config.getModel("ProfileConfig").getUser_id()){
                                var user_list_panel=that.getUser(key,value);
                                user_list.append(user_list_panel)
                                value["user_list_panel"]=user_list_panel;
                            }
                            config.getModel("ChatList").loadResponse(key,value);
                        });
                        return aside;
		}
                
                this.receiveNode=function (key,value){
                    var user_list_panel=that.getUser(key,value);
                    user_list.append(user_list_panel)
                    value["user_list_panel"]=user_list_panel;
                }
                this.autoClickChatBox = function (id,obj){
                    config.getController("ProfileMgr").loadChatPanelVgr(obj);
                }

            
            this.getBoxFooter=function (){
                    var a=$("<div>").addClass("box-header")
                            .append($("<div>").addClass("text-center").css({'background-color':'#f0f0f0'})
                                        .append($("<a>").addClass("btn btn-default col-md-12").attr({'id':'user'}).append("Submit").click(function (){
                                            config.getModel("Question").insTest("ok");
                                        }))
                            );
                        return a;
            }
                
                this.setQuestionUser =function (data,key){
                        user_list.append(that.getUser(key));
                }
                this.setQuestionUser_loading_begin =function (data,key){
                        user_list.append($("<center>").append(config.getLoadingData()));
                }
                this.setQuestionUser_loading_finish =function (data,key){
                        user_list.empty();
                }
                this.setTestSubmit_begin =function (){
                        overlayaa.append($("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin fa-3x")).append($("<h4>").append("Submission..").css({'margin-left':'-25px'})).css({'top':'50%','left':'50%','position': 'absolute'}));
                        $("body").append(overlayaa);
                        overlayaa.toggle();
                }
                this.setTestSubmit_finish =function (){
                        overlayaa.remove();
                }
                this.setTestSubmit_fail =function (){
                        overlayaa.remove();
                }
                
                this.getUser =function (key,value){
                   return $("<li>").attr({'id':key}).css({'border-bottom':'1px solid #d2d6de','cursor':'pointer'})
                      .append($("<a>").css({'padding-bottom':'5px'})
                        .append($('<img />', { 
                            class:'menu-icon direct-chat-img',
                            src: value.pic,
                            alt: 'User Image'
                        }).on('error', function() { $(this).attr({'src':'../../dist/img/avatar.png'}) }))
                       .append($("<div>").addClass("menu-info")
                            .append($("<i>").addClass("fa fa-circle text-blue pull-right").css({'margin-right':'15px','margin-top':'5px'}))
                            .append($("<h3>").addClass("control-sidebar-subheading").append(value.user_name))
                            .append($("<p>").css({'font-size':'14px'}).append(value.keyid))
                        ))
                        .click(function (){
                            config.getController("ProfileMgr").loadChatPanelVgr(value);
                        });
                }
                
                this.cretaChatBox = function (obj){
                    $("body").append(obj);
                    config.getModel("ChatList").setChatBoxObject(obj.attr("id"),"slim_scroll_bar",config.getModel("ChatList").getChatBoxObject(obj.attr("id"),"direct_chat_messages").slimScroll({height: '244px',start: 'bottom',alwaysVisible: true}));
                    $('.slimScrollBar').css({'background': 'black'});
                    var chat_box_width=parseInt(obj.css('width').replace("px",""));
                    list_obj[order]=obj;
                    order++;
                    if(a_value>chat_box_width){
                        obj.css({'right':list_value+'px'});    
                        list_value=list_value+chat_box_width;
                        a_value=a_value-chat_box_width;
                        temp_last_id=obj.attr("id");
                    }else{
                        obj.remove();
                        var id=obj.attr("id");
                        var user_details=config.getModel("ChatList").getUserDetails(id);
                        if(dropup_list==null){
                            dropup_list=that.getDropdown(id,user_details,obj).css({'right':list_value+30+'px'});
                            $("body").append(dropup_list);
                        }else{
                            dropup_list.find("ul").append($("<li>").addClass("divider"));
                            dropup_list.find("ul").append($("<li>").attr({'id':'list'+id}).css({'cursor':'pointer'}).append($("<a>").attr({'id':id}).append(user_details.user_name) 
                                                .append($('<img />', { 
                                                    class:'direct-chat-img',
                                                    css:{'width':'25px','height':'25px','left': '-15px','position': 'relative'},
                                                    src: user_details.pic,
                                                    alt: 'User Image'
                                                    }).on('error', function() { $(this).attr({'src':'../../dist/img/avatar.png'}) }))
                                                .click(function (){
                                                        that.replaceChatBox(id,user_details,obj,$(this).parent());
                                                    })  
                                                ));
                        }
                    }
                    
                }
                
                this.replaceChatBox = function (id,user_details,obj,drop_up_list_obj){
                        var lastobj=$("body").children("#"+temp_last_id);
                        obj.css({'right':lastobj.css("right")});    
                        drop_up_list_obj.empty();
                        drop_up_list_obj.append($("<a>").attr({'id':temp_last_id}));
                        var user_details1=config.getModel("ChatList").getUserDetails(temp_last_id)
                        var id1=temp_last_id;
                        drop_up_list_obj.find("a").append(lastobj.find(".box-header").find(".box-title").text()) 
                            .append($('<img />', { 
                                            class:'direct-chat-img',
                                            css:{'width':'25px','height':'25px','left': '-15px','position': 'relative'},
                                            src: lastobj.find(".box-header").find(".direct-chat-img").attr('src'),
                                            alt: 'User Image'
                                        }).on('error', function() { $(this).attr({'src':'../../dist/img/avatar.png'}) }))
                            .click(function (){
                                        that.replaceChatBox(id1,user_details1,lastobj,drop_up_list_obj);
                            });
                        obj.find(".box-header").find("#remove").click(function (){
                            config.getModel("ChatList").removeChatBoxObject(id,"direct_chat_messages");
                            config.getModel("ChatList").removeChatBoxObject(id,"direct_chat_text_box");
                            config.getView("ChatListVgr").deleteChatBox(id)
                        });
                        config.getModel("ChatList").setChatBoxObject(id,"slim_scroll_bar",config.getModel("ChatList").getChatBoxObject(id,"direct_chat_messages").slimScroll({height: '244px',start: 'bottom',alwaysVisible: true}));
                        config.getModel("ChatList").getChatBoxObject(id,"direct_chat_text_box").find("input")
                            .keyup(function(e){
                                   var code = e.keyCode ? e.keyCode : e.which;
                                   if (code == 13 && $(this).val()!="") {
                                     config.getView("ChatPanelVgr").setDirectChatMsgRight(config.getModel("ChatList").getUserDetails(id),$(this).val());  
                                     $(this).val("");
//                                     $.each(listeners, function(i){
//                                        listeners[i].insChatPanel($(this).val());
//                                     });
                                   }
                                });
                        $('.slimScrollBar').css({'background': 'black'});
                        lastobj.css('right','');
                        lastobj.remove();
                        $("body").append(obj);
                        obj.find(".box-header").find(".direct-chat-img").attr('src',user_details.pic).on('error', function() { $(this).attr({'src':'../../dist/img/avatar.png'}) });
                        temp_last_id=id;
                }
                
                this.deleteChatBox = function (user_id){
                    var obj=$("body").children("#"+user_id);
                    var temp_width;
                    var temp_key;
                    var temp_cdc=true;
                    var temp_d=false;
                    var temp_right;
                    $.each(list_obj,function (key,value){   
                        temp_width=parseInt(value.css('width').replace("px",""));
                        if(value.css('right')!=""){
                            if(value.attr('id')==user_id){
                                temp_right=value.css('right');
                                if(dropup_list==null){
                                    list_value=list_value-temp_width;
                                    a_value=a_value+temp_width;
                                }
                                value.remove();
                                temp_d=true;
                            }else{
                                if(temp_d){
                                    var a=value.css('right');
                                    value.css('right',temp_right);
                                    list_obj[temp_key]=value;
                                    temp_right=a;
                                }
                            }
                        }else{
                            if(temp_cdc){
                                value.css('right',temp_right).find(".box-header").find("#remove").click(function (){
                                    config.getModel("ChatList").removeChatBoxObject(value.attr('id'),"direct_chat_messages");
                                    config.getModel("ChatList").removeChatBoxObject(value.attr('id'),"direct_chat_text_box");
                                    config.getView("ChatListVgr").deleteChatBox(value.attr('id'))
                                });
                                config.getModel("ChatList").setChatBoxObject(value.attr('id'),"slim_scroll_bar",config.getModel("ChatList").getChatBoxObject(value.attr('id'),"direct_chat_messages").slimScroll({height: '244px',start: 'bottom',alwaysVisible: true}));
                                config.getModel("ChatList").getChatBoxObject(value.attr('id'),"direct_chat_text_box").find("input")
                                    .keyup(function(e){
                                           var code = e.keyCode ? e.keyCode : e.which;
                                           if (code == 13 && $(this).val()!="") {
                                             config.getView("ChatPanelVgr").setDirectChatMsgRight(config.getModel("ChatList").getUserDetails(id),$(this).val());  
                                             $(this).val("");
                                           }
                                        });
                                $('.slimScrollBar').css({'background': 'black'});
                                temp_last_id=value.attr('id');
                                $("body").append(value);
                                temp_right=a;
                                dropup_list.find("ul").find("li:nth-child(1)").remove();
                                if(dropup_list.find("ul").find("li:nth-child(1)").hasClass("divider")){
                                     dropup_list.find("ul").find("li:nth-child(1)").remove();
                                }
                               temp_cdc=false;
                            }
                            list_obj[temp_key]=value;
                        }
                        temp_key=key;
                    });
                    if(dropup_list!=null&&temp_cdc){
                        dropup_list.remove();
                        dropup_list=null;
                        list_value=list_value-temp_width;
                        a_value=a_value+temp_width;
                    }
                    delete list_obj[Object.keys(list_obj).length-1];
                    if(Object.keys(list_obj).length==0){
                        list_obj={}
                        order=0;
                    }
                }
                
                this.getDropdown = function (id,user_details,obj){
                   return $("<div>").addClass("input-group-btn").css({'position':'fixed','bottom':'50px','float':'right'})
                            .append($("<button>").addClass("btn btn-default dropdown-toggle").attr({'type':'button','data-toggle':'dropdown'})
                                .click(function (){
                                    if($(this).parent().hasClass("open")){
                                        $(this).parent().removeClass("open")
                                    }else{
                                        $(this).parent().addClass("open")
                                    }
                                 })
                                .append($("<span>").addClass("fa fa-caret-down")))
                            .append($("<ul>").addClass("dropdown-menu").css({'right': '','left': '','top': '-100px'})
                                    .append($("<li>").attr({'id':'list'+id}).css({'cursor':'pointer'}).append($("<a>").attr({'id':id}).append(user_details.user_name) 
                                        .append($('<img />', { 
                                            class:'direct-chat-img',
                                            css:{'width':'25px','height':'25px','left': '-15px','top':'-4px','position': 'relative'},
                                            src: user_details.pic,
                                            alt: 'User Image'
                                            }).on('error', function() { $(this).attr({'src':'../../dist/img/avatar.png'}) }))
                                        .click(function (){
                                            that.replaceChatBox(id,user_details,obj,$(this).parent());
                                        })
                            )));
                }
                
                
                this.setAnsweredUser = function (question_id,a){
                    that.removeSpan(question_id);
                    $("#"+question_id).find(".btn").attr({'id':'success'});
                    if(a!=""){
                        that.setNextQus(question_id);
                    }
                }
                
                this.setNotAnsweredUser = function (question_id,a){
                    that.removeSpan(question_id);
                    $("#"+question_id).find(".btn").attr({'id':'notanswer'});
                    if(a!=""){
                        that.setNextQus(question_id);
                    }
                }
                this.setMarkedAnsweredUser = function (question_id,a){
                    that.removeSpan(question_id);
                    $("#"+question_id).find(".btn").attr({'id':'review'});
                    $("#"+question_id).find(".btn").append($("<span></span>").addClass("cart-count default-bg").append($("<i>").addClass("fa  fa-check")))
                    if(a!=""){
                        that.setNextQus(question_id);
                    }
                }
                this.setNotMarkedAnsweredUser=function (question_id,a){
                    that.removeSpan(question_id);
                    $("#"+question_id).find(".btn").attr({'id':'review'});
                    if(a!=""){
                        that.setNextQus(question_id);
                    }
                }
                this.setNextQus = function (question_id){
                    var a;
                    $.each(ques_no, function (i) {
                        if(question_id==ques_no[i]){
                         a=i;   
                        }
                    });
                    if(a==Object.keys(ques_no).length){
                        $("#"+ques_no[1]).find(".btn").trigger("click");
                    }else{
                        $("#"+ques_no[parseInt(a)+1]).find(".btn").trigger("click");
                    }
                }
                this.setQusByList = function (question_id){
                    $("#"+question_id).find(".btn").trigger("click");
                }
                this.removeSpan=function (question_id){
                    $("#"+question_id).find(".btn").find("span").remove();
                }
                this.addListener = function(list){
                            listeners.push(list);
                }
                
                
        },
	
	/**
	 * let people create listeners easily
	 */
	ChatListListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});