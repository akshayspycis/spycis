jQuery.extend({
    ContentWrapperTestVgr: function (config) {
        var that = this;
        var no = false;
        var section;
        var nav;
        var box_header;
        var qus;
        var option;
        var dir;
        var lang_dorp_down;
        var select_option;
        var top_header_section_name;
        var listeners = new Array();
        var counter = 0;
        var question_no;
        var section_title;
        this.setContentWrapperTest = function () {
            question_no = $("<b>");
            section_title = $("<b>");
            top_header_section_name = $("<b>").css({'font-size': '14px', 'color': '#360500'});
            config.getView("ContentWrapperVgr").setHeaderTitle($("<p>").append($("<i>").addClass("fa fa-check-square").css({'font-size': '14px', 'color': '#360500'})).append("&nbsp;You are viewing&nbsp;").append(section_title).append("&nbsp;"));
            config.getView("ContentWrapperVgr").setBreadCrumbName("fa fa-newspaper-o", "Assiment Test");
            ///---------------for set box header in 3 step----------------------
            config.getView("ContentWrapperVgr").removeBoxHeader();
            config.getView("ContentWrapperVgr").removeBoxbody();
            config.getView("ContentWrapperVgr").setBoxHeader($("<h3>").addClass("box-title").append("Question&nbsp;").append(question_no));
            config.getView("ContentWrapperVgr").setBoxHeader($("<div>").addClass("box-tools pull-right").append(that.getLanguage_DropDown()));
            //-------------------------------------------------------------------
            config.getView("ContentWrapperVgr").setBoxBodyContent(that.getBoxBody());
            config.getView("MainVgr").getListener().loadLanguageVgr(that);
            //----------for settime out break load data in select option then call setLang drop funtion for set default lang----------
            setTimeout(function () {
                that.setLanguage_DropDown();
            }, 100);
        }

        this.setHeader = function (section_name) {
            top_header_section_name.empty();
            top_header_section_name.append("&nbsp;" + section_name);
        }
        this.setQuestionNo = function (no) {
            question_no.empty()
            question_no.append(no)
        }
        this.getLanguage_DropDown = function () {
            lang_dorp_down = $("<div>").addClass("form-group");
            lang_dorp_down.append($("<select>Category :</select>").addClass("lang form-control").attr({'tabindex': '-1', 'aria-hidden': 'true'})).change(function () {
                var id = lang_dorp_down.find(".lang option:first").val();
                $(".lang option:selected").each(function () {
                    id = $(this).val();
                });
                var obj = config.getModel("Question").loadQuestion(null, id);
                config.getView("ContentWrapperTestVgr").setDirection(obj.direction_name);
                config.getView("ContentWrapperTestVgr").setQuestion(obj.question_name);
                config.getView("ContentWrapperTestVgr").setOption(obj.option_name);
                config.getModel("Question").setAns();
            });
            return lang_dorp_down;
        }
        this.setLanguage_DropDown = function () {
            lang_dorp_down.find(".lang").val(config.getLanguage_id());
        }


        this.getBoxSection = function () {
            var b = $("<div>").addClass("box");
            b.append(that.getNavbar());
            return b;
        }

        this.getNavbar = function () {
            nav = $("<nav>").addClass("navbar navbar-default").attr({'role': 'navigation'});
            nav.append($("<div>").addClass("navbar-header").append($("<a>").addClass("navbar-brand").append("Sections")))
            nav.append($("<div>").addClass("navbar-collapse collapse").css({'height': '1.11111px;'}).attr({'id': 'bs-example-navbar-collapse-1', 'aria-expanded': 'false'}))
            return nav;
        }

        this.setSection = function (section) {
            var ul = $("<ul>").addClass("nav navbar-nav")
                    .append($("<li>").addClass("dropdown")
                            .append($("<a>").addClass("dropdown-toggle").attr({'href': '#', 'data-toggle': 'dropdown'})
                                    .append(section.section_name)
                                    .append($("<b>").addClass("caret"))
                                    )
                            .append($("<ul>").addClass("dropdown-menu list-group").css({'display': 'none'})
                                    .append($("<li>").addClass("list-group-item")
                                            .append($("<span>").addClass("badge").attr({'id': 'success'}).append("45"))
                                            .append("Answered")
                                            )
                                    .append($("<li>").addClass("list-group-item")
                                            .append($("<span>").addClass("badge").attr({'id': 'notanswer'}).append("30"))
                                            .append("Not Answered")
                                            )
                                    .append($("<li>").addClass("list-group-item")
                                            .append($("<span>").addClass("badge").attr({'id': 'review'}).append("45"))
                                            .append(" Mark for Review ")
                                            )
                                    .append($("<li>").addClass("list-group-item")
                                            .append($("<span>").addClass("badge").append("45"))
                                            .append("Answered Not Visited")
                                            )
                                    )
                            );

            if (counter < 6) {
                nav.find("div:nth-child(2)").append(ul);
                counter++;
                if (counter == 6) {
                    nav.find("div:nth-child(2)").append($("<ul>").addClass("nav navbar-nav")
                            .append($("<li>").addClass("dropdown")
                                    .append($("<a>").addClass("dropdown-toggle").attr({'href': '#', 'data-toggle': 'dropdown'})
                                            .append("More")
                                            .append($("<b>").addClass("caret"))
                                            )
                                    .append($("<ul>").addClass("dropdown-menu list-group").css({'display': 'none'}))
                                    ));
                }
            } else {
                nav.find("div:nth-child(2)").find("ul>.dropdown-menu").append($("<li>").addClass("list-group-item").append(ul))
            }
        }

        this.setLanguage = function (language) {
            lang_dorp_down.find(".lang").append($("<option></option>").attr({'value': language.language_id}).append(language.language_name));
        }
         var box_header;
        this.getBoxBody = function () {
            box_header = $("<div>").addClass("");
            box_header.append(that.getDirection());
            if (config.getWidth() < 991) {
                setTimeout(function () {
                    box_header.parent().addClass("no-padding");
                }, 100);
                box_header.append($("<div>").addClass("col-md-6 no-padding").append(that.getQuestion()).append(that.getOption()));
            } else {
                box_header.append($("<div>").addClass("col-md-6").append(that.getQuestion()).append(that.getOption()));
            }
            return box_header;
        }

        this.getDirection = function () {
            var high = config.getHeight();
            if (config.getWidth() > 1100) {
                high = high - 310 - 25;
                dir = $("<div>").addClass("col-md-6 question").css({'overflow-y': 'scroll', 'height': high + "px"}).append(that.getPre("Direction"));
            } else if (config.getWidth() > 991 && config.getWidth() < 1100) {
                high = high + 75 - 310 - 25;
                dir = $("<div>").addClass("col-md-6 question").css({'overflow-y': 'scroll', 'height': high + "px"}).append(that.getPre("Direction"));
            } else {
                dir = $("<div>").addClass("col-md-6 question no-padding").append(that.getPre("Direction"));
            }
            return dir;
        }

        this.getQuestion = function () {
            var high = config.getHeight();
            if (config.getWidth() > 1100) {
                high = (high - 310 - 25) / 2;
                qus = $("<div>").addClass("col-md-12").attr({'id': 'qus'}).css({'overflow-y': 'scroll', 'max-height': high + 'px'}).append(that.getPre("Question"));
            } else if (config.getWidth() > 991 && config.getWidth() < 1100) {
                high = (high + 65 - 310 - 25) / 2;
                qus = $("<div>").addClass("col-md-12").attr({'id': 'qus'}).css({'overflow-y': 'scroll', 'max-height': high + 'px'}).append(that.getPre("Question"));
            } else {
                qus = $("<div>").addClass("col-md-12 no-padding").attr({'id': 'qus'}).append(that.getPre("Question"));
            }
            return qus;
        }

        this.getOption = function () {
            var high = config.getHeight();
            if (config.getWidth() > 1100) {
                high = (high - 310 - 25) / 2 - 10;
                option = $("<div>").addClass("col-md-12").attr({'id': 'option'}).css({'overflow-y': 'scroll', 'max-height': high + 'px', 'margin-top': '10px'}).append(that.getPre("Option"));
            } else if (config.getWidth() > 991 && config.getWidth() < 1100) {
                high = (high + 65 - 310 - 25) / 2;
                option = $("<div>").addClass("col-md-12").attr({'id': 'option'}).css({'overflow-y': 'scroll', 'max-height': high + 'px', 'margin-top': '10px'}).append(that.getPre("Option"));
            } else {
                option = $("<div>").addClass("col-md-12 no-padding").attr({'id': 'option'}).append(that.getPre("Option"));
            }
            return option;
        }
        this.setDirection_Loading = function () {
            dir.find(".navbar-default").empty();
            dir.find(".navbar-default").append($("<center>").append(config.getLoadingData()));
        }
        this.setQuestion_Loading = function () {
            qus.find(".btn-default").empty();
            qus.find(".btn-default").append($("<center>").append(config.getLoadingData()));
        }
        this.setOption_Loading = function () {
            option.find(".navbar-default").empty();
            option.find(".navbar-default").append($("<center>").append(config.getLoadingData()));
        }

        this.setQuestionNO = function (no) {
            question_no.empty();
            question_no.append(no);
        }
        this.setSectionTitle = function (title) {
            section_title.empty();
            section_title.append(title);
        }
        
        this.setTeXMML = function (title) {
              config.getModel("LaTeXMathematics").Update(box_header)
        }

        this.setDirection = function (direction) {
            var high = dir.height();
            dir.find(".navbar-default").empty();
            if (direction != "null") {
                dir.find(".navbar-default").append("<b>Direction :</b><br>" + direction);
                dir.css({'display': 'block'});
                qus.parent().removeClass("col-md-12");
                qus.parent().addClass("col-md-6");
            } else {
                if (config.getWidth() > 1100) {
                    dir.parent().css({'height': high + 'px'})
                } else if (config.getWidth() > 991 && config.getWidth() < 1100) {
                    dir.parent().css({'height': high + 'px'})
                }
                dir.css({'display': 'none'});
                qus.parent().removeClass("col-md-6");
                qus.parent().addClass("col-md-12");
            }

        }

        this.setQuestion = function (Question) {
            qus.find(".navbar-default").empty();
            qus.find(".navbar-default").append("<b>Question :</b><br>" + Question);
            var high = dir.height() - qus.height() - 10;
            option.css({'max-height': high + 'px'});
        }

        this.setOption = function (o) {
            option.find(".navbar-default").empty();
            select_option = $("<form>");
            option.find(".navbar-default").append($("<b>").append("Options :")).append($("<br>")).append(select_option);
            var keys = [];
            for (var k in o) {
                if (o.hasOwnProperty(k)) {
                    keys.push(k);
                }
            }
            keys.sort();
            for (i = 0; i < keys.length; i++) {
                var key = keys[i];
                select_option.append(that.getRadio(key, o[key]));
            }
        }

        this.setSelectedOption = function (ans) {
            var selected = select_option.find("input[type='radio'][name='optionsRadios'][value='" + ans + "']");
            selected.prop("checked", true);
        }

        this.getSelectedOption = function () {
            var selected = select_option.find("input[type='radio'][name='optionsRadios']:checked");
            return selected.val();
        }

        this.clearAnswered = function () {
            var selected = select_option.find("input[type='radio'][name='optionsRadios']:checked");
            selected.prop('checked', false);
        }


        this.getRadio = function (key, value) {
            var op = $("<div>").append($("<label>").append($("<input>").attr({'type': 'radio', 'name': 'optionsRadios', 'id': key, 'value': key})).append("&nbsp;" + value));
            return op;
        }

        this.getPre = function (title) {
            var pre = $("<div>").addClass("navbar-default test_navbar-default").css({'text-align': 'justify', 'padding-left': '15px', 'padding-right': '15px'}).append(title + " :")
            return pre;
        }

        this.addListener = function (list) {
            listeners.push(list);
        }
    }
});