jQuery.extend({
	ChartSheetVgr: function(config,pre){
		var that = this;
		var row ;
		var table ;
		var temp_obj ;
		var test_id ;
		var lang_drop_down ;
		var count=0 ;
		var data1 =[];
		var data2 =[];
		var listeners = new Array();
                
                this.getTestLogo =function (){
                    return $('<img />', { 
                        class: 'img-responsive',
                        src: '../../dist/images/exam.gif',
                    });
                }
                this.addChartSheet =function (data){
                    row.append(that.getTable(data));
                    that.setChart(row.find("#atmp_natmp"),data1);
                    that.setChart(row.find("#crt_wans"),data2);
                }                                                        
                this.removeFail = function() { 
                    row.empty();
                }
                this.selLoadBegin =function (){
                   row.append($("<center>").append(config.getLoadingData()));
                }
                this.selLoadFinish=function (){
                   row.empty();
                }
                this.selLoadFail = function() { 
                    row.append("<center>Server doesn't respond.</center>");
                }
                this.removeFailTest = function() { 
                    temp_obj.empty();
                    temp_obj.append("Start Test");
                }
                this.selTestLoadBegin =function (){
                   temp_obj.empty();
                   temp_obj.append(config.getLoadingData());
                }
                this.selTestLoadFinish=function (){
                    temp_obj.empty();
                    temp_obj.append("Start Test");
                }
                this.selTestLoadFail = function() { 
                    temp_obj.empty();
                    temp_obj.append("You doesn't start Test before Date and time");
                }
                
                this.loadTest =function (){
                    count=0;
                    that.removeFail();
                    $.each(listeners, function(i){
                        listeners[i].selChartSheet(test_id);
                    });
                } 
                this.addListener = function(list){
                            listeners.push(list);
                }
            this.init = function (testid,value){
                test_id=testid;
                pre.setHeaderTitle("Report Chart Sheet");
                pre.setBreadCrumbName("fa-line-chart","Report Chart Sheet");
                pre.removeBoxHeader();
                pre.removeBoxbody();
                pre.setBoxHeaderTitle(that.getHeader(value));
                row=$("<div>").addClass("row");
                pre.setBoxBodyContent(row);
                if(config.getModel("ChartSheet")==null){
//                                table.append($("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin")));
                                $.getScript("../model/ChartSheet.js").done(function() {
                                    config.setModel("ChartSheet",new $.ChartSheet(config));
                                    $.getScript("../controller/ChartSheetMgr.js").done(function() {
                                        $.getScript("../../chart/jquery.flot.js").done(function() {
                                            $.getScript("../../chart/jquery.flot.pie.js").done(function() {
                                                config.setController("ChartSheetMgr",new $.ChartSheetMgr(config,config.getModel("ChartSheet"),config.getView("ChartSheetVgr")));
                                                that.selLoadFinish();
                                                that.loadTest();
                                            }).fail(function() {alert("Error :problem in jquery.flot.pie.js file ");}); 
                                        }).fail(function() {alert("Error:problem in jquery.flot.js file ");}); 
                                    }).fail(function() {alert("Error :problem in ChartSheetMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in ChartSheetvgr.js file ");}); 
                 }else{
                  //      that.selLoadFinish();
                        that.loadTest();
                 }             
            }
         this.labelFormatter=function (label, series) {
            return "<div style='font-size:12pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
         }
            
            this.setChart = function (obj_append,data){
                        var a=$("<div>").css({'width;':'100%','height':'100%'});
                        obj_append.append(a);
                        var placeholder = a;
			placeholder.unbind();
			$.plot(placeholder, data, {
				series: {
					pie: { 
						show: true,
						radius: 1,
						label: {
							show: true,
							radius: 3/4,
							formatter: that.labelFormatter,
							background: { 
								opacity: 0.5,
								color: "#000"
							}
						}
					}
				},
				legend: {
					show: false
				}
			});
                    
            }
            
            this.getHeader = function (value){
              return $("<div>").addClass("no-padding")
                        .append(that.getTestLogo().removeClass("img-responsive").css({'width':'50px'}))
                        .append($("<h2>").addClass("box-title").css({'line-height':'24px'}).append("&nbsp;&nbsp;Chart Sheet for Mock Test ").append($("<b>").append(value)))
                        .append($("<div>").addClass("form-group pull-right").css({'margin-right':'15px'})
                            );
            }
            
            this.getTable = function (value){
              var tna= parseInt(value.total_question)-parseInt(value.total_attempted);
              var twa= parseInt(value.total_attempted)-parseInt(value.total_correct);
              var row=$("<div>")
                      .append($("<div>").addClass("col-lg-6 col-md-6 col-sm-6 col-xs-12")
                            .append($("<div>").addClass("box")
                                .append($("<center>").addClass("box-header")
                                        .append($("<h4>").addClass("box-title").append("&nbsp;Attempted / Not Attempted")))
                                 .append($("<center>").addClass("box-body").attr({'id':'atmp_natmp'}).css({'height':'300px'}))
                                 .append($("<div>").addClass("box-footer")
                                 .append($("<ul>").addClass("nav nav-stacked")
                                        .append($("<li>").append($("<a>")
                                            .append($("<b>").append("Total Question"))
                                            .append($("<span>").addClass("pull-right").append(value.total_question))
                                        ))
                                        .append($("<li>").append($("<a>")
                                            .append($("<b>").append("Total Attempted"))
                                            .append($("<span>").addClass("pull-right").append(value.total_attempted))
                                        ))
                                        .append($("<li>").append($("<a>")
                                            .append($("<b>").append("Total Not Attempted"))
                                            .append($("<span>").addClass("pull-right").append(tna))
                                        ))
                                 ))
                             ))
                      .append($("<div>").addClass("col-lg-6 col-md-6 col-sm-6 col-xs-12")
                            .append($("<div>").addClass("box with-border")
                                .append($("<center>").addClass("box-header")
                                        .append($("<h4>").addClass("box-title").append("&nbsp;Correct / Wrong Ans.")))
                                 .append($("<center>").addClass("box-body").attr({'id':'crt_wans'}).css({'height':'300px'}))
                                 .append($("<div>").addClass("box-footer")
                                 .append($("<ul>").addClass("nav nav-stacked")
                                        .append($("<li>").append($("<a>")
                                            .append($("<b>").append("Total Attempted"))
                                            .append($("<span>").addClass("pull-right").append(value.total_attempted))
                                        ))
                                        .append($("<li>").append($("<a>")
                                            .append($("<b>").append("Total Correct Ans"))
                                            .append($("<span>").addClass("pull-right").append(value.total_correct))
                                        ))
                                        .append($("<li>").append($("<a>")
                                            .append($("<b>").append("Total Wrong Ans"))
                                            .append($("<span>").addClass("pull-right").append(twa))
                                        ))
                                 ))
                             ));
                     data1[0] = {label: "Attempted",data: value.total_attempted, color: "#71c103"}
                     data1[1] = {label: "Not Attempted",data: tna, color: "#0070c2"}
                     data2[0] = {label: "Correct",data: value.total_correct,color: "#47ba1f"}
                     data2[1] = {label: "Wrong",data: twa,color: "#f7171f"}
                     return row;
                }
        },
	ChartSheetVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selTest:function() { }
		}, list);
	}
        
});