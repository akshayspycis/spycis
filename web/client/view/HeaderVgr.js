jQuery.extend({
	HeaderVgr: function(config){
		var that = this;
                var navbar ;
                var navbar_main ;
                var header;
                this.setHeader = function(){
                    $("body").addClass("fixed")
			header =$("<header>").addClass("main-header");
                        header.append(that.setLogo);
                        navbar=$("<div>").addClass("navbar-custom-menu");
                        navbar_main=$("<nav>").addClass("navbar navbar-static-top").attr("role","navigation");
                        header.append(navbar_main.append(that.setMenuIcon()).append(navbar));
                        return header;
		}
                this.setLogo = function (){
                    var logo_anchaor =$("<a>").addClass("logo");
                    logo_anchaor.append($("<span>").addClass("logo-mini").append($("<b>").append($("<img>").attr({'src':'../../dist/images/log.png'})).css({'font-family': 'freedom'})));
                    logo_anchaor.append($("<span>").addClass("logo-lg").css({'margin-top':'-5px'}).append($("<img>").attr({'src':'../../dist/img/logo_1.png','width':'200px'})));
                    return logo_anchaor;
                }
                this.setMenuIcon = function (){
                    var anchor =$("<a>").addClass("sidebar-toggle").attr({'data-toggle':'offcanvas','role':'button'})
                            .append($("<span>Toggle navigation").addClass("sr-only"))
                            .append($("<span>").addClass("icon-bar"))
                            .append($("<span>").addClass("icon-bar"))
                            .append($("<span>").addClass("icon-bar"));
                    return anchor;
                }
                this.setBox= function (div){
                    navbar_main.find("#search_box_div").remove();
                    navbar_main.append(div);
                }
                this.setList= function (div){
                    navbar.empty();
                    navbar.append(div);
                }
                
        }
});
