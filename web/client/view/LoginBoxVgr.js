jQuery.extend({
	LoginBoxVgr: function(config){
		var that = this;
		var listeners = new Array();
                var form;
                var model_footer;
                var img_loading_forget;
                var img_loading_register;
                var img_loading_sigin;
                var loading_boolean=true;
                this.getLoginBox = function(){
                        img_loading_forget=$("<a>");
                        img_loading_register=$("<a>");
                        img_loading_sigin=$("<span>");
			var model =$("<div>").addClass("login-box-body");
                        model.append(that.getLoginBoxHeader("Sign in to start your session"));
                        model.append(that.getLoginBoxBody());
                        model.append($("<div>").addClass("social-auth-links text-center"));
                        model.append($("<a>").css({'cursor':'pointer','font-weight':'600','color':'#01579b'}).append("I forgot my password").append("&nbsp;").append(img_loading_forget).click(function (){
                            if(loading_boolean){
                                that.loadFPBox();
                            }
                        }));
                        model.append($("<br>"));
                        model.append($("<a>").addClass("text-center").css({'cursor':'pointer','font-weight':'600','color':'#01579b'}).append("Register a new membership").append("&nbsp;").append(img_loading_register).click(function (){
                            if(loading_boolean){
                                that.loadRegisterBox();
                            }
                        }));
                        return model;
		}
                
                this.getLoginBoxHeader= function (header_title){
                    var model_header =$("<p>").addClass("login-box-msg").append(header_title).css({'font-weight': '600'});
                    return model_header;
                }
                
                this.getLoginBoxBody = function (model_form){
                    form =$("<form>").attr({'autocomplete':'on'});
                    form.append($("<div>").addClass("form-group has-feedback")
                                .append($("<input/>").attr({'type':'email','id':'email','placeholder':'Email','name':'email'}).addClass("form-control"))
                                .append($("<span>").addClass("glyphicon glyphicon-envelope form-control-feedback")));
                    form.append($("<div>").addClass("form-group has-feedback")
                                .append($("<input/>").attr({'type':'password','id':'password','placeholder':'Password','name':'password'}).addClass("form-control")
                                .keyup(function(event) {
                                    if (event.keyCode==13) {
                                        if(loading_boolean){
                                            $.each(listeners, function(i){
                                                listeners[i].checkLogin(form);
                                            });
                                        }
                                    }
                                }))
                                .append($("<span>").addClass("glyphicon glyphicon-lock form-control-feedback")));
                    form.append($('<label id="error"></label>'));
                    var row = $("<div>").addClass("row")    
                    row.append($("<div>").addClass("col-xs-7").append($("<div></div>").addClass("checkbox").append($("<label></label>")
                            //.append($("<input/>").attr({'type':'checkbox','id':'checkbox','name':'checkbox'})).append(" Remember Me")
                            )));
                    row.append($("<div>").addClass("col-xs-5 pull-right")
                            .append($("<a>").addClass("btn btn-primary btn-block").attr({'type':'button','id':'submit','name':'button'}).append("Sign In").click(function (){
                                if(loading_boolean){
                                    $.each(listeners, function(i){
                                        listeners[i].checkLogin(form);
                                    });
                                }
                            }))
                            );
                    
                    form.append(row);
                    return form;
                }
                this.insLoadBegin =function (){
                    form.find(".row").find("#submit").prop('disabled', true);
                    form.find(".row").find("#submit").empty();
                    form.find(".row").find("#submit").append(config.getLoadingData());
                }
                
                this.insLoadFail = function() { 
                    form.find(".row").find("#submit").prop('disabled', false);
                    form.find(".row").find("#submit").empty();
                    form.find(".row").find("#submit").append("Sign In");
                    form.find("#error").empty();
                    form.find("#error").append("Error: Server doesn't Respond.")
                    form.find("#error").css({'display':'block','color':'red'});    
                    that.removeLoading();
                }
                
                this.setloading =function (img){
                    loading_boolean=false;
                    img.empty();
                    img.append(config.getLoadingData());
                }
                this.removeLoading =function (){
                    loading_boolean=true;
                }
                this.setServerError =function (){
                    model_footer.find("#error_msg").append("Server doesn't Respond.");
                }
                this.removeServerError =function (){
                    model_footer.find("#error_msg").empty();
                }
                this.loadRegisterBox = function (){
                    that.setloading(img_loading_register);
                    config.loadRegistraionBoxVgr(that);
                }
                this.loadFPBox = function (){
                    that.setloading(img_loading_forget);
                    config.loadFPBox(that);
                }
                this.getForm= function (){
                    return form;
                }
                this.addListener = function(list){
                            listeners.push(list);
                }
        },
	
	/**
	 * let people create listeners easily
	 */
	LoginBoxVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadNotificationVgr : function() { },
			loadNotification : function() { },
			loadNotificationMgr : function() { }
		}, list);
	}
});
