jQuery.extend({
	UserRequestVgr: function(config,pre){
		var that = this;
                var software_id;
                var ul_requested_members;
		var count=0 ;
		var listeners = new Array();
                this.getRow = function (data){
                    var button=$("<button>").addClass("btn btn-primary btn-sm").append($("<i>").addClass("fa  fa-check")).append("&nbsp;&nbsp;Accept").css({'display':'block'});
                    button.click(function (){
                        if($(this).hasClass("btn-primary"))
                            that.acceptRequest($(this),software_id,data.user_id,data.user_request_id);
                        else 
                            that.addExamCircle($(this),software_id,data.user_id);
                    });
                    var img=$("<img>").attr({'src':'../../dist/img/user2-160x160.jpg'});
                    return $("<li>").append(img).append($("<a>").addClass("users-list-name").append(data.user_name))
                            .append($("<span>").addClass("users-list-date").append(data.dob))
                            .append($("<center>").append(button))
//                            .hover(
//                                  function() {
//                                    $( this ).css({'background':'#ececec','-webkit-transform':'scale(1.8)','-ms-transform':'scale(1.8)','transform':'scale(1.8)'} );
//                                    $( this ).find("button").css({'display':'block'});
//                                  }, function() {
//                                    $( this ).css({'background':'','-webkit-transform':'','-ms-transform':'','transform':''} );
//                                    $( this ).find("button").css({'display':'none'});
//                                  }
//                            );
                }
                
                this.setUserRequest = function (id,ul){
                    software_id=id;
                    ul_requested_members=ul;
                    that.loadUserRequest(1);
                }
                this.addUserRequest =function (data){
                    ul_requested_members.append(that.getRow(data));
                }                                                        
                
                this.selLoadBegin =function (){
                   ul_requested_members.html($("<li>").append(config.getLoadingData()));
                }
                
                this.selLoadFinish=function (){
                    ul_requested_members.empty();
                }
                this.selLoadFail = function() { 
                    ul_requested_members.html("<li>Server doesn't respond.</center>");
                }
                
                this.selLoadBegin_accept =function (obj){
                    obj.html(config.getLoadingFb());
                }
                this.selLoadFinish_accept=function (obj){
                        obj.empty();
                        if(obj.hasClass("btn-primary")){
                            obj.removeClass("btn-primary");
                            obj.addClass("btn-success");
                            obj.append($("<i>").addClass("fa fa-plus")).append("&nbsp;&nbsp;Exam Circle")
                        }else{
                            obj.removeClass("btn-success");
                            obj.addClass("btn-default");
                            obj.append($("<i>").addClass("fa fa-mail-reply-all")).append("&nbsp;Wait for User")
                            obj.css({'margin-left':'-9px'})
                        }
                        
                }
                this.selLoadFail_accept = function(obj) { 
                    obj.html("Retry");
                }
                
                this.addExamCircle=function (obj,software_id,user_id){
                    $.each(listeners, function(i){
                        listeners[i].addExamCircle(obj,software_id,user_id);
                    });
                } 
                this.acceptRequest=function (obj,software_id,user_id,user_request_id){
                    $.each(listeners, function(i){
                        listeners[i].acceptRequest(obj,software_id,user_id,user_request_id);
                    });
                } 
                
                this.loadUserRequest =function (module){
                    $.each(listeners, function(i){
                        listeners[i].selUserRequest(software_id,module);
                    });
                } 
                this.addListener = function(list){
                            listeners.push(list);
                }
            this.init = function (testid,value){
                test_id=testid;
                pre.setHeaderTitle("Answer Sheet");
                pre.setBreadCrumbName("fa-line-chart","Answer Sheet");
                pre.removeBoxHeader();
                pre.removeBoxbody();
                pre.setBoxHeaderTitle(that.getHeader(value));
                table=that.getTable();
                pre.setBoxBodyContent(table);
                if(config.getModel("UserRequest")==null){
                                row.append($("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin")));
                                $.getScript("../model/UserRequest.js").done(function() {
                                    config.setModel("UserRequest",new $.UserRequest(config));
                                    $.getScript("../controller/UserRequestMgr.js").done(function() {
                                        config.setController("UserRequestMgr",new $.UserRequestMgr(config,config.getModel("UserRequest"),config.getView("UserRequestVgr")));
                                            that.selLoadFinish();
                                            that.loadTest();
                                    }).fail(function() {alert("Error :problem in UserRequestMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in UserRequestvgr.js file ");}); 
                 }else{
                        that.selLoadFinish();
                        that.loadTest();
                 }             
            }
            
        },
        	
	/**
	 * let people create listeners easily
	 */
	UserRequestVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selTest:function() { }
		}, list);
	}
        
});