jQuery.extend({
	SolutionReportVgr: function(config,pre){
		var that = this;
		var row ;
		var temp_obj ;
		var test_id ;
		var lang_drop_down ;
		var count=0 ;
		var listeners = new Array();
                this.getRow = function (val,data,option,dir){
                    var direction;
                    count++;
                    if(dir!=undefined){
                        direction=$("<div>").addClass("no-padding").css({'margin-top': '-14px'}).append($("<h4>").append("Direction : "))
                            .append(decodeURIComponent(dir))
                    }
                    return $("<div>").addClass("panel-body")
                                .append($("<div>").addClass("box-header btn-default")
                                .append($("<h3>").addClass("box-title")
                                .append("Question No. "+count)))
                                .append($("<div>").addClass("box-body")
                                        .append($("<img>").attr({'src':'../../dist/images/watermark.PNG'}).css({'pointer-events': 'none','position': 'absolute','width': '130%','margin-left':'-130px','margin-top':'50px','z-index':'-1'}))
                                        .append(direction)
                                        .append($("<h4>").append("Question : "))
                                        .append(decodeURIComponent(data)))
                                        .append(that.getOptionDetails(option))
                                .append($("<div>").addClass("box-footer")
                                    .append($("<div>").addClass("col-md-1 col-sm-2 col-xs-2 no-padding text-black"))
                                    .append($("<div>").addClass("col-md-1 col-sm-2 col-xs-2 no-padding text-black"))
                                    .append($("<div>").addClass("col-md-1 col-sm-2 col-xs-2 no-padding text-black"))
                                    .append($("<div>").addClass("col-md-1 col-sm-2 col-xs-2 no-padding text-black"))
                                    .append($("<div>").addClass("col-md-1 col-sm-2 col-xs-2 no-padding text-black"))
                                    .append($("<div>").addClass("col-md-1 col-sm-2 col-xs-2 no-padding text-black"))
                                );
                }
                this.getOptionDetails= function (data){
                    var keys = [];
                    var option=$("<div>").addClass("box-body");
                    option.append($("<h4>").append("Options : "))
                    var o=data;
                    for (var k in o) {
                        if (o.hasOwnProperty(k)) {
                            keys.push(k);
                        }
                    }
                    keys.sort();
                    for (i = 0; i < keys.length; i++) {
                    var key = keys[i];
                        var value=o[key];
//                        if(that.select!=null){
//                            if(key==that.select){
//                                if(key==that.answer){
//                                    option.append(that.getLabel("text-green",key,decodeURIComponent(value),"fa-check"))
//                                }else{
//                                    option.append(that.getLabel("text-red",key,decodeURIComponent(value),"fa-times"))
//                                }
//                            }else{
//                                option.append(that.getLabel("text-black",key,decodeURIComponent(value),""))
//                            }
//                        }else{
                            
                            option.append(that.getLabel("text-black",key,decodeURIComponent(value),""))
//                        }
                    }
//                    correct_ans.append(that.answer);
                    return option;
                }
                this.getLabel = function (fontcolor,option_name,options,icon){
                    return $("<div>")
                            .append($("<a>").addClass(fontcolor)
                            .append($("<label>")
                                .append(option_name+"&nbsp;&nbsp;")
                                .append(options+"&nbsp;&nbsp;")
                                )
                            .append($("<span>").addClass("fa pull-right "+icon)));
                }
                this.getTestLogo =function (){
                    return $('<img />', { 
                        class: 'img-responsive',
                        src: '../../dist/images/exam.gif',
                    });
                }
                this.addSolutionReport =function (data,val){
                    $.each(data["question_details"][val],function (key,value){
                        if(key!=""){
                            row.append(that.getRow(val,value,data["option_details"][key],data["discription_details"][key]));
                        }else{
                            row.append(that.getRow(val,value,data["option_details"][key]));
                        }
                        
                    });
                   
                }                                                        
                this.removeFail = function() { 
                    row.empty();
                }
                this.selLoadBegin =function (){
                   row.append($("<center>").append(config.getLoadingData()));
                }
                this.selLoadFinish=function (){
                    row.empty();
                }
                this.selLoadFail = function() { 
                    row.append("<center>Server doesn't respond.</center>");
                }
                this.removeFailTest = function() { 
                    temp_obj.empty();
                    temp_obj.append("Start Test");
                }
                this.selTestLoadBegin =function (){
                   temp_obj.empty();
                   temp_obj.append(config.getLoadingData());
                }
                this.selTestLoadFinish=function (){
                    temp_obj.empty();
                    temp_obj.append("Start Test");
                }
                this.selTestLoadFail = function() { 
                    temp_obj.empty();
                    temp_obj.append("You doesn't start Test before Date and time");
                }
                
                this.loadTest =function (){
                    count=0;
                    that.removeFail();
                    $.each(listeners, function(i){
                        listeners[i].selSolutionReport(test_id,that.getLangId());
                    });
                } 
                this.addListener = function(list){
                            listeners.push(list);
                }
            this.init = function (testid,value){
                test_id=testid;
                if(config.getModel("Language")==null){
                        $.getScript("../model/Language.js").done(function() {
                                try{
                                        config.setModel("Language",new $.Language(config));
                                        config.getModel("Language").selLanguage();
                                }catch(e){
                                    alert(e);
                                }
                        }).fail(function() {alert("Error:002 js");}); 
                 }
                row=$("<div>").addClass("row")
                pre.setHeaderTitle("Question Paper");
                pre.setBreadCrumbName("fa-line-chart","Question Paper");
                pre.removeBoxHeader();
                pre.removeBoxbody();
                pre.setBoxHeaderTitle(that.getHeader(value));
                pre.setBoxBodyContent(row);
                if(config.getModel("SolutionReport")==null){
                                row.append($("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin")));
                                $.getScript("../model/SolutionReport.js").done(function() {
                                    config.setModel("SolutionReport",new $.SolutionReport(config));
                                    $.getScript("../controller/SolutionReportMgr.js").done(function() {
                                        config.setController("SolutionReportMgr",new $.SolutionReportMgr(config,config.getModel("SolutionReport"),config.getView("SolutionReportVgr")));
                                        setTimeout(function (){
                                            that.loadLanguage();
                                            that.selLoadFinish();
                                            that.loadTest();
                                        },200)
                                    }).fail(function() {alert("Error :problem in SolutionReportMgr.js file ");}); 
                                }).fail(function() {alert("Error:problem in SolutionReportvgr.js file ");}); 
                 }else{
                          setTimeout(function (){
                                            that.loadLanguage();
                                            that.selLoadFinish();
                                            that.loadTest();
                                        },200)
                 }             
            }
            this.getLanBox = function (){
                    lang_drop_down =$("<div>").addClass("form-group pull-right").css({'margin-right':'15px'});
                    lang_drop_down.append($("<select>").addClass("form-control lang1 select2 select2-hidden-accessible").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        that.loadTest();
                    });
                    return lang_drop_down;
                }
                this.getLangId=function (){
                    var lang_id;
                        $( ".lang1 option:selected" ).each(function() {
                            lang_id = $( this ).val();
                        });
                        return lang_id;
                }
                this.loadLanguage =function (module){
                    var all = config.getModel("Language").selLanguage(module);
                    lang_drop_down.find(".lang1").empty();
                        if(all){
                                $.each(all, function(item){
                                    that.addLanguage(all[item]);
                                });
                         }
                         
                } 
                 this.addLanguage=function (data){
                   lang_drop_down.find(".select2").append(that.getLangOption(data))
                }          
                this.getLangOption = function (data){
                    var row = $("<option></option>").attr({'value':data.language_id})
                              .append($("<td>"+data.language_name+"</td>"));
                    return row;            
                }
            
            this.getHeader = function (value){
//                <select class="form-control lang1 select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true"><option value="1">English</option><option value="2">Hindi</option></select>
              return $("<div>").addClass("no-padding")
                        .append(that.getTestLogo().removeClass("img-responsive").css({'width':'50px'}))
                        .append($("<h2>").addClass("box-title").css({'line-height':'24px'}).append("&nbsp;&nbsp;Question Paper for Mock Test ").append($("<b>").append(value)))
                        .append($("<div>").addClass("form-group pull-right").css({'margin-right':'15px'})
                            .append($("<div>").addClass("pull-left").css({'margin-right':'15px'}).append(that.getLanBox()))
                            .append($("<div>").addClass("pull-right")
                                .append($("<button>").addClass("btn btn-primary pull-right").attr({'type':'button'})
                                    .append("Print&nbsp;&nbsp;&nbsp;")
                                    .append($("<span>").addClass("badge").attr({'type':'button'})
                                         .append($("<i>").addClass("fa fa-print")))
                                     .click(function (){
                                         var divContents = row.html();
                                            var printWindow = window.open('', '', '');
                                            printWindow.document.write('<html><head><title>Question Paper</title>');
                                            printWindow.document.write('</head><body >');
                                            printWindow.document.write(divContents);
                                            printWindow.document.write('</body></html>');
                                            printWindow.document.close();
                                            printWindow.print();
                                     })     
                                    )));
            }
        },
        	
	/**
	 * let people create listeners easily
	 */
	SolutionReportVgrListener: function(list) {
		if(!list) list = {};
		return $.extend({
			selTest:function() { }
		}, list);
	}
        
});