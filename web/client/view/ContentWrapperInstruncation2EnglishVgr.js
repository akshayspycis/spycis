jQuery.extend({
	ContentWrapperInstruncation2EnglishVgr: function(config,per){
            var that=this;
            var lang_dorp_down;
            var check_test;
            this.init = function (){
                per.setHeaderTitle("Online Assessment Portel");
                per.setBreadCrumbName("fa-dashboard"," Instructions");
                per.removeBoxHeader();
                per.setBoxHeaderTitle($("<h3>").addClass("box-title").css({'line-height':':24px;'}).append("Please read the following instructions carefully"));
                per.removeBoxbody();
                per.setBoxBodyContent(that.getBody1());
                per.setBoxBodyContent(that.getBody2());
                per.setBoxBodyContent($("<div>").addClass("clear"));
                per.setBoxBodyContent($("<ul>").addClass("instruction").append('<li style="color:red">Please note all questions will appear in your default language. This language can be changed for a particular question later on</li>'));
                per.setBoxBodyContent($("<p>").css({'margin-top':'50px'}));
                per.setBoxBodyContent($("<center>").append($("<a>").addClass("preinst").append($("<b>").append("<<&nbspPrevious")).click(function (){
                    config.setIns(1);
                    config.getView("ContentWrapperInstruncation1EnglishVgr").init()
                })));
                per.setBoxBodyContent($("<p>").css({'margin-top':'50px'}));
                check_test=$("<input>").attr({'type':'checkbox'})
                per.setBoxBodyContent($("<ul>").addClass("instruction").css({'list-style':'none','line-height':'26px'}).append($("<li>").append($("<label>").append(check_test).append(" I have read and understood the instructions. All Computer Hardwares allotted to me are in proper working condition. I agree that I am not carrying any prohibited gadget like mobile phone etc. / any prohibited material with me into the exam hall.  I agree that in case of not adhering to the instructions, I will be disqualified from taking the exam."))));
                per.setBoxBodyContent($("<p>").css({'margin-top':'50px'}));
                per.setBoxFooter(that.getFooter());
                config.getView("MainVgr").getListener().loadLanguageVgr(that);
            }
            
            this.getFooter = function (){
                return $("<center>").append($("<a>").addClass("next").append($("<b>").append("Begin Test&nbsp;>>")).css({'cursor':'pointer'}).click(function (){
                    if (check_test.prop('checked')==true){ 
                        config.setTest_Time(config.getModel("ContentWrapperAssesment").getTestObj(config.getTest_id()).time);
                        that.setLocalStorage("test_time",config.getTest_Time());
                        that.setLocalStorage("lang_id",config.getLanguage_id());
                        config.getView("MainVgr").setTest();
                    }else{
                        alert("Please Accept the Terms & Condtions before Proceeding.");
                    }
                }));
            }
            this.setLocalStorage = function (key,value){
                if(typeof(Storage) !== "undefined") {
                    localStorage.setItem(key,encodeURIComponent(value));
                } 
            }
            this.getBody1 = function (){
                return $("<div>").addClass("col-md-3").append($("<b>").append("Choose your default Language:"));
            }
            this.getBody2 = function (){
                return $("<div>").addClass("col-md-3").append(that.getLanguage_DropDown());
            }
            this.getLanguage_DropDown = function (){
                    lang_dorp_down =$("<div>").addClass("form-group");
                    lang_dorp_down.append($("<select>Category :</select>").addClass("lang form-control").attr({'tabindex':'-1','aria-hidden':'true'})).change(function () {
                        var id = lang_dorp_down.find(".lang option:first").val();
                            $( ".lang option:selected" ).each(function() {
                              id = $( this ).val();
                            });
                           config.setLanguage_id(id);
                    });
                    return lang_dorp_down;
                }
            this.setLanguage =function (language){
                lang_dorp_down.find(".lang").append($("<option>").attr({'value':language.language_id}).append(language.language_name));
                if(config.getLanguage_id()==null){
                    lang_dorp_down.find(".lang").trigger("change");
                }
            }
        }
        
});