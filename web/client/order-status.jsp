<%@page import="com.class_manager.checksum_details.ChecksumDetailsMgr"%>
<%@page import="com.manager.paytm_gateway.paytm.pg.checksumKit.ChecksumVerification"%>
<%@page import="com.class_manager.paytm_response_details.PaytmResponseDetailsMgr"%>
<%@page import="com.paytm.pg.merchant.CheckSumServiceHelper"%>
<%@page import="com.data_manager.config.GlobalData"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Paytm Secure Online Payment Gateway</title>
        <script src="../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    </head>
    <body>
        <table align='center'>
            <tr>
                <td><STRONG>Transaction is being processed,</STRONG></td>
            </tr>
            <tr>
                <td><font color='blue'>Please wait ...</font></td>
            </tr>
            <tr>
                <td>(Please do not press 'Refresh' or 'Back' button)</td>
            </tr>
            <%
                TreeMap<String, String> checksum_detials = new TreeMap<String, String>();
                Map<String, String[]> parameters = request.getParameterMap();
                String ORDERID = null;
                String CHECKSUMHASH = null;
                for (String parameter : parameters.keySet()) {
                    String[] values = parameters.get(parameter);
                    for (String value : values) {
                        checksum_detials.put(parameter, value);
                        if ("ORDERID".equalsIgnoreCase(parameter)) {
                            ORDERID = value;
                        } else if ("CHECKSUMHASH".equalsIgnoreCase(parameter)) {
                            CHECKSUMHASH = value;
                        }
                    }
                }
                String msg = null;
                if (parameters.size() > 0) {
                    if (new PaytmResponseDetailsMgr().insPaytmResponseDetails(checksum_detials)) {
                        if (new ChecksumVerification().checksumVerification(checksum_detials, CHECKSUMHASH)) {
                            String STATUS = checksum_detials.get("STATUS").toString();
                            int RESPCODE = Integer.parseInt(checksum_detials.get("RESPCODE").toString());
                            if (STATUS.equalsIgnoreCase("TXN_SUCCESS")) {
                                if (new ChecksumDetailsMgr().updChecksumDetails(true, ORDERID)) {
                                    msg = "Your Payment is Successfully Done.";
                                } else {
                                    msg = "Your Payment is Successfully Done.But there is a internal problem to update order status kindly contact with support@sattvikindiacouncil.com and share the transaction ID (" + checksum_detials.get("TXNID") + ")";
                                }
                            } else if (STATUS.equalsIgnoreCase("TXN_FAILURE")) {
                                new ChecksumDetailsMgr().updChecksumDetails(false, ORDERID);
                                switch (RESPCODE) {
                                    case 202:
                                        msg = "User does not have enough credit limit. Bank has declined the transaction.";
                                        break;
                                    case 205:
                                        msg = "Transaction has been declined by the bank.";
                                        break;
                                    case 207:
                                        msg = "Card used by customer has expired.";
                                        break;
                                    case 208:
                                        msg = "Transaction has been declined by the acquirer bank.";
                                        break;
                                    case 209:
                                        msg = "Card details entered by the user is/are invalid.";
                                        break;
                                    case 210:
                                        msg = "Lost Card received.";
                                        break;
                                    case 220:
                                        msg = "Bank communication error.";
                                        break;
                                    case 222:
                                        msg = "Transaction amount return by the gateway does not match with Paytm transaction amount.";
                                        break;
                                    case 227:
                                        msg = "Txn Failed.";
                                        break;
                                    case 229:
                                        msg = "3D Secure Verification failed.";
                                        break;
                                    case 232:
                                        msg = "Invalid account details.";
                                        break;
                                    case 295:
                                        msg = "No Description available.";
                                        break;
                                    case 296:
                                        msg = "We are facing problem at bank's end. Try using another Payment mode.";
                                        break;
                                    case 297:
                                        msg = "Cancel and Redirect to 3D Page.";
                                        break;
                                    case 401:
                                        msg = "Abandoned transaction.";
                                        break;
                                    case 402:
                                        msg = "Transaction abandoned from CCAvenue.";
                                        break;
                                    case 810:
                                        msg = "Closed after page load.";
                                        break;
                                    case 2271:
                                        msg = "User cancelled the transaction on banks net banking page.";
                                        break;
                                    case 2272:
                                        msg = "User cancelled the transaction from 3D secure/OTP page.";
                                        break;
                                    case 3102:
                                        msg = "Invalid card details.";
                                        break;
                                    case 330:
                                        msg = "Paytm checksum mismatch.";
                                        break;
                                    default:
                                        msg = "Internal Server Error";
                                        break;
                                }
                            }

                        } else {
                            msg = "Internal Error for check checksum details from Paytm server.";
                        }
                    } else {
                        msg = "Internal Server Error for store check Response.";
                    }
                } else {
                    msg = "No Response fetching from the paytm Server.";
                }
            %>
            <tr>
                <td> </td>
            </tr>
            <tr>
                <td> </td>
            </tr>
            <tr>
                <td><h3 id="msg_id"> <% out.print(msg);%> </h3></td>

            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td><p id="redirect_id"></p></td>
            </tr>

        </table>
        <script>
            var i = 10;
            var msg = $("#msg_id").text();
            if (msg.trim() == "Your Payment is Successfully Done.") {
                setInterval(function () {
                    document.getElementById("redirect_id").innerHTML = "It is Automatically redirect to www.Spycis.com with in " + i + " sec";
                    if (i < 1) {
                        window.close();
                    }
                    i--;
                }, 1000)
            }
        </script>
    </body>
