jQuery.extend({
	SolutionReportMgr: function(config,model,view){
		var vlist = $.SolutionReportVgrListener({
                    selSolutionReport : function(test_id,lang_id){
                            var all = model.selSolutionReport(test_id,lang_id);
                            if(all){
                                $.each(all, function(item){
                                    var keyset=all[item].keyset;
                                    $.map(keyset, function( val, i) {
                                         view.addSolutionReport(all[item]["q"][lang_id],val);
                                    });
                                });
                            }
                    }
                   
		});
            view.addListener(vlist);
            var mlist = $.SolutionReportListener({
			loadItem : function(item,key,module){
                            view.addSolutionReport(item,key);
			},
                        selLoadBegin:function (module){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (module){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (module){
                            view.selLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
