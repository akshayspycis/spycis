jQuery.extend({
	UserProfileMgr: function(config,model, view){
		var vlist = $.UserProfileVgrListener({
			updUserProfile : function(modelform){
				model.updUserProfile(modelform);
			}
		});
		view.addListener(vlist);

            var mlist = $.UserProfileListener({
			updLoadBegin : function() {
				view.updLoadBegin();
			},
			updLoadFail : function() {
				view.updLoadFail();
			},
			updLoadFinish : function() {
				view.updLoadFinish();
			}
		});
		model.addListener(mlist);
	}
});
