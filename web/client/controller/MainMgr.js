jQuery.extend({
	MainMgr: function(config){
		var vlist = $.MainVgrListener({
			loadHeaderVgr : function(){
                                $.getScript("../view/HeaderVgr.js").done(function(){
                                    try {
                                        config.setView("HeaderVgr",new $.HeaderVgr(config));
                                        vlist.loadMainSidebarVgr();
                                }catch(e){
                                        console.log("Error:004 problem in headervgr.js file "+e);
                                }
                                }).fail(function() {console.log("Error:004 problem in headervgr.js file ");}); 
			},
                        loadMainSidebarVgr : function(){
                                $.getScript("../view/MainSidebarVgr.js").done(function() {
                                    try {
                                    config.setView("MainSidebarVgr",new $.MainSidebarVgr(config));
                                    vlist.loadContentWrapperVgr();
                                        
                                }catch(e){
                                    console.log("Error:007 problem in MainSideBarVgr.js file "+e);
                                }
                                }).fail(function() {console.log("Error :007 problem in MainSidebarVgr.js file ");}); 
			},
                        loadContentWrapperVgr : function(){
                                $.getScript("../view/ContentWrapperVgr.js").done(function() {
                                    try {
                                    config.setView("ContentWrapperVgr",new $.ContentWrapperVgr(config));
                                    vlist.loadControlSidebarVgr();
                                }catch(e){
                                    console.log("Error:0110 problem in ContentWrapperVgr.js file "+e);
                                }
                                }).fail(function() {console.log("Error :010 problem in ContentWrapperVgr.js file ");}); 
			},
                        loadControlSidebarVgr : function(){
                                $.getScript("../view/ControlSidebarVgr.js").done(function() {
                                    try {
                                    config.setView("ControlSidebarVgr",new $.ControlSidebarVgr(config));
                                    vlist.loadFooterVgr();
                                }catch(e){
                                    console.log("Error:010 problem in ControlSidebarVgr.js file "+e);
                                }
                                }).fail(function() {console.log("Error :010 problem in ControlSidebarVgr.js file ");}); 
			},
                        loadFooterVgr : function(){
                                $.getScript("../view/FooterVgr.js").done(function() {
                                    try {
                                    config.setView("FooterVgr",new $.FooterVgr(config));
                                    vlist.setBodyData();
                                }catch(e){
                                    console.log(e);
                                }
                                }).fail(function() {console.log("Error :013 problem in FooterVgr.js file ");}); 
			},
                        loadLanguageVgr: function(module){  
                            if(config.getView("LanguageVgr")==null){
                                $.getScript("../view/LanguageVgr.js").done(function() {
                                    try{
                                        config.setView("LanguageVgr",new $.LanguageVgr(config));
                                        vlist.loadLanguage(module);
                                    }catch(e){
                                        console.log(e);
                                    }
                                }).fail(function() {console.log("Error:002 Vgr");}); 
                            }else{
                                config.getView("LanguageVgr").setLanguage(module);
                            }
			},         
                        loadLanguage: function(module){  
				$.getScript("../model/Language.js").done(function() {
                                    try{
                                        config.setModel("Language",new $.Language(config));
                                        vlist.loadLanguageMgr(module);
                                }catch(e){
                                    console.log(e);
                                }
                                }).fail(function() {console.log("Error:002 js");}); 
			},         
                        loadLanguageMgr: function(module){  
				$.getScript("../controller/LanguageMgr.js").done(function() {
                                    try{
                                        config.setController("LanguageMgr",new $.LanguageMgr(config,config.getModel("Language"),config.getView("LanguageVgr")));
                                        config.getView("LanguageVgr").setLanguage(module);
                                }catch(e){
                                    console.log(e);
                                }
                                }).fail(function() {console.log("Error:002 Mgr");}); 
			},         
                        loadSectionVgr: function(){  
				$.getScript("../view/SectionVgr.js").done(function() {
                                    try{
                                        config.setView("SectionVgr",new $.SectionVgr(config));
                                        vlist.loadSection();
                                }catch(e){
                                    console.log(e);
                                }
                                }).fail(function() {console.log("Error:002");}); 
			},         
                        loadSection: function(){  
				$.getScript("../model/Section.js").done(function() {
                                    try{
                                        config.setModel("Section",new $.Section());
                                        vlist.loadSectionMgr();
                                }catch(e){
                                    console.log(e);
                                }
                                }).fail(function() {console.log("Error:002");}); 
			},         
                        loadSectionMgr: function(){  
				$.getScript("../controller/SectionMgr.js").done(function() {
                                    try{
                                        config.setController("SectionMgr",new $.SectionMgr(config,config.getView("SectionVgr"),config.getModel("Section")));
                                        config.getView("SectionVgr").setSection();
                                }catch(e){
                                    console.log(e);
                                }
                                }).fail(function() {console.log("Error:002");}); 
			},         
                        loadQuestionVgr: function(kk){  
				$.getScript("../view/QuestionVgr.js").done(function() {
                                    try{
                                        config.setView("QuestionVgr",new $.QuestionVgr(config));
                                        vlist.loadQuestion();
                                }catch(e){
                                    console.log(e);
                                }
                                }).fail(function() {console.log("Error:002 vgr");}); 
			},         
                        loadQuestion: function(){  
				$.getScript("../model/Question.js").done(function() {
                                    try{
                                        config.setModel("Question",new $.Question(config));
                                        vlist.loadQuestionMgr();
                                }catch(e){
                                    console.log(e);
                                }
                                }).fail(function() {console.log("Error:002 ");}); 
			},         
                        loadQuestionMgr: function(){  
				$.getScript("../controller/QuestionMgr.js").done(function() {
                                    try{
                                        config.setController("QuestionMgr",new $.QuestionMgr(config,config.getModel("Question"),config.getView("QuestionVgr")));
                                        config.getView("QuestionVgr").setQuestion();
                                }catch(e){
                                    console.log("yahi hai call"+e);
                                }
                                }).fail(function() {console.log("Error:002 ayshi");}); 
			},
                        loadModelVgr : function(){
                            $.getScript("../view/ModelVgr.js").done(function() {
                                    try{
                                        config.setView("ModelVgr",new $.ModelVgr(config));
                                        vlist.loadErrorMsgVgr();
                                }catch(e){
                                    console.log(e);
                                }
                                }).fail(function() {console.log("Error:002");}); 
			},
                        loadErrorMsgVgr : function(){
                            $.getScript("../../admin/view/ErrorMsgVgr.js").done(function() {
                                try{
                                        config.setView("ErrorMsgVgr",new $.ErrorMsgVgr(config));
                                }catch(e){
                                    console.log(e);
                                }
                                }).fail(function() {console.log("Error:002");}); 
			},
                        setBodyData : function(){
                                config.getView("MainVgr").setBody();
                                vlist.loadOtherJs();
                                vlist.loadModelVgr();
			},
                        startAjax :function (){
                            $(document).ajaxStart(function() { Pace.restart(); });
                        },
                        loadOtherJs : function(){
//                                $.getScript("http://cdn.peerjs.com/0.3/peer.js").done(function() {
//                                    try{
//                                        config.setPeer(new Peer({key: 'm9h96vr0cdavpldi'}));
//                                        config.getPeer().on('open', function(id) {
//                                            if(id!=""){
//                                                config.getModel("ProfileConfig").setPeerID(id);
//                                            }
//                                        });
//                                    }catch(e){
//                                        console.log("yahi hai call"+e);
//                                    }
//                                }).fail(function() {console.log("Error:002 ayshi");}); 
				$.getScript("../../bootstrap/js/bootstrap.min.js"); 
                                $.getScript("../../dist/js/app.js"); 
                                $.getScript("../../plugins/slimScroll/jquery.slimscroll.min.js"); 
                                $.getScript("../../plugins/pace/pace.js").done(function() {
                                        try{
                                            $("<link/>", {
                                               rel: "stylesheet",
                                               type: "text/css",
                                               href: "../../dist/css/pace.css",
                                               onload: vlist.startAjax,
                                            }).appendTo("head");
                                        }catch(e){
                                            console.log("yahi hai call"+e);
                                        }
                                }).fail(function() {console.log("Error:002 pace.js");}); 
                                $.getScript("../../dist/js/jquery.validate.min.js"); 
                                $.getScript("../model/Resize.js"); 
                                $.getScript("../model/UploadImg.js"); 
			}
                        
		});
                
		config.getView("MainVgr").addListener(vlist);

            var mlist = $.MainListener({
			loadBegin : function() {
				view.setLodingJs($("#data_box"));
			},
			loadFail : function() {
                            
			},
			loadFinish : function() {
				view.finishLoadinJs($("#data_box"));
			},
			loadItem : function(item){
				view.message("from ajax: " + item.name);
			}
		});
		config.getModel("Main").addListener(mlist);
	}
});
