jQuery.extend({
	MockTestHomeMgr: function(config,model, view){
		var vlist = $.MockTestHomeVgrListener({
			selMockTestHome : function(){
                            var all = model.selMockTestHome();
                            if(all){
                                $.each(all, function(item){
                                    view.addMockTestHome(all[item]);
                                });
                            }
			},
                        updMockTestHome : function(form){
                                model.updMockTestHome(form);
			}
		});
		view.addListener(vlist);

            var mlist = $.MockTestHomeListener({
			loadItem : function(key,item){
				view.addMockTestHome(key,item);
			},
                        updLoadItem : function(item){
				view.updMockTestHome(item);
			},
                        selLoadBegin:function (){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (){
                            view.selLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
