jQuery.extend({
	MockTestDirectionMgr: function(config,model, view){
		var vlist = $.MockTestDirectionVgrListener({
			insMockTestDirection : function(form){
				model.insMockTestDirection(form);
			},
			selMockTestDirection : function(category_id,subcategory_id,section_id){
                            var all = model.selMockTestDirection(category_id,subcategory_id,section_id);
                            if(all){
                                $.each(all, function(item){
                                    view.addMockTestDirection(all[item]);
                                });
                            }
			},
                        delMockTestDirection : function(category_id,subcategory_id,section_id,topic_id){
                                model.delMockTestDirection(category_id,subcategory_id,section_id,topic_id);
			},
                        updMockTestDirection : function(form){
                                model.updMockTestDirection(form);
			},
                        getMockTestDirection : function(category_id,subcategory_id,section_id,topic_id){
                                return model.getMockTestDirection(category_id,subcategory_id,section_id,topic_id);
			},
                        selCategory : function(software_id,module){
                            var all = model.selCategory(software_id,module);
                            if(all){
                                $.each(all, function(item){
                                    view.addCategory(all[item]);
                                });
                            }
			},
                        selSubCategory : function(software_id,category_id,module){
                            var all = model.selSubCategory(software_id,category_id,module);
                            if(all){
                                $.each(all, function(item){
                                    view.addSubCategory(all[item]);
                                });
                            }
			},
                        selSection : function(software_id,category_id,subcategory_id,module){
                            var all = model.selSection(software_id,category_id,subcategory_id,module);
                            if(all){
                                $.each(all, function(item){
                                    view.addSection(all[item]);
                                });
                            }
			},
                        selTopic: function(software_id,category_id,subcategory_id,section_id,module){
                            var all = model.selTopic(software_id,category_id,subcategory_id,section_id,module);
                            if(all){
                                $.each(all, function(item){
                                    view.addTopic(all[item]);
                                });
                            }
			},
			clearAllClicked : function(){
				view.message("clearing data");
				model.clearAll();
			}
		});
		view.addListener(vlist);

            var mlist = $.MockTestDirectionListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
                        
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			},
			loadItem : function(item,module){
                           view.addItem(item,module);
			},
                        updLoadItem : function(item){
				view.updMockTestDirectionRow(item);
			},
                        selLoadBegin:function (module){
                                    view.selLoadBegin(module);
                        },
                        selLoadFail :function (module){
                                    view.selLoadFail(module);
                        },
                        selLoadFinish :function (module){
                            view.selLoadFinish(module);
                        },
                        delLoadBegin:function (){
                            view.delLoadBegin();
                        },
                        delLoadFail :function (){
                            view.delLoadFail();
                        },
                        delLoadFinish :function (){
                            view.delLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
