jQuery.extend({
	RegistrationMgr: function(config,model, view){
		var vlist = $.RegistrationVgrListener({
			insRegister : function(modelform){
				model.insRegister(modelform);
			}
		});
		view.addListener(vlist);

            var mlist = $.RegistrationListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			}
		});
		model.addListener(mlist);
	}
});
