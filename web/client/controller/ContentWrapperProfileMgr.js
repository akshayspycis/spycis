jQuery.extend({
	ContentWrapperProfileMgr: function(config,model, view){
		var vlist = $.ContentWrapperProfileVgrListener({
			selContentWrapperProfile : function(){
                            var all = model.selContentWrapperProfile();
                            
                            if(all){
                                $.each(all, function(item){
                                    view.addContentWrapperProfile(all[item].profile_details);
                                });
                            }
			},
                        updContentWrapperProfile : function(form){
                                model.updContentWrapperProfile(form);
			}
		});
		view.addListener(vlist);

            var mlist = $.ContentWrapperProfileListener({
			loadItem : function(item){
				view.addContentWrapperProfile(item);
			},
                        updLoadItem : function(item){
				view.updContentWrapperProfile(item);
			},
                        selLoadBegin:function (){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (){
                            view.selLoadFinish();
                        },
                        updLoadBegin:function (){
                            view.updLoadBegin();
                        },
                        updLoadFail :function (){
                            view.updLoadFail();
                        },
                        updLoadFinish :function (){
                            view.updLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
