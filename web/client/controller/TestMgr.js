jQuery.extend({
	TestMgr: function(config){
                        var that=this;
			this.loadControlSidebarVgr = function(){
                            if(config.getView("ControlSidebarVgr")==null){
                                $.getScript("../view/ControlSidebarVgr.js").done(function(){
                                    try {
                                        config.setView("ControlSidebarVgr",new $.ControlSidebarVgr(config));
                                        that.loadHeaderTestVgr();
                                    }catch(e){
                                        alert("Error= problem in ControlSidebarVgr.js file "+e);
                                    }
                                }).fail(function() {alert("Error=004 problem in ControlSidebarVgr.js file ");}); 
                            }else{
                                that.loadHeaderTestVgr();
                            }
			}
			this.loadHeaderTestVgr = function(){
                            if(config.getView("HeaderTestVgr")==null){
                                $.getScript("../view/HeaderTestVgr.js").done(function(){
                                    try {
                                        config.setView("HeaderTestVgr",new $.HeaderTestVgr(config));
                                        that.loadMainSidebarTestVgr();
                                    }catch(e){
                                        alert("Error= problem in HeaderTestVgr.js file "+e);
                                    }
                                }).fail(function() {alert("Error=004 problem in HeaderTestVgr.js file ");}); 
                            }else{
                                that.loadMainSidebarTestVgr();
                            }
			}
			this.loadMainSidebarTestVgr = function(){
                            if(config.getView("MainSidebarTestVgr")==null){
                                $.get("../view/MainSidebarTestVgr.js").done(function() {
                                    try {
                                        config.setView("MainSidebarTestVgr",new $.MainSidebarTestVgr(config));
                                        that.loadContentWrapperTestVgr();
                                    }catch(e){
                                        alert("Error=MainSidebarTestVgr.js"+e);
                                    }
                                }).fail(function() {alert("Error=002 MainSidebarTestVgr.js Loading Problem");}); 
                            }else{that.loadContentWrapperTestVgr();
                            }
			}
                        this.loadContentWrapperTestVgr = function(){
                            if(config.getView("ContentWrapperTestVgr")==null){
                                $.get("../view/ContentWrapperTestVgr.js").done(function() {
                                    try {
                                        config.setView("ContentWrapperTestVgr",new $.ContentWrapperTestVgr(config,config.getView("ContentWrapperVgr")));
                                        that.loadFooterTestVgr();
                                    }catch(e){
                                        alert("Error=ContentWrapperTestVgr.js"+e);
                                    }
                                }).fail(function() {alert("Error=ContentWrapperTestVgr.js Loading Problem");}); 
                            }else{
                                that.loadFooterTestVgr();
                            }
			}
                        this.loadFooterTestVgr = function(){
                            if(config.getView("FooterTestVgr")==null){
                                $.get("../view/FooterTestVgr.js").done(function() {
                                    try {
                                        config.setView("FooterTestVgr",new $.FooterTestVgr(config,config.getView("FooterVgr")));
                                        that.loadSectionTestVgr();
                                    }catch(e){
                                        alert("Error=FooterTestVgr.js"+e);
                                    }
                                }).fail(function() {alert("Error=002 FooterTestVgr.js Loading Problem");}); 
                            }else{
                                that.loadSectionTestVgr();
                            }
			}
                        this.loadSectionTestVgr = function(){
                            if(config.getView("SectionTestVgr")==null){
                                $.get("../view/SectionTestVgr.js").done(function() {
                                    try {
                                        config.setView("SectionTestVgr",new $.SectionTestVgr(config));
                                        that.setBody();
                                    }catch(e){
                                        alert("Error=FooterTestVgr.js"+e);
                                    }
                                }).fail(function() {alert("Error=002 FooterTestVgr.js Loading Problem");}); 
                            }else{
                                that.setBody();
                            }
			}
                        this.setBody= function(){
                            config.getView("HeaderVgr").setList(config.getView("HeaderTestVgr").getHeaderTest());
                            config.getView("MainSidebarTestVgr").setMainSidebarTest();
                            config.getView("ContentWrapperTestVgr").setContentWrapperTest();
                            config.getView("FooterTestVgr").setFooter();
                            config.getView("ContentWrapperVgr").showTopBox();
                            $("body").append(config.getView("ControlSidebarVgr").getControlSidebar());
                            $("body").removeClass("sidebar-collapse skin-blue sidebar-mini");
                            $("body").addClass("sidebar-collapse skin-blue sidebar-mini control-sidebar-open");
			}
                        this.setLoading = function (){
                            config.getView("ContentWrapperVgr").removeBoxHeader();
                            config.getView("ContentWrapperVgr").removeBoxbody();
                            config.getView("ContentWrapperVgr").removeBoxFooter();
                            config.getView("ContentWrapperVgr").setBoxBodyContent($("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin")));
                        }
                    }                           
});
