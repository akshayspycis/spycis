jQuery.extend({
	AnsSheetMgr: function(config,model,view){
		var vlist = $.AnsSheetVgrListener({
                    selAnsSheet : function(test_id){
                            var all = model.selAnsSheet(test_id);
                            if(all){
                                $.each(all, function(item){
                                    var keyset=all[item].keyset;
                                    $.map(keyset, function( val, i) {
                                         view.addAnsSheet(all[item]["q"][val],val);
                                    });
                                });
                            }
                    }
                   
		});
            view.addListener(vlist);
            var mlist = $.AnsSheetListener({
			loadItem : function(item,key,module){
                            view.addAnsSheet(item,key);
			},
                        selLoadBegin:function (module){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (module){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (module){
                            view.selLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
