jQuery.extend({
	ChartSheetMgr: function(config,model,view){
		var vlist = $.ChartSheetVgrListener({
                    selChartSheet : function(test_id){
                            var all = model.selChartSheet(test_id);
                            if(all){
                                $.each(all, function(item){
                                    var keyset=all[item].keyset;
                                    $.map(keyset, function( val, i) {
                                         view.addChartSheet(all[item]["q"][val],val);
                                    });
                                });
                            }
                    }
                   
		});
            view.addListener(vlist);
            var mlist = $.ChartSheetListener({
			loadItem : function(data,module){
                            view.addChartSheet(data);
			},
                        selLoadBegin:function (module){
                            view.selLoadBegin();
                        },
                        selLoadFail :function (module){
                            view.selLoadFail();
                        },
                        selLoadFinish :function (module){
                            view.selLoadFinish();
                        }
		});
		model.addListener(mlist);
	}
});
