jQuery.extend({
	FPBoxMgr: function(config,model, view){
		var vlist = $.FPBoxVgrListener({
			insRegister : function(modelform){
				model.insRegister(modelform);
			}
		});
		view.addListener(vlist);

            var mlist = $.FPBoxListener({
			insLoadBegin : function() {
				view.insLoadBegin();
			},
			insLoadFail : function() {
				view.insLoadFail();
			},
			insLoadFinish : function() {
				view.insLoadFinish();
			}
		});
		model.addListener(mlist);
	}
});
