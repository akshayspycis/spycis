jQuery.extend({
	InstrctionMgr: function(config){
                        var that=this;
			this.loadHeaderProfileVgr = function(){
                            if(config.getView("HeaderProfileVgr")==null){
                                $.getScript("../view/HeaderProfileVgr.js").done(function(){
                                    try {
                                        config.setView("HeaderProfileVgr",new $.HeaderProfileVgr(config));
                                        that.loadMainSidebarInstrctionVgr();
                                    }catch(e){
                                        alert("Error= problem in HeaderProfileVgr.js file "+e);
                                    }
                                }).fail(function() {alert("Error=004 problem in HeaderProfileVgr.js file ");}); 
                            }else{
                                
                                that.loadMainSidebarInstrctionVgr();
                            }
			}
			this.loadMainSidebarInstrctionVgr = function(){
                            if(config.getView("MainSidebarInstrctionVgr")==null){
                                $.get("../view/MainSidebarInstrctionVgr.js").done(function() {
                                    try {
                                        config.setView("MainSidebarInstrctionVgr",new $.MainSidebarInstrctionVgr(config));
                                        that.loadContentWrapperInstruncation1EnglishVgr();
                                    }catch(e){
                                        alert("Error=MainSidebarInstrctionVgr.js"+e);
                                    }
                                }).fail(function() {alert("Error=002 MainSidebarInstrctionVgr.js Loading Problem");}); 
                            }else{that.loadContentWrapperInstruncation1EnglishVgr();
                            }
			}
                        this.loadContentWrapperInstruncation1EnglishVgr = function(){
                            if(config.getView("ContentWrapperInstruncation1EnglishVgr")==null){
                                $.get("../view/ContentWrapperInstruncation1EnglishVgr.js").done(function() {
                                    try {
                                        config.setView("ContentWrapperInstruncation1EnglishVgr",new $.ContentWrapperInstruncation1EnglishVgr(config,config.getView("ContentWrapperVgr")));
                                        that.loadFooterProfileVgr();
                                    }catch(e){
                                        alert("Error=ContentWrapperInstruncation1EnglishVgr.js"+e);
                                    }
                                }).fail(function() {alert("Error=ContentWrapperInstruncation1EnglishVgr.js Loading Problem");}); 
                            }else{
                                that.loadFooterProfileVgr();
                            }
			}
                        this.loadContentWrapperInstruncation1HindiVgr = function(){
                            that.setLoading();
                            if(config.getView("ContentWrapperInstruncation1HindiVgr")==null){
                                $.get("../view/ContentWrapperInstruncation1HindiVgr.js").done(function() {
                                    try {
                                        config.setView("ContentWrapperInstruncation1HindiVgr",new $.ContentWrapperInstruncation1HindiVgr(config,config.getView("ContentWrapperVgr")));
                                        config.getView("ContentWrapperInstruncation1HindiVgr").init()            
                                    }catch(e){
                                        alert("Error=ContentWrapperInstruncation1HindiVgr.js"+e);
                                    }
                                }).fail(function() {alert("Error=ContentWrapperInstruncation1HindiVgr.js Loading Problem");}); 
                            }else{
                                config.getView("ContentWrapperInstruncation1HindiVgr").init()            
                            }
			}
                        this.loadContentWrapperInstruncation2EnglishVgr = function(){
                            that.setLoading();
                            if(config.getView("ContentWrapperInstruncation2EnglishVgr")==null){
                                $.get("../view/ContentWrapperInstruncation2EnglishVgr.js").done(function() {
//                                    try {
                                        config.setView("ContentWrapperInstruncation2EnglishVgr",new $.ContentWrapperInstruncation2EnglishVgr(config,config.getView("ContentWrapperVgr")));
                                        config.getView("ContentWrapperInstruncation2EnglishVgr").init()
//                                    }catch(e){
//                                        alert("Error=ContentWrapperInstruncation2EnglishVgr.js"+e);
//                                    }
                                }).fail(function() {alert("Error=002 ContentWrapperInstruncation2EnglishVgr.js Loading Problem");}); 
                            }else{
                                config.getView("ContentWrapperInstruncation2EnglishVgr").init()
                            }
			}
                        this.loadContentWrapperInstruncation2HindiVgr = function(){
                            that.setLoading();
                            if(config.getView("ContentWrapperInstruncation2HindiVgr")==null){
                                $.get("../view/ContentWrapperInstruncation2HindiVgr.js").done(function() {
                                    try {
                                        config.setView("ContentWrapperInstruncation2HindiVgr",new $.ContentWrapperInstruncation2HindiVgr(config,config.getView("ContentWrapperVgr")));
                                        config.getView("ContentWrapperInstruncation2HindiVgr").init()
                                    }catch(e){
                                        alert("Error=ContentWrapperInstruncation2HindiVgr.js"+e);
                                    }
                                }).fail(function() {alert("Error=002 ContentWrapperInstruncation2HindiVgr.js Loading Problem");}); 
                            }else{
                                config.getView("ContentWrapperInstruncation2HindiVgr").init()
                            }
			}
                        this.loadFooterProfileVgr = function(){
                            if(config.getView("FooterProfileVgr")==null){
                                $.get("../view/FooterProfileVgr.js").done(function() {
                                    try {
                                        config.setView("FooterProfileVgr",new $.FooterProfileVgr(config,config.getView("FooterVgr")));
                                        that.setBody();
                                    }catch(e){
                                        alert("Error=FooterProfileVgr.js"+e);
                                    }
                                }).fail(function() {alert("Error=002 FooterProfileVgr.js Loading Problem");}); 
                            }else{
                                that.setBody();
                            }
			}
                        this.setBody= function(){
                            config.getView("HeaderVgr").setList(config.getView("HeaderProfileVgr").getHeaderProfile());
                            config.getView("ContentWrapperVgr").hideTopBox();
                            config.getView("MainSidebarInstrctionVgr").setMainSidebarInstrction();
                            config.getView("ContentWrapperInstruncation1EnglishVgr").init()
                            config.getView("FooterProfileVgr").setFooter();
//                            config.getView("MainVgr").getListener().loadLanguageVgr();
			}
                        this.setLoading = function (){
                            config.getView("ContentWrapperVgr").removeBoxHeader();
                            config.getView("ContentWrapperVgr").removeBoxbody();
                            config.getView("ContentWrapperVgr").removeBoxFooter();
                            config.getView("ContentWrapperVgr").setBoxBodyContent($("<div>").addClass("overlay").append($("<i>").addClass("fa fa-refresh fa-spin")));
                            
                        }
                    }                           
});
