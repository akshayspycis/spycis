jQuery.extend({
	UserRequestMgr: function(config,model, view){
		var vlist = $.UserRequestVgrListener({
			selUserRequest: function(software_id,module){
                            var all = model.selUserRequest(software_id,module);
                            if(all){
                                    view.selLoadBegin();
                                    $.each(all, function(key,value){
                                        view.addUserRequest(value);
                                    });
                                    view.selLoadFinish();
                            }
			},
			sendRequest : function(obj,software_id){
                            model.sendRequest(obj,software_id);
			},
			acceptRequest : function(obj,software_id,user_id,user_request_id){
                            model.acceptRequest (obj,software_id,user_id,user_request_id);
			},
			addExamCircle : function(obj,software_id,user_id){
                            model.addExamCircle(obj,software_id,user_id);
			},
		});
		view.addListener(vlist);

            var mlist = $.UserRequestListener({
			loadItem : function(item){
                            view.addUserRequest(item);
			},
                        selLoadBegin:function (){
                            view.selLoadBegin();
                        },
                        selLoadFail:function (){
                            view.selLoadFail();
                        },
                        selLoadFinish:function (){
                            view.selLoadFinish();
                        },
                        selLoadBegin_accept:function (obj){
                            view.selLoadBegin_accept(obj);
                        },
                        selLoadFail_accept:function (obj){
                            view.selLoadFail_accept(obj);
                        },
                        selLoadFinish_accept:function (obj){
                            view.selLoadFinish_accept(obj);
                        }
		});
		model.addListener(mlist);
	}
});
