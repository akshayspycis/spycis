jQuery.extend({
	Language: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
		function toArray(){
			var item = [];
                            try {
                                $.each(cache,function(j){
                                    sec_item = {}
                                    sec_item ["language_id"] = j
                                    sec_item ["language_name"]= cache[j];
                                    item.push(sec_item);
                                });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json response from an
		 * ajax call
		 */
		function loadResponse(data){
                    $.each(data, function(item){
                        cache[data[item].language_id] = data[item].language_name;
                    });
		}
                
		this.selLanguage = function(module){
                    var outCache = toArray();
                        if(outCache.length) {
                            return outCache;
                        }else{
                            if(typeof(Storage) !== "undefined") {
                                if(localStorage.getItem("lang")!=null){
                                    loadResponse(JSON.parse(localStorage.getItem("lang")).language_details);
                                    var outCache = toArray();
                                    if(module==null){
                                        return 0; 
                                    }else{
                                        return outCache; 
                                    }
                                }
                            } 
                        }
//			if(outCache.length) return outCache;
			$.ajax({
                                url: config.getU()+"/SelLanguageDetails_TestSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {},
				error: function(){
				},
				success: function(data){
                                    if(data.language_details!=""){
                                        if(typeof(Storage) !== "undefined") {
                                            localStorage.setItem("lang",JSON.stringify(data));
                                        }    
                                        loadResponse(data.language_details) ;
                                    }
				}
			});
		}
                
		this.addListener = function(list){
			listeners.push(list);
		}
	
	}
});
