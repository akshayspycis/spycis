jQuery.extend({
	Performanace: function(config){
		var cache = {};
		var that = this;
		var date ;
		var time ;
		var listeners = new Array();
		function toArray(category_id,subcategory_id){
			var item = [];
                            try {
                                    $.each(cache,function(j){
                                                top_item ;
                                                top_item = cache[j];
                                                top_item ["test_id"]= j;
                                                item.push(top_item);
                                    });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json` response from an
		 * ajax call
		 */
		function loadResponse(data){
                    $.each(data, function (personne,value) {
                            cache[personne]=value
                            value["test_id"]=personne;
                            that.testItemLoaded(value); 
                    });
		}
                this.getTestObj = function (test_id){
                    return cache[test_id];
                }
		
		this.selTest = function(){
                    var outCache = toArray();
			if(outCache.length) return outCache;
			that.selLoadBegin();
			$.ajax({
                                url: config.getU()+"/SelPerformanaceSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'user_id':config.getModel("ProfileConfig").getUser_id()},
				error: function(){
                                    that.selLoadFinish();
                                    that.selLoadFail();
				},
				success: function(data){
                                    that.selLoadFinish();
                                    if(data.test_details!="")
                                    loadResponse(data.test_details) ;
				}
			});
		}
		
                
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin();
			});
		}
                this.selLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish();
			});
		}
                this.selLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].selLoadFail();
			});
		}
                this.selTestLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].selTestLoadBegin();
			});
		}
                this.selTestLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].selTestLoadFinish();
			});
		}
                this.selTestLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].selTestLoadFail();
			});
		}
                
		this.testItemLoaded = function(item){
			$.each(listeners, function(i){
				listeners[i].loadItem(item);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	PerformanaceListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        selTestLoadBegin: function() { },
                        selTestLoadFinish: function() { },
                        selTestLoadFail: function() { }
		}, list);
	}
});
