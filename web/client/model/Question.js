jQuery.extend({
	Question: function(config){
		var cache = {};
		var submit_question = {};
		var keyset ;
		var section_details ;
		var keyset_section;
		var that = this;
		var listeners = new Array();
                var section_count={};
                var check_test_submit=true;
                
		function toArray(){
			var item = [];
                            try {
                                    $.each(cache,function(j){
                                                sec_item = {}
                                                sec_item ["question_id"] = cache.question_id;
                                                sec_item ["question_name"]= cache.question_name;
                                                item.push(sec_item);
                                    });
                            }catch(e){}
			return item;
		}
		/**
		 * load a json response from an
		 * ajax call
		 */
		function loadResponse(data,module){
                    that.selLoadFinish();
                    $.each(data, function (personne,value) {
                        cache[personne]=value; //push values here
                    });
                    var question_id=0;
                    $.map(keyset, function( val, i) {
                        if(question_id==0)
                        question_id=val;
                        submit_question[val]={};
                        submit_question[val]["section_id"]=cache[config.getLanguage_id()]["question_section_details"][val];
                        if(Object.keys(section_count).length==0){
                            submit_question[val]["check"]=true;
                        }
                        that.questionItemLoaded(cache[config.getLanguage_id()]["question_details"][val],val,module);
                    });
                    config.getView("ContentWrapperVgr").removeTopBoxContent();
                    $.map(keyset_section, function( personne,value) {
                        obj={};
                        obj["section_id"]=personne;
                        obj["section_name"]=section_details[personne];
                        config.getView("ContentWrapperVgr").setTopBoxContent(config.getView("SectionTestVgr").getUiList(obj)); //push values here
                        config.getView("MainSidebarTestVgr").setSidebarMenu(obj);
                    });
                    if(Object.keys(section_count).length==0){
                        $.each(submit_question, function (k,value) {
                            if(section_count[submit_question[k]["section_id"]]==null){
                                   section_count[submit_question[k]["section_id"]]={};
                                   section_count[submit_question[k]["section_id"]]["count"]=1;
                                   section_count[submit_question[k]["section_id"]]["fisrt"]=k;
                               }else{
                                   section_count[submit_question[k]["section_id"]]["count"]=section_count[submit_question[k]["section_id"]]["count"]+1; 
                               }
                        });
                        if(typeof(Storage) !== "undefined") {
                            localStorage.setItem("section_count",JSON.stringify(section_count));
                        }   
                        $.each(section_count, function (k,value) {
                            config.getView("SectionTestVgr").updateLi(k,"success",0);
                            config.getView("SectionTestVgr").updateLi(k,"notanswer",0);
                            config.getView("SectionTestVgr").updateLi(k,"review",0);
                            config.getView("SectionTestVgr").updateLi(k,"noreview",section_count[k]["count"]);
                            config.getView("MainSidebarTestVgr").updateLi(k,"successa",0);
                            config.getView("MainSidebarTestVgr").updateLi(k,"notanswera",0);
                            config.getView("MainSidebarTestVgr").updateLi(k,"reviewa",0);
                            config.getView("MainSidebarTestVgr").updateLi(k,"noreviewa",section_count[k]["count"]);
                        });
                    }
                    config.setQuestion_id(question_id);
                    var obj=that.loadQuestion(question_id,null);
                    config.getView("ContentWrapperTestVgr").setDirection(obj.direction_name);
                    config.getView("ContentWrapperTestVgr").setQuestion(obj.question_name);
                    config.getView("ContentWrapperTestVgr").setOption(obj.option_name);
                    config.getView("ContentWrapperTestVgr").setQuestionNO($("#"+question_id).find(".btn").attr('vav'));
                    config.getView("ContentWrapperTestVgr").setSectionTitle(config.getModel("Question").getSectionName(question_id));
                    if(that.getQuestion_Submit_Responce(question_id)){
                        that.clearAnsweredButton("");
                    }
                    $.getScript("../../dist/js/jquery.countdownTimer.min.js").done(function() {
                        $("#hm_timer").countdowntimer({
                                minutes : config.getTest_Time(),
                                seconds : 0,
                                size : "lg",
                                timeUp : timeisUp
                            });
                            function timeisUp(){
                                if(check_test_submit){
                                    that.insTest("");
                                }
                            }
                            setInterval(function() {
                                if(config.getTest_Time()!=0){
                                    config.setTest_Time(config.getTest_Time()-1);
                                    if(typeof(Storage) !== "undefined") {
                                        localStorage.setItem("test_time",encodeURIComponent(config.getTest_Time()));
                                    }
                                    if(config.getTest_Time()<4 && !$("#hm_timer").hasClass("blink_me")){
                                        $("#hm_timer").css({'color':'#d0e8a1'});
                                        $("#hm_timer").addClass("blink_me");
                                    }
                                 }else{
                                    return false;
                                }
                            }, 60*1000);
                    }).fail(function() {alert("Error:002");}); 
		}
                
                this.loadQuestion =function (question_id,language_id){
                    if(question_id==null){
                       question_id=config.getQuestion_id();
                    }
                    if(language_id==null){
                       language_id=config.getLanguage_id();
                    }
                    var obj=cache[language_id];
                    question = {}
                    question ["direction_id"] = Object.keys(obj["question_details"][question_id]);
                    question ["question_id"] = question_id;
                    question ["question_name"]=decodeURIComponent(obj["question_details"][question_id][question.direction_id]);
                    question ["direction_name"]= decodeURIComponent(obj["discription_details"][question.direction_id]);
                    question ["option_name"]= {}
                    $.each(obj["option_details"][question_id], function (key,value) {
                        question ["option_name"][key]=decodeURIComponent(value);
                    });
                    return question;
                }
                
                
		this.selQuestion = function(module){
                    that.selLoadBegin();
                    var outCache = toArray();
                        if(outCache.length) {
                            return outCache;
                        }else{
                            if(typeof(Storage) !== "undefined") {
                                if(localStorage.getItem("question_details")!=null){
                                    config.setActive_Time(localStorage.getItem("active_time").replace("%3A", ":"));
                                    config.setTest_Date(localStorage.getItem("test_date"));
                                    keyset=JSON.parse(localStorage.getItem("keyset"));
                                    section_details=JSON.parse(localStorage.getItem("section_details"));
                                    keyset_section=JSON.parse(localStorage.getItem("keyset_section"));
                                    section_count=JSON.parse(localStorage.getItem("section_count"));
                                    loadResponse(JSON.parse(localStorage.getItem("question_details")).q);
                                    if(localStorage.getItem("submit_question")!=null){
                                        submit_question=JSON.parse(localStorage.getItem("submit_question"));
                                        $.map( keyset, function( k, iaa) {
                                               if(submit_question[k]["ans"]!=null &&submit_question[k]["status"]==true){
                                                   config.getView("ControlSidebarVgr").setAnsweredButton(k,"");
                                               }else if(submit_question[k]["ans"]!=null && submit_question[k]["status"]==false){
                                                   config.getView("ControlSidebarVgr").setMarkedAnsweredButton(k,"");
                                               }else if(submit_question[k]["ans"]==null && submit_question[k]["status"]==false){
                                                    config.getView("ControlSidebarVgr").setNotMarkedAnsweredButton(k,"");
                                               }else if(Object.keys(submit_question[k]).length>2){
                                                   config.getView("ControlSidebarVgr").setNotAnsweredButton(k,"");
                                               }
                                        });
                                        that.autoSetListCount();
                                    }
                                    if(module==null){
                                        return 0; 
                                    }else{
                                        return outCache; 
                                    }
                                }
                            } 
                        }
			if(outCache.length) return outCache;
			$.ajax({
                                url: config.getU()+"/SelQuestionDetailsSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'test_id':config.getTest_id()},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish(module);
                                    if(typeof(Storage) !== "undefined") {
                                            localStorage.setItem("question_details",JSON.stringify(data));
                                            localStorage.setItem("keyset",JSON.stringify(data.keyset));
                                            localStorage.setItem("section_details",JSON.stringify(data.section_details));
                                            localStorage.setItem("keyset_section",JSON.stringify(data.keyset_section));
                                    }    
                                    keyset=data.keyset;
                                    section_details=data.section_details;
                                    keyset_section=data.keyset_section;
                                    loadResponse(data.q,module) ;
				}
			});
		}
                this.insTestResult =function (obj){
                        that.insLoadBegin();
                        var jsonobj=JSON.stringify(obj);
                        $.ajax({
                            url: config.getU()+"/InsTestResultDetailsSvr",
                            type: 'POST',
                            data: jsonobj,
                            success: function(data)
                            {
                                alert("Your Test Submit Successfully.");
                                check_test_submit=false;
                                that.insLoadFinish();
                                that.loadProfile();
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFinish();
                                that.insLoadFail();
                            }
                        });
                }
                //in instest argument name is cnfrm for identify test is submit either form button or auto submit
                this.insTest=function (cnfrm){
                    var obj={};
                    obj["user_id"]=config.getModel("ProfileConfig").getUser_id();
                    obj["test_id"]=config.getTest_id();
                    obj["active_time"]=config.getActive_Time();
                    obj["leave_time"]=config.getTest_Time();
                    obj["test_date"]=config.getTest_Date();
                    var subm={};
                    $.each(submit_question, function (k) {
                        if(submit_question[k]["ans"]!=null &&submit_question[k]["status"]==true){
                            subm[k]=submit_question[k]["ans"];
                        }
                    });
                    obj["question_details"]=subm;
                    if(cnfrm=="ok"&&confirm("Do you want to submit Test ?")){
                        that.insTestResult(obj);
                    }
                    if(cnfrm!="ok"){
                        that.insTestResult(obj);
                    }
                    
                }
                this.insLoadBegin=function (){
                    config.getView("ControlSidebarVgr").setTestSubmit_begin();
                }
                this.insLoadFinish=function (){
                    config.getView("ControlSidebarVgr").setTestSubmit_finish();
                }
                this.loadProfile=function (){
                    $("body").find(".control-sidebar").empty();
                    $("body").removeClass();
                    $("body").addClass("sidebar-collapse skin-blue sidebar-mini");
                    config.getModel("ProfileConfig").removeLocalStorage();
                    config.getView("MainVgr").setProfile();
                }
                this.insLoadFail=function (){
                    config.getView("ControlSidebarVgr").setTestSubmit_fail();
                }
                this.setAns=function (ans){
                    if(ans!=null){
                        submit_question[config.getQuestion_id()]["ans"]=ans;
                        submit_question[config.getQuestion_id()]["status"]=true;
                        that.upSection();
                        config.getView("ControlSidebarVgr").setAnsweredButton(config.getQuestion_id());
                    }else{
                        config.getView("ContentWrapperTestVgr").setSelectedOption(submit_question[config.getQuestion_id()]["ans"]);
                    }
                    if(typeof(Storage) !== "undefined") {
                        localStorage.setItem("submit_question",JSON.stringify(submit_question));
                    }    
                }
                
                this.clearAnsweredButton=function (a){
                    submit_question[config.getQuestion_id()]["ans"]=null;
                    submit_question[config.getQuestion_id()]["status"]=null;
                    config.getView("ContentWrapperTestVgr").clearAnswered();
                    that.upSection();
                    submit_question[config.getQuestion_id()]["check"]=false;
                    config.getView("ControlSidebarVgr").setNotAnsweredButton(config.getQuestion_id(),a);
                }
                
                this.reviewAns=function (ans){
                    submit_question[config.getQuestion_id()]["ans"]=ans;
                    submit_question[config.getQuestion_id()]["status"]=true;
                    that.upSection();
                    config.getView("ControlSidebarVgr").setMarkedAnsweredButton(config.getQuestion_id());
                }
                this.reviewNotMark=function (){
                    submit_question[config.getQuestion_id()]["ans"]=null;
                    submit_question[config.getQuestion_id()]["status"]=false;
                    that.upSection();
                    config.getView("ControlSidebarVgr").setNotMarkedAnsweredButton(config.getQuestion_id());
                }
                this.setQuestion= function (section_id){
                    config.getView("ControlSidebarVgr").setQusByList(section_count[section_id]["fisrt"]);
                }
                this.getQuestion_Submit_Responce= function (k){
                        var b=true;
                        if(submit_question[k]["ans"]!=null &&submit_question[k]["status"]==true){
                                   b=false;
                        }else if(submit_question[k]["ans"]!=null && submit_question[k]["status"]==false){
                                   b=false;
                        }else if(submit_question[k]["ans"]==null && submit_question[k]["status"]==false){
                                    b=false;
                        }else if(Object.keys(submit_question[k]).length>2){
                            b=false;
                        }
                        return b;
                }
                this.upSection = function (){
                    var section_id=submit_question[config.getQuestion_id()]["section_id"];
//                    var ans=0;
//                    var not=0;
//                    var mark=0;
//                    var nomakr=0;
                    if(section_count[section_id]["count"]!=0 && submit_question[config.getQuestion_id()]["check"]){
                        
                        section_count[section_id]["count"]=section_count[section_id]["count"]-1;
//                        nomakr=section_count[section_id]["count"];
                        if(typeof(Storage) !== "undefined") {
                            localStorage.setItem("section_count",JSON.stringify(section_count));
                        }   
                    }
                    that.autoSetListCount();
//                    $.each(submit_question, function (k,value) {
//                        if(section_id==submit_question[k]["section_id"]){
//                           if(submit_question[k]["ans"]!=null &&submit_question[k]["status"]==true){
//                               ans++;
//                           }else if(submit_question[k]["ans"]!=null && submit_question[k]["status"]==false){
//                               mark++;
//                           }else if(submit_question[k]["ans"]==null && submit_question[k]["status"]==false){
//                                mark++;
//                           }else if(Object.keys(submit_question[k]).length>2){
//                                           not++;
//                           }
//                        }
//                    });
//                        config.getView("SectionTestVgr").updateLi(section_id,"success",ans);
//                                config.getView("SectionTestVgr").updateLi(section_id,"notanswer",not);
//                                config.getView("SectionTestVgr").updateLi(section_id,"review",mark);
//                                config.getView("SectionTestVgr").updateLi(section_id,"noreview",nomakr);
//                                config.getView("MainSidebarTestVgr").updateLi(section_id,"successa",ans);
//                                config.getView("MainSidebarTestVgr").updateLi(section_id,"notanswera",not);
//                                config.getView("MainSidebarTestVgr").updateLi(section_id,"reviewa",mark);
//                                config.getView("MainSidebarTestVgr").updateLi(section_id,"noreviewa",nomakr);
                }
                this.autoSetListCount = function (){
                    $.each(section_details, function (k,i) {
                        
                            var section_id=k;
                            var ans=0;
                            var not=0;
                            var mark=0;
                            var nomakr=section_count[section_id]["count"];
                               $.each(submit_question, function (k,value) {
                                    if(section_id==submit_question[k]["section_id"]){
                                       if(submit_question[k]["ans"]!=null &&submit_question[k]["status"]==true){
                                           ans++;
                                       }else if(submit_question[k]["ans"]!=null && submit_question[k]["status"]==false){
                                           mark++;
                                       }else if(submit_question[k]["ans"]==null && submit_question[k]["status"]==false){
                                            mark++;
                                       }else if(Object.keys(submit_question[k]).length>2){
                                           not++;
                                       }
                                    }
                                });
                                config.getView("SectionTestVgr").updateLi(section_id,"success",ans);
                                config.getView("SectionTestVgr").updateLi(section_id,"notanswer",not);
                                config.getView("SectionTestVgr").updateLi(section_id,"review",mark);
                                config.getView("SectionTestVgr").updateLi(section_id,"noreview",nomakr);
                                config.getView("MainSidebarTestVgr").updateLi(section_id,"successa",ans);
                                config.getView("MainSidebarTestVgr").updateLi(section_id,"notanswera",not);
                                config.getView("MainSidebarTestVgr").updateLi(section_id,"reviewa",mark);
                                config.getView("MainSidebarTestVgr").updateLi(section_id,"noreviewa",nomakr);
                        });
                    
                }
                this.getSectionName = function (question_id){
                    return section_details[submit_question[question_id]["section_id"]];
                }
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                
		this.questionItemLoaded = function(item,key,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,key,module);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	QuestionListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { }
		}, list);
	}
});
