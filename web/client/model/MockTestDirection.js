jQuery.extend({
	MockTestDirection: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
                
		function toArrayCat(){
                    var item = [];
                        try {
                            if(Object.keys(cache).length>0){
                                if(typeof(Storage) !== "undefined") {
                                    if(localStorage.getItem("cache_csst")!=null && localStorage.getItem("flag")!=null && localStorage.getItem("flag")=="true"){
                                        cache=JSON.parse(localStorage.getItem("cache_csst"));
                                    }
                                }
                            }else{
                                localStorage.setItem("flag",false);
                            }
                            $.each(cache,function(key,value){
                                    top_item = {}
                                    top_item ["category_id"] = key;
                                    top_item ["category_name"]= value.category_name;
                                    item.push(top_item);
                            });
                        }catch(e){}
                    return item;
		}
                
		function toArraySub(category_id){
			var item = [];
                            try {
                                if(cache[category_id]["subcategory_details"]!=null && Object.keys(cache[category_id]["subcategory_details"]).length>0){
                                    if(typeof(Storage) !== "undefined") {
                                        if(localStorage.getItem("cache_csst")!=null && localStorage.getItem("flag")!=null && localStorage.getItem("flag")==true){
                                            cache=JSON.parse(localStorage.getItem("cache_csst"));
                                        }
                                    }
                                }else{
                                    if(typeof(Storage) !== "undefined" && localStorage.getItem("cache_csst")!=null) {
                                        cache=JSON.parse(localStorage.getItem("cache_csst"));
                                    }
                                    localStorage.setItem("flag",false);
                                }
                                $.each(cache[category_id]["subcategory_details"],function(key,value){
                                        top_item = {}
                                        top_item ["subcategory_id"] = key;
                                        top_item ["subcategory_name"]= value.subcategory_name;
                                        item.push(top_item);
                                });
                            }catch(e){}
			return item;
		}
                
		function toArraySec(category_id,subcategory_id){
			var item = [];
                            try {
                                if(cache[category_id]["subcategory_details"][subcategory_id]["section_details"]!=null && Object.keys(cache[category_id]["subcategory_details"][subcategory_id]["section_details"]).length>0){
                                    if(typeof(Storage) !== "undefined") {
                                        if(localStorage.getItem("cache_csst")!=null && localStorage.getItem("flag")!=null && localStorage.getItem("flag")==true){
                                            cache=JSON.parse(localStorage.getItem("cache_csst"));
                                        }
                                    }
                                }else{
                                    if(typeof(Storage) !== "undefined" && localStorage.getItem("cache_csst")!=null) {
                                        cache=JSON.parse(localStorage.getItem("cache_csst"));
                                    }
                                    localStorage.setItem("flag",false);
                                }
                                $.each(cache[category_id]["subcategory_details"][subcategory_id]["section_details"],function(key,value){
                                        top_item = {}
                                        top_item ["section_id"] = key;
                                        top_item ["section_name"]= value.section_name;
                                        item.push(top_item);
                                });
                            }catch(e){}
			return item;
		}
                
		function toArrayTop(category_id,subcategory_id,section_id){
			var item = [];
                            try {
                                if(cache[category_id]["subcategory_details"][subcategory_id]["section_details"][section_id]["topic_details"]!=null && Object.keys(cache[category_id]["subcategory_details"][subcategory_id]["section_details"][section_id]["topic_details"]).length>0){
                                    if(typeof(Storage) !== "undefined") {
                                        if(localStorage.getItem("cache_csst")!=null && localStorage.getItem("flag")!=null && localStorage.getItem("flag")==true){
                                            cache=JSON.parse(localStorage.getItem("cache_csst"));
                                        }
                                    }
                                }else{
                                    if(typeof(Storage) !== "undefined" && localStorage.getItem("cache_csst")!=null) {
                                        cache=JSON.parse(localStorage.getItem("cache_csst"));
                                    }
                                    localStorage.setItem("flag",false);
                                }
                                $.each(cache[category_id]["subcategory_details"][subcategory_id]["section_details"][section_id]["topic_details"],function(key,value){
                                        top_item = {}
                                        top_item ["topic_id"] = key;
                                        top_item ["topic_name"]= value.topic_name;
                                        item.push(top_item);
                                });
                            }catch(e){}
			return item;
                        
		}
		/**
		 * load a json` response from an
		 * ajax call
		 */
		function loadResponseCat(data,module){
                    $.each(data,function(key,value){
                        if(cache[key]==null){
                            cache[key]={}
                        }
                        cache[key]=value
                        that.addItem(value,module);
                    });
                    if(typeof(Storage) !== "undefined") {
                        localStorage.setItem("flag","true");
                        localStorage.setItem("cache_csst",JSON.stringify(cache));
                    }
		}
		function loadResponseSub(category_id,data,module){
                    $.each(data,function(key,value){
                        if(cache[category_id]["subcategory_details"]==null){
                            cache[category_id]["subcategory_details"]={}
                        }
                        cache[category_id]["subcategory_details"][key]=value
                        that.addItem(value,module);
                    });
                    if(typeof(Storage) !== "undefined") {
                        localStorage.setItem("flag","true");
                        localStorage.setItem("cache_csst",JSON.stringify(cache));
                    }
		}
                
		function loadResponseSec(category_id,subcategory_id,data,module){
                    $.each(data,function(key,value){
                        if(cache[category_id]["subcategory_details"][subcategory_id]["section_details"]==null){
                            cache[category_id]["subcategory_details"][subcategory_id]["section_details"]={}
                        }
                        cache[category_id]["subcategory_details"][subcategory_id]["section_details"][key]=value
                        that.addItem(value,module);
                    });
                    if(typeof(Storage) !== "undefined") {
                        localStorage.setItem("flag","true");
                        localStorage.setItem("cache_csst",JSON.stringify(cache));
                    }
		}
                
		function loadResponseTop(category_id,subcategory_id,section_id,data,module){
                    $.each(data["topic_details"],function(key,value){
                        if(cache[category_id]["subcategory_details"][subcategory_id]["section_details"][section_id]["topic_details"]==null){
                           cache[category_id]["subcategory_details"][subcategory_id]["section_details"][section_id]["topic_details"]={}
                        }
                        cache[category_id]["subcategory_details"][subcategory_id]["section_details"][section_id]["topic_details"][key]=value
                        that.addItem(value,module);
                    });
                    if(typeof(Storage) !== "undefined") {
                        localStorage.setItem("flag","true");
                        localStorage.setItem("cache_csst",JSON.stringify(cache));
                    }
		}
                
                this.insMockTestDirection =function (form){
                        that.insLoadBegin();
                        $.each(form["language_details"],function(i){
                                    $.each(form["language_details"][i]["question_details"],function(j){
                                        form["language_details"][i]["question_details"][j]["img_details"]=null;
                                    });
                        });
                        var jsonobj=JSON.stringify(form);
                        $.ajax({
                            url: config.getU()+"/InsMockTestDirectionDetailsSvr",
                            type: 'POST',
                            data: jsonobj,
                            success: function(data)
                            {
                                if(data.trim()=="true"){
                                    that.insLoadFinish();
                                }else{
                                    that.insLoadFail();
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFinish();
                                //$("#topic_form")[0].reset();
                                that.insLoadFail();
                            }
                        });
                }
                
                this.selCategory = function(software_id,module){
                    var outCache = toArrayCat();
			if(outCache.length) return outCache;
    			that.selLoadBegin(module);
			$.ajax({
                                url:config.getU()+ "/SelMocktestFavCategorySvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'software_id':software_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    loadResponseCat(data,module);
                                    that.selLoadFinish(module);
				}
			});
		}
                
                this.selSubCategory = function(software_id,category_id,module){
                    var outCache = toArraySub(category_id);
			if(outCache.length) return outCache;
    			that.selLoadBegin(module);
			$.ajax({
                                url:config.getU()+ "/SelMocktestFavSubCategorySvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'software_id':software_id,'category_id':category_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    loadResponseSub(category_id,data,module);
                                    that.selLoadFinish(module);
				}
			});
		}
                
                this.selSection = function(software_id,category_id,subcategory_id,module){
                    var outCache = toArraySec(category_id,subcategory_id);
			if(outCache.length) return outCache;
    			that.selLoadBegin(module);
			$.ajax({
                                url:config.getU()+ "/SelMocktestFavSectionSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'software_id':software_id,'category_id':category_id,'subcategory_id':subcategory_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    loadResponseSec(category_id,subcategory_id,data,module);
                                    that.selLoadFinish(module);
				}
			});
		}
                
                this.selTopic = function(software_id,category_id,subcategory_id,section_id,module){
                    var outCache = toArrayTop(category_id,subcategory_id,section_id);
			if(outCache.length) return outCache;
    			that.selLoadBegin(module);
			$.ajax({
                                url:config.getU()+ "/SelTopicDetailsSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'category_id':category_id,'subcategory_id':subcategory_id,'section_id':section_id},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    loadResponseTop(category_id,subcategory_id,section_id,data,module);
                                    that.selLoadFinish(module);
				}
			});
		}
                
                this.clearAll = function(){
			cache = new Array();
		}
                
		this.addListener = function(list){
			listeners.push(list);
		}
                
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                

		/**
		 * we're done loading, tell everyone
		 */
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                
                this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
                
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                
                this.delLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].delLoadFail();
			});
		}
                
		this.addItem= function(item,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,module);
			});
		}
                
                
	
	},
	/**
	 * let people create listeners easily
	 */
	MockTestDirectionListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			loadItem : function() { },
                        updLoadItem: function() { },
			insLoadFail : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { },
                        delLoadBegin: function() { },
                        delLoadFinish: function() { },
                        delLoadFail: function() { },
                        updLoadBegin: function() { },
                        updLoadFinish: function() { },
                        updLoadFail: function() { }

		}, list);
	}
});
