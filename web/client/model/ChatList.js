jQuery.extend({
	ChatList: function(config){
		var cache = {};
		var user_list = {};
		var key_container_receiver = {};
		var that = this;
		var listeners = new Array();
                navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
		function toArray(test_id){
			var item = [];
                            try {
                                if(cache[test_id]!=undefined)
                                item.push(cache[test_id]);
                            }catch(e){}
			return item;
		}
		
		this.loadResponse=function(user_id,data){
                    cache[user_id]=data;
                    if(user_id!=config.getModel("ProfileConfig").getUser_id()){
                        var conn = config.getPeer().connect(data.keyid);
                        key_container_receiver[data.keyid]={};
                        key_container_receiver[data.keyid]["conn"]=conn;
                        key_container_receiver[data.keyid]["user_id"]=user_id;
                    }
		}
                this.sendMessage = function (obj){
                    
                }
                this.receiveMessage = function (obj){
                    var user_id=obj.sender_id;
                    switch(obj.subject){
                        case 'create_node_remote':
                            that.receiveNodeMessage(obj);
                        break;    
                        case 'Message':
                            console.log("remote = "+config.getModel("ChatList").getChatBoxObject(user_id,"chack_user_left") + " = " +config.getModel("ChatList").getChatBoxObject(user_id,"chack_user_left_type")+" = "+config.getModel("ChatList").getChatBoxObject(user_id,"chack_user_right"))
                            if(config.getModel("ChatList").getChatBoxObject(user_id,"direct_chat_messages")==null){
                                config.getView("ChatListVgr").autoClickChatBox(user_id,cache[user_id]);
                                var setinter=setInterval(function (){
                                    if(config.getView("ChatPanelVgr")!=null){
                                        config.getView("ChatPanelVgr").setDirectChatMsgLeft(obj,obj.body);
                                        clearInterval(setinter);
                                    }
                                },1000)
                            }else{
                                config.getView("ChatPanelVgr").setDirectChatMsgLeft(obj,obj.body);
                            }
                        break;    
                        case 'chat_img_typing_focus_in':
                            if(config.getView("ChatPanelVgr")!=null){
                                config.getView("ChatPanelVgr").setTypingMsgLeft(obj);
                            }
                        break;    
                        case 'chat_img_typing_focus_out':
                            if(config.getView("ChatPanelVgr")!=null){
                                
                                if(!config.getModel("ChatList").getChatBoxObject(obj.sender_id,"chack_user_left")){
                                    if(config.getModel("ChatList").getChatBoxObject(obj.sender_id,"chack_user_right")){
                                        if(!config.getModel("ChatList").getChatBoxObject(obj.sender_id,"chack_user_left_type")){
                                            config.getModel("ChatList").getChatBoxObject(obj.sender_id,"left_chat_msg").find("div:last-child").remove();
                                            config.getModel("ChatList").setChatBoxObject(obj.sender_id,"chack_user_left_type",true);
                                        }
                                    }else{
                                        config.getModel("ChatList").getChatBoxObject(obj.sender_id,"left_chat_msg").remove();
                                        config.getModel("ChatList").setChatBoxObject(obj.sender_id,"chack_user_left",true);           
                                        config.getModel("ChatList").setChatBoxObject(obj.sender_id,"chack_user_left_type",true);           
                                    }
                                }
                            }
                        break;    
                        default:
                        break;    
                    }
                }
                
                
                this.receiveNodeMessage = function (obj){
                    if(cache[obj.user_id]==null){
                        cache[obj.user_id]=obj;
                        config.getView("ChatListVgr").receiveNode(obj.user_id,obj);
                    }
                }
                this.sendNodeMessage = function (obj,key){
                    obj['subject']="create_node_remote";
                    key_container_receiver[key]["conn"].send(JSON.stringify(obj));
                }
                this.callVideo = function (obj,key){
                    config.getPeer().call(cache[obj.user_id].keyid, window.localStream);
                }
                config.getPeer().on('connection', function(c) { 
                    $.each(c,function (key,value){
                                if(key=="peer"){
                                    if(key_container_receiver[value]==null){
                                        var conn = config.getPeer().connect(value);
                                        key_container_receiver[value]={};
                                        key_container_receiver[value]["conn"]=conn;
                                    }else{
                                        key_container_receiver[value]["status"]=true;
                                        setTimeout(function (){
                                            that.sendNodeMessage(that.getUserDetails(config.getModel("ProfileConfig").getUser_id()),value);
                                        },5000)
                                    }
                                }
                    });
                    c.on('data', function(data){
                        that.receiveMessage(JSON.parse(data))
                    });
                    
                 });
                 config.getPeer().on('call', function(call){
                  // Answer the call automatically (instead of prompting user) for demo purposes
                  call.answer(window.localStream);
                  step3(call);
                });
                this.getUserConn = function(keyid){
                    try{
                        return key_container_receiver[keyid]["conn"];
                    }catch(e){
                        return null;
                    }
                }
                this.getUserDetails = function(user_id){
                    return cache[user_id];
                }
                
		this.selChatList = function(test_id,lang_id,module){
                    var outCache = toArray(test_id);
			if(outCache.length) return outCache;
                        that.selLoadBegin();
			$.ajax({
                                url: config.getU()+"/SelChatListSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'test_id':test_id,'user_id':config.getModel("ProfileConfig").getUser_id()},
				error: function(){
                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish(module);
                                    loadResponse(data,test_id,lang_id,module) ;
				}
			});
		}
                
                this.setChatBoxObject = function (user_id,key,obj){
                    cache[user_id][key]=obj;
                }
                this.removeChatBoxObject = function (user_id,key){
                    delete cache[user_id][key];
                }
                this.getChatBoxObject = function (user_id,key){
                    try{
                        return cache[user_id][key];
                    }catch(e){
                        return null;
                    }
                }
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                this.selLoadFail = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(module);
			});
		}
                
		this.questionItemLoaded = function(item,key,module){
			$.each(listeners, function(i){
				listeners[i].loadItem(item,key,module);
			});
		}
	
	},
	/**
	 * let people create listeners easily
 	*/
	ChatListListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { }
		}, list);
	}
});
