jQuery.extend({
	UploadImg: function(obja,config){
                this.up_obj = obja;
                var items = "";
                this.all = {}
                var that = this;
		var listeners = new Array();
                var fileinput = document.getElementById(this.up_obj.multiUpload);
                var max_width = fileinput.getAttribute('data-maxwidth');
                var max_height = fileinput.getAttribute('data-maxheight');
                var form = document.getElementById(this.up_obj.form);
		/**
		 * get contents of cache into an array
		 */
                this._init = function(){
                    if (window.File && window.FileReader && window.FileList && window.Blob) {		
                             var inputId = $("#"+that.up_obj.form).find("input[type='file']").eq(0).attr("id");
                             document.getElementById(inputId).addEventListener("change", that._read, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("dragover", function(e){ e.stopPropagation(); e.preventDefault(); }, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("drop", that._dropFiles, false);
                             document.getElementById(that.up_obj.form).addEventListener("submit", that._submit, false);
                    } else console.log("Browser supports failed");
                    
                }
                
                $("#"+this.up_obj.dragArea).click(function() {
                    fileinput.click();
                });
                
                this._submit = function(e){
                    e.stopPropagation(); 
                    e.preventDefault();
                    that._startUpload();
            	}
                
                this._preview = function(data){
                    this.items = data;
                    if(this.items.length > 0 && this.items.length <10){
                        var html;		
                        var uId = "";
                        for(var i = 0; i<this.items.length; i++){
                            uId = this.items[i].name._unique();
                            obj={};
                            obj["file"]=this.items[i];
                            that.all[uId]=obj;
                              $("#"+this.up_obj.dragArea).empty();
                              $("#"+this.up_obj.dragArea).append($("<center>").css("height","130px").append(config.getLoadingData().css({'font-size': '40px','position':'relative','top':'35%'})));
                                
                                that.readfiles(this.items[i],uId)
                            }
                            }else{
                                alert("Image file select limit maximum is 10 files.")
                            }
                        }
                        
                var item_file; 
                var ssid;
                this.loadResizeableImageCss = function (item,sid){
                    ssid=sid;
                    item_file=item; 
                    $("<link/>", {
                       rel: "stylesheet",
                       type: "text/css",
                       href: "../../a/css/component.css",
                       onload: that.callbeck,
                    }).appendTo("head");
                }        
                this.getImgCropper=function (){
                    return $("<div>").addClass("component").append($("<div>").addClass("overlay_crop_tool"))
                            .append(item_file)
                }
                this.callbeck=function (){
                    $.getScript("../../admin/view/ErrorMsgVgr.js").done(function(){
                            try {
                                        config.setView("ErrorMsgVgr",new $.ErrorMsgVgr(config));
                                        $("#error_msg").remove();
                                        var modela = config.getView("ErrorMsgVgr").getErrorMsg("Crop Your Image",that.getImgCropper(),"submit",that.callCrop_In_ResizeableImage);
                                        $("body").append(modela);
                                        //$("#"+this.up_obj.dragArea).empty();
                                        
                                        modela.modal({
                                                backdrop: 'static',
                                                keyboard: true, 
                                                show: true
                                        });
                                        modela.find(".modal-header").find(".close").remove();
                                        modela.find(".modal-footer").find("#cancel").remove();
                                        $.getScript("../model/ResizeableImage.js").done(function(){
                                        try {
                                                config.setModel("ResizeableImage",new $.ResizeableImage(config,item_file));
                                        }catch(e){
                                            alert("Error:004 problem in ResizeableImage.js file "+e);
                                        }
                                        }).fail(function() {alert("Error:004 problem in ResizeableImage.js file ");}); 
                                        
                                }catch(e){
                                        alert("Error:004 problem in ModelVgr.js file "+e);
                                }
                                }).fail(function() {alert("Error:004 problem in ModelVgr.js file ");}); 
                    
                }
                
                this.callCrop_In_ResizeableImage = function (){
                    var crop_canvas=config.getModel("ResizeableImage").crop();
                    that.all[ssid]["byte_stream"]=crop_canvas.toDataURL("image/jpeg",0.7);
                    var img = new Image;
                    img.src = that.all[ssid]["byte_stream"];
                    config.getModel("ProfileConfig").setProfilePic(crop_canvas);
                    img.width="164";
                    $("#"+that.up_obj.dragArea).empty();
                    $("#"+that.up_obj.dragArea).append($("<center>").append(img));
                    return true;
                }        
                        
                this._refresh =function (){
                    $("#"+this.up_obj.dragArea).append(html);
                    this.all={};
                }        
                        
                this.setStream = function(stream,sid){
                    that.all[sid]["byte_stream"]=stream;
                    //$(".dfiles[rel='"+sid+"'] >h5>img").remove();
                }
        
                this._read = function(evt){
                        if(evt.target.files){
                            that._preview(evt.target.files);
                            //that._preview(evt.target.files);
                        } else 
                            console.log("Failed file reading");
                }
	
                this._validate = function(format){
                        var arr = this.up_obj.support.split(",");
                        return arr.indexOf(format);
                }
	
                this._dropFiles = function(e){
                        e.stopPropagation(); 
                        e.preventDefault();
                        that._preview(e.dataTransfer.files);
                }
                
                this._deleteFiles = function(key){
                    that.all={};
                    $("#"+this.up_obj.dragArea).empty();
                    $("#"+this.up_obj.dragArea)
                                    .append($('<center><i class="fa fa-user" style="font-size: 130px; padding-left:5px;"></i></center>'))
                                    .append($('<input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload1" style="width: 0px; height: 0px; overflow: hidden;">'));
                }
                
                
                this._uploader = function(file,key){
                    if(typeof file != undefined && that._validate(file["file"].type) > 0){
			$.ajax({
				type:"POST",
                                url:config.getU()+"/InsProfilePicSvr",
				data:{'images':file["byte_stream"],'user_id':config.getModel("ProfileConfig").getUser_id()},
				success:function(url){
                                      config.loadMain();
				},
                                error:function (xhr, ajaxOptions, thrownError) {
                                    alert(xhr+ajaxOptions+thrownError)
                                    that.insLoadFail();
                                }
			});
                    }else {
                        alert("Invalid file format - "+file.name);
                    }
                }
                
                this._startUpload = function(){
                    $.each(that.all,function(key,value){
                        that._uploader(value,key);
                    });
                } 
        
                String.prototype._unique = function(){
                        return this.replace(/[a-zA-Z]/g, function(c){
                            return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
                        });
                }
                
                this.processfile =function(file,sid) {
            if( !( /image/i ).test( file.type )){
                alert( "File "+ file.name +" is not an image." );
                return false;
            }
            // read the files
              var reader = new FileReader();
              reader.readAsArrayBuffer(file);
              reader.onload = function (event) {
              var blob = new Blob([event.target.result]); // create blob...
              window.URL = window.URL || window.webkitURL;
              var blobURL = window.URL.createObjectURL(blob); // and get it's URL
              var image = new Image();
              image.src = blobURL;
              image.onload = function() {
              that.setStream(that.resizeMe(image,sid),sid)
              }
            };
        }

                this.readfiles=function(files,sid) {
                    that.processfile(files,sid); // process each file at once
                }

                this.resizeMe=function (img,sid) {
           var canvas = document.createElement('canvas');
           var width = img.width;
           var height = img.height;
          // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                  //height *= max_width / width;
                  height = Math.round(height *= max_width / width);
                  width = max_width;
                }
            } else {
                if (height > max_height) {
                  //width *= max_height / height;
                  width = Math.round(width *= max_height / height);
                  height = max_height;
                }
            }
  
             // resize the canvas and draw the image data into it
              canvas.width = width;
              canvas.height = height;
              var ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0, width, height);
              var canvas1 = document.createElement('canvas');
              canvas1.width = 50;
              canvas1.height = 50;
              var ctx = canvas1.getContext("2d");
              ctx.drawImage(img, 0, 0, 50, 50);
//              that.all[sid]["image"]=canvas1;
              var canvas2 = document.createElement('canvas');
              canvas2.width =50;
              canvas2.height = 50;
              var ctx = canvas2.getContext("2d");
              ctx.drawImage(img, 0, 0, 50, 50);
              
              var img =new Image();
              img.src = canvas.toDataURL();
//              $("#"+this.up_obj.dragArea).append($("<center>").append(img));
//              config.getModel("ProfileConfig").setProfilePic(canvas2);
              
             // preview.appendChild(canvas1); // do the actual resized preview
              that.loadResizeableImageCss(img,sid);
              return canvas.toDataURL("image/jpeg",0.7); // get the data from canvas as 70% JPG (can be also PNG, etc.)

        }
        
                this._init();
        }
});
