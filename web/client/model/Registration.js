jQuery.extend({
	Registration: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
                
                var user_name =false;
                var dob =false;
                //.................................this field check on server
                var email =false;
                var contact_no =false;
                //.................................
                var password =false;
                var captcha_value;
                var checkbox;
                //................................................................set captch details
                
                
                var captch_sum; 
                //..
                this.setUser_name =function (value){
                    user_name=value;
                }
                this.getUser_name =function (){
                    return user_name;
                }
                this.setDob =function (value){
                    dob=value;
                }
                this.getDob =function (){
                    return dob;
                }
                this.setEmail =function (value){
                    email=value;
                }
                this.getEmail =function (){
                    return email;
                }
                this.setContact_no =function (value){
                    contact_no=value;
                }
                this.getContact_no =function (){
                    return contact_no;
                }
                this.setPassword =function (value){
                    password=value;
                }
                this.getPassword =function (){
                    return password;
                }
                this.setCaptch_value=function (value){
                    captcha_value=value;
                }
                this.getCaptch_value =function (){
                    return captcha_value;
                }
                this.setCheckbox=function (value){
                    checkbox=value;
                }
                this.getCheckbox =function (){
                    return checkbox;
                }
                
                this.setValidation = function (){
                        $("#email").focusout(function (){
                            setTimeout(function (){
                                if(that.getEmail()){
                                    obj={}
                                    obj['url']=config.getU()+"/CheckEmailSvr";
                                    obj['value']=$("#email").val();
                                    obj['set']=that.setEmailResponse;
                                    obj['element']=$("#email");
                                    that.checkSqlValidation(obj);
                                }
                                },100);
                        });
                        $("#contact_no").focusout(function (){
                            setTimeout(function (){
                        
                                if(that.getContact_no()){
                                    obj={}
                                    obj['url']=config.getU()+"/CheckContactSvr";
                                    obj['value']=$("#contact_no").val();
                                    obj['set']=that.setContactResponse;
                                    obj['element']=$("#contact_no");
                                    that.checkSqlValidation(obj);
                                }
                                },100);
                        });
                        $("#password").focusout(function (){
                            setTimeout(function (){
                                if(that.getPassword()){
                                    obj={}
                                    obj['url']=config.getU()+"/SqlInjection_PasswordSvr";
                                    obj['value']=$("#password").val();
                                    obj['set']=that.setPasswordResponse;
                                    obj['element']=$("#password");
                                    that.checkSqlValidation(obj);
                                }
                            },100);
                        });
                        $("#checkbox").focusout(function (){
                            if (($("#checkbox:checked").length)<=0) {
                               that.setCheckbox(false);
                               $("#error").empty();
                               $("#error").append("You must be check terms and condition.");    
                               $("#error").css({'display':'block','color':'red'});    
                           }else{
                               that.setCheckbox(true);
                               $("#error").empty();   
                            }
                        });
                        $("#dob").focusout(function (){
                            if ($("#dob").val()=="") {
                               that.setDob(false);
                               $("#error").empty();
                               $("#error").append("Please set your Date of Birth.");    
                               $("#error").css({'display':'block','color':'red'});    
                           }else{
                               that.setDob(true);
                               $("#error").empty();   
                            }
                        });
                    $.validator.addMethod("user_name_sql", function(value, element) {
                        if((/(\%27)|(\')|(\")|(\-\-)|(\%23)|(#)|(=)/i.test(value))){
                            that.setUser_name(false);
                            return false;
                        }else{
                            that.setUser_name(true);
                            return true;
                        }
                    }, "Your username contain sql injection charector (',(,),#,%,--,-,=).");
                    $.validator.addMethod("captcha_check", function(value, element) {
                        if(captch_sum!=parseInt(value.trim())){
                            that.setCaptch_value(false);
                            return false;
                        }else{
                            that.setCaptch_value(true);
                            return true;
                        }
                    }, "The characters you entered didn't match the word verification. Please try again.");
                    $.validator.addMethod("password_sql", function(value, element) {
                         return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
                                   && /[a-z]/.test(value) // has a lowercase letter
                                   && /\d/.test(value) // has a digit
                    }, "The password should meet some minimum requirements:<br>"+
                        "one no space between character<br>"+
                        "at least one lower-case character<br>"+
                        "at least one digit");
                    config.getView("RegistrationBoxVgr").getForm().validate({
                        rules: {
                            user_name: {
                                minlength: 2,
                                required: true,
                                maxlength: 50,
                                user_name_sql: true,
                            },
                            password: {
                                required: true,
                                minlength: 6,
                                maxlength: 50,
                                password_sql:true
                            },
                            repassword: {
                              required: true,
                              maxlength: 50,
                              minlength: 6,
                              equalTo: "#password"
                            },
                            email: {
                                required: true,
                                minlength: 6,
                                maxlength: 50,
                            },
                            contact_no: {
                                number: true,
                                required: true,
                                minlength: 10,
                                maxlength: 10,
                            },
                            captcha: {
                                number: true,
                                captcha_check:true
                            },
                        
                        agree: "required"
                        },
                        messages: {
                        user_name: {
                            required: "Please enter a username",
                            maxlength:"Your username must consist of max length 50 characters",
                            minlength: "Your username must consist of at least 2 characters"
                        },
                        password: {
                            required: "Please provide a password",
                            maxlength:"Your password must consist of max length 50 characters",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        contact_no: {
                            required: "Please provide a Contact.",
                            minlength: "Your Contact must be at least 10 characters long",
                            maxlength:"Your Contact must consist of max length 10 characters"
                        },
                        repassword: {
                            required: "Please provide a confirm password",
                            minlength: "Your password must be at least 5 characters long",
                            maxlength:"Your password must consist of max length 50 characters",
                            equalTo: "Please enter the same password as above"
                        },
                        email: {
                            required: "Please provide a Valid email",
                            maxlength:"Your email must consist of max length 50 characters",
                            minlength: "Your email must be at least 6 characters long with @ and other hosting postfix.  "
                        },
                        
                    },
                        errorElement : 'label',
                        errorLabelContainer: '#error',
                        highlight: function(element, errorClass) {
                            $("#error").empty();
                           $(element).parent().removeClass("has-warning");    
                           $(element).parent().removeClass("has-success");    
                           $(element).parent().addClass("has-error");    
                           switch($(element).attr('id')){
                               case "email":
                               that.setEmail(false);
                               case "contact_no":
                               that.setContact_no(false);
                               case "password":
                               that.setPassword(false);
                               case "repassword":
                               that.setPassword(false);
                           }
                        },
                        unhighlight: function(element, errorClass) {
                           $(element).parent().removeClass("has-warning");    
                           $(element).parent().removeClass("has-error");    
                           $(element).parent().addClass("has-success");    
                           switch($(element).attr('id')){
                               case "email":
                               that.setEmail(true);
                               case "contact_no":
                               that.setContact_no(true);
                               case "password":
                               that.setPassword(true);
                               case "repassword":
                               that.setPassword(true);
                           }
                        }, 
                        success: function(element) {
                           
                        }
                    });
                    if(config.getController("RegistrationMgr")==null){
                        $.getScript("../controller/RegistrationMgr.js").done(function() {
                                    try {
                                        config.setController("RegistrationMgr",new $.RegistrationMgr(config,config.getModel("Registration"),config.getView("RegistrationBoxVgr")));
                                    }catch(e){
                                        alert(e);
                                    }
                        }).fail(function() {alert("Error :006 problem in registrationMgr.js file ");}); 
                    }
                    
                }
                
		this.checkSqlValidation = function (obj){
			$.ajax({
                                url: obj.url,
                                type: 'POST',
                                data: {'data':obj.value},
				success: function(data){
                                    obj.set(JSON.parse(data),obj.element);
				}
			});
                }
                
                this.setEmailResponse = function (data,element){
                    var a1;
                    var a2;
                    $.each(data, function(key,value) {
                        if(key=="1"){
                            a1=value;
                        }else{
                            a2=value;
                        }
                    });
                    if(a1){
                       if(a2){
                           that.setEmail(false);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-success");    
                           element.parent().addClass("has-error");    
                           $("#error").empty();
                           $("#error").append("This Email Address already registered.");    
                           $("#error").css({'display':'block','color':'red'});    
                       }else{
                           that.setEmail(true);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-success");    
                           $("#error").empty();
                       }
                    }else{
                           that.setEmail(false);   
                           element.parent().removeClass("has-success");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-warning");
                           $("#error").empty();
                           $("#error").append("Your email contain sql injection charector.");    
                           $("#error").css({'display':'block','color':'orange'});    
                    }
                }
                
                this.setContactResponse = function (data,element){
                    var a1;
                    var a2;
                    $.each(data, function(key,value) {
                        if(key=="1"){
                            a1=value;
                        }else{
                            a2=value;
                        }
                    });
                    
                    if(a1){
                       if(a2){
                           that.setContact_no(false);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-success");    
                           element.parent().addClass("has-error");    
                           $("#error").empty();
                           $("#error").append("This Contact No already registered.");    
                           $("#error").css({'display':'block','color':'red'});    
                       }else{
                           that.setContact_no(true);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-success");    
                           $("#error").empty();
                       }
                    }else{
                           that.setContact_no(false);   
                           element.parent().removeClass("has-success");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-warning");
                           $("#error").empty();
                           $("#error").append("Your Contact contain sql injection charector.");    
                           $("#error").css({'display':'block','color':'orange'});    
                    }
                }
                
                this.setPasswordResponse = function (data,element){
                    var a1;
                    $.each(data, function(key,value) {
                        if(key=="1"){
                            a1=value;
                        }
                    });
                       if(!a1){
                           that.setPassword(false);   
                           element.parent().removeClass("has-success");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-warning");
                           $("#error").empty();
                           $("#error").append("Your Password contain sql injection charector.");    
                           $("#error").css({'display':'block','color':'orange'});    
                        }else{
                           that.setPassword(true);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-success");    
                           $("#error").empty();
                        }
                }
                
                this.setCaptcha = function (element){
                    var a= Math.floor(Math.random() * 10);
                    var b= Math.floor(Math.random() * 10);
                    captch_sum=a+b;
                    element.find("#h1_img").empty();
                    element.find("#h1_img").append(a+"+"+b);
                }
                
                this.insRegister = function (form){
                    if(that.checkValidation()){
                        user_details ={}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="user_name"){
                                user_details["user_name"]= $(this).val();
                            }
                            if($(this).attr("name")=="dob"){
                                user_details["dob"]= $(this).val();
                            }
                            if($(this).attr("name")=="email"){
                                user_details["email"]= $(this).val();
                            }
                            if($(this).attr("name")=="contact_no"){
                                user_details["contact_no"]= $(this).val();
                            }
                            if($(this).attr("name")=="password"){
                                user_details["password"]= $(this).val();
                            }
                        });
                        that.insRegistration(JSON.stringify(user_details));
                    }
                }
                
                this.insRegistration =function (user_details){
                        that.insLoadBegin();
                        $.ajax({
                            url: config.getU()+"/InsRegistrationDetailsSvr",
                            type: 'POST',
                            data: user_details,
                            success: function(data)
                            {
                                if(data.trim()=="error"){
                                    that.insLoadFail();
                                }else{
                                    var obj = jQuery.parseJSON(data);
                                    config.getModel("ProfileConfig").setUserName(obj.user_name);
                                    config.getModel("ProfileConfig").setUser_id(obj.user_id);
                                    config.loadProfilePicBoxVgr(obj);
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFail();
                            }
                        });
                }
                
                this.checkValidation = function (){
                    if(!that.getUser_name()){
                        that.showError("Please enter a username")
                        return false;
                    }
                    if(!that.getEmail()){
                        that.showError("Please enter a valid email address.")
                        return false;
                    }
                    if(!that.getDob()){
                        that.showError("Please set your Date of Birth.")
                        return false;
                    }
                    if(!that.getContact_no()){
                        that.showError("Please provide a Contact..")
                        return false;
                    }
                    if(!that.getPassword()){
                        that.showError("Please provide a password.")
                        return false;
                    }
                    if(!that.getCaptch_value()){
                        that.showError("Please provide a Security Check.")
                        return false;
                    }
                    if(!that.getCheckbox()){
                        that.showError("You must be check terms and condition.")
                        return false;
                    }
                    return true;
                }
                
                this.showError = function (error){
                   $("#error").empty();
                   $("#error").append(error);    
                   $("#error").css({'display':'block','color':'red'});    
                }
                
		/**
		 * add a listener to this model
		 */
                
		this.addListener = function(list){
			listeners.push(list);
		}
		
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
                
		/**
		 * tell everyone the item we've loaded
		 */
	
	},
	/**
	 * let people create listeners easily
	 */
	RegistrationListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			insLoadFail : function() { }
		}, list);
	}
});
