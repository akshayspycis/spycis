jQuery.extend({
	UserRequest: function(config){
		var cache = {};
		var cache_request_user_list= {};
		var that = this;
		var listeners = new Array();
                var check_test_submit=true;
                
		function toArray(term){
			var item = {};
                            try {
                                var value=null;;
                                $.each(cache,function (key,v){
                                    if(key.toUpperCase().startsWith(term.toUpperCase())){
                                       value= v;
                                       $.each(value,function (s,v){
                                            if(value[s]["status"]=="false"){
                                                value[s]["status"]=cache_software_list[s]['status'];
                                            }
                                        });
                                        return value;
                                    }
                                });
                                return value;
                            }catch(e){return null;}
		}
		
		function loadResponse(data,term){
                    $.each(data, function( key,value) {
                        cache_request_user_list[key]=value;
                        that.itemLoaded(value);
                    });
		}
                function updateCache_software_list(software_id){
                    cache_software_list[software_id]['status']="true";
                }
		this.selUserRequest = function(software_id,module){
                    that.selLoadBegin();
//                    var outCache = toArray();
//			if(outCache!=null) return outCache;
			$.ajax({
                                url: config.getU()+"/SelUserRequestSvr",
                                type: 'POST',
                                dataType: 'json',
                                data: {'software_id':software_id},
				error: function(){
//                                    that.selLoadFinish(module);
                                    that.selLoadFail(module);
				},
				success: function(data){
                                    that.selLoadFinish(module);
                                    loadResponse(data);
                                    
				}
			});
		}
		this.acceptRequest= function(obj,software_id,user_id,user_request_id){
                    that.selLoadBegin_accept(obj);
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/InsUserInPublicCircleSvr",
                        data: {'user_id':config.getModel("ProfileConfig").getUser_id(),'software_id':software_id,'user_request_id':user_request_id}, // serializes the form's elements.
                        success: function(data)
                        {
                            if(data.trim()=="true"){
                                that.selLoadFinish_accept(obj);
                                //updateCache_software_list(software_id);
                            }else
                                that.selLoadFail_accept(obj);
                            },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.selLoadFail_accept(obj);
                        }
                    });
		}
		this.addExamCircle= function(obj,software_id,user_id){
                    that.selLoadBegin_accept(obj);
                    $.ajax({
                        type: "POST",
                        url: config.getU()+"/InsUserInTestCircleSvr",
                        data: {'user_id':config.getModel("ProfileConfig").getUser_id(),'software_id':software_id}, // serializes the form's elements.
                        success: function(data)
                        {
                            if(data.trim()=="true"){
                                that.selLoadFinish_accept(obj);
                                //updateCache_software_list(software_id);
                            }else
                                that.selLoadFail_accept(obj);
                            },
                        error:function (xhr, ajaxOptions, thrownError) {
                            that.selLoadFail_accept(obj);
                        }
                    });
		}
                
                this.getSectionName = function (question_id){
                    return section_details[submit_question[question_id]["section_id"]];
                }
		this.addListener = function(list){
			listeners.push(list);
		}
		/**
		 * notify everone that we're starting 
		 * to load some data
		 */
		
                this.selLoadBegin = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin(module);
			});
		}
                this.selLoadFinish = function(module){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish(module);
			});
		}
                 this.selLoadFail = function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadFail(obj);
			});
		}
                this.selLoadFail_In_grp = function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadFail_In_grp(obj);
			});
		}
                
                this.selLoadBegin_In_grp = function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin_In_grp(obj);
			});
		}
                this.selLoadFinish_In_grp = function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish_In_grp(obj);
			});
		}
                this.selLoadFail_accept= function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadFail_accept(obj);
			});
		}
                
                this.selLoadBegin_accept= function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadBegin_accept(obj);
			});
		}
                this.selLoadFinish_accept= function(obj){
			$.each(listeners, function(i){
				listeners[i].selLoadFinish_accept(obj);
			});
		}
               
                
		this.itemLoaded = function(value){
			$.each(listeners, function(i){
				listeners[i].loadItem(value);
			});
		}
	
	},
	/**
	 * let people create listeners easily
	 */
	UserRequestListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadItem : function() { },
                        selLoadBegin: function() { },
                        selLoadFinish: function() { },
                        selLoadFail: function() { }
		}, list);
	}
});
