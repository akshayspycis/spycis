jQuery.extend({
	UserProfile: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
                
                var user_name =true;
                var dob =true;
                //.................................this field check on server
                var email =true;
                var contact_no =true;
                //.................................
                this.setUser_name =function (value){
                    user_name=value;
                }
                this.getUser_name =function (){
                    return user_name;
                }
                this.setDob =function (value){
                    dob=value;
                }
                this.getDob =function (){
                    return dob;
                }
                this.setEmail =function (value){
                    email=value;
                }
                this.getEmail =function (){
                    return email;
                }
                this.setContact_no =function (value){
                    contact_no=value;
                }
                this.getContact_no =function (){
                    return contact_no;
                }
                
                this.setValidation = function (){
                        $("#email").focusout(function (){
                            setTimeout(function (){
                                if(that.getEmail()){
                                    obj={}
                                    obj['url']=config.getU()+"/CheckEmailSvr";
                                    obj['value']=$("#email").val();
                                    obj['set']=that.setEmailResponse;
                                    obj['element']=$("#email");
                                    if(obj['value']!=config.getModel("ContentWrapperProfile").getValue("email")){
                                        that.checkSqlValidation(obj);
                                    }
                                }
                                },100);
                        });
                        $("#contact_no").focusout(function (){
                            setTimeout(function (){
                        
                                if(that.getContact_no()){
                                    obj={}
                                    obj['url']=config.getU()+"/CheckContactSvr";
                                    obj['value']=$("#contact_no").val();
                                    obj['set']=that.setContactResponse;
                                    obj['element']=$("#contact_no");
                                    if(obj['value']!=config.getModel("ContentWrapperProfile").getValue("contact_no")){
                                        that.checkSqlValidation(obj);
                                    }
                                }
                                },100);
                        });
                        $("#dob").focusout(function (){
                            if ($("#dob").val()=="") {
                               that.setDob(false);
                               $("#error").empty();
                               $("#error").append("Please set your Date of Birth.");    
                               $("#error").css({'display':'block','color':'red'});    
                           }else{
                               that.setDob(true);
                               $("#error").empty();   
                            }
                        });
                    $.validator.addMethod("user_name_sql", function(value, element) {
                        if((/(\%27)|(\')|(\")|(\-\-)|(\%23)|(#)|(=)/i.test(value))){
                            that.setUser_name(false);
                            return false;
                        }else{
                            that.setUser_name(true);
                            return true;
                        }
                    }, "Your username contain sql injection charector (',(,),#,%,--,-,=).");
                    config.getView("ContentWrapperProfileVgr").getUserDetails_model().validate({
                        rules: {
                            user_name: {
                                minlength: 2,
                                required: true,
                                maxlength: 50,
                                user_name_sql: true,
                            },
                            email: {
                                required: true,
                                minlength: 6,
                                maxlength: 50,
                            },
                            contact_no: {
                                number: true,
                                required: true,
                                minlength: 10,
                                maxlength: 10,
                            },
                        agree: "required"
                        },
                        messages: {
                        user_name: {
                            required: "Please enter a username",
                            maxlength:"Your username must consist of max length 50 characters",
                            minlength: "Your username must consist of at least 2 characters"
                        },
                        contact_no: {
                            required: "Please provide a Contact.",
                            minlength: "Your Contact must be at least 10 characters long",
                            maxlength:"Your Contact must consist of max length 10 characters"
                        },
                        email: {
                            required: "Please provide a Valid email",
                            maxlength:"Your email must consist of max length 50 characters",
                            minlength: "Your email must be at least 6 characters long with @ and other hosting postfix.  "
                        },
                        
                    },
                        errorElement : 'label',
                        errorLabelContainer: '#error',
                        highlight: function(element, errorClass) {
                            $("#error").empty();
                           $(element).parent().removeClass("has-warning");    
                           $(element).parent().removeClass("has-success");    
                           $(element).parent().addClass("has-error");    
                           switch($(element).attr('id')){
                               case "email":
                               that.setEmail(false);
                               case "contact_no":
                               that.setContact_no(false);
                           }
                        },
                        unhighlight: function(element, errorClass) {
                           $(element).parent().removeClass("has-warning");    
                           $(element).parent().removeClass("has-error");    
                           $(element).parent().addClass("has-success");    
                           switch($(element).attr('id')){
                               case "email":
                               that.setEmail(true);
                               case "contact_no":
                               that.setContact_no(true);
                           }
                        }, 
                        success: function(element) {
                           
                        }
                    });
                    if(config.getController("UserProfileMgr")==null){
                        $.getScript("../controller/UserProfileMgr.js").done(function() {
                            try {
                                config.setController("UserProfileMgr",new $.UserProfileMgr(config,config.getModel("UserProfile"),config.getView("ContentWrapperProfileVgr")));
                            }catch(e){
                                alert(e);
                            }
                        }).fail(function() {alert("Error :006 problem in registrationMgr.js file ");}); 
                    }
                    
                }
                
		this.checkSqlValidation = function (obj){
			$.ajax({
                                url: obj.url,
                                type: 'POST',
                                data: {'data':obj.value},
				success: function(data){
                                    obj.set(JSON.parse(data),obj.element);
				}
			});
                }
                
                this.setEmailResponse = function (data,element){
                    var a1;
                    var a2;
                    $.each(data, function(key,value) {
                        if(key=="1"){
                            a1=value;
                        }else{
                            a2=value;
                        }
                    });
                    if(a1){
                       if(a2){
                           that.setEmail(false);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-success");    
                           element.parent().addClass("has-error");    
                           $("#error").empty();
                           $("#error").append("This Email Address already registered.");    
                           $("#error").css({'display':'block','color':'red'});    
                       }else{
                           that.setEmail(true);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-success");    
                           $("#error").empty();
                       }
                    }else{
                           that.setEmail(false);   
                           element.parent().removeClass("has-success");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-warning");
                           $("#error").empty();
                           $("#error").append("Your email contain sql injection charector.");    
                           $("#error").css({'display':'block','color':'orange'});    
                    }
                }
                
                this.setContactResponse = function (data,element){
                    var a1;
                    var a2;
                    $.each(data, function(key,value) {
                        if(key=="1"){
                            a1=value;
                        }else{
                            a2=value;
                        }
                    });
                    
                    if(a1){
                       if(a2){
                           that.setContact_no(false);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-success");    
                           element.parent().addClass("has-error");    
                           $("#error").empty();
                           $("#error").append("This Contact No already registered.");    
                           $("#error").css({'display':'block','color':'red'});    
                       }else{
                           that.setContact_no(true);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-success");    
                           $("#error").empty();
                       }
                    }else{
                           that.setContact_no(false);   
                           element.parent().removeClass("has-success");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-warning");
                           $("#error").empty();
                           $("#error").append("Your Contact contain sql injection charector.");    
                           $("#error").css({'display':'block','color':'orange'});    
                    }
                }
                
                this.updUserProfile = function (form){
                    if(that.checkValidation()){
                        user_details ={}
                        user_details["user_id"]= config.getModel("ProfileConfig").getUser_id();
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="user_name"){
                                user_details["user_name"]= $(this).val();
                                    var obj = jQuery.parseJSON(decodeURIComponent(localStorage.getItem("usaassassaesras_sasasdaseastaasiasls")));
                                    obj["user_name"]=user_details["user_name"];
                                    localStorage.setItem("usaassassaesras_sasasdaseastaasiasls",encodeURIComponent(JSON.stringify(obj)));
                                    config.getModel("ContentWrapperProfile").updValue("user_name",user_details["user_name"]);
                                    config.getModel("ProfileConfig").setUserName(user_details["user_name"]);
                                    $(".profile-username").empty();
                                    $(".profile-username").append(user_details["user_name"]);
                                    $(".nav").find(".user-menu").find(".dropdown-toggle").find(".hidden-xs").empty();
                                    $(".nav").find(".user-menu").find(".dropdown-toggle").find(".hidden-xs").append(user_details["user_name"]);
                                    $(".nav").find(".user-menu").find(".dropdown-menu").find(".user-header").find("p").empty();
                                    $(".nav").find(".user-menu").find(".dropdown-menu").find(".user-header").find("p").append(user_details["user_name"]).append($("<small>").append("Candidate Id&nbsp;&nbsp;V"+user_details["user_id"]));
                                    
                            }
                            if($(this).attr("name")=="dob"){
                                var d=$(this).val().split('-');
                                var dob=d[2]+"-"+d[1]+"-"+d[0]
                                user_details["dob"]= dob;
                                    config.getModel("ContentWrapperProfile").updValue("dob",user_details["dob"]);
                                    config.getView("ContentWrapperProfileVgr").getUl_list().find("li:eq(0)").find("a").empty();
                                    config.getView("ContentWrapperProfileVgr").getUl_list().find("li:eq(0)").find("a").append("&nbsp;&nbsp;&nbsp;"+user_details["dob"]);
                            }
                            if($(this).attr("name")=="email"){
                                user_details["email"]= $(this).val();
                                config.getModel("ContentWrapperProfile").updValue("dob",user_details["email"]);
                                    config.getView("ContentWrapperProfileVgr").getUl_list().find("li:eq(1)").find("a").empty();
                                    config.getView("ContentWrapperProfileVgr").getUl_list().find("li:eq(1)").find("a").append("&nbsp;&nbsp;&nbsp;"+user_details["email"]);
                            }
                            if($(this).attr("name")=="contact_no"){
                                user_details["contact_no"]= $(this).val();
                                config.getModel("ContentWrapperProfile").updValue("dob",user_details["contact_no"]);
                                    config.getView("ContentWrapperProfileVgr").getUl_list().find("li:eq(2)").find("a").empty();
                                    config.getView("ContentWrapperProfileVgr").getUl_list().find("li:eq(2)").find("a").append("&nbsp;&nbsp;&nbsp;"+user_details["contact_no"]);
                            }
                        });
                        
                        that.updUser(JSON.stringify(user_details));
                    }
                }
                
                this.updUser =function (user_details){
                    that.updLoadBegin();
                        $.ajax({
                            url: config.getU()+"/UpdUserProfileDetailsSvr",
                            type: 'POST',
                            data: user_details,
                            success: function(data)
                            {
                                if(data.trim()=="error"){
                                    that.updLoadFail();
                                }else{
                                    that.updLoadFinish();
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.updLoadFail();
                            }
                        });
                }
                
                this.checkValidation = function (){
                    if(!that.getUser_name()){
                        that.showError("Please enter a username")
                        return false;
                    }
                    if(!that.getEmail()){
                        that.showError("Please enter a valid email address.")
                        return false;
                    }
                    if(!that.getDob()){
                        that.showError("Please set your Date of Birth.")
                        return false;
                    }
                    if(!that.getContact_no()){
                        that.showError("Please provide a Contact..")
                        return false;
                    }
                    return true;
                }
                
                this.showError = function (error){
                   $("#error").empty();
                   $("#error").append(error);    
                   $("#error").css({'display':'block','color':'red'});    
                }
                
		/**
		 * add a listener to this model
		 */
                
		this.addListener = function(list){
			listeners.push(list);
		}
		
		this.updLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadBegin();
			});
		}
                
		this.updLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFinish();
			});
		}
                
		this.updLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].updLoadFail();
			});
		}
                
                
		/**
		 * tell everyone the item we've loaded
		 */
	
	},
	/**
	 * let people create listeners easily
	 */
	UserProfileListener: function(list) {
		if(!list) list = {};
		return $.extend({
			updLoadBegin : function() { },
			updLoadFinish : function() { },
			updLoadFail : function() { }
		}, list);
	}
});
