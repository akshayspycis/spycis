jQuery.extend({
	FPBox: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
                
                var email =false;
                this.setEmail =function (value){
                    email=value;
                }
                this.getEmail =function (){
                    return email;
                }
                this.setValidation = function (){
                        $("#email").focusout(function (){
                            setTimeout(function (){
                                if(that.getEmail()){
                                    obj={}
                                    obj['url']=config.getU()+"/CheckEmailSvr";
                                    obj['value']=$("#email").val();
                                    obj['set']=that.setEmailResponse;
                                    obj['element']=$("#email");
                                    that.checkSqlValidation(obj);
                                }
                                },100);
                        });
                    config.getView("FPBoxVgr").getForm().validate({
                        rules: {
                            email: {
                                required: true,
                                minlength: 6,
                                maxlength: 50,
                            },
                        agree: "required"
                        },
                        messages: {
                        email: {
                            required: "Please provide a Valid email",
                            maxlength:"Your email must consist of max length 50 characters",
                            minlength: "Your email must be at least 6 characters long with @ and other hosting postfix.  "
                        },
                        
                    },
                        errorElement : 'label',
                        errorLabelContainer: '#error',
                        highlight: function(element, errorClass) {
                            $("#error").empty();
                           $(element).parent().removeClass("has-warning");    
                           $(element).parent().removeClass("has-success");    
                           $(element).parent().addClass("has-error");    
                           switch($(element).attr('id')){
                               case "email":
                               that.setEmail(false);
                           }
                        },
                        unhighlight: function(element, errorClass) {
                           $(element).parent().removeClass("has-warning");    
                           $(element).parent().removeClass("has-error");    
                           $(element).parent().addClass("has-success");    
                           switch($(element).attr('id')){
                               case "email":
                               that.setEmail(true);
                           }
                        }, 
                        success: function(element) {
                           
                        }
                    });
                    if(config.getController("FPBoxMgr")==null){
                        $.getScript("../controller/FPBoxMgr.js").done(function() {
                                    try {
                                        config.setController("FPBoxMgr",new $.FPBoxMgr(config,config.getModel("FPBox"),config.getView("FPBoxVgr")));
                                    }catch(e){
                                        alert(e);
                                    }
                        }).fail(function() {alert("Error :006 problem in registrationMgr.js file ");}); 
                    }
                    
                }
                
		this.checkSqlValidation = function (obj){
			$.ajax({
                                url: obj.url,
                                type: 'POST',
                                data: {'data':obj.value},
				success: function(data){
                                    obj.set(JSON.parse(data),obj.element);
				}
			});
                }
                
                this.setEmailResponse = function (data,element){
                    var a1;
                    var a2;
                    $.each(data, function(key,value) {
                        if(key=="1"){
                            a1=value;
                        }else{
                            a2=value;
                        }
                    });
                    if(a1){
                       if(a2){
                           that.setEmail(true);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-success");    
                           $("#error").empty();
                       }else{
                           that.setEmail(false);
                           element.parent().removeClass("has-warning");    
                           element.parent().removeClass("has-success");    
                           element.parent().addClass("has-error");    
                           $("#error").empty();
                           $("#error").append("This Email Address Does not registered.");    
                           $("#error").css({'display':'block','color':'red'});    
                       }
                    }else{
                           that.setEmail(false);   
                           element.parent().removeClass("has-success");    
                           element.parent().removeClass("has-error");    
                           element.parent().addClass("has-warning");
                           $("#error").empty();
                           $("#error").append("Your email contain sql injection charector.");    
                           $("#error").css({'display':'block','color':'orange'});    
                    }
                }
                
                
                this.insRegister = function (form){
                    if(that.checkValidation()){
                        user_details ={}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="email"){
                                user_details["email"]= $(this).val();
                            }
                        });
                        if(that.checkValidation()){
                            that.insFPBox(JSON.stringify(user_details));
                        }
                        
                    }
                }
                
                this.insFPBox =function (user_details){
                        that.insLoadBegin();
                        $.ajax({
                            url: config.getU()+"/ForgetPasswordSvr",
                            type: 'POST',
                            data: user_details,
                            success: function(data)
                            {
                                
                                if(data.trim()=="true"){
                                     that.insLoadFinish();
                                }else{
                                    that.insLoadFail();
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFail();
                            }
                        });
                }
                
                this.checkValidation = function (){
                    if(!that.getEmail()){
                        that.showError("Please enter a valid email address.")
                        return false;
                    }
                    return true;
                }
                
                this.showError = function (error){
                   $("#error").empty();
                   $("#error").append(error);    
                   $("#error").css({'display':'block','color':'red'});    
                }
                
		/**
		 * add a listener to this model
		 */
                
		this.addListener = function(list){
			listeners.push(list);
		}
		
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
                
		/**
		 * tell everyone the item we've loaded
		 */
	
	},
	/**
	 * let people create listeners easily
	 */
	FPBoxListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			insLoadFail : function() { }
		}, list);
	}
});
