jQuery.extend({
	ProfileConfig: function(config){
		var listeners = new Array();
                var FirstName ;
                var profile_pic;
                var that=this;
                var user_id;
                var software_details;
                var contact_no;
                
                
                this.getProfilePic= function (clas){
                    if(jQuery.type(profile_pic)=="object"){
                        return $("<img/>").attr({'src':profile_pic.toDataURL()}).addClass(clas);
                    }else{
                        var profilepic = $('<img />', { 
                            src: profile_pic,
                            alt: 'User Image'
                        }).on('error', function() { $(this).attr({'src':'../../dist/img/avatar.png'}) });
                        profilepic.addClass(clas);
                        return profilepic;
                    }
                }
                this.setProfilePic= function (canvas){
                    if(canvas==undefined){
                        profile_pic='../../dist/img/avatar.png'
                    }else{
                        profile_pic= canvas;    
                    }
                    
                }
                
//                this.getProfilePicheader= function (){
//                    var profile_pic = $('<img />', { 
//                        src: '../../dist/img/user2-160x160.jpg',
//                        alt: 'User Image'
//                    });
//                    profile_pic.addClass("user-image")
//                    return profile_pic;
//                }
                
                this.getContact_no =function (){
                    return contact_no;
                }
                this.setContact_no =function (value){
                    contact_no=value;
                }
                this.getSoftwareDetails =function (){
                    return software_details;
                }
                this.setSoftwareDetails =function (value){
                    software_details=value;
                }
                this.getFirstName =function (){
                    return FirstName;
                }
                this.setUserName =function (value){
                    FirstName=value;
                }
                this.getUser_id =function (){
                    return user_id;
                }
                this.setUser_id =function (value){
                    user_id=value;
                }
                
                this.checkSession = function (){
                       $.ajax({
                                url: config.getU()+"/CheckSessionSvr",
                                type: 'POST',
				error: function(){
                                    config.loadLoginBoxVgr();
				},
				success: function(data){    
                                    if(data.trim()=="nonoas90asnaksjkasjjasasdjajksdjk"){
                                        that.removeSesstionAndStorage();
                                    }else{
                                        var obj=JSON.parse(data.trim())
                                        if(obj.ok=="oknonoas90asnaksjkasjjasasdjajksdjk" && obj.user_type=="admin"){
                                            window.location="../../admin/pages/home.html";
                                        }else if(obj.ok=="oknonoas90asnaksjkasjjasasdjajksdjk" && obj.user_type=="user"){
                                            if(that.checkLocalStorage()){
                                                config.setTitle('Welcome to Spycis');
                                                config.loadMain();
                                            }else{
                                                config.loadLoginBoxVgr();
                                            }
                                        }else{
                                            that.removeSesstionAndStorage();
                                        }
                                    }
                                }
			});
                }
                this.removeSesstionAndStorage=function (){
                    localStorage.removeItem("usaassassaesras_sasasdaseastaasiasls");
                    localStorage.removeItem("lang");
                    that.removeLocalStorage();
                    config.loadLoginBoxVgr();
                }
                this.setPeerID = function (id){
                    $.ajax({
                        url: config.getU()+"/SetPeerIdSvr",
                        type: 'POST',
                        data: {'user_id':config.getModel("ProfileConfig").getUser_id(),'keyid':id},
                        error: function(){
                            config.loadLoginBoxVgr();
                        },
                        success: function(data){
                            if(data.trim()=="false" || data.trim()=="error"){
                                alert(data);
                            }else{
                                config.getController("ProfileMgr").loadChatlistVgr(data);
                            }
                        }
                    });
                }
                
                this.removeLocalStorage=function (){
                    
                    localStorage.removeItem("lang_id");
                    localStorage.removeItem("question_details");
                    localStorage.removeItem("submit_question");
                    localStorage.removeItem("test_time");
                    localStorage.removeItem("testasdsaidasdasdasdasdasd");
                    localStorage.removeItem("active_time");
                    localStorage.removeItem("keyset");
                    localStorage.removeItem("section_count");
                    localStorage.removeItem("section_details");
                    localStorage.removeItem("test_date");
                    localStorage.removeItem("test_time");
                }
                this.logOut = function (){
                       config.getView("HeaderProfileVgr").setLoadBegin();
                       $.ajax({
                                url: config.getU()+"/LogOutSvr",
                                type: 'POST',
				error: function(){
                                    config.getView("HeaderProfileVgr").setLoadFinish();
				},
				success: function(data){
                                    if(data.trim()=="oknonoas90asnaksjkasjjasasdjajksdjk"){
                                    config.getView("HeaderProfileVgr").setLoadFinish();
                                    localStorage.removeItem("usaassassaesras_sasasdaseastaasiasls");
                                    that.removeLocalStorage();
                                     location.reload();
                                    }
                                }
			});
                }
                this.checkLocalStorage = function (){
                    if(typeof(Storage) !== "undefined") {
                      if(localStorage.getItem("usaassassaesras_sasasdaseastaasiasls") === null){
                          return false;
                      }else{
                           var obj = jQuery.parseJSON(decodeURIComponent(localStorage.getItem("usaassassaesras_sasasdaseastaasiasls")));
                           config.getModel("ProfileConfig").setUserName(obj.user_name);
                           config.getModel("ProfileConfig").setUser_id(obj.user_id);
                           config.getModel("ProfileConfig").setProfilePic(obj.pic);
                           config.getModel("ProfileConfig").setSoftwareDetails(obj.software_details);
                           return true;
                      }
                    } else {
                        return false;
                    }
                }
                this.checkSession();
                
	},
	
	/**
	 * let people create listeners easily
	 */
	ProfileConfigListener: function(list) {
		if(!list) list = {};
		return $.extend({
			loadBegin : function() { },
			loadFinish : function() { },
			loadItem : function() { },
			loadFail : function() { }
		}, list);
	}
});
