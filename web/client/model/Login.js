jQuery.extend({
	Login: function(config){
		var cache = {};
		var that = this;
		var listeners = new Array();
                var email =false;
                var password =false;
                var checkbox=false;
                //................................................................set captch details
                this.setEmail =function (value){
                    email=value;
                }
                this.getEmail =function (){
                    return email;
                }
                this.setPassword =function (value){
                    password=value;
                }
                this.getPassword =function (){
                    return password;
                }
                this.setCheckbox=function (value){
                    checkbox=value;
                }
                this.getCheckbox =function (){
                    return checkbox;
                }
                
                this.setValidation = function (){
                    $("#checkbox").focusout(function (){
                            if (($("#checkbox:checked").length)<=0) {
                               that.setCheckbox(false);
                           }else{
                               that.setCheckbox(true);
                            }
                        });
                    $.validator.addMethod("password_sql", function(value, element) {
                         return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
                                   && /[a-z]/.test(value) // has a lowercase letter
                                   && /\d/.test(value) // has a digit
                    }, "The password should meet some minimum requirements:<br>"+
                        "one no space between character<br>"+
                        "at least one lower-case character<br>"+
                        "at least one digit");
                    config.getView("LoginBoxVgr").getForm().validate({
                        
                        rules: {
                            password: {
                                required: true,
                                minlength: 6,
                                maxlength: 50,
                                password_sql:true
                            },
                            email: {
                                required: true,
                                minlength: 6,
                                maxlength: 50,
                            },
                        agree: "required"
                        },
                        messages: {
                        password: {
                            required: "Please provide a password",
                            maxlength:"Your password must consist of max length 50 characters",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        email: {
                            required: "Please provide a Valid email",
                            maxlength:"Your email must consist of max length 50 characters",
                            minlength: "Your email must be at least 6 characters long with @ and other hosting postfix.  "
                        },
                    },
                        errorElement : 'label',
                        errorLabelContainer: '#error',
                        highlight: function(element, errorClass) {
                            $("#error").empty();
                           $(element).parent().removeClass("has-warning");    
                           $(element).parent().removeClass("has-success");    
                           $(element).parent().addClass("has-error");    
                           switch($(element).attr('id')){
                               case "email":
                               that.setEmail(false);
                               case "password":
                               that.setPassword(false);
                           }
                        },
                        unhighlight: function(element, errorClass) {
                           $(element).parent().removeClass("has-warning");    
                           $(element).parent().removeClass("has-error");    
                           $(element).parent().addClass("has-success");    
                           switch($(element).attr('id')){
                               case "email":
                               that.setEmail(true);
                               case "password":
                               that.setPassword(true);
                           }
                        }, 
                        success: function(element) {
                           
                        }
                    });
                    if(config.getController("LoginMgr")==null){
                        $.getScript("../controller/LoginMgr.js").done(function() {
                            try {
                                config.setController("LoginMgr",new $.LoginMgr(config,config.getModel("Login"),config.getView("LoginBoxVgr")));
                            }catch(e){
                                alert(e);
                            }
                        }).fail(function() {alert("Error :006 problem in registrationMgr.js file ");}); 
                    }
                }
                this.checkLogin = function (form){
                    if(that.checkValidation()){
                        user_details ={}
                        form.find(":input").each(function() {
                            if($(this).attr("name")=="email"){
                                user_details["email"]= $(this).val();
                            }
                            if($(this).attr("name")=="password"){
                                user_details["password"]= $(this).val();
                            }
                        });
                        user_details["checkbox"]= that.getCheckbox();
                        that.checkLoginSvr(JSON.stringify(user_details));
                    }
                }
                
                this.checkLoginSvr =function (user_details){
                        that.insLoadBegin();
                        $.ajax({
                            url: config.getU()+"/CheckLoginSvr",
                            type: 'POST',
                            data: user_details,
                            success: function(data)
                            {
                                if(data.trim()=="error"){
                                    that.insLoadFail();
                                }else{
                                    if(typeof(Storage) !== "undefined") {
                                        localStorage.setItem("usaassassaesras_sasasdaseastaasiasls",encodeURIComponent(data));
                                    } 
                                    var obj = jQuery.parseJSON(data);
                                    config.getModel("ProfileConfig").setUserName(obj.user_name);
                                    config.getModel("ProfileConfig").setUser_id(obj.user_id);
                                    config.getModel("ProfileConfig").setProfilePic(obj.pic);
                                    config.getModel("ProfileConfig").setSoftwareDetails(obj.software_details);
                                    if(obj.user_type=="admin"){
                                        window.location="../../admin/pages/home.html"
                                    }else{
                                        config.loadMain();
                                    }
                                    
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError) {
                                that.insLoadFail();
                            }
                        });
                }
                
                this.checkValidation = function (){
                    if(!that.getEmail()){
                        that.showError("Please enter a valid email address.")
                        return false;
                    }
                    if(!that.getPassword()){
                        that.showError("Please provide a password.")
                        return false;
                    }
                    return true;
                }
                
                this.showError = function (error){
                   $("#error").empty();
                   $("#error").append(error);    
                   $("#error").css({'display':'block','color':'red'});    
                }
                
		/**
		 * add a listener to this model
		 */
                
		this.addListener = function(list){
			listeners.push(list);
		}
		
		this.insLoadBegin = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadBegin();
			});
		}
                
		this.insLoadFinish = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFinish();
			});
		}
                
		this.insLoadFail = function(){
			$.each(listeners, function(i){
				listeners[i].insLoadFail();
			});
		}
                
                
		/**
		 * tell everyone the item we've loaded
		 */
	
	},
	/**
	 * let people create listeners easily
	 */
	LoginListener: function(list) {
		if(!list) list = {};
		return $.extend({
			insLoadBegin : function() { },
			insLoadFinish : function() { },
			insLoadFail : function() { }
		}, list);
	}
});
